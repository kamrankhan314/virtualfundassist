<?php include('header.php'); ?>

<!-- ==== Page(Business-credit) Main Start ==== -->
<div id="sbl-container">
    <div class="hero hero-orange pb-0">
        <div class="hero-content d-flex flex-wrap align-items-center">
            <div class="col-12 col-md-6 text-left">
                <h1>Small Business Loans</h1>
                <h4>Give your small business the boost it needs by getting the ideal small business loans. Browse
                    through hundreds of options. Get funding promptly and effortlessly. Apply now!</h4>
            </div>
            <div class="col-12 col-md-6 hero-cta">
                <div class="cta-new">
                    <h3>Let's get started.</h3>
                    <form action="https://www.lendio.com/bp/basic-info" method="GET" class="cta-content"> <input
                            type="hidden" name="nosignup" value="1" />
                        <div class="input-container"> <span class="dollar">$</span> <input type="text"
                                placeholder="How much do you need?" name="amountSeeking" /></div> <button
                            class="primary-button">See your options</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div id="alt-header-swap"></div>
    <div id="sbl-way" class="gray-section">
        <div class="container">
            <h2>Small Business Loans</h2>
            <p>Small business loans vary quite a bit in their terms and requirements. Choose the one that perfectly
                aligns with your needs.</p>

            <h2>What are Small Business Loans, Exactly?</h2>
            <p>
                <span style="font-weight: 400;">When a lender provides a loan or a financial product to a small
                    business,
                    it is referred to as “Small Business Loans.”</span>
            </p>
            <p>
                <span style="font-weight: 400;">Small business loans include loans and products such as short-term
                    loans, SBA loans, loans for startups,
                    business term loans, loans for business acquisition, merchant cash advances, business line of
                    credit, credit cards for small business, A/R financing,
                    and financing for equipment.</span>
            </p>

            <h2>Small Business Loans – Deciding Factors:</h2>
            <p>
                <span>The creditworthiness of a small business is the main factor that determines whether or not it will
                    get the loan. Creditworthiness is determined using
                    a range of factors, including:</span>
            </p>
            <p>
            <ul>
                <li>Annual turnover</li>
                <li>Credit score</li>
                <li>Time period the business has operated</li>
            </ul>
            </p>
            <p>However, this is not the be-all end-all. An exception to this rule is startup loans in which the personal
                credit history of the entrepreneur is the
                primary consideration and not the creditworthiness. </p>
            <p>Equip yourself with the ‘right’ knowledge to effectively apply for a loan.</p>

            <h2>Unsecured vs Secured Business Loan </h2>
            <p>Wondering what’s the difference between a secured and unsecured loan?</p>
            <p>It all boils down to the collateral. While a secured loan is backed by collateral, an unsecured loan is
                not. That’s all. </p>
            <p>Make an informed decision about which type of loan you should go for after thoroughly analyzing the
                advantages and disadvantages.</p>
            <p><img class="sbl-faster-image aligncenter" src="images/ill-smbowner.svg" alt="" /></p>
        </div>
    </div>
    <div class="page-content d-flex flex-wrap sbl-fast">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2>The Different Types of Small Business Loans</h2>
                    <p>Following are some of the prominent types of loans for small businesses.</p>
                    <h3>Get Financing in Months: Fast Funding</h3>
                    <ul class="pb-4">
                        <li>
                            <strong>Small Business Administration (SBA) Loan:</strong>
                            The most favorite of all small business loans, SBA loans have favorable terms, including
                            low-interest rates and government-backed guarantee of paying up
                            to 80 percent of principal in case of default.
                        </li>
                        <li>
                            <strong>Business Term Loan:</strong>
                            The first loan that pops into mind when one thinks about small business loans.
                        </li>
                    </ul>
                    <h3>Get Financing in Weeks: Faster Funding</h3>
                    <ul class="pb-4">
                        <li>
                            <strong>Business Line of Credit:</strong>
                            Think of it as financing on-demand. You use the capital when you want to and only pay
                            interest on the money you use. A win-win.
                        </li>
                        <li>
                            <strong>Startup Loan: </strong>
                            Scale your business by getting the funding you need based on your personal credit.
                        </li>
                    </ul>

                    <h3>Get Loans in Days: Fastest Funding</h3>
                    <ul class="pb-4">
                        <li>
                            <strong>Short Term Loan:</strong>
                            Tailor-made to help you fulfill the immediate financing needs.
                        </li>
                        <li>
                            <strong>Business Credit Cards:</strong>
                            A credit card intended to cover day-to-day expenses of small businesses.
                        </li>
                    </ul>
                    <p>
                        <img class="sbl-faster-image" src="images/skater.svg" alt="" />
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div id="sbl-qualify" class="gray-section">
        <div class="container">
            <h2>Virtual Fund Assist – The Only Financing Solution You’ll Ever Need</h2>
            <p>
                We bring an extensive range of quality financial options for you and get you the financing you need in
                record-time.
                Our online loan application process is simple and easy. We do the hard yards, so you don’t have to.
                Apply for a small business loan today!
            </p>

            <h3>Application Process:</h3>
            <p>You might want to re-consider going to banks and financial institutions for a small business loan.
                Afterall,
                the procedure is time and effort-consuming with only a 20% chance of success. Yes, that’s right!</p>
            <p>Virtual Fund Assist understands your needs of a simpler loan application process, greater approval rate,
                and fast disbursement of funds. </p>
        </div>
    </div>
    <div class="page-content d-flex flex-wrap sbl-fast">
        <div class="container">
            <h3>Seamlessly Apply For A Small Business Loan With Virtual Fund Assist</h3>
            <ul class="pb-4">
                <li>
                    <strong>First Step:</strong>
                    Application form will take just 15 minutes to be filled. Fill it out and submit it.
                    Rest assured, our site in encrypted and we use the latest SSL technology to safeguard your
                    information.
                </li>
                <li>
                    <strong>Second Step:</strong>
                    After applying, you'll instantly receive the matches from our vast network of lenders. Our fund
                    managers will help you in choosing the right option.
                </li>
                <li>
                    <strong>Third Step:</strong>
                    After approval of your application, you'll receive the financing in just 2-3 days.
                </li>
            </ul>
            <p>Our team will guide you at every step of the tricky funding process.
                From helping you decide which type of loan you need and what your loan options are to supporting you in
                documentation and decision-making, we will do it all.</p>
            <h4 class="text-center">Become A Part Of The Virtual Fund Assist Family!</h4>
        </div>
    </div>
    <div class="bottom-cta full base dark pt-2">
        <div class="container cta-bottom py-5">
            <div class="row">
                <div class="col-12 py-md-5 text-center">
                    <h2>Get your small business loan today.</h2>
                    <form action="https://www.lendio.com/bp/basic-info" method="GET"
                        class="cta-content d-inline-block my-4">
                        <div class="input-container col-lg-8 float-lg-left mb-3"> <span
                                class="dollar d-none d-lg-block">$</span> <input type="text"
                                placeholder="How much do you need?" class="amount-seeking-input text-center pl-0"
                                name="amountSeeking" /></div>
                        <div class="button-container col-lg-4 float-lg-left"> <button
                                class="primary-button w-lg-100 ml-0 mb-2">Start</button></div>
                        <div class="clear"></div>
                    </form>
                    <h5 class="px-5" style="font-size: 17px;">Applying is free and it won't impact your credit</h5>
                    <h5 style="font-size: 17px;">Already have an account? &nbsp;<a
                            class="text-uppercase font-weight-bold" href="../bp/login.html"
                            data-wpel-link="internal"><strong>Sign in</strong></a></h5>
                </div>
            </div>
        </div>
    </div>
    <div class="contact-area border-top border-bottom py-5">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-7 d-flex text-center text-lg-left justify-content-center">
                    <img src="images/blue-line-phone.svg" class="mr-md-3 mr-4 mb-3 mb-lg-0" alt="Phone Icon">
                    <div class="hidden-sm-down pt-2"></div>
                    <h3 class="float-lg-left">Give us a call <br class="d-inheret d-sm-none" /><a class="phone"
                            href="tel:8558536346" data-wpel-link="internal">(855) 853-6346</a></h3>
                </div>
                <div class="col-12 col-md-5 text-center text-lg-left">
                    <div class="hidden-sm-down pt-2"></div>
                    <p class="hours">Monday - Friday | 9am - 9pm Eastern Time</p>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include('footer.php'); ?>
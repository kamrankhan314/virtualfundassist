<?php include('header.php'); ?>

  <!-- ==== Page(Home)  yellow-bar Start ==== -->
  <div class="hero position-relative"
        style="background: linear-gradient(90deg, #ff3366 0%, #fe9b02 100%); color: white;">
        <div class="site-close"><i class="far fa-times"></i></div>
        <div class="container p-lg-0">
            <div class="py-lg-2 row hero-row">
                <div class="hero-responsive col-12 text-left text-md-center align-middle mt-4 my-lg-2"> The Paycheck
                    Protection Program ended on August 8, 2020. We will continue to accept applications in hopes that
                    Congress passes an extension, although we are not currently sending applications to lenders. If PPP
                    resumes, Lendio will submit your application to a lender.</div>
            </div>
        </div>
    </div>

    <!-- ==== Page 404 ==== -->
    <div id="main-container">
            <div class="hero" style="color:inherit;background: ; background: linear-gradient(-243.43494882292202deg, rgb(63, 7, 221) 0%, rgb(19, 156, 236) 100%); color: white;">
                <div class="container py-md-5">
                    <div class="py-md-2 hero-row">
                        <div class="hero-responsive col-12 text-center align-middle my-2">
                            <h2 class="text-center">Page Not Found</h2>
                            <a class="site-header-start-btn primary-button" href="<?= $home_url ?>">
                                Back to Home <i class="fa flaticon-right-arrow"></i>
                            </a>
                        </div>
                        <div class="d-none d-lg-block col-lg-6"></div>
                    </div>
                </div>
            </div>
    </div>

    <div class="contact-area border-top border-bottom py-5">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-7 d-flex text-center text-lg-left justify-content-center"> 
                    <img src="images/blue-line-phone.svg" class="mr-md-3 mr-4 mb-3 mb-lg-0" alt="Phone Icon">
                    <div class="hidden-sm-down pt-2"></div>
                    <h3 class="float-lg-left">Give us a call <br class="d-inheret d-sm-none" /><a class="phone"
                            href="tel:8558536346" data-wpel-link="internal">(855) 853-6346</a></h3>
                </div>
                <div class="col-12 col-md-5 text-center text-lg-left">
                    <div class="hidden-sm-down pt-2"></div>
                    <p class="hours">Monday - Friday | 9am - 9pm Eastern Time</p>
                </div>
            </div>
        </div>
    </div>
<?php include('footer.php'); ?>
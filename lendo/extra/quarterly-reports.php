<?php include('header.php'); ?>

    <!-- ==== Page(quterly-reports) Main Start ==== -->
    <div id="quarterly-reports">
        <div class="header">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-8 py-5 header-content">
                        <h1>Lendio's SMB Economic Insights</h1>
                        <h3>A quarterly snapshot of how lending impacts small business health in all 50 states.</h3> The
                        following insights were generated from information provided by small business owners in each
                        state who completed the loan application process and secured funding using our marketplace.
                    </div>
                    <div class="d-none d-md-block col-4 py-5 header-image"> <img
                            src="images/person-map.svg" alt="" /></div>
                </div>
            </div>
        </div>
        <div class="container pt-5 mb-5">
            <div class="row mb-5">
                <div class="col-12 col-md-8 infographic-title">
                    <h3>National Business Insights</h3>
                </div>
                <div class="col-12 col-md-4 form-group ls-select infographic-select"> <select id="selected-state">
                        <option value="" disabled selected>Select State</option>
                        <option value="https://www.lendio.com/quarterly_report/alabama/">Alabama</option>
                        <option value="https://www.lendio.com/quarterly_report/alaska/">Alaska</option>
                        <option value="https://www.lendio.com/quarterly_report/arizona/">Arizona</option>
                        <option value="https://www.lendio.com/quarterly_report/arkansas/">Arkansas</option>
                        <option value="https://www.lendio.com/quarterly_report/california/">California</option>
                        <option value="https://www.lendio.com/quarterly_report/colorado/">Colorado</option>
                        <option value="https://www.lendio.com/quarterly_report/connecticut/">Connecticut</option>
                        <option value="https://www.lendio.com/quarterly_report/delaware/">Delaware</option>
                        <option value="https://www.lendio.com/quarterly_report/florida/">Florida</option>
                        <option value="https://www.lendio.com/quarterly_report/georgia/">Georgia</option>
                        <option value="https://www.lendio.com/quarterly_report/hawaii/">Hawaii</option>
                        <option value="https://www.lendio.com/quarterly_report/idaho/">Idaho</option>
                        <option value="https://www.lendio.com/quarterly_report/illinois/">Illinois</option>
                        <option value="https://www.lendio.com/quarterly_report/indiana/">Indiana</option>
                        <option value="https://www.lendio.com/quarterly_report/iowa/">Iowa</option>
                        <option value="https://www.lendio.com/quarterly_report/kansas/">Kansas</option>
                        <option value="https://www.lendio.com/quarterly_report/kentucky/">Kentucky</option>
                        <option value="https://www.lendio.com/quarterly_report/louisiana/">Louisiana</option>
                        <option value="https://www.lendio.com/quarterly_report/maine/">Maine</option>
                        <option value="https://www.lendio.com/quarterly_report/maryland/">Maryland</option>
                        <option value="https://www.lendio.com/quarterly_report/massachusetts/">Massachusetts</option>
                        <option value="https://www.lendio.com/quarterly_report/michigan/">Michigan</option>
                        <option value="https://www.lendio.com/quarterly_report/minnesota/">Minnesota</option>
                        <option value="https://www.lendio.com/quarterly_report/mississippi/">Mississippi</option>
                        <option value="https://www.lendio.com/quarterly_report/missouri/">Missouri</option>
                        <option value="https://www.lendio.com/quarterly_report/montana/">Montana</option>
                        <option value="https://www.lendio.com/quarterly_report/nebraska/">Nebraska</option>
                        <option value="https://www.lendio.com/quarterly_report/nevada/">Nevada</option>
                        <option value="https://www.lendio.com/quarterly_report/new-hampshire/">New Hampshire</option>
                        <option value="https://www.lendio.com/quarterly_report/new-jersey/">New Jersey</option>
                        <option value="https://www.lendio.com/quarterly_report/new-mexico/">New Mexico</option>
                        <option value="https://www.lendio.com/quarterly_report/new-york/">New York</option>
                        <option value="https://www.lendio.com/quarterly_report/north-carolina/">North Carolina</option>
                        <option value="https://www.lendio.com/quarterly_report/north-dakota/">North Dakota</option>
                        <option value="https://www.lendio.com/quarterly_report/ohio/">Ohio</option>
                        <option value="https://www.lendio.com/quarterly_report/oklahoma/">Oklahoma</option>
                        <option value="https://www.lendio.com/quarterly_report/oregon/">Oregon</option>
                        <option value="https://www.lendio.com/quarterly_report/pennsylvania/">Pennsylvania</option>
                        <option value="https://www.lendio.com/quarterly_report/rhode-island/">Rhode Island</option>
                        <option value="https://www.lendio.com/quarterly_report/south-carolina/">South Carolina</option>
                        <option value="https://www.lendio.com/quarterly_report/south-dakota/">South Dakota</option>
                        <option value="https://www.lendio.com/quarterly_report/tennessee/">Tennessee</option>
                        <option value="https://www.lendio.com/quarterly_report/texas/">Texas</option>
                        <option value="https://www.lendio.com/quarterly_report/utah/">Utah</option>
                        <option value="https://www.lendio.com/quarterly_report/vermont/">Vermont</option>
                        <option value="https://www.lendio.com/quarterly_report/virginia/">Virginia</option>
                        <option value="https://www.lendio.com/quarterly_report/washington/">Washington</option>
                        <option value="https://www.lendio.com/quarterly_report/washington-dc/">Washington DC</option>
                        <option value="https://www.lendio.com/quarterly_report/west-virginia/">West Virginia</option>
                        <option value="https://www.lendio.com/quarterly_report/wisconsin/">Wisconsin</option>
                        <option value="https://www.lendio.com/quarterly_report/wyoming/">Wyoming</option>
                    </select></div>
            </div>
            <script id="infogram_0_65447ea5-c614-4ac2-83fe-edfea014d8be" title="Lendio Quarterly Report_USA"
                src="../../e.infogram.com/js/dist/embed5952.js?iI9" type="text/javascript"></script>
        </div>
    </div>

<?php include('footer.php'); ?>
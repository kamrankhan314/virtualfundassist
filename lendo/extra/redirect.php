<?php
require_once 'vendor/autoload.php';
 
// init configuration
$clientID = '29175487793-q87ebea4gut01npp2e0f1l7g33ssl6so.apps.googleusercontent.com';
$clientSecret = 'z70G4gZInBiYC40olg3JhcOu';
$redirectUri = 'https://virtualfundassist.com/lendio/redirect';
  
// create Client Request to access Google API
$client = new Google_Client();
$client->setClientId($clientID);
$client->setClientSecret($clientSecret);
$client->setRedirectUri($redirectUri);
$client->addScope("email");
$client->addScope("profile");
 
// authenticate code from Google OAuth Flow
if (isset($_GET['code'])) {
  $token = $client->fetchAccessTokenWithAuthCode($_GET['code']);
  $client->setAccessToken($token['access_token']);
  
  // get profile info
  $google_oauth = new Google_Service_Oauth2($client);
  $google_account_info = $google_oauth->userinfo->get();
  //print_r($google_account_info);
  $Id =  $google_account_info->id;
  $email =  $google_account_info->email;
  $name =  $google_account_info->name;

    $_SESSION['logged_in_id'] = $Id;
    $_SESSION['logged_in_email'] = $email;
    $_SESSION['logged_in_name'] = $name;
?>
    <script type = "text/javascript">
      window.location ="<?= $base_url.'/lendio/admin' ?>";
  </script>
<?php 
  // now you can use this profile info to create account in your website and make user logged in.
} else {
  echo "<a href='".$client->createAuthUrl()."'>Google Login</a>";
}
?>

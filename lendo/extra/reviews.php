<?php include('header.php'); ?>

    <!-- ==== Page(Home)  Main-Content Start ==== -->
    <div id="success-stories">
        <div id="reviews-banner" class="full-alt-banner">
            <div class="intro-text">
                <article class="page company">
                    <h1>Success Stories</h1>
                </article>
            </div>
        </div>
        <div class="cta-new page-body full">
            <div class="page-body-content centered">
                <div class="wpsso-pinterest-pin-it-image" style="display:none !important;"> <img
                        src="<?= $home_url ?>/images/edenstouchLandscape1.png" width="0" height="0"
                        class="skip-lazy" style="width:0;height:0;" alt=""
                        data-pin-description="Just how awesome is Lendio, the nation&#039;s leading small business loan marketplace? Pretty awesome, but you don&#039;t have to take our word for it. Read reviews and testimonials from small business owners across America." />
                </div>
                <h2>At Virtual Fund Assist

, we believe that business loans should be as unique as business owners.</h2>
                <p> They should help you find your footing, forge ahead, and build momentum. You deserve a loan that
                    will help move the needle for you, your family, your business, and your community. You may be
                    starting small but you’re going to do big things.</p>
                <div class="cta-content lg">
                    <form action="https://www.lendio.com/bp/basic-info" method="GET" class="ng-pristine ng-valid">
                        <div class="input-container"> <span class="dollar">$</span> <input type="text"
                                placeholder="How much do you need?" name="amountSeeking"></div> <button class="btn">See
                            your options</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="page-body">
            <div class="page-body-content centered">
                <h2>Customer Testimonials</h2>
                <h3>While business owners have a shared mission to innovate, take risks, and grow, there’s no one
                    size-fits-all way of doing things. See how these Virtual Fund Assist

customers used funding to find their
                    momentum.</h3>
                <div class="testimonial">
                    <div class="testimonial-left">
                        <div class="video-wrapper"> <iframe class="embed-responsive-item"
                                src="https://www.youtube.com/embed/wpCEUadMZcM" frameborder="0"
                                allowfullscreen></iframe></div>
                    </div>
                    <div class="testimonial-right">
                        <h3>EuroMotorWerks</h3>
                        <p>
                        <p>When Rick Hagen of San Diego, California, developed serious medical problems, his plans for
                            opening a specialized car dealership were put on hold. LenVirtual Fund Assist

dio helped Rick get the capital he
                            needed to make his dream a reality.</p>
                        </p> <img src="<?= $home_url ?>/images/quotes.svg" alt="Quote"
                            width="25" />
                        <p class="testimonial-quote">If I hadn’t heard of you guys, I don’t know what I would have done.
                            You just believed in me.</p>
                        <p class="testimonial-name">Rick Hagen, Owner</p>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="testimonial">
                    <div class="testimonial-left">
                        <h3>Beach Helicopter Tours</h3>
                        <p>
                        <p>Michael Schaeffer of Destin, Florida, shares how Virtual Fund Assist

helped him get financing to fit the
                            unique needs of his seasonal business.</p>
                        </p> <img src="<?= $home_url ?>/images/quotes.svg" alt="Quote"
                            width="25" />
                        <p class="testimonial-quote">I don’t even know how to put it more than just an extreme relief
                            that I knew we could continue on through the slow season and make it to the busy season
                            again.</p>
                        <p class="testimonial-name">Michael Schaeffer, Owner</p>
                    </div>
                    <div class="testimonial-right">
                        <div class="video-wrapper"> <iframe class="embed-responsive-item"
                                src="https://www.youtube.com/embed/k0Ol4PLUTlc" frameborder="0"
                                allowfullscreen></iframe></div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="testimonial">
                    <div class="testimonial-left">
                        <div class="video-wrapper"> <iframe class="embed-responsive-item"
                                src="https://www.youtube.com/embed/0kld5pdg-rs" frameborder="0"
                                allowfullscreen></iframe></div>
                    </div>
                    <div class="testimonial-right">
                        <h3>Crown Jade Engineering</h3>
                        <p>
                        <p>After being turned away by banks, Mark Benjamin of Fort Collins, Colorado, came to Virtual Fund Assist

for
                            financing to grow his business building energy-efficient homes.</p>
                        </p> <img src="<?= $home_url ?>/images/quotes.svg" alt="Quote"
                            width="25" />
                        <p class="testimonial-quote">I plan to have really good growth the next year—double or triple. I
                            would not be able to grow, to expand into this, if I did not have the cash flow loan from
                            Virtual Fund Assist.</p>
                        <p class="testimonial-name">Mark Benjamin, Owner</p>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <div class="cta-new page-body alt">
            <div class="page-body-content centered"> <img
                    src="<?= $home_url ?>/images/email-icon.svg" alt="Email Icon" />
                <h2>Stay in the Loop</h2>
                <h3>Once a month you'll receive Virtual Fund Assist newsletter with useful tips and valuable resources for small
                    business success.</h3>
                <div class="cta-content">
                    <script>$(document).ready(function () {
                            hbspt.forms.create({
                                css: '',
                                portalId: '3012772',
                                formId: 'e5e8a68f-3f12-48d0-a1c4-d6e25b8c54a7'
                            });
                        });</script>
                    <div class="clear"></div> <a href="../agreements/privacy-policy/index.html" class="cta-privacy"
                        data-wpel-link="internal">We respect your privacy.</a>
                </div>
            </div>
        </div>
        <div class="page-body">
            <div class="page-body-tri-content centered spotlights">
                <h2>Small Business Spotlights</h2>
                <h3>Business owner, consultant, creative professional, contractor, entrepreneur. They’re all simple
                    names for one big job. Business ownership is so much more than an intangible dream or a lofty ideal.
                    It’s a way of life.</h3>
                <h3><a href="../blog/small-business-spotlights/index.html" data-wpel-link="internal">Read more success
                        stories</a></h3>
                <div class="page-body-column"> <a
                        href="../blog/small-business-spotlights/the-key-to-becoming-ridiculously-happy-in-business-and-life/index.html"
                        data-wpel-link="internal"> <img width="2650" height="1258"
                            src="<?= $home_url ?>/images/iStock-599470054-e1530282679111.jpg"
                            class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                            alt="Open for Business Sign"
                            srcset="https://www.lendio.com/wp-content/uploads/2014/01/iStock-599470054-e1530282679111.jpg 2650w, https://www.lendio.com/wp-content/uploads/2014/01/iStock-599470054-e1530282679111-300x142.jpg 300w, https://www.lendio.com/wp-content/uploads/2014/01/iStock-599470054-e1530282679111-1024x486.jpg 1024w, https://www.lendio.com/wp-content/uploads/2014/01/iStock-599470054-e1530282679111-800x380.jpg 800w"
                            sizes="(max-width: 2650px) 100vw, 2650px" data-wp-pid="83797" nopin="nopin"
                            title="5 Benefits of Buying an Existing Business" /> </a> <span
                        class="spotlight-date">3.12.2018</span>
                    <h2 style="font-size:2rem;">
                        <a href="../blog/small-business-spotlights/the-key-to-becoming-ridiculously-happy-in-business-and-life/index.html"
                            data-wpel-link="internal">2 Keys to Becoming Ridiculously Happy in Business and in Life</a>
                    </h2> <span class="spotlight-author">By <a
                                href="../author/chris-glenn/index.html" title="Posts by Chris Glenn" rel="author"
                                data-wpel-link="internal">Chris Glenn</a></span>
                </div>
                <div class="page-body-column"> <a
                        href="../blog/small-business-spotlights/3-ingredients-in-the-mix-for-small-business-success/index.html"
                        data-wpel-link="internal"> <img width="1257" height="669"
                            src="<?= $home_url ?>/images/meal-delivery-e1524865260371.jpg"
                            class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                            alt="Subscription-based meal delivery"
                            srcset="https://www.lendio.com/wp-content/uploads/2018/03/meal-delivery-e1524865260371.jpg 1257w, https://www.lendio.com/wp-content/uploads/2018/03/meal-delivery-e1524865260371-300x160.jpg 300w, https://www.lendio.com/wp-content/uploads/2018/03/meal-delivery-e1524865260371-1024x545.jpg 1024w, https://www.lendio.com/wp-content/uploads/2018/03/meal-delivery-e1524865260371-800x426.jpg 800w"
                            sizes="(max-width: 1257px) 100vw, 1257px" data-wp-pid="86190" nopin="nopin"
                            title="Should You Hop On the Subscription Bandwagon?" /> </a> <span
                        class="spotlight-date">3.19.2018</span>
                    <h2 style="font-size:2rem;"><a
                            href="../blog/small-business-spotlights/3-ingredients-in-the-mix-for-small-business-success/index.html"
                            data-wpel-link="internal">3 Ingredients in the Mix for Small Business Success</a></
                            style="font-size:2rem;"> <span class="spotlight-author">By <a
                                href="../author/chris-glenn/index.html" title="Posts by Chris Glenn" rel="author"
                                data-wpel-link="internal">Chris Glenn</a></span>
                </div>
                <div class="page-body-column"> <a
                        href="../blog/small-business-spotlights/veteran-business-military-precision/index.html"
                        data-wpel-link="internal"> <img width="1285" height="698"
                            src="../wp-content/uploads/2017/11/small-business-coworkers-e1510251512191.jpg"
                            class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                            alt="three small business coworkers"
                            srcset="https://www.lendio.com/wp-content/uploads/2017/11/small-business-coworkers-e1510251512191.jpg 1285w, https://www.lendio.com/wp-content/uploads/2017/11/small-business-coworkers-e1510251512191-300x163.jpg 300w, https://www.lendio.com/wp-content/uploads/2017/11/small-business-coworkers-e1510251512191-1024x556.jpg 1024w, https://www.lendio.com/wp-content/uploads/2017/11/small-business-coworkers-e1510251512191-800x435.jpg 800w"
                            sizes="(max-width: 1285px) 100vw, 1285px" data-wp-pid="84178" nopin="nopin"
                            title="How a California Veteran Runs His Business with Military Precision" /> </a> <span
                        class="spotlight-date">11.10.2017</span>
                    <h2 style="font-size:2rem;"><a
                            href="../blog/small-business-spotlights/veteran-business-military-precision/index.html"
                            data-wpel-link="internal">How a California Veteran Runs His Business with Military
                            Precision</a></ style="font-size:2rem;"> <span class="spotlight-author">By <a
                                href="../author/lis/index.html" title="Posts by Melissa Zehner" rel="author"
                                data-wpel-link="internal">Melissa Zehner</a></span>
                </div>
            </div>
        </div>
        <div class="page-body alt">
            <div class="page-body-bi-content centered reviews">
                <h2>Ratings &amp; Reviews</h2>
                <h3>See what these customers are saying about getting financing through Virtual Fund Assist.</h3>
                <h3><a href="https://www.trustpilot.com/review/www.lendio.com" target="_blank"
                        data-wpel-link="internal">See all reviews&nbsp;&nbsp;&gt;</a></h3>
                <div class="page-body-left">
                    <div class="review"> <img class="trustpilot-logo"
                            src="<?= $home_url ?>/images/trust-pilot-logo.svg"
                            alt="TrustPilot Logo" /> <img class="stars"
                            src="<?= $home_url ?>/images/5_stars.svg"
                            alt="TrustPilot 5 Stars" />
                        <div class="clear"></div>
                        <p>Keith</p>
                        <p>Quick and easy process. Dan was great helping me through the process. I would highly
                            recommend Dan and Virtual Fund Assist!</p>
                    </div>
                </div>
                <div class="page-body-right">
                    <div class="review"> <img class="trustpilot-logo"
                            src="<?= $home_url ?>/images/trust-pilot-logo.svg"
                            alt="TrustPilot Logo" /> <img class="stars"
                            src="<?= $home_url ?>/images/5_stars.svg"
                            alt="TrustPilot 5 Stars" />
                        <div class="clear"></div>
                        <p>Magdalena</p>
                        <p>Very easy process, and Landen answered all my questions without hesitation. Very
                            knowledgeable, and we got funded in a matter of hours. Thank you for taking care of us!</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom-cta pt-2 full base dark">
            <div class="container cta-bottom py-5">
                <div class="row">
                    <div class="col-12 py-md-5 text-center">
                        <h2>Get your small business loan today.</h2>
                        <form action="https://www.lendio.com/bp/basic-info" method="GET"
                            class="cta-content d-inline-block my-4">
                            <div class="input-container col-lg-8 float-lg-left mb-3"> <span
                                    class="dollar d-none d-lg-block">$</span> <input type="text"
                                    placeholder="How much do you need?" class="amount-seeking-input text-center pl-0"
                                    name="amountSeeking" /></div>
                            <div class="button-container col-lg-4 float-lg-left"> <button
                                    class="primary-button w-lg-100 ml-0 mb-2">Start</button></div>
                            <div class="clear"></div>
                        </form>
                        <h5 class="px-5" style="font-size: 17px;">Applying is free and it won't impact your credit</h5>
                        <h5 style="font-size: 17px;">Already have an account? &nbsp;<a
                                class="text-uppercase font-weight-bold" href="../bp/login.html"
                                data-wpel-link="internal"><strong>Sign in</strong></a></h5>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php include('footer.php'); ?>
<?php include('header.php'); ?>

<!-- ==== Page(Ecnomic-Injury-disaster-Loans) Main Start ==== -->
<div id="business-qa">
        <div data-vis="restricted" data-groups="" class="hg-1-col documentation-article">
            <div class="home-search">
                <div class="home-search-center">
                    <h1>All your small business loan questions, answered here.</h1>
                    <div class="search-input-container-home">
                        <form action="/home/search" method="GET">
                            <span class="search-icon"><img src="images/search-icon.png" /></span>
                            <input class="search-input" type="text"
                                placeholder="What can we help you with? Search for a topic or question…" name="phrase">
                        </form>
                    </div>
                </div>
            </div>
            <div class="hg-article">
                <div class="hg-article-body">
                    <div class="home-cats">
                        <a href="/home/types-of-business-loans" class="cat-box">
                            <div class="cat-box-image" id="type-image"></div>
                            <h3>Types of Business Loans</h3>
                        </a>
                        <a href="/home/qualifying-and-applying" class="cat-box">
                            <div class="cat-box-image" id="how-to-image"></div>
                            <h3>Qualifying and Applying</h3>
                        </a>
                        <a href="/home/small-business-basics" class="cat-box">
                            <div class="cat-box-image" id="common-q-image"></div>
                            <h3>Small Business Basics</h3>
                        </a>
                    </div>
                    <div class="article-boxes">
                        <div class="article-box">
                            <h3>Popular Articles</h3>
                            <ul class="list-unstyled stat-list pop-articles-list">
                                <li>
                                    <div><a href="/home/what-documents-do-i-need-to-apply-for-a-business-loan">
                                            What documents do I need to apply for a business loan?</a></div>
                                </li>
                                <li>
                                    <div><a href="/home/what-is-an-ach-loan">
                                            What is an ACH loan?</a></div>
                                </li>
                                <li>
                                    <div><a href="/home/what-is-an-sba-504-loan">
                                            What is an SBA 504 loan?</a></div>
                                </li>
                                <li>
                                    <div><a href="/home/will-i-qualify-for-a-business-loan">
                                            Will I qualify for a business loan?</a></div>
                                </li>
                                <li>
                                    <div><a href="/home/what-is-an-sba-express-loan">
                                            What is an SBA Express loan?</a></div>
                                </li>
                                <li class="list-action">
                                    <a href="/home/popular-articles">See more...</a>
                                </li>
                            </ul>
                        </div>
                        <div class="article-box">
                            <h3>New Articles</h3>
                            <ul class="list-unstyled stat-list">
                                <li>
                                    <div><a href="/home/how-buy-existing-business">
                                            How to buy an existing business</a></div>
                                </li>
                                <li>
                                    <div><a href="/home/how-build-client-base">
                                            How to build a client base</a></div>
                                </li>
                                <li>
                                    <div><a href="/home/how-much-collateral-business-loan">
                                            How much collateral is needed for a business loan?</a></div>
                                </li>
                                <li>
                                    <div><a href="/home/most-profitable-businesses">
                                            What are the most profitable businesses?</a></div>
                                </li>
                                <li>
                                    <div><a href="/home/buy-existing-business-no-money">
                                            How to buy an existing business with no money</a></div>
                                </li>
                                <li class="list-action">
                                    <a href="/home/new-articles">See more...</a>
                                </li>
                            </ul>
                        </div>
                        <div class="article-box">
                            <h3>Updated Articles</h3>
                            <ul class="list-unstyled stat-list">
                                <li>
                                    <div><a href="/home/section-179">
                                            What is Section 179?</a></div>
                                </li>
                                <li>
                                    <div><a href="/home/how-do-sba-loans-work">
                                            How Do SBA Loans Work?</a></div>
                                </li>
                                <li>
                                    <div><a href="/home/how-build-client-base">
                                            How to build a client base</a></div>
                                </li>
                                <li>
                                    <div><a href="/home/what-is-sba">
                                            What is the SBA?</a></div>
                                </li>
                                <li>
                                    <div><a href="/home/how-buy-existing-business">
                                            How to buy an existing business</a></div>
                                </li>
                                <li class="list-action">
                                    <a href="/home/updated-articles">See more...</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="home-help-container">
                <div class="home-message-container">
                    Can’t find your answer? Don’t worry, we’re here to help.
                    <div class="help-callout"><a href="https://www.lendio.com/contact">Send us a message ></a>
                    </div>
                </div>
                <div class="home-call-container">
                    Want to talk to a human? Give us a call
                    <div class="help-callout">(855) 853-6346</div>
                </div>
            </div>
            <div class="home-apply-container">
                <div class="home-apply-center">
                    <h3>Now that you know, it’s time to apply for your small business loan.</h3>
                    <form action="https://www.lendio.com/bp/app/dynamic?nosignup=1" method="GET">
                        <input class="search-input" type="text" placeholder="How much do you need?"
                            name="amountSeeking">
                        <button class="primary-button">SEE YOUR OPTIONS</button>
                    </form>
                </div>
            </div>
        </div>
</div>

<?php include('footer.php'); ?>
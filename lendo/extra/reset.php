<?php include('header.php'); ?>

  <!-- ==== Page(small-Business-loans) Main Start ==== -->
  <div data-server-rendered="true" id="__nuxt">
    <!---->
    <div id="__layout">
      <div data-v-70f04dc2>
        <div class="bg-white h-100 mh-100vh d-flex flex-column" data-v-70f04dc2>
          <div data-v-70f04dc2>
            <div style="display:none;" data-v-c6f17f3e>
              <div class="modal-mask">
                <div class="modal-wrapper">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title w-100"><span data-v-c6f17f3e>
                            <div data-v-c6f17f3e>Loading...</div>
                            <!---->
                            <div data-v-c6f17f3e></div>
                          </span></h5>
                        <!---->
                      </div>
                      <div class="modal-body"><span class="sign-modal" data-v-c6f17f3e>
                          <!---->
                          <div data-cy="esignature-text" data-v-c6f17f3e></div>
                        </span></div> <span data-v-c6f17f3e>
                        <div class="modal-footer" data-v-c6f17f3e><button type="button" disabled="disabled"
                            class="btn btn-primary disabled" data-v-c6f17f3e><span data-v-c6f17f3e>I
                              agree</span></button></div>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!---->
            <!---->
          </div>
          <div id="app" data-v-70f04dc2>
            <div class="container" data-v-70f04dc2>
              <div class="row mx-0 mt-4 p-4 justify-content-center">
                <div class="col-12 col-md-8 col-lg-5 p-0">
                  <h3>Reset Password</h3>
                  <p>Please enter your email and we'll send you reset instructions.</p>
                  <form name="resetPassword" class="align-self-center app-page">
                    <div>
                      <div data-cy="alert" class="alert-wrapper mb-3 closed" style="display:none;" data-v-6df1b500
                        data-v-6df1b500>
                        <div class="alert alert-undefined" data-v-6df1b500>
                          <div class="alert-content-wrapper w-100 d-flex" data-v-6df1b500>
                            <!----> <span class="alert-title" data-v-6df1b500></span> <span class="alert-content w-100"
                              data-v-6df1b500>
                              <div data-v-6df1b500></div>
                            </span>
                            <!---->
                          </div>
                        </div>
                      </div> <label for="email" class="control-label">Email</label>
                      <div class="text-center pb-3"><input tabindex="1" data-cy="reset-email-input" id="r-email"
                          name="email" type="email" autofocus="autofocus" class="form-control required error"></div>
                      <div class="buttons mt-2"><button id="send" data-cy="submit-reset-password"
                          class="btn btn-action btn-primary" data-v-34fbf442>
                          <div class="text-center position-relative" data-v-34fbf442>
                            <!---->
                            <div data-v-34fbf442>Send Instructions</div>
                          </div>
                        </button> <a href="login" class="float-right mt-2 gray-500">Cancel</a></div>
                    </div>
                  </form>
                  <!---->
                </div>
              </div>
            </div>
          </div>

  <?php include('footer.php'); ?>
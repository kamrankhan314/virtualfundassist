<?php include('header.php'); ?>
<?php include('connection.php'); ?>

<?php
if($_SERVER["REQUEST_METHOD"] == "POST") {
   $user_email = mysqli_real_escape_string($db,$_POST['email']);
   $user_password = mysqli_real_escape_string($db,md5($_POST['password'])); 
   
   $sql = "SELECT * FROM users WHERE user_email = '$user_email' AND user_password = '$user_password'";
   $result = mysqli_query($db,$sql);
   $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
  
   if(!empty($row) && sizeof($row)) {
      $base_url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
      $_SESSION['logged_in_id'] = $row['id'];
      $_SESSION['logged_in_email'] = $row['user_email'];
      $_SESSION['logged_in_name'] = $row['first_name'].$row['last_name'];
?>
  <script type = "text/javascript">
      window.location ="<?= $base_url.'/lendio/admin' ?>";
  </script>
<?php
   }else {
      $error = "Your Email or Password is invalid";
   }
}
?>
  
<!-- ==== Page(small-Business-loans) Main Start ==== -->
  <div data-server-rendered="true" id="__nuxt">
    <!---->
    <div id="__layout">
      <div data-v-70f04dc2>
        <div class="bg-white h-100 mh-100vh d-flex flex-column" data-v-70f04dc2>
          <div data-v-70f04dc2>
            <div style="display:none;" data-v-c6f17f3e>
              <div class="modal-mask">
                <div class="modal-wrapper">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title w-100"><span data-v-c6f17f3e>
                            <div data-v-c6f17f3e>Loading...</div>
                            <!---->
                            <div data-v-c6f17f3e></div>
                          </span></h5>
                        <!---->
                      </div>
                      <div class="modal-body"><span class="sign-modal" data-v-c6f17f3e>
                          <!---->
                          <div data-cy="esignature-text" data-v-c6f17f3e></div>
                        </span></div> <span data-v-c6f17f3e>
                        <div class="modal-footer" data-v-c6f17f3e><button type="button" disabled="disabled" class="btn btn-primary disabled" data-v-c6f17f3e><span data-v-c6f17f3e>I agree</span></button></div>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!---->
            <!---->
          </div>
          <div id="app" data-v-70f04dc2>
            <div class="d-flex justify-content-center" data-v-043eacb2 data-v-70f04dc2>
              <div class="w-100 p-4" style="max-width: 450px" data-v-043eacb2>
                <h3 class="text-center" data-v-043eacb2>Sign In</h3>
                <!----><?php if($error != "") { ?>
                <p class="error"><?= $error ?></p>
                <?php } ?>
                <div data-v-7e6d6c41 data-v-043eacb2>
                  <form method="POST" data-v-7e6d6c41>
                    <div data-cy="alert" class="alert-wrapper mb-3 closed" style="display:none;" data-v-6df1b500 data-v-6df1b500 data-v-7e6d6c41>
                      <div class="alert alert-undefined" data-v-6df1b500>
                        <div class="alert-content-wrapper w-100 d-flex" data-v-6df1b500>
                          <!----> <span class="alert-title" data-v-6df1b500></span> <span class="alert-content w-100" data-v-6df1b500>
                            <div data-v-6df1b500></div>
                          </span>
                          <!---->
                        </div>
                      </div>
                    </div>
                    <div class="form-group" data-v-7e6d6c41><label for="email" data-v-7e6d6c41>Email</label> 
                    <input data-cy="email" id="email" name="email" type="email" class="form-control" data-v-7e6d6c41 required>
                      <!---->
                      <!---->
                    </div>
                    <div class="form-group" data-v-7e6d6c41><label for="password" data-v-7e6d6c41>Password</label>
                      <input data-cy="password" id="password" name="password" type="password" class="form-control" data-v-7e6d6c41 required>
                      <!---->
                    </div>
                    <div class="d-flex justify-content-center mt-4" data-v-7e6d6c41><button id="login-btn" data-cy="submit" class="btn btn-action btn-primary" data-v-34fbf442 data-v-7e6d6c41>
                        <div class="text-center position-relative" data-v-34fbf442>
                          <!---->
                          <div data-v-34fbf442>Sign In</div>
                        </div>
                      </button></div>
                    <div class="d-flex justify-content-center mt-2" data-v-7e6d6c41><a href="reset" data-cy="forgot" id="forgot" class="btn btn-link" data-v-7e6d6c41>Forgot Password</a></div>
                  </form>
                  <div data-v-7e6d6c41>
                    <div class="d-flex justify-content-center align-items-center mt-4 mb-4" style="border-bottom: 1px solid #ccc; height: 0" data-v-7e6d6c41><span class="bg-white px-2" data-v-7e6d6c41>or</span></div>
                    <div class="text-center" data-v-7e6d6c41>
                      <button class="btn btn-facebook" data-v-a7181c0e data-v-7e6d6c41>
                        <img src="images/facebook-light.png" alt="Facebook Login" class="facebook-icon" data-v-a7181c0e>Login with Facebook
                      </button>
                    </div>
                  </div>
                </div>
                <p class="mt-3 text-center" data-v-043eacb2>Don't have a Lendio Account? <a href="basic-info" data-v-043eacb2>Sign Up</a>.</p>
              </div>
            </div>
          </div>
          <div class="secondary-logo" data-v-70f04dc2></div>
        </div>
      </div>
    </div>
  </div>
  <script>window.__NUXT__ = (function (a, b, c, d, e, f, g, h, i, j, k, l, m, n) { return { layout: "light", data: [{ query: {} }], error: a, state: { activationId: d, authUser: a, authToken: a, affiliateId: a, authenticated: b, facebookAuth: { status: d, userData: a }, pageTitle: "Borrower Portal - Powered by Lendio", favicons: [{ rel: h, type: j, href: "images\u002Ffavicon-32x32.png" }, { rel: h, type: j, href: "images\u002Ffavicon-16x16.png" }, { rel: "mask-icon", href: "images\u002Fsafari-pinned-tab.svg", color: "#00bff0" }, { rel: "shortcut icon", href: m }, { rel: h, href: m, type: "image\u002Fx-icon" }], dynamicAppProgress: {}, renewalAppProgress: {}, currentPage: a, marketingOwnership: a, document: { pendingUploads: [], documents: [], pppPillsSelected: [], loaded: b, requiredDocumentPills: [{ name: "July 2020", accept: f, meta: { months: ["07\u002F2020"], formattedName: "BankStatement 07-20 (M)", category: e, _localId: "_efag0e2kk" } }, { name: "June 2020", accept: f, meta: { months: ["06\u002F2020"], formattedName: "BankStatement 06-20 (M)", category: e, _localId: "_bvfqfwgnh" } }, { name: "May 2020", accept: f, meta: { months: ["05\u002F2020"], formattedName: "BankStatement 05-20 (M)", category: e, _localId: "_mydtvxm06" } }, { name: "April 2020", accept: f, meta: { months: ["04\u002F2020"], formattedName: "BankStatement 04-20 (M)", category: e, _localId: "_fj5ir48yr" } }, { name: "March 2020", accept: f, meta: { months: ["03\u002F2020"], formattedName: "BankStatement 03-20 (M)", category: e, _localId: "_y9uuk042u" } }, { name: "February 2020", accept: f, meta: { months: ["02\u002F2020"], formattedName: "BankStatement 02-20 (M)", category: e, _localId: "_6uvrcsxfc" } }], additionalDocumentPills: [{ name: n, meta: { category: "voidedCheck", _localId: "_bhfqypmox", formattedName: n } }, { name: "Proof of Ownership", helpText: "\n          \u003Cp\u003EPlease provide any one of the following documents as proof of business ownership\u003C\u002Fp\u003E\n\n          \u003Ch5\u003ESchedule K-1\u003C\u002Fh5\u003E\n          \u003Cp\u003EIRS form used by LLC and Corporations to file taxes. Owners of the business must be listed on this form. Funding providers use this document to verify ownership of LLC and corporation type businesses.\u003C\u002Fp\u003E\n\n          \u003Ch5\u003ESchedule C\u003C\u002Fh5\u003E\n          \u003Cp\u003EIRS form used by sole proprietors to file taxes, owners of the business must be listed on this form. Funding providers use this document to verify ownership of sole proprietor type businesses.\u003C\u002Fp\u003E\n\n          \u003Ch5\u003EArticles of Incorporation\u003C\u002Fh5\u003E\n          \u003Cp\u003ECorporations are required to file Articles of Incorporation. These documents list the owners of the corporation. Funding providers sometimes are able to use these documents to verify ownership of a corporation.\u003C\u002Fp\u003E\n\n          \u003Ch5\u003EBusiness License\u003C\u002Fh5\u003E\n          \u003Cp\u003ESome lenders will use a business license as long as the license has the name of the business, the owner’s name(s) and the address of the business.\u003C\u002Fp\u003E\n          ", meta: { category: "ownershipVerification", _localId: "_67p3zs5fa", formattedName: "Ownership Verification" } }, { name: "Month-to-Date Transactions", accept: "application\u002Fpdf", fileText: "PDF", helpText: "\n          \u003Cp\u003EA Month-to-Date (MTD) transaction report is a summary of transactions in your business bank account from the beginning of the current month to the present day.\u003C\u002Fp\u003E\n\n          \u003Ch5\u003EGeneral Instructions\u003C\u002Fh5\u003E\n          \u003Cp\u003EThe MTD transaction report varies from bank to bank. Here are some guidelines that may help you find and generate this summary:\u003C\u002Fp\u003E\n\n          \u003Col style=\"padding-left:60px\"\u003E\n            \u003Cli\u003ELog in to your bank’s online portal.\u003C\u002Fli\u003E\n            \u003Cli\u003ELocate a “Transactions” or “Summary” page. This is often under the “Accounts” section.\u003C\u002Fli\u003E\n            \u003Cli\u003EFilter the transactions by date. There may be a “Current Month” option, or a “Custom” option that will allow you to select the starting and ending dates.\u003C\u002Fli\u003E\n            \u003Cli\u003ESave the report as a PDF.\u003C\u002Fli\u003E\n          \u003C\u002Fol\u003E\n\n          \u003Cp\u003EReach out to a Lendio representative if you are unable to create a MTD Transaction Report. We will be happy to assist you.\u003C\u002Fp\u003E\n          ", meta: { months: ["08\u002F2020"], category: "mtdTransactions", _localId: "_j8rrh4jtn", formattedName: "MTD Transactions 08-15-20" } }], otherDocumentPills: [{ name: "Upload Document", meta: { _localId: "_08s7xf1uf", category: "other" } }] }, borrower: { borrower: a, borrowerId: a, borrowerValues: a, borrowerHasFundedDeals: a, borrowerIsPPPWithdrawn: a, borrowerHasPPPInterest: a, borrowerLastStatus: a, borrowerShouldPPPOptIn: a, borrowerExperiments: [], pppAppStatus: a, pppIncompleteStep: a, lenderUser: a, lenderUserLoaded: b, lenderUserOnline: b, signatureExpired: b, returningUserParams: {} }, cobrand: { loaded: b, config: a, affiliateCobrandEnabled: i }, progress: { appProgress: {} }, experiments: { experimentGroups: {} }, experience: { current: a, shouldExitEarly: a, loading: a, route: d, experienceName: d }, returnUser: { returnUser: a }, questions: { filterQuestionsInitialized: b, indexedAttributes: {}, answers: {}, infoAnswers: {}, mineral: a, pages: [], visiblePages: [], infoQuestions: [], filterQuestions: [], mlWhitelist: [], predictedCategories: a, historicIds: [], currentPage: a, errorMsg: d, loadingQuestions: b, savingAnswers: b, overallWhitelist: [], pageBlacklist: [], alwaysAskedAliases: [] }, covidQuestions: { indexedQuestions: {}, answers: {}, errorMsg: d, loadingQuestions: b, savingAnswers: b, skipPartner: b }, requests: { requests: [], loaded: b, error: a }, deals: { deals: [], hasBlueVineDeal: i, visibleDeals: [], declineReasons: [], loaded: b }, creditCards: { creditCards: a, creditCard: a, creditCardCategories: a, categoriesLoading: b, filteredCards: a, loading: b, error: a, warning: a }, credit: { creditScore: a, connecting: b, loading: b, error: a }, finicity: { mfa: { mfaRetryCb: a, mfaChallenges: a, mfaSession: a, answered: b }, agreedToTerms: a, mfaAnswer: a, accounts: [], activatedAccounts: [], bankValues: a, error: a, timedOut: a, loading: b, finicityProgress: 1, interval: a, aggregatorUserCreated: b, finicityConnectLink: d, statements: [], activePage: "institutionSelect" }, optIns: { allOptions: [], options: [], financingOptions: [], markingInterested: b, loading: b, leadId: a, error: a }, ppp: { firstName: d, lastName: d, businessName: d, emailAddress: d, phoneNumber: d, submitted: b, applicationSigned: b, mobile_opt_in: b, agreeToTos: b }, portal: { navLinks: [{ title: "Dashboard", url: "\u002Fportal", icon: "icon_overview.png" }, { title: "Offers", url: "\u002Fportal\u002Foffers", icon: "icon_offers.png" }, { title: "Credit Cards", url: "\u002Fportal\u002Fcredit-cards", icon: "icon_creditcard.png" }, { title: "Business Resources", url: "\u002Fportal\u002Fbusiness-resources", icon: "icon_resources.png" }], leadSourceRestricted: b } }, serverRendered: i, env: { API_URL: k, AVATAR_BASE_URL: l, baseUrl: "https:\u002F\u002Fwww.lendio.com\u002F", BP_API_URL: "https:\u002F\u002Fwww.lendio.com\u002Fapi\u002Fv2", BP_BASE_URL: "https:\u002F\u002Fwww.lendio.com\u002Fbp", CALCULATORS_BASE_URL: "https:\u002F\u002Fwww.lendio.com\u002Fcalculators", CHATLIO_ID: "fb5de168-8a80-4cec-65d1-2b9d604ea41f", debugEnabled: g, environment: "LIVE", HP_BASE_URL: "https:\u002F\u002Fwww.lendio.com", HP_TEMPLATE_BASE_URL: "https:\u002F\u002Fwww.lendio.com\u002Fcms\u002Fwp-content\u002Fthemes\u002Flendio-standards", HP_UPLOADS_BASE_URL: "https:\u002F\u002Fwww.lendio.com\u002Fcms\u002Fwp-content\u002Fuploads", logglyKey: c, LP_BASE_URL: "https:\u002F\u002Flender.lendio.com", ML_API_TOKEN: d, ML_API_URL: d, PIPELINE_BASE_URL: "https:\u002F\u002Fpipeline.lendio.com", project: "scotty", ROLLBAR_REPORT_LEVEL: "debug", ROLLBAR_TOKEN_CLIENT: "638f5b02dc9646778d0a30fc1da16f9b", ROLLBAR_TOKEN_SERVER: "974a6a5286db42998f8407a1c9050f16", s3Url: l, segmentio_read_key: "rca0mb1igi", segmentio_write_key: "1fhhxusvqc", showDebugTrackingPixel: g, slimUrl: k, YOURLS_API_URL: "https:\u002F\u002Fpipeline.lendio.com\u002Fu", PUSHER_APP_ID: "51545", PUSHER_APP_KEY: "0e5801f1dc37dc071b31", PUSHER_APP_SECRET: "51e3e340369b24158f4a", displayTrackingPixel: c, DASH_HIDE_ACTIVE_DEALS: c, DASH_HIDE_APP_COMPLETION_CARD: c, DASH_HIDE_CREDIT_CARDS: c, DASH_HIDE_LENDER_DOCS_REQ: c, DASH_HIDE_OPT_IN_OPTIONS: c, DASH_HIDE_OPTIONS_NAV: c, DASH_HIDE_OTHER_OPTIONS: c, DASH_HIDE_SUBMIT_APP_REQ: c, DASH_USE_TENANT_ALERT: c, HIDE_CHATLIO: c, HIDE_FOOTER: c, HIDE_SIGNUP: g, HIDE_SOCIAL_LOGIN: g, HIDE_PASSWORD_RESET: c, LEGAL_EASE: c, REMOVE_NAVBAR: c, THEME_COLOR_ACCENT: c, THEME_COLOR_BACKGROUND: c, THEME_COLOR_BUTTON_ACTIVE: c, THEME_COLOR_BUTTON_DISABLED: c, THEME_COLOR_BUTTON_HOVER: c, THEME_COLOR_ICONS: c, THEME_COLOR_LINK: c, THEME_COLOR_PRIMARY: c, THEME_LOGO: c, THEME_SQUARE_BUTTON: g, USE_DOCS_ONLY_FLOW: c, USE_TENANT_THEME: c, PPP_ACTIVE: "true" } } }(null, false, void 0, "", "bankStatement", ".pdf", "false", "icon", true, "image\u002Fpng", "https:\u002F\u002Fpipeline.lendio.com\u002Fapi", "https:\u002F\u002Fs3.amazonaws.com\u002Flendiols-assets\u002Favatar\u002F", "images\u002Ffavicon.ico", "Voided Check"));</script>
  <script src="_nuxt/5b287e27307c66124998.js" defer></script>
  <script src="_nuxt/pages/login.d305178214bae52b1981.js" defer></script>
  <script src="_nuxt/commons.app.f346a2bf09a9c935d0ac.js" defer></script>
  <script src="_nuxt/vendors.app.5536aad9abec168df5f1.js" defer></script>
  <script src="_nuxt/app.766b6bfa8f021e818d90.js" defer></script>

  <!-- Pixel Service Containers -->
  <div id="pixelContainer" style="display:none"></div>
  <div id="pixelDebugContainer" style="display:none"></div>
  <script src="../../connect.finicity.com/assets/sdk/finicity-connect.min.js"></script>
  <!-- <script type="text/javascript">window.LendioPerformanceLogging.docReadyTime = window.LendioPerformanceLogging.calculateLoadTime()</script> -->

<?php include('footer.php'); ?>
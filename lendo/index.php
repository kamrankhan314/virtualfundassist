<?php include('header.php'); ?>

<!-- ==== Page(Home)  yellow-bar Start ==== -->
<div class="hero position-relative" style="background: linear-gradient(90deg, #ff3366 0%, #fe9b02 100%); color: white;">
    <div class="site-close"><i class="fa fa-times"></i></div>
    <div class="container p-lg-0">
        <div class="py-lg-2 row hero-row">
            <div class="hero-responsive col-12 text-left text-md-center align-middle mt-4 my-lg-2">
                The President has signed a relief bill
                that includes $284 billion for
                Paycheck Protection Program (PPP)
                loans. Virtual Fund Assist will submit
                completed, eligible applications to
                PPP lenders once the program
                officially resumes in January 2021.
            </div>
        </div>
    </div>
</div>

<!-- ==== Page(Home)  Main-Content Start ==== -->
<div id="home-main-container" style="overflow: hidden;">
    <div class="box px-2">
        <div class="triangle-container d-lg-block d-none"> <svg viewBox=" 0 0 300 350" height="150" width="150">
                <polygon points="50 15, 100 100, 0 100" fill="none" stroke="white" stroke-width="15"></polygon>
            </svg></div>
        <div class="half-circle-left d-lg-block d-none"></div>
        <div class="big-square-left d-lg-block d-none"></div>
        <div class="little-square d-lg-block d-none"></div>
        <div class="pentagon-container d-lg-block d-none"> <svg viewBox=" 0 0 100 100" height="50" width="50">
                <polygon points="26,86 11.2,40.4 50,12.2 88.8,40.4 74,86" fill="none" stroke="white" stroke-width="12"></polygon>
            </svg></div>
        <div class="big-square-right d-lg-block d-none"></div>
        <div class="half-circle-right d-lg-block d-none"></div>
        <div class="little-circle-right d-lg-block d-none"></div>
        <div class="container px-auto mb-md-4 mb-0">
            <div class="row pb-md-5 pb-0 my-0 circle mx-0">
                <div class="col-12 px-0 py-0 pb-md-2 pb-0 pt-md-5 pt-0">
                    <div>
                        <h1 class="my-4 text-center test-home-title pb-1 pt-md-5 pt-0">Become A Part Of America’s Best Business Loans Platform
                        </h1>
                    </div>
                    <hr class="d-block d-md-none px-5 mx-5" style="border-bottom: 1px solid #eeeeee;" />
                </div>
                <div class="row mb-5">
                    <div class="col-md-6 col-12 mp-cta-home-test text-center px-0 text-md-right test-cta-border">
                        <div class="px-lg-5 px-2 pr-md-5 pl-md-0">
                            <h2 class="test-sub-header mb-0 pt-2 pt-md-0 pb-md-2 pb-0">Paycheck Protection Loan Program</h2>
                        </div>
                        <div class="px-lg-5 pl-md-0 pr-md-5 ml-xl-5">
                            <p class="py-5 mb-3 px-lg-0 px-md-0 px-3 pl-lg-4 ml-lg-5">Start your PPP loan application via Virtual Fund Assist and find the ideal PPP lenders today!</p>
                        </div>
                        <div class="col-12 cta-new pb-md-5 px-0 text-md-right pr-md-5" style="padding-bottom: 35px;"> <a id="ppp-cta-button" href="<?= $home_url ?>/tabs/ppp/owner-info" class="primary-button px-5" data-wpel-link="internal">Get Started</a></div>
                    </div>
                    <div class="col-md-6 col-12 mp-cta-home-test text-center px-0 text-md-left">
                        <div class="px-lg-5 px-2 pl-md-5 pr-md-0">
                            <h2 class="test-sub-header mb-0 pt-2 pt-md-0 pb-md-2 pb-0">Standard Business Loans & Financing</h2>
                        </div>
                        <div class="px-lg-5 pr-md-0 pl-md-5 mr-xl-5">
                            <p class="py-5 mb-3 px-lg-0 px-md-0 px-3 pr-lg-4 mr-lg-5">Look for a business line of credit, short term business loans, and more from over 300+ lenders.</p>
                        </div>
                        <div class="col-12 cta-new pb-md-5 px-0 text-md-left pl-md-5 pb-3"> <a id="mp-cta-button" href="<?= $home_url ?>/tabs/basic-info" class="primary-button px-5" data-wpel-link="internal">Get
                                Started</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="alt-header-swap"></div>
    <div class="alt-white">
        <div class="container py-0 py-md-5">
            <h2 class="text-center">How Virtual Fund Assist Works?</h2>
            <div class="row" itemscope itemtype="http://schema.org/LoanOrCredit">
                <div class="col-12 col-lg-4 p-4 p-md-5 text-center text-lg-left border-none" itemprop="Apply" itemscope itemtype="http://schema.org/None">
                    <img src="images/apply.svg" class="mb-3" alt="Apply" style="min-width:30px;" height="52.26" width="52.01" data-wp-pid="89778" nopin="nopin" />
                    <h3 itemprop="name">Apply Conveniently</h3>
                    <div class="fact-text">
                        <h5 itemprop="value">
                            <p>Fill out one simple application within a few minutes. It’s free and does not impact your credit score.</p>
                        </h5>
                    </div>
                </div>
                <div class="col-12 col-lg-4 p-4 p-md-5 text-center text-lg-left border-none" itemprop="Compare" itemscope itemtype="http://schema.org/None">
                    <img src="images/options.svg" class="mb-3" alt="Compare Options" style="min-width:30px;" height="54" width="55.82" data-wp-pid="89779" nopin="nopin" />
                    <h3 itemprop="name">Compare Seamlessly</h3>
                    <div class="fact-text">
                        <h5 itemprop="value">
                            <p>Check out the various financing options. Explore over 300+ business loan options and pick the best one with our assistance.</p>
                        </h5>
                    </div>
                </div>
                <div class="col-12 col-lg-4 p-4 p-md-5 text-center text-lg-left border-none" itemprop="Succeed" itemscope itemtype="http://schema.org/None">
                    <img src="images/success.svg" class="mb-3" alt="Succeed" style="min-width:30px;" height="54" width="55" data-wp-pid="89780" nopin="nopin" />
                    <h3 itemprop="name">Run Business Successfully</h3>
                    <div class="fact-text">
                        <h5 itemprop="value">
                            <p>Get approved and gain access to capital within 24 hours. Scale your business to become bigger and better.</p>
                        </h5>
                    </div>
                </div>
                <p class="col-12 text-center"><a class="primary-button" href="bp/basic-info.html" data-wpel-link="internal">See if you qualify</a></p>
            </div>
        </div>
    </div>
    <div class="alt-white py-5">
        <div class="container py-lg-0 py-5">
            <div class="row">
                <div class="col-12 col-md-6 py-5 text-center text-md-left">
                    <h2>Explore Multiple <br /> Financing Options.</h2>
                    <p class="body-large">Fill out just one loan application and let our state of the art algorithm and qualified professionals do the rest for you.</p>
                    <p class="body-large">We will browse through numerous loan options and handpick the best ones for you from our vast network of the top lenders.</p>
                    <p class="body-large">Join our community of business owners and let us empower you!.</p>
                    <div class="mt-4"><a class="primary-button" href="bp/lenders.html" data-wpel-link="internal">Compare Lenders</a></div>
                </div>
                <div class="col-12 text-center my-auto offset-md-1 col-md-5 pb-5 py-md-4">
                    <img src="images/home-img-1.png" class="attachment-full size-full" alt="Connect with lenders" height="414" width="300" data-wp-pid="91504" nopin="nopin" />
                </div>
            </div>
        </div>
    </div>
    <div class="py-5 py-md-5 alt-white border-top">
        <div class="container">
            <div class="row">
                <div class="text-center text-lg-left my-auto col-md-6 d-none d-md-block">
                    <img src="images/home-img-2.png" class="attachment-full size-full" alt="Get Funded" height="541.96" width="400" data-wp-pid="91505" nopin="nopin" />
                </div>
                <div class="col-12 mx-auto col-md-6 pl-0 pl-md-4 py-5">
                    <h2>Get Fast Funding.</h2>
                    <p class="body-large">Time is money, and we understand that. This is precisely why we help you get fast funding. When we say fast, we mean that.</p>
                    <p class="body-large">With us, you can get access to financing in as little as 24 hours. Yes, that’s right.</p>
                    <p class="body-large">Finding it hard to believe? Fill out the application and experience it yourself.</p>
                    <div class="mt-4"><a class="primary-button" href="bp/basic-info.html" data-wpel-link="internal">Get started</a></div>
                </div>
                <div class="col-12 text-center d-block d-md-none"> <img src="images/get-funded-illustration.svg" class="attachment-full size-full" alt="Get Funded" height="541.96" width="546.35" data-wp-pid="91505" nopin="nopin" title="Explore America&#039;s Leading Business Loan Marketplace" /></div>
            </div>
        </div>
    </div>
    <div class="stats py-0 py-lg-5">
        <div class="container py-5">
            <div class="row">
                <div class="col-12 col-md-4 text-center py-5">
                    <h2 class="stat-figure">$8 billion</h2>
                    <h2>in PPP loan approvals facilitated through Virtual Fund Assist</h2>
                </div>
                <div class="col-12 col-md-4 text-center py-5">
                    <h2 class="stat-figure">100,000+</h2>
                    <h2>businesses received PPP loan approvals with Virtual Fund Assist&#8217;s help</h2>
                </div>
                <div class="col-12 col-md-4 text-center py-5">
                    <h2 class="stat-figure">$73,000</h2>
                    <h2>average size of PPP loan approvals facilitated through us</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="loan-factors" style="background-color: #f7fafe;">
        <div style="height: 150px;overflow: hidden;">
            <svg viewBox="0 0 500 150" preserveAspectRatio="none" style="height: 100%; width: 100%;" class="flip-vertical-horizontal">
                <path d="M0.00,49.98 C240.97,224.50 257.34,-75.48 500.00,49.98 L500.00,150.00 L0.00,150.00 Z" style="stroke: none; fill: #fff;"></path>
            </svg>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12 text-center pt-5">
                    <h2>We Cater To All Your Business Loan Needs.</h2>
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-md-6 col-lg-5 offset-lg-1 text-left px-0 pt-3 pb-5">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-2" style="margin-top: 12px;">
                                            <img class="loan-factor-icon" width="30px" src="images/loc.svg" alt="Line of Credit">
                                        </div>
                                        <div class="col-9">
                                            <h3 class="py-md-2 loan-types">
                                                <a href="business-loans/line-of-credit/index.html" data-wpel-link="internal">Line of <span class="text-nowrap">Credit <i class="fa fa-arrow-right"></i></a>
                                            </h3>
                                            <p class="body-large">Benefit from the cost-effective flexibility and financial safety net.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-5 offset-lg-1 text-left px-0 pt-3 pb-5">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-2" style="margin-top: 12px;">
                                            <img class="loan-factor-icon" width="30px" src="images/term.svg" alt="Term Loan">
                                        </div>
                                        <div class="col-9">
                                            <h3 class="py-md-2 loan-types"><a href="business-loans/short-term-loans/index.html" data-wpel-link="internal">Term <span class="text-nowrap">Loan <i class="fa fa-arrow-right"></i></a></h3>
                                            <p class="body-large">Get the funding you need to effectively grow your business.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-5 offset-lg-1 text-left px-0 pt-3 pb-5">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-2" style="margin-top: 12px;">
                                            <img class="loan-factor-icon" width="30px" src="images/credit-card.svg" alt="Credit Card">
                                        </div>
                                        <div class="col-9">
                                            <h3 class="py-md-2 loan-types"><a href="business-loans/business-credit-card/index.html" data-wpel-link="internal">Credit <span class="text-nowrap">Card
                                                        <i class="fa fa-arrow-right"></i></a></h3>
                                            <p class="body-large">Enhance your working capital and cover day to day expenses hassle-free.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-5 offset-lg-1 text-left px-0 pt-3 pb-5">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-2" style="margin-top: 12px;">
                                            <img class="loan-factor-icon" width="30px" src="images/startup.svg" alt="Credit Card">
                                        </div>
                                        <div class="col-9">
                                            <h3 class="py-md-2 loan-types"><a href="business-loans/startup-loans/index.html" data-wpel-link="internal">Startup <span class="text-nowrap">Loan
                                                        <i class="fa fa-arrow-right"></i></a></h3>
                                            <p class="body-large">Inject capital in your venture to boost it without giving away any equity.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Testimonials For VFA -->
    <div class="alt-white py-0">
        <div style="height: 150px; overflow: hidden;">
            <svg viewBox="0 0 500 150" preserveAspectRatio="none" style="height: 100%; width: 100%;" class="flip-vertical-horizontal">
                <path d="M0.00,49.98 C240.97,224.50 257.34,-75.48 500.00,49.98 L500.00,150.00 L0.00,150.00 Z" style="stroke: none; fill:#f7fafe;"></path>
            </svg>
        </div>
        <div class="container pb-5 mb-5">

            <h2 class="text-center">Testimonials</h2>
            <p class="text-center">See What Our Customers Are Saying About Us.</p>
            <!-- Testinomils #01 -->
            <div class="testimonials">
                <i class="fa fa-comments"></i>
                <p class="body-large">Virtual Fund Assist is literally saving businesses one loan at a time. We had some serious short-term cash flow problems, and the instant business line of credit we got due to Virtual Fund Assist helped us get through difficult times. Highly recommended!</p>
            </div>

            <!-- Testinomils #02 -->
            <div class="testimonials">
                <i class="fa fa-comments"></i>
                <p class="body-large">I am short of words to when it comes to describing the impact Virtual Fund Assist has had on my business. Covid-19 caught us at an already difficult time, but the business loan we got thanks to Virtual Fund Assist helped us not only survive but thrive.</p>
            </div>

            <!-- Testinomils #03 -->
            <div class="testimonials">
                <i class="fa fa-comments"></i>
                <p class="body-large">Having applied for various small business loans, we turned to Virtual Fund Assistant. Our experience right from the start was exceptional. They believed in us, and helped us get start-up loans, empowering us in the process.</p>
            </div>
        </div>
    </div>

    <div class="bottom-cta full base dark pt-2">
        <div class="container cta-bottom py-5">
            <div class="row">
                <div class="col-12 py-md-5 text-center">
                    <h2>Get your small business loan today.</h2>
                    <form action="https://www.lendio.com/bp/basic-info" method="GET" class="cta-content d-inline-block my-4">
                        <div class="input-container col-lg-8 float-lg-left mb-3"> <span class="dollar d-none d-lg-block">$</span> <input type="text" placeholder="How much do you need?" class="amount-seeking-input text-center pl-0" name="amountSeeking" /></div>
                        <div class="button-container col-lg-4 float-lg-left"> <button class="primary-button w-lg-100 ml-0 mb-2">Start</button></div>
                        <div class="clear"></div>
                    </form>
                    <h5 class="px-5" style="font-size: 17px;">Applying is free and it won't impact your credit</h5>
                    <h5 style="font-size: 17px;">Already have an account? &nbsp;<a class="text-uppercase font-weight-bold" href="bp/login.html" data-wpel-link="internal"><strong>Sign in</strong></a></h5>
                </div>
            </div>
        </div>
    </div>
    <div class="contact-area border-top border-bottom py-5">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-7 d-flex text-center text-lg-left justify-content-center">
                    <img src="images/blue-line-phone.svg" class="mr-md-3 mr-4 mb-3 mb-lg-0" alt="Phone Icon">
                    <div class="hidden-sm-down pt-2"></div>
                    <h3 class="float-lg-left">Give us a call <br class="d-inheret d-sm-none" /><a class="phone" href="tel:4692250569" data-wpel-link="internal">(469) 225-0569</a></h3>
                </div>
                <div class="col-12 col-md-5 text-center text-lg-left">
                    <div class="hidden-sm-down pt-2"></div>
                    <p class="hours">Monday - Friday | 9am - 9pm Central Standard Time</p>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include('footer.php'); ?>
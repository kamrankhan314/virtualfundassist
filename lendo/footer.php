<?php $home_url = "https://virtualfundassist.com/lendio" ?>
<!-- ==== Page(Home) FOOTER Start ==== -->
<div id="site-footer-container">
    <div class="site-footer">
        <div class="site-footer-socials">
            <div class="site-social">
                <a href="https://www.instagram.com/lendio/" target="_blank" class="site-footer-social-icon" data-wpel-link="external" rel="external noopener noreferrer">
                    <img alt="Instagram" src="<?= $home_url ?>/images/Instagram.svg" />
                </a>
                <a href="https://facebook.com/Lendio" target="_blank" class="site-footer-social-icon" data-wpel-link="external" rel="external noopener noreferrer">
                    <img alt="Facebook" src="<?= $home_url ?>/images/facebook.svg" />
                </a>
                <a href="https://twitter.com/Lendio" target="_blank" class="site-footer-social-icon" data-wpel-link="external" rel="external noopener noreferrer">
                    <img alt="Twitter" src="<?= $home_url ?>/images/twitter.svg" />
                </a>
                <a href="https://www.linkedin.com/company/lendio" target="_blank" class="site-footer-social-icon" data-wpel-link="external" rel="external noopener noreferrer">
                    <img alt="LinkedIn" src="<?= $home_url ?>/images/linkedin.svg" />
                </a>
            </div>
        </div>
        <div class="site-footer-links-columns">
            <div class="site-footer-links-column">
                <h3>Resources</h3>
                <a href="<?= $home_url ?>/agreements/contact" class="site-footer-link" data-wpel-link="internal">Contact Us</a>
                <a href="<?= $home_url ?>/agreements/faq" class="site-footer-link" data-wpel-link="internal">FAQ</a>
                <a href="<?= $home_url ?>/agreements/agreement" class="site-footer-link" data-wpel-link="internal">Terms &amp;
                    Agreements</a>
            </div>
            <div class="site-footer-links-column">
                <h3>Company</h3>
                <a href="<?= $home_url ?>/company" class="site-footer-link" data-wpel-link="internal">About</a>
            </div>
            <div class="site-footer-links-column">
                <h3>Learn</h3>
                <a href="<?= $home_url ?>/business-loans" class="site-footer-link" data-wpel-link="internal">Loan Types</a>
                <a href="<?= $home_url ?>/business-calculators" class="site-footer-link" data-wpel-link="internal">Loan
                    Calculators</a>
                <a href="<?= $home_url ?>/small-business-loans" class="site-footer-link" data-wpel-link="internal">Small
                    Business Loans</a>
                <a href="<?= $home_url ?>/business-credit" class="site-footer-link" data-wpel-link="internal">Business
                    Credit</a>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <hr />
    <div class="site-footer-info">
        <div class="site-footer-copyright"> Copyright &copy; 2020 Virtual Fund Assist. All Rights Reserved. <a href="<?= $home_url ?>/agreements/terms-of-use" data-wpel-link="internal">Terms of Use</a> <a href="<?= $home_url ?>/agreements/privacy-policy" data-wpel-link="internal">Privacy
                Policy</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
        </div>
    </div>
</div>
</body>
    <!--==== JAVA SCRIPT ===-->
    <script type='text/javascript' src="<?= $home_url ?>/js/jquery.js"></script>
    <script type='text/javascript' src='<?= $home_url ?>/js/popper.js'></script>
    <script type='text/javascript' src='<?= $home_url ?>/js/bootstrap.js'></script>
    <script type='text/javascript' src='<?= $home_url ?>/js/bootstrap.bundled.js'></script>
    <script type='text/javascript' src="<?= $home_url ?>/js/waypoints.js"></script>
    <script type='text/javascript' src='<?= $home_url ?>/js/header.js'></script>
</html>
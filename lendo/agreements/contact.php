<?php include('../header.php'); ?>
<!-- ==== Page(contact-us) Main Start ==== -->
<div id="contact-page">
	<div class="page-body alt">
		<div class="page-body-content centered">
			<h1>Contact Us</h1>
			<div class="wpsso-pinterest-pin-it-image" style="display:none !important;"> <img src="../wp-content/uploads/2018/03/Wordpress-Lendio-Logo_blue.jpg" width="0" height="0" class="skip-lazy" style="width:0;height:0;" alt="" data-pin-description="We&#039;d love to hear from you and answer any questions you might have." />
			</div>
			<p>We&#8217;d love to hear from you and answer any questions you might have.<br />
			<div role="form" class="wpcf7" id="wpcf7-f61832-p61822-o1" lang="en-US" dir="ltr">
				<div class="screen-reader-response" role="alert" aria-live="polite"></div>
				<form action="https://www.lendio.com/contact/#wpcf7-f61832-p61822-o1" method="post" class="wpcf7-form init" novalidate="novalidate">
					<div style="display: none;"> <input type="hidden" name="_wpcf7" value="61832" /> <input type="hidden" name="_wpcf7_version" value="5.2" /> <input type="hidden" name="_wpcf7_locale" value="en_US" /> <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f61832-p61822-o1" /> <input type="hidden" name="_wpcf7_container_post" value="61822" /> <input type="hidden" name="_wpcf7_posted_data_hash" value="" /></div>
					<div class="form-group half-column-left"> <label>Your Full Name <span>*</span></label><br />
						<span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" aria-required="true" aria-invalid="false" /></span>
					</div>
					<div class="form-group half-column-right"><label>Your Email Address <span>*</span></label><br />
						<span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control" aria-required="true" aria-invalid="false" /></span>
					</div>
					<div class="form-group"> <label>What can we help you with? <span>*</span></label><br /> <span class="wpcf7-form-control-wrap help-category"><select name="help-category" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required form-control" id="dropdown-help" aria-required="true" aria-invalid="false">
								<option value="">---</option>
								<option value="I need a new loan">I need a new loan</option>
								<option value="Help me with my existing application">Help me with my existing
									application</option>
								<option value="Loan renewal support">Loan renewal support</option>
								<option value="General business inquiry">General business inquiry</option>
								<option value="Partner or affiliate opportunity">Partner or affiliate opportunity
								</option>
							</select></span>
						<div class="wpcf7-response-output" role="alert" aria-hidden="true"></div>
					</div>
					<div class="form-group"><label>Your Questions or Comments <span>*</span></label><br /> <span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="5" class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required form-control" aria-required="true" aria-invalid="false"></textarea></span></div>
					<p><span id="wpcf7-5f37155618b9d" class="wpcf7-form-control-wrap website-wrap" style="display:none !important; visibility:hidden !important;"><label class="hp-message">Please leave this field empty.</label><input class="wpcf7-form-control wpcf7-text" type="text" name="website" value="" size="40" tabindex="-1" autocomplete="nope" /></span></p>
					<div class="form-group centered"><input type="submit" value="Send Message" class="wpcf7-form-control wpcf7-submit btn btn-primary btn-action" /></div>
				</form>
			</div>
			</p>
		</div>
	</div>
	<div class="page-body">
		<div class="page-body-tri-content">
			<div class="page-body-column">
				<h2>Call</h2>
				<h2 class="phone">(469) 225-0569</h2>
			</div>
			<div class="page-body-column">
				<h2>Connect</h2>
				<div class="site-socials">
					<div class="site-social">
						<a href="https://www.instagram.com/lendio/" target="_blank" class="site-footer-social-icon" data-wpel-link="external" rel="external noopener noreferrer">
							<img alt="Instagram" src="<?= $home_url ?>/images/Instagram.svg" />
						</a>
						<a href="https://facebook.com/Lendio" target="_blank" class="site-footer-social-icon" data-wpel-link="external" rel="external noopener noreferrer">
							<img alt="Facebook" src="<?= $home_url ?>/images/facebook.svg" />
						</a>
						<a href="https://twitter.com/Lendio" target="_blank" class="site-footer-social-icon" data-wpel-link="external" rel="external noopener noreferrer">
							<img alt="Twitter" src="<?= $home_url ?>/images/twitter.svg" />
						</a>
						<a href="https://www.linkedin.com/company/lendio" target="_blank" class="site-footer-social-icon" data-wpel-link="external" rel="external noopener noreferrer">
							<img alt="LinkedIn" src="<?= $home_url ?>/images/linkedin.svg" />
						</a>
					</div>
				</div>
			</div>
			<div class="page-body-column">
				<h2>Visit</h2>
				<p>4103 N Story Rd, Irving,<br />TX 75060<br />(Located inside Protax Refunds)</p>
				<h3><a id="show-map" href="#">Get directions</a></h3>
			</div>
		</div>
	</div>
	<div id="map" style="display:none;"></div>
	<script type="text/javascript">
		$(document).on('click', '#show-map', function() {
			$('#map').slideDown();
			$('#show-map').hide();
			return false;
		});
	</script>
	<script>
		var map;

		function initMap() {
			map = new google.maps.Map(document.getElementById('map'), {
				zoom: 16,
				center: new google.maps.LatLng(40.565537, -111.902225),
				mapTypeId: 'styled_map',
				disableDefaultUI: true
			});

			var styledMapType = new google.maps.StyledMapType([{
					"elementType": "geometry",
					"stylers": [{
						"color": "#242f3e"
					}]
				},
				{
					"elementType": "labels.text.fill",
					"stylers": [{
						"color": "#746855"
					}]
				},
				{
					"elementType": "labels.text.stroke",
					"stylers": [{
						"color": "#242f3e"
					}]
				},
				{
					"featureType": "administrative.locality",
					"elementType": "labels.text.fill",
					"stylers": [{
						"color": "#d59563"
					}]
				},
				{
					"featureType": "poi",
					"stylers": [{
						visibility: "off"
					}]
				},
				{
					"featureType": "poi.park",
					"elementType": "geometry",
					"stylers": [{
						"color": "#263c3f"
					}]
				},
				{
					"featureType": "poi.park",
					"elementType": "labels.text.fill",
					"stylers": [{
						"color": "#6b9a76"
					}]
				},
				{
					"featureType": "road",
					"elementType": "geometry",
					"stylers": [{
						"color": "#38414e"
					}]
				},
				{
					"featureType": "road",
					"elementType": "geometry.stroke",
					"stylers": [{
						"color": "#212a37"
					}]
				},
				{
					"featureType": "road",
					"elementType": "labels.text.fill",
					"stylers": [{
						"color": "#9ca5b3"
					}]
				},
				{
					"featureType": "road.highway",
					"elementType": "geometry",
					"stylers": [{
						"color": "#746855"
					}]
				},
				{
					"featureType": "road.highway",
					"elementType": "geometry.stroke",
					"stylers": [{
						"color": "#1f2835"
					}]
				},
				{
					"featureType": "road.highway",
					"elementType": "labels.text.fill",
					"stylers": [{
						"color": "#f3d19c"
					}]
				},
				{
					"featureType": "transit",
					"elementType": "geometry",
					"stylers": [{
						"color": "#2f3948"
					}]
				},
				{
					"featureType": "transit.station",
					"elementType": "labels.text.fill",
					"stylers": [{
						"color": "#d59563"
					}]
				},
				{
					"featureType": "water",
					"elementType": "geometry",
					"stylers": [{
						"color": "#17263c"
					}]
				},
				{
					"featureType": "water",
					"elementType": "labels.text.fill",
					"stylers": [{
						"color": "#515c6d"
					}]
				},
				{
					"featureType": "water",
					"elementType": "labels.text.stroke",
					"stylers": [{
						"color": "#17263c"
					}]
				}
			]);

			map.mapTypes.set('styled_map', styledMapType);

			var marker = new google.maps.Marker({
				position: new google.maps.LatLng(40.565537, -111.902225),
				icon: '/wp-content/themes/lendio-standards/images/contact-pin.png',
				map: map
			});

			marker.addListener('click', function() {
				window.open('https://www.google.com/maps/search/?api=1&amp;query=Lendio&amp;query_place_id=482080041461073698')
			});
		}
	</script>
	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAYElHDVdyK9YF3xdxtpugChmraxdssWeA&amp;callback=initMap"></script>
</div>
<!-- ==== Page(contact-us) Main Start ==== -->
<div id="business-credit">
	<div class="hero pt-md-5" style="color:#fff;background: #0f0034;background: -moz-linear-gradient(-45deg, #0f0034 0%, #4000dc 100%);background: -webkit-linear-gradient(-45deg, #0f0034 0%,#4000dc 100%);background: linear-gradient(135deg, #0f0034 0%,#4000dc 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#0f0034', endColorstr='#4000dc',GradientType=1 );">
		<div class="container py-md-5">
			<div class="row py-md-2 hero-row">
				<div class="hero-responsive col-lg-6 text-left align-middle my-2">
					<img src="../images/business-credit-meter.svg" class="attachment-60x60 size-60x60 wp-post-image" alt="Business Credit" height="38" style="min-width:60px;display:block;" width="76" data-wp-pid="91765" nopin="nopin" title="Business Credit" />
					<h1>Business Credit</h1>
					<h5 class="my-3">Build your score, bring in the Benjamins.</h5>
				</div>
				<div class="col-lg-6 text-center align-middle my-2">
					<div class="cta p-5">
						<h3>Let's get started.</h3>
						<form action="https://www.lendio.com/bp/basic-info" method="GET" class="cta-content"> <input type="hidden" name="nosignup" value="1" />
							<div class="input-container"> <input type="text" placeholder="How much do you need?" name="amountSeeking" /></div> <button class="primary-button">See your
								options</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="alt-header-swap"></div>
	<div class="py-5 alt- border-top p-0 ">
		<div class="container py-0 py-lg-0">
			<div class="row ">
				<div class="col-12 ">
					<h2><span style="font-weight: 400;">What Is a Business Credit Score?</span></h2> <span style="font-weight: 400;">A business credit score measures the creditworthiness of a
						business. The scores range from 0–100. While the major business credit reporting
						agencies—Experian, Equifax, and Dun &amp; Bradstreet—differ in the specific criteria they
						use, they use several common criteria to calculate business credit scores. Unlike personal
						credit scores, your business credit score is available to the public, so potential lenders,
						vendors, customers, and even your nosy neighbor, Bruce, can see it. </span>
				</div>
			</div>
		</div>
	</div>
	<div class="alt-white py-5">
		<div class="container py-lg-0 py-5">
			<div class="row">
				<div class="col-12 col-md-6 py-5 text-center text-md-left">
					<h3><span style="font-weight: 400;">Business credit score criteria</span></h3>
					<p><span style="font-weight: 400;">Experian, Equifax, and Dun &amp; Bradstreet all use these
							common indicators in their reporting of business credit scores:</span></p>
					<ul>
						<li><span style="font-weight: 400;">Years in business</span></li>
						<li><span style="font-weight: 400;">Credit lines applied for in the last 9 months</span>
						</li>
						<li><span style="font-weight: 400;">Credit lines opened in the last 6 months</span></li>
						<li><span style="font-weight: 400;">Payment history for the previous 12 months</span></li>
						<li><span style="font-weight: 400;">Number of late payments</span></li>
					</ul>
					<h3><span style="font-weight: 400;">Benefits of strong business credit</span></h3>
					<p><span style="font-weight: 400;">A solid business credit score can help you:</span></p>
					<ul>
						<li><span style="font-weight: 400;">Qualify for small business loans, lines of credit, and
								credit cards</span></li>
						<li><span style="font-weight: 400;">Secure better rates and terms on financing</span></li>
						<li><span style="font-weight: 400;">Build a safety net that makes it easy to take out
								emergency loans or lines of credit if you fall on hard times</span></li>
						<li><span style="font-weight: 400;">Do business with new vendors and suppliers</span></li>
					</ul>
				</div>
				<div class="col-12 text-center my-auto offset-md-1 col-md-5 pb-5 py-md-4">
					<img src="../images/coffee-shop-cathy.svg" class="attachment-full size-full" alt="Coffee Shop Business Credit Stats" height="590" width="482" data-wp-pid="91767" nopin="nopin" title="Business Credit" />
				</div>
			</div>
		</div>
	</div>
	<div class="py-5 py-md-5 alt-grey">
		<div class="container">
			<div class="row">
				<div class="text-center text-lg-left my-auto col-md-6 d-none d-md-block"> <img width="710" height="604" src="../wp-content/uploads/2020/02/high-scores-rewards.png" class="attachment-full size-full" alt="High Scores Rewards" srcset="https://www.lendio.com/wp-content/uploads/2020/02/high-scores-rewards.png 710w, https://www.lendio.com/wp-content/uploads/2020/02/high-scores-rewards-300x255.png 300w" sizes="(max-width: 710px) 100vw, 710px" data-wp-pid="91768" nopin="nopin" title="Business Credit" /></div>
				<div class="col-12 mx-auto col-md-6 pl-0 pl-md-4 py-5">
					<h2>High scores bring big rewards</h2>
					<p>The #1 reason that traditional lenders reject most small business owners is bad credit.
						Building a strong business credit score brings you better <a href="../blog/small-business-tools/business-loan-interest-rates.html" data-wpel-link="internal">rates</a> and terms on small business loans and financing. And
						that’s only the beginning of the benefits.</p>
					<p>If your business is ever hit with hard times, a strong business credit score provides a
						safety net by making it easy to take out emergency loans or lines of credit. On top of that,
						vendors and suppliers are more likely to do business with you when your credit is solid.</p>
					<h3 class="statement-block">The #1 reason that traditional lenders reject most small business
						owners is bad credit.</h3>
				</div>
				<div class="col-12 text-center d-block d-md-none"> <img width="710" height="604" src="../wp-content/uploads/2020/02/high-scores-rewards.png" class="attachment-full size-full" alt="High Scores Rewards" srcset="https://www.lendio.com/wp-content/uploads/2020/02/high-scores-rewards.png 710w, https://www.lendio.com/wp-content/uploads/2020/02/high-scores-rewards-300x255.png 300w" sizes="(max-width: 710px) 100vw, 710px" data-wp-pid="91768" nopin="nopin" title="Business Credit" /></div>
			</div>
		</div>
	</div>
	<div class="py-5 alt-white p-0 ">
		<div class="container py-0 py-lg-0">
			<div class="row ">
				<div class="col-12 text-left ">
					<h2><span style="font-weight: 400;">How to Build Business Credit</span></h2>
					<p><span style="font-weight: 400;">You’re likely here because you’ve asked yourself, “How do I
							get good business credit?” We’ve helped more than 100,000 businesses get funded since
							2011. In doing so, we’ve learned a thing or 2 about how to establish, build, and repair
							business credit. No matter where you are in the process, here’s what you need to
							do. </span></p>
					<h3><span style="font-weight: 400;">Establish Business Credit</span></h3>
					<p><span style="font-weight: 400;">These are the first steps you need to take to establish
							business credit for your business. </span></p>
					<h4><span style="font-weight: 400;">Make your business a legal entity</span></h4>
					<p><span style="font-weight: 400;">Your business must be a corporation or limited liability
							company (LLC) to be assigned a business credit score. In addition to helping build
							business credit, establishing your business as a legally separate entity like an S-corp
							or LLC can protect your personal assets from business liabilities. </span><a href="http://www.gopjn.com/t/SENJSEpKR0pDR01ORktGQ0dMSU5KRg" data-wpel-link="external" target="_blank" rel="external noopener noreferrer"><span style="font-weight: 400;">LegalZoom can do this for you</span></a><span style="font-weight: 400;"> without the expense of a lawyer. </span></p>
					<h4><span style="font-weight: 400;">Open a business bank account</span></h4>
					<p><span style="font-weight: 400;">Once you’re legally separated from your business, you want to
							separate your business and personal finances. Opening a separate bank account for your
							business makes it easier to see important financial indicators for a business like
							income, financial assets, and expenses. </span></p>
					<p><span style="font-weight: 400;">After you’ve taken the 2 essential steps to establish
							business credit, you’re ready to start improving your business credit score. </span></p>
					<h3><span style="font-weight: 400;">Improve business credit score</span></h3>
					<p><span style="font-weight: 400;">The business credit score requirements vary from lender to
							lender, so as much as we wish we could give you a golden number, there isn’t one
							(sorry). Whatever your business credit score, you’ll benefit from improving it—whether
							that means helping you qualify for a new loan product or better rates and terms. </span>
					</p>
					<h4><span style="font-weight: 400;">Use a business bank account for expenses</span></h4>
					<p><span style="font-weight: 400;">Your business bank account should only be used for business
							expenses. This approach will help you to build business credit, streamline bookkeeping,
							and protect your personal financial assets from business liabilities. </span></p>
					<h4><span style="font-weight: 400;">Make timely payments</span></h4>
					<p><span style="font-weight: 400;">If you only take one piece of advice for building business
							credit, let it be this one. Making payments on time is the #1 way to build, improve, and
							repair your business credit score. It’s the cornerstone of credit data and a surefire
							indicator of creditworthiness. Here are 3 steps you can follow to ensure timely
							payments:</span></p>
					<ul>
						<li style="font-weight: 400;"><b>Sign up for auto-pay:</b><span style="font-weight: 400;">
								Most recurring bills offer the option to have the amount deducted automatically from
								a bank account of your choice. </span></li>
						<li style="font-weight: 400;"><b>Use bill-pay reminders: </b><span style="font-weight: 400;">You can enter your bills into your calendar, or you can
								use reminders from software like Microsoft Money or Quicken. </span></li>
						<li style="font-weight: 400;"><b>Schedule a bill-paying time: </b><span style="font-weight: 400;">Carve out a regular time slot on your schedule every month
								to formally sit down and take care of your bills. </span></li>
					</ul>
					<h4><span style="font-weight: 400;">Open a business credit card</span></h4>
					<p><span style="font-weight: 400;">Once you’ve mastered making payments on time, you’re ready to
							take your business credit into your own hands. </span><a href="../bp/business-credit-cards.html" data-wpel-link="internal"><span style="font-weight: 400;">Opening a business credit card</span></a><span style="font-weight: 400;"> will help you to build business credit in 2 ways. First, it
							will bolster your history of on-time payments. Second, it will improve your credit
							utilization ratio, i.e., how much credit is available to your business vs. how much
							you’re using. Bonus: business credit cards often offer rewards, so you can make your
							business expenditures work for you.</span></p>
					<h4><span style="font-weight: 400;">Increase available credit (taking out a loan, etc.)</span>
					</h4>
					<p><span style="font-weight: 400;">Remember what we said about the importance of on-time
							payments? When you establish a history of repaying a loan on time, that demonstrates
							creditworthiness. In turn, you’re more likely to qualify for or secure better rates in
							the future. With a variety of loan options, like equipment loans or business lines of
							credit, you can find a loan that can work double duty, meeting your business needs and
							helping you improve your business credit score at the same time. </span></p>
					<p style="text-align: center;"><a class="primary-button" href="../bp/basic-info.html" data-wpel-link="internal">See your options</a></p>
				</div>
			</div>
		</div>
	</div>
	<div class="py-5 alt-white border-top p-0 ">
		<div class="container py-0 py-lg-0">
			<div class="row ">
				<div class="col-12 text-left ">
					<h2><span style="font-weight: 400;">Repair business credit score</span></h2>
					<p><span style="font-weight: 400;">If you find yourself looking at a low business credit score,
							worry not. Any credit score can be improved. Follow these steps to repair your business
							credit:</span></p>
					<h3><span style="font-weight: 400;">Check your business credit score</span></h3>
					<p><span style="font-weight: 400;">You need to know what your business credit score is before
							you can repair it. Not sure how to go about it? We’ve outlined </span><a href="https://answers.lendio.com/home/how-check-business-credit" data-wpel-link="internal"><span style="font-weight: 400;">everything you need to know
								about checking your business credit score.</span></a><span style="font-weight: 400;"> </span></p>
					<h3><span style="font-weight: 400;">Look for mistakes</span></h3>
					<p><span style="font-weight: 400;">Credit reports are not infallible. Once you pull your credit
							report, you want to look for errors—like payments that haven’t gone through or were
							never reported. </span></p>
					<h3><span style="font-weight: 400;">Make corrections</span></h3>
					<p><span style="font-weight: 400;">After you find a mistake, you can contact the credit bureau
							with evidence of your payments or contact your vendor and request that they update their
							reports with the bureau. There may be a lot of paperwork, so if you’d rather outsource
							this task, our partner </span><a href="https://www.lexingtonlaw.com/l/credit-repair?tid=31844" data-wpel-link="external" target="_blank" rel="external noopener noreferrer"><span style="font-weight: 400;">Lexington Law</span></a><span style="font-weight: 400;">
							specializes in business credit repair. </span></p>
					<h3><span style="font-weight: 400;">Take care of outstanding debts</span></h3>
					<p><span style="font-weight: 400;">Paying down outstanding debts will help repair your credit.
							Focus on making payments early or on time. If you find yourself struggling to repay a
							loan or to pay off the balance of a credit card, pick up the phone and talk to your
							lender before defaulting on the debt. They may be able to help.</span></p>
					<h3><span style="font-weight: 400;">Make payments on time</span></h3>
					<p><span style="font-weight: 400;">We’ve said it before, and we’ll say it again: making payments
							on time is the single most important thing you can do for your business credit. </span>
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="py-5 alt-white border-top p-0 ">
		<div class="container py-0 py-lg-0">
			<div class="row ">
				<div class="col-12 text-center ">
					<h2><span style="font-weight: 400;">Can You Still Get a Loan with Bad Credit?</span></h2>
					<p><span style="font-weight: 400;">As you work to improve and repair your business credit, you
							may still be able to qualify for a small business loan. Here’s how:</span></p>
					<p>&nbsp;</p>
					<p align="center"><span class="fr-video fr-fvc fr-dvi fr-draggable" contenteditable="false"><iframe class="fr-draggable" style="max-width: 610px; width: 100%;" src="https://www.youtube.com/embed/qDm4Z7k8BcA" height="275" allowfullscreen="allowfullscreen"></iframe></span></p>
				</div>
			</div>
		</div>
	</div>
	<div class="bottom-cta full base dark pt-2">
		<div class="container cta-bottom py-5">
			<div class="row">
				<div class="col-12 py-md-5 text-center">
					<h2>Your business credit is waiting.</h2>
					<form action="https://www.lendio.com/bp/basic-info" method="GET" class="cta-content d-inline-block my-4">
						<div class="input-container col-lg-8 float-lg-left mb-3"> <span class="dollar d-none d-lg-block">$</span> <input type="text" placeholder="How much do you need?" class="amount-seeking-input text-center pl-0" name="amountSeeking" /></div>
						<div class="button-container col-lg-4 float-lg-left"> <button class="primary-button w-lg-100 ml-0 mb-2">Start</button></div>
						<div class="clear"></div>
					</form>
					<h5 class="px-5" style="font-size: 17px;">Applying is free and it won't impact your credit</h5>
					<h5 style="font-size: 17px;">Already have an account? &nbsp;<a class="text-uppercase font-weight-bold" href="../bp/login.html" data-wpel-link="internal"><strong>Sign in</strong></a></h5>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include('../footer.php'); ?>
<?php include('../header.php'); ?>

    <!-- ==== Page(faqs) Main Start ==== -->
    <div id="faqs">
        <div class="hero pt-md-5" style="color:inherit;background: ;">
            <div class="container py-md-5">
                <div class="row py-md-2 hero-row">
                    <div class="hero-responsive col-lg-6 text-left align-middle my-2">
                        <h1>Frequently Asked Questions</h1>
                        <h5 class="my-3">You have questions, we have answers.</h5>
                    </div>
                    <div class="col-lg-6 text-center align-middle my-2">
                        <div class="cta p-5">
                            <h3>Let's get started.</h3>
                            <form action="https://www.lendio.com/bp/basic-info" method="GET" class="cta-content"> <input
                                    type="hidden" name="nosignup" value="1" />
                                <div class="input-container"> <input type="text" placeholder="How much do you need?"
                                        name="amountSeeking" /></div> <button class="primary-button">See your
                                    options</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="alt-header-swap"></div>
        <div class="bottom-cta full base dark pt-2">
            <div class="container cta-bottom py-5">
                <div class="row">
                    <div class="col-12 py-md-5 text-center">
                        <h2>Your frequently asked questions is waiting.</h2>
                        <form action="https://www.lendio.com/bp/basic-info" method="GET"
                            class="cta-content d-inline-block my-4">
                            <div class="input-container col-lg-8 float-lg-left mb-3"> <span
                                    class="dollar d-none d-lg-block">$</span> <input type="text"
                                    placeholder="How much do you need?" class="amount-seeking-input text-center pl-0"
                                    name="amountSeeking" /></div>
                            <div class="button-container col-lg-4 float-lg-left"> <button
                                    class="primary-button w-lg-100 ml-0 mb-2">Start</button></div>
                            <div class="clear"></div>
                        </form>
                        <h5 class="px-5" style="font-size: 17px;">Applying is free and it won't impact your credit</h5>
                        <h5 style="font-size: 17px;">Already have an account? &nbsp;<a
                                class="text-uppercase font-weight-bold" href="../bp/login.html"
                                data-wpel-link="internal"><strong>Sign in</strong></a></h5>
                    </div>
                </div>
            </div>
        </div>

    </div>

<?php include('../footer.php'); ?>
<?php include('../header.php'); ?>

<!-- ==== Page(Credit-Gathering-Auth) Yellow_Top_Bar Start ==== -->
<div class="hero position-relative" style="background: linear-gradient(90deg, #ff3366 0%, #fe9b02 100%); color: white;">
    <div class="site-close"><i class="fa fa-times"></i></div>
    <div class="container p-lg-0">
        <div class="py-lg-2 row hero-row">
            <div class="hero-responsive col-12 text-left text-md-center align-middle mt-4 my-lg-2"> The Paycheck
                Protection Program ended on August 8, 2020. We will continue to accept applications in hopes that
                Congress passes an extension, although we are not currently sending applications to lenders. If PPP
                resumes, Lendio will submit your application to a lender.</div>
        </div>
    </div>
</div>

<!-- ==== Page(Credit-Gathering-Auth) Main Start ==== -->
<div class="container">
    <div id="alt-header-swap"></div>
    <div class="row">
        <article class="page-credit-gathering-auth col-12 pt-5">
            <h1>Credit Gathering Authorization</h1>
            <p>
                <span style="font-weight: 400;">
                By indicating your agreement to these terms (for example, by clicking the button) you
                represent that you are authorized on behalf of yourself and the business listed in your
                application (collectively, the “Applicant”) to make and submit representations regarding
                the Applicant. You authorize Virtual Fund Assist LLC., its representatives, subsidiaries,
                and affiliates (collectively, “Virtual Fund Assist LLC” or “we”) and its trusted partners (the <a href="https://virtualfundassist.com/"><span style="font-weight: 400;">https://virtualfundassist.com/</span>.</a>
                to conduct, obtain, receive, and process a review of me and/or my business as Virtual Fund Assist LLC or a <a href="https://virtualfundassist.com/"><span style="font-weight: 400;">https://virtualfundassist.com/</span>.</a> 
                considers appropriate to process my application for business-purpose financial products. 
                </span>

                <span style="font-weight: 400;">
                As part of this review, I authorize Virtual Fund Assist LLC and each of the<a href="https://virtualfundassist.com/"><span style="font-weight: 400;">https://virtualfundassist.com/</span>.</a>
                or to obtain consumer/personal and/or business and investigative financial and
                credit reports from one or more consumer reporting agencies (such as TransUnion,
                Experian and Equifax) and from other credit bureaus, banks, creditors and other third
                parties. I further authorize and understand that Virtual Fund Assist LLC and
                each<a href="https://virtualfundassist.com/"><span style="font-weight: 400;">https://virtualfundassist.com/</span>,</a> 
                respectively and individually, may obtain statements from
                creditors, financial institutions, and other third parties, as well as other information about
                me and my business, including credit card processor statements and bank statements.
                </span>

                <span style="font-weight: 400;">
                I authorize that Virtual Fund Assist LLC and/or each<a href="https://virtualfundassist.com/"><span style="font-weight: 400;">https://virtualfundassist.com/</span>.</a>
                to obtain my
                credit report, during review of any application submitted via www.virtualfundassist.com,
                including any subdomains thereof, during the closing of any financial product, or at
                various times during the term of my financial product(s) in connection with its servicing
                or enforcement. This authorization shall constitute my written instructions under the Fair
                Credit Reporting Act. I further authorize Virtual Fund Assist LLC and each <a href="https://virtualfundassist.com/"><span style="font-weight: 400;">https://virtualfundassist.com/</span>,</a> 
                or to contact third parties to verify any information provided in my credit report or
                otherwise submitted to Virtual Fund Assist LLC or its Participating Institutions in
                connection with my application for financial products.
                </span>

                <span style="font-weight: 400;">
                I confirm that the information I have provided to Virtual Fund Assist LLC as part of my
                application, as well as any subsequent information that might be submitted as part of
                my application, is true and correct and may be relied upon by Virtual Fund Assist LLC
                and the<a href="https://virtualfundassist.com/"><span style="font-weight: 400;">https://virtualfundassist.com/</span>.</a>
                in their consideration of my application. Any
                misrepresentation that I make in conjunction with the information I submit may be
                viewed as fraud, and as such may be punishable by law.
                </span>
            </p>
            <p>&nbsp;</p>
            <p>Partner list last revised on February 19, 2021. </p>
            <p><b>Virtual Fund Assist Preferred Lenders </b></p>
            
            <div class="row">
                <div class="col col-lg-3">
                    <ul>
                        <li>Accion East</li>
                        <li>Acronis</li>
                        <li>Agile Capital</li>
                        <li>Aktify</li>
                        <li>Allegro Credit</li>
                        <li>American Express</li>
                    </ul>
                </div>
                <div class="col col-lg-3">
                    <ul>
                        <li>AmOne</li>
                        <li>Apple Pie</li>
                        <li>ARF Financial</li>
                        <li>Assurance IQ</li>
                        <li>Authorize.Net </li>
                        <li>Bank of America</li>
                    </ul>
                </div>

                <div class="col col-lg-3">
                    <ul>
                        <li>Bankers Healthcare Group</li>
                        <li>Banks.com</li>
                        <li>ABench Accounting</li>
                        <li>Best Company</li>
                      <li>Better Impressions</li>
                        <li>BlueVine</li>
                    </ul>
                </div>
                <div class="col col-lg-3">
                    <ul>
                      <li>Booth Lenders</li>
                        <li>Breakout Capital</li>
                        <li>Camino Financial</li>
                        <li>Can Capital</li>
                        <li>Candela</li>
                        <li>Capital One</li>
                    </ul>
                </div>

                <div class="col col-lg-3">
                    <ul>
                        <li>CFG Merchant Solutions</li>
                        <li>Clearbanc</li>
                        <li>Clearlink</li>
                        <li>CoCard</li>
                      <li>Comcast</li>
                        <li>Cover Wallet</li>
                    </ul>
                </div>
                <div class="col col-lg-3">
                    <ul>
                      <li>Credibility Capital</li>
                        <li>Credible</li>
                        <li>Credibly</li>
                        <li>Daxko</li>
                        <li>Divvy</li>
                        <li>DreamSpring</li>
                    </ul>
                </div>

                <div class="col col-lg-3">
                    <ul>
                        <li>Eagle Business Credit</li>
                        <li>Elevate Funding</li>
                        <li>Enova</li>
                        <li>Expansion Capital Group</li>
                        <li>Fareharbor</li>
                        <li>Financial Pacific</li>
                    </ul>
                </div>
                <div class="col col-lg-3">
                    <ul>
                        <li>Finder</li>
                        <li>Fit Small Business</li>
                        <li>Fora Financial</li>
                        <li>Forward Financing</li>
                        <li>Fundation</li>
                        <li>Funding Circle</li>
                    </ul>
                </div>

                <div class="col col-lg-3">
                    <ul>
                        <li>FundKite</li>
                        <li>GetBackd</li>
                        <li>Gillman Bagley</li>
                        <li>Good Funding</li>
                        <li>Guidant Financial</li>
                        <li>Heartland</li>
                    </ul>
                </div>
                <div class="col col-lg-3">
                    <ul>
                        <li>Honeybook</li>
                        <li>Idea Financial</li>
                        <li>Indie.VC</li>
                        <li>IOU Financial</li>
                        <li>JAnover Ventures</li>
                        <li>Kalamata</li>
                    </ul>
                </div>

                <div class="col col-lg-3">
                    <ul>
                        <li>Kapitus</li>
                        <li>KickPay</li>
                        <li>Knight Capital Funding</li>
                        <li>Legal Zoom</li>
                        <li>LenCred</li>
                        <li>LendGenius</li>
                    </ul>
                </div>
                <div class="col col-lg-3">
                    <ul>
                        <li>Lending Network Marketplace</li>
                        <li>Lending Point</li>
                        <li>Lendini</li>
                        <li>Lendr</li>
                        <li>LendStart</li>
                        <li>LendVer</li>
                    </ul>
                </div>

                <div class="col col-lg-3">
                    <ul>
                        <li>Libertas</li>
                        <li>Lightspeed</li>
                        <li>Loan Close</li>
                        <li>LoanMe</li>
                        <li>Marketing360</li>
                        <li>Merchant Maverick</li>
                    </ul>
                </div>
                <div class="col col-lg-3">
                    <ul>
                        <li>MindBody</li>
                        <li>Mulligan</li>
                        <li>National Funding</li>
                        <li>Natural Intelligence</li>
                        <li>Network Solutions</li>
                        <li>North Avenue Capital</li>
                    </ul>
                </div>

                <div class="col col-lg-3">
                    <ul>
                        <li>OfficeSpace</li>
                        <li>OnDeck</li>
                        <li>OnPay</li>
                        <li>Opportunity Fund</li>
                        <li>Paychex</li>
                        <li>Provider Web Capital Funding</li>
                    </ul>
                </div>
                <div class="col col-lg-3">
                    <ul>
                        <li>Quarterspot</li>
                        <li>QuickBridge</li>
                        <li>Raistone Capital</li>
                        <li>Rapid Finance</li>
                        <li>Ready Capital</li>
                        <li>Reliant Funding</li>
                    </ul>
                </div>  

                <div class="col col-lg-3">
                    <ul>
                        <li>Restaurant 365</li>
                        <li>Salaryo</li>
                        <li>SBA.com</li>
                        <li></li>
                        <li></li>
                        <li></li>
                    </ul>
                </div>
                <div class="col col-lg-3">
                    <ul>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li> </li>
                        <li> </li>
                        <li>Reliant Funding</li>
                    </ul>
                </div>  
            </div>
        </article>
    </div>
</div>


<?php include('../footer.php'); ?>
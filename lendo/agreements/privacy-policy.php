<?php include('../header.php'); ?>

<!-- ==== Page(Privacy-Policy) Yellow_Top_Bar Start ==== -->
<div class="hero position-relative" style="background: linear-gradient(90deg, #ff3366 0%, #fe9b02 100%); color: white;">
    <div class="site-close"><i class="fa fa-times"></i></div>
    <div class="container p-lg-0">
        <div class="py-lg-2 row hero-row">
            <div class="hero-responsive col-12 text-left text-md-center align-middle mt-4 my-lg-2"> The Paycheck
                Protection Program ended on August 8, 2020. We will continue to accept applications in hopes that
                Congress passes an extension, although we are not currently sending applications to lenders. If PPP
                resumes, Lendio will submit your application to a lender.</div>
        </div>
    </div>
</div>

<!-- ==== Page(Privacy-Policy) Main Start ==== -->
<div class="container">
    <div id="alt-header-swap"></div>
    <div class="row">
        <article class="page col-12">
            <h1>Privacy Policy</h1>
            <p><b>This privacy policy shall be effective on October 1, 2020 </b></p>
            <p><i><span style="font-weight: 400;">Last Updated: October 21, 2020</span></i></p>

            <p>
                <span style="font-weight: 400;">
                    This Privacy Policy describes how Virtual Fund Assist address collects, uses, handles, shares the
                    personal information you provide to us while accessing and
                    using our Services that include content, features, pages of this website
                    <a href="https://virtualfundassist.com/">
                        <span style="font-weight: 400;">https://virtualfundassist.com/</span>.
                    </a>
                    You can only use this website according to this privacy policy and terms of use.
                </span>
            </p>

            <p>
                <span style="font-weight: 400;">You’re not allowed to use this website if you’re under 18 years of age.
                    We don’t collect and store information about a person under the age of 18 years.</span>
            </p>

            <p>
                <span style="font-weight: 400;">The policy enumerates and sets forth the rules about these
                    topics:</span>
            </p>


            <p>&nbsp;</p>
            <p><b>1. Accepting this Privacy Policy:</b></p>
            
            <p><span style="font-weight: 400;">By accessing and using the Site or Services, you consent to the
                    collection, use and disclosure of your personally identifiable Site Information, as applicable,
                    under this Privacy Policy. You can't use the Site or Services if you disagree with the Privacy
                    Policy.
                </span></p>

            <p>&nbsp;</p>
            <p><b>2. Types of Information we collect:</b></p>
        
            <p>
                <span style="font-weight: 400;">We collect these types of personal information.</span>
            </p>
            <p style="padding-left: 40px;"><strong style="font-weight: 800; color:#000;">A. Your Provided
                    Information:</strong></p>
            <p style="padding-left: 40px;">
                <span style="font-weight: 400;">For accessing our services, you need to establish an account on the Site
                    for which we will require some of your essential information such as you have to provide bank
                    statement of last three months.
                </span>
            </p>

            <p style="padding-left: 40px;"><strong style="font-weight: 800; color:#000;">B. Information that we
                    collect:</strong>
            </p>
            <p style="padding-left: 40px;">
                <span style="font-weight: 400;">
                    This information includes your personal information such as name, date of birth, nationality,
                    gender, photograph, email, home address etc., information to identify you such as passport, driver’s
                    license, national identity card, tax ID etc., information about your company, office, institution,
                    financial information such as income statement, bank account, tax return etc. and other business
                    information.
                </span>
            </p>

            <p style="padding-left: 40px;"><strong style="font-weight: 800; color:#000;">C. Automatically Collected
                    Information:</strong></p>
            <p style="padding-left: 40px;">
                <span style="font-weight: 400;">
                    For addressing customer support issues, improving performance and providing a customized user
                    experience, we collect certain information automatically. This information includes your IP address,
                    operating system, browser information, cookies, authentication data etc.
                </span>
            </p>

            <p style="padding-left: 40px;"><strong style="font-weight: 800;color:#000;">D. Information received from
                    third-parties:</strong></p>
            <p style="padding-left: 40px;">
                <span style="font-weight: 400;">
                    Such as information from public databases, ID verification partners, financial institutions, credit
                    bureaus etc.
                </span>
            </p>
            <p>&nbsp;</p>
            <p><b>3. Methods of Collecting Information:</b></p>

            <p>
                <span style="font-weight: 400;">We collect your information through log files, cookies, web beacons,
                    sign-up, application, email, telephone calls, and form third parties.</span>
            </p>
            <p>&nbsp;</p>
            <p><b>4. Ways of using information we collected:</b></p>

            <p>
                <span style="font-weight: 400;">Your information collected by us can be used for maintaining legal and
                    regulatory compliance, for enforcing terms of use we agreed upon, for preventing fraud, to provide
                    our Services, for Services-related communication, for customer support, quality control, ensuring
                    the security of information and Site, for research and development, for improving user experience,
                    for facilitating corporate acquisitions, mergers, transactions, for the purpose of marketing, and
                    for any other purpose to which you’ve given consent.</span>
            </p>
            <p>&nbsp;</p>
            <p><b>5. Sharing your information with third parties:</b></p>

            <p>
                <span style="font-weight: 400;">You agree that we may disclose your information if it’s required by law.
                    You agree that we may share your information with lenders or brokers if you requested a business
                    loan and to trusted third-party contractors who provided services to us, service providers, other
                    companies, and professionals.</span>
            </p>
            <p>&nbsp;</p>
            <p><b>6. Your rights about your information:</b></p>

            <p>
                <span style="font-weight: 400;">At any time you can access, update and store your information by signing
                    into your account on the site. You may take back consent. You can also request to remove your
                    information.</span>
            </p>
            <p>&nbsp;</p>
            <p><b>7. Safety and Security of Your Information:</b></p>

            <p>
                <span style="font-weight: 400;">You're not allowed to share your login and password with another person.
                    We ensure the security and safety of information, but in case of any breach, we will notify you.
                </span>
            </p>
            <p>&nbsp;</p>
            <p><b>8. Choice:</b></p>

            <p>
                <span style="font-weight: 400;">You have a choice to unsubscribe from receiving our marketing
                    emails.</span>
            </p>
            <p>&nbsp;</p>
            <p><b>9. Modification in this Privacy Policy:</b></p>

            <p>
                <span style="font-weight: 400;">Any modification in this Privacy Policy will be notified to you by
                    email.</span>
            </p>
            <p>&nbsp;</p>
            <p><b>10. Contact Us:</b></p>

            <p>
                <span style="font-weight: 400;">For more information about this Privacy Policy, you can contact us at <a
                        href="#">info@virtualfundassist.com</a></span>
            </p>
            <p>&nbsp;</p>
        </article>
    </div>
</div>


<?php include('../footer.php'); ?>
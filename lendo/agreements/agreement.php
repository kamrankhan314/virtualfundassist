<?php include('../header.php'); ?>

<!-- ==== Page (faqs) Agreement Start ==== -->
<div class="agree-head" style="background-color:#44d9e6; padding:20px">
    <div class="container">
        <div class="row">
            <article class="page col-12">
                <p class="text-center" style="color:#fff; text-transform: uppercase;">Acquire all the information about the agreements with Virtual Fund Assist. Go over the privacy
                    policy, terms and conditions, and customer service guarantee…
                </p>
            </article>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <article class="page col-12">
            <h1>Agreements</h1>
            <div class="wpsso-pinterest-pin-it-image" style="display:none !important;"> 
                <img src="../wp-content/uploads/2018/03/Wordpress-Lendio-Logo_blue.jpg" width="0" height="0" class="skip-lazy" style="width:0;height:0;" alt="" data-pin-description="If you want to know more about our policy for working with borrowers or the agreements Lendio makes with borrowers and lenders, you&rsquo;re in the right place." />
            </div>
            <p>Get all the information you need between our agreements with different borrowers and lenders. Browse
                through the privacy policy, terms and conditions, and customer service guarantee. In case you have any
                questions, feel free to get in touch with us.
                <a href="../cdn-cgi/l/email-protection.html#88ebfdfbfce7e5edfafbedfafee1ebedc8e4ede6ece1e7a6ebe7e5">contact us</a>.
            </p>
            <div class="agreements row">
                <div class="col-lg-4 col-md-6 col-12">
                    <h3>Privacy Policy</h3>
                    <p>Virtual Fund Assist respects and protects the privacy of our website visitors and clients…</p>
                    <a href="<?= $home_url ?>/agreements/privacy-policy" class="read-more" data-wpel-link="internal">READ AGREEMENT</a>
                </div>

                <div class="col-lg-4 col-md-6 col-12">
                    <h3>Terms & Conditions</h3>
                    <p>This is a legal agreement between you and Virtual Fund Assist, agreeing to which allows you to…</p>
                    <a href="<?= $home_url ?>/agreements/terms-of-use" class="read-more" data-wpel-link="internal">READ AGREEMENT</a>
                </div>

                <div class="col-lg-4 col-md-6 col-12">
                    <h3>Customer Service Guarantee</h3>
                    <p>We are committed to delighting you by helping you get the financing you need, when you need it…</p>
                    <a href="<?= $home_url ?>/agreements/contact" class="read-more" data-wpel-link="internal">READ AGREEMENT</a>
                </div>
            </div>
        </article>
    </div>
</div>

<?php include('../footer.php'); ?>
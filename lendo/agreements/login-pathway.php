<!-- ==== Main Header ==== -->
<?php include('../header.php'); ?>

<!-- ==== Page(Privacy-Policy) Yellow_Top_Bar Start ==== -->
<div class="hero position-relative" style="background: linear-gradient(90deg, #ff3366 0%, #fe9b02 100%); color: white;">
    <div class="site-close"><i class="fa fa-times"></i></div>
    <div class="container p-lg-0">
        <div class="py-lg-2 row hero-row">
            <div class="hero-responsive col-12 text-left text-md-center align-middle mt-4 my-lg-2"> The Paycheck
                Protection Program ended on August 8, 2020. We will continue to accept applications in hopes that
                Congress passes an extension, although we are not currently sending applications to lenders. If PPP
                resumes, Lendio will submit your application to a lender.</div>
        </div>
    </div>
</div>

<!-- ==== Page(Login-Pathway) Main Content Start ==== -->
<section class="d-flex flex-column container">
    <div class="row mx-0 justify-content-center pb-5">
        <h3 class="text-center mb-4 mt-5">Which application would you like to visit?</h3>
        <div class="w-100 mt-1">
            <div class="container d-flex justify-content-around mobile-wrapper">
                <div class="flat-card shadow border-0 d-flex flex-column py-5 px-4 justify-content-between col-xl-6 pathway-flat-card ppp-flat-card">
                    <h4 class="text-center">Paycheck Protection Program</h4>
                    <!---->
                    <div class="d-flex flex-row justify-content-center">
                        <img src="" class="align-self-center w-100">
                    </div>
                    <div class="pathway-card-footer">
                        <div class="text-center d-flex flex-row justify-content-center">
                            <p class="messaging-text">Finish your incomplete PPP Application <a href="#">More&nbsp;&gt;</a></p>
                        </div>
                        <div class="w-60 d-flex justify-content-center">
                            <button data-cy="" type="submit" class="btn btn-primary text-center">
                                <div>Visit PPP</div>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="flat-card shadow border-0 d-flex flex-column justify-content-between marketplace-flat-card py-5 px-4 col-xl-6 pathway-flat-card">
                    <h4 class="text-center">Standard Business Financing</h4>
                    <!---->
                    <div class="d-flex flex-row justify-content-center">
                        <img src="" class="align-self-center w-100">
                    </div>
                    <div class="pathway-card-footer">
                        <div class="text-center d-flex flex-row justify-content-center">
                            <p class="messaging-text">Start your standard financing application <a href="https://virtualfundassist.com/lendio/tabs/basic-info">More&nbsp;&gt;</a></p>
                        </div>
                        <div class="w-60 d-flex justify-content-center">
                            <button data-cy="https://virtualfundassist.com/lendio/tabs/basic-info" type="submit" class="btn btn-primary text-center">
                                <div>Visit Marketplace</div>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- ==== Main footer ==== -->
<?php include('../footer.php'); ?>
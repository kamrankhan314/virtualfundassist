<?php include('../header.php'); ?>

<!-- ==== Page(Terms-and-Conditions) Yellow_Top_Bar Start ==== -->
<div class="hero position-relative" style="background: linear-gradient(90deg, #ff3366 0%, #fe9b02 100%); color: white;">
    <div class="site-close"><i class="fa fa-times"></i></div>
    <div class="container p-lg-0">
        <div class="py-lg-2 row hero-row">
            <div class="hero-responsive col-12 text-left text-md-center align-middle mt-4 my-lg-2"> The Paycheck
                Protection Program ended on August 8, 2020. We will continue to accept applications in hopes that
                Congress passes an extension, although we are not currently sending applications to lenders. If PPP
                resumes, Lendio will submit your application to a lender.</div>
        </div>
    </div>
</div>

<!-- ==== Page(Terms-and-Conditions) Main Start ==== -->
<div class="container">
    <div id="alt-header-swap"></div>
    <div class="row">
        <article class="page col-12">
            <h1>Terms of Use</h1>
            <div class="wpsso-pinterest-pin-it-image" style="display:none !important;"> <img
                    src="../../wp-content/uploads/2018/03/Wordpress-Lendio-Logo_blue.jpg" width="0" height="0"
                    class="skip-lazy" style="width:0;height:0;" alt=""
                    data-pin-description="Terms of Use These Terms were last revised on January 17, 2020. This is a legal agreement between you and Lendio, Inc., a Delaware corporation" />
            </div>
            <p><span style="font-weight: 400;">These terms shall be effective on October 1, 2020. </span></p>
            <p><span style="font-weight: 400;">Consider this a legal agreement between you and Virtual Fund Assist, a
                    company, located at [ …address…]. By accessing and using this website and its services, you agree to
                    the terms and conditions laid down by this agreement and Privacy Policy. You
                    can’t use this site or services if you do not agree with these terms.
                </span>
            </p>
            <p><b>According to these terms, if any dispute arises between you and this company, it must be resolved by
                    arbitration which will be binding and that you're waiving your right to any class action suit and
                    the law of State of ____ will govern all the interaction between you and us.</b>
            </p>
            <p>&nbsp;</p>
            <p><b>Modification in the Terms:</b></p>
            <p><span style="font-weight: 400;">We have the sole discretion to change, add, modify or remove any of the
                    terms. After modification, your usage of the Site will be considered as acceptance of these
                    changes.</span></p>
            <p>&nbsp;</p>
            <p><b>Content of the Site:</b></p>
            <p><span style="font-weight: 400;">All of the content existing on the Site such as text, graphics, images,
                    logos, artwork, code, structure, design is owned by Virtual Fund Assist and has the protection of
                    copyright, trademark and all of the other intellectual property and cyber laws.</span></p>
                    <p>&nbsp;</p>
            <p><b>Terms:</b></p>
            <p><span style="font-weight: 400;">Virtual Fund Assist includes includes services, products, and software
                    provides by this company.</span></p>
                    <p>&nbsp;</p>
            <p><b>Usage:</b></p>
            <p><span style="font-weight: 400;">A. You can’t share your account with others; it’s only for your
                    use.</span></p>
            <p><span style="font-weight: 400;">B. Any liability about any content or information submitted by you lies
                    on yourself solely.</span></p>
            <p><span style="font-weight: 400;">C. You are solely responsible for risk assumption in any interaction on
                    this Site or with third-parties.</span></p>
            <p><span style="font-weight: 400;">D. We do not provide any guarantee about obtaining any loan or
                    business.</span></p>
            <p><span style="font-weight: 400;">E. You'll not give any false information.</span></p>
            <p><span style="font-weight: 400;">F. You agree to the term that you’ll not use this Site or Services for
                    any illegal purpose.</span></p>
            <p><span style="font-weight: 400;">G. You accept that you’ll use this Site only for a legal business
                    purpose.</span></p>
                    <p>&nbsp;</p>
            <p><b>Ownership Rights:</b></p>
            <p><span style="font-weight: 400;">A. You agree that We are the sole owner of this website, Services and all
                    rights therein.</span></p>
            <p><span style="font-weight: 400;">B. You agree that you’ll not submit, post, copy or disclose any
                    confidential information.</span></p>
            <p><span style="font-weight: 400;">C. By submitting information and content to any public area of this Site,
                    you agree that we can use, reproduce, publicly display, and sub-license such information and
                    content.</span></p>
                    <p>&nbsp;</p>
            <p><b>User Information:</b></p>
            <p><span style="font-weight: 400;">A. Your usage of this Site or Services amount to acceptance of terms of
                    Private Policy.</span></p>
            <p><span style="font-weight: 400;">B. We can disclose your information if required by law, or by third-party
                    or on our sole discretion.</span></p>
            <p><span style="font-weight: 400;">C. We try hard to secure information provided by you.</span></p>
            <p>&nbsp;</p>
            <p><b>Loss because of Advertiser or Sponsor:</b></p>
            <p><span style="font-weight: 400;">You agree that any loss or damage occurred because of any Advertiser or
                    Sponsor, working with us as third-party, does not make us responsible for that.</span></p>
                    <p>&nbsp;</p>
            <p><b>No Warranty:</b></p>
            <p><span style="font-weight: 400;">We provide no warranty for our services whatsoever.</br>
                    We are not responsible for any opinion, statements or content submitted on the Site by
                    third-party.</span>
            </p>
            <p>&nbsp;</p>
            <p><b>No Liability:</b></p>
            <p><span style="font-weight: 400;">We are not liable for any damage incurred by you because of the use or
                    inability to use our services.</span></p>
                    <p>&nbsp;</p>
            <p><b>Indemnification:</b></p>
            <p><span style="font-weight: 400;">You agree that against any loss, damage, liabilities or expenses, you
                    will defend Virtual Fund Assist and its directors, officers, employers, agents and
                    third-parties.</span></p>
                    <p>&nbsp;</p>
            <p><b>Effectiveness of this Agreement and Termination:</b></p>
            <p><span style="font-weight: 400;">Your usage of the Site or Services considered as acceptance of terms
                    which makes this agreement effective.</span></p>
            <p><span style="font-weight: 400;">You and we can also terminate your account without any explanation. We
                    can block your access to services without giving notice.</span></p>
                    <p>&nbsp;</p>
            <p><b>Law and Jurisdiction:</b></p>
            <p><span style="font-weight: 400;">These terms, Site and Services are subject to the laws and jurisdiction
                    of State of ____.</span></p>
                    <p>&nbsp;</p>
        </article>
    </div>
</div>


<?php include('../footer.php'); ?>
(function($) {
    $(document)
        .on('ready', function () {
            var firstLoanAmountInput = $('#first-loan-amount');
            var amountSeeking = $('.amount-seeking-input');
            var avgMonthlySalesInput = $('#avg-monthly-sales');

            if(firstLoanAmountInput.length) {
                firstLoanAmountInput
                    .mask('000,000,000,000,000', { reverse: true });
            }

            if(amountSeeking.length) {
                amountSeeking
                    .mask('000,000,000,000,000', { reverse: true });
            }

            if (avgMonthlySalesInput.length) {
                avgMonthlySalesInput
                    .mask('000,000,000,000,000', { reverse: true });
            }
        })
        .on('keyup', '#avg-monthly-sales', function () {
            if ($(this).val()) {
                $(this).removeClass('error');
            }
        })
        .on('change', '.home-select', function () {
            if ($(this).val()) {
                $(this)
                    .removeClass('no-value')
                    .removeClass('error');
            } else {
                $(this).addClass('no-value');
            }
        })
        .on('submit', '#main-form', function (e) {
            var vals = $(this).serializeArray();
            var inputIds = {
                averageMonthlySales: 'avg-monthly-sales',
                timeInBusiness: 'time-in-business',
                creditScore: 'credit-score'
            };

            var passValidation = false;
            var errors = [];
            var i = 0;
            for (i; i < vals.length; i++) {
                var val = vals[i].value.trim();
                if (val === '' || val === null) {
                    var id = inputIds[vals[i].name];
                    $('#' + id).addClass('error');
                    errors.push(id);
                }
            }

            passValidation = !errors.length ? true : false;

            return passValidation;
        });
})(jQuery);

<?php include('../header.php'); ?>
<!-- ==== Page(About) Banner + Main Start ==== -->
<div id="company-container">
    <div class="hero py-md-5" style="color:#fff;background: #45494d url(./images/company-header1.jpg) center center no-repeat;">
        <div class="container py-md-5">
            <div class="row py-md-5">
                <div class="col-12 text-center align-middle py-md-5 my-5">
                    <h1>The ultimate platform providing businesses access to hundreds of lenders, Virtual Fund Assist is
                        disrupting the financial industry. Get the loans you need, when you need them!</h1>
                </div>
            </div>
        </div>
    </div>
    <div id="alt-header-swap"></div>
    <div class="stats py-0 py-lg-5">
        <div class="container py-5">
            <h2 class="text-center">Breathing Life Into Businesses.</h2>
            <div class="row">
                <div class="col-12 col-md-4 text-center py-5">
                    <h2 class="stat-figure">$2+ billion</h2>
                    <h2>Disbursed</h2>
                </div>
                <div class="col-12 col-md-4 text-center py-5">
                    <h2 class="stat-figure">300+</h2>
                    <h2>Lenders</h2>
                </div>
                <div class="col-12 col-md-4 text-center py-5">
                    <h2 class="stat-figure">100,000+ </h2>
                    <h2>loans Issued</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="alt-white py-5">
        <div class="container py-lg-0 py-5">
            <div class="row">
                <div class="col-12 col-md-6 py-5 text-center text-md-left">
                    <h2>Cash Flow Is King</h2>
                    <p>The primary reason why most small businesses fail is lack of cash flow. Considered to be highly
                        risky, small businesses are seldom granted loans by financial institutions. </p>
                    <p>It all changes today! </p>
                    <p>Virtual Fund Assist is transforming the reality for small businesses by empowering them to get
                        the loans they need in a seamless manner so that they can bring their visions to life.
                    </p>
                </div>
                <div class="col-12 text-center my-auto offset-md-1 col-md-5 pb-5 py-md-4"> <img src="images/money-money-money.svg" class="attachment-full size-full" alt="Change the world" height="436" width="464" data-wp-pid="91702" nopin="nopin" title="Revolutionizing small business &lt;br /&gt;lending, one loan at a time." /></div>
            </div>
        </div>
    </div>
    <div class="py-5 py-md-5 alt-white border-top">
        <div class="container">
            <div class="row">
                <div class="text-center text-lg-left my-auto col-md-6 d-none d-md-block">
                    <img width="473" height="524" src="images/we-fund-people.svg" class="attachment-full size-full" alt="cash flow" sizes="(max-width: 473px) 100vw, 473px" data-wp-pid="91703" title="small business" />
                </div>
                <div class="col-12 mx-auto col-md-6 pl-0 pl-md-4 py-5">
                    <h2>We Have Empowered More Than</h2>
                    <div class="number-of-loans">100,000</div>
                    <h2 class="pb-3">Small Businesses</h2>
                    <p>Small businesses are the pillars on which the American economy stands on and we continue to help
                        them in every way we can. From giving them the boost they need in a slump to helping them expand
                        their operations, we do it all. </p>
                    <p>And guess what, we have just gotten started!</p></br>
                    <p>Staying true to our commitment to serve Americans, we continue to set the bar higher. </p>
                </div>

            </div>
        </div>
    </div>
    <div class="py-5 alt-dark p-0 ">
        <div class="container py-0 py-lg-0">
            <div class="row ">
                <div class="col-12 text-center ">
                    <h2 class="py-2 py-md-5">The next goal&#8230;</h2>
                    <h2 class="huge">1<br /> Billion<br /> Loans Issued</h2>
                    <h3>Now, you might ask why 1 billion loans? Well, that’s 1000,000,000 times we extend a helping hand
                        to small businesses in need.</h3>
                    <p class="py-5">Think about the jobs created, the life’s transformed, and the overall economic
                        growth. </p>
                </div>
            </div>
        </div>
    </div>
    <div class="alt-grey py-lg-2">
        <div class="container py-0 py-md-5">
            <div class="row">
                <div class="col-12 py-5 text-center">
                    <h2>Crafting Change For Good Is Challenging; <br class="d-block d-sm-none" />Just Like Everything
                        Worthwhile.</h2>
                    <h5 class="sub-header">Following are our esteemed partners:</h5>
                    <div class="container lender-logos">
                        <div class="row d-flex justify-content-center">
                            <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                <img class="lender-block-img" src="images/paypal-logo.svg" alt="paypal-logo" />
                            </div>
                            <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                <img class="lender-block-img" src="images/bank-of-america-logo.svg" alt="bank-of-america-logo" />
                            </div>
                            <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                <img class="lender-block-img" src="images/amex-logo.png" alt="amex-logo" />
                            </div>
                            <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                <img class="lender-block-img" src="images/chase-logo.svg" alt="chase-logo" />
                            </div>
                            <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                <img class="lender-block-img" src="images/kabbage-logo.svg" alt="kabbage-logo" />
                            </div>
                            <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                <img class="lender-block-img" src="images/fundation-logo.svg" alt="fundation-logo" />
                            </div>
                            <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                <img class="lender-block-img" src="images/ondeck-logo.svg" alt="ondeck-logo" />
                            </div>
                            <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                <img class="lender-block-img" src="images/authorize-net-logo.svg" alt="authorize-net-logo" />
                            </div>
                            <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                <img class="lender-block-img" src="images/headway-capital-logo.svg" alt="headway-capital-logo" />
                            </div>
                            <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                <img class="lender-block-img" src="images/funding-circle-logo.svg" alt="funding-circle-logo" />
                            </div>
                            <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                <img class="lender-block-img" src="images/lending-club-logo.svg" alt="lending-club-logo" />
                            </div>
                            <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                <img class="lender-block-img" src="images/smart-biz-logo.svg" alt="SmartBiz" />
                            </div>
                        </div>
                    </div>
                    <a href="../bp/lenders.html" class="primary-button mt-5" data-wpel-link="internal">Explore lenders</a>
                </div>
            </div>
        </div>
    </div>
    <div class="alt-white py-5">
        <div class="container py-lg-0 py-5">
            <div class="row">
                <div class="col-12 col-md-6 py-5 text-center text-md-left">
                    <h2 class="py-3">Making A Lasting Global Impact</h2>
                    <p class="pb-3">For every loan we facilitate through our platform, we give a percentage to
                        low-income entrepreneurs, making it clear to them that they are not alone. Virtual Fund Assist
                        stands by them in times good and bad. </p>
                    <p><a class="primary-button" href="../lendio-gives.html" data-wpel-link="internal">Learn
                            more</a></p>
                </div>
                <div class="col-12 text-center my-auto offset-md-1 col-md-5 pb-5 py-md-4"> <img src="images/location-open-business.svg" class="attachment-full size-full" alt="Beginning of small business loans" height="484" width="486" data-wp-pid="91704" nopin="nopin" title="Revolutionizing small business &lt;br /&gt;lending, one loan at a time." />
                </div>
            </div>
        </div>
    </div>
    <div class="banner-cta alt-white border-top">
        <div class="container py-5">
            <div class="row">
                <div class="col-12 pb-5 mb-5"></div>
                <div class="col-12 col-md-6"> <a class="promo-link" href="../partners/index.html" data-wpel-link="internal">
                        <div class="promo-card shadow p-5 bg-white mb-5">
                            <h3 class="h1">Become a Virtual Fund Assist

                                Partner</h3>
                            <h5 class="pt-3">Learn more<i style="font-size: 2rem;" class="fa fa-angle-right float-right"></i></h5>
                        </div>
                    </a></div>
                <div class="col-12 col-md-6"> <a class="promo-link" href="../careers/index.html" data-wpel-link="internal">
                        <div class="promo-card shadow p-5 bg-white mb-5">
                            <h3 class="h1">Apply to work at Virtual Fund Assist

                            </h3>
                            <h5 class="pt-3">Learn more<i style="font-size: 2rem;" class="fa fa-angle-right float-right"></i></h5>
                        </div>
                    </a></div>
            </div>
        </div>
    </div>
    <div class="bottom-cta full base dark pt-2">
        <div class="container cta-bottom py-5">
            <div class="row">
                <div class="col-12 py-md-5 text-center">
                    <h2>Live the American dream. </h2>
                    <h3>Get the small business loans you need, when you need it.</h3>
                    <form action="https://www.lendio.com/bp/basic-info" method="GET" class="cta-content d-inline-block my-4"> <input type="hidden" name="medium" value="Email" /><input type="hidden" name="source" value="all" /><input type="hidden" name="campaign" value="PPPLeads" /><input type="hidden" name="adgroup" value="PLPReceivedAutomatedEmail" /><input type="hidden" name="referral_url" value="unknown" />
                        <div class="input-container col-lg-8 float-lg-left mb-3"> <span class="dollar d-none d-lg-block">$</span> <input type="text" placeholder="How much do you need?" class="amount-seeking-input text-center pl-0" name="amountSeeking" /></div>
                        <div class="button-container col-lg-4 float-lg-left"> <button class="primary-button w-lg-100 ml-0 mb-2">Start</button></div>
                        <div class="clear"></div>
                    </form>
                    <h5 class="px-5" style="font-size: 17px;">Applying is free and it won't impact your credit</h5>
                    <h5 style="font-size: 17px;">Already have an account? &nbsp;<a class="text-uppercase font-weight-bold" href="../bp/login.html" data-wpel-link="internal"><strong>Sign in</strong></a></h5>
                </div>
            </div>
        </div>
    </div>
    <div class="contact-area border-top border-bottom py-5">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-7 d-flex text-center text-lg-left justify-content-center"> <img src="images/blue-line-phone.svg" class="mr-md-3 mr-4 mb-3 mb-lg-0" alt="Phone Icon">
                    <div class="hidden-sm-down pt-2"></div>
                    <h3 class="float-lg-left">Give us a call <br class="d-inheret d-sm-none" /><a class="phone" href="tel:8558536346" data-wpel-link="internal">(469) 225-0569 </a></h3>
                </div>
                <div class="col-12 col-md-5 text-center text-lg-left">
                    <div class="hidden-sm-down pt-2"></div>
                    <p class="hours">Monday - Friday | 9am - 9pm Central Standard Time</p>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include('../footer.php'); ?>
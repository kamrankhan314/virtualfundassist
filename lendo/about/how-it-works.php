<?php include('../header.php'); ?>

    <!-- ==== Page(How-It-works) Main Start ==== -->
    <div id="how-it-works-video">
        <div class="hero pt-md-2">
            <div class="container py-md-5">
                <div class="row py-md-5 hero-row">
                    <div class="hero-responsive col-lg-6 text-left align-middle my-5">
                        <h1>Get The Small Business Financing You Need, <br />When You Need It.</h1>
                        <h5 class="sub-header my-3">Smartly apply for small business loans in just 3 easy steps.</h5>
                        <div class="btns d-flex">
                            <!-- <a class="lightbox primary-button btn-outline-primary mt-4" href="https://www.youtube.com/watch?v=eik3bsBny9o" data-wpel-link="external" target="_blank" rel="external noopener noreferrer">Watch Video
                                <i class="how-it-works-vid fa fa-play"></i>
                            </a>  -->
                            <a class="primary-button mt-4" href="bp/basic-info.html" data-wpel-link="internal">Get started</a>
                        </div>
                    </div>
                    <div class="col-lg-6 text-right align-middle my-5">
                        <img src="images/how-it-works-lendio-about.svg" class="attachment- size- wp-post-image"
                            alt="How it works" height="517.39" width="851.81" data-wp-pid="91570" nopin="nopin" />
                    </div>
                </div>
            </div>
        </div>
        <div id="alt-header-swap"></div>
        <div class="py-5 py-md-5 alt-grey">
            <div class="container">
                <div class="row sub-hero-responsive">
                    <div class="text-center text-lg-left my-auto col-md-6 d-none d-md-block">
                        <img src="images/application-its-work.svg" class="attachment-full size-full"
                            alt="Apply in 15 minutes" height="779.56" width="856.44" data-wp-pid="91561"
                            nopin="nopin" />
                    </div>
                    <div class="col-12 mx-auto col-md-6 pl-0 pl-md-4 py-5">
                        <h2 class="header-numbers">01</h2>
                        <h2>Apply Online In As Few As 15 Minutes.</h2>
                        <p class="body-large">All you need are 15 minutes to fill out our online small business loan application. Yes, that’s right!</p>
                        <p class="body-large">Moreover, there is no fee or obligation for you. To top it off, the funding part is just as swift.</p>
                        <p class="body-large">You will have the funds in your pocket (in your account!) within a day of your loan approval.</p>
                        <p class="body-large">We may ask for a few additional documents that you’ll have to simply click and upload to expedite the process.</p>
                        <p class="body-large">At Virtual Fund Assist, we highly value your time. That’s why we prioritize getting you the capital you need in as little as 24 hours.</p>
                    </div>
                    <div class="col-12 text-center d-block d-md-none"> 
                        <img src="wp-content/uploads/2020/01/application.svg" class="attachment-full size-full" alt="Apply in 15 minutes" height="779.56" width="856.44" data-wp-pid="91561" nopin="nopin"
                        title="Not your typical small&lt;br /&gt;business loan application." />
                    </div>
                </div>
            </div>
        </div>
        <div class="alt-white py-lg-2">
            <div class="container py-0 py-md-5">
                <div class="row">
                    <div class="col-12 py-5 text-center">
                        <h2 class="header-numbers">02</h2>
                        <h2>Choose From The Best Lenders In The Country!</h2>
                        <div class="row">
                            <div class="col-lg-8 offset-lg-2">
                                <p class="body-large text-center">With a network of over 100 lenders across the country. </p>
                                <p class="body-large text-center">We browse the best loan options available from an array of top lenders out there — simply fill out our online application to get started.</p>
                                <p class="body-large text-center">It isn’t just about wanting a small business loan, is it?</p>
                                <p class="body-large text-center">It’s about getting the best small business loan in a timely manner with the best terms from a leading lender, right?</p>
                            </div>
                        </div>
                        <div class="container lender-logos">
                            <div class="row d-flex justify-content-center">
                                <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                    <img class="lender-block-img" src="images/paypal-logo.svg" alt="paypal-logo" />
                                </div>
                                <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                    <img class="lender-block-img" src="images/bofa_lo2_rgb_Digital.png"
                                        alt="bofa_lo2_rgb_Digital" />
                                </div>
                                <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                    <img class="lender-block-img" src="images/american-express-logo.svg"
                                        alt="american-express-logo" />
                                </div>
                                <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                    <img class="lender-block-img" src="images/Business_Backer_Stacked.png"
                                        alt="Business_Backer_Stacked" />
                                </div>
                                <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                    <img class="lender-block-img" src="images/kabbage-logo.svg" alt="kabbage-logo" />
                                </div>
                                <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                    <img class="lender-block-img" src="images/ondeck-logo.svg" alt="ondeck-logo" />
                                </div>
                                <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                    <img class="lender-block-img" src="images/authorize-net-logo.svg"
                                        alt="authorize-net-logo" />
                                </div>
                                <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                    <img class="lender-block-img" src="images/headway-capital-logo.svg"
                                        alt="headway-capital-logo" />
                                </div>
                                <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                    <img class="lender-block-img" src="images/fundation-logo.svg"
                                        alt="headway-capital-logo" />
                                </div>
                                <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                    <img class="lender-block-img" src="images/lending-club-logo.svg"
                                        alt="headway-capital-logo" />
                                </div>
                                <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                    <img class="lender-block-img" src="images/mulligna_funding.png"
                                        alt="headway-capital-logo" />
                                </div>
                            </div>
                        </div> 
                        <!-- <a href="bp/lenders.html" class="primary-button mt-5" data-wpel-link="internal">Compare lenders</a> -->
                    </div>
                </div>
            </div>
        </div>
        <div class="alt-grey py-5">
            <div class="container py-lg-0 py-5">
                <div class="row sub-hero-responsive">
                    <div class="col-12 col-md-6 py-5 text-center text-md-left">
                        <h2 class="header-numbers">03</h2>
                        <h2>Gain Fast Access To Funding.</h2>
                        <p class="body-large">Cash flow is king.</p>
                        <p class="body-large">We know how important the loan is to keep your business running smoothly. That’s why we only partner up with lenders that offer the fastest funding times. </p>
                        <p class="body-large">After approval, we will deliver your capital in as early as 24 hours – that is a guarantee!</p>
                        <p class="body-large">Get funded quickly!</p>
                    </div>
                    <div class="col-12 text-center my-auto offset-md-1 col-md-5 pb-5 py-md-4">
                        <img src="images/money-falling.svg" class="attachment-full size-full" alt="You&#039;re Funded"
                            height="543.28" width="634.52" data-wp-pid="91558" nopin="nopin" />
                    </div>
                </div>
            </div>
        </div>
        <div class="alt-dark py-5">
            <div class="container py-0 py-md-5">
                <div class="row">
                    <!-- <div class="col-md-6 col-lg-5 py-md-4 my-auto text-center d-none d-md-block"> 
                        <img width="453" height="455" src="wp-content/uploads/2020/01/funding-manager-photo.png" class="attachment-full size-full" alt="Help through the loan process"
                        srcset="https://www.lendio.com/wp-content/uploads/2020/01/funding-manager-photo.png 453w, https://www.lendio.com/wp-content/uploads/2020/01/funding-manager-photo-300x300.png 300w, https://www.lendio.com/wp-content/uploads/2020/01/funding-manager-photo-80x80.png 80w"
                        sizes="(max-width: 453px) 100vw, 453px" data-wp-pid="91559" nopin="nopin"
                        title="Not your typical small&lt;br /&gt;business loan application." />
                    </div> -->
                    <div class="col-12 text-center">
                        <h2>Virtual Fund Assists Stands With You Every Step Of The Way.</h2>
                        <p class="body-large text-center">Right from looking for the best loan options and applying to getting approved and gaining access to funds, we will be your side, so you don’t have to worry about a thing.</p>
                        <p class="body-large text-center">A dedicated personal funding manager will be at your service when you apply with Virtual Fund Assist. By carefully listening to your needs, the manager will ensure you opt for the perfect loan option for your small business.</p>
                        <p class="body-large text-center">Jump ahead of all the technical financial headaches. Wave goodbye to banks and brokers!</p>
                        <p class="body-large text-center">Let us empower you to secure financing instantly and elevate your business.</p>    

                        <h3 class="mt-5">Call us anytime
                            <span class="d-none d-md-inline"> </span>
                            <br class="d-inline d-sm-none" />
                            <a class="phone text-nowrap" href="tel:4692250569" data-wpel-link="internal">(469) 225-0569</a>
                        </h3>
                        <div class="hours">Monday &#8211; Friday | 9am &#8211; 9pm Central Standard Time</div>
                    </div>
                    <!-- <div class="col-12 text-center d-block d-md-none pb-5"> 
                        <img width="453" height="455" src="wp-content/uploads/2020/01/funding-manager-photo.png" class="attachment-full size-full" alt="Help through the loan process" srcset="https://www.lendio.com/wp-content/uploads/2020/01/funding-manager-photo.png 453w, https://www.lendio.com/wp-content/uploads/2020/01/funding-manager-photo-300x300.png 300w, https://www.lendio.com/wp-content/uploads/2020/01/funding-manager-photo-80x80.png 80w"
                        sizes="(max-width: 453px) 100vw, 453px" data-wp-pid="91559" nopin="nopin" title="Not your typical small&lt;br /&gt;business loan application." />
                    </div> -->
                </div>
            </div>
        </div>
        <div class="alt-white py-5">
            <div class="container py-lg-0 py-5">
                <div class="row">
                    <div class="col-12 col-md-6 py-5 text-center text-md-left">
                        <h2 class="mb-4">We Are Here For You, Whenever You Need Us.</h2>
                        <p class="body-large text-center">From the day you open the doors of your company to when you retire, we will be here, offering holistic support. Whether it’s securing SBA loan funding for your startup, acquiring commercial mortgages, or even applying for a credit card — we have your back under all circumstances.</p> 
                        <p class="body-large text-center">Our support goes beyond your financing needs. If you want any sort of bookkeeping, credit repair, or legal assistance, we will connect you with the ‘right’ service providers.</p> 
                        <p class="body-large text-center">When we promise to be here for you, we mean it!</p> 
                    </div>
                    <div class="col-12 text-center my-auto offset-md-1 col-md-5 pb-5 py-md-4">
                        <img src="images/growth.svg" class="attachment-full size-full" alt="Grow your business" height="454.92" width="565.17" data-wp-pid="91560" nopin="nopin" />
                    </div>
                </div>
            </div>
        </div>
        <div class="py-5 alt-grey p-0 ">
            <div class="container py-0 py-lg-0">
                <div class="row ">
                    <div class="col-12 ">
                        <p>
                            <img style="width: 29px;" src="images/quotes.svg" alt="Quote" />
                        </p>
                        <p class="body-large mb-4">
                        My opening day was near, and I was really concerned about not being able to meet our merchandise requirements. Like a Godsend, Virtual Fund Assist helped me fill out the application and secure a small loan – all within 24 hours! With the way Virtual Fund Assist breezed me through the process, my business has steadily been able to get up on its feet. Could not be more thankful to them.
                        </p>
                        <div class="container">
                            <div class="row">
                                <div class="col-12 col-lg-12 pl-0 mb-5">
                                    <p class="m-0">Jose Quinones</p>
                                    <p class="m-0">Broadway Brew Supply</p>
                                </div>
                                <div class="col-12 col-lg-12 px-0">
                                    <img class="trustpilot-logo mb-3 d-inline w-auto mt-0 pr-3"
                                        src="images/trust-pilot-logo.svg" alt="TrustPilot Logo" />
                                    <img class="stars d-inline mb-3 mt-0 w-auto" src="images/5_stars.svg"
                                        alt="TrustPilot 5 Stars" />
                                    <br />
                                    <!-- <a href="https://www.trustpilot.com/review/www.lendio.com" target="_blank"
                                        rel="noopener" data-wpel-link="internal">See more reviews on Trustpilot</a> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom-cta full base dark pt-2">
            <div class="container cta-bottom py-5">
                <div class="row">
                    <div class="col-12 py-md-5 text-center">
                        <h2>Get your small business loan today.</h2>
                        <form action="https://www.lendio.com/bp/basic-info" method="GET"
                            class="cta-content d-inline-block my-4">
                            <div class="input-container col-lg-8 float-lg-left mb-3"> <span
                                    class="dollar d-none d-lg-block">$</span> <input type="text"
                                    placeholder="How much do you need?" class="amount-seeking-input text-center pl-0"
                                    name="amountSeeking" /></div>
                            <div class="button-container col-lg-4 float-lg-left"> <button
                                    class="primary-button w-lg-100 ml-0 mb-2">Start</button></div>
                            <div class="clear"></div>
                        </form>
                        <h5 class="px-5" style="font-size: 17px;">Applying is free and it won't impact your credit</h5>
                        <h5 style="font-size: 17px;">Already have an account? &nbsp;<a
                                class="text-uppercase font-weight-bold" href="bp/login.html"
                                data-wpel-link="internal"><strong>Sign in</strong></a></h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="contact-area border-top border-bottom py-5">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-7 d-flex text-center text-lg-left justify-content-center"> <img
                            src="images/blue-line-phone.svg"
                            class="mr-md-3 mr-4 mb-3 mb-lg-0" alt="Phone Icon">
                        <div class="hidden-sm-down pt-2"></div>
                        <h3 class="float-lg-left">Give us a call <br class="d-inheret d-sm-none" /><a class="phone"
                                href="tel:4692250569" data-wpel-link="internal">(469) 225-0569</a></h3>
                    </div>
                    <div class="col-12 col-md-5 text-center text-lg-left">
                        <div class="hidden-sm-down pt-2"></div>
                        <p class="hours">Monday - Friday | 9am - 9pm Central Standard Time</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php include('../footer.php'); ?>
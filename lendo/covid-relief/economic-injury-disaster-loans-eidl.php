<?php include('../header.php'); ?>

<!-- ==== Page(PPP-Loan-forgivness-ESTIMATOR) yellow-bar Start ==== -->
<div class="hero position-relative" style="background: linear-gradient(90deg, #ff3366 0%, #fe9b02 100%); color: white;">
    <div class="site-close"><i class="fa fa-times"></i></div>
    <div class="container p-lg-0">
        <div class="py-lg-2 row hero-row">
            <div class="hero-responsive col-12 text-left text-md-center align-middle mt-4 my-lg-2"> The President has signed a relief bill
                that includes $284 billion for
                Paycheck Protection Program (PPP)
                loans. Virtual Fund Assist will submit
                completed, eligible applications to
                PPP lenders once the program
                officially resumes in January 2021. </div>
        </div>
    </div>
</div>

<!-- ==== Page(PPP-Loan-forgivness-ESTIMATOR) Main Start ==== -->
<div id="economic-injury-disaster-loans-eidl">
    <div class="hero" style="color:inherit;background: ; background: linear-gradient(-243.43494882292202deg, rgb(63, 7, 221) 0%, rgb(19, 156, 236) 100%); color: white;">
        <div class="container py-md-5">
            <div class="py-md-2 hero-row">
                <div class="hero-responsive col-12 text-left align-middle my-2">
                    <h2 class="text-center">Economic Injury Disaster Loans (EIDL)</h2>
                    <h5 class="body-large text-center px-lg-5 py-0 px-3"></h5>
                </div>
                <div class="d-none d-lg-block col-lg-6"></div>
            </div>
        </div>
    </div>
    <div id="alt-header-swap"></div>
    <div class="alt-dark py-5">
        <div class="container py-lg-0 py-5">
            <div class="row">
                <div class="col-12 col-md-6 py-5 text-center text-md-left">
                    <h2><span style="font-weight: 400;">What&#8217;s the deal with EIDL?</span></h2>
                    <p>The SBA has reopened EIDL applications to all business sectors. We’ll walk you through loan
                        amounts, uses, and qualification requirements for businesses seeking support and as a
                        reference for previous EIDL applicants.</p>
                </div>
                <div class="col-12 text-center my-auto offset-md-1 col-md-5 pb-5 py-md-4">
                    <img width="1254" height="837" src="" class="attachment-full size-full" alt="Happy business owner in front of shop" sizes="(max-width: 1254px) 100vw, 1254px" data-wp-pid="92491" nopin="nopin" title="Economic Injury Disaster Loans (EIDL)" />
                </div>
            </div>
        </div>
    </div>
    <div class="py-5 alt-grey">
        <div class="container my-5">
            <div class="col-12 mb-5"></div>
            <div class="row">
                <div id=" " class="col-12 col-md-6 d-md-block col-lg-6">
                    <div class="container-fluid">
                        <div class="row pb-4">
                            <div class="col-2"> <img src="<?= $home_url ?>/images/tools-icon.svg" class="d-block pr-2" alt="Small Business Loans" style="min-width:55px;" height="33" width="35" data-wp-pid="90734" nopin="nopin" title="Small Business Loans" /></div>
                            <div class="col-10">
                                <h3 class="pb-md-0" itemprop="name">Loan Use</h3>
                                <div class="fact-text pt-md-0 pb-md-4 pr-3">
                                    <h5 class="pt-0" itemprop="value">
                                        <p><span style="font-weight: 400;">An Economic Injury Disaster Loan (EIDL)
                                                can be used to support your business with working capital to cover
                                                day-to-day expenses.</span></p>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="collapseFacts" class="col-12 col-md-6 d-md-block collapse multi-collapse col-lg-6">
                    <div class="container-fluid">
                        <div class="row pb-4">
                            <div class="col-2"> <img src="<?= $home_url ?>/images/dollar-icon.svg" class="d-block pr-2" alt="Small Business Loans" style="min-width:55px;" height="35" width="38" data-wp-pid="90729" nopin="nopin" title="Small Business Loans" /></div>
                            <div class="col-10">
                                <h3 class="pb-md-0" itemprop="name">Loan Amount</h3>
                                <div class="fact-text pt-md-0 pb-md-4 pr-3">
                                    <h5 class="pt-0" itemprop="value">
                                        <p><span style="font-weight: 400;">Small businesses can receive up to $2
                                                million in support during an economic crisis.</span></p>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id=" " class="col-12 col-md-6 d-md-block col-lg-6">
                    <div class="container-fluid">
                        <div class="row pb-4">
                            <div class="col-2"> <img src="<?= $home_url ?>/images/storefront-icon.svg" class="d-block pr-2" alt="Small Business Loans" style="min-width:55px;" height="34" width="34" data-wp-pid="90732" nopin="nopin" title="Small Business Loans" /></div>
                            <div class="col-10">
                                <h3 class="pb-md-0" itemprop="name">Eligible Businesses</h3>
                                <div class="fact-text pt-md-0 pb-md-4 pr-3">
                                    <h5 class="pt-0" itemprop="value">
                                        <p><span style="font-weight: 400;">Primarily small businesses with fewer
                                                than 500 employees, as well as private nonprofit organizations, are
                                                eligible for EIDLs.</span></p>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3 col-12 col-md-6 text-left px-0 border-none d-md-none"> <a id="a-tag" class="justify-content-center d-block text-center font-weight-bold" data-toggle="collapse" href="#collapseFacts" role="button" aria-expanded="false" aria-controls="collapseFacts">
                        <h4 id="collapseFactsLink" class="font-weight-bold p-0 m-0">See All</h4> <svg id="collapse-arrow" class="bi bi-chevron-down" width="25" height="25" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M3.646 6.646a.5.5 0 01.708 0L10 12.293l5.646-5.647a.5.5 0 01.708.708l-6 6a.5.5 0 01-.708 0l-6-6a.5.5 0 010-.708z" clip-rule="evenodd"></path>
                        </svg>
                    </a></div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#a-tag').on('click', function() {
                var text = $('#collapseFactsLink').text();
                if (text === "See All") {
                    $('#collapseFactsLink').html('See less');
                    $('#collapse-arrow').addClass('d-none');
                } else {
                    $('#collapseFactsLink').text('See All');
                    $('#collapse-arrow').removeClass('d-none');


                }
            });


        });
    </script>
    <div class="py-5 alt-grey border-top">
        <div class="container py-0 py-lg-5">
            <div class="row">
                <div class="col-2 col-md-1 offset-md-1 py-4">
                    <!-- <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" /> -->
                </div>
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2>What is an Economic Injury Disaster Loan (EIDL)?</h2>
                    <p>An Economic Injury Disaster Loan is a traditional SBA loan reserved for disaster relief.
                        While generally known for strict requirements and long wait times before receiving funds,
                        much of that has shifted in response to the CARES Act and coronavirus (COVID-19) pandemic.
                    </p>
                    <p>The SBA is offering a loan advance, the <a href="https://www.sba.gov/funding-programs/loans/coronavirus-relief-options/economic-injury-disaster-loan-emergency-advance#section-header-0" data-wpel-link="external" target="_blank" rel="external noopener noreferrer">Emergency
                            Economic Injury Grant (EEIG)</a>, of up to $10,000 for businesses that applied.</p>
                </div>
                <div class="col-2 col-md-1 offset-md-1 py-4">
                    <!-- <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" /> -->
                </div>
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2>How Can You Use an EIDL?</h2>
                    <p>These loans offer support for small businesses and help them stay operational during
                        disaster-induced hardship. Use these funds to cover necessary day-to-day expenses your
                        business would have successfully covered before coronavirus or other <a href="../../blog/small-business-tools/mistakes-small-businesses-economic-crisis/index.html" data-wpel-link="internal">disasters impacted your finances</a>. Examples include:</p>
                    <ul>
                        <li>Payroll</li>
                        <li>Accounts payable</li>
                        <li>Providing paid sick leave to employees</li>
                        <li>Matching increased costs to obtain materials</li>
                        <li>Rent or mortgage payments</li>
                        <li>Other obligatory payments that cannot be met due to revenue loss</li>
                    </ul>
                </div>
                <div class="col-2 col-md-1 offset-md-1 py-4">
                    <!-- <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" /> -->
                </div>
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2>How Much Can You Get with an EIDL?</h2>
                    <p>Loan amounts for this disaster relief loan cap at $2 million. However, there are several
                        other important details you should know.</p>
                    <ul>
                        <li>Interest rates for small businesses are 3.75% and 2.75% for nonprofit</li>
                        <li>You can get terms up to 30 years</li>
                    </ul>
                </div>
                <div class="col-2 col-md-1 offset-md-1 py-4">
                    <!-- <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" /> -->
                </div>
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2>Who Qualifies for an EIDL?</h2>
                    <p>There are other specific requirements for EIDLs. These requirements also apply to businesses
                        that had previously applied for an EIDL due to COVID-19.</p>
                    <p>Those requirements are that you:</p>
                    <ul>
                        <li>Are a US business with fewer than 500 employees</li>
                        <li>Operate as a sole proprietor or an independent contractor during the covered period
                            (January 31, 2020, to December 31, 2020)</li>
                        <li>Are a 501(c), 501(d), or 501(e) private nonprofit</li>
                        <li>Can prove substantial economic injury caused by COVID-19</li>
                    </ul>
                    <p>The SBA requires you to prove your business has suffered as a direct result of coronavirus.
                        If your business was experiencing financial hardship before the pandemic, you likely won’t
                        qualify.</p>
                </div>
                <div class="col-2 col-md-1 offset-md-1 py-4">
                    <!-- <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" /> -->
                </div>
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2>How Can You Apply for an EIDL?</h2>
                    <p>You can apply for an EIDL and the advance <a href="https://covid19relief.sba.gov/#/" data-wpel-link="external" target="_blank" rel="external noopener noreferrer">on the SBA
                            website</a>.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="alt-white text-center py-5">
        <div class="container py-0 py-lg-5">
            <div class="row px-2">
                <div class="col-12 py-4"> <img src="<?= $home_url ?>/images/quotes.svg" alt="Quotes" /></div>
                <div class="col-12 py-4">
                    <h4 style="text-align: center;"><span style="font-weight: 400;">Russell was great. He went above
                            and beyond for us. He guided us through our options to help us secure the federal
                            funding we needed. Russell answered all our questions via email very promptly, and
                            remained polite and courteous at this uncertain time.</span></h4>
                    <h4 style="text-align: center;"><span style="font-weight: 400;">I highly recommend Russell at
                            Virtual Fund Assist for your small business financing needs. Excellent customer service.</span></h4>
                    <p style="text-align: center;">&#8211; James H.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- ==== Page(PPP-Loan-forgivness-ESTIMATOR) BOTTOM_BLUR_BAR Start ==== -->
<div class="bottom-cta full base dark pt-2">
    <div class="container cta-bottom py-5">
        <div class="row">
            <div class="col-12 py-md-5 text-center">
                <h2><span class="d-none d-lg-block">Need additional funding to your SBA loan?<br></span>Get your
                    small<br class="d-lg-none d-md-none"> business loan<br class="d-lg-none d-md-none"> today.</h2>
                <form action="https://www.lendio.com/bp/basic-info" method="GET" class="cta-content d-inline-block my-4">
                    <div class="input-container col-lg-8 float-lg-left mb-3"> <span class="dollar d-none d-lg-block">$</span> <input type="text" placeholder="How much do you need?" class="amount-seeking-input text-center pl-0" name="amountSeeking" /></div>
                    <div class="button-container col-lg-4 float-lg-left"> <button class="primary-button w-lg-100 ml-0 mb-2">Start</button></div>
                    <div class="clear"></div>
                </form>
                <h5 class="px-5" style="font-size: 17px;">Applying is free and it won't impact your credit</h5>
                <h5 style="font-size: 17px;">Already have an account? &nbsp;<a class="text-uppercase font-weight-bold" href="../../bp/login.html" data-wpel-link="internal"><strong>Sign in</strong></a></h5>
            </div>
        </div>
    </div>
</div>

<!-- ==== Page(SBA-Payback-Programm-Loan) FOOTER-TOP Start ==== -->
<div class="contact-area border-top border-bottom py-5">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-7 d-flex text-center text-lg-left justify-content-center">
                <img src="images/blue-line-phone.svg" class="mr-md-3 mr-4 mb-3 mb-lg-0" alt="Phone Icon">
                <div class="hidden-sm-down pt-2"></div>
                <h3 class="float-lg-left">Give us a call <br class="d-inheret d-sm-none" /><a class="phone" href="tel:4692250569" data-wpel-link="internal">(469) 225-0569</a></h3>
            </div>
            <div class="col-12 col-md-5 text-center text-lg-left">
                <div class="hidden-sm-down pt-2"></div>
                <p class="hours">Monday - Friday | 9am - 9pm Central Standard Time</p>
            </div>
        </div>
    </div>
</div>


<?php include('../footer.php'); ?>
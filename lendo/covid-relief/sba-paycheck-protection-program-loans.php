<?php include('../header.php'); ?>

<!-- ==== Page(SBA-Payback-Programm-Loan)  yellow-bar Start ==== -->
<div class="hero position-relative" style="background: linear-gradient(90deg, #ff3366 0%, #fe9b02 100%); color: white;">
    <div class="site-close"><i class="fa fa-times"></i></div>
    <div class="container p-lg-0">
        <div class="py-lg-2 row hero-row">
            <div class="hero-responsive col-12 text-left text-md-center align-middle mt-4 my-lg-2">The President has signed a relief bill
                that includes $284 billion for
                Paycheck Protection Program (PPP)
                loans. Virtual Fund Assist will submit
                completed, eligible applications to
                PPP lenders once the program
                officially resumes in January 2021. </div>
        </div>
    </div>
</div>

<!-- ==== Page(SBA-Payback-Programm-Loan)  Main-Content Start ==== -->
<div id="sba-paycheck-protection-program-loans">
    <div class="hero" style="color:inherit;background: ; background: linear-gradient(-243.43494882292202deg, rgb(63, 7, 221) 0%, rgb(19, 156, 236) 100%); color: white;">
        <div class="container py-md-5">
            <div class="py-md-2 hero-row">
                <div class="hero-responsive col-12 text-left align-middle my-2">
                    <h2 class="text-center">SBA Paycheck Protection Program (PPP) Loans</h2>
                    <h5 class="body-large text-center px-lg-5 py-0 px-3">Begin your PPP loan application through
                        Virtual Fund Assist to be matched with a PPP lender. Virtual Fund Assist is not a lender, and an application submitted
                        through Virtual Fund Assist does not guarantee you will receive a PPP loan or be matched to a lender. We
                        will accept applications throughout the program or until allocated funds for the program
                        have been exhausted.</h5>
                </div>
                <div class="d-none d-lg-block col-lg-6"></div>
            </div>
        </div>
    </div>
    <div id="alt-header-swap"></div>
    <div class="alt-dark py-5">
        <div class="container py-lg-0 py-5">
            <div class="row">
                <div class="col-12 col-md-6 py-5 text-center text-md-left">
                    <h3><span style="font-weight: 400;">Need COVID-19 relief? </span></h3>
                    <p>Begin your PPP loan application through Virtual Fund Assist to be matched with a PPP lender.</p>
                    <p><a class="primary-button" href="../../bp/owner-info.html" data-wpel-link="internal">Start
                            application now</a></p>
                </div>
                <div class="col-12 text-center my-auto offset-md-1 col-md-5 pb-5 py-md-4">
                    <img width="1255" height="836" src="../images/woman-restaurant.jpg" class="attachment-full size-full" alt="Owner in front of restaurant" title="SBA Paycheck Protection Program (PPP) Loans" />
                </div>
            </div>
        </div>
    </div>
    <div class="py-5 alt-grey">
        <div class="container my-5">
            <div class="col-12 mb-5"></div>
            <div class="row">
                <div id=" " class="col-12 col-md-6 d-md-block col-lg-6">
                    <div class="container-fluid">
                        <div class="row pb-4">
                            <div class="col-2">
                                <img src="<?= $home_url ?>/images/dollar-icon.svg" class="d-block pr-2" alt="Small Business Loans" style="min-width:55px;" height="35" width="38" data-wp-pid="90729" nopin="nopin" title="Small Business Loans" />
                            </div>
                            <div class="col-10">
                                <h3 class="pb-md-0" itemprop="name">Low Interest Rate</h3>
                                <div class="fact-text pt-md-0 pb-md-4 pr-3">
                                    <h5 class="pt-0" itemprop="value">
                                        <p><span style="font-weight: 400;">1% fixed rate APR for the life of the
                                                loan</span></p>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="collapseFacts" class="col-12 col-md-6 d-md-block collapse multi-collapse col-lg-6">
                    <div class="container-fluid">
                        <div class="row pb-4">
                            <div class="col-2"> <img src="<?= $home_url ?>/images/money-icon.svg" class="d-block pr-2" alt="Small Business Loans" style="min-width:55px;" height="29" width="41" data-wp-pid="90730" nopin="nopin" title="Small Business Loans" /></div>
                            <div class="col-10">
                                <h3 class="pb-md-0" itemprop="name">Payment Deferral</h3>
                                <div class="fact-text pt-md-0 pb-md-4 pr-3">
                                    <h5 class="pt-0" itemprop="value">
                                        <p><span style="font-weight: 400;">Payments deferred for 6 months.</span>
                                        </p>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id=" " class="col-12 col-md-6 d-md-block col-lg-6">
                    <div class="container-fluid">
                        <div class="row pb-4">
                            <div class="col-2"> <img src="<?= $home_url ?>/images/rocket-icon.svg" class="d-block pr-2" alt="Small Business Loans" style="min-width:55px;" height="36" width="35" data-wp-pid="90731" nopin="nopin" title="Small Business Loans" /></div>
                            <div class="col-10">
                                <h3 class="pb-md-0" itemprop="name">Loan Forgiveness</h3>
                                <div class="fact-text pt-md-0 pb-md-4 pr-3">
                                    <h5 class="pt-0" itemprop="value">
                                        <p><span style="font-weight: 400;">On qualified uses like payroll, mortgage
                                                interest, rent, and utilities</span></p>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3 col-12 col-md-6 text-left px-0 border-none d-md-none"> <a id="a-tag" class="justify-content-center d-block text-center font-weight-bold" data-toggle="collapse" href="#collapseFacts" role="button" aria-expanded="false" aria-controls="collapseFacts">
                        <h4 id="collapseFactsLink" class="font-weight-bold p-0 m-0">See All</h4> <svg id="collapse-arrow" class="bi bi-chevron-down" width="25" height="25" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M3.646 6.646a.5.5 0 01.708 0L10 12.293l5.646-5.647a.5.5 0 01.708.708l-6 6a.5.5 0 01-.708 0l-6-6a.5.5 0 010-.708z" clip-rule="evenodd"></path>
                        </svg>
                    </a></div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#a-tag').on('click', function() {
                var text = $('#collapseFactsLink').text();
                if (text === "See All") {
                    $('#collapseFactsLink').html('See less');
                    $('#collapse-arrow').addClass('d-none');
                } else {
                    $('#collapseFactsLink').text('See All');
                    $('#collapse-arrow').removeClass('d-none');


                }
            });


        });
    </script>
    <!-- ==== Paycheck Protection Loan ==== --->
    <div class="py-5 alt-grey border-top">
        <div class="container py-0 py-lg-5">
            <div class="row">
                <div class="col-2 col-md-1 offset-md-1 py-4">
                    <!-- <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" /> -->
                </div>
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2>Paycheck Protection Program (PPP) Loans</h2>
                    <p>Small businesses need a lifeline right now, and the federal government has stepped up to
                        offer Paycheck Protection Program loans. Because of the pared-down SBA requirements for PPP
                        loans, more small businesses will qualify for the SBA coronavirus loans than previous SBA
                        loan options.</p>
                </div>
                <div class="col-2 col-md-1 offset-md-1 py-4">
                    <!-- <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" /> -->
                </div>
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2>Paycheck Protection Program Loan Requirements</h2>
                    <p>The requirements for PPP loans are incredibly simple. Your business only needs to hit 2
                        criteria:</p>
                    <ul>
                        <li>Your business (or nonprofit) was in operation as of February 15, 2020</li>
                        <li>You’re an independent contractor or sole proprietor, or your business/organization has
                            either employees or independent contractors for whom they have associated payroll costs
                        </li>
                        <li>You certify that your business has sustained economic damage due to COVID-19</li>
                    </ul>
                    <p>That’s it.</p>
                </div>
                <div class="col-2 col-md-1 offset-md-1 py-4">
                    <!-- <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" /> -->
                </div>
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2>What Paycheck Protection Program Loans Are For</h2>
                    <p>PPP loans will help small businesses, including sole proprietors and independent contractors,
                        and private nonprofits maintain payrolls and continue necessary payroll-related payments
                        like rent and utilities. The full allowable uses of the loan are:</p>
                    <ul>
                        <li>Payroll costs: Compensation in the form of salaries, wages, commissions (or similar
                            compensation), cash tip payments (or the equivalent)</li>
                        <li>Healthcare costs: Any costs related to the continuation of group healthcare benefits,
                            including insurance premiums</li>
                        <li>Mortgage interest payments (but not payments on the mortgage principal)</li>
                        <li>Rent</li>
                        <li>Utilities</li>
                        <li>Interest on any other debt obligations incurred before February 15, 2020</li>
                    </ul>
                </div>
                <div class="col-2 col-md-1 offset-md-1 py-4">
                    <!-- <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" /> -->
                </div>
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2>How Paycheck Protection Program Loans Are Calculated</h2>
                    <p>PPP loans are calculated based on 2.5 times your business’s (or organization’s) monthly
                        payroll costs. Payroll costs include compensation, as outlined above, along with other
                        payroll-related costs like retirement payments, state and local taxes on payroll, payment
                        for vacation or paid leave, group healthcare costs, and allowances for separation or
                        dismissal. For a comprehensive outline of what is used and excluded, you can <a href="../../business-calculators/sba-loan-calculator/index.html" data-wpel-link="internal">visit our PPP calculator</a>, where you can also get an
                        estimate on your potential PPP loan size.</p>
                </div>
                <div class="col-2 col-md-1 offset-md-1 py-4">
                    <!-- <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" /> -->
                </div>
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2>Are SBA PPP Loans the Same as Disaster Loans?</h2>
                    <p>PPP loans differ from Economic Injury and Disaster Loans (EIDLs). PPP loans are available to
                        all US businesses based on the requirements above.</p>
                    <p>Due to overwhelming demand, applications have closed for EIDLs. If you previously applied for
                        an EIDL, here’s how the loan differs from PPP. EIDLs are limited to designated disaster
                        areas— for COVID-19, the SBA classifies that as all US states and territories—and the loans
                        have more stringent requirements. Unlike PPPs, EIDLs/disaster loans must be repaid in full.
                    </p>
                </div>
                <div class="col-2 col-md-1 offset-md-1 py-4">
                    <!-- <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" /> -->
                </div>
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2>Do You Have to Prove Economic Injury?</h2>
                    <p>To qualify for a PPP loan, you must certify that your business has sustained economic injury
                        due to the COVID-19 pandemic. This certification is required to ensure that funds are
                        distributed to small businesses that have been hit the hardest by business disruptions due
                        to the pandemic.</p>
                </div>
                <div class="col-2 col-md-1 offset-md-1 py-4">
                    <!-- <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" /> -->
                </div>
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2>Paycheck Protection Program Forgiveness</h2>
                    <p>PPP loans are eligible to be <a href="../ppp-loan-forgiveness/index.html" data-wpel-link="internal">forgiven</a>, up to 100% of the loan principal if the funds
                        are used appropriately. Here’s what you need to know about what qualifies, how to apply, and
                        how to <a href="../../business-calculators/ppp-loan-forgiveness-estimator/index.html" data-wpel-link="internal">calculate your potential PPP loan forgiveness</a>.</p>
                    <h3>Costs and Payments Eligible for Forgiveness</h3>
                    <p>Any costs incurred and payments made in the first 24 weeks of the loan, following the
                        origination date, under these set categories are eligible for forgiveness:</p>
                    <ul>
                        <li>Payroll costs (as listed above)</li>
                        <li>Mortgage interest payments (but not payments on the principal)</li>
                        <li>Rent</li>
                        <li>Utilities</li>
                    </ul>
                    <h3>Calculating Your PPP Loan Forgiveness Amount</h3>
                    <p>No more than 40% of the forgiven amount can be for non-payroll costs (i.e., mortgage
                        interest, rent, and utilities).</p>
                    <p>If your business has laid off employees, that will also affect how much of your loan can be
                        forgiven. Originally, employees needed to be rehired by June 30, 2020, in order for their
                        salaries to be counted toward forgiveness. Under the Paycheck Protection Plan Flexibility
                        Act (PPPFA), that deadline was extended until December 31, 2020. The PPPFA adds additional
                        exceptions for small businesses and nonprofits that are unable to rehire workers by that
                        date if:</p>
                    <ul>
                        <li>Your business was unable to rehire an individual included as an employee as of February
                            15, 2020.</li>
                        <li>You were able to demonstrate that you were unable to hire similarly qualified employees
                            on or before February 15, 2020.</li>
                        <li>You are able to demonstrate an inability to return to the same level of business
                            activity at which your business was operating before February 15, 2020.</li>
                    </ul>
                    <p>Additional guidance from the government is needed as to exactly how a business would
                        “demonstrate” an inability to rehire employees or that you were unable to return to the same
                        level of business activity.</p>
                    <p>With employee count considerations and other factors, the total effect on your PPP loan’s
                        forgiveness-eligibility depends on some complicated math that your funding manager can walk
                        you through to give you a specific answer for your business.</p>
                </div>
                <div class="col-2 col-md-1 offset-md-1 py-4">
                    <!-- <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" /> -->
                </div>
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2>How Long Will It Take to Receive Funds?</h2>
                    <p>Once a borrower receives a Preferred Lender Program (PLP) number for their loan, the loan is
                        approved by the SBA, and funds are reserved for the borrower. Starting on the date a
                        borrower receives a PLP number, the lender has 10 calendar days to disburse funds. The loan
                        must be disbursed in full, and the 24-week loan forgiveness period begins the day funds are
                        disbursed.</p>
                    <p>Due to overwhelming demand and high loan volume, many lenders struggled to fund loans from
                        the first round within the 10-day period. The <a href="../../blog/coronavirus/update-sba-rule-ppp-loans/index.html" data-wpel-link="internal">SBA issued guidance</a> that for borrowers who had been issued
                        a PLP number prior to April 28, lenders had 10 calendar days to disburse funds, starting on
                        April 28.</p>
                    <p>If a lender cannot disburse funds due to delays from a borrower, like missing paperwork, the
                        lender has 20 days to disburse funds. If they haven’t received the necessary information
                        from the borrower by the end of that 20-day period, the loan may be canceled. If loan funds
                        are designated for the refinancing of an Economic Injury and Disaster Loan (EIDL), the funds
                        will be deducted from the disbursement amount and paid directly to the SBA by the lender.
                    </p>
                </div>
                <div class="col-2 col-md-1 offset-md-1 py-4">
                    <!-- <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" /> -->
                </div>
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2>When Do Paycheck Protection Program Loan Payments Start?</h2>
                    <p>For portions of the loan that are not forgiven, payments are deferred for the first 6 months.
                        But keep in mind that interest will accrue during the 6-month deferral period.</p>
                </div>
                <div class="col-2 col-md-1 offset-md-1 py-4">
                    <!-- <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" /> -->
                </div>
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2>Can You Get a PPP Loan If You Have Other Loans?</h2>
                    <p>Yes, you can qualify for a PPP loan even if you already have other loans, including other SBA
                        loans. You cannot use the funds from PPP loans and other loans for duplicate use at the same
                        time. For example, if you use a disaster loan (EIDL or loan advance) to pay your business’s
                        May rent, you cannot also apply for a PPP loan to cover May rent.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="alt-white text-center py-5">
        <div class="container py-0 py-lg-5">
            <div class="row px-2">
                <div class="col-12 py-4">
                    <img src="<?= $home_url ?>/images/quotes.svg" alt="Quotes" />
                </div>
                <div class="col-12 py-4">
                    <h4 style="text-align: center;"><span style="font-weight: 400;">Anthony made applying for the
                            PPP SBA loan a pleasure! I was not looking forward to the process of applying for the
                            loan and had many questions about both the process and the loan program. Anthony
                            patiently entertained my many questions about the loan and painlessly walked me through
                            the application. Highly recommended.</span></h4>
                    <p style="text-align: center;">&#8211; David L.</p>
                    <p>&nbsp;</p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- ==== Page(SBA-Payback-Programm-Loan) FOOTER Start ==== -->
<div class="bottom-cta full base dark pt-2">
    <div class="container cta-bottom py-5">
        <div class="row">
            <div class="col-12 py-md-5 text-center">
                <h2><span class="d-none d-lg-block">Need additional funding to your SBA loan?<br></span>Get your
                    small<br class="d-lg-none d-md-none"> business loan<br class="d-lg-none d-md-none"> today.</h2>
                <form action="https://virtualfundassist.com/lendio/tabs/basic-info" method="GET" class="cta-content d-inline-block my-4">
                    <div class="input-container col-lg-8 float-lg-left mb-3"> <span class="dollar d-none d-lg-block">$</span> <input type="text" placeholder="How much do you need?" class="amount-seeking-input text-center pl-0" name="amountSeeking" /></div>
                    <div class="button-container col-lg-4 float-lg-left"> <button class="primary-button w-lg-100 ml-0 mb-2">Start</button></div>
                    <div class="clear"></div>
                </form>
                <h5 class="px-5" style="font-size: 17px;">Applying is free and it won't impact your credit</h5>
                <h5 style="font-size: 17px;">Already have an account? &nbsp;<a class="text-uppercase font-weight-bold" href="../../bp/login.html" data-wpel-link="internal"><strong>Sign in</strong></a></h5>
            </div>
        </div>
    </div>
</div>

<!-- ==== Page(SBA-Payback-Programm-Loan) FOOTER-TOP Start ==== -->
<div class="contact-area border-top border-bottom py-5">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-7 d-flex text-center text-lg-left justify-content-center">
                <img src="images/blue-line-phone.svg" class="mr-md-3 mr-4 mb-3 mb-lg-0" alt="Phone Icon">
                <div class="hidden-sm-down pt-2"></div>
                <h3 class="float-lg-left">Give us a call <br class="d-inheret d-sm-none" /><a class="phone" href="tel:4692250569" data-wpel-link="internal">(469) 225-0569</a></h3>
            </div>
            <div class="col-12 col-md-5 text-center text-lg-left">
                <div class="hidden-sm-down pt-2"></div>
                <p class="hours">Monday - Friday | 9am - 9pm Central Standard Time</p>
            </div>
        </div>
    </div>
</div>

<?php include('../footer.php'); ?>
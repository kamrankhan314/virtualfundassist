<?php include('../header.php'); ?>

    <!-- ==== Page(SBA-Payback-Programm-Loan) yellow-bar Start ==== -->
    <div class="hero position-relative"
        style="background: linear-gradient(90deg, #ff3366 0%, #fe9b02 100%); color: white;">
        <div class="site-close"><i class="fa fa-times"></i></div>
        <div class="container p-lg-0">
            <div class="py-lg-2 row hero-row">
                <div class="hero-responsive col-12 text-left text-md-center align-middle mt-4 my-lg-2"> The President has signed a relief bill
                    that includes $284 billion for
                    Paycheck Protection Program (PPP)
                    loans. Virtual Fund Assist will submit
                    completed, eligible applications to
                    PPP lenders once the program
                    officially resumes in January 2021. </div>
            </div>
        </div>
    </div>

    <!-- ==== Page(SBA-Payback-Programm-Loan)  Main-Content Start ==== -->
    <div id="apply-ppp-loan">
        <div class="hero"
            style="color:inherit;background: ; background: linear-gradient(-243.43494882292202deg, rgb(63, 7, 221) 0%, rgb(19, 156, 236) 100%); color: white;">
            <div class="container py-md-5">
                <div class="py-md-2 hero-row">
                    <div class="hero-responsive col-12 text-left align-middle my-2">
                        <h2 class="text-center">How to Apply for a PPP Loan</h2>
                        <h5 class="body-large text-center px-lg-5 py-0 px-3">Begin your PPP loan application through
                        Virtual Fund Assist to be matched with a PPP lender. Virtual Fund Assist is not a lender, and an application submitted
                            through Virtual Fund Assist does not guarantee you will receive a PPP loan or be matched to a lender. We
                            will accept applications throughout the program or until allocated funds for the program
                            have been exhausted.</h5>
                    </div>
                    <div class="d-none d-lg-block col-lg-6"></div>
                </div>
            </div>
        </div>
        <div id="alt-header-swap"></div>
        <div class="py-5 alt-white p-0 ">
            <div class="container py-0 py-lg-0">
                <div class="row ">
                    <div class="col-12 ">
                        <h2>Here&#8217;s Your Step-by-Step Guide to Applying for a PPP Loan</h2>
                        <p>This step-by-step guide will walk you through our PPP application to ensure you have a
                            completed application—so that your PPP loan can be submitted, approved, and funded as fast
                            as possible.</p>
                        <h3>New Requirements for an SBA Loan</h3>
                        <p>The PPP loan application has been recently updated to include new requirements. If you’ve
                            already completed an application, it is essential that you log in to add these new
                            requirements in addition to confirming existing fields:</p>
                        <ul>
                            <li>Business start date</li>
                            <li>Business industry</li>
                            <li>Date of birth for all business owners with 20%+ share in the business (including the
                                applicant)</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="py-5 alt-grey p-0 ">
            <div class="container py-0 py-lg-0">
                <div class="row ">
                    <div class="col-12 ">
                        <h2>Step 1: Create or Log into your Lendio Account</h2>
                        <ul>
                            <li>
                                <img class="size-full wp-image-92683 alignright" src="../images/create-profile.png" alt="Logging in to the PPP application" width="399" height="324" data-wp-pid="" sizes="(max-width: 399px) 100vw, 399px" />Click on the link to 
                                <a href="https://virtualfundassist.com/lendio/login" data-wpel-link="internal">If you already have an account.</a>.
                            </li>
                            <li>Enter your username and password.</li>
                        </ul>
                        <h2>Select ‘Edit Application’</h2>
                        <p>Once you’ve logged in, you will be directed to a page that looks like this. Select “Edit
                            Application” to review existing information and complete new requirements.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="py-5 alt-grey border-top p-0 ">
            <div class="container py-0 py-lg-0">
                <div class="row ">
                    <div class="col-12 ">
                        <h2>Step 2: Confirm Existing Business Information</h2>
                        <ol>
                            <li style="font-weight: 400;">
                                <span style="font-weight: 400;">
                                    <img class=" wp-image-92691 alignright" src="<?= $home_url ?>/images/confirm-business-ppp.jpg" alt="Confirming business info in PPP application" width="295" height="621"
                                        data-wp-pid="92691" />Business address: Confirm that address is accurate, including the </span>
                                    <b>5-digit zip code</b><span style="font-weight: 400;"> (it’s
                                    important that it’s the 5-digit zip code and not the 9-digit zip code).</span></li>
                            <li style="font-weight: 400;"><span style="font-weight: 400;">Business type: Verify you’ve
                                    selected the right business type. This affects the documents you may be required to
                                    provide. </span></li>
                            <li style="font-weight: 400;"><span style="font-weight: 400;">Business Tax ID: Confirm that
                                    you have entered a valid </span><b>9-digit tax ID</b><span
                                    style="font-weight: 400;">. Double-check for any typos and confirm that the ID
                                    number is </span><b>9 digits</b><span style="font-weight: 400;"> long. </span></li>
                            <li style="font-weight: 400;"><span style="font-weight: 400;">Loan amount requested: The
                                    requested amount should be 2.5 times your average monthly payroll costs. It should
                                    not be $0. It should not be hundreds of millions of dollars. For help, you can use
                                    our </span><a href="../../business-calculators/sba-loan-calculator/index.html"
                                    data-wpel-link="internal"><span style="font-weight: 400;">PPP loan
                                        calculator</span></a><span style="font-weight: 400;">. </span></li>
                            <li style="font-weight: 400;"><span style="font-weight: 400;">Employee count: Verify
                                    employee count is between 1 and 500. </span><b><a
                                        href="../../blog/coronavirus/ppp-loans-self-employed/index.html"
                                        data-wpel-link="internal">Sole proprietors</a> and <a
                                        href="../../blog/coronavirus/ppp-loans-1099-employees/index.html"
                                        data-wpel-link="internal">1099 workers</a> should enter 1. 0 is not a valid
                                    entry.</b></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="py-5 alt-grey border-top p-0 ">
            <div class="container py-0 py-lg-0">
                <div class="row ">
                    <div class="col-12 ">
                        <h2>Step 3: Add New Requirements for Business Information</h2>
                        <p><span style="font-weight: 400;">On the same “Business Information” page of the application,
                                you need to complete these new required fields. </span></p>
                        <ol>
                            <li style="font-weight: 400;"><span style="font-weight: 400;">Business start date:
                                </span><b>This must be the same as the date on your Secretary of State filing</b><span
                                    style="font-weight: 400;">. </span></li>
                            <li style="font-weight: 400;"><span style="font-weight: 400;">Industry: <strong>Start typing
                                        in your industry</strong>, then select it from the menu options that appear. If
                                    you don’t see your exact industry, pick 1 that matches the closest. Applications
                                    cannot be approved by the SBA without an industry. </span></li>
                        </ol>
                        <p>&nbsp;</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="py-5 alt-grey border-top p-0 ">
            <div class="container py-0 py-lg-0">
                <div class="row ">
                    <div class="col-12 ">
                        <h2>Step 4: Confirm Ownership</h2>
                        <p><span style="font-weight: 400;"><img class="alignright wp-image-92893 "
                                    src="../../wp-content/uploads/2020/04/confirm-ownership-e1587999938727.png"
                                    alt="Confirming ownership in PPP loan application" width="291" height="275"
                                    data-wp-pid="92893"
                                    srcset="https://www.lendio.com/wp-content/uploads/2020/04/confirm-ownership-e1587999938727.png 574w, https://www.lendio.com/wp-content/uploads/2020/04/confirm-ownership-e1587999938727-300x282.png 300w"
                                    sizes="(max-width: 291px) 100vw, 291px" />On the first “Owner Information” page,
                                confirm the owner percentage of the applicant. </span><b>The ownership percentage must
                                be greater than 0</b><span style="font-weight: 400;">. </span></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="py-5 alt-grey border-top p-0 ">
            <div class="container py-0 py-lg-0">
                <div class="row ">
                    <div class="col-12 ">
                        <h2>Step 5: Confirm Additional Owner Info</h2>
                        <p><span style="font-weight: 400;"><img class="size-full wp-image-92682 alignright"
                                    src="../../wp-content/uploads/2020/04/confirm-additional-owner-ppp-application-e1586872780979.png"
                                    alt="Confirm additional owner info screen in PPP application" width="274"
                                    height="462" data-wp-pid="92682"
                                    srcset="https://www.lendio.com/wp-content/uploads/2020/04/confirm-additional-owner-ppp-application-e1586872780979.png 274w, https://www.lendio.com/wp-content/uploads/2020/04/confirm-additional-owner-ppp-application-e1586872780979-178x300.png 178w"
                                    sizes="(max-width: 274px) 100vw, 274px" />Next, confirm that all necessary
                                information for additional owners is included. You must add information for any
                                individual with at least a 20% share in the business. </span></p>
                        <p><span style="font-weight: 400;">All fields on the “Additional Owner” page must be completed,
                            </span><b>including date of birth</b><span style="font-weight: 400;">. (You will also be
                                asked to include date of birth for the owner who is applying on the “E-Sign”
                                page.)</span></p>
                        <p><span style="font-weight: 400;">Note: You will need a color copy of each owner’s Driver’s
                                License—both front and back. You will be asked to upload the copies in the “Documents”
                                section (It’s next. We wanted to give you a fair warning so it’s on your radar). </span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="py-5 alt-grey border-top p-0 ">
            <div class="container py-0 py-lg-0">
                <div class="row ">
                    <div class="col-12 ">
                        <h2>Step 6: Confirm Documents</h2>
                        <p><span style="font-weight: 400;">On the “Upload Your Documents” page, you should review all
                                uploaded documents. Confirm that everything previously uploaded is correct, select the
                                matching document boxes, and add any documents you’re missing to ensure a completed PPP
                                application. </span></p>
                        <p><span style="font-weight: 400;">If you don’t have the following attachments added to your
                                application, please upload them:</span></p>
                        <ol>
                            <li style="font-weight: 400;"><span style="font-weight: 400;">Driver’s License for all
                                    owners with 20%+ share in the business: A </span><b>color copy</b><span
                                    style="font-weight: 400;"> of the front and back of a valid Driver’s License for
                                    each owner. Make sure to double-check that the </span><b>Driver’s License is not
                                    expired</b><span style="font-weight: 400;">. </span></li>
                            <li style="font-weight: 400;"><span style="font-weight: 400;">Acceptable Payroll Documents
                                    (listed in order of lender and supposed SBA preference):</span>
                                <ol>
                                    <li style="font-weight: 400;"><span style="font-weight: 400;">941 Quarterly Tax
                                            Filings (2019, 2020 Q1)</span></li>
                                    <li style="font-weight: 400;"><span style="font-weight: 400;">944 Annual Tax Filings
                                            (2019)</span></li>
                                    <li style="font-weight: 400;"><span style="font-weight: 400;">Payroll Register for
                                            the previous 12 months</span></li>
                                    <li style="font-weight: 400;"><span style="font-weight: 400;">12 months most recent
                                            bank statements</span></li>
                                </ol>
                            </li>
                        </ol>
                        <p>NOTE: The recommended list of documents you see on your application may vary depending on the
                            business type.</p>
                        <p><img class="aligncenter wp-image-92689"
                                src="../../wp-content/uploads/2020/04/Screen-Shot-2020-04-13-at-8.50.36-PM.png"
                                alt="Upload ID and payroll" width="520" height="451" data-wp-pid="92689"
                                srcset="https://www.lendio.com/wp-content/uploads/2020/04/Screen-Shot-2020-04-13-at-8.50.36-PM.png 834w, https://www.lendio.com/wp-content/uploads/2020/04/Screen-Shot-2020-04-13-at-8.50.36-PM-300x260.png 300w, https://www.lendio.com/wp-content/uploads/2020/04/Screen-Shot-2020-04-13-at-8.50.36-PM-800x694.png 800w"
                                sizes="(max-width: 520px) 100vw, 520px" /></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="py-5 alt-grey border-top p-0 ">
            <div class="container py-0 py-lg-0">
                <div class="row ">
                    <div class="col-12 ">
                        <h2>Step 7: Confirm E-Sign</h2>
                        <p><span style="font-weight: 400;"><img class=" wp-image-92688 alignright"
                                    src="../../wp-content/uploads/2020/04/confirm-esign-ppp.png"
                                    alt="confirm e-sign on PPP application" width="298" height="406" data-wp-pid="92688"
                                    srcset="https://www.lendio.com/wp-content/uploads/2020/04/confirm-esign-ppp.png 376w, https://www.lendio.com/wp-content/uploads/2020/04/confirm-esign-ppp-220x300.png 220w"
                                    sizes="(max-width: 298px) 100vw, 298px" />It’s almost time to sign on the dotted
                                line. Before you resubmit your PPP application, please:</span></p>
                        <ol>
                            <li style="font-weight: 400;"><span style="font-weight: 400;">Confirm that your full legal
                                    name is on the application. </span></li>
                            <li style="font-weight: 400;"><span style="font-weight: 400;">Ensure your Social Security
                                    number is entered correctly (</span><b>9 digits</b><span
                                    style="font-weight: 400;">).</span></li>
                            <li style="font-weight: 400;"><span style="font-weight: 400;">Enter the applicant’s date of
                                    birth </span><b>(New field)</b><span style="font-weight: 400;">. </span></li>
                            <li style="font-weight: 400;"><span style="font-weight: 400;">Enter Applicant Gender: Select
                                    “Male,” “Female,” or “Not Disclosed.” The SBA requires this demographic information
                                    to be recorded for its program reporting. </span></li>
                            <li style="font-weight: 400;"><strong>Recertify, re-sign, and then re-submit! This is an
                                    essential step for any changes to be saved and uploaded. </strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">And that’s it! You’re good to go. </span></p>
                        <p>&nbsp;</p>
                        <h6></h6>
                    </div>
                </div>
            </div>
        </div>
        <div class="py-5 py-md-5 alt-dark">
            <div class="container">
                <div class="row">
                    <div class="text-center text-lg-left my-auto col-md-6 d-none d-md-block"> <img width="2121"
                            height="1414" src="../../wp-content/uploads/2020/04/iStock-1212773712.jpg"
                            class="attachment-full size-full"
                            alt="Small business owner wearing a medical mask and working on computer"
                            srcset="https://www.lendio.com/wp-content/uploads/2020/04/iStock-1212773712.jpg 2121w, https://www.lendio.com/wp-content/uploads/2020/04/iStock-1212773712-300x200.jpg 300w, https://www.lendio.com/wp-content/uploads/2020/04/iStock-1212773712-1024x683.jpg 1024w, https://www.lendio.com/wp-content/uploads/2020/04/iStock-1212773712-1536x1024.jpg 1536w, https://www.lendio.com/wp-content/uploads/2020/04/iStock-1212773712-2048x1365.jpg 2048w, https://www.lendio.com/wp-content/uploads/2020/04/iStock-1212773712-800x533.jpg 800w"
                            sizes="(max-width: 2121px) 100vw, 2121px" data-wp-pid="92619" nopin="nopin"
                            title="Q2 Forecast for American Small Business—Will You Survive?" /></div>
                    <div class="col-12 mx-auto col-md-6 pl-0 pl-md-4 py-5">
                        <h3><span style="font-weight: 400;">Apply for your PPP loan.</span></h3>
                        <p class="body-large text-center px-lg-5 py-0 px-3" style="text-align: left;">Begin your PPP
                            loan application through Virtual Fund Assist to be matched with a PPP lender. Virtual Fund Assist is not a lender, and
                            an application submitted through Virtual Fund Assist does not guarantee you will receive a PPP loan or be
                            matched to a lender. We will accept applications throughout the program or until allocated
                            funds for the program have been exhausted.</p>
                        <p><a class="primary-button" href="../../bp/owner-infodc9c.html?interest=PPP%20Only"
                                data-wpel-link="internal">Get Started</a></p>
                    </div>
                    <div class="col-12 text-center d-block d-md-none"> <img width="2121" height="1414"
                            src="../../wp-content/uploads/2020/04/iStock-1212773712.jpg"
                            class="attachment-full size-full"
                            alt="Small business owner wearing a medical mask and working on computer"
                            srcset="https://www.lendio.com/wp-content/uploads/2020/04/iStock-1212773712.jpg 2121w, https://www.lendio.com/wp-content/uploads/2020/04/iStock-1212773712-300x200.jpg 300w, https://www.lendio.com/wp-content/uploads/2020/04/iStock-1212773712-1024x683.jpg 1024w, https://www.lendio.com/wp-content/uploads/2020/04/iStock-1212773712-1536x1024.jpg 1536w, https://www.lendio.com/wp-content/uploads/2020/04/iStock-1212773712-2048x1365.jpg 2048w, https://www.lendio.com/wp-content/uploads/2020/04/iStock-1212773712-800x533.jpg 800w"
                            sizes="(max-width: 2121px) 100vw, 2121px" data-wp-pid="92619" nopin="nopin"
                            title="Q2 Forecast for American Small Business—Will You Survive?" /></div>
                </div>
            </div>
        </div>
    </div>

    <!-- ==== Page(SBA-Payback-Programm-Loan) Bottom-Blue-Bar Start ==== -->
    <div class="bottom-cta full base dark pt-2">
        <div class="container cta-bottom py-5">
            <div class="row">
                <div class="col-12 py-md-5 text-center">
                    <h2><span class="d-none d-lg-block">Need additional funding to your SBA loan?<br></span>Get your
                        small<br class="d-lg-none d-md-none"> business loan<br class="d-lg-none d-md-none"> today.</h2>
                    <form action="https://www.lendio.com/bp/basic-info" method="GET"
                        class="cta-content d-inline-block my-4"> <input type="hidden" name="referral_url"
                            value="https://www.google.com/" />
                        <div class="input-container col-lg-8 float-lg-left mb-3"> <span
                                class="dollar d-none d-lg-block">$</span> <input type="text"
                                placeholder="How much do you need?" class="amount-seeking-input text-center pl-0"
                                name="amountSeeking" /></div>
                        <div class="button-container col-lg-4 float-lg-left"> <button
                                class="primary-button w-lg-100 ml-0 mb-2">Start</button></div>
                        <div class="clear"></div>
                    </form>
                    <h5 class="px-5" style="font-size: 17px;">Applying is free and it won't impact your credit</h5>
                    <h5 style="font-size: 17px;">Already have an account? &nbsp;<a
                            class="text-uppercase font-weight-bold" href="../../bp/login.html"
                            data-wpel-link="internal"><strong>Sign in</strong></a></h5>
                </div>
            </div>
        </div>
    </div>

    <!-- ==== Page(SBA-Payback-Programm-Loan) FOOTER-TOP Start ==== -->
    <div class="contact-area border-top border-bottom py-5">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-7 d-flex text-center text-lg-left justify-content-center">
                    <img src="images/blue-line-phone.svg" class="mr-md-3 mr-4 mb-3 mb-lg-0" alt="Phone Icon">
                    <div class="hidden-sm-down pt-2"></div>
                    <h3 class="float-lg-left">Give us a call <br class="d-inheret d-sm-none" /><a class="phone" href="tel:4692250569" data-wpel-link="internal">(469) 225-0569</a></h3>
                </div>
                <div class="col-12 col-md-5 text-center text-lg-left">
                    <div class="hidden-sm-down pt-2"></div>
                    <p class="hours">Monday - Friday | 9am - 9pm Central Standard Time</p>
                </div>
            </div>
        </div>
    </div>

<?php include('../footer.php'); ?>
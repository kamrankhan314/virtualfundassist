<?php include('../header.php'); ?>

<!-- ==== Page(PPP-Loan-forgivness) yellow-bar Start ==== -->
<div class="hero position-relative" style="background: linear-gradient(90deg, #ff3366 0%, #fe9b02 100%); color: white;">
    <div class="site-close"><i class="fa fa-times"></i></div>
    <div class="container p-lg-0">
        <div class="py-lg-2 row hero-row">
            <div class="hero-responsive col-12 text-left text-md-center align-middle mt-4 my-lg-2"> The President has signed a relief bill
                that includes $284 billion for
                Paycheck Protection Program (PPP)
                loans. Virtual Fund Assist will submit
                completed, eligible applications to
                PPP lenders once the program
                officially resumes in January 2021. </div>
        </div>
    </div>
</div>

<!-- ==== Page(PPP-Loan-forgivness) Main Start ==== -->
<div id="ppp-loan-forgiveness">
    <div class="hero" style="color:inherit;background: ; background: linear-gradient(-243.43494882292202deg, rgb(63, 7, 221) 0%, rgb(19, 156, 236) 100%); color: white;">
        <div class="container py-md-5">
            <div class="py-md-2 hero-row">
                <div class="hero-responsive col-12 text-left align-middle my-2">
                    <h2 class="text-center">PPP Loan Forgiveness</h2>
                    <h5 class="body-large text-center px-lg-5 py-0 px-3">Begin your PPP loan application through
                        Virtual Fund Assist to be matched with a PPP lender. Virtual Fund Assist is not a lender, and an application submitted
                        through Virtual Fund Assist does not guarantee you will receive a PPP loan or be matched to a lender. We
                        will accept applications throughout the program or until allocated funds for the program
                        have been exhausted.</h5>
                </div>
                <div class="d-none d-lg-block col-lg-6"></div>
            </div>
        </div>
    </div>
    <div id="alt-header-swap"></div>
    <div class="alt-dark py-5">
        <div class="container py-lg-0 py-5">
            <div class="row">
                <div class="col-12 col-md-6 py-5 text-center text-md-left">
                    <h2>Here&#8217;s everything you need to know about PPP loan forgiveness.</h2>
                    <p>We’re here to help you figure out what qualifies for loan forgiveness, what you need to
                        apply, and how to submit your application.</p>
                </div>
                <div class="col-12 text-center my-auto offset-md-1 col-md-5 pb-5 py-md-4">
                    <img width="700" height="448" src="" class="attachment-full size-full" alt="PPP Loan Forgiveness" sizes="(max-width: 700px) 100vw, 700px" data-wp-pid="93405" nopin="nopin" title="PPP Loan Forgiveness" />
                </div>
            </div>
        </div>
    </div>
    <div class="py-5 alt-white">
        <div class="container py-0 py-lg-5">
            <div class="row">
                <div class="col-2 col-md-1 offset-md-1 py-4">
                    <!-- <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" /> -->
                </div>
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2>What Is PPP Loan Forgiveness?</h2>
                    <p>Paycheck Protection Program (PPP) loans, created under the CARES Act, allow for potential
                        loan forgiveness on eligible costs incurred during a set 24-week period after a borrower’s
                        PPP funds are disbursed, defined under the CARES Act as the “covered period.”</p>
                </div>
                <div class="col-2 col-md-1 offset-md-1 py-4">
                    <!-- <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" /> -->
                </div>
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2>Eligible Costs for Loan Forgiveness</h2>
                    <p>According to SBA guidance <a href="https://home.treasury.gov/system/files/136/PPP-IFR-Loan-Forgiveness.pdf" data-wpel-link="external" target="_blank" rel="external noopener noreferrer">released on
                            May 20, 2020</a>, the following costs and expenses are eligible for loan forgiveness
                        under the Paycheck Protection Program. These costs must be paid or incurred during the
                        covered period.</p>
                    <ul>
                        <li>Payroll costs incurred during the covered period or alternative payroll covered period
                        </li>
                        <li>Mortgage interest payments on mortgages established prior to February 15, 2020 (Payments
                            toward the mortgage principal are not eligible. Advance payments on mortgages are also
                            not eligible.)</li>
                        <li>Business rent or lease payments for lease agreements established prior to February 15,
                            2020</li>
                        <li>Utility payments toward electricity, gas, water, transportation, telephone, or internet
                            access, as long as service began prior to February 15, 2020 (Advance payments on
                            utilities are not eligible.)</li>
                    </ul>
                </div>
                <div class="col-2 col-md-1 offset-md-1 py-4">
                    <!-- <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" /> -->
                </div>
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2>Payroll Changes That Affect PPP Loan Forgiveness</h2>
                    <p>If a borrower has reduced the number of employees employed by the business or nonprofit or if
                        salary has been reduced for employees, that will affect a borrower’s loan forgiveness
                        eligibility.</p>
                    <h3>What to Know if You Have Laid Off Employees</h3>
                    <p>Under the Paycheck Protection Program Flexibility Act (PPPFA), the deadline for rehiring all
                        full-time employees has been extended until December 31, 2020. The PPPFA also provided the
                        following exceptions for rehiring employees by that date:</p>
                    <ul>
                        <li>If your business/nonprofit was unable to rehire an individual included as an employee as
                            of February 15, 2020.</li>
                        <li>If your business/nonprofit was able to demonstrate that you were unable to hire
                            similarly qualified employees on or before February 15, 2020.</li>
                        <li>If your business/nonprofit was able to demonstrate an inability to return to the same
                            level of business activity at which your business was operating before February 15,
                            2020.</li>
                    </ul>
                    <p>The SBA has not released guidance on what will be required for a borrower to “demonstrate”
                        that their business or nonprofit was unable to hire a “similarly qualified employee” or that
                        they have been unable to return to the “same level of business activity” prior to February
                        15, 2020.</p>
                    <h3>What to Know if You’ve Reduced Employee Salary</h3>
                    <p>Reductions in employee salary may also affect your eligible forgiveness amount. The SBA has
                        indicated that a reduction of 25% or more in the annual salary for employees who make less
                        than $100k/year will reduce the loan forgiveness amount.</p>
                </div>
                <div class="col-2 col-md-1 offset-md-1 py-4">
                    <!-- <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" /> -->
                </div>
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2>Payroll Limits for PPP Loan Forgiveness</h2>
                    <p>When applying for a PPP loan, borrowers were only able to use a maximum annual salary of
                        $100k for each employee. The SBA has indicated similar restrictions will be put into place
                        for loan forgiveness. It is likely that a maximum payroll amount of $15k per employee will
                        be eligible for loan forgiveness.</p>
                    <p>That amount is calculated based on 8 weeks of pay for an employee with the $100k salary cap.
                        While the SBA has extended the length of time that borrowers may use the funds to 24 weeks
                        under the PPPFA, the 8-week payroll cap for forgiveness eligibility remains intact. If use
                        of the $15k cap is drawn out over 24 weeks due to a reduction in employee salary for
                        employees earning $100k and above, it may affect forgiveness eligibility. More clarity on
                        both of these points is needed from the SBA. We will update this page when they provide
                        further guidance.</p>
                </div>
                <div class="col-2 col-md-1 offset-md-1 py-4">
                    <!-- <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" /> -->
                </div>
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2>Do Payments for Payroll and Utilities Have to Be Made During the Covered Period to Count?
                    </h2>
                    <p>The short answer: not necessarily. The SBA has provided some wiggle room for borrowers,
                        allowing for payments made outside of the covered period to be eligible for forgiveness as
                        long as they’re incurred during the covered period.</p>
                    <p>For payroll expenses, costs are considered to be incurred the day the employee earns the pay.
                        So if the earnings are incurred during the covered period but not paid until after the
                        covered period, these costs may still be included in the application for forgiveness.</p>
                    <p>For eligible non-payroll expenses, you can include costs incurred during the covered period,
                        even if the next billing date is not within the covered period.</p>
                </div>
                <div class="col-2 col-md-1 offset-md-1 py-4">
                    <!-- <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" /> -->
                </div>
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2>How Much of Your PPP Loan Can Be Used for Non-Payroll Costs and Still Be Forgiven?</h2>
                    <p>Eligible non-payroll costs must be limited to 40% or less of the total forgiveness amount.
                    </p>
                </div>
                <div class="col-2 col-md-1 offset-md-1 py-4">
                    <!-- <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" /> -->
                </div>
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2>How to Apply for Loan Forgiveness</h2>
                    <p>Borrowers must apply for PPP loan forgiveness through their lender.</p>
                </div>
                <div class="col-2 col-md-1 offset-md-1 py-4">
                    <!-- <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" /> -->
                </div>
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2>Can You Apply for Loan Forgiveness Directly Through the SBA?</h2>
                    <p>You cannot. Lenders will be responsible for submitting forgiveness applications.</p>
                </div>
                <div class="col-2 col-md-1 offset-md-1 py-4">
                    <!-- <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" /> -->
                </div>
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2>What Happens if You’re Not Approved for Loan Forgiveness?</h2>
                    <p>If you’re not approved for loan forgiveness, your lender may request additional
                        documentation. Otherwise, you will be required to repay the loan. The outstanding balance
                        will continue to accrue interest at 1% over a 2-year or 5-year loan term. If you decide to
                        repay early, you can do so without incurring any early payment penalties or fees.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="py-5 alt-dark p-0 ">
        <div class="container py-0 py-lg-0">
            <div class="row ">
                <div class="col-12 ">
                    <p><img class="aligncenter wp-image-93400" src="../../wp-content/uploads/2020/06/Homepage%404x.png" alt="Man with application" width="500" height="462" data-wp-pid="93400" srcset="https://www.lendio.com/wp-content/uploads/2020/06/Homepage@4x.png 1722w, https://www.lendio.com/wp-content/uploads/2020/06/Homepage@4x-300x277.png 300w, https://www.lendio.com/wp-content/uploads/2020/06/Homepage@4x-1024x946.png 1024w, https://www.lendio.com/wp-content/uploads/2020/06/Homepage@4x-1536x1418.png 1536w, https://www.lendio.com/wp-content/uploads/2020/06/Homepage@4x-800x739.png 800w" sizes="(max-width: 500px) 100vw, 500px" /></p>
                    <h2>What You’ll Need for Your PPP Forgiveness Application</h2>
                    <p>Before you sit down to complete your application for loan forgiveness, you’ll want to compile
                        the following information:</p>
                    <h3>Identification Information</h3>
                    <ul>
                        <li>The name of your business: business legal name, DBA, tradename (if applicable)</li>
                        <li>Business Tax Identification Number (TIN): Social Security number (SSN) or Employer
                            Identification Number (EIN)</li>
                        <li>SBA PPP loan number</li>
                    </ul>
                    <h3>PPP Loan Information</h3>
                    <ul>
                        <li>Lender PPP loan number</li>
                        <li>Your PPP loan amount</li>
                    </ul>
                    <h3>Economic Injury and Disaster Loan (EIDL) Information</h3>
                    <ul>
                        <li>EIDL advance amount, if you received one</li>
                        <li>EIDL application number, if you applied</li>
                    </ul>
                    <h3>Payroll Information</h3>
                    <ul>
                        <li>Number of employees at the time of your PPP application</li>
                        <li>Number of current employees (at the time you’re applying for loan forgiveness)</li>
                        <li>PPP loan disbursement date, aka the date you received funds from your lender</li>
                        <li>Payroll schedule</li>
                        <li>Cost of covered non-payroll expenses</li>
                        <li>Supporting documentation for all</li>
                        <li>The covered period, aka the date range for the 24 weeks eligible for loan forgiveness
                            starting when you received your PPP loan disbursement or the 24-week range starting on
                            your first pay period following disbursement if you’re using the “alternative payroll
                            covered period,” which the SBA defines as the 168-day period beginning the first day of
                            the next payroll period after disbursement</li>
                    </ul>
                    <h3>Documentation for How the Eligible Funds Were Spent</h3>
                    <p>The SBA will require the following documentation as evidence that funds were spent on
                        payroll, mortgage interest payments, rent payments, and utility payments during the 24-week
                        covered period.</p>
                    <ul>
                        <li>Copies of canceled checks</li>
                        <li>Bank statements with ACH information</li>
                        <li>Utility bills</li>
                        <li>Mortgage statements</li>
                        <li>Lease agreements</li>
                        <li>List of all employees on payroll during the covered period</li>
                    </ul>
                    <h3>Documentation Required to Prove Employee Retention</h3>
                    <p>The SBA requires the following documentation be provided as evidence that the borrower has
                        met employee retention requirements:</p>
                    <ul>
                        <li>Evidence that employees were kept on payroll or rehired once the loan was received</li>
                        <li>A calculation of the average monthly number of full-time equivalent employees for the
                            period of February 12, 2019, to June 20, 2019, or January 1, 2020, to February 29, 2020.
                        </li>
                        <li>Evidence of pay restoration by December 31, 2020, for any employee whose salary was
                            reduced by 25% or more.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- ==== Page(PPP-Loan-forgivness) Blue-Bar-Bottom Start ==== -->
<div class="bottom-cta full base dark pt-2">
    <div class="container cta-bottom py-5">
        <div class="row">
            <div class="col-12 py-md-5 text-center">
                <h2><span class="d-none d-lg-block">Need additional funding to your SBA loan?<br></span>Get your
                    small<br class="d-lg-none d-md-none"> business loan<br class="d-lg-none d-md-none"> today.</h2>
                <form action="https://www.lendio.com/bp/basic-info" method="GET" class="cta-content d-inline-block my-4"> <input type="hidden" name="referral_url" value="https://www.google.com/" />
                    <div class="input-container col-lg-8 float-lg-left mb-3"> <span class="dollar d-none d-lg-block">$</span> <input type="text" placeholder="How much do you need?" class="amount-seeking-input text-center pl-0" name="amountSeeking" /></div>
                    <div class="button-container col-lg-4 float-lg-left"> <button class="primary-button w-lg-100 ml-0 mb-2">Start</button></div>
                    <div class="clear"></div>
                </form>
                <h5 class="px-5" style="font-size: 17px;">Applying is free and it won't impact your credit</h5>
                <h5 style="font-size: 17px;">Already have an account? &nbsp;<a class="text-uppercase font-weight-bold" href="../../bp/login.html" data-wpel-link="internal"><strong>Sign in</strong></a></h5>
            </div>
        </div>
    </div>
</div>

<!-- ==== Page(SBA-Payback-Programm-Loan) FOOTER-TOP Start ==== -->
<div class="contact-area border-top border-bottom py-5">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-7 d-flex text-center text-lg-left justify-content-center">
                <img src="images/blue-line-phone.svg" class="mr-md-3 mr-4 mb-3 mb-lg-0" alt="Phone Icon">
                <div class="hidden-sm-down pt-2"></div>
                <h3 class="float-lg-left">Give us a call <br class="d-inheret d-sm-none" /><a class="phone" href="tel:4692250569" data-wpel-link="internal">(469) 225-0569</a></h3>
            </div>
            <div class="col-12 col-md-5 text-center text-lg-left">
                <div class="hidden-sm-down pt-2"></div>
                <p class="hours">Monday - Friday | 9am - 9pm Central Standard Time</p>
            </div>
        </div>
    </div>
</div>

<?php include('../footer.php'); ?>
<?php include('../header.php'); ?>

<!-- ==== Page(SBA-CoronaVirus-Loans) yellow-bar Start ==== -->
<div class="hero position-relative" style="background: linear-gradient(90deg, #ff3366 0%, #fe9b02 100%); color: white;">
    <div class="site-close"><i class="fa fa-times"></i></div>
    <div class="container p-lg-0">
        <div class="py-lg-2 row hero-row">
            <div class="hero-responsive col-12 text-left text-md-center align-middle mt-4 my-lg-2"> The President has signed a relief bill
                that includes $284 billion for
                Paycheck Protection Program (PPP)
                loans. Virtual Fund Assist will submit
                completed, eligible applications to
                PPP lenders once the program
                officially resumes in January 2021. </div>
        </div>
    </div>
</div>

<!-- ==== Page(SBA-CoronaVirus-Loans) Main Start ==== -->
<div id="sba-coronavirus-loans">
    <div class="hero" style="color:inherit;background: ; background: linear-gradient(-243.43494882292202deg, rgb(63, 7, 221) 0%, rgb(19, 156, 236) 100%); color: white;">
        <div class="container py-md-5">
            <div class="py-md-2 hero-row">
                <div class="hero-responsive col-12 text-left align-middle my-2">
                    <h2 class="text-center">SBA Coronavirus Loans (COVID-19)</h2>
                    <h5 class="body-large text-center px-lg-5 py-0 px-3">Begin your PPP loan application through
                        Virtual Fund Assist to be matched with a PPP lender. Virtual Fund Assist is not a lender, and an application submitted
                        through Virtual Fund Assist does not guarantee you will receive a PPP loan or be matched to a lender. We
                        will accept applications throughout the program or until allocated funds for the program
                        have been exhausted.</h5>
                </div>
                <div class="d-none d-lg-block col-lg-6"></div>
            </div>
        </div>
    </div>
    <div id="alt-header-swap"></div>
    <div class="py-5 alt-grey">
        <div class="container my-5">
            <div class="col-12 mb-5"></div>
            <div class="row">
                <div id=" " class="col-12 col-md-6 d-md-block col-lg-6">
                    <div class="container-fluid">
                        <div class="row pb-4">
                            <div class="col-2"> <img src="<?= $home_url ?>/images/apply.svg" class="d-block pr-2" alt="Apply" style="min-width:55px;" height="52.26" width="52.01" data-wp-pid="89778" nopin="nopin" title="Explore America&#039;s Leading Business Loan Marketplace" /></div>
                            <div class="col-10">
                                <h3 class="pb-md-0" itemprop="name">Loan Uses</h3>
                                <div class="fact-text pt-md-0 pb-md-4 pr-3">
                                    <h5 class="pt-0" itemprop="value">
                                        <p>Loans can be used to &#8220;pay fixed debts, payroll, accounts payable,
                                            and other bills that can&#8217;t be paid because of the disaster&#8217;s
                                            impact.&#8221;</p>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="collapseFacts" class="col-12 col-md-6 d-md-block collapse multi-collapse col-lg-6">
                    <div class="container-fluid">
                        <div class="row pb-4">
                            <div class="col-2"> <img src="<?= $home_url ?>/images/dollar-icon.svg" class="d-block pr-2" alt="Loan Amount Icon" style="min-width:55px;" height="34" width="31" data-wp-pid="91618" nopin="nopin" title="American Express® Merchant Financing" /></div>
                            <div class="col-10">
                                <h3 class="pb-md-0" itemprop="name">Loan Amount</h3>
                                <div class="fact-text pt-md-0 pb-md-4 pr-3">
                                    <h5 class="pt-0" itemprop="value">
                                        <p>Up to $2 million through SBA&#8217;s Economic Injury and Disaster Loans.
                                            Loans over $25,000 will require collateral.</p>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id=" " class="col-12 col-md-6 d-md-block col-lg-6">
                    <div class="container-fluid">
                        <div class="row pb-4">
                            <div class="col-2"> <img src="<?= $home_url ?>/images/mortgage-blue.svg" class="d-block pr-2" alt="Commercial Mortgage Loan Calculator" style="min-width:55px;" height="51.12" width="52.05" data-wp-pid="90172" nopin="nopin" title="Commercial Mortgage Calculator" /></div>
                            <div class="col-10">
                                <h3 class="pb-md-0" itemprop="name">Eligible Businesses</h3>
                                <div class="fact-text pt-md-0 pb-md-4 pr-3">
                                    <h5 class="pt-0" itemprop="value">
                                        <p>Small businesses and private, nonprofit organizations in eligible all
                                            states and territories.</p>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3 col-12 col-md-6 text-left px-0 border-none d-md-none"> <a id="a-tag" class="justify-content-center d-block text-center font-weight-bold" data-toggle="collapse" href="#collapseFacts" role="button" aria-expanded="false" aria-controls="collapseFacts">
                        <h4 id="collapseFactsLink" class="font-weight-bold p-0 m-0">See All</h4> <svg id="collapse-arrow" class="bi bi-chevron-down" width="25" height="25" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M3.646 6.646a.5.5 0 01.708 0L10 12.293l5.646-5.647a.5.5 0 01.708.708l-6 6a.5.5 0 01-.708 0l-6-6a.5.5 0 010-.708z" clip-rule="evenodd"></path>
                        </svg>
                    </a></div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#a-tag').on('click', function() {
                var text = $('#collapseFactsLink').text();
                if (text === "See All") {
                    $('#collapseFactsLink').html('See less');
                    $('#collapse-arrow').addClass('d-none');
                } else {
                    $('#collapseFactsLink').text('See All');
                    $('#collapse-arrow').removeClass('d-none');


                }
            });


        });
    </script>
    <div class="py-5 alt-white">
        <div class="container py-0 py-lg-5">
            <div class="row">
                <div class="col-2 col-md-1 offset-md-1 py-4"> <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" /></div>
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2 style="text-align: left;"><span style="font-weight: 400;">What are SBA Coronavirus
                            Loans?</span></h2>
                    <p>To combat the novel coronavirus pandemic, the US government passed The Coronavirus Aid,
                        Relief, and Economic Security Act (CARES Act), a historic <a href="../../blog/coronavirus/relief-bill-small-businesses/index.html" data-wpel-link="internal">$2 trillion stimulus bill</a> to support individuals and small
                        businesses.</p>
                    <p><a href="../../blog/coronavirus/cares-act-commonly-asked-questions-answers/index.html" data-wpel-link="internal">The CARES Act</a> created a new type of SBA loan, the <a href="../../paycheck-protection-program-loans/index.html" data-wpel-link="internal">Paycheck Protection Program</a>, in addition to bolstering
                        existing programs to support small businesses and private nonprofits through the economic
                        downturn caused by the spread of the novel coronavirus.</p>
                    <p>Small businesses with fewer than 500 employees (with some exceptions for larger companies
                        like hotels and restaurants) and private nonprofits can apply for SBA loans through 4
                        programs:</p>
                    <ul>
                        <li><a href="../../paycheck-protection-program-loans/index.html" data-wpel-link="internal">Paycheck Protection Program (PPP)</a></li>
                        <li><a href="../economic-injury-disaster-loans-eidl/index.html" data-wpel-link="internal">Economic Injury Disaster Loan (EIDL)</a>: currently
                            limited to the agriculture industry</li>
                        <li>SBA Debt Relief</li>
                        <li>SBA Express Bridge Loan</li>
                    </ul>
                </div>
                <div class="col-2 col-md-1 offset-md-1 py-4"> <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" /></div>
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2 style="text-align: left;"><span style="font-weight: 400;">The Paycheck Protection Program
                            (PPP)</span></h2>
                    <p>PPP loans were established by the CARES Act to incentivize small businesses to maintain
                        employees and lessen their economic burden through the COVID-19 outbreak. If used for
                        approved purposes, a portion (or all) of the principal loan amount may be forgivable.</p>
                    <p>Congress has allocated $600 billion to fund PPP loans. Small businesses can qualify for loans
                        up to $10 million, and many typical SBA loan requirements do not apply to PPP loans, making
                        them more accessible to a larger number of businesses, including sole proprietors and
                        independent contractors.</p>
                    <h3>Who Is Eligible for a PPP Loan?</h3>
                    <p>In order to qualify for a PPP loan, your business must certify that you have suffered
                        economic losses due to the COVID-19 pandemic. The business or nonprofit must have been in
                        operation as of February 15, 2020, and must have fewer than 500 employees.</p>
                    <p>PPP loans do not require collateral or guarantees, which makes them accessible to a larger
                        number of small businesses. Because you don’t have to meet many of the other eligibility
                        requirements for SBA loans (time in business, revenue, etc.), many newer businesses will
                        qualify.</p>
                    <h3>What Can You Use a PPP Loan for?</h3>
                    <p>PPP loans can be used for “payroll support,” which includes items like:</p>
                    <ul>
                        <li>Employee salaries</li>
                        <li>Paid sick or medical leave</li>
                        <li>Insurance premiums</li>
                        <li>Mortgage/rent and utility payments incurred during the covered period</li>
                    </ul>
                    <h3 style="text-align: left;"><span style="font-weight: 400;">How Much Can You Get with a PPP
                            loan?</span></h3>
                    <p><span style="font-weight: 400;">Loan amounts for PPP loans are 2.5 times your business’s
                            average monthly payroll costs, up to $10 million. This includes the sum of payments
                            like:</span></p>
                    <ul>
                        <li style="font-weight: 400;"><span style="font-weight: 400;">Salary, wage, commission, or
                                similar compensation</span></li>
                        <li style="font-weight: 400;"><span style="font-weight: 400;">Cash tips or
                                equivalents</span></li>
                        <li style="font-weight: 400;"><span style="font-weight: 400;">Vacation, parental, family,
                                medical, or sick leave</span></li>
                        <li style="font-weight: 400;"><span style="font-weight: 400;">Allowance for dismissal or
                                separation</span></li>
                        <li style="font-weight: 400;"><span style="font-weight: 400;">Group healthcare benefits,
                                including insurance premiums</span></li>
                        <li style="font-weight: 400;"><span style="font-weight: 400;">Retirement benefits</span>
                        </li>
                        <li style="font-weight: 400;"><span style="font-weight: 400;">State or local taxes on the
                                compensation</span></li>
                    </ul>
                    <h3 style="text-align: left;"><span style="font-weight: 400;">What Can Be Forgiven Under the
                            Loan?</span></h3>
                    <p>The total sum of payroll costs, mortgage interest payments, rent, and utilities incurred or
                        paid by the borrower in the first 24 weeks of the loan term, beginning on the origination
                        date. 60% of the loan must be used toward payroll for the loan to be eligible for
                        forgiveness.</p>
                    <h3 style="text-align: left;"><span style="font-weight: 400;">How Layoffs Affect Loan
                            Forgiveness</span></h3>
                    <p><span style="font-weight: 400;">Loan forgiveness is designed to incentivize businesses to
                            keep on employees, hence why they’re called payroll protection plan loans. The amount
                            forgiven will change if you have laid off employees or reduced individual pay beyond 25%
                            for any employee between February 15, 2020, and June 30, 2020. Important to note:
                            furloughed employees are included in this provision.  </span></p>
                    <h3 style="text-align: left;"><span style="font-weight: 400;">What If You’ve Already Laid Off
                            Employees?</span></h3>
                    <p><span style="font-weight: 400;">If you laid off employees due to coronavirus between February
                            15, 2020, and April 26, 2020, and you rehire them before June 30, 2020, those changes
                            will not be counted. </span></p>
                    <h3 style="text-align: left;"><span style="font-weight: 400;">Are You Taxed on the Forgiven
                            Amount?</span></h3>
                    <p><span style="font-weight: 400;">Any portion of the loan that is forgiven is excluded from
                            taxable income.</span></p>
                </div>
                <div class="col-2 col-md-1 offset-md-1 py-4"> <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" /></div>
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2 style="text-align: left;"><span style="font-weight: 400;">Economic Injury and Disaster Loan
                            (EIDL) and Loan Advance</span></h2>
                    <p><a href="../economic-injury-disaster-loans-eidl/index.html" data-wpel-link="internal">EIDLs</a> are the traditional disaster-relief SBA loans. They
                        are similar to traditional SBA loans (like the SBA 7(a) loan) and follow a more traditional
                        SBA loan process—that means higher requirements and a longer wait period before receiving
                        funds.<br /> Due to an overwhelming number of EIDL applications, the SBA has limited new
                        applications to the agriculture industry.</p>
                    <h3>What Are the Requirements for an EIDL?</h3>
                    <p>Small businesses in agriculture may still apply for EIDLs. In addition to that limitation,
                        borrowers must be able to demonstrate “substantial economic injury” caused by the COVID-19
                        pandemic. The SBA considers “substantial economic injury” to mean that a business is unable
                        to meet its financial obligations or pay ordinary, essential operating expenses. Vitally,
                        this economic hardship must be a direct result of the disaster—in this case, COVID-19. So if
                        your business was struggling prior to coronavirus, you would be unlikely to qualify for an
                        EIDL.</p>
                </div>
                <div class="col-2 col-md-1 offset-md-1 py-4"> <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" /></div>
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2 style="text-align: left;"><span style="font-weight: 400;">SBA Debt Relief</span></h2>
                    <p><span style="font-weight: 400;">The SBA Debt Relief program provides financial support
                            through the SBA 7(a) program.</span></p>
                    <h3 style="text-align: left;"><span style="font-weight: 400;">How Does SBA Debt Relief
                            Work?</span></h3>
                    <p><span style="font-weight: 400;">The SBA will pay the principal and interest on new SBA 7(a)
                            loans issued before September 25, 2020. For existing SBA 7(a) loans, the SBA will pay
                            the principal and interest for a 6-month period. SBA Debt Relief is limited to the SBA
                            7(a) program and does not apply to PPP loans or EIDL. </span></p>
                    <h3 style="text-align: left;"><span style="font-weight: 400;">Who Qualifies for SBA Debt
                            Relief?</span></h3>
                    <p><span style="font-weight: 400;">Borrowers who qualify for an SBA 7(a) loan or have an
                            existing SBA 7(a) loan may qualify for SBA Debt Relief. This program maintains the
                            existing loan requirements for SBA 7(a) loans, so it will be determined by time in
                            business, revenue, credit score, and other factors, and may be highly
                            competitive. </span></p>
                </div>
                <div class="col-2 col-md-1 offset-md-1 py-4"> <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" /></div>
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2 style="text-align: left;"><span style="font-weight: 400;">SBA Express Bridge Loans</span>
                    </h2>
                    <p><span style="font-weight: 400;">The SBA is working hard to provide fast access to capital
                            (not usually its strong suit) to ease the burden on small businesses. SBA Express Bridge
                            Loans provide access to quicker SBA capital with less paperwork for eligible
                            borrowers. </span></p>
                    <h3 style="text-align: left;"><span style="font-weight: 400;">Who is Eligible for an SBA Express
                            Bridge Loan?</span></h3>
                    <p><span style="font-weight: 400;">Borrowers who have an existing relationship with an SBA
                            Express Lender. Small businesses that have an urgent need for capital while waiting for
                            a decision and funding on their EIDL may qualify for an SBA Express Disaster Bridge
                            Loan. </span></p>
                    <h3 style="text-align: left;"><span style="font-weight: 400;">How Much Can You Get with an SBA
                            Express Bridge Loan?</span></h3>
                    <p><span style="font-weight: 400;">Up to $25,000. </span></p>
                </div>
                <div class="col-2 col-md-1 offset-md-1 py-4"> <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" /></div>
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2 style="text-align: left;"><span style="font-weight: 400;">What if You Don’t Qualify or Can’t
                            Wait for an SBA Coronavirus (COVID-19) Loan?</span></h2>
                    <p><span style="font-weight: 400;">You can apply for </span><a href="../../small-business-loans/index.html" data-wpel-link="internal"><span style="font-weight: 400;">other types of financing</span></a><span style="font-weight: 400;"> to give your business a financial safety net or take
                            advantage of national, local, and </span><a href="../../blog/small-business-tools/relief-resources-small-businesses/index.html" data-wpel-link="internal"><span style="font-weight: 400;">corporate-sponsored relief
                                resources</span></a><span style="font-weight: 400;">. </span></p>
                </div>
            </div>
        </div>
    </div>
    <div class="alt-white text-center py-5 border-top">
        <div class="container py-0 py-lg-5">
            <div class="row px-2">
                <div class="col-12">
                    <h4 style="text-align: center;">Virtual Fund Assist had an incredibly easy and straightforward application
                        for the Paycheck Protection Program. It took less than 15 minutes. Devin walked me through
                        it. It could not have been easier!</h4>
                    <p style="text-align: center;">&#8211; Jonathan O.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row my-5">
            <div class="col-12 mb-4">
                <h2 class="text-left">More about SBA Disaster Relief Loans</h2>
            </div>
            <div class="lower-post col-12 col-md-4"> <a href="../../blog/small-business-tools/mistakes-small-businesses-economic-crisis/index.html" class="image-link" data-wpel-link="internal">
                    <div class="post-image mb-4" style="background-image:url('<?= $home_url ?>/images/business-owner-loan-application.jpg')">
                    </div>
                </a>
                <p class="post-time">Mar 26, 2020</p>
                <h2 class="post-title"><a href="../../blog/small-business-tools/mistakes-small-businesses-economic-crisis/index.html" data-wpel-link="internal">10 Mistakes Small Businesses Make During an Economic Crisis</a>
                </h2>
            </div>
            <div class="lower-post col-12 col-md-4"> <a href="../../blog/coronavirus/ideas-keeping-doors-open-coronavirus/index.html" class="image-link" data-wpel-link="internal">
                    <div class="post-image mb-4" style="background-image:url('<?= $home_url ?>/images/istock-14.jpg')">
                    </div>
                </a>
                <p class="post-time">Mar 31, 2020</p>
                <h2 class="post-title"><a href="../../blog/coronavirus/ideas-keeping-doors-open-coronavirus/index.html" data-wpel-link="internal">3 Ideas for Keeping Your Doors Open During Coronavirus</a></h2>
            </div>
            <div class="lower-post col-12 col-md-4"> <a href="../../blog/coronavirus/complications-small-business-coronavirus/index.html" class="image-link" data-wpel-link="internal">
                    <div class="post-image mb-4" style="background-image:url('<?= $home_url ?>/images/coronavirus-tape.jpg')">
                    </div>
                </a>
                <p class="post-time">Mar 21, 2020</p>
                <h2 class="post-title"><a href="../../blog/coronavirus/complications-small-business-coronavirus/index.html" data-wpel-link="internal">4 Complications Your Small Business May Face Due to
                        Coronavirus</a></h2>
            </div>
        </div>
    </div>
</div>

<!-- ==== Page(SBA-CoronaVirus-Loans) Blue-Bottom-Bar Start ==== -->
<div class="bottom-cta full base dark pt-2">
    <div class="container cta-bottom py-5">
        <div class="row">
            <div class="col-12 py-md-5 text-center">
                <h2><span class="d-none d-lg-block">Need additional funding to your SBA loan?<br></span>Get your
                    small<br class="d-lg-none d-md-none"> business loan<br class="d-lg-none d-md-none"> today.</h2>
                <form action="https://virtualfundassist.com/lendio/tabs/basic-info" method="GET" class="cta-content d-inline-block my-4"> <input type="hidden" name="referral_url" value="https://www.google.com/" />
                    <div class="input-container col-lg-8 float-lg-left mb-3"> <span class="dollar d-none d-lg-block">$</span> <input type="text" placeholder="How much do you need?" class="amount-seeking-input text-center pl-0" name="amountSeeking" /></div>
                    <div class="button-container col-lg-4 float-lg-left"> <button class="primary-button w-lg-100 ml-0 mb-2">Start</button></div>
                    <div class="clear"></div>
                </form>
                <h5 class="px-5" style="font-size: 17px;">Applying is free and it won't impact your credit</h5>
                <h5 style="font-size: 17px;">Already have an account? &nbsp;<a class="text-uppercase font-weight-bold" href="../../bp/login.html" data-wpel-link="internal"><strong>Sign in</strong></a></h5>
            </div>
        </div>
    </div>
</div>

<!-- ==== Page(SBA-Payback-Programm-Loan) FOOTER-TOP Start ==== -->
<div class="contact-area border-top border-bottom py-5">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-7 d-flex text-center text-lg-left justify-content-center">
                <img src="images/blue-line-phone.svg" class="mr-md-3 mr-4 mb-3 mb-lg-0" alt="Phone Icon">
                <div class="hidden-sm-down pt-2"></div>
                <h3 class="float-lg-left">Give us a call <br class="d-inheret d-sm-none" /><a class="phone" href="tel:4692250569" data-wpel-link="internal">(469) 225-0569</a></h3>
            </div>
            <div class="col-12 col-md-5 text-center text-lg-left">
                <div class="hidden-sm-down pt-2"></div>
                <p class="hours">Monday - Friday | 9am - 9pm Central Standard Time</p>
            </div>
        </div>
    </div>
</div>

<?php include('../footer.php'); ?>
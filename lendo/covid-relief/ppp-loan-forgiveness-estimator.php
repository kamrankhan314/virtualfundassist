<?php include('../header.php'); ?>

<!-- ==== Page(PPP-Loan-forgivness) yellow-bar Start ==== -->
<div class="hero position-relative pt-2 pb-3" style="background: linear-gradient(90deg, #ff3366 0%, #fe9b02 100%); color: white;">
    <div class="site-close"><i class="fa fa-times"></i></div>
    <div class="container p-lg-0 px-4 pr-5">
        <div class="py-lg-2 row hero-row">
            <div class="hero-responsive col-12 text-left text-md-center align-middle mt-2 my-lg-2 covid-disclaimer-banner">
                The Paycheck Protection Program ended on August 8, 2020. We will continue to accept applications in
                hopes that Congress passes an extension, although we are not currently sending applications to
                lenders. If PPP resumes, Virtual Fund Assist will submit your application to a lender.</div>
        </div>
    </div>
</div>

<!-- ==== Page(PPP-Loan-forgivness) Main Start ==== -->
<div id="business-calculators-page" class="container px-0 forgiveness-calc">
    <div class="row all-calcs-link">
        <div class="col-12 text-left"> <a href="/business-calculators" data-wpel-link="internal"><i class="fa fa-arrow-left"></i> All Calculators</a></div>
    </div>
    <div class="row">
        <div class="col-12 px-4 px-lg-3 pt-4">
            <h1>PPP Loan Forgiveness Estimator</h1>
            <p class="pb-0 mb-0">Get an estimate of how much of your Paycheck Protection Program loan will be
                forgiven.</p>
        </div>
    </div>
    <div id="alt-header-swap"></div>
    <script type="text/javascript">
        (function loadCalc() {
            var c = document.createElement('loan-calculator');
            c.setAttribute('type', '');
            c.setAttribute('calc-type', 'loanForgiveness');
            var currentScript = document.currentScript || (function() {
                var scripts = document.getElementsByTagName('script');
                return scripts[scripts.length - 1];
            })();

            currentScript.parentNode.appendChild(c);
            var s = document.createElement('script');
            s.src = "https://www.lendio.com/calculators/vue/calc-component.js";
            s.type = "text/javascript"
            s.async = false;
            document.getElementsByTagName('head')[0].appendChild(s);
        })();
    </script>
    <div class="row py-lg-5">
        <div class="col-12 col-lg-9 ml-lg-auto pl-0 pr-lg-2 pr-0 py-lg-5">
            <div class="alt-white calculator-faqs">
                <div class="container my-5">
                    <div class="row px-3">
                        <div class="col-12 text-left faq-content">
                            <h2>Factors That Affect PPP Loan Forgiveness</h2>
                            <p>PPP loan forgiveness is determined through calculations with several factors, each of
                                which is outlined below.</p>
                        </div>
                        <div class="col-12 col-md-auto value-block border-none py-2 faqs">
                            <div class="d-inline p-0 faq-text justify-content-center">
                                <h3>PPP Loan Amount</h3>
                                <p class="p-0 m-0">
                                <p>The PPP loan amount is the total amount awarded to a borrower for their PPP loan.
                                    That amount was calculated based on 2.5 times a borrower’s average monthly
                                    payroll cost during the covered period.</p>
                                </p>
                            </div>
                        </div>
                        <div class="col-12 col-md-auto value-block border-none py-2 faqs">
                            <div class="d-inline p-0 faq-text justify-content-center">
                                <h3>Payroll Costs</h3>
                                <p class="p-0 m-0">
                                <p>Payroll costs for the loan forgiveness calculations are the portion of the PPP
                                    loan amount used to cover payroll costs during the 24-week covered period
                                    following disbursement of the loan.</p>
                                </p>
                            </div>
                        </div>
                        <div class="col-12 col-md-auto value-block border-none py-2 faqs">
                            <div class="d-inline p-0 faq-text justify-content-center">
                                <h3>Mortgage Interest, Rent/Lease, and Utility Payments</h3>
                                <p class="p-0 m-0">
                                <p>Following SBA guidance on allowed uses of the loan, no more than 40% of the PPP
                                    loan amount can be used toward mortgage interest, rent/lease, and utility costs.
                                    Payments toward mortgage interest, rent or lease payments, and utility payments
                                    incurred during the 24-week covered period can be included in PPP loan
                                    forgiveness in these fields. Payments toward a mortgage principal are ineligible
                                    and should not be included.</p>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="alt-white border-top calculator-faqs">
                <div class="container my-5">
                    <div class="row px-3">
                        <div class="col-12 text-left faq-content">
                            <h2>How Is the Loan Forgiveness Amount Decided?</h2>
                            <p>Your PPP loan forgiveness amount will be awarded based on one of 3 calculations: the
                                modified total, the payroll cost 60% requirement, or the total PPP loan amount.
                                According to SBA guidance, the least of these 3 calculations should be used as the
                                loan forgiveness amount.</p>
                        </div>
                        <div class="col-12 col-md-auto value-block border-none py-2 faqs">
                            <div class="d-inline p-0 faq-text justify-content-center">
                                <h3>When the Modified Total Is Used as the PPP Forgiveness Amount</h3>
                                <p class="p-0 m-0">
                                <p>The modified total will be used as a borrower’s PPP loan forgiveness amount:</p>
                                <ul>
                                    <li>If a borrower has reduced the number of full-time equivalent employees and
                                        fails to rehire them before the deadline or qualify for an allowed
                                        exemption. <i>Full details on exemptions can be found on our </i><a href="https://www.lendio.com/covid-relief/ppp-loan-forgiveness/" data-wpel-link="internal"><i>Loan Forgiveness</i></a><i> page</i>.</li>
                                    <li>If a borrower has reduced salary and hourly wages by at least 25%.</li>
                                </ul>
                                </p>
                            </div>
                        </div>
                        <div class="col-12 col-md-auto value-block border-none py-2 faqs">
                            <div class="d-inline p-0 faq-text justify-content-center">
                                <h3>When the 60% Payroll Requirement Is Used as the PPP Forgiveness Amount</h3>
                                <p class="p-0 m-0">
                                <p>For the 60% payroll requirement to be used as the loan forgiveness amount, a
                                    borrower must meet the following requirements:</p>
                                <ul>
                                    <li>The average number of full-time equivalent employees during the covered
                                        period matches the average number of full-time equivalent during the
                                        reference period. <i>For full details on requirements for rehiring and
                                            potential exemptions if a borrower is unable to rehire employees, please
                                            refer to our </i><a href="https://www.lendio.com/covid-relief/ppp-loan-forgiveness/" data-wpel-link="internal"><i>Loan Forgiveness</i></a><i> page. </i></li>
                                    <li>At least 60% of the loan amount was used on payroll costs during the covered
                                        period.</li>
                                </ul>
                                </p>
                            </div>
                        </div>
                        <div class="col-12 col-md-auto value-block border-none py-2 faqs">
                            <div class="d-inline p-0 faq-text justify-content-center">
                                <h3>When the PPP Loan Amount Is Used as the PPP Forgiveness Amount</h3>
                                <p class="p-0 m-0">
                                <p>Based on guidance from the SBA, it is anticipated that full forgiveness of the
                                    PPP loan amount will be uncommon. If a borrower chose to take out a smaller loan
                                    amount that would only cover their payroll costs during the covered period and
                                    <i>not</i> mortgage interest, rent/lease payments, or utility payments, the
                                    borrower may be eligible for forgiveness of the total PPP loan amount.
                                </p>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="alt-white border-top calculator-faqs">
                <div class="container my-5">
                    <div class="row px-3">
                        <div class="col-12 text-left faq-content">
                            <h2>PPP Loan Forgiveness FAQs</h2>
                            <p>Let’s review some common questions about terminology and requirements regarding PPP
                                loan forgiveness.</p>
                        </div>
                        <div class="col-12 col-md-auto value-block border-none py-2 faqs">
                            <div class="d-inline p-0 faq-text justify-content-center">
                                <h3>What does the ‘covered period’ mean?</h3>
                                <p class="p-0 m-0">
                                <p>Due to an extension under the Paycheck Protection Program Flexibility Act (PPFA),
                                    the covered period refers to the 24-week period following disbursement of the
                                    loan or December 31, 2020, whichever date comes first.</p>
                                <p>Borrowers who received a loan prior to June 5, 2020, may opt to use the
                                    “alternative covered period,” meaning the borrower can opt to use an 8-week
                                    period instead of the 24-week period. This 8-week period can begin either on the
                                    date the borrower receives the loan or on the first day of the pay period
                                    immediately following the receipt of the PPP loan.</p>
                                </p>
                            </div>
                        </div>
                        <div class="col-12 col-md-auto value-block border-none py-2 faqs">
                            <div class="d-inline p-0 faq-text justify-content-center">
                                <h3>What does ‘full-time equivalent employees’ mean?</h3>
                                <p class="p-0 m-0">
                                <p>Full-time equivalent employees (FTEs) is a calculation based on the hours worked
                                    for both full-time and part-time employees over a workweek. If an employer has a
                                    40-hour workweek, an employee that works 40 hours during that week is 1.0
                                    full-time equivalent employee.</p>
                                <p>There are 2 methods for calculating an individual who has worked less than
                                    full-time:</p>
                                <ul>
                                    <li>An employer can take the number of average hours worked by an employee
                                        during the covered period and divide the number of hours by 40. For example,
                                        an employee who works 30 hours per week during the covered period would
                                        equal .75. An employee who worked 20 hours would be calculated as 0.5, and
                                        an employee who works 10 hours would be calculated as 0.25.</li>
                                    <li>An employer can simply assign each employee who works less than full time as
                                        a .5,</li>
                                </ul>
                                </p>
                            </div>
                        </div>
                        <div class="col-12 col-md-auto value-block border-none py-2 faqs">
                            <div class="d-inline p-0 faq-text justify-content-center">
                                <h3>What is the reference period?</h3>
                                <p class="p-0 m-0">
                                <p>The reference period refers to the time period used to calculate a borrower’s PPP
                                    loan amount. The reference period is a period prior to receipt of the PPP loan
                                    used as a baseline in calculating the number of FTE a business may have.</p>
                                <p>The majority of businesses have 2 options; February 15, 2019 – June 30, 2019 OR
                                    January and February 2020. If you have a seasonal business, you get a third
                                    option—any consecutive 12-week period from May 1, 2019 – September 15, 2019.</p>
                                </p>
                            </div>
                        </div>
                        <div class="col-12 col-md-auto value-block border-none py-2 faqs">
                            <div class="d-inline p-0 faq-text justify-content-center">
                                <h3>What is the 60% payroll requirement?</h3>
                                <p class="p-0 m-0">
                                <p>60% of the PPP loan amount must be used toward payroll costs. No more than 40% of
                                    the forgiven amount can be used for non-payroll costs like mortgage interest,
                                    rent, or utilities. This requirement was originally 75% and was reduced to 60%
                                    under the PPPFA.</p>
                                </p>
                            </div>
                        </div>
                        <div class="col-12 col-md-auto value-block border-none py-2 faqs">
                            <div class="d-inline p-0 faq-text justify-content-center">
                                <h3>Will the full amount of the loan be forgiven?</h3>
                                <p class="p-0 m-0">
                                <p>For most borrowers, a portion of their PPP loan amount will be forgiven. In some
                                    cases, the full PPP loan amount <i>may</i> be forgiven. Borrowers should not
                                    anticipate full forgiveness of the PPP loan amount.</p>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wpsso-pinterest-pin-it-image" style="display:none !important;"> <img src="https://www.lendio.com/wp-content/uploads/2018/03/Wordpress-Lendio-Logo_blue.jpg" width="0" height="0" class="skip-lazy" style="width:0;height:0;" alt="" data-pin-description="Get an estimate of how much of your Paycheck Protection Program loan will be forgiven." />
            </div>
        </div>
        <div class="sidebar col-12 col-md-3 py-3 py-md-5 pl-md-3 pr-md-0">
            <div class="banner-block-top"></div>
            <div class="shadow">
                <div class="p-4 mb-5"> <img width="2560" height="1920" src="https://www.lendio.com/wp-content/uploads/2020/05/iStock-1146369554-scaled.jpg" class="mb-4 wp-post-image" alt="Minority female small business owner reviewing their business expenses" srcset="https://www.lendio.com/wp-content/uploads/2020/05/iStock-1146369554-scaled.jpg 2560w, https://www.lendio.com/wp-content/uploads/2020/05/iStock-1146369554-300x225.jpg 300w, https://www.lendio.com/wp-content/uploads/2020/05/iStock-1146369554-1024x768.jpg 1024w, https://www.lendio.com/wp-content/uploads/2020/05/iStock-1146369554-1536x1152.jpg 1536w, https://www.lendio.com/wp-content/uploads/2020/05/iStock-1146369554-2048x1536.jpg 2048w, https://www.lendio.com/wp-content/uploads/2020/05/iStock-1146369554-800x600.jpg 800w" sizes="(max-width: 2560px) 100vw, 2560px" data-wp-pid="93086" nopin="nopin" title="How to Track Expenses for PPP Loan Forgiveness" />
                    <h3>How to Track Expenses for PPP Loan Forgiveness</h3>
                    <p>If you applied for and received a Paycheck Protection Program (PPP) loan, you’re likely very
                        aware of the uses, expectations,&hellip;</p> <a href="https://www.lendio.com/?post_type=post&amp;p=93085" data-wpel-link="internal"><strong>Read article <i class="fa fa-arrow-right m-0"></i></strong></a>
                </div>
            </div>
            <div class="banner-block-top"></div>
            <div class="shadow">
                <div class="p-4 mb-5"> <img width="2368" height="1266" src="https://www.lendio.com/wp-content/uploads/2020/04/iStock-1167579980.jpg" class="mb-4 wp-post-image" alt="Female small business owner filling out paperwork" srcset="https://www.lendio.com/wp-content/uploads/2020/04/iStock-1167579980.jpg 2368w, https://www.lendio.com/wp-content/uploads/2020/04/iStock-1167579980-300x160.jpg 300w, https://www.lendio.com/wp-content/uploads/2020/04/iStock-1167579980-1024x547.jpg 1024w, https://www.lendio.com/wp-content/uploads/2020/04/iStock-1167579980-1536x821.jpg 1536w, https://www.lendio.com/wp-content/uploads/2020/04/iStock-1167579980-2048x1095.jpg 2048w, https://www.lendio.com/wp-content/uploads/2020/04/iStock-1167579980-800x428.jpg 800w" sizes="(max-width: 2368px) 100vw, 2368px" data-wp-pid="92787" nopin="nopin" title="What Documents Will Be Needed to Apply for PPP Loan Forgiveness?" />
                    <h3>What Documents Will Be Needed to Apply for PPP Loan Forgiveness?</h3>
                    <p>When the economy took a dramatic downturn as a result of the coronavirus (COVID-19) pandemic,
                        the government stepped in and&hellip;</p> <a href="https://www.lendio.com/?post_type=post&amp;p=92786" data-wpel-link="internal"><strong>Read article <i class="fa fa-arrow-right m-0"></i></strong></a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- ==== Page(PPP-Loan-forgivness) Blue-Bottom-bar Start ==== -->
<div class="bottom-cta pt-2 full base dark">
    <div class="container cta-bottom py-5">
        <div class="row">
            <div class="col-12 py-md-5 text-center">
                <h2>Get your small business loan today.</h2>
                <form action="/bp/basic-info" method="GET" class="cta-content d-inline-block my-4">
                    <div class="input-container col-lg-8 float-lg-left mb-3"> <span class="dollar d-none d-lg-block">$</span> <input type="text" placeholder="How much do you need?" class="amount-seeking-input text-center pl-0" name="amountSeeking" /></div>
                    <div class="button-container col-lg-4 float-lg-left"> <button class="primary-button w-lg-100 ml-0 mb-2">Start</button></div>
                    <div class="clear"></div>
                </form>
                <h5 class="px-5" style="font-size: 17px;">Applying is free and it won't impact your credit</h5>
                <h5 style="font-size: 17px;">Already have an account? &nbsp;<a class="text-uppercase font-weight-bold" href="/bp/login" data-wpel-link="internal"><strong>Sign in</strong></a></h5>
            </div>
        </div>
    </div>
</div>

<!-- ==== Page(SBA-Payback-Programm-Loan) FOOTER-TOP Start ==== -->
<div class="contact-area border-top border-bottom py-5">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-7 d-flex text-center text-lg-left justify-content-center">
                <img src="../images/blue-line-phone.svg" class="mr-md-3 mr-4 mb-3 mb-lg-0" alt="Phone Icon">
                <div class="hidden-sm-down pt-2"></div>
                <h3 class="float-lg-left">Give us a call <br class="d-inheret d-sm-none" /><a class="phone" href="tel:4692250569" data-wpel-link="internal">(469) 225-0569</a></h3>
            </div>
            <div class="col-12 col-md-5 text-center text-lg-left">
                <div class="hidden-sm-down pt-2"></div>
                <p class="hours">Monday - Friday | 9am - 9pm Central Standard Time</p>
            </div>
        </div>
    </div>
</div>

<?php include('../footer.php'); ?>
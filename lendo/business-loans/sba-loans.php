<?php include('../header.php'); ?>

     <!-- ==== Page(SBS Loans) Main Start ==== -->
    <div class="hero position-relative"
        style="background: linear-gradient(90deg, #ff3366 0%, #fe9b02 100%); color: white;">
        <div class="site-close"><i class="fa fa-times"></i></div>
        <div class="container p-lg-0">
            <div class="py-lg-2 row hero-row">
                <div class="hero-responsive col-12 text-left text-md-center align-middle mt-4 my-lg-2"> 
                The President has signed a relief bill
                that includes $284 billion for
                Paycheck Protection Program (PPP)
                loans. Virtual Fund Assist will submit
                completed, eligible applications to
                PPP lenders once the program
                officially resumes in January 2021.
                </div>
            </div>
        </div>
    </div>
    <div id="sba-loans">
        <div class="hero pt-md-5"
            style="color:#fff;background: #4000dc;background: -moz-linear-gradient(-45deg, #4000dc 0%, #05c2f0 100%);background: -webkit-linear-gradient(-45deg, #4000dc 0%,#05c2f0 100%);background: linear-gradient(135deg, #4000dc 0%,#05c2f0 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#4000dc', endColorstr='#05c2f0',GradientType=1 );">
            <div class="container py-md-5">
                <div class="row py-md-2 hero-row">
                    <div class="hero-responsive col-lg-6 text-left align-middle my-2"> 
                        <img src="<?= $home_url ?>/images/sba-loan-icon.svg" class="attachment-60x60 size-60x60 wp-post-image" alt="SBA loan icon" height="50" style="min-width:60px;display:block;" width="55" data-wp-pid="86352" nopin="nopin" title="SBA Loan" />
                        <h1>SBA Loan</h1>
                        <h5 class="my-3">Need COVID-19 relief? Begin your PPP loan application through Virtual Fund Assist to be
                            matched with a PPP lender. Virtual Fund Assist is not a lender, and an application submitted through
                            Virtual Fund Assist does not guarantee you will receive a PPP loan or be matched to a lender. We will
                            accept applications throughout the program or until allocated funds for the program have
                            been exhausted.</h5>
                    </div>
                    <div class="col-lg-6 text-center align-middle my-2">
                        <div class="cta p-5">
                            <h3>Let's get started.</h3>
                            <form action="https://www.lendio.com/bp/basic-info" method="GET" class="cta-content"> <input
                                    type="hidden" name="nosignup" value="1" />
                                <div class="input-container"> <span class="dollar">$</span> <input type="text"
                                        placeholder="How much do you need?" name="amountSeeking" /></div> <button
                                    class="primary-button">See your options</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="alt-header-swap"></div>
        <div class="py-5 alt-grey">
            <div class="container my-5">
                <div class="col-12 mb-5"></div>
                <div class="row">
                    <div id=" " class="col-12 col-md-6 d-md-block col-lg-6">
                        <div class="container-fluid">
                            <div class="row pb-4">
                                <div class="col-2"> 
                                    <img src="<?= $home_url ?>/images/dollar-icon.svg" class="d-block pr-2" alt="Loan Amount Icon" style="min-width:55px;" height="34" width="31" data-wp-pid="91618" nopin="nopin" />
                                </div>
                                <div class="col-10">
                                    <h3 class="pb-md-0" itemprop="name">Loan Amount</h3>
                                    <div class="fact-text pt-md-0 pb-md-4 pr-3">
                                        <h5 class="pt-0" itemprop="value">
                                            <p>$1,000 &#8211; $500,000</p>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="collapseFacts" class="col-12 col-md-6 d-md-block collapse multi-collapse col-lg-6">
                        <div class="container-fluid">
                            <div class="row pb-4">
                                <div class="col-2"> 
                                    <img src="<?= $home_url ?>/images/term-icon.svg" class="d-block pr-2" alt="Loan Term Icon" style="min-width:55px;" height="34" width="33" data-wp-pid="91619" nopin="nopin" />
                                </div>
                                <div class="col-10">
                                    <h3 class="pb-md-0" itemprop="name">Loan Term</h3>
                                    <div class="fact-text pt-md-0 pb-md-4 pr-3">
                                        <h5 class="pt-0" itemprop="value">
                                            <p>1-2 Year Maturity</p>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id=" " class="col-12 col-md-6 d-md-block col-lg-6">
                        <div class="container-fluid">
                            <div class="row pb-4">
                                <div class="col-2"> 
                                    <img src="<?= $home_url ?>/images/time-icon.svg" class="d-block pr-2" alt="Loan Time Icon" style="min-width:55px;" height="34" width="34" data-wp-pid="91620" nopin="nopin" />
                                </div>
                                <div class="col-10">
                                    <h3 class="pb-md-0" itemprop="name">Time to Funds</h3>
                                    <div class="fact-text pt-md-0 pb-md-4 pr-3">
                                        <h5 class="pt-0" itemprop="value">
                                            <p>As soon as 1-2 Weeks</p>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="collapseFacts" class="col-12 col-md-6 d-md-block collapse multi-collapse col-lg-6">
                        <div class="container-fluid">
                            <div class="row pb-4">
                                <div class="col-2"> 
                                    <img src="<?= $home_url ?>/images/percent-icon.svg" class="d-block pr-2" alt="Loan Interest Icon" style="min-width:55px;" height="32" width="32" data-wp-pid="91621" nopin="nopin" />
                                    </div>
                                <div class="col-10">
                                    <h3 class="pb-md-0" itemprop="name">Interest Rate</h3>
                                    <div class="fact-text pt-md-0 pb-md-4 pr-3">
                                        <h5 class="pt-0" itemprop="value">
                                            <p>As low as 8-24%</p>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-3 col-12 col-md-6 text-left px-0 border-none d-md-none"> <a id="a-tag"
                            class="justify-content-center d-block text-center font-weight-bold" data-toggle="collapse"
                            href="#collapseFacts" role="button" aria-expanded="false" aria-controls="collapseFacts">
                            <h4 id="collapseFactsLink" class="font-weight-bold p-0 m-0">See All</h4> <svg
                                id="collapse-arrow" class="bi bi-chevron-down" width="25" height="25"
                                viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M3.646 6.646a.5.5 0 01.708 0L10 12.293l5.646-5.647a.5.5 0 01.708.708l-6 6a.5.5 0 01-.708 0l-6-6a.5.5 0 010-.708z"
                                    clip-rule="evenodd"></path>
                            </svg>
                        </a></div>
                </div>
            </div>
        </div>
       
        <div class="alt-white py-5">
            <div class="container py-lg-0 py-5">
                <div class="row">
                    <div class="col-12 col-md-6 py-5 text-center text-md-left">
                        <h2>Meet the SBA, your small business ally.</h2>
                        <p>The <a href="https://www.sba.gov/" data-wpel-link="external" target="_blank"
                                rel="external noopener noreferrer">U.S. Small Business Administration</a> (SBA) is a
                            federal agency built solely for the purpose of helping small businesses get the funding they
                            need<span style="font-weight: 400;">—especially in times of economic hardship caused by
                                events like the coronavirus (COVID-19) pandemic. </span></p>
                        <p>Contrary to what you might think, the SBA doesn’t actually foot any of the cash. Instead, it
                            establishes the guidelines for loans and then guarantees a portion of those loans. Because
                            lenders have much less risk in the case of a default, they’re more likely to provide funds
                            to entrepreneurs like you.</p>
                        <p><a class="primary-button mt-4" href="../../bp/basic-info/index.html"
                                data-wpel-link="internal">Get SBA Aid</a></p>
                    </div>
                    <div class="col-12 text-center my-auto offset-md-1 col-md-5 pb-5 py-md-4"> <img width="1107"
                            height="690"
                            src="../images/sba-loans.jpg"
                            class="attachment-full size-full" alt="Small business owner in her store"
                            srcset="../images/sba-loans.jpg 1107w, ../images/sba-loans.jpg 1024w"
                            sizes="(max-width: 1107px) 100vw, 1107px" data-wp-pid="86298" nopin="nopin"
                            title="SBA Loan" /></div>
                </div>
            </div>
        </div>
        <div class="py-5 alt-white border-top">
            <div class="container py-0 py-lg-5">
                <div class="row">
                   <!-- <div class="col-2 col-md-1 offset-md-1 py-4"> 
                    <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" />
                </div> -->
                    <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                        <h2>Whatever your business needs, there&#8217;s an SBA loan for that.</h2>
                        <p>Seriously. You can find an SBA loan option to cover just about every nook and cranny of your
                            small business. Some of the most common SBA loans are the 7(a), 504, and SBA Express.</p>
                        <h3>SBA 7(a) Loan</h3>
                        <p>The 7(a) is one of the most flexible SBA loans. You can use it to:</p>
                        <ul class="checkmark-list">
                            <li>Buy land</li>
                            <li>Cover construction costs</li>
                            <li>Buy or expand an existing business</li>
                            <li>Refinance your existing debt</li>
                            <li>Buy machinery, furniture, supplies, or materials</li>
                        </ul>
                        <p>SBA 7(a) loans of less than $25,000 may not require collateral but higher loan amounts likely
                            will. For loans of $350,000 or higher, the SBA requires your lender to ask for the maximum
                            possible amount of collateral to limit risk of default. If you don&#8217;t have enough
                            business collateral to cover it, that&#8217;s okay &#8211; many forms of personal collateral
                            will also help you qualify.</p>
                        <p>If you&#8217;re looking for a large amount of money, you can get a 7(a) loan for up to $5
                            million if you meet all the qualification requirements.</p>
                        <h3>SBA 504 Loan</h3>
                        <p>504 loans can be a bit more complicated than 7(a)s. Because you would use a 504 to fund a
                            project, a thorough examination of your project costs will come into play. When your loan is
                            funded, the lender will initially cover 50% of your costs and the SBA will cover 40% &#8211;
                            that means you&#8217;re responsible for covering at least 10% right off the bat.
                            You&#8217;ll also be required to personally guarantee at least 20% of the loan.</p>
                        <p>You must use your SBA 504 loan to finance fixed assets, although some soft costs can also be
                            included. Examples of qualifying projects are:</p>
                        <ul class="checkmark-list">
                            <li>Buy an existing building</li>
                            <li>Build a new facility or renovate an existing facility</li>
                            <li>Buy land or make land improvements such as grading, landscaping, and adding parking lots
                            </li>
                            <li>Buy long-term machinery</li>
                            <li>Refinance debt incurred through the expansion of your business or renovation of your
                                facilities or equipment</li>
                        </ul>
                        <p>There are some cool perks to the SBA 504 loan. For example, you&#8217;ll benefit from 90%
                            financing, longer amortizations, no balloon payments, and fixed interest rates.</p>
                        <p>To qualify for an SBA 504 loan, your business must have a tangible net worth of more than $15
                            million and an average net income of $5 million or less for the two years prior to your
                            application.</p>
                        <h3>SBA Express Loan</h3>
                        <p>If you need cash in a jiffy, the SBA Express is the loan for you. Unlike the somewhat slower
                            review processes you might encounter with other SBA loans, SBA Express applications are
                            reviewed within 36 hours. This doesn&#8217;t mean that you&#8217;ll get access to funds that
                            fast though &#8211; it often still takes at least 30 days to get your SBA Express loan
                            funded.</p>
                        <p>You can finance up to $350,000 with an SBA Express. If your loan amount is more than $25,000,
                            your lender may require you to secure your loan with collateral. The loan can be used as
                            working capital (5-10 year term) or a line of credit (7-year term), or as a commercial real
                            estate loan (25-year term).</p>
                        <h4>Looking for a PPP loan? <a
                                href="../../covid-relief/sba-paycheck-protection-program-loans/index.html"
                                data-wpel-link="internal">Click here</a>.</h4>
                    </div>
                    <!-- <div class="col-2 col-md-1 offset-md-1 py-4"> 
                    <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" />
                </div> -->
                    <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                        <h2>Are SBA Loans Paid for by the Government?</h2>
                        <p>The short answer is, yes and no. Typically, because each type of SBA loan is
                            government-backed, many people might think the government is funding your small business
                            loans. While that’s usually not the case for traditional SBA loans, the adjustments made to
                            EIDLs by the CARES Act and the creation of PPPs mean the funds for these loans come straight
                            from the US Treasury. In the case of the 7(a), 504, and Express loans, the SBA guarantees
                            the loans, limiting the risk for the lender and making SBA loans more appealing to lenders.
                        </p>
                    </div>
                    <!-- <div class="col-2 col-md-1 offset-md-1 py-4"> 
                    <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" />
                </div> -->
                    <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                        <h2>The Benefits of a Government-Backed Loan</h2>
                        <p>SBA loans offer enviable <a
                                href="../../blog/small-business-tools/business-loan-interest-rates/index.html"
                                data-wpel-link="internal">rates</a> and terms for small businesses that might not
                            usually qualify for a traditional bank loan. The benefits don’t end there. These
                            government-backed loans offer monthly payments, fixed interest rates, special-case principal
                            amount forgiveness, and long repayment terms.</p>
                        <p>SBA loans are an excellent way to build and improve your credit, which puts you in a stronger
                            position the next time you need financing. Better credit can qualify you for higher amounts
                            and different forms of financing.</p>
                    </div>
                    <!-- <div class="col-2 col-md-1 offset-md-1 py-4"> 
                    <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" />
                </div> -->
                    <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                        <h2>Qualifying for an SBA Loan</h2>
                        <p>While traditional SBA loans are significantly easier to attain than your average bank loan,
                            they’re still more difficult to acquire than most loans from non-institutional lenders.
                            They’re known for being more paperwork-intensive with a much longer time to funds and a
                            higher percentage of rejection than direct online lenders. However, due to the economic
                            stress caused by COVID-19, some of the SBA loans have loosened their requirements to ensure
                            aid reaches as many small business owners as possible.</p>
                    </div>
                    <!-- <div class="col-2 col-md-1 offset-md-1 py-4"> 
                    <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" />
                </div> -->
                    <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                        <h2>How to Apply for an SBA Loan</h2>
                        <p>You’ll need to provide enough information to assess your loan application for a pre-approval
                            letter. In addition to providing the standard items like your business license, rounding up
                            these documents can speed up the process and make it easier for you to get approved:</p>
                        <ul>
                            <li>2 years of business tax returns</li>
                            <li>2 years of personal tax returns</li>
                            <li>YTD P&amp;L statement</li>
                            <li>YTD balance sheet</li>
                            <li>Debt schedule</li>
                        </ul>
                        <p>While both Economic Injury and Disaster Loan and Paycheck Protection Program loans are easier
                            to acquire than other SBA loans, they still have special requirements. In the case of an
                            EIDL, you must provide evidence your business has suffered as a direct result of
                            coronavirus. If you’re unable to do so, your business likely won’t qualify. But don’t sweat
                            it! We’re here to help.</p>
                    </div>
                    <!-- <div class="col-2 col-md-1 offset-md-1 py-4"> 
                    <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" />
                </div> -->
                    <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                        <h2>Virtual Fund Assist Makes SBA Applications Easy</h2>
                        <p>Don&#8217;t worry—more paperwork doesn&#8217;t necessarily equal more hassle. Our proprietary
                            <a href="../../bp/basic-info.html" data-wpel-link="internal">application platform</a> lets
                            you upload copies of your documents with just a click, which means you don&#8217;t need to
                            tote around a mountain of paperwork to get approved. And our personal funding managers can
                            walk you through the whole process if you need a little extra help.</p>
                    </div>
                    <!-- <div class="col-2 col-md-1 offset-md-1 py-4"> 
                    <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" />
                </div> -->
                    <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                        <h2>SBA Loan Interest Rates</h2>
                        <p>SBA loan interest rates are some of the lowest in the business. Because SBA loan interest
                            rates are based on the prime rate, SBA interest rates change whenever the Federal Reserve
                            moves the needle. You can find current SBA interest rates on our <a
                                href="../../business-calculators/sba-loan-calculator/index.html"
                                data-wpel-link="internal">SBA calculator page</a>, where you can also calculate the
                            estimated cost and monthly payments for your SBA loan.</p>
                        <p>&nbsp;</p>
                        <p align="center"><span class="fr-video fr-fvc fr-dvi fr-draggable"
                                contenteditable="false"><iframe class="fr-draggable"
                                    src="https://www.youtube.com/embed/W90eIG-ku78" width="610" height="275"
                                    allowfullscreen="allowfullscreen"></iframe></span></p>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="alt-grey text-center py-5">
            <div class="container py-0 py-lg-5">
                <div class="row px-2">
                    <div class="col-12 py-4"> 
                        <img src="<?= $home_url ?>/images/quotes.svg" alt="Quotes" />
                    </div>
                    <div class="col-12 py-4">
                        <h3>Michael made the SBA loan process as efficient, quick, and manageable as possible. He always
                            made himself available for the most important or simple questions, and fought for me when
                            someone had to fight on my behalf. The process itself will always be a large endeavor, but
                            Michael and the team at Virtual Fund Assist do the best job of making it a smooth process.</h3>
                        <h3>Dimitri Blackstone-Karapanos</h3>
                    </div>
                </div>
            </div>
        </div> -->
    </div>

    <!-- ==== Page(SBS Loans) Bottom-Blue Start ==== -->
    <div class="bottom-cta full base dark pt-2">
        <div class="container cta-bottom py-5">
            <div class="row">
                <div class="col-12 py-md-5 text-center">
                    <h2>Your sba loan is waiting.</h2>
                    <form action="https://www.lendio.com/bp/basic-info" method="GET"
                        class="cta-content d-inline-block my-4">
                        <div class="input-container col-lg-8 float-lg-left mb-3"> <span
                                class="dollar d-none d-lg-block">$</span> <input type="text"
                                placeholder="How much do you need?" class="amount-seeking-input text-center pl-0"
                                name="amountSeeking" /></div>
                        <div class="button-container col-lg-4 float-lg-left"> <button
                                class="primary-button w-lg-100 ml-0 mb-2">Start</button></div>
                        <div class="clear"></div>
                    </form>
                    <h5 class="px-5" style="font-size: 17px;">Applying is free and it won't impact your credit</h5>
                    <h5 style="font-size: 17px;">Already have an account? &nbsp;<a
                            class="text-uppercase font-weight-bold" href="../../bp/login.html"
                            data-wpel-link="internal"><strong>Sign in</strong></a></h5>
                </div>
            </div>
        </div>
    </div>
  
<?php include('../footer.php'); ?>
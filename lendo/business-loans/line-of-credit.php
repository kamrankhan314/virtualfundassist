<?php include('../header.php'); ?>

<!-- ==== Page(Business Line Of Credit) Main Start ==== -->
<div id="line-of-credit">
    <div class="hero pt-md-5" style="color:#fff;background: #4000dc;background: -moz-linear-gradient(-45deg, #4000dc 0%, #05c2f0 100%);background: -webkit-linear-gradient(-45deg, #4000dc 0%,#05c2f0 100%);background: linear-gradient(135deg, #4000dc 0%,#05c2f0 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#4000dc', endColorstr='#05c2f0',GradientType=1 );">
        <div class="container py-md-5">
            <div class="row py-md-2 hero-row">
                <div class="hero-responsive col-lg-6 text-left align-middle my-2">
                    <img src="<?= $home_url ?>/images/bus-line-of-credit-icon.svg" class="attachment-60x60 size-60x60 wp-post-image" alt="Business line of credit" height="56" style="min-width:60px;display:block;" width="60" data-wp-pid="86263" nopin="nopin" title="Business Line of Credit" />
                    <h1>Business Line of Credit</h1>
                    <h5 class="my-3">The safety net you need, the flexibility you want.</h5>
                </div>
                <div class="col-lg-6 text-center align-middle my-2">
                    <div class="cta p-5">
                        <h3>Let's get started.</h3>
                        <form action="https://www.lendio.com/bp/basic-info" method="GET" class="cta-content"> <input type="hidden" name="nosignup" value="1" /> <input type="hidden" name="referral_url" value="https://rivcoccsd.org/financing-help" />
                            <div class="input-container"> <span class="dollar">$</span> <input type="text" placeholder="How much do you need?" name="amountSeeking" /></div> <button class="primary-button">See your options</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="alt-header-swap"></div>
    <!-- Grey Section -->
    <div class="py-5 alt-grey">
        <div class="container my-5">
            <div class="col-12 mb-5"></div>
            <div class="row">
                <div id=" " class="col-12 col-md-6 d-md-block col-lg-6">
                    <div class="container-fluid">
                        <div class="row pb-4">
                            <div class="col-2">
                                <img src="<?= $home_url ?>/images/dollar-icon.svg" class="d-block pr-2" alt="Loan Amount Icon" style="min-width:55px;" height="34" width="31" data-wp-pid="91618" nopin="nopin" />
                            </div>
                            <div class="col-10">
                                <h3 class="pb-md-0" itemprop="name">Loan Amount</h3>
                                <div class="fact-text pt-md-0 pb-md-4 pr-3">
                                    <h5 class="pt-0" itemprop="value">
                                        <p>$1,000 &#8211; $500,000</p>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="collapseFacts" class="col-12 col-md-6 d-md-block collapse multi-collapse col-lg-6">
                    <div class="container-fluid">
                        <div class="row pb-4">
                            <div class="col-2">
                                <img src="<?= $home_url ?>/images/term-icon.svg" class="d-block pr-2" alt="Loan Term Icon" style="min-width:55px;" height="34" width="33" data-wp-pid="91619" nopin="nopin" />
                            </div>
                            <div class="col-10">
                                <h3 class="pb-md-0" itemprop="name">Loan Term</h3>
                                <div class="fact-text pt-md-0 pb-md-4 pr-3">
                                    <h5 class="pt-0" itemprop="value">
                                        <p>1-2 Year Maturity</p>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id=" " class="col-12 col-md-6 d-md-block col-lg-6">
                    <div class="container-fluid">
                        <div class="row pb-4">
                            <div class="col-2">
                                <img src="<?= $home_url ?>/images/time-icon.svg" class="d-block pr-2" alt="Loan Time Icon" style="min-width:55px;" height="34" width="34" data-wp-pid="91620" nopin="nopin" />
                            </div>
                            <div class="col-10">
                                <h3 class="pb-md-0" itemprop="name">Time to Funds</h3>
                                <div class="fact-text pt-md-0 pb-md-4 pr-3">
                                    <h5 class="pt-0" itemprop="value">
                                        <p>As soon as 1-2 Weeks</p>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="collapseFacts" class="col-12 col-md-6 d-md-block collapse multi-collapse col-lg-6">
                    <div class="container-fluid">
                        <div class="row pb-4">
                            <div class="col-2">
                                <img src="<?= $home_url ?>/images/percent-icon.svg" class="d-block pr-2" alt="Loan Interest Icon" style="min-width:55px;" height="32" width="32" data-wp-pid="91621" nopin="nopin" />
                            </div>
                            <div class="col-10">
                                <h3 class="pb-md-0" itemprop="name">Interest Rate</h3>
                                <div class="fact-text pt-md-0 pb-md-4 pr-3">
                                    <h5 class="pt-0" itemprop="value">
                                        <p>As low as 8-24%</p>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3 col-12 col-md-6 text-left px-0 border-none d-md-none"> <a id="a-tag" class="justify-content-center d-block text-center font-weight-bold" data-toggle="collapse" href="#collapseFacts" role="button" aria-expanded="false" aria-controls="collapseFacts">
                        <h4 id="collapseFactsLink" class="font-weight-bold p-0 m-0">See All</h4> <svg id="collapse-arrow" class="bi bi-chevron-down" width="25" height="25" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M3.646 6.646a.5.5 0 01.708 0L10 12.293l5.646-5.647a.5.5 0 01.708.708l-6 6a.5.5 0 01-.708 0l-6-6a.5.5 0 010-.708z" clip-rule="evenodd"></path>
                        </svg>
                    </a></div>
            </div>
        </div>
    </div>

    <div class="alt-white py-5">
        <div class="container py-lg-0 py-5">
            <div class="row">
                <div class="col-12 col-md-6 py-5 text-center text-md-left">
                    <h2>Want flexible financing? Then you want a business line of credit.</h2>
                    <p>Think of a <a href="../../blog/small-business-insights/10-reasons-small-business-owners-take-line-credit/index.html" data-wpel-link="internal">line of credit</a> as a financial safety net for your
                        business. It’s there if you need it, but you’re under no obligation to use it. And when you
                        do tap into it, you can use it to cover almost any small business need. Plus, you only pay
                        interest on the funds you use, not the full amount. In other words, it’s as flexible as a
                        double-jointed yoga teacher.</p>
                    <p><a class="primary-button mt-4" href="../../bp/basic-info.html" data-wpel-link="internal">See
                            your options</a></p>
                </div>
                <div class="col-12 text-center my-auto offset-md-1 col-md-5 pb-5 py-md-4"> <img width="515" height="380" src="../images/line-credit.svg" class="attachment-full size-full" alt="Business Line of Credit" srcset="../images/line-credit.svg 515w, ../images/line-credit.svg 300w" sizes="(max-width: 515px) 100vw, 515px" data-wp-pid="86265" nopin="nopin" title="Business Line of Credit" /></div>
            </div>
        </div>
    </div>
    <div class="alt-grey py-lg-2">
        <div class="container py-0 py-md-5">
            <div class="row">
                <div class="col-12 py-5 text-center">
                    <h2>Explore small business loan options from these leading lenders</h2>
                    <div class="container lender-logos">
                        <div class="row d-flex justify-content-center">
                            <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                <img class="lender-block-img" src="<?= $home_url ?>/images/chase-logo.png" alt="chase-logo" />
                            </div>
                            <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                <img class="lender-block-img" src="<?= $home_url ?>/images/amex-logo.png" alt="amex-logo" />
                            </div>
                            <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                <img class="lender-block-img" src="<?= $home_url ?>/images/lencred-logo.png" alt="amex-logo" />
                            </div>
                            <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                <img class="lender-block-img" src="<?= $home_url ?>/images/bank-of-america-logo.png" alt="amex-logo" />
                            </div>
                            <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                <img class="lender-block-img" src="<?= $home_url ?>/images/seek-capital-logo.png" alt="amex-logo" />
                            </div>
                        </div>
                    </div> <a href="../../bp/lenders.html" class="primary-button mt-5" data-wpel-link="internal">Find your lender</a>
                </div>
            </div>
        </div>
    </div>
    <div class="py-5 alt-white">
        <div class="container py-0 py-lg-5">
            <div class="row">
                <!-- <div class="col-2 col-md-1 offset-md-1 py-4">
                        <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" />
                    </div> -->
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2>What is a business line of credit?</h2>
                    <p>A business line of credit is a financial safety net for your business. It’s also one of the
                        most flexible forms of financing. You can use it for buying equipment, hiring staff,
                        increasing inventory, adding a second location, paying invoices, installing a cappuccino
                        machine, and more.</p>
                    <p>And because a line of credit is revolving, you can use it as many times as you want. As soon
                        as you repay what you’ve used, those funds become available to you again.</p>
                </div>
                <!-- <div class="col-2 col-md-1 offset-md-1 py-4">
                        <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" />
                    </div> -->
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2>Qualifying and applying for your line of credit is pretty easy.</h2>
                    <p>To get your business line of credit, you’ll typically need to be in business at least 6
                        months and have $50,000 or more in annual revenue. You’ll also need a <a href="../../business-credit/index.html" data-wpel-link="internal">credit score</a> of
                        560 or higher.</p>
                    <p>Your lender may ask you to make a personal guarantee, which is an agreement that the lender
                        may be able to levy personal assets such as a car, house, or bank account if you default on
                        the line of credit.</p>
                    <p>Applying is easy: simply fill out our <a href="../../bp/basic-infoced1.html?step=0&amp;page=0&amp;nosignup=1" data-wpel-link="internal">15-minute application</a>, then compare business line of
                        credit options from 75+ lenders.</p>
                </div>
                <!-- <div class="col-2 col-md-1 offset-md-1 py-4">
                        <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" />
                    </div> -->
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2>You pay interest on the funds you use, not the whole thing.</h2>
                    <p>One of the coolest things about a business line of credit is that you only pay interest on
                        the funds you use, not the full amount. For example, if you’re approved for a $40,000
                        business line of credit and you use $20,000 for office upgrades, you’ll just pay interest on
                        that $20,000. This could save you a bundle in interest. See, pretty cool, huh?</p>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="alt-white text-center py-5 border-top">
            <div class="container py-0 py-lg-5">
                <div class="row px-2">
                    <div class="col-12 py-4">
                        <img src="<?= $home_url ?>/images/quotes.svg" alt="Quotes" />
                    </div>
                    <div class="col-12 py-4">
                        <h3>Virtual Fund Assist agent Brett was a pleasure to work with and super helpful in connecting me with the
                            right lender for a business line of credit. This new way of banking with peer-to-peer
                            lending is amazing for small businesses compared to the stringent large banking
                            requirements.</h3>
                        <h3>Chris M.</h3>
                    </div>
                </div>
            </div>
        </div> -->
</div>

<!-- ==== Page(Business Line Of Credit) Bottom-Blue Start ==== -->
<div class="bottom-cta full base dark pt-2">
    <div class="container cta-bottom py-5">
        <div class="row">
            <div class="col-12 py-md-5 text-center">
                <h2>Your business line of credit is waiting.</h2>
                <form action="https://www.lendio.com/bp/basic-info" method="GET" class="cta-content d-inline-block my-4"> <input type="hidden" name="referral_url" value="https://rivcoccsd.org/financing-help" />
                    <div class="input-container col-lg-8 float-lg-left mb-3"> <span class="dollar d-none d-lg-block">$</span> <input type="text" placeholder="How much do you need?" class="amount-seeking-input text-center pl-0" name="amountSeeking" /></div>
                    <div class="button-container col-lg-4 float-lg-left"> <button class="primary-button w-lg-100 ml-0 mb-2">Start</button></div>
                    <div class="clear"></div>
                </form>
                <h5 class="px-5" style="font-size: 17px;">Applying is free and it won't impact your credit</h5>
                <h5 style="font-size: 17px;">Already have an account? &nbsp;<a class="text-uppercase font-weight-bold" href="../../bp/login.html" data-wpel-link="internal"><strong>Sign in</strong></a></h5>
            </div>
        </div>
    </div>
</div>

<?php include('../footer.php'); ?>
<?php include('../header.php'); ?>

<!-- ==== Page(Eqipment-financing) Main Start ==== -->
<div id="commercial-real-estate">
    <div class="hero pt-md-5" style="color:#fff;background: #4000dc;background: -moz-linear-gradient(-45deg, #4000dc 0%, #05c2f0 100%);background: -webkit-linear-gradient(-45deg, #4000dc 0%,#05c2f0 100%);background: linear-gradient(135deg, #4000dc 0%,#05c2f0 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#4000dc', endColorstr='#05c2f0',GradientType=1 );">
        <div class="container py-md-5">
            <div class="row py-md-2 hero-row">
                <div class="hero-responsive col-lg-6 text-left align-middle my-2">
                    <img src="<?= $home_url ?>/images/commercial-financing-icon.svg" class="attachment-60x60 size-60x60 wp-post-image" alt="Commercial mortgage" height="54" style="min-width:60px;display:block;" width="54" data-wp-pid="86272" nopin="nopin" title="Commercial Mortgage" />
                    <h1>Commercial Mortgage</h1>
                    <h5 class="my-3">Use it to buy, build, expand, remodel, or even refinance.</h5>
                </div>
                <div class="col-lg-6 text-center align-middle my-2">
                    <div class="cta p-5">
                        <h3>Let's get started.</h3>
                        <form action="https://www.lendio.com/bp/basic-info" method="GET" class="cta-content"> <input type="hidden" name="nosignup" value="1" /> <input type="hidden" name="referral_url" value="https://www.google.com/" />
                            <div class="input-container"> <span class="dollar">$</span> <input type="text" placeholder="How much do you need?" name="amountSeeking" /></div> <button class="primary-button">See your options</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="alt-header-swap"></div>
    <!-- Grey Section -->
    <div class="py-5 alt-grey">
        <div class="container my-5">
            <div class="col-12 mb-5"></div>
            <div class="row">
                <div id=" " class="col-12 col-md-6 d-md-block col-lg-6">
                    <div class="container-fluid">
                        <div class="row pb-4">
                            <div class="col-2">
                                <img src="<?= $home_url ?>/images/dollar-icon.svg" class="d-block pr-2" alt="Loan Amount Icon" style="min-width:55px;" height="34" width="31" data-wp-pid="91618" nopin="nopin" />
                            </div>
                            <div class="col-10">
                                <h3 class="pb-md-0" itemprop="name">Loan Amount</h3>
                                <div class="fact-text pt-md-0 pb-md-4 pr-3">
                                    <h5 class="pt-0" itemprop="value">
                                        <p>$1,000 &#8211; $500,000</p>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="collapseFacts" class="col-12 col-md-6 d-md-block collapse multi-collapse col-lg-6">
                    <div class="container-fluid">
                        <div class="row pb-4">
                            <div class="col-2">
                                <img src="<?= $home_url ?>/images/term-icon.svg" class="d-block pr-2" alt="Loan Term Icon" style="min-width:55px;" height="34" width="33" data-wp-pid="91619" nopin="nopin" />
                            </div>
                            <div class="col-10">
                                <h3 class="pb-md-0" itemprop="name">Loan Term</h3>
                                <div class="fact-text pt-md-0 pb-md-4 pr-3">
                                    <h5 class="pt-0" itemprop="value">
                                        <p>1-2 Year Maturity</p>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id=" " class="col-12 col-md-6 d-md-block col-lg-6">
                    <div class="container-fluid">
                        <div class="row pb-4">
                            <div class="col-2">
                                <img src="<?= $home_url ?>/images/time-icon.svg" class="d-block pr-2" alt="Loan Time Icon" style="min-width:55px;" height="34" width="34" data-wp-pid="91620" nopin="nopin" />
                            </div>
                            <div class="col-10">
                                <h3 class="pb-md-0" itemprop="name">Time to Funds</h3>
                                <div class="fact-text pt-md-0 pb-md-4 pr-3">
                                    <h5 class="pt-0" itemprop="value">
                                        <p>As soon as 1-2 Weeks</p>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="collapseFacts" class="col-12 col-md-6 d-md-block collapse multi-collapse col-lg-6">
                    <div class="container-fluid">
                        <div class="row pb-4">
                            <div class="col-2">
                                <img src="<?= $home_url ?>/images/percent-icon.svg" class="d-block pr-2" alt="Loan Interest Icon" style="min-width:55px;" height="32" width="32" data-wp-pid="91621" nopin="nopin" />
                            </div>
                            <div class="col-10">
                                <h3 class="pb-md-0" itemprop="name">Interest Rate</h3>
                                <div class="fact-text pt-md-0 pb-md-4 pr-3">
                                    <h5 class="pt-0" itemprop="value">
                                        <p>As low as 8-24%</p>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3 col-12 col-md-6 text-left px-0 border-none d-md-none"> <a id="a-tag" class="justify-content-center d-block text-center font-weight-bold" data-toggle="collapse" href="#collapseFacts" role="button" aria-expanded="false" aria-controls="collapseFacts">
                        <h4 id="collapseFactsLink" class="font-weight-bold p-0 m-0">See All</h4> <svg id="collapse-arrow" class="bi bi-chevron-down" width="25" height="25" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M3.646 6.646a.5.5 0 01.708 0L10 12.293l5.646-5.647a.5.5 0 01.708.708l-6 6a.5.5 0 01-.708 0l-6-6a.5.5 0 010-.708z" clip-rule="evenodd"></path>
                        </svg>
                    </a></div>
            </div>
        </div>
    </div>
    <div class="alt-white py-5">
        <div class="container py-lg-0 py-5">
            <div class="row">
                <div class="col-12 col-md-6 py-5 text-center text-md-left">
                    <h2>Putting a roof over your small business</h2>
                    <p>Building out your business location is a smart way to increase your assets. Every renovation,
                        upgrade, or expansion adds value to your property, gives you the space you need to
                        streamline operations, or attracts more customers. A commercial mortgage can help you do all
                        this and more. Remember, there’s more at stake than square footage. Making a savvy financing
                        move could help you build a firmer foundation for your small business—literally and
                        fiscally.</p>
                    <p><a class="primary-button mt-4" href="../../bp/basic-info.html" data-wpel-link="internal">See
                            your options</a></p>
                </div>
                <div class="col-12 text-center my-auto offset-md-1 col-md-5 pb-5 py-md-4"> <img width="515" height="380" src="../images/commercial-real-sate.svg" class="attachment-full size-full" alt="Commercial Financing" srcset="../images/commercial-real-sate.svg 515w, ../images/commercial-real-sate.svg 300w" sizes="(max-width: 515px) 100vw, 515px" data-wp-pid="86274" nopin="nopin" title="Commercial Mortgage" /></div>
            </div>
        </div>
    </div>
    <div class="py-5 alt-white border-top">
        <div class="container py-0 py-lg-5">
            <div class="row">
                <!-- <div class="col-2 col-md-1 offset-md-1 py-4"> 
                        <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" />
                    </div> -->
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2>Meet your commercial mortgage</h2>
                    <p>A commercial mortgage is designed to help your business building-related financial costs.</p>
                    <h3>How can you use a commercial mortgage?</h3>
                    <p>You can use a commercial mortgage to purchase a commercial property—whether it’s office
                        space, a factory, retail, or restaurant space. If you can’t find an existing building that’s
                        right for your needs, you can use your building loan to cover the construction costs of
                        building a new space. <a href="../../blog/small-business-tools/signs-time-expand/index.html" data-wpel-link="internal">Need to expand</a>? A commercial mortgage covers that.</p>
                    <h3>Can you use a commercial mortgage to refinance a commercial property?</h3>
                    <p>Yes, you can extend your payment terms and adjust your interest rate by <a href="../../blog/small-business-tools/refinance-business-loan/index.html" data-wpel-link="internal">refinancing</a> with a commercial mortgage.</p>
                    <h3>Can you use a commercial mortgage for renovations on a commercial property?</h3>
                    <p>Yes! Would updating your building help you to attract more customers? A commercial mortgage
                        can give you the financing to make it happen. Restaurants and retailers have demonstrated
                        the ways an updated interior can drive customers, bringing the owners a sweet return on
                        their investment.</p>
                    <h3>Can you use a commercial mortgage to buy a hat?</h3>
                    <p>In our professional opinion, probably not. That sounds like a personal purchase, and one
                        small enough that you probably shouldn’t take out a loan for it. Unless it’s a fancy,
                        super-important hat that your business needs. In that case, we recommend an <a href="../equipment-financing/index.html" data-wpel-link="internal">equipment loan</a>.
                    </p>
                </div>
                <!-- <div class="col-2 col-md-1 offset-md-1 py-4"> 
                        <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" />
                    </div> -->
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2>How to qualify for a commercial mortgage</h2>
                    <p>Most lenders will require your business to occupy 51% of the property to qualify for a
                        commercial mortgage. Because a commercial mortgage is an asset-based loan, the loan amount
                        and rate will largely be based on <a href="../../business-credit/index.html" data-wpel-link="internal">your credit</a> and the value of the property you’re using as
                        collateral.</p>
                    <h3>Pick the right location.</h3>
                    <p>The value of the property affects the mortgage decision. For that reason, a prime retail
                        space in Los Angeles may be easier to finance than a rural storage unit a few hours outside
                        Fargo, North Dakota. You’ll want to take this into consideration when you’re deciding where
                        to buy or build.</p>
                    <h3>Communicate renovation plans to lenders.</h3>
                    <p>If you’re planning on making upgrades to a property, your lender will want to know about
                        them. For full-scale property renovations, your lender will also want to assess the
                        after-repair value (commonly called ARV) of the property. Be sure you have a plan for how
                        you’ll use the mortgage before you apply—that way, you’ll have answers to all the lender’s
                        questions.</p>
                    <h3>Prepare necessary documents.</h3>
                    <ul>
                        <li>Purchase contract</li>
                        <li>Property blueprints</li>
                        <li>Market analysis for the property</li>
                        <li>Project budget and scope of work</li>
                        <li>Assessment of the property’s existing conditions</li>
                    </ul>
                </div>
                <!-- <div class="col-2 col-md-1 offset-md-1 py-4"> 
                        <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" />
                    </div> -->
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2>The fastest way to get a commercial mortgage</h2>
                    <p>Virtual Fund Assist’s single <a href="../../bp/basic-info.html" data-wpel-link="internal">15-minute
                            application</a> allows you to compare options from our curated network of 75+ lenders.
                        Commercial mortgages can be funded in as little as 45 days, so you might as well get the
                        ball rolling… in a lot less time than the 29 hours your average bank application takes.</p>
                </div>
                <!-- <div class="col-2 col-md-1 offset-md-1 py-4"> 
                        <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" />
                    </div> -->
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2>Commercial mortgage rates</h2>
                    <p>Commercial mortgage interest rates can be as low as 4.25%, making the loans an incredibly
                        cost-effective form of capital.</p>
                    <p>When you apply through Virtual Fund Assist, you can compare commercial mortgage rates from our curated
                        network of 75+ lenders, and your funding manager can help you weigh the benefits and costs
                        of your financing options.</p>
                    <p>To get a ballpark idea of how much your commercial mortgage payments could be, check out our
                        <a href="../../business-calculators/commercial-mortgage-calculator/index.html" data-wpel-link="internal">commercial real estate calculator</a>. Plug in a few numbers
                        and we’ll do the math for you.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="alt-white text-center py-5 border-top">
            <div class="container py-0 py-lg-5">
                <div class="row px-2">
                    <div class="col-12 py-4"> 
                        <img src="<?= $home_url ?>/images/quotes.svg" alt="Quotes" /></div>
                    <div class="col-12 py-4">
                        <h3>Needed capital to do some renovations for my business. The money I borrowed helped me do
                            that, and now more clients are coming in than ever.</h3>
                        <h3>Matt D.</h3>
                    </div>
                </div>
            </div>
        </div> -->
</div>

<!-- ==== Page(Eqipment-financing) Blue-Bottom Start ==== -->
<div class="bottom-cta full base dark pt-2">
    <div class="container cta-bottom py-5">
        <div class="row">
            <div class="col-12 py-md-5 text-center">
                <h2>Your commercial mortgage is waiting.</h2>
                <form action="https://www.lendio.com/bp/basic-info" method="GET" class="cta-content d-inline-block my-4"> <input type="hidden" name="referral_url" value="https://www.google.com/" />
                    <div class="input-container col-lg-8 float-lg-left mb-3"> <span class="dollar d-none d-lg-block">$</span> <input type="text" placeholder="How much do you need?" class="amount-seeking-input text-center pl-0" name="amountSeeking" /></div>
                    <div class="button-container col-lg-4 float-lg-left"> <button class="primary-button w-lg-100 ml-0 mb-2">Start</button></div>
                    <div class="clear"></div>
                </form>
                <h5 class="px-5" style="font-size: 17px;">Applying is free and it won't impact your credit</h5>
                <h5 style="font-size: 17px;">Already have an account? &nbsp;<a class="text-uppercase font-weight-bold" href="../../bp/login.html" data-wpel-link="internal"><strong>Sign in</strong></a></h5>
            </div>
        </div>
    </div>
</div>

<?php include('../footer.php'); ?>
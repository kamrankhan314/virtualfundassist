<?php include('../header.php'); ?>

<!-- ==== Page(Credit-Card)  Yellow-Bar Start ==== -->
<div class="hero position-relative" style="background: linear-gradient(90deg, #ff3366 0%, #fe9b02 100%); color: white;">
    <div class="site-close"><i class="fa fa-times"></i></div>
    <div class="container p-lg-0">
        <div class="py-lg-2 row hero-row">
            <div class="hero-responsive col-12 text-left text-md-center align-middle mt-4 my-lg-2"> The President has signed a relief bill
                that includes $284 billion for
                Paycheck Protection Program (PPP)
                loans. Virtual Fund Assist will submit
                completed, eligible applications to
                PPP lenders once the program
                officially resumes in January 2021.</div>
        </div>
    </div>
</div>

<!-- ==== Page(Credit-Card)  Main Start ==== -->
<div id="business-credit-card">
    <div class="hero pt-md-5" style="color:#fff;background: #4000dc;background: -moz-linear-gradient(-45deg, #4000dc 0%, #05c2f0 100%);background: -webkit-linear-gradient(-45deg, #4000dc 0%,#05c2f0 100%);background: linear-gradient(135deg, #4000dc 0%,#05c2f0 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#4000dc', endColorstr='#05c2f0',GradientType=1 );">
        <div class="container py-md-5">
            <div class="row py-md-2 hero-row">
                <div class="hero-responsive col-lg-6 text-left align-middle my-2">
                    <h1>Business Credit Card</h1>
                    <h5 class="my-3">A little piece of plastic with big business-boosting capabilities.</h5>
                </div>
                <div class="col-lg-6 text-center align-middle my-2">
                    <div class="cta p-5">
                        <h3>Let's get started.</h3>
                        <form action="https://www.lendio.com/bp/basic-info" method="GET" class="cta-content"> <input type="hidden" name="nosignup" value="1" />
                            <div class="input-container"> <span class="dollar">$</span> <input type="text" placeholder="How much do you need?" name="amountSeeking" /></div> <button class="primary-button">See your options</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="alt-header-swap"></div>
    <div class="alt-white py-5">
        <div class="container py-lg-0 py-5">
            <div class="row">
                <div class="col-12 col-md-6 py-5 text-center text-md-left">
                    <h2>Business financing that fits in  your wallet.</h2>
                    <p>Opening a business credit card is a rite of passage for a small business. Go from big idea to
                        big business with access to working capital that fits in your back pocket. A business credit
                        card helps you track expenses, build a strong business credit history, and increase your
                        working capital so you can reap the literal rewards.</p>
                    <p><a class="primary-button mt-4" href="../bp/basic-info.html" data-wpel-link="internal">See
                            your options</a></p>
                </div>
                <div class="col-12 text-center my-auto offset-md-1 col-md-5 pb-5 py-md-4">
                    <img width="500" height="369" src="<?= $home_url ?>/images/business-card.jpg" class="attachment-full size-full" nopin="nopin" title="Business Credit Card" />
                </div>
            </div>
        </div>
    </div>
    <div class="alt-grey py-lg-2">
        <div class="container py-0 py-md-5">
            <div class="row">
                <div class="col-12 py-5 text-center">
                    <h2>Explore small business loan options from these leading lenders</h2>
                    <div class="container lender-logos">
                        <div class="row d-flex justify-content-center">
                            <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                <img class="lender-block-img" src="<?= $home_url ?>/images/chase-logo.png" alt="chase-logo" />
                            </div>
                            <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                <img class="lender-block-img" src="<?= $home_url ?>/images/amex-logo.png" alt="amex-logo" />
                            </div>
                            <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                <img class="lender-block-img" src="<?= $home_url ?>/images/lencred-logo.png" alt="amex-logo" />
                            </div>
                            <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                <img class="lender-block-img" src="<?= $home_url ?>/images/bank-of-america-logo.png" alt="amex-logo" />
                            </div>
                            <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                <img class="lender-block-img" src="<?= $home_url ?>/images/seek-capital-logo.png" alt="amex-logo" />
                            </div>
                        </div>
                    </div> <a href="../bp/lenders.html" class="primary-button mt-5" data-wpel-link="internal">Find
                        Your lender</a>
                </div>
            </div>
        </div>
    </div>
    <div class="py-5 alt-white">
        <div class="container py-0 py-lg-5">
            <div class="row">
                <div class="col-2 col-md-1 offset-md-1 py-4">
                    <!-- <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" /> -->
                </div>
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2>The Convenience and Flexibility of Business Credit Cards</h2>
                    <p>Business credit cards offer a sweet spot between personal credit cards and corporate credit
                        cards. Specifically designed for small businesses, business credit cards provide a flexible,
                        convenient way for your small business to make purchases.</p>
                    <p>Often, you’ll benefit from a 0% introductory rate and a rewards program while providing your
                        small business with a financial safety net—all the security of a loan with less paperwork
                        and a smaller commitment. Pretty cool, huh?</p>
                    <h3>When can you use a business credit card?</h3>
                    <p>You can use a business credit card anytime, anywhere.</p>
                    <h3>What can you use a business credit card to do?</h3>
                    <p>You can use your business credit card for almost any business-related need, including
                        purchasing equipment, increasing inventory, taking clients out to lunch, or simply covering
                        monthly costs.</p>
                    <h3>How can you use a credit card for business financing?</h3>
                    <p>If you don’t yet qualify for a business loan or if you’re not quite ready for one, business
                        credit cards may be the solution for you. A business credit card can increase your working
                        capital, get fast access to cash when you need it, take advantage of rewards programs, and
                        build your credit.</p>
                </div>
                <div class="col-2 col-md-1 offset-md-1 py-4">
                    <!-- <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" /> -->
                </div>
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2>Qualifying for a Business Credit Card</h2>
                    <p>It’s relatively easy to get a business credit card. Whether you’re getting a business credit
                        card for a new business or you’re finally cleaning up your business finances by moving
                        company charges off your personal credit card, we’re here to help you find the best business
                        credit card for you.</p>
                    <h3>Who can get a business credit card?</h3>
                    <p>Anyone can qualify for a business credit card. Decisions are made based on your personal
                        credit history. You’ll typically need a credit score of 680 or higher to qualify. If your
                        business has been in business long enough to have developed a credit history of its own,
                        that may help.</p>
                    <h3>Do you have to be an existing business?</h3>
                    <p>Nope. Business credit cards are a great fit for startups and other new businesses because
                        decisions are made based on your personal credit score. If you have a strong personal credit
                        history, your new business shouldn’t have a hard time qualifying for a card.</p>
                </div>
                <div class="col-2 col-md-1 offset-md-1 py-4">
                    <!-- <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" /> -->
                </div>
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2>Applying for a Business Credit Card</h2>
                    <p>Fill out our 15-minute application and access offers from 75+ lenders. You can see which
                        cards you qualify for before choosing the one that best suits your business needs. Once you
                        choose a card, you can get approved the same day.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="alt-white text-center py-5 border-top">
        <div class="container py-0 py-lg-5">
            <div class="row px-2">
                <div class="col-12 py-4"> <img src="<?= $home_url ?>/images/quote-icon.webp" alt="Quotes" />
                </div>
                <div class="col-12 py-4">
                    <h3>Virtual Fund Assist found me credit cards with much larger credit lines than I was able to find on my
                        own. I will now be able to invest in another property and I will have the necessary funds to
                        complete the project with an easily attainable profit margin.</h3>
                    <h3>Alan</h3>
                </div>
            </div>
        </div>
    </div> -->
</div>
<div class="bottom-cta full base dark pt-2">
    <div class="container cta-bottom py-5">
        <div class="row">
            <div class="col-12 py-md-5 text-center">
                <h2>Your business credit card is waiting.</h2>
                <form action="https://www.lendio.com/bp/basic-info" method="GET" class="cta-content d-inline-block my-4">
                    <div class="input-container col-lg-8 float-lg-left mb-3"> <span class="dollar d-none d-lg-block">$</span> <input type="text" placeholder="How much do you need?" class="amount-seeking-input text-center pl-0" name="amountSeeking" /></div>
                    <div class="button-container col-lg-4 float-lg-left"> <button class="primary-button w-lg-100 ml-0 mb-2">Start</button></div>
                    <div class="clear"></div>
                </form>
                <h5 class="px-5" style="font-size: 17px;">Applying is free and it won't impact your credit</h5>
                <h5 style="font-size: 17px;">Already have an account? &nbsp;<a class="text-uppercase font-weight-bold" href="../bp/login.html" data-wpel-link="internal"><strong>Sign in</strong></a></h5>
            </div>
        </div>
    </div>
</div>

<?php include('../footer.php'); ?>
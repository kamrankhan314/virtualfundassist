<?php include('../header.php'); ?>

<!-- ==== Page(Startup-Loan) Main Start ==== -->
<div id="startup-loans">
    <div class="hero pt-md-5" style="color:#fff;background: #4000dc;background: -moz-linear-gradient(-45deg, #4000dc 0%, #05c2f0 100%);background: -webkit-linear-gradient(-45deg, #4000dc 0%,#05c2f0 100%);background: linear-gradient(135deg, #4000dc 0%,#05c2f0 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#4000dc', endColorstr='#05c2f0',GradientType=1 );">
        <div class="container py-md-5">
            <div class="row py-md-2 hero-row">
                <div class="hero-responsive col-lg-6 text-left align-middle my-2">
                    <img src="<?= $home_url ?>/images/startup-loan-icon.svg" class="attachment-60x60 size-60x60 wp-post-image" alt="Startup loan icon" height="56" style="min-width:60px;display:block;" width="57" data-wp-pid="86269" nopin="nopin" title="Startup Loan" />
                    <h1>Startup Loan</h1>
                    <h5 class="my-3">You’ve got the startup, now get the funding.</h5>
                </div>
                <div class="col-lg-6 text-center align-middle my-2">
                    <div class="cta p-5">
                        <h3>Let's get started.</h3>
                        <form action="https://www.lendio.com/bp/basic-info" method="GET" class="cta-content"> <input type="hidden" name="nosignup" value="1" /> <input type="hidden" name="referral_url" value="https://www.google.com/" />
                            <div class="input-container"> <span class="dollar">$</span> <input type="text" placeholder="How much do you need?" name="amountSeeking" /></div> <button class="primary-button">See your options</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="alt-header-swap"></div>
    <!-- Grey Section -->
    <div class="py-5 alt-grey">
        <div class="container my-5">
            <div class="col-12 mb-5"></div>
            <div class="row">
                <div id=" " class="col-12 col-md-6 d-md-block col-lg-6">
                    <div class="container-fluid">
                        <div class="row pb-4">
                            <div class="col-2">
                                <img src="<?= $home_url ?>/images/dollar-icon.svg" class="d-block pr-2" alt="Loan Amount Icon" style="min-width:55px;" height="34" width="31" data-wp-pid="91618" nopin="nopin" />
                            </div>
                            <div class="col-10">
                                <h3 class="pb-md-0" itemprop="name">Loan Amount</h3>
                                <div class="fact-text pt-md-0 pb-md-4 pr-3">
                                    <h5 class="pt-0" itemprop="value">
                                        <p>$1,000 &#8211; $500,000</p>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="collapseFacts" class="col-12 col-md-6 d-md-block collapse multi-collapse col-lg-6">
                    <div class="container-fluid">
                        <div class="row pb-4">
                            <div class="col-2">
                                <img src="<?= $home_url ?>/images/term-icon.svg" class="d-block pr-2" alt="Loan Term Icon" style="min-width:55px;" height="34" width="33" data-wp-pid="91619" nopin="nopin" />
                            </div>
                            <div class="col-10">
                                <h3 class="pb-md-0" itemprop="name">Loan Term</h3>
                                <div class="fact-text pt-md-0 pb-md-4 pr-3">
                                    <h5 class="pt-0" itemprop="value">
                                        <p>1-2 Year Maturity</p>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id=" " class="col-12 col-md-6 d-md-block col-lg-6">
                    <div class="container-fluid">
                        <div class="row pb-4">
                            <div class="col-2">
                                <img src="<?= $home_url ?>/images/time-icon.svg" class="d-block pr-2" alt="Loan Time Icon" style="min-width:55px;" height="34" width="34" data-wp-pid="91620" nopin="nopin" />
                            </div>
                            <div class="col-10">
                                <h3 class="pb-md-0" itemprop="name">Time to Funds</h3>
                                <div class="fact-text pt-md-0 pb-md-4 pr-3">
                                    <h5 class="pt-0" itemprop="value">
                                        <p>As soon as 1-2 Weeks</p>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="collapseFacts" class="col-12 col-md-6 d-md-block collapse multi-collapse col-lg-6">
                    <div class="container-fluid">
                        <div class="row pb-4">
                            <div class="col-2">
                                <img src="<?= $home_url ?>/images/percent-icon.svg" class="d-block pr-2" alt="Loan Interest Icon" style="min-width:55px;" height="32" width="32" data-wp-pid="91621" nopin="nopin" />
                            </div>
                            <div class="col-10">
                                <h3 class="pb-md-0" itemprop="name">Interest Rate</h3>
                                <div class="fact-text pt-md-0 pb-md-4 pr-3">
                                    <h5 class="pt-0" itemprop="value">
                                        <p>As low as 8-24%</p>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3 col-12 col-md-6 text-left px-0 border-none d-md-none"> <a id="a-tag" class="justify-content-center d-block text-center font-weight-bold" data-toggle="collapse" href="#collapseFacts" role="button" aria-expanded="false" aria-controls="collapseFacts">
                        <h4 id="collapseFactsLink" class="font-weight-bold p-0 m-0">See All</h4> <svg id="collapse-arrow" class="bi bi-chevron-down" width="25" height="25" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M3.646 6.646a.5.5 0 01.708 0L10 12.293l5.646-5.647a.5.5 0 01.708.708l-6 6a.5.5 0 01-.708 0l-6-6a.5.5 0 010-.708z" clip-rule="evenodd"></path>
                        </svg>
                    </a></div>
            </div>
        </div>
    </div>
    <div class="alt-white py-5">
        <div class="container py-lg-0 py-5">
            <div class="row">
                <div class="col-12 col-md-6 py-5 text-center text-md-left">
                    <h2>Launch your startup to greatness.</h2>
                    <p>Starting a business comes with costs. Enter: startup loans, the business loans specifically
                        designed to help finance the next big thing. Securing a startup loan is better than giving
                        equity away to investors or borrowing cash from family and friends. Keep doing it your way,
                        and we’ll help you access the capital you need.</p>
                    <p><a class="primary-button mt-4" href="../../bp/basic-info.html" data-wpel-link="internal">See
                            your options</a></p>
                </div>
                <div class="col-12 text-center my-auto offset-md-1 col-md-5 pb-5 py-md-4"> <img width="515" height="380" src="../images/startup-loans.svg" class="attachment-full size-full" alt="Startup Loan" srcset="../images/startup-loans.svg 515w,  ../images/startup-loans.svg 300w" sizes="(max-width: 515px) 100vw, 515px" data-wp-pid="86271" nopin="nopin" title="Startup Loan" /></div>
            </div>
        </div>
    </div>
    <div class="alt-grey py-lg-2">
        <div class="container py-0 py-md-5">
            <div class="row">
                <div class="col-12 py-5 text-center">
                    <h2>Explore small business loan options from these leading lenders</h2>
                    <div class="container lender-logos">
                        <div class="row d-flex justify-content-center">
                            <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                <img class="lender-block-img" src="<?= $home_url ?>/images/chase-logo.png" alt="bank-of-america-logo" />
                            </div>
                            <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                <img class="lender-block-img" src="<?= $home_url ?>/images/amex-logo.png" alt="bank-of-america-logo" />
                            </div>
                            <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                <img class="lender-block-img" src="<?= $home_url ?>/images/bank-of-america-logo.png" alt="bank-of-america-logo" />
                            </div>
                        </div>
                    </div> <a href="../../bp/lenders.html" class="primary-button mt-5" data-wpel-link="internal">Find your lender</a>
                </div>
            </div>
        </div>
    </div>
    <div class="py-5 alt-white">
        <div class="container py-0 py-lg-5">
            <div class="row">
                <!-- <div class="col-2 col-md-1 offset-md-1 py-4"> 
                        <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" />
                    </div> -->
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2>Get to Know Startup Business Loans</h2>
                    <p>Be your own startup accelerator with a startup business loan. A financial cushion can take
                        some of the unnecessary stress and backbreaking work out of building a business. A startup
                        business loan can enable you to hire staff, lease office space, increase inventory, buy
                        equipment, or simply cover monthly expenses while you’re growing.</p>
                    <h3>How can a startup loan accelerate my small business?</h3>
                    <p>A startup loan empowers you to invest in your own business. Instead of giving up equity to
                        investors, a startup loan maintains your equity while accessing the working capital your
                        startup needs so you can move into that larger office space or order the inventory you need
                        to fulfill those massive purchase orders that keep rolling in.</p>
                    <h3>Can I get a startup business loan with bad credit?</h3>
                    <p>Startup loans are awarded with an emphasis on the business owner’s personal credit history.
                        That’s one of the aspects that makes startup business loans such an accessible financing
                        option for new businesses.</p>
                    <p>If you have a poor credit history, you may still qualify. Before you commit to a startup
                        loan, you may want to consider what’s affected your credit history in the past. If you have
                        a history of late payments, it’s in your best interest to consider whether you can afford
                        this loan and if you can meet the payment schedule. For those looking to build credit, a
                        business credit card can be an excellent fit because it provides access to financing and an
                        opportunity to build credit for your business—and you’re not required to use the capital.
                    </p>
                </div>
                <!-- <div class="col-2 col-md-1 offset-md-1 py-4"> 
                        <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" />
                    </div> -->
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2>Requirements for a Startup Loan</h2>
                    <p>Typical requirements for a startup loan:</p>
                    <ul class="checkmark-list">
                        <li><a href="../../business-credit/index.html" data-wpel-link="internal">Credit score</a> of
                            680 or higher</li>
                        <li>6 months (or more) in business</li>
                    </ul>
                    <p>Most lenders will also want to see if you have experience in a field related to your small
                        business startup. Some lenders may require you to secure your loan with collateral, which
                        can include assets like a car or a house.</p>
                    <h2>How to Apply for a Startup Business Loan The Lendio Way</h2>
                    <p>Apply for a startup loan in the time it takes to pick up your morning latte. Fill out a
                        single 15-minute application to compare startup loan options from our curated network of 75+
                        lenders.</p>
                    <p>&nbsp;</p>
                </div>
                <!-- <div class="col-2 col-md-1 offset-md-1 py-4"> 
                        <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" />
                    </div> -->
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2>Know How Much You Can Afford</h2>
                    <p>Borrowing only what you know you can repay makes good sense at any time, but it’s especially
                        important when you’re starting a business. Before signing on the dotted line, make sure
                        you’ve calculated how much financing your small business can afford.</p>
                    <p>In addition to your loan amount, startup loans are determined by your interest rate, term,
                        and collateral. These factors can vary substantially depending on the type of startup loan
                        you choose. You can use our startup loan calculator to estimate your monthly payments.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="alt-white text-center py-5 border-top">
            <div class="container py-0 py-lg-5">
                <div class="row px-2">
                    <div class="col-2 col-md-1 offset-md-1 py-4"> 
                        <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" />
                    </div>
                    <div class="col-12 py-4">
                        <h3>I recently started a small inventory-based business and needed financing to help cover
                            startup costs. Lendio and its representatives came through for me in spectacular fashion. I
                            was able to obtain both business and personal credit lines that allowed me to order
                            merchandise to be fully stocked ahead of our opening. It did not stop there, though! They
                            offer counseling to maximize my credit profile to ensure I can continue obtaining financing
                            as my business grows. The confidence with which Lendio handled my application was a relief.
                            Thanks for everything!</h3>
                        <h3>Jose Q.</h3>
                    </div>
                </div>
            </div>
        </div> -->
</div>

<!-- ==== Page(Startup-Loan) Blue-Bar Start ==== -->
<div class="bottom-cta full base dark pt-2">
    <div class="container cta-bottom py-5">
        <div class="row">
            <div class="col-12 py-md-5 text-center">
                <h2>Your startup loan is waiting.</h2>
                <form action="https://www.lendio.com/bp/basic-info" method="GET" class="cta-content d-inline-block my-4"> <input type="hidden" name="referral_url" value="https://www.google.com/" />
                    <div class="input-container col-lg-8 float-lg-left mb-3"> <span class="dollar d-none d-lg-block">$</span> <input type="text" placeholder="How much do you need?" class="amount-seeking-input text-center pl-0" name="amountSeeking" /></div>
                    <div class="button-container col-lg-4 float-lg-left"> <button class="primary-button w-lg-100 ml-0 mb-2">Start</button></div>
                    <div class="clear"></div>
                </form>
                <h5 class="px-5" style="font-size: 17px;">Applying is free and it won't impact your credit</h5>
                <h5 style="font-size: 17px;">Already have an account? &nbsp;<a class="text-uppercase font-weight-bold" href="../../bp/login.html" data-wpel-link="internal"><strong>Sign in</strong></a></h5>
            </div>
        </div>
    </div>
</div>

<?php include('../footer.php'); ?>
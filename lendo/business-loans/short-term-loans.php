<?php include('../header.php'); ?>

<!-- ==== Page(Short-Term-Loan) Main Start ==== -->
<div id="short-term-loans">
    <div class="hero pt-md-5" style="color:#fff;background: #4000dc;background: -moz-linear-gradient(-45deg, #4000dc 0%, #05c2f0 100%);background: -webkit-linear-gradient(-45deg, #4000dc 0%,#05c2f0 100%);background: linear-gradient(135deg, #4000dc 0%,#05c2f0 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#4000dc', endColorstr='#05c2f0',GradientType=1 );">
        <div class="container py-md-5">
            <div class="row py-md-2 hero-row">
                <div class="hero-responsive col-lg-6 text-left align-middle my-2">
                    <img src="<?= $home_url ?>/images/short-term-loans-icon.svg" class="attachment-60x60 size-60x60 wp-post-image" alt="Short term loan" height="63" style="min-width:60px;display:block;" width="49" data-wp-pid="86275" nopin="nopin" title="Short Term Loan" />
                    <h1>Short Term Loan</h1>
                    <h5 class="my-3">If loans were athletes, this would be the sprinter.</h5>
                </div>
                <div class="col-lg-6 text-center align-middle my-2">
                    <div class="cta p-5">
                        <h3>Let's get started.</h3>
                        <form action="https://www.lendio.com/bp/basic-info" method="GET" class="cta-content"> <input type="hidden" name="nosignup" value="1" />
                            <div class="input-container"> <span class="dollar">$</span> <input type="text" placeholder="How much do you need?" name="amountSeeking" /></div> <button class="primary-button">See your options</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="alt-header-swap"></div>
    <!-- Grey Section -->
    <div class="py-5 alt-grey">
        <div class="container my-5">
            <div class="col-12 mb-5"></div>
            <div class="row">
                <div id=" " class="col-12 col-md-6 d-md-block col-lg-6">
                    <div class="container-fluid">
                        <div class="row pb-4">
                            <div class="col-2">
                                <img src="<?= $home_url ?>/images/dollar-icon.svg" class="d-block pr-2" alt="Loan Amount Icon" style="min-width:55px;" height="34" width="31" data-wp-pid="91618" nopin="nopin" />
                            </div>
                            <div class="col-10">
                                <h3 class="pb-md-0" itemprop="name">Loan Amount</h3>
                                <div class="fact-text pt-md-0 pb-md-4 pr-3">
                                    <h5 class="pt-0" itemprop="value">
                                        <p>$1,000 &#8211; $500,000</p>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="collapseFacts" class="col-12 col-md-6 d-md-block collapse multi-collapse col-lg-6">
                    <div class="container-fluid">
                        <div class="row pb-4">
                            <div class="col-2">
                                <img src="<?= $home_url ?>/images/term-icon.svg" class="d-block pr-2" alt="Loan Term Icon" style="min-width:55px;" height="34" width="33" data-wp-pid="91619" nopin="nopin" />
                            </div>
                            <div class="col-10">
                                <h3 class="pb-md-0" itemprop="name">Loan Term</h3>
                                <div class="fact-text pt-md-0 pb-md-4 pr-3">
                                    <h5 class="pt-0" itemprop="value">
                                        <p>1-2 Year Maturity</p>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id=" " class="col-12 col-md-6 d-md-block col-lg-6">
                    <div class="container-fluid">
                        <div class="row pb-4">
                            <div class="col-2">
                                <img src="<?= $home_url ?>/images/time-icon.svg" class="d-block pr-2" alt="Loan Time Icon" style="min-width:55px;" height="34" width="34" data-wp-pid="91620" nopin="nopin" />
                            </div>
                            <div class="col-10">
                                <h3 class="pb-md-0" itemprop="name">Time to Funds</h3>
                                <div class="fact-text pt-md-0 pb-md-4 pr-3">
                                    <h5 class="pt-0" itemprop="value">
                                        <p>As soon as 1-2 Weeks</p>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="collapseFacts" class="col-12 col-md-6 d-md-block collapse multi-collapse col-lg-6">
                    <div class="container-fluid">
                        <div class="row pb-4">
                            <div class="col-2">
                                <img src="<?= $home_url ?>/images/percent-icon.svg" class="d-block pr-2" alt="Loan Interest Icon" style="min-width:55px;" height="32" width="32" data-wp-pid="91621" nopin="nopin" />
                            </div>
                            <div class="col-10">
                                <h3 class="pb-md-0" itemprop="name">Interest Rate</h3>
                                <div class="fact-text pt-md-0 pb-md-4 pr-3">
                                    <h5 class="pt-0" itemprop="value">
                                        <p>As low as 8-24%</p>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3 col-12 col-md-6 text-left px-0 border-none d-md-none"> <a id="a-tag" class="justify-content-center d-block text-center font-weight-bold" data-toggle="collapse" href="#collapseFacts" role="button" aria-expanded="false" aria-controls="collapseFacts">
                        <h4 id="collapseFactsLink" class="font-weight-bold p-0 m-0">See All</h4> <svg id="collapse-arrow" class="bi bi-chevron-down" width="25" height="25" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M3.646 6.646a.5.5 0 01.708 0L10 12.293l5.646-5.647a.5.5 0 01.708.708l-6 6a.5.5 0 01-.708 0l-6-6a.5.5 0 010-.708z" clip-rule="evenodd"></path>
                        </svg>
                    </a></div>
            </div>
        </div>
    </div>

    <div class="alt-white py-5">
        <div class="container py-lg-0 py-5">
            <div class="row">
                <div class="col-12 col-md-6 py-5 text-center text-md-left">
                    <h2>Sometimes speed is everything.</h2>
                    <p>No matter how much planning you do, small business ownership is full of surprises. Thinking
                        on your feet and coming up with quick solutions is often the difference between shutting
                        your doors and shutting out the competition. When time is money, a short term loan can get
                        you financing in as little as 24 hours.</p>
                    <p><a class="primary-button mt-4" href="../../bp/basic-info.html" data-wpel-link="internal">See
                            your options</a></p>
                </div>
                <div class="col-12 text-center my-auto offset-md-1 col-md-5 pb-5 py-md-4"> <img width="515" height="380" src="../images/short-term-loan.svg" class="attachment-full size-full" alt="Short Term Loan" srcset="../images/short-term-loan.svg 515w, ../images/short-term-loan.svg 300w" sizes="(max-width: 515px) 100vw, 515px" data-wp-pid="86357" nopin="nopin" title="Short Term Loan" /></div>
            </div>
        </div>
    </div>
    <div class="alt-grey py-lg-2">
        <div class="container py-0 py-md-5">
            <div class="row">
                <div class="col-12 py-5 text-center">
                    <h2>Explore small business loan options from these leading lenders</h2>
                    <div class="container lender-logos">
                        <div class="row d-flex justify-content-center">
                            <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                <img class="lender-block-img" src="<?= $home_url ?>/images/chase-logo.png" alt="chase-logo">
                            </div>
                            <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                <img class="lender-block-img" src="<?= $home_url ?>/images/amex-logo.png" alt="amex-logo">
                            </div>
                            <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                <img class="lender-block-img" src="<?= $home_url ?>/images/lencred-logo.png" alt="amex-logo">
                            </div>
                            <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                <img class="lender-block-img" src="<?= $home_url ?>/images/bank-of-america-logo.png" alt="amex-logo">
                            </div>
                            <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                <img class="lender-block-img" src="<?= $home_url ?>/images/seek-capital-logo.png" alt="amex-logo">
                            </div>
                        </div>
                    </div>
                    <a href="../../bp/lenders.html" class="primary-button mt-5" data-wpel-link="internal">Find your lender</a>
                </div>
            </div>
        </div>
    </div>
    <div class="py-5 alt-white">
        <div class="container py-0 py-lg-5">
            <div class="row">
                <!-- <div class="col-2 col-md-1 offset-md-1 py-4"> 
                    <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" />
                </div> -->
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2>It’s not just fast &#8211; it’s also flexible.</h2>
                    <p>A short term loan is like the Swiss Army Knife of loans &#8211; it’s handy, flexible, and
                        able to get you out of a bind. You can use it to cover unexpected costs, survive a slump,
                        finance a short term project, or even capitalize on a new business opportunity. It’s
                        definitely the loan you want in your back pocket.</p>
                    <p>&nbsp;</p>
                </div>
                <!-- <div class="col-2 col-md-1 offset-md-1 py-4"> 
                        <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" />
                    </div> -->
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2>Scoring a short term loan isn’t as hard as you think.</h2>
                    <p>If you have <a href="../../business-credit/index.html" data-wpel-link="internal">solid
                            credit</a> and you’ve been in business for two years or more, you have a good chance of
                        qualifying for a short term loan. Your lender may also ask you to put down some collateral
                        to secure the loan.</p>
                    <p>You can find out if you qualify in less time than it takes to pick up your morning latte:
                        just fill out our no-cost, no-obligation <a href="../../bp/basic-infoced1.html?step=0&amp;page=0&amp;nosignup=1" data-wpel-link="internal">application</a> to compare short term loan options from 75+
                        lenders. Now there’s a fine way to start your day.</p>
                </div>
                <!-- <div class="col-2 col-md-1 offset-md-1 py-4"> 
                        <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" />
                    </div> -->
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2>Our team can help you find the best deal.</h2>
                    <p>You may be in a hurry to get financing, but you don’t want to rush into the wrong short term
                        loan. That’s where our experts come in. Our personal funding managers can help you compare
                        <a href="../../blog/small-business-tools/business-loan-interest-rates/index.html" data-wpel-link="internal">rates</a>, terms, and payments for a variety of loan options
                        so you can pick the right one for your business. Which means you don’t just get a short term
                        loan, you also get the best deal out there.
                    </p>
                    <p>Use our <a href="../../business-calculators/short-term-loan-calculator/index.html" data-wpel-link="internal">short term loan calculator</a> to figure out how much you can
                        afford, then <a href="../../bp/basic-infoced1.html?step=0&amp;page=0&amp;nosignup=1" data-wpel-link="internal">see your options</a>.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="alt-white text-center py-5 border-top">
            <div class="container py-0 py-lg-5">
                <div class="row px-2">
                    <div class="col-12 py-4"> 
                        <img src="<?= $home_url ?>/images/quotes.svg" alt="Quotes" />
                        </div>
                    <div class="col-12 py-4">
                        <h3>I worked with Doug on some short term financing for our slow season and upcoming dealer
                            shows. I learned a lot from him about our business credit. He was great at communicating
                            everything to me and the process was easy.</h3>
                        <h3>Brad</h3>
                    </div>
                </div>
            </div>
        </div> -->
</div>
<div class="bottom-cta full base dark pt-2">
    <div class="container cta-bottom py-5">
        <div class="row">
            <div class="col-12 py-md-5 text-center">
                <h2>Your short term loan is waiting.</h2>
                <form action="https://www.lendio.com/bp/basic-info" method="GET" class="cta-content d-inline-block my-4">
                    <div class="input-container col-lg-8 float-lg-left mb-3"> <span class="dollar d-none d-lg-block">$</span> <input type="text" placeholder="How much do you need?" class="amount-seeking-input text-center pl-0" name="amountSeeking" /></div>
                    <div class="button-container col-lg-4 float-lg-left"> <button class="primary-button w-lg-100 ml-0 mb-2">Start</button></div>
                    <div class="clear"></div>
                </form>
                <h5 class="px-5" style="font-size: 17px;">Applying is free and it won't impact your credit</h5>
                <h5 style="font-size: 17px;">Already have an account? &nbsp;<a class="text-uppercase font-weight-bold" href="../../bp/login.html" data-wpel-link="internal"><strong>Sign in</strong></a></h5>
            </div>
        </div>
    </div>
</div>

<?php include('../footer.php'); ?>
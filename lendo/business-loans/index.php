<?php include('../header.php'); ?>

    <!-- ==== Page(Loan-Type) Main Start ==== -->
    <div id="small-business-loans">
        <div class="hero white">
            <div class="hero-content">
                <div class="hero-copy">
                    <h1>Business loan options of all shapes and sizes.</h1>
                    <p>Find the financing solution that actually fits your business - and your wallet.</p>
                </div>
                <div class="cta-new">
                    <form action="https://www.lendio.com/bp/basic-info" method="GET" class="cta-content"> <input
                            type="hidden" name="nosignup" value="1" /> <button class="primary-button">See your
                            options</button></form>
                </div>
                <div class="hero-icon"></div>
            </div>
        </div>
        <div id="alt-header-swap"></div>
        <div class="cards-list">
            <div class="cards-list-content">
                <div class="cards-list-items-container"> <a href="business-loans/line-of-credit/index.html"
                        data-wpel-link="internal"> <span class="cards-list-item"> <span
                                class="cards-list-inner-container"> <span class="cards-list-icon-container"> <img
                                        src="<?= $home_url ?>/images/Bus-line-of-credit-icon.svg"
                                        alt="Business Line of Credit" /> </span> <span
                                    class="cards-list-text-container">
                                    <h3>Business Line of Credit</h3> <span class="cards-excerpt">
                                        <p>Gain access to additional cash whenever you need it.</p>
                                    </span>
                                </span> </span> </span> </a> <a href="business-loans/sba-loans/index.html"
                        data-wpel-link="internal"> <span class="cards-list-item"> <span
                                class="cards-list-inner-container"> <span class="cards-list-icon-container"> <img
                                        src="<?= $home_url ?>/images/SBA-loan-icon.svg" alt="SBA Loan" /> </span>
                                <span class="cards-list-text-container">
                                    <h3>SBA Loan</h3> <span class="cards-excerpt">
                                        <p>Get a boost with the SBA 7a, 504, or Express loan.</p>
                                    </span>
                                </span> </span> </span> </a> <a href="business-loans/short-term-loans/index.html"
                        data-wpel-link="internal"> <span class="cards-list-item"> <span
                                class="cards-list-inner-container"> <span class="cards-list-icon-container"> <img
                                        src="<?= $home_url ?>/images/Short-term-loan-icon.svg"
                                        alt="Short Term Loan" /> </span> <span class="cards-list-text-container">
                                    <h3>Short Term Loan</h3> <span class="cards-excerpt">
                                        <p>Flexible business financing with 1-3 year terms.</p>
                                    </span>
                                </span> </span> </span> </a> <a href="business-loans/cash-advance/index.html"
                        data-wpel-link="internal"> <span class="cards-list-item"> <span
                                class="cards-list-inner-container"> <span class="cards-list-icon-container"> <img
                                        src="<?= $home_url ?>/images/Merch-cash-advance-icon.svg"
                                        alt="Merchant Cash Advance" /> </span> <span class="cards-list-text-container">
                                    <h3>Merchant Cash Advance</h3> <span class="cards-excerpt">
                                        <p>Borrow against your future earnings to get cash now.</p>
                                    </span>
                                </span> </span> </span> </a> <a href="business-loans/term-loans/index.html"
                        data-wpel-link="internal"> <span class="cards-list-item"> <span
                                class="cards-list-inner-container"> <span class="cards-list-icon-container"> <img
                                        src="<?= $home_url ?>/images/Bus-term-loan-icon.svg"
                                        alt="Business Term Loan" /> </span> <span class="cards-list-text-container">
                                    <h3>Business Term Loan</h3> <span class="cards-excerpt">
                                        <p>This standard loan is a go-to for many business owners.</p>
                                    </span>
                                </span> </span> </span> </a> <a href="business-loans/business-credit-card/index.html"
                        data-wpel-link="internal"> <span class="cards-list-item"> <span
                                class="cards-list-inner-container"> <span class="cards-list-icon-container"> <img
                                        src="<?= $home_url ?>/images/Bus-credit-card-icon.svg"
                                        alt="Business Credit Card" /> </span> <span class="cards-list-text-container">
                                    <h3>Business Credit Card</h3> <span class="cards-excerpt">
                                        <p>Increase working capital while building business credit.</p>
                                    </span>
                                </span> </span> </span> </a> <a href="business-loans/equipment-financing/index.html"
                        data-wpel-link="internal"> <span class="cards-list-item"> <span
                                class="cards-list-inner-container"> <span class="cards-list-icon-container"> <img
                                        src="<?= $home_url ?>/images/Equipment-loan-icon.svg"
                                        alt="Equipment Financing" /> </span> <span class="cards-list-text-container">
                                    <h3>Equipment Financing</h3> <span class="cards-excerpt">
                                        <p>Finance any business tool, from tractors to software.</p>
                                    </span>
                                </span> </span> </span> </a> <a href="business-loans/commercial-real-estate/index.html"
                        data-wpel-link="internal"> <span class="cards-list-item"> <span
                                class="cards-list-inner-container"> <span class="cards-list-icon-container"> <img
                                        src="<?= $home_url ?>/images/Commercial-mortgage-icon.svg"
                                        alt="Commercial Mortgage" /> </span> <span class="cards-list-text-container">
                                    <h3>Commercial Mortgage</h3> <span class="cards-excerpt">
                                        <p>Funding to buy, build, expand, remodel, or refinance.</p>
                                    </span>
                                </span> </span> </span> </a> <a
                        href="business-loans/accounts-receivable-financing/index.html" data-wpel-link="internal"> <span
                            class="cards-list-item"> <span class="cards-list-inner-container"> <span
                                    class="cards-list-icon-container"> <img
                                        src="<?= $home_url ?>/images/Acc-Rec-Icon.svg"
                                        alt="Accounts Receivable Financing" /> </span> <span
                                    class="cards-list-text-container">
                                    <h3>Accounts Receivable Financing</h3> <span class="cards-excerpt">
                                        <p>Get an advance on your outstanding receivables.</p>
                                    </span>
                                </span> </span> </span> </a> <a href="business-loans/startup-loans/index.html"
                        data-wpel-link="internal"> <span class="cards-list-item"> <span
                                class="cards-list-inner-container"> <span class="cards-list-icon-container"> <img
                                        src="<?= $home_url ?>/images/Startup-loan-icon.svg" alt="Startup Loan" />
                                </span> <span class="cards-list-text-container">
                                    <h3>Startup Loan</h3> <span class="cards-excerpt">
                                        <p>Launch your new business without giving up any equity.</p>
                                    </span>
                                </span> </span> </span> </a> <a href="business-loans/acquisition-loan/index.html"
                        data-wpel-link="internal"> <span class="cards-list-item"> <span
                                class="cards-list-inner-container"> <span class="cards-list-icon-container"> <img
                                        src="<?= $home_url ?>/images/Bus-Aqu-loan-icon.svg"
                                        alt="Business Acquisition Loan" /> </span> <span
                                    class="cards-list-text-container">
                                    <h3>Business Acquisition Loan</h3> <span class="cards-excerpt">
                                        <p>Get financing to buy an existing business or franchise.</p>
                                    </span>
                                </span> </span> </span> </a>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <div class="cta-new full white">
            <div class="cta-content lg">
                <h2>Secure your business loan today.</h2>
                <form action="https://www.lendio.com/bp/basic-info" method="GET"
                    class="cta-content ng-pristine ng-valid">
                    <div class="input-container"> <span class="dollar">$</span> <input type="text"
                            placeholder="How much do you need?" name="amountSeeking"></div> <button
                        class="primary-button">See your options</button>
                </form>
                <div class="clear"></div>
            </div>
        </div>
        <div class="loan-full-list">
            <h2>Learn about our Small Business Loan Products</h2>
            <div class="loan-list-items"> <a name="business-line-of-credit" class="anchor"></a>
                <div class="loan-list-item">
                    <div class="loan-list-content">
                        <h3><a href="business-loans/line-of-credit/index.html" data-wpel-link="internal">Business Line
                                of Credit</a></h3>
                        <p>
                        <p>Need a little extra cash now and then? A business line of credit gives you access to working
                            capital when you need it. You can use your line of credit for most business needs, and you
                            pay interest only on the funds you use. It's an easy way to give your small business a
                            boost.</p>
                        </p>
                    </div>
                    <div class="clear"></div>
                </div> <a name="sba-loan" class="anchor"></a>
                <div class="loan-list-item">
                    <div class="loan-list-content">
                        <h3><a href="business-loans/sba-loans/index.html" data-wpel-link="internal">SBA Loan</a></h3>
                        <p>
                        <p>SBA loans were developed by Uncle Sam to support small business growth. They’re offered by
                            lenders and backed by the U.S. Small Business Administration (<a href="https://www.sba.gov/"
                                data-wpel-link="external" target="_blank" rel="external noopener noreferrer">SBA</a>).
                            We have several different SBA loan options in our toolbag, including SBA 7a, SBA 504, and
                            SBA Express loans.</p>
                        <p>Read our in-depth guide to <a
                                href="blog/small-business-tools/sba-loan-requirements/index.html"
                                data-wpel-link="internal">SBA loan requirements</a>.</p>
                        </p>
                    </div>
                    <div class="clear"></div>
                </div> <a name="short-term-loan" class="anchor"></a>
                <div class="loan-list-item">
                    <div class="loan-list-content">
                        <h3><a href="business-loans/short-term-loans/index.html" data-wpel-link="internal">Short Term
                                Loan</a></h3>
                        <p>
                        <p>Surprises happen - and that’s why short term loans exist. It gives you the green you need to
                            stay afloat during a temporary cash shortage or manage the overhead that comes with taking
                            on a larger project. You can get funded in just 1-4 weeks so you can get back to business as
                            usual.</p>
                        <p>Read our in-depth guide to <a
                                href="blog/small-business-tools/guide-short-term-loans/index.html"
                                data-wpel-link="internal">short term loans</a>.</p>
                        </p>
                    </div>
                    <div class="clear"></div>
                </div> <a name="merchant-cash-advance" class="anchor"></a>
                <div class="loan-list-item">
                    <div class="loan-list-content">
                        <h3><a href="business-loans/cash-advance/index.html" data-wpel-link="internal">Merchant Cash
                                Advance</a></h3>
                        <p>
                        <p>Because sometimes you just need to get paid. Use a merchant cash advance to borrow against
                            your future earnings and put money in your pocket in as little as 24 hours. It’s easy to
                            qualify for and doesn’t require you to put up collateral or give up any equity in your small
                            business.</p>
                        </p>
                    </div>
                    <div class="clear"></div>
                </div> <a name="business-term-loan" class="anchor"></a>
                <div class="loan-list-item">
                    <div class="loan-list-content">
                        <h3><a href="business-loans/term-loans/index.html" data-wpel-link="internal">Business Term
                                Loan</a></h3>
                        <p>
                        <p>This standard business loan option offers fixed interest rates, regular repayment terms, and
                            a fixed maturity date. See, pretty standard. Use your business term loan for anything from
                            an expansion to an equipment purchase. Crunch the numbers, then apply for your term loan
                            today.</p>
                        </p>
                    </div>
                    <div class="clear"></div>
                </div> <a name="business-credit-card" class="anchor"></a>
                <div class="loan-list-item">
                    <div class="loan-list-content">
                        <h3><a href="business-loans/business-credit-card/index.html" data-wpel-link="internal">Business
                                Credit Card</a></h3>
                        <p>
                        <p>Whether you’re just starting out or a well-established business, a business credit card is
                            the no-brainer way to cover unexpected costs and improve your business credit score. Choose
                            one with a rewards program to bolster your cash benefits or just get some cool free stuff
                            online.</p>
                        </p>
                    </div>
                    <div class="clear"></div>
                </div> <a name="equipment-financing" class="anchor"></a>
                <div class="loan-list-item">
                    <div class="loan-list-content">
                        <h3><a href="business-loans/equipment-financing/index.html" data-wpel-link="internal">Equipment
                                Financing</a></h3>
                        <p>
                        <p>Yes, you can afford that new truck, telephone system, or convection oven. Maybe even all
                            three, although that’s a weird combo. Because business equipment varies so much between
                            industries, you can choose from several different equipment financing options.</p>
                        <p>Read our in-depth guide to <a
                                href="blog/small-business-tools/equipment-financing-business/index.html"
                                data-wpel-link="internal">equipment financing</a>.</p>
                        </p>
                    </div>
                    <div class="clear"></div>
                </div> <a name="commercial-mortgage" class="anchor"></a>
                <div class="loan-list-item">
                    <div class="loan-list-content">
                        <h3><a href="business-loans/commercial-real-estate/index.html"
                                data-wpel-link="internal">Commercial Mortgage</a></h3>
                        <p>
                        <p>A commercial mortgage can help you buy, build, expand, remodel, or refinance. And it offers
                            several sweet benefits: it’s a secure piece of collateral, typically has low interest rates,
                            and helps you start earning equity. Plus, building stuff is just the grownup version of
                            playing with Legos.</p>
                        </p>
                    </div>
                    <div class="clear"></div>
                </div> <a name="accounts-receivable-financing" class="anchor"></a>
                <div class="loan-list-item">
                    <div class="loan-list-content">
                        <h3><a href="business-loans/accounts-receivable-financing/index.html"
                                data-wpel-link="internal">Accounts Receivable Financing</a></h3>
                        <p>
                        <p>Stop trying to find extra cash while you’re waiting for those Net-30 receivables to roll in.
                            Instead, use accounts receivable financing to get an advance from a lender on the money
                            you’re owed for completed services. Cash flow problems solved.</p>
                        </p>
                    </div>
                    <div class="clear"></div>
                </div> <a name="startup-loan" class="anchor"></a>
                <div class="loan-list-item">
                    <div class="loan-list-content">
                        <h3><a href="business-loans/startup-loans/index.html" data-wpel-link="internal">Startup Loan</a>
                        </h3>
                        <p>
                        <p>Use a startup loan to launch your new business without giving up any equity - and establish
                            your business credit in the process. All you need is a credit score of 680 or higher and
                            possibly a little collateral. So easy even a kid could do it.</p>
                        </p>
                    </div>
                    <div class="clear"></div>
                </div> <a name="business-acquisition-loan" class="anchor"></a>
                <div class="loan-list-item">
                    <div class="loan-list-content">
                        <h3><a href="business-loans/acquisition-loan/index.html" data-wpel-link="internal">Business
                                Acquisition Loan</a></h3>
                        <p>
                        <p>A business acquisition loan helps you get the funding you need to purchase an existing small
                            business or franchise. Simply submit your business plan and financial projections, then
                            we’ll help you get the cash - now there’s a deal you can shake on.</p>
                        </p>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <div class="bottom-cta pt-2 full base dark">
            <div class="cta-content lg">
                <div class="container cta-bottom py-5">
                    <div class="row">
                        <div class="col-12 py-md-5 text-center">
                            <h2>Get your small business loan today.</h2>
                            <form action="https://www.lendio.com/bp/basic-info" method="GET"
                                class="cta-content d-inline-block my-4">
                                <div class="input-container col-lg-8 float-lg-left mb-3"> <span
                                        class="dollar d-none d-lg-block">$</span> <input type="text"
                                        placeholder="How much do you need?"
                                        class="amount-seeking-input text-center pl-0" name="amountSeeking" /></div>
                                <div class="button-container col-lg-4 float-lg-left"> <button
                                        class="primary-button w-lg-100 ml-0 mb-2">Start</button></div>
                                <div class="clear"></div>
                            </form>
                            <h5 class="px-5" style="font-size: 17px;">Applying is free and it won't impact your credit
                            </h5>
                            <h5 style="font-size: 17px;">Already have an account? &nbsp;<a
                                    class="text-uppercase font-weight-bold" href="bp/login.html"
                                    data-wpel-link="internal"><strong>Sign in</strong></a></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php include('../footer.php'); ?>
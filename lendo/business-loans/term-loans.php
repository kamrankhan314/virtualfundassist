<?php include('../header.php'); ?>

<!-- ==== Page(Business-Term-Loans) Main Start ==== -->
<div id="term-loans">
    <div class="hero pt-md-5" style="color:#fff;background: #4000dc;background: -moz-linear-gradient(-45deg, #4000dc 0%, #05c2f0 100%);background: -webkit-linear-gradient(-45deg, #4000dc 0%,#05c2f0 100%);background: linear-gradient(135deg, #4000dc 0%,#05c2f0 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#4000dc', endColorstr='#05c2f0',GradientType=1 );">
        <div class="container py-md-5">
            <div class="row py-md-2 hero-row">
                <div class="hero-responsive col-lg-6 text-left align-middle my-2">
                    <img src="<?= $home_url ?>/images/business-term-loan-icon.svg" class="attachment-60x60 size-60x60 wp-post-image" alt="Business term loan icon" height="58" style="min-width:60px;display:block;" width="59" data-wp-pid="86281" nopin="nopin" title="Business Term Loan" />
                    <h1>Business Term Loan</h1>
                    <h5 class="my-3">The classic way to boost your small business.</h5>
                </div>
                <div class="col-lg-6 text-center align-middle my-2">
                    <div class="cta p-5">
                        <h3>Let's get started.</h3>
                        <form action="https://www.lendio.com/bp/basic-info" method="GET" class="cta-content"> <input type="hidden" name="nosignup" value="1" /> <input type="hidden" name="referral_url" value="https://www.google.com/" />
                            <div class="input-container"> <span class="dollar">$</span> <input type="text" placeholder="How much do you need?" name="amountSeeking" /></div> <button class="primary-button">See your options</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="alt-header-swap"></div>
    <!-- Grey Section -->
    <div class="py-5 alt-grey">
        <div class="container my-5">
            <div class="col-12 mb-5"></div>
            <div class="row">
                <div id=" " class="col-12 col-md-6 d-md-block col-lg-6">
                    <div class="container-fluid">
                        <div class="row pb-4">
                            <div class="col-2">
                                <img src="<?= $home_url ?>/images/dollar-icon.svg" class="d-block pr-2" alt="Loan Amount Icon" style="min-width:55px;" height="34" width="31" data-wp-pid="91618" nopin="nopin" />
                            </div>
                            <div class="col-10">
                                <h3 class="pb-md-0" itemprop="name">Loan Amount</h3>
                                <div class="fact-text pt-md-0 pb-md-4 pr-3">
                                    <h5 class="pt-0" itemprop="value">
                                        <p>$1,000 &#8211; $500,000</p>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="collapseFacts" class="col-12 col-md-6 d-md-block collapse multi-collapse col-lg-6">
                    <div class="container-fluid">
                        <div class="row pb-4">
                            <div class="col-2">
                                <img src="<?= $home_url ?>/images/term-icon.svg" class="d-block pr-2" alt="Loan Term Icon" style="min-width:55px;" height="34" width="33" data-wp-pid="91619" nopin="nopin" />
                            </div>
                            <div class="col-10">
                                <h3 class="pb-md-0" itemprop="name">Loan Term</h3>
                                <div class="fact-text pt-md-0 pb-md-4 pr-3">
                                    <h5 class="pt-0" itemprop="value">
                                        <p>1-2 Year Maturity</p>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id=" " class="col-12 col-md-6 d-md-block col-lg-6">
                    <div class="container-fluid">
                        <div class="row pb-4">
                            <div class="col-2">
                                <img src="<?= $home_url ?>/images/time-icon.svg" class="d-block pr-2" alt="Loan Time Icon" style="min-width:55px;" height="34" width="34" data-wp-pid="91620" nopin="nopin" />
                            </div>
                            <div class="col-10">
                                <h3 class="pb-md-0" itemprop="name">Time to Funds</h3>
                                <div class="fact-text pt-md-0 pb-md-4 pr-3">
                                    <h5 class="pt-0" itemprop="value">
                                        <p>As soon as 1-2 Weeks</p>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="collapseFacts" class="col-12 col-md-6 d-md-block collapse multi-collapse col-lg-6">
                    <div class="container-fluid">
                        <div class="row pb-4">
                            <div class="col-2">
                                <img src="<?= $home_url ?>/images/percent-icon.svg" class="d-block pr-2" alt="Loan Interest Icon" style="min-width:55px;" height="32" width="32" data-wp-pid="91621" nopin="nopin" />
                            </div>
                            <div class="col-10">
                                <h3 class="pb-md-0" itemprop="name">Interest Rate</h3>
                                <div class="fact-text pt-md-0 pb-md-4 pr-3">
                                    <h5 class="pt-0" itemprop="value">
                                        <p>As low as 8-24%</p>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3 col-12 col-md-6 text-left px-0 border-none d-md-none"> <a id="a-tag" class="justify-content-center d-block text-center font-weight-bold" data-toggle="collapse" href="#collapseFacts" role="button" aria-expanded="false" aria-controls="collapseFacts">
                        <h4 id="collapseFactsLink" class="font-weight-bold p-0 m-0">See All</h4> <svg id="collapse-arrow" class="bi bi-chevron-down" width="25" height="25" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M3.646 6.646a.5.5 0 01.708 0L10 12.293l5.646-5.647a.5.5 0 01.708.708l-6 6a.5.5 0 01-.708 0l-6-6a.5.5 0 010-.708z" clip-rule="evenodd"></path>
                        </svg>
                    </a></div>
            </div>
        </div>
    </div>
    <div class="alt-white py-5">
        <div class="container py-lg-0 py-5">
            <div class="row">
                <div class="col-12 col-md-6 py-5 text-center text-md-left">
                    <h2>The classic small business loan</h2>
                    <p>We all love a classic because it’s tried and true. The same goes for a business term
                        loan—it’s a stable, flexible way to get more working capital, grow your business, and build
                        your credit. If you’re craving a simple small business financing solution, this is it.</p>
                    <p><a class="primary-button mt-4" href="../../bp/basic-info.html" data-wpel-link="internal">See
                            your options</a></p>
                </div>
                <div class="col-12 text-center my-auto offset-md-1 col-md-5 pb-5 py-md-4"> <img width="515" height="380" src="../images/term-loans.jpg" class="attachment-full size-full" alt="Business Term Loan" srcset="../images/term-loans.jpg 515w, ../images/term-loans.jpg 300w" sizes="(max-width: 515px) 100vw, 515px" data-wp-pid="86354" nopin="nopin" title="Business Term Loan" /></div>
            </div>
        </div>
    </div>
    <div class="alt-grey py-lg-2">
        <div class="container py-0 py-md-5">
            <div class="row">
                <div class="col-12 py-5 text-center">
                    <h2>Explore small business loan options from these leading lenders</h2>
                    <div class="container lender-logos">
                        <div class="row d-flex justify-content-center">
                            <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                <img class="lender-block-img" src="<?= $home_url ?>/images/chase-logo.png" alt="chase-logo">
                            </div>
                            <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                <img class="lender-block-img" src="<?= $home_url ?>/images/amex-logo.png" alt="amex-logo">
                            </div>
                            <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                <img class="lender-block-img" src="<?= $home_url ?>/images/lencred-logo.png" alt="amex-logo">
                            </div>
                            <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                <img class="lender-block-img" src="<?= $home_url ?>/images/bank-of-america-logo.png" alt="amex-logo">
                            </div>
                            <div class="lender-partner-block col-6 col-md-3 p-4 p-md-5">
                                <img class="lender-block-img" src="<?= $home_url ?>/images/seek-capital-logo.png" alt="amex-logo">
                            </div>
                        </div>
                    </div>
                    <a href="../../bp/lenders.html" class="primary-button mt-5" data-wpel-link="internal">Find your lender</a>
                </div>
            </div>
        </div>
    </div>
    <div class="py-5 alt-white">
        <div class="container py-0 py-lg-5">
            <div class="row">
                <!-- <div class="col-2 col-md-1 offset-md-1 py-4"> 
                        <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" />
                    </div> -->
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2>What is a Term Loan?</h2>
                    <p>A term loan is what you’re probably thinking of when you say “small business loan.” It’s a
                        classic financing option to help power the growth of small businesses. With a business term
                        loan, a small business can borrow a lump sum from a lender that is paid back in steady
                        increments over the set borrowing period, or term.</p>
                </div>
                <!-- <div class="col-2 col-md-1 offset-md-1 py-4"> 
                        <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" />
                    </div> -->
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2>How can you use a business term loan?</h2>
                    <p>Business term loans are set up to meet pretty much any business need, no matter how unique.
                        You can leverage your loan for everything from capital improvements to financing new
                        equipment or hiring more staff.</p>
                </div>
                <!-- <div class="col-2 col-md-1 offset-md-1 py-4"> 
                        <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" />
                    </div> -->
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2>How to Apply for a Business Term Loan</h2>
                    <p>You could do it the hard way, where you spend upward of 29 hours completing a bank
                        application, or you can do it the Virtual Fund Assist way, where you fill out a single <a href="../../bp/basic-info.html" data-wpel-link="internal">15-minute application</a> to
                        compare offers from our curated network of 75+ lenders.</p>
                    <h3>What will lenders use to qualify you for a business term loan?</h3>
                    <ul class="pb-2">
                        <li>Your credit score</li>
                        <li>Time in business</li>
                        <li>Collateral</li>
                        <li>P&amp;L or bank statements to show your revenue</li>
                    </ul>
                    <p>Once you’re approved, you can access your funds in as little as 24 hours.</p>
                </div>
                <!-- <div class="col-2 col-md-1 offset-md-1 py-4"> 
                        <img src="<?= $home_url ?>/images/circle-checkmark.png" alt="Check" />
                    </div> -->
                <div class="col-10 pt-3 pb-4 pr-5 pr-lg-0">
                    <h2>Enjoy Predictable Payments</h2>
                    <p>A business term loan typically comes with a fixed interest rate or flat fee. That means your
                        payments will stay constant over the lifetime of your loan term (1–5 years). These terms
                        enable you to easily calculate how much financing your business can afford to ensure you
                        keep up with your monthly payments until the loan is repaid.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="alt-grey text-center py-5">
        <div class="container py-0 py-lg-5">
            <div class="row px-2">
                <div class="col-12 py-4">
                    <img src="<?= $home_url ?>/images/quotes.svg" alt="Quotes" />
                </div>
                <div class="col-12 py-4">
                    <h3>Taylor and his team worked hard to bring me the best possible deal for my business. He kept
                        in touch about the progress of my loan and was very helpful. I would highly recommend him
                        and Virtual Fund Assist!</h3>
                    <h3>Kevin</h3>
                </div>
            </div>
        </div>
    </div> -->
</div>

<!-- ==== Page(Business-Term-Loans) Blue-Bottom Start ==== -->
<div class="bottom-cta full base dark pt-2">
    <div class="container cta-bottom py-5">
        <div class="row">
            <div class="col-12 py-md-5 text-center">
                <h2>Your business term loan is waiting.</h2>
                <form action="https://www.lendio.com/bp/basic-info" method="GET" class="cta-content d-inline-block my-4"> <input type="hidden" name="referral_url" value="https://www.google.com/" />
                    <div class="input-container col-lg-8 float-lg-left mb-3"> <span class="dollar d-none d-lg-block">$</span> <input type="text" placeholder="How much do you need?" class="amount-seeking-input text-center pl-0" name="amountSeeking" /></div>
                    <div class="button-container col-lg-4 float-lg-left"> <button class="primary-button w-lg-100 ml-0 mb-2">Start</button></div>
                    <div class="clear"></div>
                </form>
                <h5 class="px-5" style="font-size: 17px;">Applying is free and it won't impact your credit</h5>
                <h5 style="font-size: 17px;">Already have an account? &nbsp;<a class="text-uppercase font-weight-bold" href="../../bp/login.html" data-wpel-link="internal"><strong>Sign in</strong></a></h5>
            </div>
        </div>
    </div>
</div>

<?php include('../footer.php'); ?>
<?php include('../header.php'); ?>

<!-- ==== Page(Business-line-credit-calculator) Main-body Start ==== -->
<div id="business-calculators-page" class="container ">
    <div class="row all-calcs-link">
        <div class="col-12 text-left"> <a href="../../business-calculators.html" data-wpel-link="internal"><i class="fa fa-arrow-left"></i> All Calculators</a></div>
    </div>
    <div class="row">
        <div class="col-12 ">
            <h1>Line of Credit Calculator</h1>
            <p class="pb-5">Get an estimate of your potential line of credit payment based on our network of 75+
                different lenders.</p>
        </div>
    </div>
    <div id="alt-header-swap"></div>
    <script type="text/javascript">
        (function loadCalc() {
            var c = document.createElement('loan-calculator');
            c.setAttribute('type', '');
            c.setAttribute('calc-type', 'lineOfCredit');
            var currentScript = document.currentScript || (function() {
                var scripts = document.getElementsByTagName('script');
                return scripts[scripts.length - 1];
            })();

            currentScript.parentNode.appendChild(c);
            var s = document.createElement('script');
            s.src = "https://virtualfundassist.com/lendio/calculators/calc-component.js";
            s.type = "text/javascript"
            s.async = false;
            document.getElementsByTagName('head')[0].appendChild(s);
        })();
    </script>
    <div class="row py-lg-5">
        <div class="col-12 col-lg-9 ml-lg-auto pl-0 pr-lg-2 pr-0 py-lg-5">
            <div class="iframe-container py-5 alt-white""><div class=" container">
                <div class="row">
                    <div class="inline-progressive-form mx-3">
                        <div class="col-12 text-center">
                            <progressive-form adgroup="inlinePWF" id="inline-pwf" source="HomePortal" medium="Organic" />
                            <h6>
                                <p class="pb-3" style="color:#737171;">Applying is free and it won't impact your
                                    credit</p>
                            </h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="py-0 pb-lg-5">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h3 class="mt-5">See options from these leading lenders</h3>
                    </div>
                    <div class="col-12 col-md-4 text-center">
                        <div class="lender-partner-block shadow p-4">
                            <div class="logo-block d-flex align-content-center justify-content-center">
                                <img src="<?= $home_url ?>/images/headway-logo.png" alt="Headway Capital">
                            </div>
                            <p class="mb-1">Average Loan Amount</p>
                            <h4>$11,443.00</h4>
                            <p> <a href="../../bp/basic-info.html" data-wpel-link="internal">Get Started <i class="fa fa-arrow-right"></i></a></p>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 text-center">
                        <div class="lender-partner-block shadow p-4">
                            <div class="logo-block d-flex align-content-center justify-content-center">
                                <img src="<?= $home_url ?>/images/kabbage-logo.png" alt="Kabbage">
                            </div>
                            <p class="mb-1">Average Loan Amount</p>
                            <h4>$17,582.00</h4>
                            <p> <a href="../../bp/basic-info.html" data-wpel-link="internal">Get Started <i class="fa fa-arrow-right"></i></a></p>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 text-center">
                        <div class="lender-partner-block shadow p-4">
                            <div class="logo-block d-flex align-content-center justify-content-center">
                                <img src="<?= $home_url ?>/images/fundbox-logo.png" alt="FundBox">
                            </div>
                            <p class="mb-1">Average Loan Amount</p>
                            <h4>$5,208.00</h4>
                            <p> <a href="../../bp/basic-info.html" data-wpel-link="internal">Get Started <i class="fa fa-arrow-right"></i></a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wpsso-pinterest-pin-it-image" style="display:none !important;"> <img src="<?= $home_url ?>/images/loc.svg" width="0" height="0" class="skip-lazy" style="width:0;height:0;" alt="" data-pin-description="How affordable is that business line of credit? Do the math before you draw. Our calculator will help you figure out your costs." />
        </div>
        <h2>Understanding line of credit payments</h2>
        <p>A <a href="../../business-loans/line-of-credit/index.html" data-wpel-link="internal">line of credit</a>
            is one of the most accessible, flexible forms of business financing. Line of credit funds can be used
            for just about anything—from buying equipment to paying invoices. Use it when you need it; leave it when
            you don’t.</p>
        <p>The best part? You only pay interest on the capital you use. So your line of credit payment is comprised
            of the amount borrowed, interest rates, and term.</p>
        <h3>Line of credit interest rates</h3>
        <p>You only pay interest rates on the funds borrowed for a line of credit. If you have a line of credit for
            $50,000, and you use $20,000, you only pay interest on the $20,000. If you borrow $1,000, you only owe
            interest on the $1,000.</p>
        <p><a href="../../blog/small-business-insights/beginners-guide-business-loan-interest/index.html" data-wpel-link="internal">Interest rates</a> for a line of credit have a wide range, typically
            falling between 10% and 60%. Your interest rate will be determined by your credit score, what the lender
            offers, and how much capital you use. Keep in mind that you only pay interest on the funds you use, not
            the full line of credit amount. This helps you keep your costs lower.</p>
        <h3>Line of credit term</h3>
        <p>The term length for a line of credit is typically 1–2 years.</p>
        <h3>Keep an eye out for fees</h3>
        <p>Lendio offers free applications for a line of credit (and every other form of financing) with our network
            of 75+ lenders. Many lenders charge application fees. If you’re shopping around, ask about fees before
            you apply. No one wants to get all the way through a loan application only to be surprised by a fee at
            the end.</p>
        <p>You’ll also want to consider potential origination fees. Some lenders charge origination fees, an up
            front fee to cover the cost of a loan. Ask your funding manager if the lender charges origination fees
            so you can anticipate the full cost of the loan before you apply.</p>
        <h3>How you can reduce line of credit costs</h3>
        <p>Good borrower behavior goes a long way with lines of credit. Make payments on time—it’s that advice you
            hear over and over again because it’s true. If you tend to lean towards the Dory side of memory, set
            yourself up for success by setting up automatic payments or setting calendar reminders for due dates.
            That way, you can keep your costs low without having to sing “just keep paying” on repeat.</p>
        <h3>Still on the fence?</h3>
        <p>We know. Financial applications can be a drag. That’s why we carved ours down to a single <a href="../../bp/basic-info.html" data-wpel-link="internal">15-minute application</a> that gives you
            access to a curated network of 75+ lenders. That’s an average of 12 seconds for each lender. You won’t
            find a better value for your time.</p>
        <p>If you don’t find the right fit with any of our lenders, you can move on with your life. No fees. No
            obligations. Just a little bit more financial wisdom.</p>
    </div>
    <div class="sidebar col-12 col-md-3 py-3 py-md-5 pl-md-3 pr-md-0">
        <div class="banner-block-top"></div>
        <div class="shadow">
            <div class="p-4 mb-5"> <img width="850" height="460" src="<?= $home_url ?>/images/Can-I-qualify-for-a-Business-Line-of-Credit.jpg" class="mb-4 wp-post-image" alt="Can I qualify for a Business Line of Credit" srcset="https://www.lendio.com/wp-content/uploads/2016/03/Can-I-qualify-for-a-Business-Line-of-Credit.jpg 850w, https://www.lendio.com/wp-content/uploads/2016/03/Can-I-qualify-for-a-Business-Line-of-Credit-300x162.jpg 300w, https://www.lendio.com/wp-content/uploads/2016/03/Can-I-qualify-for-a-Business-Line-of-Credit-800x433.jpg 800w" sizes="(max-width: 850px) 100vw, 850px" data-wp-pid="75750" nopin="nopin" title="Line of Credit Calculator" />
                <h3>Can I Qualify For a Business Line of Credit</h3>
                <p>Mostly, every small business owner will encounter situations where they need some quick access to
                    extra capital to help their&hellip;</p> <a href="../../index5f69.html?post_type=post&amp;p=75996" data-wpel-link="internal"><strong>Read
                        article <i class="fa fa-arrow-right m-0"></i></strong></a>
            </div>
        </div>
        <div class="banner-block-top"></div>
        <div class="shadow">
            <div class="p-4 mb-5"> <img width="1254" height="752" src="<?= $home_url ?>/images/black-female-business-owner-e1534958368574.jpg" class="mb-4 wp-post-image" alt="Business owner considering a business line of credit or a business loan" srcset="https://www.lendio.com/wp-content/uploads/2018/08/black-female-business-owner-e1534958368574.jpg 1254w, https://www.lendio.com/wp-content/uploads/2018/08/black-female-business-owner-e1534958368574-300x180.jpg 300w, https://www.lendio.com/wp-content/uploads/2018/08/black-female-business-owner-e1534958368574-1024x614.jpg 1024w, https://www.lendio.com/wp-content/uploads/2018/08/black-female-business-owner-e1534958368574-800x480.jpg 800w" sizes="(max-width: 1254px) 100vw, 1254px" data-wp-pid="87793" nopin="nopin" title="Los Angeles Program Helps Black Female Business Owners" />
                <h3>Should I Get a Business Loan or Line of Credit?</h3>
                <p>So your business needs financing. Once you’ve created a funding plan that shows how much money
                    you’ll need to borrow,&hellip;</p> <a href="../../indexda97.html?post_type=post&amp;p=11120" data-wpel-link="internal"><strong>Read article <i class="fa fa-arrow-right m-0"></i></strong></a>
            </div>
        </div>
        <div class="border p-4 mb-5 text-center page-body">
            <img width="158" height="153" src="<?= $home_url ?>/images/custservicerep.png" class="mb-4 wp-post-image" alt="Lendio Customer Service" data-wp-pid="91834" nopin="nopin" title="Talk to an expert" />
            <h3 class="mb-0">Talk to an expert</h3>
            <div class="number"> <span>Call us</span><br /> <span>(469) 225-0569</div>
            <p>Monday - Friday<br>9am - 9pm CST</p>
        </div>
    </div>
</div>
</div>

<!-- ==== Page(Business-line-credit-calculator) Bottom-Blue-Bar Start ==== -->
<div class="bottom-cta pt-2 full base dark">
    <div class="container cta-bottom py-5">
        <div class="row">
            <div class="col-12 py-md-5 text-center">
                <h2>Get your small business loan today.</h2>
                <form action="https://www.lendio.com/bp/basic-info" method="GET" class="cta-content d-inline-block my-4">
                    <div class="input-container col-lg-8 float-lg-left mb-3"> <span class="dollar d-none d-lg-block">$</span> <input type="text" placeholder="How much do you need?" class="amount-seeking-input text-center pl-0" name="amountSeeking" /></div>
                    <div class="button-container col-lg-4 float-lg-left"> <button class="primary-button w-lg-100 ml-0 mb-2">Start</button></div>
                    <div class="clear"></div>
                </form>
                <h5 class="px-5" style="font-size: 17px;">Applying is free and it won't impact your credit</h5>
                <h5 style="font-size: 17px;">Already have an account? &nbsp;<a class="text-uppercase font-weight-bold" href="../../bp/login.html" data-wpel-link="internal"><strong>Sign in</strong></a></h5>
            </div>
        </div>
    </div>
</div>

<!-- ==== Page(Business-line-credit-calculator) Footer-Top Start ==== -->
<div class="contact-area alt border-top border-bottom py-5">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-7 text-center text-lg-left"> <img src="<?= $home_url ?>/images/blue-line-phone.svg" class="float-lg-left mr-3 mb-3 mb-lg-0" alt="Phone Icon">
                <div class="hidden-sm-down pt-2"></div>
                <h3 class="float-lg-left">Give us a call <br class="d-inheret d-sm-none" /><a class="phone" href="tel:8558536346" data-wpel-link="internal">(469) 225-0569</a></h3>
            </div>
            <div class="col-12 col-md-5 text-center text-lg-left">
                <div class="hidden-sm-down pt-2"></div>
                <p class="hours">Monday - Friday | 9am - 9pm Central Standard Time</p>
            </div>
        </div>
    </div>
</div>

<?php include('../footer.php'); ?>
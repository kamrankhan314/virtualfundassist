<?php include('../header.php'); ?>

<!-- ==== Page(Equipment-Loan-calculator) Yellow-Top-Bar Start ==== -->
<div class="hero position-relative" style="background: linear-gradient(90deg, #ff3366 0%, #fe9b02 100%); color: white;">
    <div class="site-close"><i class="fa fa-times"></i></div>
    <div class="container p-lg-0">
        <div class="py-lg-2 row hero-row">
            <div class="hero-responsive col-12 text-left text-md-center align-middle mt-4 my-lg-2"> The President has signed a relief bill
                that includes $284 billion for
                Paycheck Protection Program (PPP)
                loans. Virtual Fund Assist will submit
                completed, eligible applications to
                PPP lenders once the program
                officially resumes in January 2021.</div>
        </div>
    </div>
</div>

<!-- ==== Page(Equipment-Loan-calculator) Main-body Start ==== -->
<div id="business-calculators-page" class="container ">
    <div class="row all-calcs-link">
        <div class="col-12 text-left"> <a href="../../business-calculators.html" data-wpel-link="internal"><i class="fa fa-arrow-left"></i> All Calculators</a></div>
    </div>
    <div class="row">
        <div class="col-12 ">
            <h1>Equipment Loan Calculator</h1>
            <p class="pb-5">Get an estimate of how much your equipment loan could cost.</p>
        </div>
    </div>
    <div id="alt-header-swap"></div>
    <script type="text/javascript">
        (function loadCalc() {
            var c = document.createElement('loan-calculator');
            c.setAttribute('type', '');
            c.setAttribute('calc-type', 'equipment');
            var currentScript = document.currentScript || (function() {
                var scripts = document.getElementsByTagName('script');
                return scripts[scripts.length - 1];
            })();

            currentScript.parentNode.appendChild(c);
            var s = document.createElement('script');
            s.src = "https://virtualfundassist.com/lendio/calculators/calc-component.js";
            s.type = "text/javascript"
            s.async = false;
            document.getElementsByTagName('head')[0].appendChild(s);
        })();
    </script>

    <div class="row py-lg-5">
        <div class="col-12 col-lg-9 ml-lg-auto pl-0 pr-lg-2 pr-0 py-lg-5">
            <div class="iframe-container py-5 alt-white""><div class=" container">
                <div class="row">
                    <div class="inline-progressive-form mx-3">
                        <div class="col-12 text-center">
                            <progressive-form adgroup="inlinePWF" id="inline-pwf" source="HomePortal" medium="Organic" />
                            <h6>
                                <p class="pb-3" style="color:#737171;">Applying is free and it won't impact your
                                    credit</p>
                            </h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="py-0 pb-lg-5">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h3 class="mt-5">See options from these leading lenders</h3>
                    </div>
                    <div class="col-12 col-md-4 text-center">
                        <div class="lender-partner-block shadow p-4">
                            <div class="logo-block d-flex align-content-center justify-content-center">
                                <img src="<?= $home_url ?>/images/financial-pacific-leasing.png">
                            </div>
                            <p class="mb-1">Average Loan Amount</p>
                            <h4>$42,556.00</h4>
                            <p> <a href="../../bp/basic-info.html" data-wpel-link="internal">Get Started <i class="fa fa-arrow-right"></i></a></p>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 text-center">
                        <div class="lender-partner-block shadow p-4">
                            <div class="logo-block d-flex align-content-center justify-content-center">
                                <img src="<?= $home_url ?>/images/global-financial-and-leasing.jpg" alt="Global Financial Leasing">
                            </div>
                            <p class="mb-1">Average Loan Amount</p>
                            <h4>$107,796.00</h4>
                            <p> <a href="../../bp/basic-info.html" data-wpel-link="internal">Get Started <i class="fa fa-arrow-right"></i></a></p>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 text-center">
                        <div class="lender-partner-block shadow p-4">
                            <div class="logo-block d-flex align-content-center justify-content-center">
                                <img src="<?= $home_url ?>/images/balboa-logo.png" alt="Balboa Capital">
                            </div>
                            <p class="mb-1">Average Loan Amount</p>
                            <h4>$41,070.00</h4>
                            <p> <a href="../../bp/basic-info.html" data-wpel-link="internal">Get Started <i class="fa fa-arrow-right"></i></a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wpsso-pinterest-pin-it-image" style="display:none !important;"> <img src="<?= $home_url ?>/images/calculator-800x960.png" width="0" height="0" class="skip-lazy" style="width:0;height:0;" alt="" data-pin-description="Finally ready to upgrade that old equipment? Use this easy equipment loan calculator to figure out what your monthly payments will be. Then browse equipment financing options from a nationwide network of lenders for free. Tractors, pizza ovens, software, security systems - it&#039;s all covered." />
        </div>
        <h2>How Your Equipment Loan Payments Are Calculated</h2>
        <p>Like the tractor you may be seeking an equipment loan to finance, your monthly equipment loan payments
            are made up of many parts. In addition to the loan amount, you have to consider equipment loan interest
            rates and amounts, the loan term, and collateral. We used our work with more than 75 lenders to inform
            calculator estimates for your equipment loan.</p>
        <h3>Equipment Loan Amounts</h3>
        <p>Loan amounts vary widely between industries ($5,000–5,000,000). How much you can borrow depends on the
            type of equipment you’re financing, its lifetime value, and whether it’s new or used. We partner with
            plenty of lenders who specialize in industry-specific equipment loans to help you get the best deal.</p>
        <h3>Equipment Loan Interest Rates</h3>
        <p><a href="../../business-loans/equipment-financing/index.html" data-wpel-link="internal">Equipment
                loan</a> interest rates typically range between 2% and 20%. While the variance is as wide as that
            tractor you’re financing, it’s only because the rate you get largely depends on your credit score and
            how long you’ve been in business. The higher your credit score, the lower your <a href="../../blog/small-business-tools/business-loan-interest-rates/index.html" data-wpel-link="internal">interest rate</a> will likely be.</p>
        <h3>Equipment Loan Terms</h3>
        <p>Terms are typically 12–72 months and will vary by loan option and lender.</p>
        <h3>Let’s Get Specific (About Your Rates and Terms)</h3>
        <p>Instead of spending a day Googling equipment loan interest rates that may or may not apply to you, take
            15 minutes to fill out an application and access our network of 75+ lenders. That breaks down to an
            average of 12 seconds per lender. You won’t find a better investment… for your time.</p>
        <hr style="margin: 70px 0;" />
        <h2>Potential Fees</h2>
        <p>Applying for an equipment loan <a href="index.html" data-wpel-link="internal">through Lendio</a> is
            always free, but many lenders will charge you application fees. If you’re shopping around, be sure to
            ask about fees before submitting your application so you don’t encounter any surprises.</p>
        <p>While you’re at it, ask about potential origination fees—up-front costs a lender may charge when a loan
            is funded—so you can calculate the total cost of your equipment loan.</p>
        <hr style="margin: 70px 0;" />
        <h2>Steps You Can Take to Reduce Equipment Loan Costs</h2>
        <p>Making your monthly minimum payments on time is the no-brainer way to save money while also <a href="../../business-credit/index.html" data-wpel-link="internal">boosting your credit score</a>.
            Calendaring your due dates or setting up automatic payments can help you stay on schedule. And if you’re
            worried about missing a payment, contact your lender immediately to check out your options.</p>
        <p>Some lenders have prepayment penalties, while others will praise you for paying off your loan early and
            offer you a discount. Always ask about potential penalties and discounts so you can choose a repayment
            schedule that fits your budget and timeline.</p>
        <p>Again, there are a lot of maybes here because every lender is different. Put these items on your
            checklist, and your funding manager can answer any questions about your repayment strategy.</p>
    </div>
    <div class="sidebar col-12 col-md-3 py-3 py-md-5 pl-md-3 pr-md-0">
        <div class="banner-block-top"></div>
        <div class="shadow">
            <div class="p-4 mb-5"> <img width="1253" height="836" src="../../wp-content/uploads/2019/02/man-brewery.jpg" class="mb-4 wp-post-image" alt="8 Ways Equipment Financing Can Grow Your Business" srcset="https://www.lendio.com/wp-content/uploads/2019/02/man-brewery.jpg 1253w, https://www.lendio.com/wp-content/uploads/2019/02/man-brewery-300x200.jpg 300w, https://www.lendio.com/wp-content/uploads/2019/02/man-brewery-1024x683.jpg 1024w, https://www.lendio.com/wp-content/uploads/2019/02/man-brewery-800x534.jpg 800w" sizes="(max-width: 1253px) 100vw, 1253px" data-wp-pid="89395" nopin="nopin" title="8 Ways Equipment Financing Can Grow Your Business" />
                <h3>8 Ways Equipment Financing Can Grow Your Business</h3>
                <p>When growing your business is a priority, having the right equipment may be instrumental to your
                    plans. If purchasing new&hellip;</p> <a href="../../indexa9fc.html?post_type=post&amp;p=89392" data-wpel-link="internal"><strong>Read article <i class="fa fa-arrow-right m-0"></i></strong></a>
            </div>
        </div>
        <div class="banner-block-top"></div>
        <div class="shadow">
            <div class="p-4 mb-5">
                <img width="1255" height="668" src="<?= $home_url ?>/images/forklift-driver.jpg" class="mb-4 wp-post-image" alt="Forklift driver in a warehouse" srcset="https://www.lendio.com/wp-content/uploads/2018/09/forklift-driver-e1536599768112.jpg 1255w, https://www.lendio.com/wp-content/uploads/2018/09/forklift-driver-e1536599768112-300x160.jpg 300w, https://www.lendio.com/wp-content/uploads/2018/09/forklift-driver-e1536599768112-1024x545.jpg 1024w, https://www.lendio.com/wp-content/uploads/2018/09/forklift-driver-e1536599768112-800x426.jpg 800w" sizes="(max-width: 1255px) 100vw, 1255px" data-wp-pid="87951" nopin="nopin" title="Everything You Need to Know About Equipment Financing" />
                <h3>Everything You Need to Know About Equipment Financing</h3>
                <p>Have you heard of equipment financing? It’s a charter member of the “super explanatory name”
                    club. Other members of the&hellip;</p> <a href="../../index4de7.html?post_type=post&amp;p=87948" data-wpel-link="internal"><strong>Read article <i class="fa fa-arrow-right m-0"></i></strong></a>
            </div>
        </div>
        <div class="border p-4 mb-5 text-center page-body">
            <img width="158" height="153" src="<?= $home_url ?>/images/custservicerep.png" class="mb-4 wp-post-image" alt="Lendio Customer Service" data-wp-pid="91834" nopin="nopin" title="Talk to an expert" />
            <h3 class="mb-0">Talk to an expert</h3>
            <div class="number"> <span>Call us</span><br /> <span>(469) 225-0569 </div>
            <p>Monday - Friday<br>9am - 9pm CST</p>
        </div>
    </div>
</div>
</div>

<!-- ==== Page(Equipment-Loan-calculator) Bottom-Blue-Bar Start ==== -->
<div class="bottom-cta pt-2 full base dark">
    <div class="container cta-bottom py-5">
        <div class="row">
            <div class="col-12 py-md-5 text-center">
                <h2>Get your small business loan today.</h2>
                <form action="https://www.lendio.com/bp/basic-info" method="GET" class="cta-content d-inline-block my-4">
                    <div class="input-container col-lg-8 float-lg-left mb-3"> <span class="dollar d-none d-lg-block">$</span> <input type="text" placeholder="How much do you need?" class="amount-seeking-input text-center pl-0" name="amountSeeking" /></div>
                    <div class="button-container col-lg-4 float-lg-left"> <button class="primary-button w-lg-100 ml-0 mb-2">Start</button></div>
                    <div class="clear"></div>
                </form>
                <h5 class="px-5" style="font-size: 17px;">Applying is free and it won't impact your credit</h5>
                <h5 style="font-size: 17px;">Already have an account? &nbsp;<a class="text-uppercase font-weight-bold" href="../../bp/login.html" data-wpel-link="internal"><strong>Sign in</strong></a></h5>
            </div>
        </div>
    </div>
</div>

<!-- ==== Page(Equipment-Loan-calculator) Footer-Top Start ==== -->
<div class="contact-area alt border-top border-bottom py-5">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-7 text-center text-lg-left">
                <img src="<?= $home_url ?>/images/blue-line-phone.svg" class="float-lg-left mr-3 mb-3 mb-lg-0" alt="Phone Icon">
                <div class="hidden-sm-down pt-2"></div>
                <h3 class="float-lg-left">Give us a call <br class="d-inheret d-sm-none" /><a class="phone" href="tel:8558536346" data-wpel-link="internal">(469) 225-0569</a></h3>
            </div>
            <div class="col-12 col-md-5 text-center text-lg-left">
                <div class="hidden-sm-down pt-2"></div>
                <p class="hours">Monday - Friday | 9am - 9pm Central Standard Time</p>
            </div>
        </div>
    </div>
</div>

<?php include('../footer.php'); ?>
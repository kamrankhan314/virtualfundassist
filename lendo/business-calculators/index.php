<?php include('../header.php'); ?>

<!-- ==== Page(Loan-calculators) Yellow-Bar Start ==== -->
<div class="hero position-relative" style="background: linear-gradient(90deg, #ff3366 0%, #fe9b02 100%); color: white;">
  <div class="site-close"><i class="fa fa-times"></i></div>
  <div class="container p-lg-0">
    <div class="py-lg-2 row hero-row">
      <div class="hero-responsive col-12 text-left text-md-center align-middle mt-4 my-lg-2">
        The President has signed a relief bill
        that includes $284 billion for
        Paycheck Protection Program (PPP)
        loans. Virtual Fund Assist will submit
        completed, eligible applications to
        PPP lenders once the program
        officially resumes in January 2021.
      </div>
    </div>
  </div>
</div>

<!-- ==== Page(Loan-calculators) Main Start ==== -->
<div id="business-calculators-page">
  <div class="hero pt-md-2">
    <div class="container">
      <div class="row hero-row">
        <div class="hero-responsive col-lg-6 text-left align-middle mt-5">
          <h1>Small Business Loan Calculators</h1>
          <h5 class="sub-header my-3">
            <p>We do the math so you don&#8217;t have to. Get an estimate of <br />what your loan could cost with
              these handy calculators.</p>
          </h5>
        </div>
        <div class="col-lg-6 text-left align-middle mt-5">
          <img class="lightbox hero-img" src="<?= $home_url ?>/images/business-calculator-hero.jpg" alt="Business loan calculators" />
        </div>
      </div>
    </div>
  </div>
  <div id="alt-header-swap"></div>
  <div class="container pb-5">
    <div class="row" id="loan-factors-row">
      <div class="col-12 text-center pt-5">
        <div class="container py-5" id="loan-factors-container">
          <div class="row">
            <!-- <div class="col-12 col-md-6 col-lg-5 text-left pt-3 pb-5">
              <div class="container">
                <div class="row">
                  <div class="col-2 icon-column">
                    <img src="<?= $home_url ?>/images/ach_loan.svg" class="pt-3 wp-post-image" alt="ACH Loan Calculator" height="61.9" style="min-width:30px;display:block;" width="51.3" data-wp-pid="92170" nopin="nopin" title="ACH Loan Calculator" />
                  </div>
                  <div class="col-10">
                    <h4 class="py-md-2 loan-types"><a href="business-calculators/ach-loan-calculator/index.html" data-wpel-link="internal">ACH Loan <span class="text-nowrap">Calculator <i class="fa fa-arrow-right"></i></span></a></h4>
                    <p>ACH math can be a little confusing but we've got your back. Just select your loan amount and
                      payment timeline - we'll do the rest of the work for you.</p>
                  </div>
                </div>
              </div>
            </div> --> 
            <div class="col-12 col-md-6 col-lg-5 text-left pt-3 pb-5">
              <div class="container">
                <div class="row">
                  <div class="col-2 icon-column">
                    <img src="<?= $home_url ?>/images/term-blue.svg" class="pt-3 wp-post-image" alt="Term Loan" height="51" style="min-width:30px;display:block;" width="51.09" data-wp-pid="89773" nopin="nopin" title="Explore America&#039;s Leading Business Loan Marketplace" />
                  </div>
                  <div class="col-10">
                    <h4 class="py-md-2 loan-types"><a href="business-calculators/business-term-loan-calculator/index.html" data-wpel-link="internal">Business Term Loan <span class="text-nowrap">Calculator <i class="fa fa-arrow-right"></i></span></a></h4>
                    <p>The business term loan is a classic choice, but can you keep up with the monthly payments? This
                      term loan calculator will help you know what to expect.</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-md-6 col-lg-5 offset-lg-1 text-left pt-3 pb-5">
              <div class="container">
                <div class="row">
                  <div class="col-2 icon-column">
                    <img src="<?= $home_url ?>/images/credit-card-blue.svg" class="pt-3 wp-post-image" alt="Credit Card" height="38.8" style="min-width:30px;display:block;" width="53.19" data-wp-pid="91678" nopin="nopin" title="The 7 Best Loans for Restaurants" />
                  </div>
                  <div class="col-10">
                    <h4 class="py-md-2 loan-types"><a href="business-calculators/business-credit-card-calculator/index.html" data-wpel-link="internal">Business Credit Card <span class="text-nowrap">Calculator <i class="fa fa-arrow-right"></i></span></a></h4>
                    <p>Can you keep up with your business credit card payments? This calculator will tell you what to
                      expect each month.</p>
                  </div>
                </div>
              </div>
            </div>
            <!-- <div class="col-12 col-md-6 col-lg-5 offset-lg-1 text-left pt-3 pb-5">
              <div class="container">
                <div class="row">
                  <div class="col-2 icon-column">
                    <img src="<?= $home_url ?>/images/equipment-1.svg" class="pt-3 wp-post-image" alt="Equipment Loan Calculator" height="54.92" style="min-width:30px;display:block;" width="54.92" data-wp-pid="90168" nopin="nopin" title="Equipment Loan Calculator" />
                  </div>
                  <div class="col-10">
                    <h4 class="py-md-2 loan-types"><a href="business-calculators/equipment-loan-calculator/index.html" data-wpel-link="internal">Equipment Loan <span class="text-nowrap">Calculator <i class="fa fa-arrow-right"></i></span></a></h4>
                    <p>Can you afford that sweet new tractor or pizza oven? Use this calculator to figure out what
                      your monthly payments will be.</p>
                  </div>
                </div>
              </div>
            </div> -->
            <div class="col-12 col-md-6 col-lg-5 text-left pt-3 pb-5">
              <div class="container">
                <div class="row">
                  <div class="col-2 icon-column"> <img src="<?= $home_url ?>/images/sba.svg" class="pt-3 wp-post-image" alt="SBA Loan Calculator" height="52" style="min-width:30px;display:block;" width="51.65" data-wp-pid="90169" nopin="nopin" title="SBA Paycheck Protection Program (PPP) Loan Calculator" /></div>
                  <div class="col-10">
                    <h4 class="py-md-2 loan-types"><a href="business-calculators/sba-loan-calculator/index.html" data-wpel-link="internal">SBA Paycheck Protection Program (PPP) Loan <span class="text-nowrap">Calculator <i class="fa fa-arrow-right"></i></span></a></h4>
                    <p>The SBA will back your loan but they won’t do the math for you. We will, though. Estimate your
                      monthly payments here.</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-md-6 col-lg-5 offset-lg-1 text-left pt-3 pb-5">
              <div class="container">
                <div class="row">
                  <div class="col-2 icon-column">
                    <img src="<?= $home_url ?>/images/short-term.svg" class="pt-3 wp-post-image" alt="Short Term Loan Calculator" height="51.72" style="min-width:30px;display:block;" width="51.72" data-wp-pid="90170" nopin="nopin" title="Short Term Loan Calculator" />
                  </div>
                  <div class="col-10">
                    <h4 class="py-md-2 loan-types"><a href="business-calculators/short-term-loan-calculator/index.html" data-wpel-link="internal">Short Term Loan <span class="text-nowrap">Calculator <i class="fa fa-arrow-right"></i></span></a></h4>
                    <p>How much will your short term loan payments be? Determine your dollar situation by letting our
                      calculator crunch the numbers for you.</p>
                  </div>
                </div>
              </div>
            </div>
            
            <!-- <div class="col-12 col-md-6 col-lg-5 offset-lg-1 text-left pt-3 pb-5">
              <div class="container">
                <div class="row">
                  <div class="col-2 icon-column">
                    <img src="<?= $home_url ?>/images/mortgage-blue.svg" class="pt-3 wp-post-image" alt="Commercial Mortgage Loan Calculator" height="51.12" style="min-width:30px;display:block;" width="52.05" data-wp-pid="90172" nopin="nopin" title="Commercial Mortgage Calculator" />
                  </div>
                  <div class="col-10">
                    <h4 class="py-md-2 loan-types"><a href="business-calculators/commercial-mortgage-calculator/index.html" data-wpel-link="internal">Commercial Mortgage <span class="text-nowrap">Calculator <i class="fa fa-arrow-right"></i></span></a></h4>
                    <p>First comes the equation, then comes that second location. Use this handy calculator to
                      estimate your monthly commercial mortgage payments.</p>
                  </div>
                </div>
              </div>
            </div> -->
            <!-- <div class="col-12 col-md-6 col-lg-5 text-left pt-3 pb-5">
              <div class="container">
                <div class="row">
                  <div class="col-2 icon-column"> <img src="<?= $home_url ?>/images/loc-blue.svg" class="pt-3 wp-post-image" alt="Line of Credit" height="50.28" style="min-width:30px;display:block;" width="56.35" data-wp-pid="89771" nopin="nopin" title="Explore America&#039;s Leading Business Loan Marketplace" /></div>
                  <div class="col-10">
                    <h4 class="py-md-2 loan-types"><a href="business-calculators/business-line-credit-calculator/index.html" data-wpel-link="internal">Line of Credit <span class="text-nowrap">Calculator <i class="fa fa-arrow-right"></i></span></a></h4>
                    <p>How affordable is that business line of credit? Do the math before you draw. Our calculator
                      will help you figure out your costs.</p>
                  </div>
                </div>
              </div>
            </div> -->
          </div>
          <div class="row row d-flex justify-content-center">
          <div class="col-12 col-md-6 col-lg-5 pt-3 pb-5">
              <div class="container">
                <div class="row">
                  <div class="col-12 icon-column text-center">
                    <img src="<?= $home_url ?>/images/startup-blue.svg" class="pt-3 wp-post-image" alt="Startup Loan Calculator" height="53.78" style="min-width:30px;" width="53.38" data-wp-pid="90171" nopin="nopin" title="Startup Business Loan Calculator" />
                  </div>
                  <div class="col-12 text-center">
                    <h4 class="py-md-2 loan-types"><a href="business-calculators/startup-business-loan-calculator/index.html" data-wpel-link="internal">Startup Business Loan <span class="text-nowrap">Calculator <i class="fa fa-arrow-right"></i></span></a></h4>
                    <p>Your business may be new but it’s going places. See how much financing you need to really take
                      off.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="bottom-cta full base dark">
    <div class="container cta-bottom py-5">
      <div class="row">
        <div class="col-12 py-md-5 text-center">
          <h2>Get your small business loan today.</h2>
          <form action="https://www.lendio.com/bp/basic-info" method="GET" class="cta-content d-inline-block my-4">
            <div class="input-container col-lg-8 float-lg-left mb-3"> <span class="dollar d-none d-lg-block">$</span>
              <input type="text" placeholder="How much do you need?" class="amount-seeking-input text-center pl-0" name="amountSeeking" />
            </div>
            <div class="button-container col-lg-4 float-lg-left"> <button class="primary-button w-lg-100 ml-0 mb-2">Start</button></div>
            <div class="clear"></div>
          </form>
          <h5 class="px-5" style="font-size: 17px;">Applying is free and it won't impact your credit</h5>
          <h5 style="font-size: 17px;">Already have an account? &nbsp;<a class="text-uppercase font-weight-bold" href="bp/login.html" data-wpel-link="internal"><strong>Sign in</strong></a></h5>
        </div>
      </div>
    </div>
  </div>
  <div class="contact-area alt border-top border-bottom py-5">
    <div class="container">
      <div class="row">
        <div class="col-12 col-md-7 text-center text-lg-left"> <img src="<?= $home_url ?>/images/blue-line-phone.svg" class="float-lg-left mr-3 mb-3 mb-lg-0" alt="Phone Icon">
          <div class="hidden-sm-down pt-2"></div>
          <h3 class="float-lg-left">Give us a call <br class="d-inheret d-sm-none" /><a class="phone" href="tel:8558536346" data-wpel-link="internal">(469) 225-0569</a></h3>
        </div>
        <div class="col-12 col-md-5 text-center text-lg-left">
          <div class="hidden-sm-down pt-2"></div>
          <p class="hours">Monday - Friday | 9am - 9pm Central Standard Time</p>
        </div>
      </div>
    </div>
  </div>
</div>

<?php include('../footer.php'); ?>
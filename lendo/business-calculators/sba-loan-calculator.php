<?php include('../header.php'); ?>

<!-- ==== Page(SBA-Loan-calculator) Main-body Start ==== -->
<div id="business-calculators-page" class="container ">
    <div class="row all-calcs-link">
        <div class="col-12 text-left"> <a href="/business-calculators" data-wpel-link="internal"><i class="fa fa-arrow-left"></i> All Calculators</a></div>
    </div>
    <div class="row">
        <div class="col-12 ">
            <h1>SBA Paycheck Protection Program (PPP) Loan Calculator</h1>
            <p class="pb-5">Calculate an estimate of how much you can receive with a PPP loan.</p>
        </div>
    </div>
    <div id="alt-header-swap"></div>
    <script type="text/javascript">
        (function loadCalc() {
            var c = document.createElement('loan-calculator');
            c.setAttribute('type', '');
            c.setAttribute('calc-type', 'sba');
            var currentScript = document.currentScript || (function() {
                var scripts = document.getElementsByTagName('script');
                return scripts[scripts.length - 1];
            })();

            currentScript.parentNode.appendChild(c);
            var s = document.createElement('script');
            s.src = "https://www.lendio.com/calculators/vue/calc-component.js";
            s.type = "text/javascript"
            s.async = false;
            document.getElementsByTagName('head')[0].appendChild(s);
        })();
    </script>
    <div class="row py-lg-5">
        <div class="col-12 col-lg-9 ml-lg-auto pl-0 pr-lg-2 pr-0 py-lg-5">
            <div class="py-0 pb-lg-5">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <h3 class="mt-5">See options from these leading lenders</h3>
                        </div>
                        <div class="col-12 col-md-4 text-center">
                            <div class="lender-partner-block shadow p-4">
                                <div class="logo-block d-flex align-content-center justify-content-center"> <img src="https://www.lendio.com/wp-content/uploads/2019/05/smart-biz-logo.svg" alt="SmartBiz"></div>
                                <p class="mb-1">Average Loan Amount</p>
                                <h4>$238,000.00</h4>
                                <p><a href="/bp/basic-info" data-wpel-link="internal">Get Started <i class="fa fa-arrow-right"></i></a></p>
                            </div>
                        </div>
                        <div class="col-12 col-md-4 text-center offset-md-4 ">
                            <div class="lender-partner-block shadow p-4">
                                <div class="logo-block d-flex align-content-center justify-content-center"> <img src="https://www.lendio.com/wp-content/uploads/2017/08/celtic-bank.png" alt=""></div>
                                <p class="mb-1">Average Loan Amount</p>
                                <h4>$150,000.00</h4>
                                <p><a href="/bp/basic-info" data-wpel-link="internal">Get Started <i class="fa fa-arrow-right"></i></a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wpsso-pinterest-pin-it-image" style="display:none !important;"> <img src="https://www.lendio.com/wp-content/uploads/2017/02/calculator-800x960.png" width="0" height="0" class="skip-lazy" style="width:0;height:0;" alt="" data-pin-description="Ready to get an SBA loan for your small business? Use this easy SBA loan payment calculator to figure out what your monthly payment will be. Then access our free marketplace to compare SBA loan options from 75+ leading lenders, including 504, 7(a), and SBA Express." />
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-12 pt-3 pb-4 pr-5 pr-lg-0">
                        <h2>SBA Paycheck Protection Program (PPP) Loans and How to Calculate Them</h2>
                        <p>The <a href="https://www.lendio.com/paycheck-protection-program-loans/" data-wpel-link="internal">Paycheck Protection Program</a> (PPP), established under
                            The Coronavirus Aid, Relief, and Economic Security Act (CARES Act), provides a financial
                            lifeline for small businesses in the wake of the COVID-19 pandemic. PPP loans will give
                            a vast swath of small businesses the financial safety net they need to survive the
                            economic downturn caused by the spread of the novel coronavirus, and it will be
                            available to a broad number of American small businesses.</p>
                        <p>But let’s be real—the calculations for PPP can feel complicated, so we made it as easy as
                            we could to calculate your potential PPP loan amount because we know the last thing
                            small business owners need to do right now is pour over legislation trying to make sense
                            of it.</p>
                    </div>
                    <div class="col-12 pt-3 pb-4 pr-5 pr-lg-0">
                        <h2>SBA Paycheck Protection Program (PPP) Loan Amounts</h2>
                        <p>Your total PPP loan amount will be based on 2.5 times your monthly payroll costs, up to
                            $10 million. Unlike other SBA calculators, PPP loans are calculated specifically based
                            on a small business or nonprofit’s average payroll costs, as opposed to time in
                            business, revenue, and credit history.</p>
                    </div>
                    <div class="col-12 pt-3 pb-4 pr-5 pr-lg-0">
                        <h2>SBA Loan Calculations: Understanding the Math in PPP Loans</h2>
                        <p>What will the SBA use to calculate? Because the small business landscape has changed so
                            drastically since the beginning of February, that depends on how long your business has
                            been open, whether it’s seasonal, and if you’ve taken out an EIDL.</p>
                        <h3>SBA Loan Calculations for Businesses in Operation from February 15, 2019, to June 30,
                            2019</h3>
                        <p>For these businesses and nonprofits, the maximum loan size will be 2.5 times the average
                            monthly payroll costs during the 12-month period prior to the loan being made.</p>
                        <h3>SBA Loan Calculations for Businesses <em>Not</em> in Operation from February 15, 2019,
                            to June 30, 2019</h3>
                        <p>In this case, the maximum loan amount is set at 2.5 times the average monthly payroll
                            costs between January 1, 2020, and February 29, 2020.</p>
                        <h3>SBA Loan Calculations for Seasonal Businesses</h3>
                        <p>For seasonal businesses, the SBA will use the average monthly payroll costs between
                            February 1, 2019, and June 30, 2019, or March 1, 2019, and June 30, 2019.</p>
                    </div>
                    <div class="col-12 pt-3 pb-4 pr-5 pr-lg-0">
                        <h2>What’s Included in Calculating SBA Paycheck Protection Program Loans</h2>
                        <p>Once you’ve qualified for an SBA loan under the Paycheck Protection Program (PPP), the
                            amount of <a href="https://www.lendio.com/paycheck-protection-program-loans/" data-wpel-link="internal">your PPP loan</a> will be calculated using payroll costs
                            including:</p>
                        <ul>
                            <li>Compensation: Salary, wages, commission/similar compensation, cash tips/equivalent
                                are all included here</li>
                            <li>Paid time off: Vacation pay and parental, family, medical, or sick leave</li>
                            <li>Allowance for dismissal or separation</li>
                            <li>Healthcare costs: payments required for group healthcare benefits, including
                                insurance premiums</li>
                            <li>Retirement benefit payments</li>
                            <li>State and local taxes associated with the compensation of employees</li>
                        </ul>
                        <h3>What’s Not Included in SBA PPP Loan Calculations</h3>
                        <p>The following are excluded from PPP calculations:</p>
                        <ul>
                            <li>Annual employee or owner compensation over $100,000 ($8,333/month)</li>
                            <li>Taxes imposed or withheld under Chapters 21, 22, and 24 of the IRS Code</li>
                            <li>Employee compensation for individuals whose principal place of residence is outside
                                the US</li>
                            <li>Qualified sick and family leave for which you can receive a credit under the <a href="https://www.dol.gov/agencies/whd/pandemic/ffcra-employer-paid-leave" data-wpel-link="external" target="_blank" rel="external noopener noreferrer">Families First Coronavirus Response Act</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 pt-3 pb-4 pr-5 pr-lg-0">
                        <h2>SBA Paycheck Protection Program (PPP) Loan Interest Rates</h2>
                        <p>SBA PPP loans have a 1.0% fixed rate APR for the life of the loan.</p>
                    </div>
                    <div class="col-12 pt-3 pb-4 pr-5 pr-lg-0">
                        <h2>SBA Paycheck Protection Program (PPP) Loan Terms</h2>
                        <p>The maximum term for SBA PPP loans is 5 years.</p>
                    </div>
                    <div class="col-12 pt-3 pb-4 pr-5 pr-lg-0">
                        <h2>SBA Paycheck Protection Program (PPP) Loan Forgiveness</h2>
                        <p>Funds used to cover certain payroll-related costs in the first 8 weeks of the loan,
                            starting at the origination date, are eligible to be forgiven.</p>
                    </div>
                    <div class="col-12 pt-3 pb-4 pr-5 pr-lg-0">
                        <h2>Paycheck Protection Program Payments</h2>
                        <p>(Say that heading fast 10 times.) One of the highlights of PPP loans for cashed strapped
                            businesses is that payments are deferred for the first 6 months. For qualifying
                            businesses, payments can be deferred up to 12 months.</p>
                    </div>
                    <div class="col-12 pt-3 pb-4 pr-5 pr-lg-0">
                        <h2>Paycheck Protection Program (PPP) Loans and Other SBA Loans</h2>
                        <p>You can <a href="https://www.lendio.com/paycheck-protection-program-loans/" data-wpel-link="internal">qualify for a PPP loan</a> in addition to other SBA loans
                            you may have already applied or qualified for, like an SBA Economic Injury and Disaster
                            Loan (EIDL) or an SBA 7(a) loan, but the funds cannot be for the same intended use as
                            another SBA loan. So if you’ve already filed for an EIDL loan to cover your rent
                            payments, you cannot apply for a PPP loan to cover rent payments.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="sidebar col-12 col-md-3 py-3 py-md-5 pl-md-3 pr-md-0">
            <div class="banner-block-top"></div>
            <div class="shadow">
                <div class="p-4 mb-5"> <img width="1254" height="836" src="https://www.lendio.com/wp-content/uploads/2020/03/closed-coronavirus.jpg" class="mb-4 wp-post-image" alt="Sign: Closed due to Coronavirus" srcset="https://www.lendio.com/wp-content/uploads/2020/03/closed-coronavirus.jpg 1254w, https://www.lendio.com/wp-content/uploads/2020/03/closed-coronavirus-300x200.jpg 300w, https://www.lendio.com/wp-content/uploads/2020/03/closed-coronavirus-1024x683.jpg 1024w, https://www.lendio.com/wp-content/uploads/2020/03/closed-coronavirus-800x533.jpg 800w" sizes="(max-width: 1254px) 100vw, 1254px" data-wp-pid="92156" nopin="nopin" title="Loans and Grants for Small Businesses Impacted by Coronavirus" />
                    <h3>Loans and Grants for Small Businesses Impacted by Coronavirus</h3>
                    <p>The US Small Business Administration (SBA) is one of the few constants in the volatile world
                        of entrepreneurship. Tasked with&hellip;</p> <a href="https://www.lendio.com/?post_type=post&amp;p=92155" data-wpel-link="internal"><strong>Read article <i class="fa fa-arrow-right m-0"></i></strong></a>
                </div>
            </div>
            <div class="banner-block-top"></div>
            <div class="shadow">
                <div class="p-4 mb-5"> <img width="1254" height="836" src="https://www.lendio.com/wp-content/uploads/2020/03/coronavirus-piggy-bank.jpg" class="mb-4 wp-post-image" alt="Piggy bank with Coronavirus face mask" srcset="https://www.lendio.com/wp-content/uploads/2020/03/coronavirus-piggy-bank.jpg 1254w, https://www.lendio.com/wp-content/uploads/2020/03/coronavirus-piggy-bank-300x200.jpg 300w, https://www.lendio.com/wp-content/uploads/2020/03/coronavirus-piggy-bank-1024x683.jpg 1024w, https://www.lendio.com/wp-content/uploads/2020/03/coronavirus-piggy-bank-800x533.jpg 800w" sizes="(max-width: 1254px) 100vw, 1254px" data-wp-pid="92174" nopin="nopin" title="Everything You Need to Know About SBA COVID-19 (Coronavirus) Loans" />
                    <h3>Everything You Need to Know About SBA COVID-19 (Coronavirus) Loans</h3>
                    <p>Due to the fast-changing nature of COVID-19 programs and SBA COVID-19 programs, posts may
                        become quickly out of date. Please&hellip;</p> <a href="https://www.lendio.com/?post_type=post&amp;p=92173" data-wpel-link="internal"><strong>Read article <i class="fa fa-arrow-right m-0"></i></strong></a>
                </div>
            </div>
            <div class="border p-4 mb-5 text-center page-body"> <img width="158" height="153" src="https://www.lendio.com/wp-content/uploads/2020/02/custServiceRep.png" class="mb-4 wp-post-image" alt="Lendio Customer Service" data-wp-pid="91834" nopin="nopin" title="Talk to an expert" />
                <h3 class="mb-0">Talk to an expert</h3>
                <div class="number"> <span>Call us</span><br /> <span>(469) 225-0569 </div>
                <p>Monday - Friday<br>9am - 9pm CST</p>
            </div>
        </div>
    </div>
</div>

<!-- ==== Page(SBA-Loan-calculator) Bottom-Blue-Bar Start ==== -->
<div class="bottom-cta pt-2 full base dark">
    <div class="container cta-bottom py-5">
        <div class="row">
            <div class="col-12 py-md-5 text-center">
                <h2>Get your small business loan today.</h2>
                <form action="/bp/basic-info" method="GET" class="cta-content d-inline-block my-4"> <input type="hidden" name="referral_url" value="https://www.google.com/" />
                    <div class="input-container col-lg-8 float-lg-left mb-3"> <span class="dollar d-none d-lg-block">$</span> <input type="text" placeholder="How much do you need?" class="amount-seeking-input text-center pl-0" name="amountSeeking" /></div>
                    <div class="button-container col-lg-4 float-lg-left"> <button class="primary-button w-lg-100 ml-0 mb-2">Start</button></div>
                    <div class="clear"></div>
                </form>
                <h5 class="px-5" style="font-size: 17px;">Applying is free and it won't impact your credit</h5>
                <h5 style="font-size: 17px;">Already have an account? &nbsp;<a class="text-uppercase font-weight-bold" href="/bp/login" data-wpel-link="internal"><strong>Sign in</strong></a></h5>
            </div>
        </div>
    </div>
</div>

<!-- ==== Page(SBA-Loan-calculator) Footer-Top Start ==== -->
<div class="contact-area alt border-top border-bottom py-5">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-7 text-center text-lg-left"> <img src="<?= $home_url ?>/images/blue-line-phone.svg" class="float-lg-left mr-3 mb-3 mb-lg-0" alt="Phone Icon">
                <div class="hidden-sm-down pt-2"></div>
                <h3 class="float-lg-left">Give us a call <br class="d-inheret d-sm-none" /><a class="phone" href="tel:8558536346" data-wpel-link="internal">(469) 225-0569</a></h3>
            </div>
            <div class="col-12 col-md-5 text-center text-lg-left">
                <div class="hidden-sm-down pt-2"></div>
                <p class="hours">Monday - Friday | 9am - 9pm Central Standard Time</p>
            </div>
        </div>
    </div>
</div>

<?php include('../footer.php'); ?>
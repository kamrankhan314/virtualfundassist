<?php include('../header.php'); ?>

    <!-- ==== Page(Business-Credit-Card-Calculator) Main-body Start ==== -->
    <div id="business-calculators-page" class="container ">
        <div class="row all-calcs-link">
            <div class="col-12 text-left"> <a href="../../business-calculators.html" data-wpel-link="internal"><i
                        class="fa fa-arrow-left"></i> All Calculators</a></div>
        </div>
        <div class="row">
            <div class="col-12 ">
                <h1>Business Credit Card Calculator</h1>
                <p class="pb-5">Business credit cards offer flexible, accessible capital for monthly expenditures for
                    businesses that may not qualify for a small business loan. Get an estimate of how much your business
                    credit card payment could cost.</p>
            </div>
        </div>
        <div id="alt-header-swap"></div>
        <script type="text/javascript">(function loadCalc() {
                var c = document.createElement('loan-calculator');
                c.setAttribute('type', '');
                c.setAttribute('calc-type', 'businessCC');
                var currentScript = document.currentScript || (function () {
                    var scripts = document.getElementsByTagName('script');
                    return scripts[scripts.length - 1];
                })();

                currentScript.parentNode.appendChild(c);
                var s = document.createElement('script');
                s.src = "https://virtualfundassist.com/lendio/calculators/calc-component.js";
                s.type = "text/javascript"
                s.async = false;
                document.getElementsByTagName('head')[0].appendChild(s);
            })();</script>
        <div class="row py-lg-5">
            <div class="col-12 col-lg-9 ml-lg-auto pl-0 pr-lg-2 pr-0 py-lg-5">
                <div class="iframe-container py-5 alt-white""><div class=" container">
                    <div class="row">
                        <div class="inline-progressive-form mx-3">
                            <div class="col-12 text-center">
                                <progressive-form adgroup="inlinePWF" id="inline-pwf" source="HomePortal"
                                    medium="Organic" />
                                <h6>
                                    <p class="pb-3" style="color:#737171;">Applying is free and it won't impact your
                                        credit</p>
                                </h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="py-0 pb-lg-5">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <h3 class="mt-5">See options from these leading lenders</h3>
                        </div>
                        <div class="col-12 col-md-4 text-center">
                            <div class="lender-partner-block shadow p-4">
                                <div class="logo-block d-flex align-content-center justify-content-center"> <img
                                        src="<?= $home_url ?>/images/lencred-logo.png" alt="LenCred"></div>
                                <p class="mb-1">Average Loan Amount</p>
                                <h4>$27,579.00</h4>
                                <p> <a href="../../bp/basic-info.html" data-wpel-link="internal">Get Started <i
                                            class="fa fa-arrow-right"></i></a></p>
                            </div>
                        </div>
                        <div class="col-12 col-md-4 text-center offset-md-4 ">
                            <div class="lender-partner-block shadow p-4">
                                <div class="logo-block d-flex align-content-center justify-content-center">
                                    <img src="<?= $home_url ?>/images/seek-logo.png" alt="Seek Business Capital">
                                </div>
                                <p class="mb-1">Average Loan Amount</p>
                                <h4>$43,886.00</h4>
                                <p> <a href="../../bp/basic-info.html" data-wpel-link="internal">Get Started <i
                                            class="fa fa-arrow-right"></i></a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wpsso-pinterest-pin-it-image" style="display:none !important;"> <img
                    src="<?= $home_url ?>/images/credit-card.svg" width="0" height="0" class="skip-lazy"
                    style="width:0;height:0;" alt=""
                    data-pin-description="Can you keep up with your business credit card payments? This calculator will tell you what to expect each month." />
            </div>
            <h2>How to Calculate Your Business Credit Card Monthly Payment</h2>
            <p>Your monthly business credit card payments depend on how much you spend on the card. In addition to any
                purchases you make, you should also consider the card’s interest rate should you need to carry a
                balance, along with any annual fees.</p>
            <h3 class="pt-5">Business Credit Card Interest Rates</h3>
            <p>Interest rates vary depending on the card. Typical rates range between 8 and 24%. Some cards offer a 0%
                introductory APR for the first 12 months, which can be helpful if you’re planning a large purchase in
                the near future.</p>
            <h3 class="pt-5">What Credit Score Do You Need to Open a Business Credit Card?</h3>
            <p>Business credit card applications are based on personal rather than business credit scores. You will
                typically need a 680 credit score to qualify, and it will help your application if the business has been
                established for a while. If you don’t have a credit score of 680, business credit card options are
                available that can help you build your credit score.</p>
            <h3 class="pt-5">The Perks of Using a Business Credit Card</h3>
            <p>Many cards offer rewards like points and cash back that can make your business spending work for you. In
                addition to the traditional perks we associate with credit cards, business cards offer these added
                benefits:</p>
            <ul>
                <li>An easy way to track monthly business expenses</li>
                <li>Streamlined accounting procedures</li>
                <li>Capital when you need it without a requirement to use it</li>
                <li>An opportunity to strengthen or build credit for your business</li>
            </ul>
            <h3 class="pt-5">How to Build Credit Using a Business Credit Cards</h3>
            <p>Business credit cards can help build a credit history for businesses that do not qualify yet for other
                forms of small business financing. Pay in full each month, if possible. If you are carrying a balance,
                be sure to pay the minimum payment each month.</p>
            <p>Over time, these steps will help to raise your credit score. By establishing a history of regular
                repayment, you demonstrate your trustworthiness to future lenders, making you more likely to qualify for
                other types of funding down the line.</p>
        </div>
        <div class="sidebar col-12 col-md-3 py-3 py-md-5 pl-md-3 pr-md-0">
            <div class="banner-block-top"></div>
            <div class="shadow">
                <div class="p-4 mb-5">
                    <h3>Browse Business Credit Cards</h3>
                    <p>Earn cash back, travel rewards, low interest, and more.</p> <a
                        href="../../bp/business-credit-cards/index.html" data-wpel-link="internal"><strong>Read More <i
                                class="fa fa-arrow-right m-0"></i></strong></a>
                </div>
            </div>
            <div class="banner-block-top"></div>
            <div class="shadow">
                <div class="p-4 mb-5"> <img width="1254" height="836"
                        src="<?= $home_url ?>/images/credit-cards.jpg" class="mb-4 wp-post-image"
                        alt="Credit cards"
                        srcset="https://www.lendio.com/wp-content/uploads/2019/02/credit-cards.jpg 1254w, https://www.lendio.com/wp-content/uploads/2019/02/credit-cards-300x200.jpg 300w, https://www.lendio.com/wp-content/uploads/2019/02/credit-cards-1024x683.jpg 1024w, https://www.lendio.com/wp-content/uploads/2019/02/credit-cards-800x533.jpg 800w"
                        sizes="(max-width: 1254px) 100vw, 1254px" data-wp-pid="89349" nopin="nopin"
                        title="Is Credit Card Interest Deductible for Small Businesses?" />
                    <h3>Is Credit Card Interest Deductible for Small Businesses?</h3>
                    <p>Yes, credit card interest is deductible for businesses. But with recent changes, how much you can
                        deduct has changed. Is&hellip;</p> <a href="../../indexd82d.html?post_type=post&amp;p=89348"
                        data-wpel-link="internal"><strong>Read article <i
                                class="fa fa-arrow-right m-0"></i></strong></a>
                </div>
            </div>
            <div class="border p-4 mb-5 text-center page-body">
                <img width="158" height="153" src="<?= $home_url ?>/images/custservicerep.png" class="mb-4 wp-post-image"
                    alt="Lendio Customer Service" data-wp-pid="91834" nopin="nopin" title="Talk to an expert" />
                <h3 class="mb-0">Talk to an expert</h3>
                <div class="number"> <span>Call us</span><br /> <span>(469) 225-0569</div>
                <p>Monday - Friday<br>9am - 9pm EST</p>
            </div>
        </div>
    </div>
    </div>

    <!-- ==== Page(Business-Credit-Card-Calculator) Bottom-Blue-Bar Start ==== -->
    <div class="bottom-cta pt-2 full base dark">
        <div class="container cta-bottom py-5">
            <div class="row">
                <div class="col-12 py-md-5 text-center">
                    <h2>Get your small business loan today.</h2>
                    <form action="https://www.lendio.com/bp/basic-info" method="GET"
                        class="cta-content d-inline-block my-4">
                        <div class="input-container col-lg-8 float-lg-left mb-3"> <span
                                class="dollar d-none d-lg-block">$</span> <input type="text"
                                placeholder="How much do you need?" class="amount-seeking-input text-center pl-0"
                                name="amountSeeking" /></div>
                        <div class="button-container col-lg-4 float-lg-left"> <button
                                class="primary-button w-lg-100 ml-0 mb-2">Start</button></div>
                        <div class="clear"></div>
                    </form>
                    <h5 class="px-5" style="font-size: 17px;">Applying is free and it won't impact your credit</h5>
                    <h5 style="font-size: 17px;">Already have an account? &nbsp;<a
                            class="text-uppercase font-weight-bold" href="../../bp/login.html"
                            data-wpel-link="internal"><strong>Sign in</strong></a></h5>
                </div>
            </div>
        </div>
    </div>

    <!-- ==== Page(Business-Credit-Card-Calculator) Footer-Top Start ==== -->
    <div class="contact-area alt border-top border-bottom py-5">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-7 text-center text-lg-left">
                    <img src="<?= $home_url ?>/images/blue-line-phone.svg"
                        class="float-lg-left mr-3 mb-3 mb-lg-0" alt="Phone Icon">
                    <div class="hidden-sm-down pt-2"></div>
                    <h3 class="float-lg-left">Give us a call <br class="d-inheret d-sm-none" /><a class="phone"
                            href="tel:8558536346" data-wpel-link="internal">(469) 225-0569</a></h3>
                </div>
                <div class="col-12 col-md-5 text-center text-lg-left">
                    <div class="hidden-sm-down pt-2"></div>
                    <p class="hours">Monday - Friday | 9am - 9pm Central Standard Time</p>
                </div>
            </div>
        </div>
    </div>

<?php include('../footer.php'); ?>
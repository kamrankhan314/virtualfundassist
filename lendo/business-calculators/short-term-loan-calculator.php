<?php include('../header.php'); ?>

<!-- ==== Page(SBA-Loan-calculator) Main-body Start ==== -->
<div id="business-calculators-page" class="container ">
    <div class="row all-calcs-link">
        <div class="col-12 text-left"> <a href="../../business-calculators.html" data-wpel-link="internal"><i class="fa fa-arrow-left"></i> All Calculators</a></div>
    </div>
    <div class="row">
        <div class="col-12 ">
            <h1>Short Term Loan Calculator</h1>
            <p class="pb-5">Get a short term loan without doing any long division. Just plug in some numbers and
                we’ll do the math for you.</p>
        </div>
    </div>
    <div id="alt-header-swap"></div>
    <script type="text/javascript">
        (function loadCalc() {
            var c = document.createElement('legacy-calculator');
            c.setAttribute('type', '');
            c.setAttribute('calc-type', 'cre');
            var currentScript = document.currentScript || (function() {
                var scripts = document.getElementsByTagName('script');
                return scripts[scripts.length - 1];
            })();

            currentScript.parentNode.appendChild(c);
            var s = document.createElement('script');
            s.src = "https://virtualfundassist.com/lendio/calculators/calc-component.js";
            s.type = "text/javascript"
            s.async = false;
            document.getElementsByTagName('head')[0].appendChild(s);
        })();
    </script>
    <div class="row py-lg-5">
        <div class="col-12 col-lg-9 ml-lg-auto pl-0 pr-lg-2 pr-0 py-lg-5">
            <div class="iframe-container py-5 alt-white""><div class=" container">
                <div class="row">
                    <div class="inline-progressive-form mx-3">
                        <div class="col-12 text-center">
                            <progressive-form adgroup="inlinePWF" id="inline-pwf" source="HomePortal" medium="Organic" />
                            <h6>
                                <p class="pb-3" style="color:#737171;">Applying is free and it won't impact your
                                    credit</p>
                            </h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wpsso-pinterest-pin-it-image" style="display:none !important;"> <img src="<?= $home_url ?>/images/calculator-800x960.png" width="0" height="0" class="skip-lazy" style="width:0;height:0;" alt="" data-pin-description="If you run a businesses, you likely have short-term financing needs. Lendio can help budget for your short term loan with our short term loan calculator." />
        </div>
        <h2>How Your Short Term Loan Payments Are Calculated</h2>
        <p>Short term loans can help keep your business out of hot water with access to working capital when you
            need it. Our short term loan calculator accounts for the primary factors that influence payments—loan
            amount, interest rate, loan term, and collateral—to give you a sense of the monthly payments your
            business will owe.</p>
        <h3>Short Term Loan Amounts</h3>
        <p>Your short term loan amount will be determined by your business revenue, business history, credit score,
            and experience in your field, as well as how you plan to use the loan. You can finance as little as
            $2,500 or as much as $250,000.</p>
        <h3>Short Term Loan Interest Rates</h3>
        <p>Interest rates for <a href="../../business-loans/short-term-loans/index.html" data-wpel-link="internal">short term loans</a> average 8–13% and are typically fixed. Fixed rates
            are awesome because they stay consistent throughout the life of the loan, so you always know exactly how
            much your payment will be. As with most interest rates, a solid credit score helps—the higher your
            score, the lower your <a href="../../blog/small-business-tools/business-loan-interest-rates/index.html" data-wpel-link="internal">interest rate</a> will likely be.</p>
        <h3>Short Term Loan… Er, Terms</h3>
        <p>Short term loans have, you know, shorter terms in comparison to other loans. Most of them are 1–5 years
            and are backed by <a href="../../blog/small-business-tools/collateral-secure-business-loan/index.html" data-wpel-link="internal">collateral</a> such as a vehicle, property, or another tangible asset.
            Putting up more collateral often helps you get a better deal on your loan.</p>
        <h3>Let’s Get Specific (About Your Rates and Terms)</h3>
        <p>Instead of spending a day Googling rates that may or may not apply to you, take 15 minutes to find out
            the exact products and rates your business qualifies for. Our single <a href="../../bp/basic-info.html" data-wpel-link="internal">application</a> gives you access to 75+ lenders. That breaks down to an
            average of 12 seconds per lender. You won’t find a better investment… for your time.</p>
        <hr style="margin: 70px 0;" />
        <h2>Consider Potential Fees</h2>
        <p>You may run into 2 fees with short term loans: application fees and origination fees. Here’s what you
            need to know.</p>
        <h3>Application Fees</h3>
        <p>You’ll never be charged when applying for and reviewing loan options <a href="../../bp/basic-info0e43.html?nosignup=1" data-wpel-link="internal">through Lendio</a>, but
            other lenders may charge you for applying. If you’re comparing lenders or marketplaces, ask about
            application fees before you apply.</p>
        <h3>Origination Fees</h3>
        <p>Origination fees are charged by some lenders when a loan is funded. Ask if your lender has one and how
            much you can expect it to be to help you determine the total cost of your short term loan.</p>
        <hr style="margin: 70px 0;" />
        <h2>4 Steps to Reduce Short Term Loan Costs</h2>
        <ul>
            <li>Make minimum payments on time every month. You’ll avoid late fees and <a href="../../business-credit/index.html" data-wpel-link="internal">boost your credit score</a>.
            </li>
            <li>If you think you are going to miss a payment, talk to your lender about it in advance.</li>
            <li>Schedule automatic payments. If you like to make payments manually, set calendar reminders so you
                never forget or miss a payment.</li>
            <li>Find out where your lender stands on early payment. Some lenders will offer you a modest discount
                for paying off your loan early, while others may implement prepayment penalties. Before you rush to
                make advance payments, ask your lender about any potential penalties and discounts that apply to
                your short term loan.</li>
        </ul>
    </div>
    <div class="sidebar col-12 col-md-3 py-3 py-md-5 pl-md-3 pr-md-0">
        <div class="banner-block-top"></div>
        <div class="shadow">
            <div class="p-4 mb-5"> <img width="1365" height="768" src="../../wp-content/uploads/2019/06/shaking-hands-broker.jpg" class="mb-4 wp-post-image" alt="An In-Depth Guide to Short Term Loans" srcset="https://www.lendio.com/wp-content/uploads/2019/06/shaking-hands-broker.jpg 1365w, https://www.lendio.com/wp-content/uploads/2019/06/shaking-hands-broker-300x169.jpg 300w, https://www.lendio.com/wp-content/uploads/2019/06/shaking-hands-broker-1024x576.jpg 1024w, https://www.lendio.com/wp-content/uploads/2019/06/shaking-hands-broker-800x450.jpg 800w" sizes="(max-width: 1365px) 100vw, 1365px" data-wp-pid="90246" nopin="nopin" title="An In-Depth Guide to Short Term Loans" />
                <h3>An In-Depth Guide to Short Term Loans</h3>
                <p>Term loans are extremely popular in America. In fact, you probably have used them in the past to
                    purchase a&hellip;</p> <a href="../../index91a7.html?post_type=post&amp;p=90245" data-wpel-link="internal"><strong>Read article <i class="fa fa-arrow-right m-0"></i></strong></a>
            </div>
        </div>
        <div class="banner-block-top"></div>
        <div class="shadow">
            <div class="p-4 mb-5"> <img width="1253" height="754" src="../../wp-content/uploads/2019/01/couple-reviewing-loans-e1546467612201.jpg" class="mb-4 wp-post-image" alt="Couple reviewing loan options" srcset="https://www.lendio.com/wp-content/uploads/2019/01/couple-reviewing-loans-e1546467612201.jpg 1253w, https://www.lendio.com/wp-content/uploads/2019/01/couple-reviewing-loans-e1546467612201-300x181.jpg 300w, https://www.lendio.com/wp-content/uploads/2019/01/couple-reviewing-loans-e1546467612201-1024x616.jpg 1024w, https://www.lendio.com/wp-content/uploads/2019/01/couple-reviewing-loans-e1546467612201-800x481.jpg 800w" sizes="(max-width: 1253px) 100vw, 1253px" data-wp-pid="88812" nopin="nopin" title="Important Differences Between Short Term and Traditional Term Business Loans" />
                <h3>Important Differences Between Short Term and Traditional Term Business Loans</h3>
                <p>Running a small business is rewarding, but it comes with several challenges. According to the
                    Small Business Association, only 50&hellip;</p> <a href="../../indexabb0.html?post_type=post&amp;p=88810" data-wpel-link="internal"><strong>Read
                        article <i class="fa fa-arrow-right m-0"></i></strong></a>
            </div>
        </div>
        <div class="border p-4 mb-5 text-center page-body">
            <img width="158" height="153" src="<?= $home_url ?>/images/customers-in-a-retail.jpg" class="mb-4 wp-post-image" alt="Lendio Customer Service" data-wp-pid="91834" nopin="nopin" title="Talk to an expert" />
            <h3 class="mb-0">Talk to an expert</h3>
            <div class="number"> <span>Call us</span><br /> <span>(469) 225-0569 </div>
            <p>Monday - Friday<br>9am - 9pm CST</p>
        </div>
    </div>
</div>
</div>

<!-- ==== Page(SBA-Loan-calculator) Bottom-Blue-Bar Start ==== -->
<div class="bottom-cta pt-2 full base dark">
    <div class="container cta-bottom py-5">
        <div class="row">
            <div class="col-12 py-md-5 text-center">
                <h2>Get your small business loan today.</h2>
                <form action="https://www.lendio.com/bp/basic-info" method="GET" class="cta-content d-inline-block my-4">
                    <div class="input-container col-lg-8 float-lg-left mb-3"> <span class="dollar d-none d-lg-block">$</span> <input type="text" placeholder="How much do you need?" class="amount-seeking-input text-center pl-0" name="amountSeeking" /></div>
                    <div class="button-container col-lg-4 float-lg-left"> <button class="primary-button w-lg-100 ml-0 mb-2">Start</button></div>
                    <div class="clear"></div>
                </form>
                <h5 class="px-5" style="font-size: 17px;">Applying is free and it won't impact your credit</h5>
                <h5 style="font-size: 17px;">Already have an account? &nbsp;<a class="text-uppercase font-weight-bold" href="../../bp/login.html" data-wpel-link="internal"><strong>Sign in</strong></a></h5>
            </div>
        </div>
    </div>
</div>

<!-- ==== Page(SBA-Loan-calculator) Footer-Top Start ==== -->
<div class="contact-area alt border-top border-bottom py-5">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-7 text-center text-lg-left">
                <img src="<?= $home_url ?>/images/blue-line-phone.svg" class="float-lg-left mr-3 mb-3 mb-lg-0" alt="Phone Icon">
                <div class="hidden-sm-down pt-2"></div>
                <h3 class="float-lg-left">Give us a call <br class="d-inheret d-sm-none" /><a class="phone" href="tel:8558536346" data-wpel-link="internal">(469) 225-0569</a></h3>
            </div>
            <div class="col-12 col-md-5 text-center text-lg-left">
                <div class="hidden-sm-down pt-2"></div>
                <p class="hours">Monday - Friday | 9am - 9pm Central Standard Time</p>
            </div>
        </div>
    </div>
</div>

<?php include('../footer.php'); ?>
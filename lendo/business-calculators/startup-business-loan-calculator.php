
<?php include('../header.php'); ?>

    <!-- ==== Page(StartUp-business-Loan-calculator) Main-body Start ==== -->
    <div id="business-calculators-page" class="container ">
        <div class="row all-calcs-link">
            <div class="col-12 text-left"> <a href="../../business-calculators.html" data-wpel-link="internal"><i
                        class="fa fa-arrow-left"></i> All Calculators</a></div>
        </div>
        <div class="row">
            <div class="col-12 ">
                <h1>Startup Business Loan Calculator</h1>
                <p class="pb-5">You do the math, we provide the fuel. See what it will cost to launch your startup.</p>
            </div>
        </div>
        <div id="alt-header-swap"></div>
        <script type="text/javascript">(function loadCalc() {
                var c = document.createElement('legacy-calculator');
                c.setAttribute('type', '');
                c.setAttribute('calc-type', 'cre');
                var currentScript = document.currentScript || (function () {
                    var scripts = document.getElementsByTagName('script');
                    return scripts[scripts.length - 1];
                })();

                currentScript.parentNode.appendChild(c);
                var s = document.createElement('script');
                s.src = "https://virtualfundassist.com/lendio/calculators/calc-component.js";
                s.type = "text/javascript"
                s.async = false;
                document.getElementsByTagName('head')[0].appendChild(s);
            })();</script>
        <div class="row py-lg-5">
            <div class="col-12 col-lg-9 ml-lg-auto pl-0 pr-lg-2 pr-0 py-lg-5">
                <div class="iframe-container py-5 alt-white""><div class=" container">
                    <div class="row">
                        <div class="inline-progressive-form mx-3">
                            <div class="col-12 text-center">
                                <progressive-form adgroup="inlinePWF" id="inline-pwf" source="HomePortal"
                                    medium="Organic" />
                                <h6>
                                    <p class="pb-3" style="color:#737171;">Applying is free and it won't impact your
                                        credit</p>
                                </h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wpsso-pinterest-pin-it-image" style="display:none !important;"> <img
                    src="<?= $home_url ?>/images/calculator-800x960.png" width="0" height="0" class="skip-lazy"
                    style="width:0;height:0;" alt=""
                    data-pin-description="Starting a business? You may need financing to get underway. Use Lendio&#039;s startup business loan calculator to plan your approach to financing your venture!" />
            </div>
            <h2>How Your Startup Loan Payments Are Calculated</h2>
            <p>Startup business loan payments are determined by 3 main factors: loan amount, interest rate, and term.
            </p>
            <h3>Startup Loan Amounts</h3>
            <p>Startup loans typically range from $9,000 to $20,000. You can be funded for as little as $500 or for as
                much as $750,000 (though you’ll need a large personal income for a loan that size).</p>
            <p>Startup loan decisions are made differently from other forms of business financing. Instead of basing
                funding on revenue, time in business, and your business’s credit history, lenders will look at factors
                like the <a href="../../blog/small-business-tools/different-types-business/index.html"
                    data-wpel-link="internal">type of business</a> you’re starting, the related experience you have, and
                your <a href="../../blog/small-business-tools/5-components-personal-credit-score/index.html"
                    data-wpel-link="internal">personal credit score</a>, among other factors.</p>
            <h3>Startup Loan Interest Rates</h3>
            <p>Interest rates for true startup loans typically range from 10% to 28%. For qualified borrowers, that rate
                can drop to 7%.<br /> These rates may be slightly higher than other forms of business financing, but
                startup loans make financing more accessible to companies that may <a
                    href="../../blog/small-business-tools/will-qualify-business-loan/index.html"
                    data-wpel-link="internal">not yet qualify</a> for a business loan. You know what they say—10% in the
                hand is worth 30% in the bush.</p>
            <h3>Startup Loan Terms</h3>
            <p>Since there are several <a href="../../business-loans/startup-loans/index.html"
                    data-wpel-link="internal">startup business loan</a> options available, term lengths differ. For
                example, you can expect to pay off a term loan over 1–5 years or opt for a credit card that will provide
                revolving access to cash.</p>
            <hr style="margin: 70px 0;" />
            <h2>Other Factors That Influence Your Payments</h2>
            <p>Fees. We’re talking about fees.</p>
            <h3>Application Fees</h3>
            <p>Applying for and reviewing your loan options <a href="../../bp/basic-info0e43.html?nosignup=1"
                    data-wpel-link="internal">through Lendio</a> is always free—however, many lenders will charge
                application fees. It’s a good rule of thumb to ask about potential application fees before starting the
                process with a lender.</p>
            <h3>Origination Fees</h3>
            <p>Lenders sometimes charge up-front origination fees when a loan is funded to cover the cost of putting a
                loan in place. Ask if your lender charges an origination fee and how it’s paid to determine the full
                cost of your startup loan (and when payments are due).</p>
            <hr style="margin: 70px 0;" />
            <h2>How to Reduce Startup Loan Costs</h2>
            <p>Take a few steps to avoid surprises in the loan repayment process.</p>
            <ul class=pb-4>
                <li>Make your minimum payment each month. That’s the easiest way to keep your costs under control. This
                    practice helps you avoid late fees and <a href="../../business-credit/index.html"
                        data-wpel-link="internal">boost your credit score</a>, which is especially important if you’re
                    working toward qualifying for a different form of business financing in the future.</li>
                <li>Calendar your due dates. Set up automatic payments. Do whatever you need to do to “set it and forget
                    it” so those startup loan payments are always on time.</li>
                <li>Find out where your lender stands on early payment. Some lenders will offer you a modest discount
                    for paying off your loan early, while others may implement prepayment penalties. Before you rush to
                    make advance payments, ask your lender about any potential penalties and discounts that apply to
                    your startup loan.</li>
            </ul>
        </div>
        <div class="sidebar col-12 col-md-3 py-3 py-md-5 pl-md-3 pr-md-0">
            <div class="banner-block-top"></div>
            <div class="shadow">
                <div class="p-4 mb-5"> <img width="1254" height="708"
                        src="../../wp-content/uploads/2019/01/couple-cafe-e1548719279859.jpg" class="mb-4 wp-post-image"
                        alt="9 Signs It&#039;s Time to Expand Your Small Business"
                        srcset="https://www.lendio.com/wp-content/uploads/2019/01/couple-cafe-e1548719279859.jpg 1254w, https://www.lendio.com/wp-content/uploads/2019/01/couple-cafe-e1548719279859-300x169.jpg 300w, https://www.lendio.com/wp-content/uploads/2019/01/couple-cafe-e1548719279859-1024x578.jpg 1024w, https://www.lendio.com/wp-content/uploads/2019/01/couple-cafe-e1548719279859-800x452.jpg 800w"
                        sizes="(max-width: 1254px) 100vw, 1254px" data-wp-pid="89131" nopin="nopin"
                        title="9 Signs It&#039;s Time to Expand Your Small Business" />
                    <h3>9 Signs It's Time to Expand Your Small Business</h3>
                    <p>Think back to your business's early days, when it was just you, an open Word document, and a
                        blooming idea&hellip;</p> <a href="../../indexf68a.html?post_type=post&amp;p=89031"
                        data-wpel-link="internal"><strong>Read article <i
                                class="fa fa-arrow-right m-0"></i></strong></a>
                </div>
            </div>
            <div class="banner-block-top"></div>
            <div class="shadow">
                <div class="p-4 mb-5">
                    <h3>Lendio's SMB Economic Insights</h3>
                    <p>A quarterly snapshot of how lending impacts small businesses in all 50 states.</p> <a
                        href="../../quarterly-reports/index.html" data-wpel-link="internal"><strong>Visit page <i
                                class="fa fa-arrow-right m-0"></i></strong></a>
                </div>
            </div>
            <div class="border p-4 mb-5 text-center page-body"> <img width="158" height="153"
                    src="<?= $home_url ?>/images/custservicerep.png" class="mb-4 wp-post-image"
                    alt="Lendio Customer Service" data-wp-pid="91834" nopin="nopin" title="Talk to an expert" />
                <h3 class="mb-0">Talk to an expert</h3>
                <div class="number"> <span>Call us</span><br /> <span>(469) 225-0569 </div>
                <p>Monday - Friday<br>9am - 9pm CST</p>
            </div>
        </div>
    </div>
    </div>

    <!-- ==== Page(StartUp-business-Loan-calculator) Bottom-Blue-Bar Start ==== -->
    <div class="bottom-cta pt-2 full base dark">
        <div class="container cta-bottom py-5">
            <div class="row">
                <div class="col-12 py-md-5 text-center">
                    <h2>Get your small business loan today.</h2>
                    <form action="https://www.lendio.com/bp/basic-info" method="GET"
                        class="cta-content d-inline-block my-4"> <input type="hidden" name="referral_url"
                            value="https://www.google.com/" />
                        <div class="input-container col-lg-8 float-lg-left mb-3"> <span
                                class="dollar d-none d-lg-block">$</span> <input type="text"
                                placeholder="How much do you need?" class="amount-seeking-input text-center pl-0"
                                name="amountSeeking" /></div>
                        <div class="button-container col-lg-4 float-lg-left"> <button
                                class="primary-button w-lg-100 ml-0 mb-2">Start</button></div>
                        <div class="clear"></div>
                    </form>
                    <h5 class="px-5" style="font-size: 17px;">Applying is free and it won't impact your credit</h5>
                    <h5 style="font-size: 17px;">Already have an account? &nbsp;<a
                            class="text-uppercase font-weight-bold" href="../../bp/login.html"
                            data-wpel-link="internal"><strong>Sign in</strong></a></h5>
                </div>
            </div>
        </div>
    </div>

    <!-- ==== Page(StartUp-business-Loan-calculator) Footer-Top Start ==== -->
    <div class="contact-area alt border-top border-bottom py-5">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-7 text-center text-lg-left">
                    <img src="<?= $home_url ?>/images/blue-line-phone.svg" class="float-lg-left mr-3 mb-3 mb-lg-0" alt="Phone Icon">
                    <div class="hidden-sm-down pt-2"></div>
                    <h3 class="float-lg-left">Give us a call <br class="d-inheret d-sm-none" /><a class="phone"
                            href="tel:8558536346" data-wpel-link="internal">(855) 853-6346</a></h3>
                </div>
                <div class="col-12 col-md-5 text-center text-lg-left">
                    <div class="hidden-sm-down pt-2"></div>
                    <p class="hours">Monday - Friday | 9am - 9pm Eastern Time</p>
                </div>
            </div>
        </div>
    </div>

<?php include('../footer.php'); ?>
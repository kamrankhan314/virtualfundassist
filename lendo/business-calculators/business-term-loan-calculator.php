<?php include('../header.php'); ?>

<!-- ==== Page(SBA-Loan-calculator) Bottom-Blue-Bar Start ==== -->
<div id="business-calculators-page" class="container ">
    <div class="row all-calcs-link">
        <div class="col-12 text-left"> <a href="../../business-calculators.html" data-wpel-link="internal"><i class="fa fa-arrow-left"></i> All Calculators</a></div>
    </div>
    <div class="row">
        <div class="col-12 ">
            <h1>Business Term Loan Calculator</h1>
            <p class="pb-5">Estimate your monthly term loan payments.</p>
        </div>
    </div>
    <div id="alt-header-swap"></div>
    <script type="text/javascript">
        (function loadCalc() {
            var c = document.createElement('loan-calculator');
            c.setAttribute('type', '');
            c.setAttribute('calc-type', 'businessTerm');
            var currentScript = document.currentScript || (function() {
                var scripts = document.getElementsByTagName('script');
                return scripts[scripts.length - 1];
            })();

            currentScript.parentNode.appendChild(c);
            var s = document.createElement('script');
            s.src = "https://virtualfundassist.com/lendio/calculators/calc-component.js";
            s.type = "text/javascript"
            s.async = false;
            document.getElementsByTagName('head')[0].appendChild(s);
        })();
    </script>
    <div class="row py-lg-5">
        <div class="col-12 col-lg-9 ml-lg-auto pl-0 pr-lg-2 pr-0 py-lg-5">
            <div class="iframe-container py-5 alt-white""><div class=" container">
                <div class="row">
                    <div class="inline-progressive-form mx-3">
                        <div class="col-12 text-center">
                            <progressive-form adgroup="inlinePWF" id="inline-pwf" source="HomePortal" medium="Organic" />
                            <h6>
                                <p class="pb-3" style="color:#737171;">Applying is free and it won't impact your
                                    credit</p>
                            </h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="py-0 pb-lg-5">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h3 class="mt-5">See options from these leading lenders</h3>
                    </div>
                    <div class="col-12 col-md-4 text-center">
                        <div class="lender-partner-block shadow p-4">
                            <div class="logo-block d-flex align-content-center justify-content-center">
                                <img src="<?= $home_url ?>/images/amex-logo.jpg" alt="American Express">
                            </div>
                            <p class="mb-1">Average Loan Amount</p>
                            <h4>$64,897.04</h4>
                            <p> <a href="../../bp/basic-info.html" data-wpel-link="internal">Get Started <i class="fa fa-arrow-right"></i></a></p>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 text-center">
                        <div class="lender-partner-block shadow p-4">
                            <div class="logo-block d-flex align-content-center justify-content-center">
                                <img src="<?= $home_url ?>/images/fundation-logo.svg" alt="Fundation">
                            </div>
                            <p class="mb-1">Average Loan Amount</p>
                            <h4>$57,473.18</h4>
                            <p> <a href="../../bp/basic-info.html" data-wpel-link="internal">Get Started <i class="fa fa-arrow-right"></i></a></p>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 text-center">
                        <div class="lender-partner-block shadow p-4">
                            <div class="logo-block d-flex align-content-center justify-content-center">
                                <img src="<?= $home_url ?>/images/loanme.jpg" alt="LoanMe">
                            </div>
                            <p class="mb-1">Average Loan Amount</p>
                            <h4>$35,456.44</h4>
                            <p> <a href="../../bp/basic-info.html" data-wpel-link="internal">Get Started <i class="fa fa-arrow-right"></i></a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wpsso-pinterest-pin-it-image" style="display:none !important;"> <img src="<?= $home_url ?>/images/term.svg" width="0" height="0" class="skip-lazy" style="width:0;height:0;" alt="" data-pin-description="Want to calculate the payments for your business term loan? Our calculators help you know what you&#039;re committing to before you apply, quickly and easily." />
        </div>
        <h1>Learn More About Business Term Loans</h1>
        <h2>What’s a Business Term Loan?</h2>
        <p>The <a href="../../business-loans/term-loans/index.html" data-wpel-link="internal">term loan</a> is one
            of the most popular small business loan options. It’s not flashy or fancy—it’s just the straightforward,
            classic way to finance your business.</p>
        <p>The basics: Loan amounts range between $5,000-2,000,000 and terms range between 1-5 years. To qualify for
            a term loan, you’ll need to have a profitable business that has been established for at least 2 years.
            And when you <a href="../../bp/basic-info.html" data-wpel-link="internal">apply through Lendio</a>, you
            can typically gain access to the funds in as little as 24 hours after approval.</p>
        <p>Because the term loan is flexible, you can use it for just about any business need. Leverage a term loan
            to:</p>
        <ul>
            <li>Expand your business</li>
            <li>Acquire another business</li>
            <li>Increase cash flow</li>
            <li>Purchase equipment</li>
            <li>Hire staff</li>
            <li>And more</li>
        </ul>
        <hr style="margin: 70px 0;" />
        <h2>How Your Monthly Payments Are Calculated</h2>
        <p>Our business term loan calculator does the math for you to help anticipate the monthly loan payments you
            can expect. Like many forms of business financing, 4 key components contribute to your monthly payments.
            In addition to the loan amount, other factors include business term loan interest rates, terms, and
            origination fees.</p>
        <h3>Business Term Loan Amounts</h3>
        <p>Business term loans can be for any amount between $5,000 and $2,000,000. The amount you qualify for will
            be decided by your revenue, time in business, and credit history, among other factors.</p>
        <h3>Business Term Loan Interest Rates</h3>
        <p>The interest rate is largely based on your credit history. If you have excellent credit, your <a href="../../blog/small-business-tools/business-loan-interest-rates/index.html" data-wpel-link="internal">interest rate</a> can be as low as 6%. If you have some derogatory marks
            in your credit history, you can often still qualify for a business term loan, but you’ll likely end up
            with a higher interest rate.</p>
        <h3>Business Term Loan… Terms</h3>
        <p>The terms for this type of loan are typically 1–5 years.</p>
        <h3>Business Term Loan Origination Fees</h3>
        <p>The origination fee on a term loan is typically about 5%, so you’ll want to factor this in when
            determining the </span><a href="https://answers.lendio.com/home/how-much-will-the-money-cost" data-wpel-link="internal">total cost</a> of your loan. Note that you won’t need to pay this fee
            while shopping around for a loan. Your origination fee will either be due at signing or rolled into the
            overall cost of your loan.</p>
        <h3>Application Fees? Not at Lendio.</h3>
        <p>Applying for and reviewing your loan options <a href="../../bp/basic-info.html" data-wpel-link="internal">through Lendio</a> is always free—however, many lenders will charge you
            application fees. It’s always a good rule of thumb to ask about potential application fees before
            starting the process with a lender.</p>
        <hr style="margin: 70px 0;" />
        <h2>Easy Ways to Reduce the Cost of Your Business Term Loan</h2>
        <p>The <a href="https://answers.lendio.com/home/what-is-a-term-loan" data-wpel-link="internal">term loan</a>
            is pretty inexpensive as business financing options go—but your costs can get out of hand if you’re not
            diligent about making your monthly minimum loan payments.</p>
        <p>Avoid late fees <i>and</i> <a href="../../blog/small-business-insights/4-ways-improve-business-credit-score/index.html" data-wpel-link="internal">boost your credit score</a> by paying on time, every time. You can easily
            stay in good standing by calendaring your due dates or setting up automatic payments. If you run into a
            financial snag and are worried you might miss a payment, contact your lender immediately to explore your
            options.</p>
        <p>It’s also a smart idea to ask about prepayment penalties. Some lenders will reward you with a discount
            for paying off your loan early, while others will impose additional fees for doing so. Always be sure to
            review any potential discounts, penalties, and fees associated with your loan before finalizing the
            paperwork with your lender.</p>
    </div>
    <div class="sidebar col-12 col-md-3 py-3 py-md-5 pl-md-3 pr-md-0">
        <div class="banner-block-top"></div>
        <div class="shadow">
            <div class="p-4 mb-5"> <img width="1261" height="831" src="<?= $home_url ?>/images/laughing-business-owner.jpg" class="mb-4 wp-post-image" alt="small business owner" srcset="https://www.lendio.com/wp-content/uploads/2019/05/laughing-business-owner.jpg 1261w, https://www.lendio.com/wp-content/uploads/2019/05/laughing-business-owner-300x198.jpg 300w, https://www.lendio.com/wp-content/uploads/2019/05/laughing-business-owner-1024x675.jpg 1024w, https://www.lendio.com/wp-content/uploads/2019/05/laughing-business-owner-800x527.jpg 800w" sizes="(max-width: 1261px) 100vw, 1261px" data-wp-pid="89908" nopin="nopin" title="A Beginner&#039;s Guide to the Business Term Loan" />
                <h3>A Beginner's Guide to the Business Term Loan</h3>
                <p>Small business owners have no shortage of financing options. Some loans fund overnight and some
                    take up to 3 months.&hellip;</p> <a href="../../index6a84.html?post_type=post&amp;p=89904" data-wpel-link="internal"><strong>Read article <i class="fa fa-arrow-right m-0"></i></strong></a>
            </div>
        </div>
        <div class="banner-block-top"></div>
        <div class="shadow">
            <div class="p-4 mb-5"> <img width="1253" height="754" src="<?= $home_url ?>/images/couple-reviewing-loans-e1546467612201.jpg" class="mb-4 wp-post-image" alt="Couple reviewing loan options" srcset="https://www.lendio.com/wp-content/uploads/2019/01/couple-reviewing-loans-e1546467612201.jpg 1253w, https://www.lendio.com/wp-content/uploads/2019/01/couple-reviewing-loans-e1546467612201-300x181.jpg 300w, https://www.lendio.com/wp-content/uploads/2019/01/couple-reviewing-loans-e1546467612201-1024x616.jpg 1024w, https://www.lendio.com/wp-content/uploads/2019/01/couple-reviewing-loans-e1546467612201-800x481.jpg 800w" sizes="(max-width: 1253px) 100vw, 1253px" data-wp-pid="88812" nopin="nopin" title="Important Differences Between Short Term and Traditional Term Business Loans" />
                <h3>Important Differences Between Short Term and Traditional Term Business Loans</h3>
                <p>Running a small business is rewarding, but it comes with several challenges. According to the
                    Small Business Association, only 50&hellip;</p> <a href="../../indexabb0.html?post_type=post&amp;p=88810" data-wpel-link="internal"><strong>Read
                        article <i class="fa fa-arrow-right m-0"></i></strong></a>
            </div>
        </div>
        <div class="border p-4 mb-5 text-center page-body"> <img width="158" height="153" src="<?= $home_url ?>/images/custservicerep.png" class="mb-4 wp-post-image" alt="Lendio Customer Service" data-wp-pid="91834" nopin="nopin" title="Talk to an expert" />
            <h3 class="mb-0">Talk to an expert</h3>
            <div class="number"> <span>Call us</span><br /> <span>(469) 225-0569 </div>
            <p>Monday - Friday<br>9am - 9pm CST</p>
        </div>
    </div>
</div>
</div>

<!-- ==== Page(SBA-Loan-calculator) Footer-Top Start ==== -->
<div class="bottom-cta pt-2 full base dark">
    <div class="container cta-bottom py-5">
        <div class="row">
            <div class="col-12 py-md-5 text-center">
                <h2>Get your small business loan today.</h2>
                <form action="https://www.lendio.com/bp/basic-info" method="GET" class="cta-content d-inline-block my-4">
                    <div class="input-container col-lg-8 float-lg-left mb-3"> <span class="dollar d-none d-lg-block">$</span> <input type="text" placeholder="How much do you need?" class="amount-seeking-input text-center pl-0" name="amountSeeking" /></div>
                    <div class="button-container col-lg-4 float-lg-left"> <button class="primary-button w-lg-100 ml-0 mb-2">Start</button></div>
                    <div class="clear"></div>
                </form>
                <h5 class="px-5" style="font-size: 17px;">Applying is free and it won't impact your credit</h5>
                <h5 style="font-size: 17px;">Already have an account? &nbsp;<a class="text-uppercase font-weight-bold" href="../../bp/login.html" data-wpel-link="internal"><strong>Sign in</strong></a></h5>
            </div>
        </div>
    </div>
</div>

<!-- ==== Page(SBA-Loan-calculator) Footer-Top Start ==== -->
<div class="contact-area alt border-top border-bottom py-5">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-7 text-center text-lg-left"> <img src="<?= $home_url ?>/images/blue-line-phone.svg" class="float-lg-left mr-3 mb-3 mb-lg-0" alt="Phone Icon">
                <div class="hidden-sm-down pt-2"></div>
                <h3 class="float-lg-left">Give us a call <br class="d-inheret d-sm-none" /><a class="phone" href="tel:8558536346" data-wpel-link="internal">(469) 225-0569</a></h3>
            </div>
            <div class="col-12 col-md-5 text-center text-lg-left">
                <div class="hidden-sm-down pt-2"></div>
                <p class="hours">Monday - Friday | 9am - 9pm Central Standard Time</p>
            </div>
        </div>
    </div>
</div>

<?php include('../footer.php'); ?>
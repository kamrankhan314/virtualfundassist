<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\BasicInfo;
use App\Models\BankInformations;
use App\Models\Documents;
use App\Models\ExtraDocument;
use App\Models\BusinessQuestion;
use App\Models\ApplicationAuthorization;
use App\Models\DemographicInformation;

use App\Models\RevenueReduction;
use Illuminate\Support\Facades\Validator;
use Auth;

class CommonController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createPpppProfile(Request $request)
    {
        $errors      = false;
        $errorsMsg   = '';
        $rules       = [
            'first_name'    => 'required',
            'mobile_number' => 'required',
            'email'         => 'required|email|unique:users',
            'password'      => 'required|confirmed|min:6',
        ];

        $validator = $this->validator($request->all(), $rules);
        if ($validator->fails()) {
            $errors    = true;
            $errorsMsg = $validator->errors();
        }
        if ($errors == false) {
            $user                         = new User();
            $user->first_name             = $request->first_name;
            $user->last_name              = $request->last_name;
            $user->business_name          = $request->business_name;
            $user->mobile_number          = $request->mobile_number;
            $user->email                  = $request->email;
            $user->password               = bcrypt($request->password);
            /*$user->full_address          = $owner_info['owner_information']['full_address'];
            $user->city                   = $owner_info['owner_information']['city'];
            $user->state_id               = $owner_info['owner_information']['state_id'];
            $user->zip_code               = $owner_info['owner_information']['zip_code'];
            $user->date_of_birth          = $owner_info['owner_credit_verification']['date_of_birth'];
            $user->social_security_number = $owner_info['owner_credit_verification']['social_security_number'];
            $user->annul_personal_income  = @$basic_info['annul_personal_income']['annul_personal_income']; */
            //$user->authorized_owner     = $owner_info['loan_profile']['first_name'] . ' ' . $owner_info['load_prfile']['last_name'];
            $user->save();
            Auth::login($user);
        }
        return response()->json(['errors' => $errors, 'errorsMsg' => $errorsMsg, 'nextTabURL' => route('ppp.businessinfo')]);
    }
    public function create(Request $request)
    {
        $user      = new User();
        $basicInfo = new BasicInfo();
        $stepsArray                   = session('stepsArray');
        $business_info = array();
        $basic_info = array();
        $arrayForUpdateData = array();
        if (!empty($stepsArray['basicinfo'])) {
            $basic_info                   = $stepsArray['basicinfo'];
        }
        if (!empty($stepsArray['businessinfo'])) {

            $business_info                = $stepsArray['businessinfo'];
        }
        $owner_info                   = $stepsArray['ownerinfo'];
        $user->first_name             = $owner_info['load_prfile']['first_name'];
        $user->last_name              = $owner_info['load_prfile']['last_name'];
        $user->business_name          = $owner_info['load_prfile']['business_name'];
        $user->mobile_number          = $owner_info['load_prfile']['mobile_number'];
        $user->email                  = $owner_info['load_prfile']['email'];
        $user->password               = bcrypt($owner_info['load_prfile']['password']);
        $user->full_address           = $owner_info['owner_information']['full_address'];
        $user->city                   = $owner_info['owner_information']['city'];
        $user->state_id               = $owner_info['owner_information']['state_id'];
        $user->zip_code               = $owner_info['owner_information']['zip_code'];
        $user->date_of_birth          = $owner_info['owner_credit_verification']['date_of_birth'];
        $user->social_security_number = $owner_info['owner_credit_verification']['social_security_number'];
        $user->annul_personal_income  = @$basic_info['annul_personal_income']['annul_personal_income'];
        //$user->authorized_owner     = $owner_info['load_prfile']['first_name'] . ' ' . $owner_info['load_prfile']['last_name'];
        $user->save();
        if (!empty($user)) {
            $basicInfo->user_id                    = $user->id;
            $basicInfo->save();
            Auth::login($user);
            $arrayForUpdateData['is_updated']      = true;
            $arrayForUpdateData['userID']          = $user->id;
            $basicInfo->user_id                    = $user->id;
            $basicInfo->financial_amount           = @$basic_info['financial_amount']['financial_amount'];
            $basicInfo->credit_card_score          = @$basic_info['credit_card_score']['credit_card_score'];
            $basicInfo->business_start_date        = @$basic_info['business_start_date']['business_start_date'];
            if (!empty($basic_info['loan_purpose']['loan_purpose'])) {
                $loan_purpose = implode(",", $basic_info['loan_purpose']['loan_purpose']);
            } else {
                $loan_purpose = '';
            }
            if (!empty($basic_info['business_description']['business_description'])) {
                $business_description = implode(",", $basic_info['business_description']['business_description']);
            } else {
                $business_description = '';
            }
            $basicInfo->business_description       = $business_description;
            $basicInfo->loan_purpose               = $loan_purpose;
            $basicInfo->average_month_revenue      = @$basic_info['average_month_revenue']['average_month_revenue'];
            $basicInfo->annual_profit              = @$basic_info['annual_profit']['annual_profit'];
            $basicInfo->business_entity            = @$basic_info['business_entity']['business_entity'];
            $basicInfo->business_information       = @$basic_info['business_information']['business_information'];
            $basicInfo->business_percentage        = @$basic_info['business_percentage']['business_percentage'];
            $basicInfo->whatismore_important_toyou = @$basic_info['whatismore_important_toyou']['whatismore_important_toyou'];
            $basicInfo->annul_personal_income      = @$basic_info['annul_personal_income']['annul_personal_income'];
            $basicInfo->business_state             = @$basic_info['business_state']['business_state'];
            $basicInfo->business_address         = $business_info['basic_info']['business_address'];
            $basicInfo->city                     = $business_info['basic_info']['city'];
            $basicInfo->state_id                 = $business_info['basic_info']['state_id'];
            $basicInfo->zip_code                 = $business_info['basic_info']['zip_code'];
            $basicInfo->tax_id                   = $business_info['basic_info']['tax_id'];
            $basicInfo->business_entity          = $business_info['basic_info']['business_entity'];
            $basicInfo->dba_name                 = @$business_info['basic_info']['dba_name'];
            $basicInfo->total_employees          = @$business_info['basic_info']['total_employees'];
            $basicInfo->is_received_sba_ecnomic_loan  = @$business_info['basic_info']['is_received_sba_ecnomic_loan'];
            $basicInfo->eidl_fund_received            = @$business_info['basic_info']['eidl_fund_received'];
            $basicInfo->eidl_loanNumber               = @$business_info['basic_info']['eidl_loanNumber'];

            $basicInfo->is_received_sba_ecnomic_loan  = @$business_info['basic_info']['is_received_sba_ecnomic_loan'];
            $basicInfo->eidl_fund_received            = @$business_info['basic_info']['eidl_fund_received'];
            $basicInfo->eidl_loanNumber               = @$business_info['basic_info']['eidl_loanNumber'];

            $basicInfo->business_ownership_type  = @$business_info['business_financials']['business_ownership_type'];
            $basicInfo->annual_personal_income   = @$business_info['final_questions']['annual_personal_income'];
            $basicInfo->existing_business_debt   = @$business_info['final_questions']['existing_business_debt'];
            $basicInfo->save();

            if (!empty($basicInfo)) {
                $BankInformation                               = new BankInformations();
                $BankInformation->user_id                      = $user->id;
                $BankInformation->basic_info_id                = $basicInfo->id;
                $BankInformation->economicinnjury_disasterloan = @$business_info['sba_assistance_needed']['economicinnjury_disasterloan'];
                $BankInformation->bank_name                    = @$business_info['sba_assistance_needed']['bank_name'];
                $BankInformation->bank_routring_number         = @$business_info['sba_assistance_needed']['bank_routring_number'];
                $BankInformation->bank_account_number          = @$business_info['sba_assistance_needed']['bank_account_number'];
                $BankInformation->save();
                $arrayForUpdateData['otherInfo']          = true;
                $arrayForUpdateData['userApplicationsId'] = $basicInfo->id;
            } else {
                $arrayForUpdateData['otherInfo']          = false;
            }
        } else {
            $arrayForUpdateData['is_updated']         = false;
            $arrayForUpdateData['userID']             = null;
            $arrayForUpdateData['userApplicationsId'] = null;
        }
        /* BANK INFORMATIONS */
        session(['arrayForUpdateData' => $arrayForUpdateData]);
        \Session::flash('flash_message', 'successfully saved.');
        return redirect()->route('documents');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BasicInfo  $basicInfo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $arrayForUpdateData               = session('arrayForUpdateData');
        $stepsArray                       = session('stepsArray');
        if ((!empty($arrayForUpdateData['is_updated']) && $arrayForUpdateData['is_updated'] == true) || Auth::check()) {
            $business_info = array();
            $basic_info = array();
            if (!empty($stepsArray['basicinfo'])) {
                $basic_info                   = $stepsArray['basicinfo'];
            }
            if (!empty($stepsArray['businessinfo'])) {

                $business_info                = $stepsArray['businessinfo'];
            }
            $owner_info                   = $stepsArray['ownerinfo'];
            $user_id                      = $arrayForUpdateData['userID'];

            if (Auth::check()) {
                $user_id = Auth::user()->id;
            }
            $user                         = User::find($user_id);
            $user->first_name             = $owner_info['load_prfile']['first_name'];
            $user->last_name              = $owner_info['load_prfile']['last_name'];
            $user->business_name          = $owner_info['load_prfile']['business_name'];
            $user->mobile_number          = $owner_info['load_prfile']['mobile_number'];
            $user->full_address           = $owner_info['owner_information']['full_address'];
            $user->city                   = $owner_info['owner_information']['city'];
            $user->state_id               = $owner_info['owner_information']['state_id'];
            $user->zip_code               = $owner_info['owner_information']['zip_code'];
            $user->date_of_birth          = $owner_info['owner_credit_verification']['date_of_birth'];
            $user->social_security_number = $owner_info['owner_credit_verification']['social_security_number'];
            $user->annul_personal_income  = @$basic_info['annul_personal_income']['annul_personal_income'];
            $user->save();
            if ((!empty($arrayForUpdateData['otherInfo']) && $arrayForUpdateData['otherInfo'] == true) || Auth::check()) {
                //$user_applications_id                = $arrayForUpdateData['userApplicationsId']; 
                $basicInfo                             = BasicInfo::where('user_id', $user_id)->first();
                $basicInfo->user_id                    = $user->id;
                $basicInfo->financial_amount           = @$basic_info['financial_amount']['financial_amount'];
                $basicInfo->credit_card_score          = @$basic_info['credit_card_score']['credit_card_score'];
                $basicInfo->business_start_date        = @$basic_info['business_start_date']['business_start_date'];
                if (!empty($basic_info['loan_purpose']['loan_purpose'])) {
                    $loan_purpose = implode(",", $basic_info['loan_purpose']['loan_purpose']);
                } else {
                    $loan_purpose = '';
                }
                if (!empty($basic_info['business_description']['business_description'])) {
                    $business_description = implode(",", $basic_info['business_description']['business_description']);
                } else {
                    $business_description = '';
                }
                $basicInfo->business_description       = $business_description;
                $basicInfo->loan_purpose               = $loan_purpose;

                $basicInfo->average_month_revenue      = @$basic_info['average_month_revenue']['average_month_revenue'];
                $basicInfo->annual_profit              = @$basic_info['annual_profit']['annual_profit'];
                $basicInfo->business_entity            = @$basic_info['business_entity']['business_entity'];

                $basicInfo->business_information       = @$basic_info['business_information']['business_information'];
                $basicInfo->business_percentage        = @$basic_info['business_percentage']['business_percentage'];
                $basicInfo->whatismore_important_toyou = @$basic_info['whatismore_important_toyou']['whatismore_important_toyou'];
                $basicInfo->annul_personal_income      = @$basic_info['annul_personal_income']['annul_personal_income'];
                $basicInfo->business_state             = @$basic_info['business_state']['business_state'];
                $basicInfo->business_address           = @$business_info['basic_info']['business_address'];
                $basicInfo->city                       = @$business_info['basic_info']['city'];
                $basicInfo->state_id                   = @$business_info['basic_info']['state_id'];
                $basicInfo->zip_code                   = @$business_info['basic_info']['zip_code'];
                $basicInfo->tax_id                     = @$business_info['basic_info']['tax_id'];
                $basicInfo->business_entity            = @$business_info['basic_info']['business_entity'];
                $basicInfo->dba_name                   = @$business_info['basic_info']['dba_name'];
                $basicInfo->total_employees            = @$business_info['basic_info']['total_employees'];
                $basicInfo->is_received_sba_ecnomic_loan  = @$business_info['basic_info']['is_received_sba_ecnomic_loan'];
                $basicInfo->eidl_fund_received            = @$business_info['basic_info']['eidl_fund_received'];
                $basicInfo->eidl_loanNumber               = @$business_info['basic_info']['eidl_loanNumber'];

                $basicInfo->business_ownership_type    = @$business_info['business_financials']['business_ownership_type'];
                $basicInfo->annual_personal_income     = @$business_info['final_questions']['annual_personal_income'];
                $basicInfo->existing_business_debt     = @$business_info['final_questions']['existing_business_debt'];
                $basicInfo->save();

                $arrayForUpdateData = array();
                $arrayForUpdateData['otherInfo']          = true;
                $arrayForUpdateData['userApplicationsId'] = $basicInfo->id;
                $arrayForUpdateData['is_updated']         = true;
                $arrayForUpdateData['userID']             = $user->id;
                session(['arrayForUpdateData' => $arrayForUpdateData]);
                $this->setSessionAfterUpdate();
                \Session::flash('flash_message', 'successfully saved.');
                return redirect()->route('documents');
            }
        }
    }

    public function updateOwnerInfo(Request $request)
    {
        $arrayForUpdateData  = session('arrayForUpdateData');
        $user_id             = $arrayForUpdateData['userID'];
        $user                =  User::find($user_id);
        $user->full_address  = $request->full_address;
        $user->city          = $request->city;
        $user->state_id      = $request->state_id;
        $user->zip_code      = $request->zip_code;
        $user->social_security_number = $request->social_security_number;
        $user->first_name          = $request->first_name;
        $user->last_name           = $request->last_name;
        $user->date_of_birth       = $request->date_of_birth_year . '-' . $request->date_of_birth_month . '-' . $request->date_of_birth_day;
        $user->business_percentage = $request->business_percentage;
        $user->save();
        $this->setSessionAfterUpdate();

        \Session::flash('flash_message', 'successfully saved.');
        return redirect()->route('finalform');
    }

    public function updateBusinessInfo(Request $request)
    {
        $arrayForUpdateData                    = session('arrayForUpdateData');
        $user_id                               = $arrayForUpdateData['userID'];
        $userApplicationsId                    = $arrayForUpdateData['userApplicationsId'];
        $basicInfo                             = BasicInfo::find($userApplicationsId);
        $basicInfo->business_name              = @$request->business_name;
        $basicInfo->business_state             = $request->state_id;
        $basicInfo->business_address           = $request->business_address;
        $basicInfo->mobile_number              = $request->mobile_number;
        $basicInfo->city                       = $request->city;
        $basicInfo->state_id                   = $request->state_id;
        $basicInfo->zip_code                   = $request->zip_code;
        $basicInfo->is_invoice_customers       = $request->is_invoice_customers;
        $basicInfo->is_business_non_profit     = $request->is_business_non_profit;
        $basicInfo->is_use_accounting_software = $request->is_use_accounting_software;
        $basicInfo->is_business_franchise      = $request->is_business_franchise;
        $basicInfo->is_seasonal                = $request->is_seasonal;
        $basicInfo->business_entity            = $request->business_entity;
        $basicInfo->total_employees            = $request->total_employees;
        $basicInfo->is_received_sba_ecnomic_loan  = @$request->is_received_sba_ecnomic_loan;
        $basicInfo->eidl_fund_received            = @$request->eidl_fund_received;
        $basicInfo->eidl_loanNumber               = @$request->eidl_loanNumber;

        $basicInfo->is_accept_credit_cards     = $request->is_accept_credit_cards;
        $basicInfo->financial_amount           = $request->financial_amount;
        $basicInfo->business_start_date        = $request->business_start_date_year . '-' . $request->business_start_date_month . '-' . $request->business_start_date_day;
        $basicInfo->average_month_revenue      = $request->average_month_revenue;

        $basicInfo->annual_profit              = $request->annual_profit;
        $basicInfo->business_entity            = $request->business_entity;

        $basicInfo->business_information       = $request->business_information;
        $basicInfo->annual_personal_income     = $request->annual_personal_income;
        $basicInfo->loan_purpose = implode(",", $request->loan_purpose);
        $basicInfo->save();
        $this->setSessionAfterUpdate();

        \Session::flash('flash_message', 'successfully saved.');
        return redirect()->route('finalform');
    }

    private function pppSetSessionAfterUpdate()
    {
        $arrayForUpdateData  = session('arrayForUpdateData');
        $user_id             = @$arrayForUpdateData['userID'];
        $userApplicationsId  = @$arrayForUpdateData['userApplicationsId'];
        $stepArr             = array();
        $user                = User::find($user_id);
        $basicInfo           = BasicInfo::find($userApplicationsId);

        if (!empty($basicInfo)) {
            $businessQuestions    = BusinessQuestion::where('user_id', $user_id)->where('basic_info_id', $basicInfo->id)->first();
            $businessStartDate   = explode("-", $basicInfo->business_start_date);
            $stepArr['businessinfo']['basic_info'] =  array(
                'business_address'             => @$basicInfo->business_address,
                'city'                         => @$basicInfo->city,
                'state_id'                     => @$basicInfo->state_id,
                'zip_code'                     => @$basicInfo->zip_code,
                'tax_id'                       => @$basicInfo->tax_id,
                'business_entity'              => @$basicInfo->business_entity,
                'total_employees'              => @$basicInfo->total_employees,
                'is_received_sba_ecnomic_loan' => @$basicInfo->is_received_sba_ecnomic_loan,
                'eidl_fund_received'           => @$basicInfo->eidl_fund_received,
                'eidl_loanNumber'              => @$basicInfo->eidl_loanNumber,
                'dba_name'                     => @$basicInfo->dba_name,
                'business_type'                => @$basicInfo->business_type,
                'average_monthly_payroll'      => @$basicInfo->average_monthly_payroll,
                'sbadetails'                   => @$basicInfo->sbadetails,
                'business_name'                => @$basicInfo->business_name,
                'business_start_date_year'     => @$businessStartDate[0],
                'business_start_date_month'    => @$businessStartDate[1],
                'business_start_date_day'      => @$businessStartDate[2],
                'business_start_date'          => @$basicInfo->business_start_date,
                'business_tin'                 => @$basicInfo->business_tin,
                'business_industry'            => @$basicInfo->business_industry,
                'financial_amount'             => @$basicInfo->financial_amount,
                'average_payroll_year'         => @$basicInfo->average_payroll_year,
                'sbayear'                      => @$basicInfo->sbayear,
                'loan_purpose'                 => explode(",", $basicInfo->loan_purpose)
            );
            if (!empty($businessQuestions)) {
                $stepArr['businessinfo']['business_questions'] = array(
                    'first_question'  => $businessQuestions->first_question,
                    'second_question' => $businessQuestions->second_question,
                    'third_question'  => $businessQuestions->third_question,
                    'fourth_question' => $businessQuestions->fourth_question,
                    'fifth_question'  => $businessQuestions->fifth_question,
                    'sixth_question'  => $businessQuestions->sixth_question,
                    'seven_question'  => $businessQuestions->seven_question,
                    'eight_question'  => $businessQuestions->eight_question
                );
            }
            session(['pppstepsArray' => $stepArr]);
        }
    }

    private function setSessionAfterUpdate()
    {
        $arrayForUpdateData  = session('arrayForUpdateData');
        $user_id             = $arrayForUpdateData['userID'];
        $userApplicationsId  = $arrayForUpdateData['userApplicationsId'];
        $stepArr             = array();
        $user                = User::find($user_id);
        $basicInfo           = BasicInfo::find($userApplicationsId);

        if (!empty($basicInfo)) {
            $bankInformations    = BankInformations::where('user_id', $user_id)->first();

            $stepArr['basicinfo']['financial_amount']  = array('financial_amount' => $basicInfo->financial_amount);
            $stepArr['basicinfo']['credit_card_score'] = array('credit_card_score' => $basicInfo->credit_card_score);
            $businessStartDate = explode("-", $basicInfo->business_start_date);
            $stepArr['basicinfo']['business_start_date'] =  array(
                'business_start_date_year'  => $businessStartDate[0],
                'business_start_date_month' => $businessStartDate[1],
                'business_start_date_day'   => $businessStartDate[2],
                'business_start_date'       => $basicInfo->business_start_date
            );
            $stepArr['basicinfo']['annul_personal_income'] = array('annul_personal_income' => $user->annul_personal_income);
            $stepArr['basicinfo']['average_month_revenue'] = array('average_month_revenue' => $basicInfo->average_month_revenue);
            $stepArr['basicinfo']['annual_profit']         = array('annual_profit' => $basicInfo->annual_profit);
            $stepArr['basicinfo']['business_entity']       = array('business_entity' => $basicInfo->business_entity);



            $stepArr['basicinfo']['loan_purpose']['loan_purpose']  = explode(",", $basicInfo->loan_purpose);

            $stepArr['basicinfo']['business_information'] = array('business_information' => $basicInfo->business_information);

            $stepArr['basicinfo']['business_percentage'] = array('business_percentage' => $basicInfo->business_percentage);
            $stepArr['basicinfo']['business_description'] = array('business_description' => explode(",", $basicInfo->business_description));

            $stepArr['basicinfo']['business_state'] = array('business_state' =>  $basicInfo->business_state);
            $stepArr['basicinfo']['whatismore_important_toyou'] = array('whatismore_important_toyou' =>  $basicInfo->whatismore_important_toyou);

            $stepArr['basicinfo']['existing_business_debt'] = array('existing_business_debt' =>  $basicInfo->existing_business_debt);
            $stepArr['basicinfo']['is_invoice_customers'] = array('is_invoice_customers' =>  $basicInfo->is_invoice_customers);
            $stepArr['basicinfo']['is_seasonal'] = array('is_seasonal' =>  $basicInfo->is_seasonal);
            $stepArr['basicinfo']['is_accept_credit_cards'] = array('is_accept_credit_cards' =>  $basicInfo->is_accept_credit_cards);
            $stepArr['basicinfo']['is_use_accounting_software'] = array('is_use_accounting_software' =>  $basicInfo->is_use_accounting_software);
            $stepArr['basicinfo']['is_business_non_profit'] = array('is_business_non_profit' =>  $basicInfo->is_business_non_profit);
            $stepArr['basicinfo']['is_business_franchise'] = array('is_business_franchise' =>  $basicInfo->is_business_franchise);

            $stepArr['ownerinfo']['load_prfile'] = array(
                'first_name' => $user->first_name, 'last_name' => $user->last_name, 'business_name' => $user->business_name, 'mobile_number' =>  $user->business_name,
                'email' => $user->email
            );

            $stepArr['ownerinfo']['owner_information'] = array(
                'full_address' => $user->full_address,
                'city'         => $user->city,
                'state_id'     => $user->state_id,
                'zip_code'     => $user->zip_code
            );

            $dateOfBirth = explode("-", $user->date_of_birth);
            $stepArr['ownerinfo']['owner_credit_verification'] = array(
                'date_of_birth_year'     => $dateOfBirth[0],
                'date_of_birth_month'    => $dateOfBirth[1],
                'date_of_birth_day'      => $dateOfBirth[2],
                'social_security_number' => $user->social_security_number,
                'date_of_birth'          => $user->date_of_birth
            );

            $stepArr['ownerinfo']['owner_credit_not_verified'] =  array(
                'tab_name'  => 'owner_info',
                'step_name' => 'owner_credit_not_verified'
            );

            $businessQuestions    = BusinessQuestion::where('user_id', $user_id)->where('basic_info_id', $basicInfo->id)->first();
            $businessStartDate   = explode("-", $basicInfo->business_start_date);

            $stepArr['businessinfo']['basic_info'] =  array(
                'business_address'             => $basicInfo->business_address,
                'city'                         => $basicInfo->city,
                'state_id'                     => $basicInfo->state_id,
                'zip_code'                     => $basicInfo->zip_code,
                'tax_id'                       => $basicInfo->tax_id,
                'business_entity'              => $basicInfo->business_entity,
                'total_employees'              => $basicInfo->total_employees,
                'is_received_sba_ecnomic_loan' => $basicInfo->is_received_sba_ecnomic_loan,
                'eidl_fund_received'           => $basicInfo->eidl_fund_received,
                'eidl_loanNumber'              => $basicInfo->eidl_loanNumber,
                'dba_name'                     => $basicInfo->dba_name,
                'business_type'                => $basicInfo->business_type,
                'average_monthly_payroll'      => $basicInfo->average_monthly_payroll,
                'sbadetails'                   => $basicInfo->sbadetails,
                'business_name'                => $basicInfo->business_name,
                'business_start_date_year'     => @$businessStartDate[0],
                'business_start_date_month'    => @$businessStartDate[1],
                'business_start_date_day'      => @$businessStartDate[2],
                'business_start_date'          => $basicInfo->business_start_date,
                'business_tin'                 => $basicInfo->business_tin,
                'business_industry'            => $basicInfo->business_industry,
                'financial_amount'             => $basicInfo->financial_amount,
                'average_payroll_year'         => $basicInfo->average_payroll_year,
                'sbayear'                      => $basicInfo->sbayear,
                'loan_purpose'                 => explode(",", $basicInfo->loan_purpose)
            );

            if (!empty($businessQuestions)) {
                $stepArr['businessinfo']['business_questions'] = array(
                    'first_question'  => $businessQuestions->first_question,
                    'second_question' => $businessQuestions->second_question,
                    'third_question'  => $businessQuestions->third_question,
                    'fourth_question' => $businessQuestions->fourth_question,
                    'fifth_question'  => $businessQuestions->fifth_question,
                    'sixth_question'  => $businessQuestions->sixth_question,
                    'seven_question'  => $businessQuestions->seven_question,
                    'eight_question'  => $businessQuestions->eight_question
                );
            }

            $stepArr['businessinfo']['sba_assistance_needed'] = array(
                'economicinnjury_disasterloan' => $bankInformations->economicinnjury_disasterloan,
                'bank_name'                    => $bankInformations->bank_name,
                'bank_routring_number'         => $bankInformations->bank_routring_number,
                'bank_account_number'          => $bankInformations->bank_account_number
            );
            $stepArr['businessinfo']['business_financials'] = array('business_ownership_type' => $basicInfo->business_ownership_type);
            $stepArr['businessinfo']['final_questions'] = array(
                'existing_business_debt' => $basicInfo->existing_business_debt,
                'annual_personal_income' => $basicInfo->annual_personal_income
            );
            session(['stepsArray' => $stepArr]);
        }
    }

    /*
  File upload
  */
    public function uploadFile(Request $request)
    {
        $documents = session('selectedDocuments');
        \Session::forget('selectedDocuments');
        $arrayForUpdateData       = session('arrayForUpdateData');
        $document                 = new Documents();
        $document->user_id        = $arrayForUpdateData['userID'];
        $document->basic_info_id  = $arrayForUpdateData['userApplicationsId'];

        if ($request->hasFile('drivers_license_front')) {
            $doc                             = $request->file('drivers_license_front')->getClientOriginalName();
            $path                            = $request->file('drivers_license_front')->storeAs('public/documents', $doc);
            $document->drivers_license_front = $path;
        }

        if ($request->hasFile('drivers_license_back')) {
            $doc                             = $request->file('drivers_license_back')->getClientOriginalName();
            $path                            = $request->file('drivers_license_back')->storeAs('public/documents', $doc);
            $document->drivers_license_back = $path;
        }
        if ($request->hasFile('voided_check')) {
            $doc                             = $request->file('voided_check')->getClientOriginalName();
            $path                            = $request->file('voided_check')->storeAs('public/documents', $doc);
            $document->voided_check = $path;
        }
        if ($request->hasFile('schedule_c')) {
            $doc                             = $request->file('schedule_c')->getClientOriginalName();
            $path                            = $request->file('schedule_c')->storeAs('public/documents', $doc);
            $document->schedule_c = $path;
        }
        if ($request->hasFile('bank_statement')) {
            $doc                             = $request->file('bank_statement')->getClientOriginalName();
            $path                            = $request->file('bank_statement')->storeAs('public/documents', $doc);
            $document->bank_statement = $path;
        }
        if ($request->hasFile('business_formation_doc')) {
            $doc                             = $request->file('business_formation_doc')->getClientOriginalName();
            $path                            = $request->file('business_formation_doc')->storeAs('public/documents', $doc);
            $document->business_formation_doc = $path;
        }
        $document->save();
        \Session::flash('flash_message', 'successfully saved.');
        return redirect()->route('finalform');
    }
    public function createPPPBusiness($basic_info)
    {
        if (Auth::check()) {
            $user_id             = Auth::user()->id;
            $user                = User::find($user_id);
            $user->business_name = $basic_info['business_name'];
            $user->save();
            $basicInfoSession = session('arrayForUpdateData');
            if (empty($basicInfoSession['otherInfo']) || $basicInfoSession['otherInfo'] == false) {
                $arrayForUpdateData                 = array();
                $basicInfo                          = new BasicInfo();
                $basicInfo->user_id                 =  $user_id;
                $basicInfo->application_type        = 'PPP';
                $basicInfo->business_address        = @$basic_info['business_address'];
                $basicInfo->city                    = @$basic_info['city'];
                $basicInfo->state_id                = @$basic_info['state_id'];
                $basicInfo->zip_code                = @$basic_info['zip_code'];
                $basicInfo->dba_name                = @$basic_info['dba_name'];
                $basicInfo->business_type           = @$basic_info['business_type'];
                $basicInfo->business_tin            = @$basic_info['business_tin'];
                $basicInfo->tax_id                  = @$basic_info['tax_id'];
                $basicInfo->business_industry       = @$basic_info['business_industry'];
                $basicInfo->total_employees         = @$basic_info['total_employees'];
                $basicInfo->sbayear                 = @$basic_info['sbayear'];
                $basicInfo->sbadetails              = @$basic_info['sbadetails'];
                $basicInfo->average_monthly_payroll = @$basic_info['average_monthly_payroll'];
                $basicInfo->financial_amount        = @$basic_info['financial_amount'];
                $basicInfo->business_start_date     = @$basic_info['business_start_date_year'] . '-' . @$basic_info['business_start_date_month'] . '-' . @$basic_info['business_start_date_day'];
                if (!empty($basic_info['loan_purpose'])) {
                    $loan_purpose = implode(",", $basic_info['loan_purpose']);
                } else {
                    $loan_purpose = '';
                }
                $basicInfo->loan_purpose = $loan_purpose;
                if ($basicInfo->save()) {
                    $arrayForUpdateData['otherInfo']          = true;
                    $arrayForUpdateData['is_updated']         = true;
                    $arrayForUpdateData['userID']             = $user_id;
                    $arrayForUpdateData['userApplicationsId'] = $basicInfo->id;
                }
                session(['arrayForUpdateData' => $arrayForUpdateData]);
            } else {
                $arrayForUpdateData                 = array();
                $basicInfo                          = BasicInfo::find($basicInfoSession['userApplicationsId']);
                $basicInfo->user_id                 = $user_id;
                $basicInfo->business_address        = @$basic_info['business_address'];
                $basicInfo->city                    = @$basic_info['city'];
                $basicInfo->state_id                = @$basic_info['state_id'];
                $basicInfo->zip_code                = @$basic_info['zip_code'];
                $basicInfo->dba_name                = @$basic_info['dba_name'];
                $basicInfo->business_type           = @$basic_info['business_type'];
                $basicInfo->business_tin            = @$basic_info['business_tin'];
                $basicInfo->tax_id                  = @$basic_info['tax_id'];
                $basicInfo->business_industry       = @$basic_info['business_industry'];
                $basicInfo->total_employees         = @$basic_info['total_employees'];
                $basicInfo->sbayear                 = @$basic_info['sbayear'];
                $basicInfo->sbadetails              = @$basic_info['sbadetails'];
                $basicInfo->average_monthly_payroll = @$basic_info['average_monthly_payroll'];
                $basicInfo->financial_amount        = @$basic_info['financial_amount'];
                $basicInfo->business_start_date     = @$basic_info['business_start_date_year'] . '-' . @$basic_info['business_start_date_month'] . '-' . @$basic_info['business_start_date_day'];
                if (!empty($basic_info['loan_purpose'])) {
                    $loan_purpose = implode(",", @$basic_info['loan_purpose']);
                } else {
                    $loan_purpose = '';
                }
                $basicInfo->loan_purpose = $loan_purpose;
                $basicInfo->save();
            }
        }
    }

    public function createPPPLoan()
    {
        $stepsArray              = session('pppstepsArray');
        $basic_info              = $stepsArray['businessinfo']['basic_info'];
        $business_questions      = $stepsArray['businessinfo']['business_questions'];
        if (Auth::check()) {
            $user_id             = Auth::user()->id;
            $user                = User::find($user_id);
            $user->business_name = $basic_info['business_name'];
            $user->save();
            $basicInfoSession = session('arrayForUpdateData');
            if (empty($basicInfoSession['otherInfo']) || $basicInfoSession['otherInfo'] == false) {
                $arrayForUpdateData                 = array();
                $basicInfo                          = new BasicInfo();
                $basicInfo->user_id                 =  $user_id;
                $basicInfo->application_type        = 'PPP';
                $basicInfo->business_address        = @$basic_info['business_address'];
                $basicInfo->city                    = @$basic_info['city'];
                $basicInfo->state_id                = @$basic_info['state_id'];
                $basicInfo->zip_code                = @$basic_info['zip_code'];
                $basicInfo->dba_name                = @$basic_info['dba_name'];
                $basicInfo->business_type           = @$basic_info['business_type'];
                $basicInfo->business_tin            = @$basic_info['business_tin'];
                $basicInfo->tax_id                  = @$basic_info['tax_id'];
                $basicInfo->business_industry       = @$basic_info['business_industry'];
                $basicInfo->total_employees         = @$basic_info['total_employees'];
                $basicInfo->sbayear                 = @$basic_info['sbayear'];
                $basicInfo->sbadetails              = @$basic_info['sbadetails'];
                $basicInfo->average_monthly_payroll = @$basic_info['average_monthly_payroll'];
                $basicInfo->financial_amount        = @$basic_info['financial_amount'];
                $basicInfo->business_start_date     = @$basic_info['business_start_date_year'] . '-' . @$basic_info['business_start_date_month'] . '-' . @$basic_info['business_start_date_day'];
                if (!empty($basic_info['loan_purpose'])) {
                    $loan_purpose = implode(",", $basic_info['loan_purpose']);
                } else {
                    $loan_purpose = '';
                }
                $basicInfo->loan_purpose = $loan_purpose;
                if ($basicInfo->save()) {
                    $arrayForUpdateData['is_updated']         = true;
                    $arrayForUpdateData['userID']             = $user->id;
                    $arrayForUpdateData['otherInfo']          = true;
                    $arrayForUpdateData['userApplicationsId'] = $basicInfo->id;
                    $businessQuestion                         = new BusinessQuestion();
                    $businessQuestion->user_id                = $user_id;
                    $businessQuestion->basic_info_id          = $basicInfo->id;
                    $businessQuestion->first_question         = (!empty($business_questions['first_question'])) ? $business_questions['first_question'] : 'No';
                    $businessQuestion->second_question        = (!empty($business_questions['second_question'])) ? $business_questions['second_question'] : 'No';
                    $businessQuestion->third_question         = (!empty($business_questions['third_question'])) ? $business_questions['third_question'] : 'No';
                    $businessQuestion->fourth_question        = (!empty($business_questions['fourth_question'])) ? $business_questions['fourth_question'] : 'No';
                    $businessQuestion->fifth_question         = (!empty($business_questions['fifth_question'])) ? $business_questions['fifth_question'] : 'No';
                    $businessQuestion->sixth_question         = (!empty($business_questions['sixth_question'])) ? $business_questions['sixth_question'] : 'No';
                    $businessQuestion->seven_question         = (!empty($business_questions['seven_question'])) ? $business_questions['seven_question'] : 'No';
                    $businessQuestion->eight_question         = (!empty($business_questions['eight_question'])) ? $business_questions['eight_question'] : 'No';
                    $businessQuestion->save();
                } else {
                    $arrayForUpdateData['otherInfo']          = false;
                    $arrayForUpdateData['is_updated']         = false;
                    $arrayForUpdateData['userID']             = null;
                    $arrayForUpdateData['userApplicationsId'] = null;
                }
                session(['arrayForUpdateData' => $arrayForUpdateData]);
                \Session::flash('flash_message', 'successfully saved.');
            } else {
                $arrayForUpdateData                 = array();
                $basicInfo                          =  BasicInfo::find($basicInfoSession['userApplicationsId']);
                $basicInfo->user_id                 =  $user_id;
                $basicInfo->business_address        = @$basic_info['business_address'];
                $basicInfo->city                    = @$basic_info['city'];
                $basicInfo->state_id                = @$basic_info['state_id'];
                $basicInfo->zip_code                = @$basic_info['zip_code'];
                $basicInfo->dba_name                = @$basic_info['dba_name'];
                $basicInfo->business_type           = @$basic_info['business_type'];
                $basicInfo->business_tin            = @$basic_info['business_tin'];
                $basicInfo->tax_id                  = @$basic_info['tax_id'];
                $basicInfo->business_industry       = @$basic_info['business_industry'];
                $basicInfo->total_employees         = @$basic_info['total_employees'];
                $basicInfo->sbayear                 = @$basic_info['sbayear'];
                $basicInfo->sbadetails              = @$basic_info['sbadetails'];
                $basicInfo->average_monthly_payroll = @$basic_info['average_monthly_payroll'];
                $basicInfo->financial_amount        = @$basic_info['financial_amount'];
                $basicInfo->business_start_date     = @$basic_info['business_start_date_year'] . '-' . @$basic_info['business_start_date_month'] . '-' . @$basic_info['business_start_date_day'];
                if (!empty($basic_info['loan_purpose'])) {
                    $loan_purpose = implode(",", @$basic_info['loan_purpose']);
                } else {
                    $loan_purpose = '';
                }
                $basicInfo->loan_purpose = $loan_purpose;
                $basicInfo->save();
                $businessQuestion                         = BusinessQuestion::where('user_id', $user_id)->where('basic_info_id', $basicInfo->id)->first();
                if (empty($businessQuestion)) {
                    $businessQuestion                     = new BusinessQuestion();
                }
                $businessQuestion->user_id                = $user_id;
                $businessQuestion->basic_info_id          = $basicInfo->id;
                $businessQuestion->first_question         = (!empty($business_questions['first_question'])) ? $business_questions['first_question'] : 'No';
                $businessQuestion->second_question        = (!empty($business_questions['second_question'])) ? $business_questions['second_question'] : 'No';
                $businessQuestion->third_question         = (!empty($business_questions['third_question'])) ? $business_questions['third_question'] : 'No';
                $businessQuestion->fourth_question        = (!empty($business_questions['fourth_question'])) ? $business_questions['fourth_question'] : 'No';
                $businessQuestion->fifth_question         = (!empty($business_questions['fifth_question'])) ? $business_questions['fifth_question'] : 'No';
                $businessQuestion->sixth_question         = (!empty($business_questions['sixth_question'])) ? $business_questions['sixth_question'] : 'No';
                $businessQuestion->seven_question         = (!empty($business_questions['seven_question'])) ? $business_questions['seven_question'] : 'No';
                $businessQuestion->eight_question         = (!empty($business_questions['eight_question'])) ? $business_questions['eight_question'] : 'No';
                $businessQuestion->save();
                \Session::flash('flash_message', 'successfully saved.');
            }
            return redirect()->route('ppp.ownerinfo');
        }
    }

    public function previousStep(Request $request)
    {
        $postFields = $request->all();
        $stepsArrayMaintainer = session('stepsArrayMaintainer');
        return response()->json($stepsArrayMaintainer[$postFields['step_name']]);
    }

    public function pppPreviousStep(Request $request)
    {
        $postFields           = $request->all();
        $stepsArrayMaintainer = session('pppstepsArrayMaintainer');
        return response()->json($stepsArrayMaintainer[$postFields['step_name']]);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $errors                = false;
        $errorsMsg             = '';
        $postFields            = $request->all();
        $stepsArray            = session('stepsArray');
        $stepsArrayMaintainer  = session('stepsArrayMaintainer');
        switch ($postFields['step_name']) {
            case 'financial_amount':
                $rules     = ['financial_amount' => 'required'];
                $validator = $this->validator($request->all(), $rules);
                if ($validator->fails()) {
                    $errors      = true;
                    $errorsMsg = $validator->errors();
                }
                $stepsArray['basicinfo'][$postFields['step_name']] = $postFields;
                $returnData = array(
                    'nextStep'  => 'credit_card_score',
                    'preStep'   => 'financial_amount',
                    'errors'    => $errors,
                    'errorsMsg' => $errorsMsg
                );
                $stepsArrayMaintainer[$postFields['step_name']] = $returnData;

                break;

            case 'credit_card_score':
                $rules     = ['credit_card_score' => 'required'];
                $validator = $this->validator($request->all(), $rules);
                if ($validator->fails()) {
                    $errors      = true;
                    $errorsMsg = $validator->errors();
                }
                $stepsArray['basicinfo'][$postFields['step_name']] = $postFields;
                $returnData = array(
                    'nextStep' => 'business_start_date',
                    'preStep'  => 'credit_card_score',
                    'errors'   => $errors,
                    'errorsMsg' => $errorsMsg
                );
                $stepsArrayMaintainer[$postFields['step_name']] = $returnData;

                break;
            case 'business_start_date':

                $rules     = [
                    'business_start_date_month' => 'required',
                    'business_start_date_day'   => 'required',
                    'business_start_date_year'  => 'required'
                ];
                $validator = $this->validator($request->all(), $rules);
                if ($validator->fails()) {
                    $errors      = true;
                    $errorsMsg = $validator->errors();
                } else {
                    $fullDate =  $postFields["business_start_date_year"] . '-' . $postFields["business_start_date_month"] . '-' . $postFields["business_start_date_day"];
                    $postFields["business_start_date"] = $fullDate;
                    if (strtotime($fullDate) > strtotime(date("Y-m-d"))) {
                        $errors      = true;
                        $errorsMsg   =  'date_greater';
                    }
                }
                $stepsArray['basicinfo'][$postFields['step_name']] = $postFields;
                $returnData = array(
                    'nextStep' => 'average_month_revenue',
                    'preStep'  => 'business_start_date',
                    'errors'   => $errors,
                    'errorsMsg' => $errorsMsg
                );
                $stepsArrayMaintainer[$postFields['step_name']] = $returnData;
                break;
            case 'average_month_revenue':
                $rules     = ['average_month_revenue' => 'required'];
                $validator = $this->validator($request->all(), $rules);
                if ($validator->fails()) {
                    $errors      = true;
                    $errorsMsg = $validator->errors();
                }
                $stepsArray['basicinfo'][$postFields['step_name']] = $postFields;
                $returnData = array(
                    'nextStep' => 'loan_purpose',
                    'preStep'  => 'average_month_revenue',
                    'errors'   => $errors,
                    'errorsMsg' => $errorsMsg
                );
                $stepsArrayMaintainer[$postFields['step_name']] = $returnData;
                break;
            case 'loan_purpose':
                $rules     = ['loan_purpose' => 'required'];
                $validator = $this->validator($request->all(), $rules);
                if ($validator->fails()) {
                    $errors      = true;
                    $errorsMsg = $validator->errors();
                }
                $stepsArray['basicinfo'][$postFields['step_name']] = $postFields;
                $returnData = array(
                    'nextStep' => 'business_information',
                    'preStep'  => 'loan_purpose',
                    'errors'   => $errors,
                    'errorsMsg' => $errorsMsg
                );
                $stepsArrayMaintainer[$postFields['step_name']] = $returnData;
                break;

            case 'business_information':
                $rules     = ['business_information' => 'required'];
                $validator = $this->validator($request->all(), $rules);
                if ($validator->fails()) {
                    $errors      = true;
                    $errorsMsg = $validator->errors();
                }
                $stepsArray['basicinfo'][$postFields['step_name']] = $postFields;
                $returnData = array(
                    'nextStep' => 'business_percentage',
                    'preStep'  => 'business_information',
                    'errors'   => $errors,
                    'errorsMsg' => $errorsMsg
                );
                $stepsArrayMaintainer[$postFields['step_name']] = $returnData;
                break;

            case 'business_percentage':
                $rules     = ['business_percentage' => 'required'];
                $validator = $this->validator($request->all(), $rules);
                if ($validator->fails()) {
                    $errors      = true;
                    $errorsMsg = $validator->errors();
                }
                $stepsArray['basicinfo'][$postFields['step_name']] = $postFields;
                $returnData = array(
                    'nextStep' => 'annul_personal_income',
                    'preStep'  => 'business_percentage',
                    'errors'   => $errors,
                    'errorsMsg' => $errorsMsg
                );
                $stepsArrayMaintainer[$postFields['step_name']] = $returnData;
                break;

            case 'annul_personal_income':
                $rules     = ['annul_personal_income' => 'required'];
                $validator = $this->validator($request->all(), $rules);
                if ($validator->fails()) {
                    $errors      = true;
                    $errorsMsg = $validator->errors();
                }
                $stepsArray['basicinfo'][$postFields['step_name']] = $postFields;
                $returnData = array(
                    'nextStep' => 'annual_profit',
                    'preStep'  => 'annul_personal_income',
                    'errors'   => $errors,
                    'errorsMsg' => $errorsMsg
                );
                $stepsArrayMaintainer[$postFields['step_name']] = $returnData;
                break;

            case 'annual_profit':
                $rules     = ['annual_profit' => 'required'];
                $validator = $this->validator($request->all(), $rules);
                if ($validator->fails()) {
                    $errors      = true;
                    $errorsMsg = $validator->errors();
                }
                $stepsArray['basicinfo'][$postFields['step_name']] = $postFields;
                $returnData = array(
                    'nextStep'  => 'business_entity',
                    'preStep'   => 'annual_profit',
                    'errors'    => $errors,
                    'errorsMsg' => $errorsMsg
                );
                $stepsArrayMaintainer[$postFields['step_name']] = $returnData;
                break;

            case 'business_entity':
                $rules     = ['business_entity' => 'required'];
                $validator = $this->validator($request->all(), $rules);
                if ($validator->fails()) {
                    $errors      = true;
                    $errorsMsg = $validator->errors();
                }
                $stepsArray['basicinfo'][$postFields['step_name']] = $postFields;
                $returnData = array(
                    'nextStep' => 'business_description',
                    'preStep'  => 'business_entity',
                    'errors'   => $errors,
                    'errorsMsg' => $errorsMsg
                );
                $stepsArrayMaintainer[$postFields['step_name']] = $returnData;
                break;


            case 'business_description':

                $rules     = ['business_description' => 'required'];
                $validator = $this->validator($request->all(), $rules);
                if ($validator->fails()) {
                    $errors      = true;
                    $errorsMsg = $validator->errors();
                }

                $stepsArray['basicinfo'][$postFields['step_name']] = $postFields;
                $returnData = array(
                    'nextStep' => 'business_state',
                    'preStep'  => 'business_description',
                    'errors'   => $errors,
                    'errorsMsg' => $errorsMsg
                );
                $stepsArrayMaintainer[$postFields['step_name']] = $returnData;
                break;

            case 'bankruptcy':
                $rules     = ['bankruptcy' => 'required'];
                $validator = $this->validator($request->all(), $rules);
                if ($validator->fails()) {
                    $errors      = true;
                    $errorsMsg = $validator->errors();
                }
                $stepsArray['basicinfo'][$postFields['step_name']] = $postFields;
                $returnData = array(
                    'nextStep' => 'bankruptcy_charge_date',
                    'preStep'  => 'bankruptcy',
                    'errors'   => $errors,
                    'errorsMsg' => $errorsMsg
                );
                $stepsArrayMaintainer[$postFields['step_name']] = $returnData;
                break;
            case 'bankruptcy_charge_date':
                $rules     = ['bankruptcy_charge_date' => 'required'];
                $validator = $this->validator($request->all(), $rules);
                if ($validator->fails()) {
                    $errors      = true;
                    $errorsMsg = $validator->errors();
                }
                $stepsArray['basicinfo'][$postFields['step_name']] = $postFields;
                $returnData = array(
                    'nextStep' => 'business_state',
                    'preStep'  => 'bankruptcy_charge_date',
                    'errors'   => $errors,
                    'errorsMsg' => $errorsMsg
                );
                $stepsArrayMaintainer[$postFields['step_name']] = $returnData;
                break;
            case 'business_state':
                $rules     = ['business_state' => 'required'];
                $validator = $this->validator($request->all(), $rules);
                if ($validator->fails()) {
                    $errors      = true;
                    $errorsMsg = $validator->errors();
                }

                $stepsArray['basicinfo'][$postFields['step_name']] = $postFields;
                $returnData = array(
                    'nextStep' => 'whatismore_important_toyou',
                    'preStep' => 'business_state',
                    'errors'   => $errors,
                    'errorsMsg' => $errorsMsg
                );
                $stepsArrayMaintainer[$postFields['step_name']] = $returnData;
                break;

            case 'whatismore_important_toyou':
                $rules     = ['whatismore_important_toyou' => 'required'];
                $nextTabURL  = route('ownerinfo');
                $validator = $this->validator($request->all(), $rules);
                if ($validator->fails()) {
                    $errors      = true;
                    $errorsMsg = $validator->errors();
                    $nextTabURL = '';
                }
                $stepsArray['basicinfo'][$postFields['step_name']] = $postFields;
                $returnData = array(
                    'nextStep' => '',
                    'preStep' => 'whatismore_important_toyou',
                    'nextTabURL' => $nextTabURL,
                    'errors'   => $errors,
                    'errorsMsg' => $errorsMsg
                );
                $stepsArrayMaintainer[$postFields['step_name']] = $returnData;
                break;

            case 'load_prfile':
                $rules     = [
                    'first_name'    => 'required',
                    'business_name' => 'required',
                    'mobile_number' => 'required',
                    'email'         => 'required|email|unique:users',
                    'password'      => 'required|confirmed|min:6',
                ];

                $validator = $this->validator($request->all(), $rules);
                if ($validator->fails()) {
                    $errors      = true;
                    $errorsMsg = $validator->errors();
                }

                $stepsArray['ownerinfo'][$postFields['step_name']] = $postFields;
                $returnData = array(
                    'nextStep' => 'owner_information',
                    'preStep'  => 'load_prfile',
                    'errors'   => $errors,
                    'errorsMsg' => $errorsMsg
                );
                $stepsArrayMaintainer[$postFields['step_name']] = $returnData;
                break;
            case 'owner_information':

                $rules     = [
                    'full_address' => 'required',
                    'city'         => 'required',
                    'state_id'     => 'required',
                    'zip_code'     => 'required',
                ];

                $validator = $this->validator($request->all(), $rules);
                if ($validator->fails()) {
                    $errors      = true;
                    $errorsMsg = $validator->errors();
                }

                $stepsArray['ownerinfo'][$postFields['step_name']] = $postFields;
                $returnData = array(
                    'nextStep' => 'owner_credit_verification',
                    'preStep'  => 'owner_information',
                    'errors'   => $errors,
                    'errorsMsg' => $errorsMsg
                );
                break;
                $stepsArrayMaintainer[$postFields['step_name']] = $returnData;
            case 'owner_credit_verification':
                $rules     = [
                    'date_of_birth_month'    => 'required',
                    'date_of_birth_day'      => 'required',
                    'date_of_birth_year'     => 'required',
                    'social_security_number' => 'required|min:9'
                ];
                $validator = $this->validator($request->all(), $rules);
                if ($validator->fails()) {
                    $errors      = true;
                    $errorsMsg = $validator->errors();
                }
                $dateOfBirth = $postFields['date_of_birth_year'] . '-' . $postFields['date_of_birth_month'] . '-' . $postFields['date_of_birth_day'];
                $postFields['date_of_birth'] =  $dateOfBirth;
                $stepsArray['ownerinfo'][$postFields['step_name']] = $postFields;
                $returnData = array(
                    'nextStep' => 'owner_credit_not_verified',
                    'preStep'  => 'owner_credit_verification',
                    'errors'   => $errors,
                    'errorsMsg' => $errorsMsg
                );
                $stepsArrayMaintainer[$postFields['step_name']] = $returnData;
                break;

            case 'owner_credit_not_verified':
                $stepsArray['ownerinfo'][$postFields['step_name']] = $postFields;
                $returnData = array(
                    'nextStep'   => 'basic_info',
                    'preStep'    => 'owner_credit_not_verified',
                    'errors'     => $errors,
                    'errorsMsg'  => $errorsMsg,
                    'nextTabURL' => route('businessinfo')
                );
                $stepsArrayMaintainer[$postFields['step_name']] = $returnData;
                break;
            case 'basic_info':
                $rules     = [
                    'business_address' => 'required',
                    'city'             => 'required',
                    'state_id'         => 'required',
                    'zip_code'         => 'required',
                    'business_entity'  => 'required'
                ];
                if (!empty($request->is_received_sba_ecnomic_loan) && $request->is_received_sba_ecnomic_loan == 'Yes') {
                    $rules['eidl_fund_received'] = 'required';
                }
                $validator = $this->validator($request->all(), $rules);
                if ($validator->fails()) {
                    $errors      = true;
                    $errorsMsg = $validator->errors();
                }
                $stepsArray['businessinfo'][$postFields['step_name']] = $postFields;
                $returnData = array(
                    'nextStep' => 'sba_assistance_needed',
                    'preStep'  => 'basic_info',
                    'errors'   => $errors,
                    'errorsMsg' => $errorsMsg
                );
                $stepsArrayMaintainer[$postFields['step_name']] = $returnData;
                break;

            case 'sba_assistance_needed':
                $rules     = [
                    'economicinnjury_disasterloan' => 'required',
                    'bank_name' => 'required',
                    'bank_routring_number' => 'required',
                    'bank_account_number' => 'required'
                ];
                $validator = $this->validator($request->all(), $rules);
                if ($validator->fails()) {
                    $errors      = true;
                    $errorsMsg = $validator->errors();
                }
                $stepsArray['businessinfo'][$postFields['step_name']] = $postFields;
                $returnData = array(
                    'nextStep' => 'business_financials',
                    'preStep'  => 'sba_assistance_needed',
                    'errors'   => $errors,
                    'errorsMsg' => $errorsMsg
                );
                $stepsArrayMaintainer[$postFields['step_name']] = $returnData;
                break;

            case 'business_financials':
                $rules     = ['business_ownership_type' => 'required'];
                $validator = $this->validator($request->all(), $rules);
                if ($validator->fails()) {
                    $errors      = true;
                    $errorsMsg = $validator->errors();
                }
                $stepsArray['businessinfo'][$postFields['step_name']] = $postFields;
                $returnData = array(
                    'nextStep' => 'final_questions',
                    'preStep'  => 'business_financials',
                    'errors'   => $errors,
                    'errorsMsg' => $errorsMsg
                );
                $stepsArrayMaintainer[$postFields['step_name']] = $returnData;
                break;
            case 'final_questions':
                $rules     = ["annual_personal_income" => 'required'];
                $validator = $this->validator($request->all(), $rules);
                if ($validator->fails()) {
                    $errors      = true;
                    $errorsMsg = $validator->errors();
                }
                $stepsArray['businessinfo'][$postFields['step_name']] = $postFields;

                $returnData = array(
                    'nextStep' => '',
                    'preStep'  => '',
                    'nextTabURL' => route('businessinfo'),
                    'errors'   => $errors,
                    'errorsMsg' => $errorsMsg,
                    'nextTabURL' => route('documents')
                );
                $stepsArrayMaintainer[$postFields['step_name']] = $returnData;
                break;
        }
        session(['stepsArray' =>  $stepsArray, 'stepsArrayMaintainer' => $stepsArrayMaintainer]);
        //dd(session('stepsArray'));
        return response()->json($returnData);
    }



    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BasicInfo  $basicInfo
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        \Session::flash('flash_message', 'successfully saved.');
        return redirect()->back();
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function pppStore(Request $request)
    {
        $errors                = false;
        $errorsMsg             = '';
        $postFields            = $request->all();
        $stepsArray            = session('pppstepsArray');
        $stepsArrayMaintainer  = session('pppstepsArrayMaintainer');
        switch ($postFields['step_name']) {
            case 'ownership_changed':
                $stepsArray['ownerinfo'][$postFields['step_name']] = $postFields;
                if (Auth::check()) {
                    $user_id          = Auth::user()->id;
                    $basicInfoSession = session('arrayForUpdateData');
                    if (empty($basicInfoSession) && $basicInfoSession['otherInfo'] == false) {
                        $arrayForUpdateData                 = array();
                        $basicInfo                          = new BasicInfo();
                        $basicInfo->user_id                 =  $user_id;
                        $basicInfo->application_type        = 'PPP';
                        $basicInfo->ownership_changed       = $postFields['ownership_changed'];
                        if ($basicInfo->save()) {
                            $arrayForUpdateData['is_updated']         = true;
                            $arrayForUpdateData['userID']             = $user_id;
                            $arrayForUpdateData['otherInfo']          = true;
                            $arrayForUpdateData['userApplicationsId'] = $basicInfo->id;
                            session(['arrayForUpdateData' => $arrayForUpdateData]);
                        }
                    } else {
                        $arrayForUpdateData                           = array();
                        $basicInfo                                    =  BasicInfo::find($basicInfoSession['userApplicationsId']);
                        $basicInfo->user_id                           =  $user_id;
                        $basicInfo->ownership_changed                 = $postFields['ownership_changed'];
                        $basicInfo->save();
                    }
                }
                $returnData = array(
                    'nextStep'  => 'business_percentage',
                    'preStep'   => 'ownership_changed',
                    'errors'    => $errors,
                    'errorsMsg' => $errorsMsg
                );
                $stepsArrayMaintainer[$postFields['step_name']] = $returnData;
                break;
            case 'business_percentage':
                $stepsArray['ownerinfo'][$postFields['step_name']] = $postFields;
                if (Auth::check()) {
                    $user_id          = Auth::user()->id;
                    $basicInfoSession = session('arrayForUpdateData');
                    if (empty($basicInfoSession) && $basicInfoSession['otherInfo'] == false) {
                        $arrayForUpdateData                 = array();
                        $basicInfo                          = new BasicInfo();
                        $basicInfo->user_id                 =  $user_id;
                        $basicInfo->application_type        = 'PPP';
                        $basicInfo->business_percentage     = @$postFields['business_percentage'];
                        $basicInfo->ownership_has_twentypercent_share       = @$postFields['ownership_has_twentypercent_share'];
                        if ($basicInfo->save()) {
                            $arrayForUpdateData['is_updated']         = true;
                            $arrayForUpdateData['userID']             = $user_id;
                            $arrayForUpdateData['otherInfo']          = true;
                            $arrayForUpdateData['userApplicationsId'] = $basicInfo->id;
                            session(['arrayForUpdateData' => $arrayForUpdateData]);
                        }
                    } else {
                        $arrayForUpdateData                           = array();
                        $basicInfo                                    =  BasicInfo::find($basicInfoSession['userApplicationsId']);
                        $basicInfo->user_id                           =  $user_id;
                        $basicInfo->business_percentage               = @$postFields['business_percentage'];
                        $basicInfo->ownership_has_twentypercent_share = @$postFields['ownership_has_twentypercent_share'];
                        $basicInfo->save();
                        $this->pppSetSessionAfterUpdate();
                    }
                }
                $returnData = array(
                    'nextStep'   => '',
                    'preStep'    => 'ownership_changed',
                    'errors'     => $errors,
                    'errorsMsg'  => $errorsMsg,
                    'nextTabURL' => route('ppp.documents')
                );
                $stepsArrayMaintainer[$postFields['step_name']] = $returnData;
                break;

            case 'basic_info':
                $rules     = [
                    'business_name'             => 'required',
                    'business_address'          => 'required',
                    'city'                      => 'required',
                    'state_id'                  => 'required',
                    'zip_code'                  => 'required',
                    'dba_name'                  => 'required',
                    'business_start_date_month' => 'required',
                    'business_start_date_day'   => 'required',
                    'business_start_date_year'  => 'required',
                    'business_type'             => 'required',
                    'total_employees'           => 'required',
                    'average_monthly_payroll'   => 'required',
                    'financial_amount'          => 'required'
                ];

                $validator = $this->validator($request->all(), $rules);
                if ($validator->fails()) {
                    $errors      = true;
                    $errorsMsg = $validator->errors();
                }
                $stepsArray['businessinfo'][$postFields['step_name']] = $postFields;
                $returnData = array(
                    'nextStep'  => 'business_questions',
                    'preStep'   => 'basic_info',
                    'errors'    => $errors,
                    'errorsMsg' => $errorsMsg
                );
                $stepsArrayMaintainer[$postFields['step_name']] = $returnData;
                if ($errors == false) {
                    $this->createPPPBusiness($postFields);
                }
                break;

            case 'revenue_reduction':
                $stepsArray['businessinfo'][$postFields['step_name']] = $postFields;
                $nearray      = array();
                foreach ($postFields as $keys => $values) {
                    if ($keys == 'tab_name' || $keys == 'step_name' || $keys == 'agree_to_terms') {
                        continue;
                    }
                    foreach ($values as $year => $row) {
                        switch ($keys) {
                            case 'g_first_quarter':
                                $nearray['g_' . $year]['first_quarter'] =  $row;
                                $nearray['g_' . $year]['type'] = 'gross_receipts';
                                $nearray['g_' . $year]['year'] = $year;
                                break;
                            case 'g_second_quarter':
                                $nearray['g_' . $year]['second_quarter'] =  $row;
                                $nearray['g_' . $year]['type'] = 'gross_receipts';
                                $nearray['g_' . $year]['year'] = $year;
                                break;
                            case 'g_third_quarter':
                                $nearray['g_' . $year]['third_quarter'] =  $row;
                                $nearray['g_' . $year]['type'] = 'gross_receipts';
                                $nearray['g_' . $year]['year'] = $year;
                                break;
                            case 'g_fourth_quarter':
                                $nearray['g_' . $year]['fourth_quarter'] =  $row;
                                $nearray['g_' . $year]['type'] = 'gross_receipts';
                                $nearray['g_' . $year]['year'] = $year;
                                break;
                            case 'e_first_quarter':
                                $nearray['expenses_' . $year]['first_quarter'] =  $row;
                                $nearray['expenses_' . $year]['type'] = 'expenses';
                                $nearray['expenses_' . $year]['year'] = $year;
                                break;
                            case 'e_second_quarter':
                                $nearray['expenses_' . $year]['second_quarter'] =  $row;
                                $nearray['expenses_' . $year]['type'] = 'expenses';
                                $nearray['expenses_' . $year]['year'] = $year;
                                break;
                            case 'e_third_quarter':
                                $nearray['expenses_' . $year]['third_quarter'] =  $row;
                                $nearray['expenses_' . $year]['type'] = 'expenses';
                                $nearray['expenses_' . $year]['year'] = $year;
                                break;
                            case 'e_fourth_quarter':
                                $nearray['expenses_' . $year]['fourth_quarter'] =  $row;
                                $nearray['expenses_' . $year]['type'] = 'expenses';
                                $nearray['expenses_' . $year]['year'] = $year;
                                break;
                        }
                    }
                }
                if (!empty($nearray)) {
                    $arrayForUpdateData                       = session('arrayForUpdateData');
                    $user_id            = (!empty($arrayForUpdateData['userID'])) ? $arrayForUpdateData['userID'] : 7;
                    $userApplicationsId = (!empty($arrayForUpdateData['userApplicationsId'])) ? $arrayForUpdateData['userID'] : '';
                    foreach ($nearray as $key => $val) {
                        $revenue_reductions = RevenueReduction::where('user_id', $user_id)
                            ->where('basic_info_id', $userApplicationsId)
                            ->where('type', $val['type'])->where('year', $val['year'])->first();
                        if (empty($revenue_reductions)) {
                            $revenue_reductions = new RevenueReduction();
                        }
                        $revenue_reductions->first_quarter  = $val['first_quarter'];
                        $revenue_reductions->second_quarter = $val['second_quarter'];
                        $revenue_reductions->third_quarter  = $val['third_quarter'];
                        $revenue_reductions->fourth_quarter = $val['fourth_quarter'];
                        $revenue_reductions->type           = $val['type'];
                        $revenue_reductions->year           = $val['year'];
                        $revenue_reductions->user_id        = $user_id;
                        $revenue_reductions->basic_info_id  = $userApplicationsId;
                        //$revenue_reductions->save();
                    }
                }
                $returnData = array(
                    'nextStep'  => 'business_questions',
                    'preStep'   => 'revenue_reduction',
                    'errors'    => $errors,
                    'errorsMsg' => $errorsMsg
                );
                $stepsArrayMaintainer[$postFields['step_name']] = $returnData;
                break;

            case 'business_questions':
                $stepsArray['businessinfo'][$postFields['step_name']] = $postFields;
                $returnData = array(
                    'nextStep'  => '',
                    'preStep'   => 'business_questions',
                    'errors'    => $errors,
                    'errorsMsg' => $errorsMsg,
                    'nextTabURL' => route('ppp.createppploan'),
                );
                $stepsArrayMaintainer[$postFields['step_name']] = $returnData;
                break;
        }
        session(['pppstepsArray' =>  $stepsArray, 'pppstepsArrayMaintainer' => $stepsArrayMaintainer]);
        return response()->json($returnData);
    }

    public function deleteDocument($id, $field)
    {

        $document = Documents::find($id);
        switch ($field) {
            case 'id_front':
                $document->id_front = null;
                break;
            case 'id_back':
                $document->id_back = null;
                break;
            case 'voided_check':
                $document->voided_check = null;
                break;
        }
        if ($document->save()) {
            $message   = array('msg' => 'success');
        } else $message = array('msg' => 'error');
        return response()->json($message);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function pppDocumentStore(Request $request)
    {
        $errors                = false;
        $isUpdate               = false;
        $errorsMsg             = '';
        $postFields            = $request->all();
        $stepsArray            = session('pppDocumentStepsArray');
        $stepsArrayMaintainer  = session('pppstepsArrayMaintainer');
        if (!Auth::check()) {
            return redirect()->route('home');
        }
        if (!empty(session('arrayForUpdateData'))) {
            $arrayForUpdateData = session('arrayForUpdateData');
            $user_id            = $arrayForUpdateData['userID'];
            $basic_info_id      = $arrayForUpdateData['userApplicationsId'];
            $isUpdate            = true;
        }

        switch ($postFields['step_name']) {
            case 'document_intro':
                $returnData = array(
                    'nextStep'  => 'mandatory_documents',
                    'preStep'   => 'document_intro',
                    'errors'    => $errors,
                    'errorsMsg' => $errorsMsg
                );
                $stepsArrayMaintainer[$postFields['step_name']] = $returnData;
                break;

            case 'mandatory_documents':
                if ($isUpdate) {
                    $document           = Documents::where('user_id', '=', $user_id)->where('basic_info_id', $basic_info_id)->first();
                    if (empty($document)) {
                        $document       = new Documents();
                    }

                    $document->user_id        = $user_id;
                    $document->basic_info_id  = $basic_info_id;
                    if ($request->hasFile('id_front')) {
                        $doc                             = $request->file('id_front')->getClientOriginalName();
                        $path                            = $request->file('id_front')->storeAs('public/documents', $doc);
                        $document->id_front = $path;
                    }
                    if ($request->hasFile('id_back')) {
                        $doc                             = $request->file('id_back')->getClientOriginalName();
                        $path                            = $request->file('id_back')->storeAs('public/documents', $doc);
                        $document->id_back = $path;
                    }
                    if ($request->hasFile('voided_check')) {
                        $doc                             = $request->file('voided_check')->getClientOriginalName();
                        $path                            = $request->file('voided_check')->storeAs('public/documents', $doc);
                        $document->voided_check = $path;
                    }
                    $document->save();
                }
                $returnData = array(
                    'nextStep'  => 'payroll_documents',
                    'preStep'   => 'mandatory_documents',
                    'errors'    => $errors,
                    'errorsMsg' => $errorsMsg
                );
                $stepsArrayMaintainer[$postFields['step_name']] = $returnData;
                break;

            case 'business_type_details':
                $stepsArray['pppdocuments'][$postFields['step_name']] = $postFields;
                if ($isUpdate) {
                    $basicInfo                      = BasicInfo::find($basic_info_id);
                    $basicInfo->llc_member_type     = (empty($postFields['llc_member_type'])) ? 'Single' : $postFields['llc_member_type'];
                    $basicInfo->has_w_two_employees = (empty($postFields['has_w_two_employees'])) ? 'No' : $postFields['has_w_two_employees'];
                    $basicInfo->save();
                }
                $returnData = array(
                    'nextStep'  => 'payroll_documents',
                    'preStep'   => 'business_type_details',
                    'errors'    => $errors,
                    'errorsMsg' => $errorsMsg
                );

                $stepsArrayMaintainer[$postFields['step_name']] = $returnData;
                break;
                
            case 'payroll_documents':
                if ($isUpdate) {
                    $basicInfo                       = BasicInfo::find($basic_info_id);
                    $basicInfo->average_payroll_year = (empty($postFields['average_payroll_year'])) ? '2019' : $postFields['average_payroll_year'];
                    $basicInfo->save();
                }
                $document = '';
                if (!empty($postFields['id'])) {
                    $document                 = ExtraDocument::find($postFields['id']);
                }
                if (empty($document)) {
                    $document                 = new ExtraDocument();
                    $document->basic_info_id  = $basic_info_id;
                }

                if ($request->hasFile('document_name')) {
                    $doc                      = $request->file('document_name')->getClientOriginalName();
                    $path                     = $request->file('document_name')->storeAs('public/documents', $doc);
                    $document->document_name  = $path;
                }

                $document->document_type  = $postFields['document_type'];
                $document->type           = $postFields['type'];
                $document->save();

                $returnData = array(
                    'nextStep'  => 'LLC_taxes',
                    'preStep'   => 'payroll_documents',
                    'errors'    => $errors,
                    'errorsMsg' => $errorsMsg
                );
                $stepsArrayMaintainer[$postFields['step_name']] = $returnData;
                break;

            case 'LLC_taxes':

                $document = '';
                if (!empty($postFields['id'])) {
                    $document                 = ExtraDocument::find($postFields['id']);
                }
                if (empty($document)) {
                    $document                 = new ExtraDocument();
                    $document->basic_info_id  = $basic_info_id;
                }

                if ($request->hasFile('document_name')) {
                    $doc                      = $request->file('document_name')->getClientOriginalName();
                    $path                     = $request->file('document_name')->storeAs('public/documents', $doc);
                    $document->document_name  = $path;
                }

                $document->document_type  = $postFields['document_type'];
                $document->type           = $postFields['type'];
                $document->save();

                $returnData = array(
                    'nextStep'  => 'additional_documents',
                    'preStep'   => 'LLC_taxes',
                    'errors'    => $errors,
                    'errorsMsg' => $errorsMsg
                );
                $stepsArrayMaintainer[$postFields['step_name']] = $returnData;
                break;
            case 'additional_documents':
                $stepsArray['pppdocuments'][$postFields['step_name']] = $postFields['document_type'];
                $document = '';
                if (!empty($postFields['id'])) {
                    $document                 = ExtraDocument::find($postFields['id']);
                }
                if (empty($document)) {
                    $document                 = new ExtraDocument();
                    $document->basic_info_id  = $basic_info_id;
                }

                if ($request->hasFile('document_name')) {
                    $doc                      = $request->file('document_name')->getClientOriginalName();
                    $path                     = $request->file('document_name')->storeAs('public/documents', $doc);
                    $document->document_name  = $path;
                }

                $document->document_type  = $postFields['document_type'];
                $document->type           = $postFields['type'];
                $document->save();

                $returnData = array(
                    'nextStep'  => 'demographic_information',
                    'preStep'   => 'additional_documents',
                    'errors'    => $errors,
                    'errorsMsg' => $errorsMsg
                );
                $stepsArrayMaintainer[$postFields['step_name']] = $returnData;
                break;
            case 'demographic_information':
                $stepsArray['pppdocuments'][$postFields['step_name']] = $postFields;
                DemographicInformation::where('basic_info_id', $basic_info_id)->delete();
                for ($i = 0; $i < count($postFields['first_name']); $i++) {
                    $demographic_information                  = new DemographicInformation();
                    $demographic_information->basic_info_id   = $basic_info_id;
                    $demographic_information->first_name      = (!empty($postFields['first_name'][$i])) ? $postFields['first_name'][$i] : '';
                    $demographic_information->last_name       = (!empty($postFields['last_name'][$i])) ? $postFields['last_name'][$i] : '';
                    $demographic_information->position        = (!empty($postFields['position'][$i])) ? $postFields['position'][$i] : '';
                    $demographic_information->veteran_status  = (!empty($postFields['veteran_status'][$i])) ? $postFields['veteran_status'][$i] : '';
                    $demographic_information->gender          = (!empty($postFields['gender'][$i])) ? $postFields['gender'][$i] : 'Not Disclosed';
                    $demographic_information->race            = (!empty($postFields['race'][$i])) ? $postFields['race'][$i] : '';
                    $demographic_information->ethnicity       = (!empty($postFields['ethnicity'][$i])) ? $postFields['ethnicity'][$i] : '';
                    $demographic_information->save();
                }
                $returnData = array(
                    'nextStep'  => 'sign_and_consent',
                    'preStep'   => 'demographic_information',
                    'errors'    => $errors,
                    'errorsMsg' => $errorsMsg
                );
                $stepsArrayMaintainer[$postFields['step_name']] = $returnData;
                break;
            case 'sign_and_consent':
                $rules     = [
                    'first_name'       => 'required',
                    'birth_month'      => 'required',
                    'birth_day'        => 'required',
                    'birth_year'       => 'required',
                    'applicant_ssn'    => 'required',
                    'applicant_title'  => 'required',
                    'gender'           => 'required',
                    'full_address'     => 'required',
                    'city'             => 'required',
                    'zipcode'          => 'required',
                    'state'            => 'required'
                ];

                $validator = $this->validator($request->all(), $rules);
                if ($validator->fails()) {
                    $errors      = true;
                    $errorsMsg = $validator->errors();
                }
                $returnData = array(
                    'nextStep'   => '',
                    'preStep'    => 'sign_and_consent',
                    'errors'     => $errors,
                    'errorsMsg'  => $errorsMsg,
                    'nextTabURL' => route('ppp.finalform'),
                );
                if ($errors == false) {
                    $stepsArray['pppdocuments'][$postFields['step_name']] = $postFields;
                    $application_authorization                     = ApplicationAuthorization::where('basic_info_id', $basic_info_id)->first();
                    if (empty($application_authorization)) {
                        $application_authorization                 = new ApplicationAuthorization();
                        $application_authorization->basic_info_id  = $basic_info_id;
                    }
                    $application_authorization->first_name       = $postFields['first_name'];
                    $application_authorization->last_name        = $postFields['last_name'];
                    $application_authorization->date_of_birth    = $postFields['birth_year'] . '-' . $postFields['birth_month'] . '-' . $postFields['birth_day'];
                    $application_authorization->gender           = $postFields['gender'];
                    $application_authorization->applicant_ssn    = $postFields['applicant_ssn'];
                    $application_authorization->full_address     = $postFields['full_address'];
                    $application_authorization->city             = $postFields['city'];
                    $application_authorization->state            = $postFields['state'];
                    $application_authorization->zipcode          = $postFields['zipcode'];
                    $application_authorization->applicant_title  = $postFields['applicant_title'];
                    //$application_authorization->agree_to_terms   = $postFields['agree_to_terms'];
                    //$application_authorization->accept_terms     = @$postFields['accept_terms'];
                    $application_authorization->certifications   = json_encode($postFields['certifications']);
                    $application_authorization->save();
                    $stepsArrayMaintainer[$postFields['step_name']] = $returnData;
                }
                break;
            case 'e-sign':
                $rules     = [
                    'agree_to_terms' => 'required',
                    'accept_terms'   => 'required'
                ];

                $validator = $this->validator($request->all(), $rules);
                if ($validator->fails()) {
                    $errors      = true;
                    $errorsMsg = $validator->errors();
                }
                $returnData = array(
                    'nextStep'   => '',
                    'preStep'    => 'e-sign',
                    'errors'     => $errors,
                    'errorsMsg'  => $errorsMsg,
                    'nextTabURL' => route('offers'),
                );
                if ($errors == false) {
                    $stepsArray['pppdocuments'][$postFields['step_name']] = $postFields;
                    $application_authorization                     = ApplicationAuthorization::where('basic_info_id', $basic_info_id)->first();
                    if (empty($application_authorization)) {
                        $application_authorization                 = new ApplicationAuthorization();
                        $application_authorization->basic_info_id  = $basic_info_id;
                    }
                    $application_authorization->agree_to_terms   = $postFields['agree_to_terms'];
                    $application_authorization->accept_terms     = @$postFields['accept_terms'];
                    $application_authorization->save();
                    $stepsArrayMaintainer[$postFields['step_name']] = $returnData;
                }

                break;
        }

        session(['pppstepsArray' =>  $stepsArray, 'pppstepsArrayMaintainer' => $stepsArrayMaintainer]);
        return response()->json($returnData);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BasicInfo  $basicInfo
     * @return \Illuminate\Http\Response
     */
    public function pppValidate(Request $request)
    {
        $arrayForUpdateData = array();
        if (Auth::check()) {
            $user_id = Auth::user()->id;
        }
        $has_ppload = $request->get('has_ppload');
        if ($has_ppload == 'Yes') {
            $sba_loan_number = $request->get('sba_loan_number');
            $basicInfo       = BasicInfo::where('sba_loan_number', $sba_loan_number)->where('user_id', $user_id)
                ->where('application_type', 'PPP')->where('financial_amount', $request->get('financial_amount'))->first();
            if ($basicInfo) {
                $arrayForUpdateData['otherInfo']          = true;
                $arrayForUpdateData['is_updated']         = true;
                $arrayForUpdateData['userID']             = $user_id;
                $arrayForUpdateData['userApplicationsId'] = $basicInfo->id;
                session(['arrayForUpdateData' => $arrayForUpdateData]);
                $this->pppSetSessionAfterUpdate();

                $returnData = array(
                    'nextStep'   => '',
                    'preStep'    => '',
                    'errors'     => false,
                    'errorsMsg'  => '',
                    'nextTabURL' => route('ppp.businessinfo')
                );
            } else {
                $returnData = array(
                    'nextStep'  => '',
                    'preStep'   => '',
                    'errors'    => true,
                    'errorsMsg' => 'Record Could not matched'
                );
            }
        } else {
            \Session::forget(['arrayForUpdateData', 'stepsArray', 'pppstepsArray']);
            $request->session()->forget(['arrayForUpdateData', 'stepsArray', 'pppstepsArray']);
            $returnData = array(
                'nextStep'   => '',
                'preStep'    => '',
                'errors'     => false,
                'errorsMsg'  => '',
                'nextTabURL' => route('ppp.businessinfo')
            );
        }
        return response()->json($returnData);
    }
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */

    public function validator(array $data, array $rules)
    {
        return Validator::make($data, $rules);
    }
}


/* DROP TRIGGER `basic_infos_uuid`;
DELIMITER ;;
CREATE TRIGGER `basic_infos_uuid` BEFORE INSERT ON `basic_infos` FOR EACH ROW
BEGIN
  IF new.sba_loan_number IS NULL THEN
    SET new.sba_loan_number = uuid_short();
  END IF;
END;;
DELIMITER ; */

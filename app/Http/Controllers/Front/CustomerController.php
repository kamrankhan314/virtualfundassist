<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repository\UserRepository;
use App\Models\Documents;
use App\Models\User;
use Auth;
use App\Models\BasicInfo;
use App\Models\BankInformations;
use App\Models\ExtraDocument;
use App\Models\BusinessQuestion;
use App\Models\RevenueReduction;
use App\Models\ApplicationAuthorization;
use App\Models\DemographicInformation;

class CustomerController extends Controller
{
    private $model;
    public function __construct(UserRepository $myModel)
    {
        $this->model = $myModel;
    }

    public function index()
    {
        $stepArr['basicinfo']['financial_amount']  = array('financial_amount' => financial_amount);
        $stepArr['basicinfo']['credit_card_score'] = array('credit_card_score' => credit_card_score);
        $stepArr['basicinfo']['business_start_date'] =  array('business_start_date' => 2021 - 01 - 04);

        $stepArr['basicinfo']['average_month_revenue'] = array('average_month_revenue' => '$200k+');

        $stepArr['basicinfo']['loan_purpose'] = array(
            '0' => 'Working Capital',
            '1' => 'Purchase a Franchise',
            '2' => 'Buy Out a Partner',
            '3' => 'Start a Business'
        );

        $stepArr['basicinfo']['business_information'] = array('business_information' => 'manufacturing');

        $stepArr['basicinfo']['business_percentage'] = array('business_percentage' => '78');

        $stepArr['basicinfo']['business_description'] = array('business_description' => array(
            '0' => 'Have a prior bankruptcy',
            '1' => 'Operate as a franchise',
            '2' => 'Operate as a non-profit',
            '3' => 'Do business seasonally',
        ));

        $stepArr['basicinfo']['bankruptcy'] = array('bankruptcy' => 'Open');

        $stepArr['basicinfo']['bankruptcy_charge_date'] = array('bankruptcy_charge_date' => '2021-01-04');

        $stepArr['basicinfo']['business_state'] = array('business_state' => 'IN');

        $stepArr['ownerinfo']['load_prfile'] = array(
            'first_name' => 'Asif', 'last_name' => Khan, 'business_name' => 'Asif business', 'mobile_number' => '8888888',
            'email' => 'admin@mail.com',
            'password' => '12345678',
            'confirmPassword' => '12345678'
        );

        $stepArr['ownerinfo']['owner_information'] = array(
            'full_address' => 'Addreess', 'city' => 'City',
            'state_id' => 'IL',
            'zip_code' => '123'
        );

        $stepArr['ownerinfo']['owner_credit_verification'] = array(
            'date_of_birth' => '2021-01-04',
            'social_security_number' => '11111111'
        );

        $stepArr['ownerinfo']['owner_credit_not_verified'] =  array(
            'tab_name' => 'owner_info',
            'step_name' => 'owner_credit_not_verified'
        );

        $stepArr['businessinfo']['basic_info'] =  array(
            'business_address'  => 'Business Address',
            'city'            => 'City',
            'state_id'        => 'MD',
            'zip_code'        => '123',
            'tax_id'          => '12345',
            'dba_name'        => 'DBA Name',
            'total_employees' => '56'
        );

        $stepArr['businessinfo']['business_financials'] = array('business_ownership_type' => 'lease');

        $stepArr['businessinfo']['final_questions'] = array('annul_personal_income' => '78');
    }
    public function basicinfo()
    {
        $stepsArray         = session('stepsArray');
        //session()->flush();
        return view('basicinfo');
    }

    public function pppBasicInfo()
    {
        $stepsArray         = session('stepsArray');
        //session()->flush();
        return view('ppp.basicinfo');
    }

    public function ownerinfo()
    {
        return view('owner_info');
    }

    public function pppOwnerInfo()
    {
        $stepsArray         = session('pppstepsArray');
        $ownerinfo = array();
        if (!empty($stepsArray['ownerinfo'])){
            $ownerinfo =  $stepsArray['ownerinfo'];
        }
        $arrayForUpdateData = session('arrayForUpdateData');
        if(!empty($arrayForUpdateData) && $arrayForUpdateData['otherInfo']){
            $basicInfo = BasicInfo::find($arrayForUpdateData['userApplicationsId']);
            $ownerinfo["ownership_changed"]["ownership_changed"]                   = $basicInfo->ownership_changed; 
            $ownerinfo["business_percentage"]["ownership_has_twentypercent_share"] = $basicInfo->ownership_has_twentypercent_share;
            $ownerinfo["business_percentage"]["business_percentage"]               = $basicInfo->business_percentage;
        } 
        //session()->flush(); 
        return view('ppp.owner_info', compact('ownerinfo'));
    }

    public function businessinfo()
    {
        $stepsArray         = session('stepsArray');
        return view('businessinfo');
    }

    public function pppBusinessInfo()
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        $stepsArray    = session('pppstepsArray');
        $businessQuestions = array();
        $revenueReductions = array();
        if (!empty(session('arrayForUpdateData'))) {
            $arrayForUpdateData = session('arrayForUpdateData');
            $user_id            = $arrayForUpdateData['userID'];
            if (!empty($arrayForUpdateData['userApplicationsId'])) {
                $basic_info_id      = $arrayForUpdateData['userApplicationsId'];
                $businessQuestions  = BusinessQuestion::where('user_id', '=', $user_id)->where('basic_info_id', $basic_info_id)->first();
                $revenueReductions  = RevenueReduction::where('user_id', '=', $user_id)->where('basic_info_id', $basic_info_id)->first();
            }
        }

        return view('ppp.businessinfo', compact('stepsArray', 'businessQuestions', 'revenueReductions'));
    }

    public function documents()
    {
        if (!Auth::check()) {
            return redirect()->route('home');
        }
        $docsInDb = array();
        if (!empty(session('arrayForUpdateData'))) {
            $arrayForUpdateData = session('arrayForUpdateData');
            $user_id            = $arrayForUpdateData['userID'];
            $docsInDb           = Documents::where('user_id', '=', $user_id)->first();
        }
        $stepsArray             = session('stepsArray');

        return view('documents', compact('docsInDb'));
    }

    public function pppDocuments()
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        $docsInDb                 = array();
        $basicInfo                = array();
        $extraDocument            = array();
        $demographicInformation   = array();
        $applicationAuthorization = array();
        if (!empty(session('arrayForUpdateData'))) {
            $arrayForUpdateData        = session('arrayForUpdateData');
            $user_id                   = $arrayForUpdateData['userID'];
            $basic_info_id             = $arrayForUpdateData['userApplicationsId'];
            $docsInDb                  = Documents::where('user_id', '=', $user_id)->where('basic_info_id', $basic_info_id)->first();
            $basicInfo                 = BasicInfo::find($basic_info_id);
            $extraDocuments            = ExtraDocument::where('basic_info_id', $basic_info_id)->get();
            $demographicInformation    = DemographicInformation::where('basic_info_id', $basic_info_id)->get();
            $applicationAuthorization  = ApplicationAuthorization::where('basic_info_id', $basic_info_id)->first();
            if (!empty($extraDocuments)) {
                foreach ($extraDocuments as $row) {
                    $extraDocument[$row->type] = array(
                        'id' => $row->id, 'document_name' => $row->document_name,
                        'document_type' => $row->document_type, 'document_year' => $row->document_year
                    );
                }
            }
        } 
        return view('ppp.documents', compact('docsInDb', 'basicInfo', 'extraDocument', 'demographicInformation', 'applicationAuthorization'));
    }
    public function submitfinal()
    {
        $docsInDb = array();
        if (!Auth::check()) {
            return redirect()->route('home');
        } else {
            if (!empty(session('arrayForUpdateData'))) {
                $arrayForUpdateData = session('arrayForUpdateData');
                $user_id = $arrayForUpdateData['userID'];
                $docsInDb    = Documents::where('user_id', '=', $user_id)->first();
            }
        }

        $stepsArray         = session('stepsArray');
        $arrayForUpdateData = session('arrayForUpdateData');
        $fileData           =  Documents::where('user_id', $arrayForUpdateData['userID'])
            ->where('basic_info_id', $arrayForUpdateData['userApplicationsId'])->get();
        $docs = array();
        if (!$fileData->isEmpty()) {
            foreach ($fileData as $showDocuments) {
                $docs[$showDocuments->sort_order] = $showDocuments->document_name;
            }
            session(['selectedDocuments' => $docs]);
        }
        $selectedDocuments = session('selectedDocuments');
        $business_info     = '';
        $basic_info        = '';
        if (!empty($stepsArray['basicinfo'])) {
            $basic_info        = $stepsArray['basicinfo'];
        }
        if (!empty($stepsArray['businessinfo'])) {
            $business_info        = $stepsArray['businessinfo'];
        }

        $owner_info        = $stepsArray['ownerinfo'];
        //PUT HERE AFTER YOU SAVE
        return view('finalresult', compact('basic_info', 'owner_info', 'business_info', 'selectedDocuments', 'docsInDb'));
    }


    public function pppSubmitFinal()
    {
        $docsInDb = array();
        if (!Auth::check()) {
            return redirect()->route('login');
        } else {
            $applicationAuthorization = array();
            if (!empty(session('arrayForUpdateData'))) {
                $arrayForUpdateData        = session('arrayForUpdateData'); 
                $basic_info_id             = $arrayForUpdateData['userApplicationsId'];
                $applicationAuthorization  = ApplicationAuthorization::where('basic_info_id', $basic_info_id)->first(); 
            } 
        } 
        //PUT HERE AFTER YOU SAVE
        return view('ppp.finalresult',compact('applicationAuthorization'));
    }
}


/* DELIMITER ;;
CREATE TRIGGER `basic_infos_uuid` BEFORE INSERT ON `basic_infos` FOR EACH ROW
BEGIN
  IF new.sba_loan_number IS NULL THEN
    SET new.sba_loan_number = uuid_short();
  END IF;
END;;
DELIMITER ; */

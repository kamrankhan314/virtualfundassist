<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\BasicInfo;
use App\Models\BankInformations;
use App\Models\BusinessQuestion;
use Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $user_id   =  Auth::user()->id; 
        $basicInfo =  BasicInfo::where('user_id', $user_id)->orderBy('id', 'DESC')->limit(1)->first(); 
        if (empty($basicInfo)){ 
            return redirect()->route('login.pathway');
        }
        $this->setSessionAfterUpdate();
        return view('dashboard',compact('basicInfo'));
        //return view('welcome');
    }
    public function home()
    {
        $user_id   =  Auth::user()->id;
        $basicInfo =  BasicInfo::where('user_id', $user_id)->where('application_type','PPP')->orderBy('id', 'DESC')->limit(1)->first();
        if (!empty($basicInfo)) {
            $this->setPppSessionAfterUpdate();
            return redirect()->route('ppp.businessinfo');
        }
        else {
            return redirect()->route('ppp.basicinfo');
        }
    }
    public function offers()
    {
        $user_id   =  Auth::user()->id;
        $basicInfo =  BasicInfo::where('user_id', $user_id)->where('application_type', 'PPP')->orderBy('id', 'DESC')->limit(1)->first();
        if (!empty($basicInfo)) {
            $this->setPppSessionAfterUpdate(); 
        }
        return view('offers',compact('basicInfo'));
    }
    public function resources()
    {
        return view('resources');
    }
    public function creditcards()
    {
        return view('creditcards');
    }
    public function loginPathway(){
        return view('login_pathway');
    }


    private function setSessionAfterUpdate()
    {
        $user_id             =  Auth::user()->id;
        $stepArr             =  array();
        $user                =  User::find($user_id);
        $basicInfo           =  BasicInfo::where('user_id', $user_id)->where('application_type', 'Basic')->orderBy('id', 'DESC')->limit(1)->first();
         $arrayForUpdateData = array();
        if (!empty($basicInfo)) {
            $bankInformations    =  BankInformations::where('basic_info_id', $basicInfo->id)->first();
            $arrayForUpdateData['otherInfo']          = true;
            $arrayForUpdateData['userApplicationsId'] = $basicInfo->id;
            $arrayForUpdateData['is_updated']         = true;
            $arrayForUpdateData['userID']             = $user->id;
            session(['arrayForUpdateData' => $arrayForUpdateData]);
            $stepArr['basicinfo']['financial_amount']  = array('financial_amount' => $basicInfo->financial_amount);
            $stepArr['basicinfo']['credit_card_score'] = array('credit_card_score' => $basicInfo->credit_card_score);
            if(!empty($businessStartDate)){
            $businessStartDate = explode("-", $basicInfo->business_start_date);

            $stepArr['basicinfo']['business_start_date'] =  array(
                'business_start_date_year'  => @$businessStartDate[0],
                'business_start_date_month' => @$businessStartDate[1],
                'business_start_date_day'   => @$businessStartDate[2],
                'business_start_date'       => @$basicInfo->business_start_date
            );
        }
            $stepArr['basicinfo']['average_month_revenue'] = array('average_month_revenue' => $basicInfo->average_month_revenue);

            $stepArr['basicinfo']['loan_purpose']['loan_purpose'] = explode(",", $basicInfo->loan_purpose);

            $stepArr['basicinfo']['annul_personal_income'] = array('annul_personal_income' => $user->annul_personal_income);
            $stepArr['basicinfo']['business_information'] = array('business_information' => $basicInfo->business_information);

            $stepArr['basicinfo']['business_percentage'] = array('business_percentage' => $basicInfo->business_percentage);

            $stepArr['basicinfo']['business_description'] = array('business_description' => explode(",", $basicInfo->business_description));

            $stepArr['basicinfo']['business_state'] = array('business_state' =>  $basicInfo->business_state);
            $stepArr['basicinfo']['whatismore_important_toyou'] = array('whatismore_important_toyou' =>  $basicInfo->whatismore_important_toyou);

            $stepArr['basicinfo']['existing_business_debt'] = array('existing_business_debt' =>  $basicInfo->existing_business_debt);
            $stepArr['basicinfo']['is_invoice_customers'] = array('is_invoice_customers' =>  $basicInfo->is_invoice_customers);
            $stepArr['basicinfo']['is_seasonal'] = array('is_seasonal' =>  $basicInfo->is_seasonal);
            $stepArr['basicinfo']['is_accept_credit_cards'] = array('is_accept_credit_cards' =>  $basicInfo->is_accept_credit_cards);
            $stepArr['basicinfo']['is_use_accounting_software'] = array('is_use_accounting_software' =>  $basicInfo->is_use_accounting_software);
            $stepArr['basicinfo']['is_business_non_profit'] = array('is_business_non_profit' =>  $basicInfo->is_business_non_profit);
            $stepArr['basicinfo']['is_business_franchise'] = array('is_business_franchise' =>  $basicInfo->is_business_franchise);

            $stepArr['ownerinfo']['load_prfile'] = array(
                'first_name' => $user->first_name, 'last_name' => $user->last_name, 'business_name' => $user->business_name, 'mobile_number' =>  $user->business_name,
                'email' => $user->email
            );

            $stepArr['ownerinfo']['owner_information'] = array(
                'full_address' => $user->full_address,
                'city'         => $user->city,
                'state_id'     => $user->state_id,
                'zip_code'     => $user->zip_code
            );
if(!empty($dateOfBirth)){
            $dateOfBirth = explode("-", $user->date_of_birth);
            $stepArr['ownerinfo']['owner_credit_verification'] = array(
                'date_of_birth_year' => $dateOfBirth[0],
                'date_of_birth_month' => $dateOfBirth[1],
                'date_of_birth_day' => $dateOfBirth[2],
                'social_security_number' => $user->social_security_number,
                'date_of_birth' => $user->date_of_birth
            );
        }

            $stepArr['ownerinfo']['owner_credit_not_verified'] =  array(
                'tab_name' => 'owner_info',
                'step_name' => 'owner_credit_not_verified'
            );

            $businessQuestions    = BusinessQuestion::where('user_id', $user_id)->where('basic_info_id', $basicInfo->id)->first();
            $businessStartDate   = explode("-", $basicInfo->business_start_date);

            $stepArr['businessinfo']['basic_info'] =  array(
                'business_address'             => $basicInfo->business_address,
                'city'                         => $basicInfo->city,
                'state_id'                     => $basicInfo->state_id,
                'zip_code'                     => $basicInfo->zip_code,
                'tax_id'                       => $basicInfo->tax_id,
                'business_entity'              => $basicInfo->business_entity,    
                'total_employees'              => $basicInfo->total_employees,
                'is_received_sba_ecnomic_loan' => $basicInfo->is_received_sba_ecnomic_loan,
                'eidl_fund_received'           => $basicInfo->eidl_fund_received,
                'eidl_loanNumber'              => $basicInfo->eidl_loanNumber,
                'dba_name'                     => $basicInfo->dba_name,
                'business_type'                => $basicInfo->business_type,
                'average_monthly_payroll'      => $basicInfo->average_monthly_payroll,
                'sbadetails'                   => $basicInfo->sbadetails,
                'business_name'                => $basicInfo->business_name,
                'business_start_date_year'     => @$businessStartDate[0],
                'business_start_date_month'    => @$businessStartDate[1],
                'business_start_date_day'      => @$businessStartDate[2],
                'business_start_date'          => $basicInfo->business_start_date,
                'business_tin'                 => $basicInfo->business_tin,
                'business_industry'            => $basicInfo->business_industry,
                'financial_amount'             => $basicInfo->financial_amount,
                'average_payroll_year'         => $basicInfo->average_payroll_year,
                'sbayear'                      => $basicInfo->sbayear,
                'loan_purpose'                 => explode(",", $basicInfo->loan_purpose)
            );

            if (!empty($businessQuestions)) {
                $stepArr['businessinfo']['business_questions'] = array(
                    'first_question'  => $businessQuestions->first_question,
                    'second_question' => $businessQuestions->second_question,
                    'third_question'  => $businessQuestions->third_question,
                    'fourth_question' => $businessQuestions->fourth_question,
                    'fifth_question'  => $businessQuestions->fifth_question,
                    'sixth_question'  => $businessQuestions->sixth_question,
                    'seven_question'  => $businessQuestions->seven_question,
                    'eight_question'  => $businessQuestions->eight_question
                );
            }
if(!empty($bankInformations)){
            $stepArr['businessinfo']['sba_assistance_needed'] = array(
                'economicinnjury_disasterloan' => $bankInformations->economicinnjury_disasterloan,
                'bank_name' => $bankInformations->bank_name,
                'bank_routring_number' => $bankInformations->bank_routring_number,
                'bank_account_number' => $bankInformations->bank_account_number
            );
        }
            $stepArr['businessinfo']['business_financials'] = array('business_ownership_type' => $basicInfo->business_ownership_type);
            $stepArr['businessinfo']['final_questions'] = array(
                'existing_business_debt' => $basicInfo->existing_business_debt,
                'annual_personal_income' => $basicInfo->annual_personal_income
            );

            session(['stepsArray' => $stepArr]);
        }
    }


    private function setPppSessionAfterUpdate()
    {
        $user_id             =  Auth::user()->id;
        $stepArr             =  array();
        $user                =  User::find($user_id);
        $basicInfo           =  BasicInfo::where('user_id', $user_id)->where('application_type', 'PPP')->orderBy('id', 'DESC')->limit(1)->first();
        $arrayForUpdateData = array();
        if (!empty($basicInfo)) {
            $bankInformations    =  BankInformations::where('basic_info_id', $basicInfo->id)->first();
            $arrayForUpdateData['otherInfo']          = true;
            $arrayForUpdateData['userApplicationsId'] = $basicInfo->id;
            $arrayForUpdateData['is_updated']         = true;
            $arrayForUpdateData['userID']             = $user->id;
            session(['arrayForUpdateData' => $arrayForUpdateData]);
            $stepArr['basicinfo']['financial_amount']  = array('financial_amount' => $basicInfo->financial_amount);
            $stepArr['basicinfo']['credit_card_score'] = array('credit_card_score' => $basicInfo->credit_card_score);
            if (!empty($businessStartDate)) {
                $businessStartDate = explode("-", $basicInfo->business_start_date);

                $stepArr['basicinfo']['business_start_date'] =  array(
                    'business_start_date_year'  => @$businessStartDate[0],
                    'business_start_date_month' => @$businessStartDate[1],
                    'business_start_date_day'   => @$businessStartDate[2],
                    'business_start_date'       => @$basicInfo->business_start_date
                );
            }
            $stepArr['basicinfo']['average_month_revenue'] = array('average_month_revenue' => $basicInfo->average_month_revenue);

            $stepArr['basicinfo']['loan_purpose']['loan_purpose'] = explode(",", $basicInfo->loan_purpose);

            $stepArr['basicinfo']['annul_personal_income'] = array('annul_personal_income' => $user->annul_personal_income);
            $stepArr['basicinfo']['business_information'] = array('business_information' => $basicInfo->business_information);

            $stepArr['basicinfo']['business_percentage'] = array('business_percentage' => $basicInfo->business_percentage);

            $stepArr['basicinfo']['business_description'] = array('business_description' => explode(",", $basicInfo->business_description));

            $stepArr['basicinfo']['business_state'] = array('business_state' =>  $basicInfo->business_state);
            $stepArr['basicinfo']['whatismore_important_toyou'] = array('whatismore_important_toyou' =>  $basicInfo->whatismore_important_toyou);

            $stepArr['basicinfo']['existing_business_debt'] = array('existing_business_debt' =>  $basicInfo->existing_business_debt);
            $stepArr['basicinfo']['is_invoice_customers'] = array('is_invoice_customers' =>  $basicInfo->is_invoice_customers);
            $stepArr['basicinfo']['is_seasonal'] = array('is_seasonal' =>  $basicInfo->is_seasonal);
            $stepArr['basicinfo']['is_accept_credit_cards'] = array('is_accept_credit_cards' =>  $basicInfo->is_accept_credit_cards);
            $stepArr['basicinfo']['is_use_accounting_software'] = array('is_use_accounting_software' =>  $basicInfo->is_use_accounting_software);
            $stepArr['basicinfo']['is_business_non_profit'] = array('is_business_non_profit' =>  $basicInfo->is_business_non_profit);
            $stepArr['basicinfo']['is_business_franchise'] = array('is_business_franchise' =>  $basicInfo->is_business_franchise);

            $stepArr['ownerinfo']['load_prfile'] = array(
                'first_name' => $user->first_name, 'last_name' => $user->last_name, 'business_name' => $user->business_name, 'mobile_number' =>  $user->business_name,
                'email' => $user->email
            );

            $stepArr['ownerinfo']['owner_information'] = array(
                'full_address' => $user->full_address,
                'city'         => $user->city,
                'state_id'     => $user->state_id,
                'zip_code'     => $user->zip_code
            );
            if (!empty($dateOfBirth)) {
                $dateOfBirth = explode("-", $user->date_of_birth);
                $stepArr['ownerinfo']['owner_credit_verification'] = array(
                    'date_of_birth_year' => $dateOfBirth[0],
                    'date_of_birth_month' => $dateOfBirth[1],
                    'date_of_birth_day' => $dateOfBirth[2],
                    'social_security_number' => $user->social_security_number,
                    'date_of_birth' => $user->date_of_birth
                );
            }

            $stepArr['ownerinfo']['owner_credit_not_verified'] =  array(
                'tab_name' => 'owner_info',
                'step_name' => 'owner_credit_not_verified'
            );

            $businessQuestions    = BusinessQuestion::where('user_id', $user_id)->where('basic_info_id', $basicInfo->id)->first();
            $businessStartDate   = explode("-", $basicInfo->business_start_date);

            $stepArr['businessinfo']['basic_info'] =  array(
                'business_address'             => $basicInfo->business_address,
                'city'                         => $basicInfo->city,
                'state_id'                     => $basicInfo->state_id,
                'zip_code'                     => $basicInfo->zip_code,
                'tax_id'                       => $basicInfo->tax_id,
                'business_entity'              => $basicInfo->business_entity,
                'total_employees'              => $basicInfo->total_employees,
                'is_received_sba_ecnomic_loan' => $basicInfo->is_received_sba_ecnomic_loan,
                'eidl_fund_received'           => $basicInfo->eidl_fund_received,
                'eidl_loanNumber'              => $basicInfo->eidl_loanNumber,
                'dba_name'                     => $basicInfo->dba_name,
                'business_type'                => $basicInfo->business_type,
                'average_monthly_payroll'      => $basicInfo->average_monthly_payroll,
                'sbadetails'                   => $basicInfo->sbadetails,
                'business_name'                => $basicInfo->business_name,
                'business_start_date_year'     => @$businessStartDate[0],
                'business_start_date_month'    => @$businessStartDate[1],
                'business_start_date_day'      => @$businessStartDate[2],
                'business_start_date'          => $basicInfo->business_start_date,
                'business_tin'                 => $basicInfo->business_tin,
                'business_industry'            => $basicInfo->business_industry,
                'financial_amount'             => $basicInfo->financial_amount,
                'average_payroll_year'         => $basicInfo->average_payroll_year,
                'sbayear'                      => $basicInfo->sbayear,
                'loan_purpose'                 => explode(",", $basicInfo->loan_purpose)
            );

            if (!empty($businessQuestions)) {
                $stepArr['businessinfo']['business_questions'] = array(
                    'first_question'  => $businessQuestions->first_question,
                    'second_question' => $businessQuestions->second_question,
                    'third_question'  => $businessQuestions->third_question,
                    'fourth_question' => $businessQuestions->fourth_question,
                    'fifth_question'  => $businessQuestions->fifth_question,
                    'sixth_question'  => $businessQuestions->sixth_question,
                    'seven_question'  => $businessQuestions->seven_question,
                    'eight_question'  => $businessQuestions->eight_question
                );
            }
            if (!empty($bankInformations)) {
                $stepArr['businessinfo']['sba_assistance_needed'] = array(
                    'economicinnjury_disasterloan' => $bankInformations->economicinnjury_disasterloan,
                    'bank_name' => $bankInformations->bank_name,
                    'bank_routring_number' => $bankInformations->bank_routring_number,
                    'bank_account_number' => $bankInformations->bank_account_number
                );
            }
            $stepArr['businessinfo']['business_financials'] = array('business_ownership_type' => $basicInfo->business_ownership_type);
            $stepArr['businessinfo']['final_questions'] = array(
                'existing_business_debt' => $basicInfo->existing_business_debt,
                'annual_personal_income' => $basicInfo->annual_personal_income
            );

            session(['pppstepsArray' => $stepArr]);
        }
    }
}

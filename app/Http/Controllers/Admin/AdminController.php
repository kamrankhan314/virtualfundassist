<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\Models\User;
use App\Models\BasicInfo;
use App\Models\BankInformations;
use App\Models\Documents;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
        View::share('nav', 'dashboard'); //ss
    }

    public function index()
    {
        return view('admin.home'); //ss
    }

    public function customers()
    {
        $users = User::orderBy('updated_at','DESC')->get();
        return view('admin.customers',compact('users')); //ss
    }

    public function customerDetails($id){
        $basicInfo = BasicInfo::where('user_id',$id)->get();
        $bankInfo  = BankInformations::where('user_id',$id)->get();
        $documents = Documents::where('user_id',$id)->orderBy('updated_at', 'DESC')->first(); 
        return view('admin.customer_details',compact('basicInfo', 'bankInfo','documents'));
    }

    public function basicInfoDetails($id)
    {
        $info = BasicInfo::where('user_id', $id)->first(); 
        return view('admin.basicinfo', compact('info'));
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BankInformations extends Model
{
    /**
     * The attributes that are mass assignable.
     * 
     * @var array
     */
    protected $fillable = ['user_id', 'basic_info_id', 'economicinnjury_disasterloan', 'bank_name',
    'bank_routring_number', 'bank_account_number'];

    protected $timestamp = true;
}

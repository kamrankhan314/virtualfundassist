<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RevenueReduction extends Model
{
    protected $fillable = ['first_quarter' ,
                        'second_quarter' ,
                        'third_quarter',
                        'fourth_quarter',
                        'type',
                        'year',
                        'user_id',
                        'basic_info_id'];
}

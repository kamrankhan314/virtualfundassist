<?php

namespace App\Providers;

use App\Repository\Interfaces\EloquentRepositoryInterface; 
use App\Repository\sInterface\UserRepositoryInterface; 
use App\Repository\UserRepository; 
use App\Repository\BaseRepository; 
use Illuminate\Support\ServiceProvider; 

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
	    $this->app->bind(EloquentRepositoryInterface::class,BaseRepository::class);
	    $this->app->bind(UserRepositoryInterface::class,UserRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
}); 
Route::group(['namespace' => 'Front'], function () {
    Route::get('/user-dashboard', 'UserController@index')->name('user.dashboard');
    
    Route::get('/edit-application', 'UserController@home')->name('edit.application');
    Route::get('/offers', 'UserController@offers')->name('offers');
    Route::get('/resource', 'UserController@resources')->name('resources');
    Route::get('/creditcards', 'UserController@creditcards')->name('creditcards');
    Route::get('/login-pathway', 'UserController@loginPathway')->name('login.pathway');
    
    Route::get('/basic-info', 'CustomerController@basicinfo')->name('basicinfo');
    Route::get('/owner-info', 'CustomerController@ownerinfo')->name('ownerinfo');
    Route::get('/business-info', 'CustomerController@businessinfo')->name('businessinfo');
    Route::get('/documents', 'CustomerController@documents')->name('documents');
    Route::get('/finalform', 'CustomerController@submitfinal')->name('finalform');
    Route::get('/update-allform', 'CommonController@show')->name('update.allform'); 
    Route::get('/', 'CommonController@index')->name('services.index');
    Route::post('/createfund', 'CommonController@create')->name('basicinfo.create');
   
    Route::get('/basicinfos/{basicinfo}/edit', 'CommonController@edit')->name('services.edit'); 

    Route::post('/basicinfo', 'CommonController@store')->name('basicinfo.store');
    Route::post('/return-steps', 'CommonController@previousStep')->name('return.steps');
    Route::post('/documents', 'CommonController@UploadDocuments')->name('upload.documents');
    Route::post('/uploadfile', 'CommonController@uploadFile')->name('upload.file');

    Route::post('/basicinfos/update', 'CommonController@update')->name('basicinfos.update');
    Route::post('/basicinfos/update-business-info', 'CommonController@updateBusinessInfo')->name('update.business.info');
    Route::post('/basicinfos/update-owner-info', 'CommonController@updateOwnerInfo')->name('update.owner.info');


    Route::get('/ppp/basic-info', 'CustomerController@pppBasicInfo')->name('ppp.basicinfo');
    Route::get('/ppp/owner-info', 'CustomerController@pppOwnerInfo')->name('ppp.ownerinfo');
    Route::get('/ppp/business-info', 'CustomerController@pppBusinessInfo')->name('ppp.businessinfo');
    Route::get('/ppp/documents', 'CustomerController@pppDocuments')->name('ppp.documents');
    Route::get('/ppp/esign', 'CustomerController@pppSubmitFinal')->name('ppp.finalform');

    Route::post('/ppp/basicinfo', 'CommonController@pppStore')->name('ppp.basicinfo.store');
    Route::post('ppp/return-steps', 'CommonController@pppPreviousStep')->name('ppp.return.steps');
    Route::post('/ppp/validate','CommonController@pppValidate')->name('ppp.validate');
    Route::post('/ppp/createprofile', 'CommonController@createPpppProfile')->name('create.ppp.profile');
    Route::get('/ppp/createppploan', 'CommonController@createPPPLoan')->name('ppp.createppploan');

    Route::post('/ppp/documentstore', 'CommonController@pppDocumentStore')->name('ppp.document.store');
    Route::post('/ppp/uploadfile', 'CommonController@pppUploadFile')->name('pppupload.file'); 
    Route::get('/ppp/filedelete/{id}/{field}','CommonController@deleteDocument')->name('ppp.deletefile');
     
});
Auth::routes();
Route::get('/', 'HomeController@home')->name('home'); 
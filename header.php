<?php session_start(); ?>

<!DOCTYPE html>
<?php $home_url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]/lendio"; ?>

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="In need of financing and business loan? Virtual Fund Assist is here to cater to all your business funding needs. Simple application. Quick online business loans. Low rates. Get business loan now!" />
    <title>Virtual Fund Assist | Ultimate Business Loan Marketplace</title>

    <!--== FAVICON ==-->
    <link rel="apple-touch-icon" sizes="180x180" href="<?= $home_url ?>/images/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= $home_url ?>/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= $home_url ?>/images/favicon-16x16.png">
    <link rel="shortcut icon" href="<?= $home_url ?>/images/favicon.ico">

    <!--== GOOLFE FONTS ==-->
    <link rel="preload" href="<?= $home_url ?>/fonts/proximasoft-regular-webfont.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="<?= $home_url ?>/fonts/proximasoft-light-webfont.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="<?= $home_url ?>/fonts/proximasoft-medium-webfont.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="<?= $home_url ?>/fonts/proximasoft-bold-webfont.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="<?= $home_url ?>/fonts/proximasoft-semibold-webfont.woff2" as="font" type="font/woff2" crossorigin>

    <!--==== Style CSS ====-->
    <link rel='stylesheet' href='<?= $home_url ?>/css/styles.mind.css' type='text/css' media='all' />
    <link rel='stylesheet' href='<?= $home_url ?>/css/login.css' type='text/css' media='all' />
    <link rel='stylesheet' href='<?= $home_url ?>/css/main.css' type='text/css' media='all' />

    <!-- Font-awsome -->
    <link rel="stylesheet" href="<?= $home_url ?>/css/fontawesome.css" type="text/css">

</head>

<?php $page_name =  basename($_SERVER['PHP_SELF']);
if ($page_name == "company.php" || $page_name == "partners.php") {
    $header_type_class = "site-header-type-transparent";
} else {
    $header_type_class = "site-header-type-white";
}

if ($page_name == "business-and-qa.php") { ?>
    <link rel="stylesheet" href="<?= $home_url ?>/css/business-and-qa.css" type="text/css">
<?php } ?>
<!-- ==== Page(Home) Body Start ==== -->

<body class="virtual-fund-assis" data-ng-app="wp-app">
    <!-- ==== Page(Home) HEADER (FIXED) Start ==== -->
    <div id="site-header-container-fixed" class="<?= $header_type_class ?>">
        <div class="container nav-container">
            <div class="site-header">
                <a class="site-logo" href="<?= $home_url ?>" data-wpel-link="internal">
                    <img alt="Site Logo" src="<?= $home_url ?>/images/logo.png" />
                </a>
                <div class="site-menu-container" data-cy="site-menu-container-1">
                    <div class="site-menu">

                        <!-- ===  Main Menu 1 === -->
                        <ul id="menu-main-1" class="site-menu-list">
                            <li id="menu-item-93250" class="menu-item menu-item-type-post_type menu-item-object-covid_relief menu-item-has-children menu-link-93250 site-menu-link" data-cy="mobile-menu-link-covid-19-relief"><a href="<?= $home_url ?>/covid-relief/sba-paycheck-protection-program-loans" class="parent-link" data-cy="covid-19-relief" data-wpel-link="internal">COVID-19
                                    Relief<div class="site-submenu-caret"></div></a>
                                <div class="site-submenu">
                                    <h3><a href="#" class="site-submenu-header" data-wpel-link="internal">COVID-19
                                            Relief</a></h3>
                                    <ul class="site-submenu-list">
                                        <li id="menu-item-93252" class="menu-item menu-item-type-post_type menu-item-object-covid_relief menu-link-93252 site-submenu-link" data-cy="mobile-menu-link-sba-paycheck-protection-program-ppp"><a href="<?= $home_url ?>/covid-relief/sba-paycheck-protection-program-loans" data-cy="sba-paycheck-protection-program-ppp" data-wpel-link="internal">SBA Paycheck Protection Program (PPP)</a></li>
                                        <li id="menu-item-93253" class="menu-item menu-item-type-post_type menu-item-object-covid_relief menu-link-93253 site-submenu-link" data-cy="mobile-menu-link-93253"><a href="<?= $home_url ?>/covid-relief/apply-ppp-loan" data-cy="93253" data-wpel-link="internal">How to Apply for a PPP Loan</a></li>
                                        <li id="menu-item-93408" class="menu-item menu-item-type-post_type menu-item-object-covid_relief menu-link-93408 site-submenu-link" data-cy="mobile-menu-link-93408"><a href="<?= $home_url ?>/covid-relief/ppp-loan-forgiveness" data-cy="93408" data-wpel-link="internal">PPP Loan Forgiveness</a></li>
                                        <li id="menu-item-93599" class="menu-item menu-item-type-custom menu-item-object-custom menu-link-93599 site-submenu-link" data-cy="mobile-menu-link-ppp-loan-forgiveness-estimator"><a href="<?= $home_url ?>/covid-relief/ppp-loan-forgiveness-estimator" data-cy="ppp-loan-forgiveness-estimator" data-wpel-link="internal">PPP
                                                Loan Forgiveness Estimator</a></li>
                                        <li id="menu-item-93254" class="menu-item menu-item-type-post_type menu-item-object-covid_relief menu-link-93254 site-submenu-link" data-cy="mobile-menu-link-93254"><a href="<?= $home_url ?>/covid-relief/economic-injury-disaster-loans-eidl" data-cy="93254" data-wpel-link="internal">Economic Injury Disaster Loans
                                                (EIDL)</a></li>
                                        <li id="menu-item-93256" class="menu-item menu-item-type-post_type menu-item-object-covid_relief menu-link-93256 site-submenu-link" data-cy="mobile-menu-link-93256"><a href="<?= $home_url ?>/covid-relief/sba-coronavirus-loans" data-cy="93256" data-wpel-link="internal">SBA Coronavirus Loans
                                                (COVID-19)</a></li>

                                        <div class="submenu-padding"></div>
                                        <div class="submenu-pointer"></div>
                                    </ul>
                                </div>
                            </li>
                            <li id="menu-item-93259" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-link-93259 site-menu-link" data-cy="mobile-menu-link-about">
                                <a href="<?= $home_url ?>/company" class="parent-link" data-cy="about" data-wpel-link="internal">About
                                    <div class="site-submenu-caret"></div>
                                </a>
                                <div class="site-submenu">
                                    <h3>
                                        <a href="#" class="site-submenu-header" data-wpel-link="internal">About</a>
                                    </h3>
                                    <ul class="site-submenu-list">
                                        <li id="menu-item-93260" class="menu-item menu-item-type-post_type menu-item-object-page menu-link-93260 site-submenu-link" data-cy="mobile-menu-link-how-it-works">
                                            <a href="<?= $home_url ?>/about/how-it-works" data-cy="how-it-works" data-wpel-link="internal">How It Works</a>
                                        </li>
                                        <li id="menu-item-93261" class="menu-item menu-item-type-post_type menu-item-object-page menu-link-93261 site-submenu-link" data-cy="mobile-menu-link-about-us">
                                            <a href="<?= $home_url ?>/about/company" data-cy="about-us" data-wpel-link="internal">About Us</a>
                                        </li>
                                        <div class="submenu-padding"></div>
                                        <div class="submenu-pointer"></div>
                                    </ul>
                                </div>
                            </li>
                            <li id="menu-item-93264" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-link-93264 site-menu-link" data-cy="mobile-menu-link-loan-types"><a href="<?= $home_url ?>/business-loans" class="parent-link" data-cy="loan-types" data-wpel-link="internal">Loan Types<div class="site-submenu-caret"></div></a>
                                <div class="site-submenu">
                                    <h3><a href="#" class="site-submenu-header" data-wpel-link="internal">Loan Types</a>
                                    </h3>
                                    <ul class="site-submenu-list">
                                        <li id="menu-item-93265" class="menu-item menu-item-type-custom menu-item-object-custom menu-link-93265 site-submenu-link" data-cy="mobile-menu-link-credit-cards"><a href="<?= $home_url ?>/business-loans/business-credit-card" data-cy="credit-cards" data-wpel-link="internal">Credit Cards</a></li>
                                        <li id="menu-item-93266" class="menu-item menu-item-type-post_type menu-item-object-business_loans menu-link-93266 site-submenu-link" data-cy="mobile-menu-link-93266"><a href="<?= $home_url ?>/business-loans/line-of-credit" data-cy="93266" data-wpel-link="internal">Business Line of Credit</a></li>
                                        <li id="menu-item-93267" class="menu-item menu-item-type-post_type menu-item-object-business_loans menu-link-93267 site-submenu-link" data-cy="mobile-menu-link-sba-loans"><a href="<?= $home_url ?>/business-loans/sba-loans" data-cy="sba-loans" data-wpel-link="internal">SBA Loans</a></li>
                                        <li id="menu-item-93268" class="menu-item menu-item-type-post_type menu-item-object-business_loans menu-link-93268 site-submenu-link" data-cy="mobile-menu-link-93268"><a href="<?= $home_url ?>/business-loans/short-term-loans" data-cy="93268" data-wpel-link="internal">Short Term Loan</a></li>
                                        <li id="menu-item-93269" class="menu-item menu-item-type-post_type menu-item-object-business_loans menu-link-93269 site-submenu-link" data-cy="mobile-menu-link-93269"><a href="<?= $home_url ?>/business-loans/term-loans" data-cy="93269" data-wpel-link="internal">Business Term Loan</a></li>
                                        <li id="menu-item-93273" class="menu-item menu-item-type-post_type menu-item-object-business_loans menu-link-93273 site-submenu-link" data-cy="mobile-menu-link-93273"><a href="<?= $home_url ?>/business-loans/commercial-real-estate" data-cy="93273" data-wpel-link="internal">Commercial Mortgage</a></li>

                                        <li id="menu-item-93275" class="menu-item menu-item-type-post_type menu-item-object-business_loans menu-link-93275 site-submenu-link" data-cy="mobile-menu-link-93275"><a href="<?= $home_url ?>/business-loans/startup-loans" data-cy="93275" data-wpel-link="internal">Startup Loan</a></li>
                                        <div class="submenu-padding"></div>
                                        <div class="submenu-pointer"></div>
                                    </ul>
                                </div>
                            </li>
                            <li id="menu-item-93277" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-link-93277 site-menu-link" data-cy="mobile-menu-link-loan-calculators"><a href="<?= $home_url ?>/business-calculators" class="parent-link" data-cy="loan-calculators" data-wpel-link="internal">Loan
                                    Calculators<div class="site-submenu-caret"></div></a>
                                <div class="site-submenu">
                                    <h3><a href="#" class="site-submenu-header" data-wpel-link="internal">Loan
                                            Calculators</a></h3>
                                    <ul class="site-submenu-list">
                                        <li id="menu-item-93278" class="menu-item menu-item-type-post_type menu-item-object-calculators menu-link-93278 site-submenu-link" data-cy="mobile-menu-link-93278"><a href="<?= $home_url ?>/business-calculators/equipment-loan-calculator" data-cy="93278" data-wpel-link="internal">Equipment Loan Calculator</a>
                                        </li>
                                        <li id="menu-item-93279" class="menu-item menu-item-type-post_type menu-item-object-calculators menu-link-93279 site-submenu-link" data-cy="mobile-menu-link-sba-paycheck-protection-program-calculator"><a href="<?= $home_url ?>/business-calculators/sba-loan-calculator" data-cy="sba-paycheck-protection-program-calculator" data-wpel-link="internal">SBA PPP Loan Calculator</a></li>
                                        <li id="menu-item-93280" class="menu-item menu-item-type-post_type menu-item-object-calculators menu-link-93280 site-submenu-link" data-cy="mobile-menu-link-93280"><a href="<?= $home_url ?>/business-calculators/short-term-loan-calculator" data-cy="93280" data-wpel-link="internal">Short Term Loan Calculator</a>
                                        </li>
                                        <li id="menu-item-93281" class="menu-item menu-item-type-post_type menu-item-object-calculators menu-link-93281 site-submenu-link" data-cy="mobile-menu-link-93281"><a href="<?= $home_url ?>/business-calculators/startup-business-loan-calculator" data-cy="93281" data-wpel-link="internal">Startup Business Loan
                                                Calculator</a></li>
                                        <li id="menu-item-93283" class="menu-item menu-item-type-post_type menu-item-object-calculators menu-link-93283 site-submenu-link" data-cy="mobile-menu-link-93283"><a href="<?= $home_url ?>/business-calculators/business-term-loan-calculator" data-cy="93283" data-wpel-link="internal">Business Term Loan
                                                Calculator</a></li>
                                        <li id="menu-item-93285" class="menu-item menu-item-type-post_type menu-item-object-calculators menu-link-93285 site-submenu-link" data-cy="mobile-menu-link-93285"><a href="<?= $home_url ?>/business-calculators/business-line-credit-calculator" data-cy="93285" data-wpel-link="internal">Line of Credit Calculator</a>
                                        </li>
                                        <li id="menu-item-93286" class="menu-item menu-item-type-post_type menu-item-object-calculators menu-link-93286 site-submenu-link" data-cy="mobile-menu-link-93286"><a href="<?= $home_url ?>/business-calculators/business-credit-card-calculator" data-cy="93286" data-wpel-link="internal">Business Credit Card
                                                Calculator</a></li>
                                        <div class="submenu-padding"></div>
                                        <div class="submenu-pointer"></div>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                        <div class="cta-container">

                            <!-- ===  Remove After Big Menu Push! === -->
                            <a href="<?= $home_url ?>/basic-info" data-wpel-link="internal">
                                <button class="secondary-button alt">Get Loan Offers</button>
                            </a>
                        </div>
                        <div class="py-4 text-center d-lg-none d-md-block">
                            <a href="#" style="font-weight: 600;" data-wpel-link="internal">Login</a>
                        </div>
                    </div>
                    <a href="#" class="site-menu-close" data-cy="site-menu-close-2"> </a>
                </div>
                <a href="#" class="site-menu-toggle" data-cy="site-menu-toggle-1"></a>
                <div class="header-right">
                    <?php if (isset($_SESSION['logged_in_id'])) { ?>
                        <a class="site-header-sign-in" href="<?= $home_url ?>/logout" data-wpel-link="internal">Sign Out</a>
                    <?php } else { ?>
                        <a class="site-header-sign-in" href="<?= $home_url ?>/login" data-wpel-link="internal">Sign In</a>
                    <?php } ?>
                    <a href="<?= $home_url ?>/basic-info" class="site-header-start-btn primary-button" data-wpel-link="internal">Get Loan Offers</a>
                </div>
                <div class="clear"></div>
            </div>
            <div class="light-logo-preloader"></div>
        </div>
    </div>

    <!-- ==== Page(Home) HEADER Start ==== -->
    <div id="site-header-container" class="<?= $header_type_class ?>">
        <div class="ie-banner" hidden>
            <div class="container">
                <div class="ie-banner-content"> Lendio no longer supports Internet Explorer. To improve your browsing
                    experience, we recommend you use an alternative service such as <a href="https://www.google.com/chrome/" target="_blank" data-wpel-link="external" rel="external noopener noreferrer">Google Chrome</a>, <a href="https://www.microsoft.com/en-us/edge" target="_blank" data-wpel-link="external" rel="external noopener noreferrer">Microsoft Edge</a>, or <a href="https://www.mozilla.org/en-US/firefox/new/" target="_blank" data-wpel-link="external" rel="external noopener noreferrer">Firefox</a>.
                </div>
            </div>
        </div>
        <div class="container nav-container">
            <div class="site-header">
                <a class="site-logo" href="<?= $home_url ?>" data-wpel-link="internal">
                    <img alt="Site Logo" src="<?= $home_url ?>/images/logo.png" />
                </a>
                <div id="site-menu-container" data-cy="site-menu-container-2">
                    <div class="site-menu">
                        <ul id="menu-main-1" class="site-menu-list">
                            <li class="menu-item menu-item-type-post_type menu-item-object-covid_relief menu-item-has-children menu-link-93250 site-menu-link" data-cy="mobile-menu-link-covid-19-relief"><a href="<?= $home_url ?>/covid-relief/sba-paycheck-protection-program-loans" class="parent-link" data-cy="covid-19-relief" data-wpel-link="internal">COVID-19
                                    Relief<div class="site-submenu-caret"></div></a>
                                <div class="site-submenu">
                                    <h3><a href="#" class="site-submenu-header" data-wpel-link="internal">COVID-19
                                            Relief</a></h3>
                                    <ul class="site-submenu-list">
                                        <li class="menu-item menu-item-type-post_type menu-item-object-covid_relief menu-link-93252 site-submenu-link" data-cy="mobile-menu-link-sba-paycheck-protection-program-ppp"><a href="<?= $home_url ?>/covid-relief/sba-paycheck-protection-program-loans" data-cy="sba-paycheck-protection-program-ppp" data-wpel-link="internal">SBA Paycheck Protection Program (PPP)</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-covid_relief menu-link-93253 site-submenu-link" data-cy="mobile-menu-link-93253"><a href="<?= $home_url ?>/covid-relief/apply-ppp-loan" data-cy="93253" data-wpel-link="internal">How to Apply for a PPP Loan</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-covid_relief menu-link-93408 site-submenu-link" data-cy="mobile-menu-link-93408"><a href="<?= $home_url ?>/covid-relief/ppp-loan-forgiveness" data-cy="93408" data-wpel-link="internal">PPP Loan Forgiveness</a></li>
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-link-93599 site-submenu-link" data-cy="mobile-menu-link-ppp-loan-forgiveness-estimator"><a href="<?= $home_url ?>/covid-relief/ppp-loan-forgiveness-estimator" data-cy="ppp-loan-forgiveness-estimator" data-wpel-link="internal">PPP
                                                Loan Forgiveness Estimator</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-covid_relief menu-link-93254 site-submenu-link" data-cy="mobile-menu-link-93254"><a href="<?= $home_url ?>/covid-relief/economic-injury-disaster-loans-eidl" data-cy="93254" data-wpel-link="internal">Economic Injury Disaster Loans
                                                (EIDL)</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-covid_relief menu-link-93256 site-submenu-link" data-cy="mobile-menu-link-93256"><a href="<?= $home_url ?>/covid-relief/sba-coronavirus-loans" data-cy="93256" data-wpel-link="internal">SBA Coronavirus Loans
                                                (COVID-19)</a></li>
                                        <div class="submenu-padding"></div>
                                        <div class="submenu-pointer"></div>
                                    </ul>
                                </div>
                            </li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-link-93259 site-menu-link" data-cy="mobile-menu-link-about">
                                <a href="<?= $home_url ?>/company" class="parent-link" data-cy="about" data-wpel-link="internal">About
                                    <div class="site-submenu-caret"></div>
                                </a>
                                <div class="site-submenu">
                                    <h3>
                                        <a href="#" class="site-submenu-header" data-wpel-link="internal">About</a>
                                    </h3>
                                    <ul class="site-submenu-list">
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-link-93260 site-submenu-link" data-cy="mobile-menu-link-how-it-works">
                                            <a href="<?= $home_url ?>/about/how-it-works" data-cy="how-it-works" data-wpel-link="internal">How It Works</a>
                                        </li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-link-93261 site-submenu-link" data-cy="mobile-menu-link-about-us">
                                            <a href="<?= $home_url ?>/about/company" data-cy="about-us" data-wpel-link="internal">About Us</a>
                                        </li>
                                        <div class="submenu-padding"></div>
                                        <div class="submenu-pointer"></div>
                                    </ul>
                                </div>
                            </li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-link-93264 site-menu-link" data-cy="mobile-menu-link-loan-types"><a href="<?= $home_url ?>/business-loans" class="parent-link" data-cy="loan-types" data-wpel-link="internal">Loan Types<div class="site-submenu-caret"></div></a>
                                <div class="site-submenu">
                                    <h3><a href="#" class="site-submenu-header" data-wpel-link="internal">Loan Types</a>
                                    </h3>
                                    <ul class="site-submenu-list">
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-link-93265 site-submenu-link" data-cy="mobile-menu-link-credit-cards"><a href="<?= $home_url ?>/business-loans/business-credit-card" data-cy="credit-cards" data-wpel-link="internal">Credit Cards</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-business_loans menu-link-93266 site-submenu-link" data-cy="mobile-menu-link-93266"><a href="<?= $home_url ?>/business-loans/line-of-credit" data-cy="93266" data-wpel-link="internal">Business Line of Credit</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-business_loans menu-link-93267 site-submenu-link" data-cy="mobile-menu-link-sba-loans"><a href="<?= $home_url ?>/business-loans/sba-loans" data-cy="sba-loans" data-wpel-link="internal">SBA Loans</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-business_loans menu-link-93268 site-submenu-link" data-cy="mobile-menu-link-93268"><a href="<?= $home_url ?>/business-loans/short-term-loans" data-cy="93268" data-wpel-link="internal">Short Term Loan</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-business_loans menu-link-93269 site-submenu-link" data-cy="mobile-menu-link-93269"><a href="<?= $home_url ?>/business-loans/term-loans" data-cy="93269" data-wpel-link="internal">Business Term Loan</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-business_loans menu-link-93273 site-submenu-link" data-cy="mobile-menu-link-93273"><a href="<?= $home_url ?>/business-loans/commercial-real-estate" data-cy="93273" data-wpel-link="internal">Commercial Mortgage</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-business_loans menu-link-93275 site-submenu-link" data-cy="mobile-menu-link-93275"><a href="<?= $home_url ?>/business-loans/startup-loans" data-cy="93275" data-wpel-link="internal">Startup Loan</a></li>
                                        <div class="submenu-padding"></div>
                                        <div class="submenu-pointer"></div>
                                    </ul>
                                </div>
                            </li>
                            <li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-link-93277 site-menu-link" data-cy="mobile-menu-link-loan-calculators"><a href="<?= $home_url ?>/business-calculators" class="parent-link" data-cy="loan-calculators" data-wpel-link="internal">Loan
                                    Calculators<div class="site-submenu-caret"></div></a>
                                <div class="site-submenu">
                                    <h3><a href="#" class="site-submenu-header" data-wpel-link="internal">Loan
                                            Calculators</a></h3>
                                    <ul class="site-submenu-list">
                                        <li class="menu-item menu-item-type-post_type menu-item-object-calculators menu-link-93278 site-submenu-link" data-cy="mobile-menu-link-93278"><a href="<?= $home_url ?>/business-calculators/equipment-loan-calculator" data-cy="93278" data-wpel-link="internal">Equipment Loan Calculator</a>
                                        </li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-calculators menu-link-93279 site-submenu-link" data-cy="mobile-menu-link-sba-paycheck-protection-program-calculator"><a href="<?= $home_url ?>/business-calculators/sba-loan-calculator" data-cy="sba-paycheck-protection-program-calculator" data-wpel-link="internal">SBA PPP Loan Calculator</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-calculators menu-link-93280 site-submenu-link" data-cy="mobile-menu-link-93280"><a href="<?= $home_url ?>/business-calculators/short-term-loan-calculator" data-cy="93280" data-wpel-link="internal">Short Term Loan Calculator</a>
                                        </li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-calculators menu-link-93281 site-submenu-link" data-cy="mobile-menu-link-93281"><a href="<?= $home_url ?>/business-calculators/startup-business-loan-calculator" data-cy="93281" data-wpel-link="internal">Startup Business Loan
                                                Calculator</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-calculators menu-link-93283 site-submenu-link" data-cy="mobile-menu-link-93283"><a href="<?= $home_url ?>/business-calculators/business-term-loan-calculator" data-cy="93283" data-wpel-link="internal">Business Term Loan
                                                Calculator</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-calculators menu-link-93285 site-submenu-link" data-cy="mobile-menu-link-93285"><a href="<?= $home_url ?>/business-calculators/business-line-credit-calculator" data-cy="93285" data-wpel-link="internal">Line of Credit Calculator</a>
                                        </li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-calculators menu-link-93286 site-submenu-link" data-cy="mobile-menu-link-93286"><a href="<?= $home_url ?>/business-calculators/business-credit-card-calculator" data-cy="93286" data-wpel-link="internal">Business Credit Card
                                                Calculator</a></li>
                                        <div class="submenu-padding"></div>
                                        <div class="submenu-pointer"></div>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                        <div class="cta-container"> <a href="<?= $home_url ?>/basic-info" data-wpel-link="internal"><button class="primary-button">Get Loan Offers</button></a>
                        </div>
                    </div> <a href="#" class="site-menu-close" data-cy="site-menu-close-2"> </a>
                </div>
                <a href="#" class="site-menu-toggle" data-cy="site-menu-toggle-2"></a>
                <div class="header-right">
                    <?php if (isset($_SESSION['logged_in_id'])) { ?>
                        <a class="site-header-sign-in" href="<?= $home_url ?>/logout" data-wpel-link="internal">Sign Out</a>
                    <?php } else { ?>
                        <a class="site-header-sign-in" href="<?= $home_url ?>/tabs/login" data-wpel-link="internal">Sign In</a>
                    <?php } ?>
                    <a href="<?= $home_url ?>/tabs/owner-info" class="site-header-start-btn primary-button" data-wpel-link="internal">Get Loan Offers</a>
                </div>
                <div class="clear"></div>
            </div>
            <div class="light-logo-preloader"></div>
        </div>
    </div>
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'User Dashboard') }}</title>
        <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('public/dist/css/bootstrap.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('public/css/userdashboard.css')}}">

 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> 
  
    
    @stack('styles')
    <style>
        .log-out {
            cursor: pointer;
        }

        .log-out img {
            width: 30px;
            height: 25px;
        }
    </style>
</head>

<!-- ==== Home-Page (Dashboard) Body Start in HTML==== -->

<body>
    <div class="ppp-home-page">
        <!-- Open/Close dashboard Btn -->
        <input type="checkbox" name="value" id="check">
        <label for="check">
            <i class="fa fa-bars" id="btn"></i>
            <i class="fa fa-times" id="cancel"></i>
        </label>
        <!-- Asidebar Start here -->

        <aside class="sidebar">
            <header>
                <a href="{{route('user.dashboard')}}"><img width="80%" src="{{ asset('public/img/logo.png')}}"></a>
            </header>
            <ul>
                <li><a href="{{route('user.dashboard')}}"><img
                            src="{{ asset('public/img/icon_overview.png')}}">Dashboard</a></li>
                <li><a href="{{route('offers')}}"><img src="{{ asset('public/img/icon_offers.png')}}">Offers</a></li>
                <li><a href="{{route('creditcards')}}"><img src="{{ asset('public/img/icon_creditcard.png')}}">Credit
                        Cards</a></li>
                <li><a href="{{route('resources')}}"><img src="{{ asset('public/img/icon_resources.png')}}">Your
                        Resources</a></li>

                <li><a onclick="event.preventDefault();  document.getElementById('logout-form').submit();"
                        class="log-out">
                        <img src="{{ asset('public/img/logout.png')}}">Log Out</a></li>
            </ul>
        </aside>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">@csrf</form>

        <!-- Dashboard content Start here -->
        <section class="dashboard-content">
            <div class="getting-finance">
                @yield('content')
            </div>
        </section>
    </div>

    </div>
</body>

</html> 

 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js">
        </script>
@stack('scripts')

</body>
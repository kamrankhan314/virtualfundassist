<!-- ====== (Header) For SECTION Tabs  ===== -->
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<?php $home_url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]/lendio"; 
$home_url = 'http://localhost/virtualfundassist/lendo'; 
?>

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="In need of financing and business loan? Virtual Fund Assist is here to cater to all your business funding needs. Simple application. Quick online business loans. Low rates. Get business loan now!" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'virtualfundassist') }}</title>

    <!--== FAVICON ==-->
    <link rel="apple-touch-icon" sizes="180x180" href="<?= $home_url ?>/images/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= $home_url ?>/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= $home_url ?>/images/favicon-16x16.png">
    <link rel="shortcut icon" href="<?= $home_url ?>/images/favicon.ico">

    <!--== GOOLFE FONTS ==-->
    <link rel="preload" href="<?= $home_url ?>/fonts/proximasoft-regular-webfont.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="<?= $home_url ?>/fonts/proximasoft-light-webfont.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="<?= $home_url ?>/fonts/proximasoft-medium-webfont.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="<?= $home_url ?>/fonts/proximasoft-bold-webfont.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="<?= $home_url ?>/fonts/proximasoft-semibold-webfont.woff2" as="font" type="font/woff2" crossorigin>

    <!--==== Style CSS ====-->
    <link rel='stylesheet' href='<?= $home_url ?>/css/styles.mind.css' type='text/css' media='all' />
    {{-- <link rel='stylesheet' href='<?= $home_url ?>/css/login.css' type='text/css' media='all' /> --}}
    <link rel='stylesheet' href='<?= $home_url ?>/css/main.css' type='text/css' media='all' />

     <!--==== Tabs CSS ===-->
     <link rel="stylesheet" href="{{ asset('public/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('public/css/loader.css')}}">
    
    <!-- Font-awsome -->
    <link rel="stylesheet" href="<?= $home_url ?>/css/fontawesome.css" type="text/css">

   
    @stack('styles')
</head>

<?php $page_name =  basename($_SERVER['PHP_SELF']);
if ($page_name == "company.php" || $page_name == "partners.php") {
    $header_type_class = "site-header-type-transparent";
} else {
    $header_type_class = "site-header-type-white";
}

if ($page_name == "business-and-qa.php") { ?>
    <link rel="stylesheet" href="<?= $home_url ?>/css/business-and-qa.css" type="text/css">
<?php } ?>
<!-- ==== Page(Home) Body Start ==== -->
<body class="virtual-fund-assis" data-ng-app="wp-app">
    <div id="loader-wrapper">
        <div id="loader"></div>
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <!-- ==== Page(Home) HEADER (FIXED) Start ==== -->
    <div id="site-header-container-fixed" class="<?= $header_type_class ?>">
        <div class="container nav-container">
            <div class="site-header">
                <a class="site-logo" href="<?= $home_url ?>" data-wpel-link="internal">
                    <img alt="Site Logo" src="<?= $home_url ?>/images/logo.png" />
                </a>
                <div class="site-menu-container" data-cy="site-menu-container-1">
                    <div class="site-menu">

                        <!-- ===  Main Menu 1 === -->
                        <ul id="menu-main-1" class="site-menu-list">
                            <li id="menu-item-93250" class="menu-item menu-item-type-post_type menu-item-object-covid_relief menu-item-has-children menu-link-93250 site-menu-link" data-cy="mobile-menu-link-covid-19-relief"><a href="<?= $home_url ?>/covid-relief/sba-paycheck-protection-program-loans" class="parent-link" data-cy="covid-19-relief" data-wpel-link="internal">COVID-19
                                    Relief<div class="site-submenu-caret"></div></a>
                                <div class="site-submenu">
                                    <h3><a href="#" class="site-submenu-header" data-wpel-link="internal">COVID-19
                                            Relief</a></h3>
                                    <ul class="site-submenu-list">
                                        <li id="menu-item-93252" class="menu-item menu-item-type-post_type menu-item-object-covid_relief menu-link-93252 site-submenu-link" data-cy="mobile-menu-link-sba-paycheck-protection-program-ppp"><a href="<?= $home_url ?>/covid-relief/sba-paycheck-protection-program-loans" data-cy="sba-paycheck-protection-program-ppp" data-wpel-link="internal">SBA Paycheck Protection Program (PPP)</a></li>
                                        <li id="menu-item-93253" class="menu-item menu-item-type-post_type menu-item-object-covid_relief menu-link-93253 site-submenu-link" data-cy="mobile-menu-link-93253"><a href="<?= $home_url ?>/covid-relief/apply-ppp-loan" data-cy="93253" data-wpel-link="internal">How to Apply for a PPP Loan</a></li>
                                        <li id="menu-item-93408" class="menu-item menu-item-type-post_type menu-item-object-covid_relief menu-link-93408 site-submenu-link" data-cy="mobile-menu-link-93408"><a href="<?= $home_url ?>/covid-relief/ppp-loan-forgiveness" data-cy="93408" data-wpel-link="internal">PPP Loan Forgiveness</a></li>
                                        <li id="menu-item-93599" class="menu-item menu-item-type-custom menu-item-object-custom menu-link-93599 site-submenu-link" data-cy="mobile-menu-link-ppp-loan-forgiveness-estimator"><a href="<?= $home_url ?>/covid-relief/ppp-loan-forgiveness-estimator" data-cy="ppp-loan-forgiveness-estimator" data-wpel-link="internal">PPP
                                                Loan Forgiveness Estimator</a></li>
                                        <li id="menu-item-93254" class="menu-item menu-item-type-post_type menu-item-object-covid_relief menu-link-93254 site-submenu-link" data-cy="mobile-menu-link-93254"><a href="<?= $home_url ?>/covid-relief/economic-injury-disaster-loans-eidl" data-cy="93254" data-wpel-link="internal">Economic Injury Disaster Loans
                                                (EIDL)</a></li>
                                        <li id="menu-item-93256" class="menu-item menu-item-type-post_type menu-item-object-covid_relief menu-link-93256 site-submenu-link" data-cy="mobile-menu-link-93256"><a href="<?= $home_url ?>/covid-relief/sba-coronavirus-loans" data-cy="93256" data-wpel-link="internal">SBA Coronavirus Loans
                                                (COVID-19)</a></li>

                                        <div class="submenu-padding"></div>
                                        <div class="submenu-pointer"></div>
                                    </ul>
                                </div>
                            </li>
                            <li id="menu-item-93259" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-link-93259 site-menu-link" data-cy="mobile-menu-link-about">
                                <a href="<?= $home_url ?>/company" class="parent-link" data-cy="about" data-wpel-link="internal">About
                                    <div class="site-submenu-caret"></div>
                                </a>
                                <div class="site-submenu">
                                    <h3>
                                        <a href="#" class="site-submenu-header" data-wpel-link="internal">About</a>
                                    </h3>
                                    <ul class="site-submenu-list">
                                        <li id="menu-item-93260" class="menu-item menu-item-type-post_type menu-item-object-page menu-link-93260 site-submenu-link" data-cy="mobile-menu-link-how-it-works">
                                            <a href="<?= $home_url ?>/about/how-it-works" data-cy="how-it-works" data-wpel-link="internal">How It Works</a>
                                        </li>
                                        <li id="menu-item-93261" class="menu-item menu-item-type-post_type menu-item-object-page menu-link-93261 site-submenu-link" data-cy="mobile-menu-link-about-us">
                                            <a href="<?= $home_url ?>/about/company" data-cy="about-us" data-wpel-link="internal">About Us</a>
                                        </li>
                                        <div class="submenu-padding"></div>
                                        <div class="submenu-pointer"></div>
                                    </ul>
                                </div>
                            </li>
                            <li id="menu-item-93264" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-link-93264 site-menu-link" data-cy="mobile-menu-link-loan-types"><a href="<?= $home_url ?>/business-loans" class="parent-link" data-cy="loan-types" data-wpel-link="internal">Loan Types<div class="site-submenu-caret"></div></a>
                                <div class="site-submenu">
                                    <h3><a href="#" class="site-submenu-header" data-wpel-link="internal">Loan Types</a>
                                    </h3>
                                    <ul class="site-submenu-list">
                                        <li id="menu-item-93265" class="menu-item menu-item-type-custom menu-item-object-custom menu-link-93265 site-submenu-link" data-cy="mobile-menu-link-credit-cards"><a href="<?= $home_url ?>/business-loans/business-credit-card" data-cy="credit-cards" data-wpel-link="internal">Credit Cards</a></li>
                                        <li id="menu-item-93266" class="menu-item menu-item-type-post_type menu-item-object-business_loans menu-link-93266 site-submenu-link" data-cy="mobile-menu-link-93266"><a href="<?= $home_url ?>/business-loans/line-of-credit" data-cy="93266" data-wpel-link="internal">Business Line of Credit</a></li>
                                        <li id="menu-item-93267" class="menu-item menu-item-type-post_type menu-item-object-business_loans menu-link-93267 site-submenu-link" data-cy="mobile-menu-link-sba-loans"><a href="<?= $home_url ?>/business-loans/sba-loans" data-cy="sba-loans" data-wpel-link="internal">SBA Loans</a></li>
                                        <li id="menu-item-93268" class="menu-item menu-item-type-post_type menu-item-object-business_loans menu-link-93268 site-submenu-link" data-cy="mobile-menu-link-93268"><a href="<?= $home_url ?>/business-loans/short-term-loans" data-cy="93268" data-wpel-link="internal">Short Term Loan</a></li>
                                        <li id="menu-item-93269" class="menu-item menu-item-type-post_type menu-item-object-business_loans menu-link-93269 site-submenu-link" data-cy="mobile-menu-link-93269"><a href="<?= $home_url ?>/business-loans/term-loans" data-cy="93269" data-wpel-link="internal">Business Term Loan</a></li>
                                        <li id="menu-item-93273" class="menu-item menu-item-type-post_type menu-item-object-business_loans menu-link-93273 site-submenu-link" data-cy="mobile-menu-link-93273"><a href="<?= $home_url ?>/business-loans/commercial-real-estate" data-cy="93273" data-wpel-link="internal">Commercial Mortgage</a></li>

                                        <li id="menu-item-93275" class="menu-item menu-item-type-post_type menu-item-object-business_loans menu-link-93275 site-submenu-link" data-cy="mobile-menu-link-93275"><a href="<?= $home_url ?>/business-loans/startup-loans" data-cy="93275" data-wpel-link="internal">Startup Loan</a></li>
                                        <div class="submenu-padding"></div>
                                        <div class="submenu-pointer"></div>
                                    </ul>
                                </div>
                            </li>
                            <li id="menu-item-93277" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-link-93277 site-menu-link" data-cy="mobile-menu-link-loan-calculators"><a href="<?= $home_url ?>/business-calculators" class="parent-link" data-cy="loan-calculators" data-wpel-link="internal">Loan
                                    Calculators<div class="site-submenu-caret"></div></a>
                                <div class="site-submenu">
                                    <h3><a href="#" class="site-submenu-header" data-wpel-link="internal">Loan
                                            Calculators</a></h3>
                                    <ul class="site-submenu-list">
                                        <li id="menu-item-93278" class="menu-item menu-item-type-post_type menu-item-object-calculators menu-link-93278 site-submenu-link" data-cy="mobile-menu-link-93278"><a href="<?= $home_url ?>/business-calculators/equipment-loan-calculator" data-cy="93278" data-wpel-link="internal">Equipment Loan Calculator</a>
                                        </li>
                                        <li id="menu-item-93279" class="menu-item menu-item-type-post_type menu-item-object-calculators menu-link-93279 site-submenu-link" data-cy="mobile-menu-link-sba-paycheck-protection-program-calculator"><a href="<?= $home_url ?>/business-calculators/sba-loan-calculator" data-cy="sba-paycheck-protection-program-calculator" data-wpel-link="internal">SBA PPP Loan Calculator</a></li>
                                        <li id="menu-item-93280" class="menu-item menu-item-type-post_type menu-item-object-calculators menu-link-93280 site-submenu-link" data-cy="mobile-menu-link-93280"><a href="<?= $home_url ?>/business-calculators/short-term-loan-calculator" data-cy="93280" data-wpel-link="internal">Short Term Loan Calculator</a>
                                        </li>
                                        <li id="menu-item-93281" class="menu-item menu-item-type-post_type menu-item-object-calculators menu-link-93281 site-submenu-link" data-cy="mobile-menu-link-93281"><a href="<?= $home_url ?>/business-calculators/startup-business-loan-calculator" data-cy="93281" data-wpel-link="internal">Startup Business Loan
                                                Calculator</a></li>
                                        <li id="menu-item-93283" class="menu-item menu-item-type-post_type menu-item-object-calculators menu-link-93283 site-submenu-link" data-cy="mobile-menu-link-93283"><a href="<?= $home_url ?>/business-calculators/business-term-loan-calculator" data-cy="93283" data-wpel-link="internal">Business Term Loan
                                                Calculator</a></li>
                                        <li id="menu-item-93285" class="menu-item menu-item-type-post_type menu-item-object-calculators menu-link-93285 site-submenu-link" data-cy="mobile-menu-link-93285"><a href="<?= $home_url ?>/business-calculators/business-line-credit-calculator" data-cy="93285" data-wpel-link="internal">Line of Credit Calculator</a>
                                        </li>
                                        <li id="menu-item-93286" class="menu-item menu-item-type-post_type menu-item-object-calculators menu-link-93286 site-submenu-link" data-cy="mobile-menu-link-93286"><a href="<?= $home_url ?>/business-calculators/business-credit-card-calculator" data-cy="93286" data-wpel-link="internal">Business Credit Card
                                                Calculator</a></li>
                                        <div class="submenu-padding"></div>
                                        <div class="submenu-pointer"></div>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                        <div class="cta-container">

                            <!-- ===  Remove After Big Menu Push! === -->
                            <a href="<?= $home_url ?>/basic-info" data-wpel-link="internal">
                                <button class="secondary-button alt">Get Loan Offers</button>
                            </a>
                        </div>
                        <div class="py-4 text-center d-lg-none d-md-block">
                            <a href="#" style="font-weight: 600;" data-wpel-link="internal">Login</a>
                        </div>
                    </div>
                    <a href="#" class="site-menu-close" data-cy="site-menu-close-2"> </a>
                </div>
                <a href="#" class="site-menu-toggle" data-cy="site-menu-toggle-1"></a>
                
    


                 <div class="header-right"> <a href="<?= $home_url ?>/tabs/owner-info" class="site-header-start-btn primary-button" data-wpel-link="internal">Get Loan Offers</a>
                
                 <ul class="nav navbar-nav admin-dash">
                    <li class="nav-item dropdown">
                    @guest
                     <a class="site-header-sign-in" href="<?= $home_url ?>/login" data-wpel-link="internal">Sign In</a>
                    @else
                  {{Auth::user()->first_name}} {{Auth::user()->last_name}}  
                    <div class="dropdown-menu dropdown-menu-right">
                            <a href="{{route('offers')}}" class="dropdown-item">Offers</a>
                            <a href="{{route('user.dashboard')}}" class="dropdown-item">Dashoard</a>
                            <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault();  document.getElementById('logout-form').submit();">{{ __('Sign Out') }}</a>
                    </div>
                    <form id="logout-form" action="{{route('logout')}}" method="POST" class="d-none">@csrf</form>
                    @endguest

                    </li>
                 </ul>
                </div> 


                <div class="clear"></div>
            </div>
            <div class="light-logo-preloader"></div>
        </div>
    </div>

    <!-- ==== Page(Home) HEADER Start ==== -->
    <div id="site-header-container" class="<?= $header_type_class ?>">
        <div class="ie-banner" hidden>
            <div class="container">
                <div class="ie-banner-content"> Lendio no longer supports Internet Explorer. To improve your browsing
                    experience, we recommend you use an alternative service such as <a href="https://www.google.com/chrome/" target="_blank" data-wpel-link="external" rel="external noopener noreferrer">Google Chrome</a>, <a href="https://www.microsoft.com/en-us/edge" target="_blank" data-wpel-link="external" rel="external noopener noreferrer">Microsoft Edge</a>, or <a href="https://www.mozilla.org/en-US/firefox/new/" target="_blank" data-wpel-link="external" rel="external noopener noreferrer">Firefox</a>.
                </div>
            </div>
        </div>
        <div class="container nav-container">
            <div class="site-header">
                <a class="site-logo" href="<?= $home_url ?>" data-wpel-link="internal">
                    <img alt="Site Logo" src="<?= $home_url ?>/images/logo.png" />
                </a>
                <div id="site-menu-container" data-cy="site-menu-container-2">
                    <div class="site-menu">
                        <ul id="menu-main-1" class="site-menu-list">
                            <li class="menu-item menu-item-type-post_type menu-item-object-covid_relief menu-item-has-children menu-link-93250 site-menu-link" data-cy="mobile-menu-link-covid-19-relief"><a href="<?= $home_url ?>/covid-relief/sba-paycheck-protection-program-loans" class="parent-link" data-cy="covid-19-relief" data-wpel-link="internal">COVID-19
                                    Relief<div class="site-submenu-caret"></div></a>
                                <div class="site-submenu">
                                    <h3><a href="#" class="site-submenu-header" data-wpel-link="internal">COVID-19
                                            Relief</a></h3>
                                    <ul class="site-submenu-list">
                                        <li class="menu-item menu-item-type-post_type menu-item-object-covid_relief menu-link-93252 site-submenu-link" data-cy="mobile-menu-link-sba-paycheck-protection-program-ppp"><a href="<?= $home_url ?>/covid-relief/sba-paycheck-protection-program-loans" data-cy="sba-paycheck-protection-program-ppp" data-wpel-link="internal">SBA Paycheck Protection Program (PPP)</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-covid_relief menu-link-93253 site-submenu-link" data-cy="mobile-menu-link-93253"><a href="<?= $home_url ?>/covid-relief/apply-ppp-loan" data-cy="93253" data-wpel-link="internal">How to Apply for a PPP Loan</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-covid_relief menu-link-93408 site-submenu-link" data-cy="mobile-menu-link-93408"><a href="<?= $home_url ?>/covid-relief/ppp-loan-forgiveness" data-cy="93408" data-wpel-link="internal">PPP Loan Forgiveness</a></li>
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-link-93599 site-submenu-link" data-cy="mobile-menu-link-ppp-loan-forgiveness-estimator"><a href="<?= $home_url ?>/covid-relief/ppp-loan-forgiveness-estimator" data-cy="ppp-loan-forgiveness-estimator" data-wpel-link="internal">PPP
                                                Loan Forgiveness Estimator</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-covid_relief menu-link-93254 site-submenu-link" data-cy="mobile-menu-link-93254"><a href="<?= $home_url ?>/covid-relief/economic-injury-disaster-loans-eidl" data-cy="93254" data-wpel-link="internal">Economic Injury Disaster Loans
                                                (EIDL)</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-covid_relief menu-link-93256 site-submenu-link" data-cy="mobile-menu-link-93256"><a href="<?= $home_url ?>/covid-relief/sba-coronavirus-loans" data-cy="93256" data-wpel-link="internal">SBA Coronavirus Loans
                                                (COVID-19)</a></li>
                                        <div class="submenu-padding"></div>
                                        <div class="submenu-pointer"></div>
                                    </ul>
                                </div>
                            </li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-link-93259 site-menu-link" data-cy="mobile-menu-link-about">
                                <a href="<?= $home_url ?>/company" class="parent-link" data-cy="about" data-wpel-link="internal">About
                                    <div class="site-submenu-caret"></div>
                                </a>
                                <div class="site-submenu">
                                    <h3>
                                        <a href="#" class="site-submenu-header" data-wpel-link="internal">About</a>
                                    </h3>
                                    <ul class="site-submenu-list">
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-link-93260 site-submenu-link" data-cy="mobile-menu-link-how-it-works">
                                            <a href="<?= $home_url ?>/about/how-it-works" data-cy="how-it-works" data-wpel-link="internal">How It Works</a>
                                        </li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-link-93261 site-submenu-link" data-cy="mobile-menu-link-about-us">
                                            <a href="<?= $home_url ?>/about/company" data-cy="about-us" data-wpel-link="internal">About Us</a>
                                        </li>
                                        <div class="submenu-padding"></div>
                                        <div class="submenu-pointer"></div>
                                    </ul>
                                </div>
                            </li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-link-93264 site-menu-link" data-cy="mobile-menu-link-loan-types"><a href="<?= $home_url ?>/business-loans" class="parent-link" data-cy="loan-types" data-wpel-link="internal">Loan Types<div class="site-submenu-caret"></div></a>
                                <div class="site-submenu">
                                    <h3><a href="#" class="site-submenu-header" data-wpel-link="internal">Loan Types</a>
                                    </h3>
                                    <ul class="site-submenu-list">
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-link-93265 site-submenu-link" data-cy="mobile-menu-link-credit-cards"><a href="<?= $home_url ?>/business-loans/business-credit-card" data-cy="credit-cards" data-wpel-link="internal">Credit Cards</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-business_loans menu-link-93266 site-submenu-link" data-cy="mobile-menu-link-93266"><a href="<?= $home_url ?>/business-loans/line-of-credit" data-cy="93266" data-wpel-link="internal">Business Line of Credit</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-business_loans menu-link-93267 site-submenu-link" data-cy="mobile-menu-link-sba-loans"><a href="<?= $home_url ?>/business-loans/sba-loans" data-cy="sba-loans" data-wpel-link="internal">SBA Loans</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-business_loans menu-link-93268 site-submenu-link" data-cy="mobile-menu-link-93268"><a href="<?= $home_url ?>/business-loans/short-term-loans" data-cy="93268" data-wpel-link="internal">Short Term Loan</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-business_loans menu-link-93269 site-submenu-link" data-cy="mobile-menu-link-93269"><a href="<?= $home_url ?>/business-loans/term-loans" data-cy="93269" data-wpel-link="internal">Business Term Loan</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-business_loans menu-link-93273 site-submenu-link" data-cy="mobile-menu-link-93273"><a href="<?= $home_url ?>/business-loans/commercial-real-estate" data-cy="93273" data-wpel-link="internal">Commercial Mortgage</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-business_loans menu-link-93275 site-submenu-link" data-cy="mobile-menu-link-93275"><a href="<?= $home_url ?>/business-loans/startup-loans" data-cy="93275" data-wpel-link="internal">Startup Loan</a></li>
                                        <div class="submenu-padding"></div>
                                        <div class="submenu-pointer"></div>
                                    </ul>
                                </div>
                            </li>
                            <li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-link-93277 site-menu-link" data-cy="mobile-menu-link-loan-calculators"><a href="<?= $home_url ?>/business-calculators" class="parent-link" data-cy="loan-calculators" data-wpel-link="internal">Loan
                                    Calculators<div class="site-submenu-caret"></div></a>
                                <div class="site-submenu">
                                    <h3><a href="#" class="site-submenu-header" data-wpel-link="internal">Loan
                                            Calculators</a></h3>
                                    <ul class="site-submenu-list">
                                        <li class="menu-item menu-item-type-post_type menu-item-object-calculators menu-link-93278 site-submenu-link" data-cy="mobile-menu-link-93278"><a href="<?= $home_url ?>/business-calculators/equipment-loan-calculator" data-cy="93278" data-wpel-link="internal">Equipment Loan Calculator</a>
                                        </li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-calculators menu-link-93279 site-submenu-link" data-cy="mobile-menu-link-sba-paycheck-protection-program-calculator"><a href="<?= $home_url ?>/business-calculators/sba-loan-calculator" data-cy="sba-paycheck-protection-program-calculator" data-wpel-link="internal">SBA PPP Loan Calculator</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-calculators menu-link-93280 site-submenu-link" data-cy="mobile-menu-link-93280"><a href="<?= $home_url ?>/business-calculators/short-term-loan-calculator" data-cy="93280" data-wpel-link="internal">Short Term Loan Calculator</a>
                                        </li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-calculators menu-link-93281 site-submenu-link" data-cy="mobile-menu-link-93281"><a href="<?= $home_url ?>/business-calculators/startup-business-loan-calculator" data-cy="93281" data-wpel-link="internal">Startup Business Loan
                                                Calculator</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-calculators menu-link-93283 site-submenu-link" data-cy="mobile-menu-link-93283"><a href="<?= $home_url ?>/business-calculators/business-term-loan-calculator" data-cy="93283" data-wpel-link="internal">Business Term Loan
                                                Calculator</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-calculators menu-link-93285 site-submenu-link" data-cy="mobile-menu-link-93285"><a href="<?= $home_url ?>/business-calculators/business-line-credit-calculator" data-cy="93285" data-wpel-link="internal">Line of Credit Calculator</a>
                                        </li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-calculators menu-link-93286 site-submenu-link" data-cy="mobile-menu-link-93286"><a href="<?= $home_url ?>/business-calculators/business-credit-card-calculator" data-cy="93286" data-wpel-link="internal">Business Credit Card
                                                Calculator</a></li>
                                        <div class="submenu-padding"></div>
                                        <div class="submenu-pointer"></div>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                        <div class="cta-container"> <a href="<?= $home_url ?>/basic-info" data-wpel-link="internal"><button class="primary-button">Get Loan Offers</button></a>
                        </div>
                    </div> <a href="#" class="site-menu-close" data-cy="site-menu-close-2"> </a>
                </div>
                <a href="#" class="site-menu-toggle" data-cy="site-menu-toggle-2"></a>
               
                <div class="header-right">
                     <a href="<?= $home_url ?>/tabs/owner-info" class="site-header-start-btn primary-button" data-wpel-link="internal">Get Loan Offers</a>
                
                 <ul class="nav navbar-nav admin-dash">
                    <li class="nav-item dropdown">
                    @guest
                     <a class="site-header-sign-in" href="<?= $home_url ?>/tabs/login" data-wpel-link="internal">Sign In</a>
                    @else
                  {{Auth::user()->first_name}} {{Auth::user()->last_name}}  
                    <div class="dropdown-menu dropdown-menu-right">
                            <a href="{{route('offers')}}" class="dropdown-item">Offers</a>
                            <a href="{{route('user.dashboard')}}" class="dropdown-item">Dashoard</a>
                            <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault();  document.getElementById('logout-form').submit();">{{ __('Sign Out') }}</a>
                    </div>
                    <form id="logout-form" action="{{route('logout')}}" method="POST" class="d-none">@csrf</form>
                    @endguest

                    </li>
                 </ul>
                </div> 

            <div class="clear"></div>
            </div>
            <div class="light-logo-preloader"></div>
        </div>
    </div>
    <!-- ==== Page(Home)  yellow-bar Start ==== -->
    <div class="hero position-relative" style="background: linear-gradient(90deg, #ff3366 0%, #fe9b02 100%); color: white;">
        <div class="site-close"><i class="fa fa-times"></i></div>
        <div class="container p-lg-0">
            <div class="py-lg-2 row hero-row">
                <div class="hero-responsive col-12 text-left text-md-center align-middle mt-4 my-lg-2">
                    The President has signed a relief bill
                    that includes $284 billion for
                    Paycheck Protection Program (PPP)
                    loans. Virtual Fund Assist will submit
                    completed, eligible applications to
                    PPP lenders once the program
                    officially resumes in January 2021.
                </div>
            </div>
        </div>
    </div>

    <!-- Tabs -->
    <section id="tabs">
        <div class="container">
            <div class="row"> 
                @if(!(request()->is('login-pathway')) && !(request()->is('login')) && !(request()->is('/')))
                <div class="col col-12">
                    <nav>
                        <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link {{ (request()->is('basic-info')) ? 'active' : '' }}" id="nav-basicinfo-tab" href="{{route('basicinfo')}}" role="tab" aria-controls="nav-basicinfo" aria-selected="true">Basic Info</a>
                            <a class="nav-item nav-link {{ (request()->is('owner-info')) ? 'active' : '' }}" id="nav-ownerinfo-tab" href="{{route('ownerinfo')}}" role="tab" aria-controls="nav-ownerinfo" aria-selected="false">Owner Info</a>
                            <a class="nav-item nav-link {{ (request()->is('business-info')) ? 'active' : '' }}" id="nav-businessinfo-tab" href="{{route('businessinfo')}}" role="tab" aria-controls="nav-businessinfo" aria-selected="false">Business Info</a>
                            <a class="nav-item nav-link {{ (request()->is('documents')) ? 'active' : '' }}" id="nav-documents-tab" href="{{route('documents')}}" role="tab" aria-controls="nav-documents" aria-selected="false">Documents</a>
                            <a class="nav-item nav-link {{ (request()->is('finalform')) ? 'active' : '' }}" id="nav-submit-tab" href="{{route('finalform')}}" role="tab" aria-controls="nav-submit" aria-selected="false">Submit</a>
                        </div>
                    </nav>
                </div>
                @endif

                <div class="col col-12">
                    <div class="tab-content py-3 px-3 px-sm-0 mt-5" id="nav-tabContent">

                        @yield('content')
                        <!-- end -->
                    </div>
                </div>

            </div>
        </div>
    </section>

    <!--==== TABS FOOTER  ===-->                    
  <?php include('lendo/footer.php'); ?>

    <!--==== JAVA SCRIPT ===-->
    <script type='text/javascript' src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js">
    </script>
    <script>
        $(document).ready(function() {
            // Check/uncheck checkboxes clicking on its parent div
            $(document).on('click', ".checkbox_option", function() {
                var chk = $(this).find("input[type='checkbox']");
                if (chk.is(":checked") == false) {
                    chk.prop("checked", true);
                    $(this).addClass('pill-active');
                } else {
                    chk.prop("checked", false);
                    $(this).removeClass('pill-active');
                }
            });
            $("#loader-wrapper").css("display", "none");
            $(document).ajaxStart(function() {
                $("#loader-wrapper").css("display", "block");
            });

            $(document).ajaxComplete(function() {
                $("#loader-wrapper").css("display", "none");
            });


            function formatPhoneNumber(phoneNumberString) {
                // REFREENCE https://stackoverflow.com/questions/8358084/regular-expression-to-reformat-a-us-phone-number-in-javascript
                var cleaned = ('' + phoneNumberString).replace(/\D/g, '')
                var match = cleaned.match(/^(1|)?(\d{3})(\d{3})(\d{4})$/)
                if (match) {
                    var intlCode = (match[1] ? '+1 ' : '')
                    $("#error_mobile_number").hide();
                    return [intlCode, '(', match[2], ') ', match[3], '-', match[4]].join('')
                }
                $("#error_mobile_number").show();
                $("#error_mobile_number").text('Invalid,correct format');
                return null
            }

            $('#mobile_number').on('focusout', function() {
                var formattedNumber = formatPhoneNumber($(this).val());
                $("#mobile_number").val(formattedNumber);
            });

        });
        //jQuery time
        var current_fs, next_fs, previous_fs;
        var left, opacity, scale;
        var animating;

        $(".next").click(function() {
            if (animating) return false;
            animating = true;

            current_fs = $(this).parent();
            next_fs = $(this).parent().next();

            next_fs.show();
            //hide the current fieldset with style
            current_fs.animate({
                opacity: 0
            }, {
                step: function(now, mx) {
                    //as the opacity of current_fs reduces to 0 - stored in "now"
                    //1. scale current_fs down to 80%
                    scale = 1 - (1 - now) * 0.2;
                    //2. bring next_fs from the right(50%)
                    left = (now * 50) + "%";
                    //3. increase opacity of next_fs to 1 as it moves in
                    opacity = 1 - now;
                    current_fs.css({
                        'transform': 'scale(' + scale + ')',
                        'position': 'absolute'
                    });
                    next_fs.css({
                        'left': left,
                        'opacity': opacity
                    });
                },
                duration: 800,
                complete: function() {
                    current_fs.hide();
                    animating = false;
                },
                //this comes from the custom easing plugin
                easing: 'easeInOutBack'
            });
        });

        $(".previous").click(function() {
            if (animating) return false;
            animating = true;

            current_fs = $(this).parent().parent();
            previous_fs = $(this).parent().parent().prev();

            //de-activate current step on progressbar
            //$("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

            //show the previous fieldset
            previous_fs.show();
            //hide the current fieldset with style
            current_fs.animate({
                opacity: 0
            }, {
                step: function(now, mx) {
                    //as the opacity of current_fs reduces to 0 - stored in "now"
                    //1. scale previous_fs from 80% to 100%
                    scale = 0.8 + (1 - now) * 0.2;
                    //2. take current_fs to the right(50%) - from 0%
                    left = ((1 - now) * 50) + "%";
                    //3. increase opacity of previous_fs to 1 as it moves in
                    opacity = 1 - now;
                    current_fs.css({
                        'left': left
                    });
                    previous_fs.css({
                        'transform': 'scale(' + scale + ')',
                        'opacity': opacity
                    });
                },
                duration: 800,
                complete: function() {
                    current_fs.hide();
                    animating = false;
                },
                //this comes from the custom easing plugin
                easing: 'easeInOutBack'
            });
        });

        $(".submit").click(function() {
            return false;
        })
    </script>

    @stack('scripts')
@extends('layouts.master')
@push('styles')


<!-- CSS -->
<link rel="stylesheet" type="text/css" href="{{asset('public/dist/select2/css/select2.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/dist/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
<style>
    .bootstrap-select .bs-ok-default::after {
        width: 0.3em;
        height: 0.6em;
        border-width: 0 0.1em 0.1em 0;
        transform: rotate(45deg) translateY(0.5rem);
    }

    .btn.dropdown-toggle:focus {
        outline: none !important;
    }
</style>
@endpush
@section('content')
<!-- ==== (SUBMIT) TAB (CONTENT-4) START FROM HERE === -->
<div class="tab-pane fade active show" id="nav-submit" role="tabpanel" aria-labelledby="nav-submit-tab">
    @if(session('flash_message'))
    <div class="alert alert-primary" role="alert">
        {{session('flash_message')}}
    </div>
    @endif 

    <section class="align-self-center app-page">
        <h3 class="text-center">Review and Submit</h3>
        <!--  <form class="py-4 text-center" action="" method="get">
             @csrf -->
        <div class="py-4 text-center">
            <button type="button" onclick="location.href ='{{route("update.allform")}}'" class="btn btn-submit-offer text-center">Submit for Offers</button>
        </div>
        <!--  </form> -->
        <div class="question-group-header">
            <h4 class="text-truncate">Owner Information</h4>
            <a href="#" type="button" class="edit-button ml-2 d-sm-inline d-none" data-toggle="modal" data-target="#exampleModal">
                <svg class="svg-inline--fa fa-pencil fa-w-16" aria-hidden="true" data-fa-processed="" data-prefix="far" data-icon="pencil" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                    <path fill="currentColor" d="M491.609 73.625l-53.861-53.839c-26.378-26.379-69.076-26.383-95.46-.001L24.91 335.089.329 484.085c-2.675 16.215 11.368 30.261 27.587 27.587l148.995-24.582 315.326-317.378c26.33-26.331 26.581-68.879-.628-96.087zM120.644 302l170.259-169.155 88.251 88.251L210 391.355V350h-48v-48h-41.356zM82.132 458.132l-28.263-28.263 12.14-73.587L84.409 338H126v48h48v41.59l-18.282 18.401-73.586 12.141zm378.985-319.533l-.051.051-.051.051-48.03 48.344-88.03-88.03 48.344-48.03.05-.05.05-.05c9.147-9.146 23.978-9.259 33.236-.001l53.854 53.854c9.878 9.877 9.939 24.549.628 33.861z">
                    </path>
                </svg>
                Edit
            </a>
        </div>
        <div class="d-flex flex-wrap justify-content-between mb-5">
            <div class="col-12 col-md-6 px-0 py-2 d-flex flex-wrap align-content-start border-bottom border-gray-200 question-wrap">
                <span class="font-weight-bold col-12 px-0 mx-0">Owner Address</span>
                <span class="col-12 px-0 mx-0">
                    <div class="question-value"><br>{{@$owner_info['owner_information']['full_address']}}</div>
                </span>
            </div>
            <div class="col-12 col-md-6 px-0 py-2 d-flex flex-wrap align-content-start border-bottom border-gray-200 question-wrap">
                <span class="font-weight-bold col-12 px-0 mx-0">Social Security Number</span>
                <span class="col-12 px-0 mx-0">
                    <div class="question-value">{{$owner_info['owner_credit_verification']['social_security_number']}}
                    </div>
                </span>
            </div>
            <div class="col-12 col-md-6 px-0 py-2 d-flex flex-wrap align-content-start border-bottom border-gray-200 question-wrap">
                <span class="font-weight-bold col-12 px-0 mx-0">Owner Date of Birth</span>
                <span class="col-12 px-0 mx-0">
                    <div class="question-value">{{@$owner_info['owner_credit_verification']['date_of_birth']}}</div>
                </span>
            </div>
            <div class="col-12 col-md-6 px-0 py-2 d-flex flex-wrap align-content-start border-bottom border-gray-200 question-wrap">
                <span class="font-weight-bold col-12 px-0 mx-0">Annual Personal Income</span>
                <span class="col-12 px-0 mx-0">
                    <div class="question-value">{{@$basic_info['annul_personal_income']['annul_personal_income']}}</div>
                </span>
            </div>
            <div class="col-12 col-md-6 px-0 py-2 d-flex flex-wrap align-content-start border-bottom border-gray-200 question-wrap">
                <span class="font-weight-bold col-12 px-0 mx-0">First Name</span>
                <span class="col-12 px-0 mx-0">
                    <div class="question-value">{{$owner_info['load_prfile']['first_name']}}</div>
                </span>
            </div>
            <div class="col-12 col-md-6 px-0 py-2 d-flex flex-wrap align-content-start border-bottom border-gray-200 question-wrap">
                <span class="font-weight-bold col-12 px-0 mx-0">Last Name </span>
                <span class="col-12 px-0 mx-0">
                    <div class="question-value">{{$owner_info['load_prfile']['last_name']}}</div>
                </span>
            </div>
            <div class="col-12 col-md-6 px-0 py-2 d-flex flex-wrap align-content-start border-bottom border-gray-200 question-wrap">
                <span class="font-weight-bold col-12 px-0 mx-0">What is your percentage of ownership
                    (direct or indirect)?</span>
                <span class="col-12 px-0 mx-0">
                    <div class="question-value">{{@$basic_info['business_percentage']['business_percentage']}}%</div>
                </span>
            </div>
        </div>

        <div class="question-group-header">
            <h4 class="text-truncate">Business Information</h4>
            <a href="#" type="button" class="edit-button ml-2 d-sm-inline d-none" data-toggle="modal" data-target="#BusinessModal">
                <svg class="svg-inline--fa fa-pencil fa-w-16" aria-hidden="true" data-fa-processed="" data-prefix="far" data-icon="pencil" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                    <path fill="currentColor" d="M491.609 73.625l-53.861-53.839c-26.378-26.379-69.076-26.383-95.46-.001L24.91 335.089.329 484.085c-2.675 16.215 11.368 30.261 27.587 27.587l148.995-24.582 315.326-317.378c26.33-26.331 26.581-68.879-.628-96.087zM120.644 302l170.259-169.155 88.251 88.251L210 391.355V350h-48v-48h-41.356zM82.132 458.132l-28.263-28.263 12.14-73.587L84.409 338H126v48h48v41.59l-18.282 18.401-73.586 12.141zm378.985-319.533l-.051.051-.051.051-48.03 48.344-88.03-88.03 48.344-48.03.05-.05.05-.05c9.147-9.146 23.978-9.259 33.236-.001l53.854 53.854c9.878 9.877 9.939 24.549.628 33.861z">
                    </path>
                </svg>
                Edit
            </a>
        </div>
        <div class="d-flex flex-wrap justify-content-between mb-5">
            <div class="col-12 col-md-6 px-0 py-2 d-flex flex-wrap align-content-start border-bottom border-gray-200 question-wrap">
                <span class="font-weight-bold col-12 px-0 mx-0">Business Address</span>
                <span class="col-12 px-0 mx-0">
                    <div class="question-value"><br>{{@$business_info['basic_info']['business_address']}}</div>
                </span>
            </div>
            <div class="col-12 col-md-6 px-0 py-2 d-flex flex-wrap align-content-start border-bottom border-gray-200 question-wrap">
                <span class="font-weight-bold col-12 px-0 mx-0"> Business Name</span>
                <span class="col-12 px-0 mx-0">
                    <div class="question-value">{{$owner_info['load_prfile']['business_name']}}</div>
                </span>
            </div>
            <div class="col-12 col-md-6 px-0 py-2 d-flex flex-wrap align-content-start border-bottom border-gray-200 question-wrap">
                <span class="font-weight-bold col-12 px-0 mx-0">Business Phone Number</span>
                <span class="col-12 px-0 mx-0">
                    <div class="question-value">{{$owner_info['load_prfile']['mobile_number']}}</div>
                </span>
            </div>
            <div class="col-12 col-md-6 px-0 py-2 d-flex flex-wrap align-content-start border-bottom border-gray-200 question-wrap">
                <span class="font-weight-bold col-12 px-0 mx-0">Annual Profits</span>
                <span class="col-12 px-0 mx-0">
                    <div class="question-value">{{@$business_info['final_questions']['annual_personal_income']}}</div>
                </span>
            </div>
            <div class="col-12 col-md-6 px-0 py-2 d-flex flex-wrap align-content-start border-bottom border-gray-200 question-wrap">
                <span class="font-weight-bold col-12 px-0 mx-0">Do you invoice your customers?
                </span> <span class="col-12 px-0 mx-0">
                    <div class="question-value">{{@$basic_info["is_invoice_customers"]["is_invoice_customers"]}}</div>
                </span>
            </div>
            <div class="col-12 col-md-6 px-0 py-2 d-flex flex-wrap align-content-start border-bottom border-gray-200 question-wrap">
                <span class="font-weight-bold col-12 px-0 mx-0">Is your business a non-profit?</span>
                <span class="col-12 px-0 mx-0">
                    <div class="question-value">{{@$basic_info["is_business_non_profit"]["is_business_non_profit"]}}
                    </div>
                </span>
            </div>
            <div class="col-12 col-md-6 px-0 py-2 d-flex flex-wrap align-content-start border-bottom border-gray-200 question-wrap">
                <span class="font-weight-bold col-12 px-0 mx-0">Do you use accounting software?</span>
                <span class="col-12 px-0 mx-0">
                    <div class="question-value">{{@$basic_info["is_use_accounting_software"]["is_use_accounting_software"]}}</div>
                </span>
            </div>
            <div class="col-12 col-md-6 px-0 py-2 d-flex flex-wrap align-content-start border-bottom border-gray-200 question-wrap">
                <span class="font-weight-bold col-12 px-0 mx-0">Entity Type</span>
                <span class="col-12 px-0 mx-0">
                    <div class="question-value">{{@$business_info['basic_info']['business_entity']}}</div>
                </span>
            </div>
            <div class="col-12 col-md-6 px-0 py-2 d-flex flex-wrap align-content-start border-bottom border-gray-200 question-wrap">
                <span class="font-weight-bold col-12 px-0 mx-0">Is your business a franchise?</span>
                <span class="col-12 px-0 mx-0">
                    <div class="question-value">{{@$basic_info["is_business_franchise"]["is_business_franchise"]}}
                    </div>
                </span>
            </div>
            <div class="col-12 col-md-6 px-0 py-2 d-flex flex-wrap align-content-start border-bottom border-gray-200 question-wrap">
                <span class="font-weight-bold col-12 px-0 mx-0">Number of Employees (Including
                    Owners)</span>
                <span class="col-12 px-0 mx-0">
                    <div class="question-value">{{@$business_info['basic_info']['total_employees']}}</div>
                </span>
            </div>
            <div class="col-12 col-md-6 px-0 py-2 d-flex flex-wrap align-content-start border-bottom border-gray-200 question-wrap">
                <span class="font-weight-bold col-12 px-0 mx-0">Is this a seasonal business?</span>
                <span class="col-12 px-0 mx-0">
                    <div class="question-value">{{@$basic_info["is_seasonal"]["is_seasonal"]}}</div>
                </span>
            </div>
            <div class="col-12 col-md-6 px-0 py-2 d-flex flex-wrap align-content-start border-bottom border-gray-200 question-wrap">
                <span class="font-weight-bold col-12 px-0 mx-0">How much do you need?</span>
                <span class="col-12 px-0 mx-0">
                    <div class="question-value">{{@$basic_info['financial_amount']['financial_amount']}}</div>
                </span>
            </div>
            <div class="col-12 col-md-6 px-0 py-2 d-flex flex-wrap align-content-start border-bottom border-gray-200 question-wrap">
                <span class="font-weight-bold col-12 px-0 mx-0">Business Start Date</span>
                <span class="col-12 px-0 mx-0">
                    <div class="question-value">{{@$basic_info['business_start_date']['business_start_date']}}</div>
                </span>
            </div>
            <div class="col-12 col-md-6 px-0 py-2 d-flex flex-wrap align-content-start border-bottom border-gray-200 question-wrap">
                <span class="font-weight-bold col-12 px-0 mx-0">Average Monthly Sales </span>
                <span class="col-12 px-0 mx-0">
                    <div class="question-value">{{@$basic_info['average_month_revenue']['average_month_revenue']}}
                    </div>
                </span>
            </div>
            <div class="col-12 col-md-6 px-0 py-2 d-flex flex-wrap align-content-start border-bottom border-gray-200 question-wrap">
                <span class="font-weight-bold col-12 px-0 mx-0">Loan Purpose</span>
                <span class="col-12 px-0 mx-0">
                    <div class="question-value">@if(!empty($basic_info['loan_purpose']['loan_purpose'])) {{implode(",",@$basic_info['loan_purpose']['loan_purpose'])}}@endif</div>
                </span>
            </div>
            <div class="col-12 col-md-6 px-0 py-2 d-flex flex-wrap align-content-start border-bottom border-gray-200 question-wrap">
                <span class="font-weight-bold col-12 px-0 mx-0">Which best describes your business?</span>
                <span class="col-12 px-0 mx-0">
                    <div class="question-value">
                        @if(!empty($basic_info['business_description']['business_description'])){{implode(',',@$basic_info['business_description']['business_description'])}}
                        @endif</div>
                </span>
            </div>
            <div class="col-12 col-md-6 px-0 py-2 d-flex flex-wrap align-content-start border-bottom border-gray-200 question-wrap">
                <span class="font-weight-bold col-12 px-0 mx-0">Do you accept credit cards?</span>
                <span class="col-12 px-0 mx-0">
                    <div class="question-value">{{@$basic_info["is_accept_credit_cards"]["is_accept_credit_cards"]}}
                    </div>
                </span>
            </div>
            <div class="col-12 col-md-6 px-0 py-2 d-flex flex-wrap align-content-start border-bottom border-gray-200 question-wrap">
                <span class="font-weight-bold col-12 px-0 mx-0">Business State</span>
                <span class="col-12 px-0 mx-0">
                    <div class="question-value">{{@$business_info['basic_info']['state_id']}}</div>
                </span>
            </div>
        </div>
        <div class="question-group-header">
            <h4>Documents</h4>
            <a href="{{route('documents')}}" class="edit-button ml-2">
                <svg class="svg-inline--fa fa-pencil fa-w-16" aria-hidden="true" data-fa-processed="" data-prefix="far" data-icon="pencil" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                    <path fill="currentColor" d="M491.609 73.625l-53.861-53.839c-26.378-26.379-69.076-26.383-95.46-.001L24.91 335.089.329 484.085c-2.675 16.215 11.368 30.261 27.587 27.587l148.995-24.582 315.326-317.378c26.33-26.331 26.581-68.879-.628-96.087zM120.644 302l170.259-169.155 88.251 88.251L210 391.355V350h-48v-48h-41.356zM82.132 458.132l-28.263-28.263 12.14-73.587L84.409 338H126v48h48v41.59l-18.282 18.401-73.586 12.141zm378.985-319.533l-.051.051-.051.051-48.03 48.344-88.03-88.03 48.344-48.03.05-.05.05-.05c9.147-9.146 23.978-9.259 33.236-.001l53.854 53.854c9.878 9.877 9.939 24.549.628 33.861z">
                    </path>
                </svg>
                <span class="d-sm-inline d-none"> Edit</span>
            </a>
        </div>

        <div class="d-flex flex-wrap justify-content-between mb-5">
            @php
            $imageArray = array('png','jpe','jpeg','jpg','gif','bmp');
            @endphp
            @if(!empty($docsInDb) && $docsInDb->drivers_license_front != '')
            @php
            $realpath = $docsInDb->drivers_license_front;
            $realpathArray = explode(".",$realpath);
            $fileExpenstion = end($realpathArray);
            @endphp
            <div class="col-12 col-md-6 px-0 py-2 flex-wrap align-content-start border-bottom border-gray-200">
                <span class="font-weight-bold col-12 px-0 mx-0">
                    @if(in_array(strtolower($fileExpenstion),$imageArray))
                    <a href="{{asset('storage/app/'.$docsInDb->drivers_license_front)}}" target="_blank"><img width="100" src="{{asset('storage/app/'.$docsInDb->drivers_license_front)}}" /></a>
                    @else
                    <a href="{{asset('storage/app/'.$docsInDb->drivers_license_front)}}" target="_blank">{{$docsInDb->drivers_license_front}}</a>
                    @endif
                </span>
            </div>
            @endif

            @if(!empty($docsInDb) && $docsInDb->drivers_license_back != '')
            @php
            $realpath = $docsInDb->drivers_license_back;
            $realpathArray = explode(".",$realpath);
            $fileExpenstion = end($realpathArray);
            @endphp
            <div class="col-12 col-md-6 px-0 py-2 flex-wrap align-content-start border-bottom border-gray-200">
                <span class="font-weight-bold col-12 px-0 mx-0">
                    @if(in_array(strtolower($fileExpenstion),$imageArray))
                    <a href="{{asset('storage/app/'.$docsInDb->drivers_license_back)}}" target="_blank"><img width="100" src="{{asset('storage/app/'.$docsInDb->drivers_license_back)}}" /></a>
                    @else
                    <a href="{{asset('storage/app/'.$docsInDb->drivers_license_back)}}" target="_blank">{{$docsInDb->drivers_license_back}}</a>
                    @endif
                </span>
            </div>
            @endif

            @if(!empty($docsInDb) && $docsInDb->voided_check != '')
            @php
            $realpath = $docsInDb->voided_check;
            $realpathArray = explode(".",$realpath);
            $fileExpenstion = end($realpathArray);
            @endphp
            <div class="col-12 col-md-6 px-0 py-2 flex-wrap align-content-start border-bottom border-gray-200">
                <span class="font-weight-bold col-12 px-0 mx-0">
                    @if(in_array(strtolower($fileExpenstion),$imageArray))
                    <a href="{{asset('storage/app/'.$docsInDb->voided_check)}}" target="_blank"><img width="100" src="{{asset('storage/app/'.$docsInDb->voided_check)}}" /></a>
                    @else
                    <a href="{{asset('storage/app/'.$docsInDb->voided_check)}}" target="_blank">{{$docsInDb->voided_check}}</a>
                    @endif
                </span>
            </div>
            @endif




            @if(!empty($docsInDb) && $docsInDb->schedule_c != '')
            @php
            $realpath = $docsInDb->schedule_c;
            $realpathArray = explode(".",$realpath);
            $fileExpenstion = end($realpathArray);
            @endphp
            <div class="col-12 col-md-6 px-0 py-2 flex-wrap align-content-start border-bottom border-gray-200">
                <span class="font-weight-bold col-12 px-0 mx-0">
                    @if(in_array(strtolower($fileExpenstion),$imageArray))
                    <a href="{{asset('storage/app/'.$docsInDb->schedule_c)}}" target="_blank"><img width="100" src="{{asset('storage/app/'.$docsInDb->schedule_c)}}" /></a>
                    @else
                    <a href="{{asset('storage/app/'.$docsInDb->schedule_c)}}" target="_blank">{{$docsInDb->schedule_c}}</a>
                    @endif
                </span>
            </div>
            @endif



            @if(!empty($docsInDb) && $docsInDb->bank_statement != '')
            @php
            $realpath = $docsInDb->bank_statement;
            $realpathArray = explode(".",$realpath);
            $fileExpenstion = end($realpathArray);
            @endphp
            <div class="col-12 col-md-6 px-0 py-2 flex-wrap align-content-start border-bottom border-gray-200">
                <span class="font-weight-bold col-12 px-0 mx-0">
                    @if(in_array(strtolower($fileExpenstion),$imageArray))
                    <a href="{{asset('storage/app/'.$docsInDb->bank_statement)}}" target="_blank"><img width="100" src="{{asset('storage/app/'.$docsInDb->bank_statement)}}" /></a>
                    @else
                    <a href="{{asset('storage/app/'.$docsInDb->bank_statement)}}" target="_blank">{{$docsInDb->bank_statement}}</a>
                    @endif
                </span>
            </div>
            @endif

            @if(!empty($docsInDb) && $docsInDb->business_formation_doc != '')
            @php
            $realpath = $docsInDb->business_formation_doc;
            $realpathArray = explode(".",$realpath);
            $fileExpenstion = end($realpathArray);
            @endphp
            <div class="col-12 col-md-6 px-0 py-2 flex-wrap align-content-start border-bottom border-gray-200">
                <span class="font-weight-bold col-12 px-0 mx-0">
                    @if(in_array(strtolower($fileExpenstion),$imageArray))
                    <a href="{{asset('storage/app/'.$docsInDb->business_formation_doc)}}" target="_blank"><img width="100" src="{{asset('storage/app/'.$docsInDb->business_formation_doc)}}" /></a>
                    @else
                    <a href="{{asset('storage/app/'.$docsInDb->business_formation_doc)}}" target="_blank">{{$docsInDb->business_formation_doc}}</a>
                    @endif
                </span>
            </div>
            @endif


        </div>
        <!-- ===  Modal === -->
        <!-- === Edit Owner-Info Modal Tab === -->
        <div class="modal fade my-modal owner-info-modal-box" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="{{route('update.owner.info')}}" method="POST">
                        @csrf
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Edit Owner Info</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group form-row">
                                <label class="col-sm-12" for="exampleInputAddress">Owner Address</label>
                                <div class="col">
                                    <input class="form-control col-sm-12" type="address" class="form-control" aria-describedby="address" placeholder="Enter your address here" name="full_address" value="{{@$owner_info['owner_information']['full_address']}}" required>
                                </div>
                            </div>
                            <div class="form-group form-row">
                                <div class="col">
                                    <input class="form-control col-sm-12" type="city" class="form-control" id="city" name="city" value="{{@$owner_info['owner_information']['city']}}" required>
                                </div>
                            </div>
                            <div class="form-group form-row">
                                @php
                                $state_list = array('AL'=>"Alabama",
                                'AK'=>"Alaska",'AZ'=>"Arizona",'AR'=>"Arkansas",'CA'=>"California",
                                'CO'=>"Colorado",'CT'=>"Connecticut",
                                'DE'=>"Delaware",'DC'=>"District Of
                                Columbia",'FL'=>"Florida",'GA'=>"Georgia",'HI'=>"Hawaii",'ID'=>"Idaho",
                                'IL'=>"Illinois",'IN'=>"Indiana",'IA'=>"Iowa",'KS'=>"Kansas",'KY'=>"Kentucky",'LA'=>"Louisiana",
                                'ME'=>"Maine",'MD'=>"Maryland",'MA'=>"Massachusetts",'MI'=>"Michigan",'MN'=>"Minnesota",'MS'=>"Mississippi",
                                'MO'=>"Missouri",'MT'=>"Montana",'NE'=>"Nebraska",'NV'=>"Nevada",'NH'=>"New Hampshire",
                                'NJ'=>"New Jersey",'NM'=>"New Mexico",'NY'=>"New York",'NC'=>"North
                                Carolina",'ND'=>"North Dakota",
                                'OH'=>"Ohio",'OK'=>"Oklahoma",'OR'=>"Oregon",'PA'=>"Pennsylvania",'RI'=>"Rhode Island",
                                'SC'=>"South Carolina",'SD'=>"South
                                Dakota",'TN'=>"Tennessee",'TX'=>"Texas",'UT'=>"Utah",'VT'=>"Vermont",
                                'VA'=>"Virginia", 'WA'=>"Washington",'WV'=>"West
                                Virginia",'WI'=>"Wisconsin",'WY'=>"Wyoming");
                                @endphp
                                <div class="col">
                                    <select id="" name="state_id" data-cy="borrowerStateId" class="form-control">
                                        <option disabled="disabled" selected="selected" value="">
                                            @foreach($state_list as $keys=> $state_names)
                                        <option value="{{$keys}}" @if(!empty($owner_info['owner_information']['state_id']) && $owner_info['owner_information']['state_id']==$keys) selected @endif>
                                            {{$state_names}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-control-feedback text-danger" id="error_state_id" style="display: none">Field is required</div>
                                <div class="col">

                                    <input type="ZipCode" class="form-control" id="ZipCode" value="{{@$owner_info['owner_information']['zip_code']}}" name="zip_code" placeholder="Zip Code" required>
                                </div>
                            </div>

                            <div class="form-group form-row">
                                <label class="col-sm-12" for="socialsecuritynumber">Social Security Number</label>
                                <div class="col">
                                    <input type="secritynumber" class="form-control col-sm-12" value="{{@$owner_info['owner_credit_verification']['social_security_number'] }}" name="social_security_number" required>
                                </div>
                            </div>
                            <div class="form-group form-row">

                                <label class="col-sm-12" for="DateOfBirth">Owner Date of Birth</label>

                                @php
                                $months = array( 'January','February','March','April','May','June','July
                                ','August','September',
                                'October','November','December');
                                @endphp
                                <div class="col">
                                    <div class="ls-select">
                                        <select id="" id="month" name="date_of_birth_month" data-cy="" class="form-control">
                                            <option value="" selected="selected" disabled="disabled">Month</option>
                                            @php
                                            $i = 1;
                                            @endphp
                                            @foreach( $months as $month)
                                            @php
                                            if($i <10) $i='0' .$i @endphp <option value="{{$i}}" @if(isset($owner_info['owner_credit_verification']['date_of_birth_month']) && $owner_info['owner_credit_verification']['date_of_birth_month']==$i) selected @endif>{{$month}}</option>
                                                @php
                                                $i++;
                                                @endphp
                                                @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-control-feedback text-danger" id="error_date_of_birth_month" style="display: none">Field is required</div>
                                <div class="col">
                                    <input type="number" min="1" max="31" id="day" name="date_of_birth_day" placeholder="01" data-cy="" class="form-control" value="{{@$owner_info['owner_credit_verification']['date_of_birth_day']}}" required>
                                </div>
                                <div class="form-control col-sm-4" id="error_date_of_birth_day" style="display: none">Field is required</div>
                                <div class="col">
                                    <input type="number" id="year" name="date_of_birth_year" data-cy="" placeholder="2021" class="form-control" value="{{@$owner_info['owner_credit_verification']['date_of_birth_year']}}" required>
                                </div>
                                <div class="form-control-feedback text-danger" id="error_date_of_birth_year" style="display: none">Field is required</div>


                            </div>
                            <div class="form-group form-row">
                                <div class="col">
                                    <label for="#">Annual Personal Income?</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">$</span>
                                        </div>
                                        <input name="annul_personal_income" value="{{@$basic_info['annul_personal_income']['annul_personal_income']}}"  type="text" separator="," placeholder="" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-row">
                                <div class="col">
                                    <label for="#">First Name</label>
                                    <input value="{{@$owner_info['load_prfile']['first_name'] }}" id="first_name" data-cy="firstName" name="first_name" type="text" separator="," placeholder="" class="form-control" required>
                                </div>
                                <div class="form-control-feedback text-danger" id="error_first_name" style="display: none">Field is required</div>
                                <div class="col">
                                    <label for="#">Last Name</label>
                                    <input value="{{@$owner_info['load_prfile']['last_name'] }}" id="lastName" name="last_name" type="text" separator="," placeholder="" class="form-control" required>
                                </div>
                                <div class="form-control-feedback text-danger" id="error_last_name" style="display: none">Field is required</div>
                            </div>
                            <div class="form-group form-row">
                                <div class="col">
                                    <label for="#">What is your percentage of ownership (direct or indirect)?</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">%</span>
                                        </div>
                                        <input value="{{@$basic_info['business_percentage']['business_percentage']}}" id="business_percentage" name="business_percentage" type="text" separator="," placeholder="" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-control-feedback text-danger" id="error_business_percentage" style="display: none">Field is required</div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- === Edit Business-Info Modal Tab === -->
        <div class="modal fade my-modal" id="BusinessModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="{{route('update.business.info')}}" method="POST">
                        @csrf
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Edit Business Info</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group form-row">
                                <div class="col">
                                    <label class="col-sm-12" for="exampleInputAddress">Business Address</label>
                                    <input class="form-control" type="address" class="form-control" id="" aria-describedby="address" placeholder="Enter your address here" value="{{@$business_info['basic_info']['business_address'] }}" name="business_address" required>
                                </div>
                                <div class="form-control-feedback text-danger" id="error_business_address" style="display: none">Field is required</div>
                            </div>
                            <div class="form-group form-row">
                                <div class="col">
                                    <input class="form-control col-sm-12" type="text" class="form-control" id="city" value="{{@$business_info['basic_info']['city'] }}" name="city" required>
                                </div>
                                <div class="form-control-feedback text-danger" id="error_business_address" style="display: none">Field is required</div>
                            </div>
                            <div class="form-group form-row">
                                @php
                                $state_list = array('AL'=>"Alabama",
                                'AK'=>"Alaska",'AZ'=>"Arizona",'AR'=>"Arkansas",'CA'=>"California",
                                'CO'=>"Colorado",'CT'=>"Connecticut",
                                'DE'=>"Delaware",'DC'=>"District Of
                                Columbia",'FL'=>"Florida",'GA'=>"Georgia",'HI'=>"Hawaii",'ID'=>"Idaho",
                                'IL'=>"Illinois",'IN'=>"Indiana",'IA'=>"Iowa",'KS'=>"Kansas",'KY'=>"Kentucky",'LA'=>"Louisiana",
                                'ME'=>"Maine",'MD'=>"Maryland",'MA'=>"Massachusetts",'MI'=>"Michigan",'MN'=>"Minnesota",'MS'=>"Mississippi",
                                'MO'=>"Missouri",'MT'=>"Montana",'NE'=>"Nebraska",'NV'=>"Nevada",'NH'=>"New
                                Hampshire",
                                'NJ'=>"New Jersey",'NM'=>"New Mexico",'NY'=>"New York",'NC'=>"North
                                Carolina",'ND'=>"North Dakota",
                                'OH'=>"Ohio",'OK'=>"Oklahoma",'OR'=>"Oregon",'PA'=>"Pennsylvania",'RI'=>"Rhode
                                Island",
                                'SC'=>"South Carolina",'SD'=>"South
                                Dakota",'TN'=>"Tennessee",'TX'=>"Texas",'UT'=>"Utah",'VT'=>"Vermont",
                                'VA'=>"Virginia", 'WA'=>"Washington",'WV'=>"West
                                Virginia",'WI'=>"Wisconsin",'WY'=>"Wyoming");
                                @endphp
                                <div class="col">
                                    <select required name="state_id" data-cy="borrowerStateId" class="form-control">
                                        <option selected="selected" value="">
                                            @foreach($state_list as $keys=> $state_names)
                                        <option value="{{$keys}}" @if(!empty($business_info['basic_info']['state_id']) && $business_info['basic_info']['state_id']==$keys) selected @endif>
                                            {{$state_names}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-control-feedback text-danger" id="error_state_id" style="display: none">Field is required</div>
                                <div class="col">
                                    <input type="text" class="form-control" id="ZipCode" value="{{@$business_info['basic_info']['zip_code'] }}" name="zip_code" placeholder="Zip Code" required>
                                </div>
                                <div class="form-control-feedback text-danger" id="error_state_id" style="display: none">Field is required</div>
                            </div>
                            <div class="form-group form-row">
                                <div class="col">
                                    <label class="col-sm-12" for="socialsecuritynumber">Business Name</label>
                                    <input type="text" class="form-control col-sm-12" value="{{@$owner_info['load_prfile']['business_name'] }}" id="business_name" name="business_name" required>
                                </div>
                                <div class="col">
                                    <label class="col-sm-12" for="DateOfBirth">Business Phone</label>
                                    <input type="text" separator="," class="form-control" name="mobile_number" value="{{$owner_info['load_prfile']['mobile_number']}}" required>
                                </div>
                            </div>
                            <div class="form-group form-row">
                                <div class="col">
                                    <label for="#">Annual Profits?</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">$</span>
                                        </div>
                                        <input data-cy="" type="text" separator="," placeholder="" class="form-control" value="{{@$business_info['final_questions']['annual_personal_income']}}" name="annual_personal_income" id="annual_personal_income" required>
                                    </div>
                                </div>
                                <div class="invalid-feedback " id="error_annual_personal_income" style="display: none"> Please enter a valid dollar amount</div>
                            </div>
                            <div class="form-group form-row">
                                <div class="col">
                                    <label for="#">Do you invoice your customers?</label>
                                    <div class="input-group">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" value="Yes" name="is_invoice_customers" @if(!empty($basic_info["is_invoice_customers"]["is_invoice_customers"]) && $basic_info["is_invoice_customers"]["is_invoice_customers"]=="Yes" ) checked="checked" @endif required>
                                            <label class="form-check-label" for="flexRadioDefault1">
                                                Yes
                                            </label>
                                        </div>
                                    </div>
                                    <div class="input-group">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" value="No" name="is_invoice_customers" @if(!empty($basic_info["is_invoice_customers"]["is_invoice_customers"]) && $basic_info["is_invoice_customers"]["is_invoice_customers"]=="No" ) checked="checked" @endif required>
                                            <label class="form-check-label" for="flexRadioDefault1">
                                                No
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-row">
                                <div class="col">
                                    <label for="#">Is your business a non-profit?</label>
                                    <div class="input-group">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" value="Yes" name="is_business_non_profit" @if(!empty($basic_info["is_business_non_profit"]["is_business_non_profit"]) && $basic_info["is_business_non_profit"]["is_business_non_profit"]=="Yes" ) checked="checked" @endif required>
                                            <label class="form-check-label" for="flexRadioDefault1">
                                                Yes
                                            </label>
                                        </div>
                                    </div>
                                    <div class="input-group">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" value="No" name="is_business_non_profit" @if(!empty($basic_info["is_business_non_profit"]["is_business_non_profit"]) && $basic_info["is_business_non_profit"]["is_business_non_profit"]=="No" ) checked="checked" @endif required>
                                            <label class="form-check-label" for="flexRadioDefault1">
                                                No
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-row">
                                <div class="col">
                                    <label for="#">Do you use accounting software?</label>
                                    <div class="input-group">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" value="Yes" name="is_use_accounting_software" @if(!empty($basic_info["is_use_accounting_software"]["is_use_accounting_software"]) && $basic_info["is_use_accounting_software"]["is_use_accounting_software"]=="Yes" ) checked="checked" @endif required>
                                            <label class="form-check-label" for="flexRadioDefault1">
                                                Yes
                                            </label>
                                        </div>
                                    </div>
                                    <div class="input-group">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" value="No" name="is_use_accounting_software" @if(!empty($basic_info["is_use_accounting_software"]["is_use_accounting_software"]) && $basic_info["is_use_accounting_software"]["is_use_accounting_software"]=="No" ) checked="checked" @endif required>
                                            <label class="form-check-label" for="flexRadioDefault1">
                                                No
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-row">
                                <div class="col">
                                    <label for="#">Is your business a franchise?</label>
                                    <div class="input-group">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" value="Yes" name="is_business_franchise" @if(!empty($basic_info["is_business_franchise"]["is_business_franchise"]) && $basic_info["is_business_franchise"]["is_business_franchise"]=="Yes" ) checked="checked" @endif required>
                                            <label class="form-check-label" for="flexRadioDefault1">
                                                Yes
                                            </label>
                                        </div>
                                    </div>
                                    <div class="input-group">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" value="No" name="is_business_franchise" @if(!empty($basic_info["is_business_franchise"]["is_business_franchise"]) && $basic_info["is_business_franchise"]["is_business_franchise"]=="No" ) checked="checked" @endif required>
                                            <label class="form-check-label" for="flexRadioDefault1">
                                                No
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-row">
                                <div class="col">
                                    <label for="#">Is this a seasonal business?</label>
                                    <div class="input-group">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" value="Yes" name="is_seasonal" @if(!empty($basic_info["is_seasonal"]["is_seasonal"]) && $basic_info["is_seasonal"]["is_seasonal"]=="Yes" ) checked="checked" @endif required>
                                            <label class="form-check-label" for="flexRadioDefault1">
                                                Yes
                                            </label>
                                        </div>
                                    </div>
                                    <div class="input-group">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" value="No" name="is_seasonal" @if(!empty($basic_info["is_seasonal"]["is_seasonal"]) && $basic_info["is_seasonal"]["is_seasonal"]=="No" ) checked="checked" @endif required>
                                            <label class="form-check-label" for="flexRadioDefault1">
                                                No
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-row">
                                <div class="col">
                                    <label for="#">Entity Type</label>
                                    <select id="business_entity" name="business_entity" data-cy="business_entity" class="form-control">
                                        <option data-v-39b23e98="" value="">Select an option</option>
                                        <option @if(!empty($business_info['basic_info']['business_entity']) && $business_info['basic_info']['business_entity']=='llc' ) selected @endif data-v-39b23e98="" value="llc">
                                            LLC
                                        </option>
                                        <option @if(!empty($business_info['basic_info']['business_entity']) && $business_info['basic_info']['business_entity']=='corporation' ) selected @endif data-v-39b23e98="" value="corporation">
                                            Corporation
                                        </option>
                                        <option @if(!empty($business_info['basic_info']['business_entity']) && $business_info['basic_info']['business_entity']=='soleProprietor' ) selected @endif data-v-39b23e98="" value="soleProprietor">
                                            Sole Proprietor
                                        </option>
                                        <option @if(!empty($business_info['basic_info']['business_entity']) && $business_info['basic_info']['business_entity']=='legalPartnership' ) selected @endif data-v-39b23e98="" value="legalPartnership">
                                            Legal Partnership
                                        </option>
                                    </select>
                                </div>
                                <div class="form-control-feedback text-danger" id="error_business_entity" style="display: none">Field is required</div>
                            </div>
                            <div class="form-group form-row">
                                <div class="col">
                                    <label for="#">Do you accept credit cards?</label>
                                    <div class="input-group">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" value="Yes" name="is_accept_credit_cards" @if(!empty($basic_info["is_accept_credit_cards"]["is_accept_credit_cards"]) && $basic_info["is_accept_credit_cards"]["is_accept_credit_cards"]=="Yes" ) checked="checked" @endif required>
                                            <label class="form-check-label" for="flexRadioDefault1">
                                                Yes
                                            </label>
                                        </div>
                                    </div>
                                    <div class="input-group">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" value="No" name="is_accept_credit_cards" @if(!empty($basic_info["is_accept_credit_cards"]["is_accept_credit_cards"]) && $basic_info["is_accept_credit_cards"]["is_accept_credit_cards"]=="No" ) checked="checked" @endif required>
                                            <label class="form-check-label" for="flexRadioDefault1">
                                                No
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-row">
                                <div class="col">
                                    <label for="#">How much do you need?</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">$</span>
                                        </div>
                                        <input value="{{@$basic_info['financial_amount']['financial_amount']}}" id="financial_amount" name="financial_amount" data-cy="financial_amount" type="text" separator="," placeholder="" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-control-feedback text-danger" id="error_financial_amount" style="display: none">Field is required</div>
                            </div>
                            <div class="form-group form-row">
                                <label class="col-sm-12" for="DateOfBirth">Owner Date of Birth</label>
                                @php
                                $months = array( 'January','February','March','April','May','June','July
                                ','August','September',
                                'October','November','December');
                                @endphp
                                <div class="col">
                                    <select id="" id="month" name="business_start_date_month" data-cy="" class="form-control">
                                        <option value="" selected="selected" disabled="disabled">Month</option>
                                        @php
                                        $i = 1;
                                        @endphp
                                        @foreach( $months as $month)
                                        @php
                                        if($i <10) $i='0' .$i @endphp <option value="{{$i}}" @if(isset($basic_info['business_start_date']['business_start_date_month']) && $basic_info['business_start_date']['business_start_date_month']==$i) selected @endif>{{$month}}</option>
                                            @php
                                            $i++;
                                            @endphp
                                            @endforeach
                                    </select>
                                </div>
                                <div class="form-control-feedback text-danger" id="error_business_start_date_month" style="display: none">Field is required</div>

                                <div class="col">
                                    <input type="number" min="1" max="31" id="day" name="business_start_date_day" placeholder="01" data-cy="" class="form-control" value="{{@$basic_info['business_start_date']['business_start_date_day']}}" required>
                                </div>
                                <div class="form-control" id="error_date_of_birth_day" style="display: none">Field is required</div>

                                <div class="col">
                                    <input type="number" id="year" name="business_start_date_year" data-cy="" placeholder="2021" class="form-control" value="{{@$basic_info['business_start_date']['business_start_date_year']}}" required>
                                </div>
                                <div class="form-control-feedback text-danger" id="error_business_start_date_year" style="display: none">Field is required</div>
                            </div>
                            <div class="form-group form-row">
                                <div class="col">
                                    <label for="#">Average Monthly Sales</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">$</span>
                                        </div>
                                        <input value="{{@$basic_info['average_month_revenue']['average_month_revenue']}}" id="average_month_revenue" name="average_month_revenue" type="text" separator="," placeholder="" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-control-feedback text-danger" id="error_average_month_revenue" style="display: none">Field is required</div>
                            </div>
                            <div class="form-group form-row">
                                <div class="col">
                                    <label for="#">Loan Purpose</label>
                                    <div class="input-group">
                                        <!-- Multiselect dropdown -->
                                        <select class="select2" multiple="multiple" data-placeholder="Select a State" style="width: 100%;" name="loan_purpose[]">
                                            <option value="Expansion" @if(!empty($basic_info['loan_purpose']['loan_purpose']) && in_array('Expansion',$basic_info['loan_purpose']['loan_purpose']) ) selected @endif>Expansion</option>
                                            <option value="Working Capital" @if(!empty($basic_info['loan_purpose']['loan_purpose']) && in_array('Working Capital',$basic_info['loan_purpose']['loan_purpose']) ) selected @endif>Working Capital</option>
                                            <option value="Payroll" @if(!empty($basic_info['loan_purpose']['loan_purpose']) && in_array('Payroll',$basic_info['loan_purpose']['loan_purpose']) ) selected @endif>Payroll</option>
                                            <option value="Purchase a Business" @if(!empty($basic_info['loan_purpose']['loan_purpose']) && in_array('Purchase a Business',$basic_info['loan_purpose']['loan_purpose']) ) selected @endif>Purchase a Business</option>
                                            <option value="Purchase a Franchise" @if(!empty($basic_info['loan_purpose']['loan_purpose']) && in_array('Purchase a Franchise',$basic_info['loan_purpose']['loan_purpose']) ) selected @endif>
                                                Purchase a Franchise</option>
                                            <option value="Real Estate" @if(!empty($basic_info['loan_purpose']['loan_purpose']) && in_array('Real Estate',$basic_info['loan_purpose']['loan_purpose']) ) selected @endif>Real
                                                Estate</option>
                                            <option value="Buy Out a Partner" @if(!empty($basic_info['loan_purpose']['loan_purpose']) && in_array('Buy Out a Partner',$basic_info['loan_purpose']['loan_purpose']) ) selected @endif>Buy Out a Partner</option>
                                            <option value="Start a Business" @if(!empty($basic_info['loan_purpose']['loan_purpose']) && in_array('Start a Business',$basic_info['loan_purpose']['loan_purpose']) ) selected @endif>
                                                Start a Business</option>
                                            <option value="Finance Accounts Receivable" @if(!empty($basic_info['loan_purpose']['loan_purpose']) && in_array('Finance Accounts Receivable',$basic_info['loan_purpose']['loan_purpose']) ) selected @endif>
                                                Finance Accounts Receivable</option>
                                            <option value="Other" @if(!empty($basic_info['loan_purpose']['loan_purpose']) && in_array('Other',$basic_info['loan_purpose']['loan_purpose']) ) selected @endif>Other</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-row">
                                <div class="col">
                                    <label class="col-sm-12" for="DateOfBirth">Which best describes your business?</label>
                                    <select name="business_information" id="business_information" data-cy="" class="form-control mb-3">
                                        <option value="">Select an option</option>
                                        <option @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='agricultureForestry' ) selected @endif value="agricultureForestry">Agriculture, Forestry,
                                            Fishing
                                            and Hunting</option>
                                        <option @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='artsEntertainment' ) selected @endif value="artsEntertainment">Arts, Entertainment, and
                                            Recreation
                                        </option>
                                        <option @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='adultEntertainment' ) selected @endif value="adultEntertainment">Adult Entertainment</option>
                                        <option value="gambling" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='gambling' ) selected @endif>Gambling</option>
                                        <option value="automotive" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='automotive' ) selected @endif>Automobile Dealers &amp; Parts</option>
                                        <option value="construction" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='construction' ) selected @endif>Construction</option>
                                        <option value="education" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='education' ) selected @endif>Education</option>
                                        <option value="finance" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='finance' ) selected @endif>Finance and Insurance</option>
                                        <option value="healthcare" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='healthcare' ) selected @endif>Healthcare</option>
                                        <option value="socialAssistance" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='socialAssistance' ) selected @endif>Social Assistance</option>
                                        <option value="informationMedia" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='informationMedia' ) selected @endif>IT, Media, or Publishing</option>
                                        <option value="legalServices" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='legalServices' ) selected @endif>Legal Services</option>
                                        <option value="mining" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='mining' ) selected @endif>Mining (except Oil and Gas)</option>
                                        <option value="oilGas" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='oilGas' ) selected @endif>Oil and Gas Extraction</option>
                                        <option value="manufacturing" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='manufacturing' ) selected @endif>Manufacturing</option>
                                        <option value="governmentPublic" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='governmentPublic' ) selected @endif>Political, Governmental, or Public Organizations</option>
                                        <option value="realEstate" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='realEstate' ) selected @endif>Real Estate</option>
                                        <option value="religiousOrganizations" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='religiousOrganizations' ) selected @endif>Religious Organizations</option>
                                        <option value="restaurants" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='restaurants' ) selected @endif>Restaurants and Food Services</option>
                                        <option value="retail" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='retail' ) selected @endif>Retail Stores</option>
                                        <option value="firearms" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='firearms' ) selected @endif>Firearm Sales</option>
                                        <option value="gasStations" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='gasStations' ) selected @endif>Gas Stations</option>
                                        <option value="transportation" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='transportation' ) selected @endif>Transportation and Warehousing</option>
                                        <option value="freightTrucking" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='freightTrucking' ) selected @endif>Freight Trucking</option>
                                        <option value="travelAgencies" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='travelAgencies' ) selected @endif>Travel Agencies</option>
                                        <option value="utilities" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='utilities' ) selected @endif>Utilities</option>
                                        <option value="wholesale" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='wholesale' ) selected @endif>Wholesale Trade</option>
                                        <option value="other" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='other' ) selected @endif>All Other</option>
                                    </select>
                                </div>
                                <div class="form-control-feedback text-danger" id="error_business_start_date_month" style="display: none">Field is required</div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </section>
</div>
@endsection

@push('scripts')

<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
<!-- JS -->
<script src="{{asset('public/dist/select2/js/select2.full.min.js')}}" type="text/javascript"></script>

<!-- Script -->
<script type="text/javascript">
    //Initialize Select2 Elements
    $(document).ready(function() {
        $('.select2').select2({
            theme: 'bootstrap4'
        });

        function CommaFormatted(amount) {
            var delimiter = ","; // replace comma if desired 
            var d = amount;
            var i = parseInt(amount);
            if (isNaN(i)) {
                return '';
            }
            var minus = '';
            if (i < 0) {
                minus = '-';
            }
            i = Math.abs(i);
            var n = new String(i);
            var a = [];
            while (n.length > 3) {
                var nn = n.substr(n.length - 3);
                a.unshift(nn);
                n = n.substr(0, n.length - 3);
            }
            if (n.length > 0) {
                a.unshift(n);
            }
            n = a.join(delimiter);
            if (d.length < 1) {
                amount = n;
            } else {
                amount = n
            }
            amount = minus + amount;
            return amount;
        }
        $('#financial_amount').on('keyup', function() {
            var number = removeCommas($(this).val());
            var formattedNumber = CommaFormatted(number);
            $("#financial_amount").val(formattedNumber);
        });

        $('#annual_personal_income').on('keyup', function() {
            var number = removeCommas($(this).val());
            var formattedNumber = CommaFormatted(number);
            $("#annual_personal_income").val(formattedNumber);
        });

        function removeCommas(str) {
            while (str.search(",") >= 0) {
                str = (str + "").replace(',', '');
            }
            return str;
        }

    });
</script>
@endpush
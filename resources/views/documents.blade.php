@extends('layouts.master')
@push('styles')
@endpush
@section('content')
@php
$imageArray = array('png','jpe','jpeg','jpg','gif','bmp');
@endphp
<!-- ==== (DOCUMENTS) TAB (CONTENT-4) START FROM HERE === -->
<div class="tab-pane fade active show" id="nav-documents" role="tabpanel" aria-labelledby="nav-documents-tab">
      @if(session('flash_message'))
    <div class="alert alert-primary" role="alert">
        {{session('flash_message')}}
    </div>
    @endif 
    <h2 class="page-header mb-0 mt-3 text-center">Standard Business Financing Application</h2>
    <div class="documents-info" id="msform">
        <fieldset>
            <form action="{{route('upload.file')}}" method="POST" enctype="multipart/form-data" name="form_document1" name="form_document1">
                @csrf
                <section class="row">
                    <div class="col-md-12">
                        <div class="question-text col-12">
                            <h3>Upload statements</h3>
                        </div>
                        <h6 style="text-align: left;"><strong>Requirements:</strong></h6>
                        <ul style="text-align: left; padding: 0px;">
                            <li>Use .PDF/jpg,jpeg,png file format.</li>
                            <li>Include every page in the statement (even blank pages).</li>
                        </ul>
                    </div>
                    <div class="col-md-12">
                        <!-- ======= (DROP-ZOE-1) Drivers License Front ========= -->
                        <div class="upload-documets d-flex align-items-center mb-5">
                            <label class="control-label">Drivers License Front</label>
                            <div class="parent-parent-dropzon ml-auto">
                                <!--- ===== Drop Zone Hidden | Reset Field ===== --->
                                <div class="preview-zone hidden" id="preview-zone">
                                    <div class="box box-solid">
                                        <div class="box-body">
                                            @if(!empty($docsInDb) && $docsInDb->drivers_license_front != '')
                                            @php
                                            $realpath = $docsInDb->drivers_license_front;
                                            $realpathArray = explode(".",$realpath);
                                            $fileExpenstion = end($realpathArray);
                                            @endphp
                                            @if(in_array(strtolower($fileExpenstion),$imageArray))
                                            <a href="{{asset('storage/app/'.$docsInDb->drivers_license_front)}}" target="_blank"><img width="100" height="100" src="{{asset('storage/app/'.$docsInDb->drivers_license_front)}}" /></a>
                                            @else
                                            <a href="{{asset('storage/app/'.$docsInDb->drivers_license_front)}}" target="_blank">{{$docsInDb->drivers_license_front}}</a>
                                            @endif
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="dropzone-wrapper d-flex justify-content-center align-items-center" id="dropzone-wrapper1">
                                    <div class="dropzone-desc">
                                        <i class="glyphicon glyphicon-download-alt"></i>
                                        <p>Choose an image file or drag it here.</p>
                                    </div>
                                    <input type="file" name="drivers_license_front" class="dropzone" accept="image/x-png,image/gif,image/jpeg,.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf" required>
                                </div>
                            </div>
                        </div>

                        <!-- ======= (DROP-ZOE-2) Drivers License Back ========= -->
                        <div class="upload-documets d-flex align-items-center mb-5">
                            <label class="control-label">Drivers License Back</label>
                            <div class="parent-parent-dropzon ml-auto">
                                <!--- ===== Drop Zone Hidden | Reset Field ===== --->
                                <div class="preview-zone hidden" id="preview-zone">
                                    <div class="box box-solid">
                                        <div class="box-body">
                                            @if(!empty($docsInDb) && $docsInDb->drivers_license_back != '')
                                            @php
                                            $realpath = $docsInDb->drivers_license_back;
                                            $realpathArray = explode(".",$realpath);
                                            $fileExpenstion = end($realpathArray);
                                            @endphp
                                            @if(in_array(strtolower($fileExpenstion),$imageArray))
                                            <a href="{{asset('storage/app/'.$docsInDb->drivers_license_back)}}" target="_blank"><img width="100" height="100" src="{{asset('storage/app/'.$docsInDb->drivers_license_back)}}" /></a>
                                            @else
                                            <a href="{{asset('storage/app/'.$docsInDb->drivers_license_back)}}" target="_blank">{{$docsInDb->drivers_license_back}}</a>
                                            @endif
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="dropzone-wrapper d-flex justify-content-center align-items-center" id="dropzone-wrapper1">
                                    <div class="dropzone-desc">
                                        <i class="glyphicon glyphicon-download-alt"></i>
                                        <p>Choose an image file or drag it here.</p>
                                    </div>
                                    <input type="file" name="drivers_license_back" class="dropzone" accept="image/x-png,image/gif,image/jpeg,.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf" required>
                                </div>
                            </div>
                        </div>

                        <!-- ======= (DROP-ZOE-2) voided_check ========= -->
                        <div class="upload-documets d-flex align-items-center mb-5">
                            <label class="control-label">Voided Check</label>
                            <div class="parent-parent-dropzon ml-auto">
                                <!--- ===== Drop Zone Hidden | Reset Field ===== --->
                                <div class="preview-zone hidden" id="preview-zone">
                                    <div class="box box-solid">
                                        <div class="box-body">
                                            @if(!empty($docsInDb) && $docsInDb->voided_check != '')
                                            @php
                                            $realpath = $docsInDb->voided_check;
                                            $realpathArray = explode(".",$realpath);
                                            $fileExpenstion = end($realpathArray);
                                            @endphp
                                            @if(in_array(strtolower($fileExpenstion),$imageArray))
                                            <a href="{{asset('storage/app/'.$docsInDb->voided_check)}}" target="_blank"><img width="100" height="100" src="{{asset('storage/app/'.$docsInDb->voided_check)}}" /></a>
                                            @else
                                            <a href="{{asset('storage/app/'.$docsInDb->voided_check)}}" target="_blank">{{$docsInDb->voided_check}}</a>
                                            @endif
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="dropzone-wrapper d-flex justify-content-center align-items-center" id="dropzone-wrapper1">
                                    <div class="dropzone-desc">
                                        <i class="glyphicon glyphicon-download-alt"></i>
                                        <p>Choose an image file or drag it here.</p>
                                    </div>
                                    <input type="file" name="voided_check" class="dropzone" accept="image/x-png,image/gif,image/jpeg,.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf" required>
                                </div>
                            </div>
                        </div>

                        <!-- ======= (DROP-ZOE-2) schedule_c ========= -->
                        <div class="upload-documets d-flex align-items-center mb-5">
                            <label class="control-label">Schedule C</label>
                            <div class="parent-parent-dropzon ml-auto">
                                <!--- ===== Drop Zone Hidden | Reset Field ===== --->
                                <div class="preview-zone hidden" id="preview-zone">
                                    <div class="box box-solid">
                                        <div class="box-body">
                                            @if(!empty($docsInDb) && $docsInDb->schedule_c != '')
                                            @php
                                            $realpath = $docsInDb->schedule_c;
                                            $realpathArray = explode(".",$realpath);
                                            $fileExpenstion = end($realpathArray);
                                            @endphp
                                            @if(in_array(strtolower($fileExpenstion),$imageArray))
                                            <a href="{{asset('storage/app/'.$docsInDb->schedule_c)}}" target="_blank"><img width="100" height="100" src="{{asset('storage/app/'.$docsInDb->schedule_c)}}" /></a>
                                            @else
                                            <a href="{{asset('storage/app/'.$docsInDb->schedule_c)}}" target="_blank">{{$docsInDb->schedule_c}}</a>
                                            @endif
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="dropzone-wrapper d-flex justify-content-center align-items-center" id="dropzone-wrapper1">
                                    <div class="dropzone-desc">
                                        <i class="glyphicon glyphicon-download-alt"></i>
                                        <p>Choose an image file or drag it here.</p>
                                    </div>
                                    <input type="file" name="schedule_c" class="dropzone" accept="image/x-png,image/gif,image/jpeg,.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf" required>
                                </div>
                            </div>
                        </div>

                        <!-- ======= (DROP-ZOE-2) Bank_Statement ========= -->
                        <div class="upload-documets d-flex align-items-center mb-5">
                            <label class="control-label">Bank Statement</label>
                            <div class="parent-parent-dropzon ml-auto">
                                <!--- ===== Drop Zone Hidden | Reset Field ===== --->
                                <div class="preview-zone hidden" id="preview-zone">
                                    <div class="box box-solid">
                                        <div class="box-body">
                                            @if(!empty($docsInDb) && $docsInDb->bank_statement != '')
                                            @php
                                            $realpath = $docsInDb->bank_statement;
                                            $realpathArray = explode(".",$realpath);
                                            $fileExpenstion = end($realpathArray);
                                            @endphp
                                            @if(in_array(strtolower($fileExpenstion),$imageArray))
                                            <a href="{{asset('storage/app/'.$docsInDb->bank_statement)}}" target="_blank"><img width="100" height="100" src="{{asset('storage/app/'.$docsInDb->bank_statement)}}" /></a>
                                            @else
                                            <a href="{{asset('storage/app/'.$docsInDb->bank_statement)}}" target="_blank">{{$docsInDb->bank_statement}}</a>
                                            @endif
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="dropzone-wrapper d-flex justify-content-center align-items-center" id="dropzone-wrapper1">
                                    <div class="dropzone-desc">
                                        <i class="glyphicon glyphicon-download-alt"></i>
                                        <p>Choose an image file or drag it here.</p>
                                    </div>
                                    <input type="file" name="bank_statement" class="dropzone" accept="image/x-png,image/gif,image/jpeg,.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf" required>
                                </div>
                            </div>
                        </div>

                        <!-- ======= (DROP-ZOE-2) Business Formation docs ========= -->
                        <div class="upload-documets d-flex align-items-center">
                            <label class="control-label">Business Formation docs.</label>
                            <div class="parent-parent-dropzon ml-auto">
                                <!--- ===== Drop Zone Hidden | Reset Field ===== --->
                                <div class="preview-zone hidden" id="preview-zone">
                                    <div class="box box-solid">
                                        <div class="box-body">
                                            @if(!empty($docsInDb) && $docsInDb->business_formation_doc != '')
                                            @php
                                            $realpath = $docsInDb->business_formation_doc;
                                            $realpathArray = explode(".",$realpath);
                                            $fileExpenstion = end($realpathArray);
                                            @endphp
                                            @if(in_array(strtolower($fileExpenstion),$imageArray))
                                            <a href="{{asset('storage/app/'.$docsInDb->business_formation_doc)}}" target="_blank"><img width="100" height="100" src="{{asset('storage/app/'.$docsInDb->business_formation_doc)}}" /></a>
                                            @else
                                            <a href="{{asset('storage/app/'.$docsInDb->business_formation_doc)}}" target="_blank">{{$docsInDb->business_formation_doc}}</a>
                                            @endif
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="dropzone-wrapper d-flex justify-content-center align-items-center" id="dropzone-wrapper1">
                                    <div class="dropzone-desc">
                                        <i class="glyphicon glyphicon-download-alt"></i>
                                        <p>Choose an image file or drag it here.</p>
                                    </div>
                                    <input type="file" name="business_formation_doc" class="dropzone" accept="image/x-png,image/gif,image/jpeg,.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <div class="footer-form mt-5">
                    <button type="submit" name="next" class="btn btn-primary text-center action-button" value="Next">
                        <span>Upload</span>
                    </button>
                </div>
            </form>
        </fieldset>

    </div>
</div>

@endsection
@push('scripts')

<!-- Script -->
<script>
    var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");
    function readFile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                var htmlPreview =
                    '<i class="fa fa-times remove-preview box-tools"></i>' +
                    '<img width="100" src="' + e.target.result + '" />' +
                    '<p>' + input.files[0].name + '</p>' ;
                    
                var wrapperZone = $(input).parent();
                var previewZone = $(input).parent().parent().find('.preview-zone');
                var boxZone = $(input).parent().parent().find('.preview-zone').find('.box').find('.box-body');

                wrapperZone.removeClass('dragover');
                previewZone.removeClass('hidden');
                boxZone.empty();
                boxZone.append(htmlPreview);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    function reset(e) {
        e.val(null);
        //e.wrap('<form>').closest('form').get(0).reset();
        //e.unwrap();
    }

    $(".dropzone").change(function() {
        readFile(this);
    });

    $('#dropzone-wrapper').on('dragover', function(e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).addClass('dragover');
    });

    $('#dropzone-wrapper').on('dragleave', function(e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).removeClass('dragover');
    });

    $(document).on('click','.remove-preview', function() {
        var boxZone     = $(this).parents('.box-body');
        //var previewZone = $(this).parents('.preview-zone');
        var dropzone     = $(this).parents('.upload-documets').find('.dropzone');
        boxZone.empty();
        //previewZone.addClass('hidden');
        reset(dropzone);
    });
</script>

@endpush
@extends('layouts.userdashboard')
@push('styles')
@endpush
@section('content')

<!-- Finish Your Application -->
<h1>Dashboard</h1>
<p>Welcome to your loan dashboard. Here you can complete the next steps for your loan application. Check
    back regularly for status updates.</p>
<div class="portal-card">
    <div class="row">
        <div class="col-4 col-md-3">
            <div class="overview-percent-meter">
                <div class="completeness-position">
                    <svg viewBox="0 0 120 120" class="completeness">
                        <circle cx="60" cy="60" r="54" stroke-width="6" class="completeness__track"></circle>
                        <circle cx="60" cy="60" r="54" stroke-width="6" class="completeness__fill" style="stroke-dashoffset: 135.717; stroke-dasharray: 339.292;"></circle>
                    </svg>
                    <div class="percent-text">60%</div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-9 chance">
            <h2>Finish Your Application</h2>
            <div> <p>A complete application gives you the best chance at getting financing.</p>
                <br/>
              
@if(!empty($basicInfo->sba_loan_number))
               <h4 style="border-bottom:2px solid #05c2f0"> Your Loan Number is {{@$basicInfo->sba_loan_number}}</h4> @endif
                 </div>
            <div class="text-md-left">
                <button class="btn btn-action btn-primary" onclick="window.location='{{route('ppp.basicinfo')}}'">
                    <div class="text-center position-relative">
                        <div>Get Started</div>
                    </div>
                </button>
            </div>
        </div>
    </div>
</div>
<!-- Qualified Offers -->
<div class="sub-title">Qualified Offers</div>
<div class="row">
        <!-- LOAN-OFFERS -->
        <div class="col col-md-4 col-12">
            <div class="portal-card">
                <div class="widget-title">Loan Offers</div>
                <div class="widget-data">0</div>
                <div>Pending Offers</div>
                <div class="widget-subtext pb-4">Still waiting on loan options</div>
            </div>
        </div>
        <!-- CREDIT-CARDS -->
        <div class="col col-md-4 col-12">
            <div class="portal-card">
                <div class="widget-title">Credit Cards</div>
                <div class="widget-data">14</div>
                <div>Matches Found</div>
                <div class="widget-subtext pb-4 sub-text-link">
                    <a href="creditcards.html">VIEW</a>
                </div>
            </div>
        </div>
        <!-- YOUR-RESOURCES-->
        <div class="col col-md-4 col-12">
            <div class="portal-card">
                <div class="widget-title">Your Resources</div>
                <div class="widget-data">3</div>
                <div>Matches Found</div>
                <div class="widget-subtext pb-4 sub-text-link">
                    <a href="resources.html">VIEW</a>
                </div>
            </div>
        </div>
    
</div>
</div>
<!-- The New Round of PPP is Live! -->
<div class="row">
    <div class="col col-md-12 col-12">
        <div class="portal-card text-center">
            <div class="upper-section-text">
                <h2 data-v-3514b218="" class="mb-2 text-xs-left">The New Round of PPP is Live!</h2>
                <div class="">
                    The time is here! Click below to monitor your PPP offers page to check for status updates, review
                    offers, and edit your application.
                </div>
            </div>
            <button type="button" class="btn btn-primary continue-button text-center">
                <span>PPP Offers Page</span>
            </button>
        </div>
    </div>
</div>

@endsection

@push('scripts')
@endpush
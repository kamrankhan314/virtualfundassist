@extends('layouts.master')
@section('content')
@php
$stepsArray = session('stepsArray');
if(!empty($stepsArray['businessinfo'])){
$business_info = $stepsArray['businessinfo'];
} 

@endphp
<!-- ==== (BUSINESS INFO_TAB) START FROM HERE === -->
<div class=" tab-pane fade mt-5 active show " id="nav-businessinfo" role="tabpanel" aria-labelledby="nav-businessinfo-tab">
    <h2 class="page-header mb-0 mt-3 text-center">Standard Business Financing Application</h2>

    <div id="msform">
        <!-- (1st FieldSet) ** Business Info ** === -->
        <fieldset id="fieldset_basic_info" class="fldset-steps">
            <form class="stepsform">
                <input type="hidden" name="tab_name" value="business_info">
                <input type="hidden" name="step_name" value="basic_info">

                <h3 class="text-center">Business Info</h3>
                <div id="borrowerAttribute" class="attributeClass">
                    <div class="form-group">
                        <div class="borrower-attribute-name">
                            <label for="babusinessAddress">Business Address</label>
                        </div>
                        <div class="type-container">

                            <!-- ** (label name) Business Address ** -->
                            <div class="form-group">
                                <input value="{{@$business_info['basic_info']['business_address'] }}" name="business_address" data-cy="borrowerStreet" type="text" placeholder="Enter your address here" class="form-control pac-target-input" autocomplete="off">
                                <div class="form-control-feedback text-danger text-left" id="error_business_address" style="display: none">Field is required</div>

                            </div>

                            <!-- ** (label name) City ** -->
                            <div class="form-group">
                                <input value="{{@$business_info['basic_info']['city'] }}" name="city" data-cy="borrowerCity" type="text" placeholder="City" class="form-control">
                                <div class="form-control-feedback text-danger text-left" id="error_city" style="display: none">
                                    Field is required</div>

                            </div>

                            <!-- **S(label name) tate ** -->
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col-sm-10 col-md-6">
                                        <div class="ls-select mb-0">
                                            <svg class="svg-inline--fa fa-angle-down fa-w-10" data-fa-pseudo-element=":before" aria-hidden="true" data-fa-processed="" data-prefix="far" data-icon="angle-down" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
                                                <path fill="currentColor" d="M151.5 347.8L3.5 201c-4.7-4.7-4.7-12.3 0-17l19.8-19.8c4.7-4.7 12.3-4.7 17 0L160 282.7l119.7-118.5c4.7-4.7 12.3-4.7 17 0l19.8 19.8c4.7 4.7 4.7 12.3 0 17l-148 146.8c-4.7 4.7-12.3 4.7-17 0z">
                                                </path>
                                            </svg> @php
                                            $state_list = array('AL'=>"Alabama",
                                            'AK'=>"Alaska",
                                            'AZ'=>"Arizona",
                                            'AR'=>"Arkansas",
                                            'CA'=>"California",
                                            'CO'=>"Colorado",
                                            'CT'=>"Connecticut",
                                            'DE'=>"Delaware",
                                            'DC'=>"District Of Columbia",
                                            'FL'=>"Florida",
                                            'GA'=>"Georgia",
                                            'HI'=>"Hawaii",
                                            'ID'=>"Idaho",
                                            'IL'=>"Illinois",
                                            'IN'=>"Indiana",
                                            'IA'=>"Iowa",
                                            'KS'=>"Kansas",
                                            'KY'=>"Kentucky",
                                            'LA'=>"Louisiana",
                                            'ME'=>"Maine",
                                            'MD'=>"Maryland",
                                            'MA'=>"Massachusetts",
                                            'MI'=>"Michigan",
                                            'MN'=>"Minnesota",
                                            'MS'=>"Mississippi",
                                            'MO'=>"Missouri",
                                            'MT'=>"Montana",
                                            'NE'=>"Nebraska",
                                            'NV'=>"Nevada",
                                            'NH'=>"New Hampshire",
                                            'NJ'=>"New Jersey",
                                            'NM'=>"New Mexico",
                                            'NY'=>"New York",
                                            'NC'=>"North Carolina",
                                            'ND'=>"North Dakota",
                                            'OH'=>"Ohio",
                                            'OK'=>"Oklahoma",
                                            'OR'=>"Oregon",
                                            'PA'=>"Pennsylvania",
                                            'RI'=>"Rhode Island",
                                            'SC'=>"South Carolina",
                                            'SD'=>"South Dakota",
                                            'TN'=>"Tennessee",
                                            'TX'=>"Texas",
                                            'UT'=>"Utah",
                                            'VT'=>"Vermont",
                                            'VA'=>"Virginia",
                                            'WA'=>"Washington",
                                            'WV'=>"West Virginia",
                                            'WI'=>"Wisconsin",
                                            'WY'=>"Wyoming");
                                            @endphp
                                            <select id="" name="state_id" data-cy="borrowerStateId" class="form-control">
                                                <option disabled="disabled" selected="selected" value="">
                                                    Select a state</option>
                                                @foreach($state_list as $keys=> $state_names)
                                                <option value="{{$keys}}" @if(!empty($business_info['basic_info']['state_id']) && $business_info['basic_info']['state_id']==$keys) selected @endif>
                                                    {{$state_names}}
                                                </option>
                                                @endforeach
                                            </select>
                                            <div class="form-control-feedback text-danger text-left" id="error_state_id" style="display: none">Field is required</div>

                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-md-6">
                                        <input value="{{@$business_info['basic_info']['zip_code'] }}" name="zip_code" data-cy="borrowerZipId" masked="true" type="text" placeholder="ZIP Code" class="form-control form-zip required">
                                        <div class="form-control-feedback text-danger text-left" id="error_zip_code" style="display: none">Field is required</div>

                                    </div>
                                </div>
                            </div>

                            <!-- ** (label name) Zip_Code ** -->
                            <div class="form-group">
                                <div class="borrower-attribute-name">
                                    <label for="">Entity Type</label>
                                    <span class="borrower-attribute-tooltip">
                                        <a>
                                            <div class="tooltip-wrapper">
                                                <span class="tooltip-trigger"></span>
                                                <svg class="svg-inline--fa fa-question-circle fa-w-16" aria-hidden="true" data-fa-processed="" data-prefix="far" data-icon="question-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                    <path fill="currentColor" d="M256 8C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm0 448c-110.532 0-200-89.431-200-200 0-110.495 89.472-200 200-200 110.491 0 200 89.471 200 200 0 110.53-89.431 200-200 200zm107.244-255.2c0 67.052-72.421 68.084-72.421 92.863V300c0 6.627-5.373 12-12 12h-45.647c-6.627 0-12-5.373-12-12v-8.659c0-35.745 27.1-50.034 47.579-61.516 17.561-9.845 28.324-16.541 28.324-29.579 0-17.246-21.999-28.693-39.784-28.693-23.189 0-33.894 10.977-48.942 29.969-4.057 5.12-11.46 6.071-16.666 2.124l-27.824-21.098c-5.107-3.872-6.251-11.066-2.644-16.363C184.846 131.491 214.94 112 261.794 112c49.071 0 101.45 38.304 101.45 88.8zM298 368c0 23.159-18.841 42-42 42s-42-18.841-42-42 18.841-42 42-42 42 18.841 42 42z">
                                                    </path>
                                                </svg>
                                            </div>
                                        </a>
                                    </span>
                                </div>
                                <div class="type-container">
                                    {{-- <input value="{{@$business_info['basic_info']['dba_name'] }}" name="dba_name"
                                    type="text" data-cy="226" class="form-control is-invalid"> --}}
                                    <div class="ls-select mb-0">
                                        <svg class="svg-inline--fa fa-angle-down fa-w-10" data-fa-pseudo-element=":before" aria-hidden="true" data-fa-processed="" data-prefix="far" data-icon="angle-down" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
                                            <path fill="currentColor" d="M151.5 347.8L3.5 201c-4.7-4.7-4.7-12.3 0-17l19.8-19.8c4.7-4.7 12.3-4.7 17 0L160 282.7l119.7-118.5c4.7-4.7 12.3-4.7 17 0l19.8 19.8c4.7 4.7 4.7 12.3 0 17l-148 146.8c-4.7 4.7-12.3 4.7-17 0z">
                                            </path>
                                        </svg>
                                        <select id="business_entity" name="business_entity" data-cy="business_entity" class="form-control">
                                            <option data-v-39b23e98="" value="">Select an option</option>
                                            <option @if(!empty($business_info['basic_info']['business_entity']) && $business_info['basic_info']['business_entity']=='LLC' ) selected @endif   value="LLC">
                                                LLC </option>
                                            <option @if(!empty($business_info['basic_info']['business_entity']) && $business_info['basic_info']['business_entity']=='Corporation' ) selected @endif   value="Corporation">
                                                Corporation
                                            </option>
                                            <option @if(!empty($business_info['basic_info']['business_entity']) && $business_info['basic_info']['business_entity']=='Sole Proprietor' ) selected @endif   value="Sole Proprietor">
                                                Sole Proprietor
                                            </option>
                                            <option @if(!empty($business_info['basic_info']['business_entity']) && $business_info['basic_info']['business_entity']=='Legal Partnership' ) selected @endif   value="Legal Partnership">
                                                Legal Partnership
                                            </option>
                                        </select>
                                        <div class="form-control-feedback text-danger text-left" id="error_business_entity" style="display: none">Field is required</div>

                                    </div>
                                </div>
                            </div>

                            <!-- ** (label name) Federal/State Tax ID ** -->
                            <div class="form-group">
                                <div class="borrower-attribute-name">
                                    <label for="ba236">Federal/State Tax ID</label>
                                    <span class="borrower-attribute-tooltip">
                                        <a>
                                            <div class="tooltip-wrapper">
                                                <span class="tooltip-trigger"></span>
                                                <svg class="svg-inline--fa fa-question-circle fa-w-16" aria-hidden="true" data-fa-processed="" data-prefix="far" data-icon="question-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                    <path fill="currentColor" d="M256 8C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm0 448c-110.532 0-200-89.431-200-200 0-110.495 89.472-200 200-200 110.491 0 200 89.471 200 200 0 110.53-89.431 200-200 200zm107.244-255.2c0 67.052-72.421 68.084-72.421 92.863V300c0 6.627-5.373 12-12 12h-45.647c-6.627 0-12-5.373-12-12v-8.659c0-35.745 27.1-50.034 47.579-61.516 17.561-9.845 28.324-16.541 28.324-29.579 0-17.246-21.999-28.693-39.784-28.693-23.189 0-33.894 10.977-48.942 29.969-4.057 5.12-11.46 6.071-16.666 2.124l-27.824-21.098c-5.107-3.872-6.251-11.066-2.644-16.363C184.846 131.491 214.94 112 261.794 112c49.071 0 101.45 38.304 101.45 88.8zM298 368c0 23.159-18.841 42-42 42s-42-18.841-42-42 18.841-42 42-42 42 18.841 42 42z">
                                                    </path>
                                                </svg>
                                            </div>
                                        </a>
                                    </span>
                                </div>
                                <div class="type-container">
                                    <input value="{{@$business_info['basic_info']['tax_id']}}" id="tax_id" name="tax_id" type="text" data-cy="236" placeholder="12-3456789" class="form-control">
                                </div>
                            </div>

                            <!-- ** (label name) Number of Employees (Including Owners) ** -->
                            <div class="form-group">
                                <div class="borrower-attribute-name">
                                    <label for="">Number of Employees (Including Owners)
                                    </label>
                                    <span class="borrower-attribute-tooltip"></span>
                                </div>
                                <div class="type-container">
                                    <div class="input-group">
                                        <input value="{{@$business_info['basic_info']['total_employees'] }}" name="total_employees" type="number" data-cy="number_of_employees" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <!-- ** (label name) Has the applicant received an SBA ecnomic injury disaster ** -->
                            <div class="form-group">
                                <p class="w-para">Has the applicant received an SBA ecnomic injury disaster loan between january 31, 2020 and april 30, 2021</p>
                                <label class="container mb-3">Yes
                                    <input @if(!empty($business_info['basic_info']['is_received_sba_ecnomic_loan']) && $business_info['basic_info']['is_received_sba_ecnomic_loan']=='Yes' ) checked="checked" @endif type="radio"  name="is_received_sba_ecnomic_loan" value="Yes">
                                    <span class="checkmark"></span>
                                </label>
                                <label class="container" style="margin-left: 4px;">No
                                    <input type="radio" name="is_received_sba_ecnomic_loan" value="No" @if(!empty($business_info['basic_info']['is_received_sba_ecnomic_loan']) && $business_info['basic_info']['is_received_sba_ecnomic_loan']=='No' ) checked="checked" @endif>
                                    <span class="checkmark"></span>
                                </label>
                            </div>

                            <!-- ** (label name) Ecnomic Injury Disaster Loan (EIDL) ** -->
                            <div class="form-group">
                                <label class="m-0" for="#">Ecnomic Injury Disaster Loan (EIDL)</label>
                                <small class="d-block pb-2">Enter EIDL fund received between jan 31 and march 08,200. ENTER 0 if not applicable</small>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">$</span>
                                    </div>
                                    <input value="{{@$business_info['basic_info']['eidl_fund_received']}}" id="eidl_fund_received" name="eidl_fund_received" type="text" separator="," placeholder="" class="form-control">
                                </div>

                                 <div class="form-control-feedback text-danger text-left" id="error_eidl_fund_received" style="display: none">Field is required</div>

                            </div>

                            <!-- ** (label name) EIDL Loan Number ** -->
                            <div class="form-group">
                                <label class="m-0" for="">EIDL Loan Number</label>
                                <small class="d-block pb-2">Provide the 10 digit EIDL number associated with your EIDL loan</small>

                                <div class="type-container">
                                    <div class="input-group">
                                        <input value="{{@$business_info['basic_info']['eidl_loanNumber']}}" name="eidl_loanNumber" type="text" class="form-control">
                                    </div>
                                </div>
                            </div>

                        </div> 
                        <div data-cy="alert" class="alert-wrapper my-3 closed" style="display: none;">
                            <div class="alert alert-undefined">
                                <div class="alert-content-wrapper w-100 d-flex">
                                    <span class="alert-icon icon-24-undefined" style="vertical-align: middle;"></span>
                                    <span class="alert-title"></span>
                                    <span class="alert-content w-100"></span>
                                </div>
                            </div>
                        </div>
<!-- ** Buttons ** -->
<div class="footer">
    <div class="footer-form">
        <button type="submit" name="next" class="btn btn-primary text-center action-button" value="Next">
            <span>Next</span>
        </button>
    </div>
    <div class="skip-btn d-none">
        <a href="#">Fwd <i class="fas fa-arrow-right"></i></a>
    </div>
</div>
</div>
</form>
</fieldset>

<!-- (2nd FieldSet) ** Which SBA Assistance Needed ? ** === -->
<fieldset id="fieldset_sba_assistance_needed" class="fldset-steps">
    <form class="stepsform">
        <input type="hidden" name="tab_name" value="business_info">
        <input type="hidden" name="step_name" value="sba_assistance_needed">

        <h3 class="text-center">Which SBA Assistance Needed ?</h3>
        <div id="borrowerAttribute" class="attributeClass">
            <div class="form-group">
                <div class="borrower-attribute-name">
                    <label for="babusinessAddress">Business Address</label>
                </div>
                <div id="borrowerAttribute" class="attributeClass">
                    <div class="form-group">
                        <div class="form-check">
                            <input type="radio" name="economicinnjury_disasterloan" @if(!empty($business_info['sba_assistance_needed']['economicinnjury_disasterloan']) && $business_info['sba_assistance_needed']['economicinnjury_disasterloan']=="Economic Injury Disaster Loan" ) checked="checked" @endif value="Economic Injury Disaster Loan" class="form-check-input" type="radio" id="exampleRadios1">
                            <label class="form-check-label" for="exampleRadios1">
                                Economic Injury Disaster Loan (EIDL)
                            </label>
                        </div>
                    </div>
                </div>
                <div id="borrowerAttribute" class="attributeClass">
                    <div class="form-group">
                        <div class="form-check">
                            <input type="radio" name="economicinnjury_disasterloan" @if(!empty($business_info['sba_assistance_needed']['economicinnjury_disasterloan']) && $business_info['sba_assistance_needed']['economicinnjury_disasterloan']=="Paycheck Protection Program" ) checked="checked" @endif value="Paycheck Protection Program" class="form-check-input" type="radio" id="exampleRadios2">
                            <label class="form-check-label" for="exampleRadios2">
                                Paycheck Protection Program (PPP)
                            </label>
                        </div>
                    </div>
                </div>
                <div id="borrowerAttribute" class="attributeClass">
                    <div class="form-group">
                        <div class="form-check">
                            <input type="radio" name="economicinnjury_disasterloan" @if(!empty($business_info['sba_assistance_needed']['economicinnjury_disasterloan']) && $business_info['sba_assistance_needed']['economicinnjury_disasterloan']=="Economic Injury Disaster Loan (EIDL) and Paycheck Protection Program (PPP)" ) checked="checked" @endif value="Economic Injury Disaster Loan (EIDL) and Paycheck Protection Program (PPP)" class="form-check-input" type="radio" id="exampleRadios3">
                            <label class="form-check-label" for="exampleRadios3">
                                Economic Injury Disaster Loan (EIDL) and Paycheck Protection Program (PPP)
                            </label>
                        </div>
                    </div>
                    <div class="form-control-feedback text-danger text-left" id="error_economicinnjury_disasterloan" style="display: none">Field is required</div>
                </div>
                <div id="borrowerAttribute" class="attributeClass">
                    <div class="form-group">
                        <div class="form-check">
                            <input type="radio" name="economicinnjury_disasterloan" @if(!empty($business_info['sba_assistance_needed']['economicinnjury_disasterloan']) && $business_info['sba_assistance_needed']['economicinnjury_disasterloan']=="Forgiveness" ) checked="checked" @endif value="Forgiveness" class="form-check-input" type="radio" id="exampleRadios4">
                            <label class="form-check-label" for="exampleRadios4">
                                Forgiveness
                            </label>
                        </div>
                    </div>
                    <div class="form-control-feedback text-danger text-left" id="error_economicinnjury_disasterloan" style="display: none">Field is required</div>
                </div>

                <div class="form-group">
                    <div class="borrower-attribute-name">
                        <label for="">Bank Name * </label>
                    </div>
                    <div class="type-container">
                        <input value="{{@$business_info['sba_assistance_needed']['bank_name'] }}" name="bank_name" type="text" class="form-control">
                        <div class="form-control-feedback text-danger text-left" id="error_bank_name" style="display: none">Field is required</div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="borrower-attribute-name">
                        <label for="">Bank Routing Number *</label>
                    </div>
                    <div class="type-container">
                        <input value="{{@$business_info['sba_assistance_needed']['bank_routring_number']}}" name="bank_routring_number" type="text" class="form-control">
                        <div class="form-control-feedback text-danger text-left" id="error_bank_routring_number" style="display: none">Field is required</div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="borrower-attribute-name">
                        <label for="">Bank Account Number *</label>
                    </div>
                    <div class="type-container">
                        <input value="{{@$business_info['sba_assistance_needed']['bank_account_number']}}" name="bank_account_number" type="text" class="form-control">
                        <div class="form-control-feedback text-danger text-left" id="error_bank_account_number" style="display: none">Field is required</div>
                    </div>
                </div>
            </div>
        </div>

        <div data-cy="alert" class="alert-wrapper my-3 closed" style="display: none;">
            <div class="alert alert-undefined">
                <div class="alert-content-wrapper w-100 d-flex">
                    <span class="alert-icon icon-24-undefined" style="vertical-align: middle;"></span>
                    <span class="alert-title"></span>
                    <span class="alert-content w-100"></span>
                </div>
            </div>
        </div>
        <div class="footer">
            <div class="footer-form mt-5">
                <button type="button" name="previous" data-value="basic_info" class="previousBtn action-button btn-primary" value="Previous" style="float: left;" />
                <span>Previous</span>
                </button>
                <button type="submit" name="next" class="btn action-button btn-primary" value="Next" style="float: right;" />
                <span>Next</span>
                </button>

            </div>
            <div class="skip-btn d-none">
                <a href="#">Fwd <i class="fas fa-arrow-right"></i></a>
            </div>
        </div>
    </form>
</fieldset>

<!-- (3rd FieldSet) ** Business Location Ownership Type ** === -->
<fieldset id="fieldset_business_financials" class="fldset-steps">
    <form class="stepsform">
        <input type="hidden" name="tab_name" value="business_info">
        <input type="hidden" name="step_name" value="business_financials">

        <section class="align-self-center app-page usiness-info-2">
            <h3 class="text-center">Business Financials</h3>
            <div id="" class="attributeClass">
                <div class="form-group">
                    <div class="borrower-attribute-name">
                        <label for="">Business Location Ownership Type</label>
                        <span class="borrower-attribute-tooltip"></span>
                    </div>
                    <div class="type-container">
                        <div id="borrowerAttributeSelect">
                            <div class="form-group ls-select mb-0">
                                <svg class="svg-inline--fa fa-angle-down fa-w-10" data-fa-pseudo-element=":before" aria-hidden="true" data-fa-processed="" data-prefix="far" data-icon="angle-down" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
                                    <path fill="currentColor" d="M151.5 347.8L3.5 201c-4.7-4.7-4.7-12.3 0-17l19.8-19.8c4.7-4.7 12.3-4.7 17 0L160 282.7l119.7-118.5c4.7-4.7 12.3-4.7 17 0l19.8 19.8c4.7 4.7 4.7 12.3 0 17l-148 146.8c-4.7 4.7-12.3 4.7-17 0z">
                                    </path>
                                </svg>
                                <select name="business_ownership_type" data-cy="business_property_type_own_rent" class="form-control no-select">
                                    <option value="">Select an option</option>
                                    <option @if(!empty($business_info['business_financials']['business_ownership_type']) && $business_info['business_financials']['business_ownership_type']=='lease' ) selected @endif value="lease">Lease</option>
                                    <option @if(!empty($business_info['business_financials']['business_ownership_type']) && $business_info['business_financials']['business_ownership_type']=='mortgage' ) selected @endif value="mortgage">Mortgage</option>
                                    <option @if(!empty($business_info['business_financials']['business_ownership_type']) && $business_info['business_financials']['business_ownership_type']=='own' ) selected @endif value="own">Own</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-control-feedback text-danger text-left" id="error_business_ownership_type" style="display: none">Field is required</div>
                </div>
            </div>
        </section>
        <div class="footer">
            <div class="footer-form">
                <button type="button" name="previous" data-value="sba_assistance_needed" class="previousBtn action-button btn-primary" value="Previous" style="float: left;" />
                <span>Previous</span>
                </button>
                <button type="submit" name="next" class="btn btn-primary text-center action-button" value="Next">
                    <span>Next</span>
                </button>
            </div>
        </div>
    </form>
</fieldset>

<!-- (3th FieldSet) ** Do you have existing business debt? ** === -->
<fieldset id="fieldset_final_questions" class="fldset-steps">
    <form class="stepsform">
        <input type="hidden" name="tab_name" value="business_info">
        <input type="hidden" name="step_name" value="final_questions">
        <section class="align-self-center app-page business-info-3">
            <h3 class="text-center">Final Questions</h3>
            <div id="borrowerAttribute" class="attributeClass">
                <div class="form-group">
                    <div class="borrower-attribute-name d-block">
                        <label for="">Business Debt Amount</label>
                        <span class="borrower-attribute-tooltip"></span>
                        <div class="type-container">
                            <div id="borrowerAttributeCurrency">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">$</span>
                                    </div>
                                    <input value="{{@$business_info['final_questions']['annual_personal_income']}}" id="annual_personal_income" name="annual_personal_income" data-cy="annual_personal_income" type="text" separator="," placeholder="" class="form-control" tabindex="1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-control-feedback text-danger text-left" id="error_annual_personal_income" style="display: none">Field is required</div>
                </div>
            </div>
            <div id="borrowerAttribute" class="attributeClass">
                <div class="form-group mt-4">
                    <div class="borrower-attribute-name">
                        <label for="">Do you have existing business debt?</label>
                        <span class="borrower-attribute-tooltip"></span>
                    </div>
                    <div class="type-container">
                        <div id="borrowerAttribute" class="attributeClass">
                            <div class="form-group m-0">
                                <div class="form-check">
                                    <input @if(!empty($business_info['final_questions']['existing_business_debt']) && $business_info['final_questions']['existing_business_debt']=='Yes' ) checked @endif name="existing_business_debt" type="radio" class="form-check-input" id="exampleRadios1" value="Yes">
                                    <label class="form-check-label" for="exampleRadios1">Yes</label>
                                </div>
                            </div>
                        </div>
                        <div id="borrowerAttribute" class="attributeClass">
                            <div class="form-group m-0">
                                <div class="form-check">
                                    <input @if(!empty($business_info['final_questions']['existing_business_debt']) && $business_info['final_questions']['existing_business_debt']=='No' ) checked @endif name="existing_business_debt" type="radio" name="exampleRadios" class="form-check-input" id="exampleRadios2" value="No">
                                    <label class="form-check-label" for="exampleRadios1">No</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
        <div class="footer">
            <div class="footer-form">
                <button type="button" name="previous" data-value="final_questions" class="previousBtn action-button btn-primary" value="Previous" style="float: left;" />
                <span>Previous</span>
                </button>
                <button type="submit" name="next" class="btn btn-primary text-center action-button" value="Next">
                    <span>Next</span>
                </button>
            </div>

        </div>

    </form>
</fieldset>

</div>
</div>
@php
$arrayForUpdateData = session('arrayForUpdateData');
if ((!empty($arrayForUpdateData['is_updated']) && $arrayForUpdateData['is_updated'] == true) || Auth::check()) {
$route = route('basicinfos.update');
}else{
$route = route('basicinfo.create');
}
@endphp
<form id="finalresult" action="{{$route}}" method="POST">
    @csrf
</form>
@endsection

@push('scripts')

<script>
    $(document).ready(function() {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(".previousBtn").click(function(event) {
            event.preventDefault();
            var fieldName = $(this).data('value');
            $.ajax({
                url: "{{ url('/return-steps') }}",
                method: 'POST',
                data: 'step_name=' + fieldName,
                dataType: 'json',
                success: function(result) {
                    console.log(result);
                    $('.fldset-steps').hide();
                    if (result.nextStep != "") {
                        $('#fieldset_' + result.nextStep).hide();
                    }
                    if (result.preStep != "") {
                        $('#fieldset_' + result.preStep).show();
                    }
                }
            });

        });

        $(".stepsform").submit(function(event) {
            event.preventDefault(); //prevent default action 
            var post_url = $(this).attr("action"); //get form action url
            var request_method = $(this).attr("method"); //get form GET/POST method
            var form_data = new FormData(this); //Creates new FormData object
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: "{{ url('/basicinfo') }}",
                method: 'POST',
                data: form_data,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 800000,
                success: function(result) {
                    if (result.errors == true) {

                        $("#error_" + result.preStep).show();
                        $.each(result.errorsMsg, function(index, item) {
                            $.each(item, function(key, value) {
                                $("#" + index).removeClass('is-invalid');
                                $("#error_" + index).show();
                                $("#error_" + index).text(value);
                                $("#" + index).addClass('is-invalid');
                            });
                        });
                        return false;
                    } else {
                        $('.fldset-steps').hide();
                        $('#fieldset_' + result.nextStep).show();
                    }

                    if (typeof result.nextTabURL != 'undefined' && result.nextTabURL !=
                        '') {
                        //window.location.replace(result.nextTabURL);
                        $("#finalresult").submit();
                    }
                    //fieldset_financial_amount
                }
            });

        });
        /* ############################################# */


        function CommaFormatted(amount) {
            var delimiter = ","; // replace comma if desired 
            var d = amount;
            var i = parseInt(amount);
            if (isNaN(i)) {
                return '';
            }
            var minus = '';
            if (i < 0) {
                minus = '-';
            }
            i = Math.abs(i);
            var n = new String(i);
            var a = [];
            while (n.length > 3) {
                var nn = n.substr(n.length - 3);
                a.unshift(nn);
                n = n.substr(0, n.length - 3);
            }
            if (n.length > 0) {
                a.unshift(n);
            }
            n = a.join(delimiter);
            if (d.length < 1) {
                amount = n;
            } else {
                amount = n
            }
            amount = minus + amount;
            return amount;
        }
        $('#annual_personal_income').on('keyup', function() {
            var number = removeCommas($(this).val());
            var formattedNumber = CommaFormatted(number);
            $("#annual_personal_income").val(formattedNumber);
        });


        function removeCommas(str) {
            while (str.search(",") >= 0) {
                str = (str + "").replace(',', '');
            }
            return str;
        }

    });
</script>
@endpush
@extends('layouts.master')
@section('content')
@php
$stepsArray = session('stepsArray');
if(!empty($stepsArray['ownerinfo'])){
$owner_info = $stepsArray['ownerinfo'];
}
@endphp
<!-- ==== (OWNER INFO) TAB (CONTENT-2) START FROM HERE === -->
<div class="tab-pane fade active show" id="nav-ownerinfo" role="tabpanel" aria-labelledby="nav-ownerinfo-tab">
    <h2 class="page-header mb-0 mt-3 text-center">Standard Business Financing Application</h2>
    <!-- (OWNER INFO) TAB/FORM START FROM HERE -->
    <div id="msform">
        @if(!Auth::check())
        <fieldset id="fieldset_load_prfile" class="fldset-steps">
            <div class="form-control-feedback text-danger" id="error_load_prfile" style="display: none">
                Please Fill the required fields.
            </div>
            <form class="stepsform">
                <input type="hidden" name="tab_name" value="owner_info">
                <input type="hidden" name="step_name" value="load_prfile">
                <div class="header-info text-center">
                    <h3>Create your loan profile</h3>
                    <span class="hr-divider"><span>or</span></span>
                    <h5>Already have an account?<a class="pl-2" href="https://virtualfundassist.com/lendio/tabs/login" alt="signin">Sign in</a></h5>
                </div>
                <!-- FIRST & LAST NANME FIELD -->
                <div class="form-group row name">
                    <div class="col col-md-6 col-12">
                        <label for="firstName" class="control-label">Name</label>
                        <input value="{{@$owner_info['load_prfile']['first_name'] }}" id="first_name" data-cy="firstName" name="first_name" type="text" placeholder="First" class="form-control">
                        <div class="form-control-feedback text-danger" id="error_first_name" style="display: none">Field is required</div>
                    </div>
                    <div class="col col-md-6 col-12"><label>&nbsp;</label>
                        <input value="{{@$owner_info['load_prfile']['last_name'] }}" id="lastName" data-cy="lastName" name="last_name" type="text" placeholder="Last" class="form-control">
                    </div>
                </div>

                <!-- BUSINESS NANME FIELD -->
                <div class="form-group row business-name">
                    <div class="col col-12">
                        <label for="businessName" class="control-label">Business Name</label>
                        <input value="{{@$owner_info['load_prfile']['business_name'] }}" id="business_name" name="business_name" type="text" placeholder="Business Name" class="form-control">
                        <div class="form-control-feedback text-danger" id="error_business_name" style="display: none">Field is required</div>
                    </div>
                </div>

                <!-- MOBILE PHONE FIELD -->
                <div class="form-group row phone">
                    <div class="col col-12">
                        <label for="phone" class="control-label">Mobile Phone</label>
                        <input value="{{@$owner_info['load_prfile']['mobile_number'] }}" id="mobile_number" name="mobile_number" type="tel" placeholder="(___)___-____" class="form-control" maxlength="12">
                        <div class="form-control-feedback text-danger" id="error_mobile_number" style="display: none">Field is required</div>
                    </div>
                </div>

                <!-- EMAIL FIELD -->
                <div class="form-group row email">
                    <div class="col col-12">
                        <label for="email" class="control-label">Email address</label>
                        <div style="width: 100%;">
                            <input value="{{@$owner_info['load_prfile']['email'] }}" id="email" name="email" type="email" placeholder="Email" class="form-control">
                            <div class="form-control-feedback text-danger" id="error_email" style="display: none">Field is required</div>
                        </div>
                    </div>
                </div>

                <!-- Pass/ConfirmPass FIELD -->
                <div class="row">
                    <div class="col col-12">
                        <div class="form-group w-100">
                            <label for="password">Password</label>
                            <input type="password" id="password" name="password" class="form-control">
                        </div>
                        <div class="form-group w-100">
                            <label for="password_confirmation">Confirm Password</label>
                            <input type="password" id="password_confirmation" name="password_confirmation" class="form-control">
                            <div class="form-control-feedback text-danger" id="error_password" style="display: none">Field is required</div>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <div class="footer-form">
                        <button type="submit" name="next" class="btn btn-primary text-center action-button" value="Next">
                            <span>continue</span>
                        </button>
                    </div>
                    <div class="skip-btn d-none">
                        <a href="#">Fwd <i class="fas fa-arrow-right"></i></a>
                    </div>
                </div>
            </form>
        </fieldset>
        @endif
        <!-- Ower-info Form FieldSet-1 -->
        <fieldset id="fieldset_owner_information" class="fldset-steps">
            <form class="stepsform">
                <input type="hidden" name="tab_name" value="owner_info">
                <input type="hidden" name="step_name" value="owner_information">

                <h3>Owner Information</h3>
                <div id="" class="attributeClass">
                    <!-- <div class="form-control-feedback text-danger" id="error_owner_information" style="display: none">Please Fill the required fields.</div> -->
                    <div class="form-group">
                        <div class="borrower-attribute-name">
                            <label for="baownerAddress">Owner Address</label>
                            <span class="borrower-attribute-tooltip"></span>
                        </div>
                        <div class="type-container">
                            <div class="form-group">
                                <input value="{{@$owner_info['owner_information']['full_address'] }}" name="full_address" data-cy="borrowerStreet" type="text" placeholder="Enter your address here" class="form-control pac-target-input" autocomplete="off">
                                <div class="form-control-feedback text-danger" id="error_full_address" style="display: none">Field is required</div>
                            </div>
                            <div class="form-group">
                                <input value="{{@$owner_info['owner_information']['city'] }}" name="city" data-cy="borrowerCity" type="text" placeholder="City" class="form-control">
                                <div class="form-control-feedback text-danger" id="error_city" style="display: none"> Field is required</div>
                            </div>
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col-sm-10 col-md-6">
                                        <div class="ls-select mb-0">
                                            <svg class="svg-inline--fa fa-angle-down fa-w-10" data-fa-pseudo-element=":before" aria-hidden="true" data-fa-processed="" data-prefix="far" data-icon="angle-down" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
                                                <path fill="currentColor" d="M151.5 347.8L3.5 201c-4.7-4.7-4.7-12.3 0-17l19.8-19.8c4.7-4.7 12.3-4.7 17 0L160 282.7l119.7-118.5c4.7-4.7 12.3-4.7 17 0l19.8 19.8c4.7 4.7 4.7 12.3 0 17l-148 146.8c-4.7 4.7-12.3 4.7-17 0z">
                                                </path>
                                            </svg>
                                            @php
                                            $state_list = array('AL'=>"Alabama",
                                            'AK'=>"Alaska",'AZ'=>"Arizona",'AR'=>"Arkansas",'CA'=>"California",
                                            'CO'=>"Colorado",'CT'=>"Connecticut",
                                            'DE'=>"Delaware",'DC'=>"District Of
                                            Columbia",'FL'=>"Florida",'GA'=>"Georgia",'HI'=>"Hawaii",'ID'=>"Idaho",
                                            'IL'=>"Illinois",'IN'=>"Indiana",'IA'=>"Iowa",'KS'=>"Kansas",'KY'=>"Kentucky",'LA'=>"Louisiana",
                                            'ME'=>"Maine",'MD'=>"Maryland",'MA'=>"Massachusetts",'MI'=>"Michigan",'MN'=>"Minnesota",'MS'=>"Mississippi",
                                            'MO'=>"Missouri",'MT'=>"Montana",'NE'=>"Nebraska",'NV'=>"Nevada",'NH'=>"New
                                            Hampshire",
                                            'NJ'=>"New Jersey",'NM'=>"New Mexico",'NY'=>"New York",'NC'=>"North
                                            Carolina",'ND'=>"North Dakota",
                                            'OH'=>"Ohio",'OK'=>"Oklahoma",'OR'=>"Oregon",'PA'=>"Pennsylvania",'RI'=>"Rhode
                                            Island",
                                            'SC'=>"South Carolina",'SD'=>"South
                                            Dakota",'TN'=>"Tennessee",'TX'=>"Texas",'UT'=>"Utah",'VT'=>"Vermont",
                                            'VA'=>"Virginia", 'WA'=>"Washington",'WV'=>"West
                                            Virginia",'WI'=>"Wisconsin",'WY'=>"Wyoming");
                                            @endphp
                                            <select id="" name="state_id" data-cy="borrowerStateId" class="form-control">
                                                <option disabled="disabled" selected="selected" value="">
                                                    @foreach($state_list as $keys=> $state_names)
                                                <option value="{{$keys}}" @if(!empty($owner_info['owner_information']['state_id']) && $owner_info['owner_information']['state_id']==$keys) selected @endif>{{$state_names}}</option>
                                                @endforeach
                                            </select>
                                            <div class="form-control-feedback text-danger" id="error_state_id" style="display: none">Field is required</div>

                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-md-6">
                                        <input value="{{@$owner_info['owner_information']['zip_code'] }}" name="zip_code" data-cy="borrowerZipId" masked="true" type="text" placeholder="ZIP Code" class="form-control form-zip required">

                                        <div class="form-control-feedback text-danger" id="error_zip_code" style="display: none">Field is required</div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <div class="footer-form">
                        <button type="button" name="previous" data-value="{{@$stepsArrayMaintainer['business_start_date']['preStep']}}" class="previousBtn  action-button btn-primary" value="Previous" style="float: left;" />
                        <span>Previous</span>
                        </button>
                        <button type="submit" name="next" class="btn btn-primary text-center action-button" value="Next">
                            <span>Next</span>
                        </button>
                    </div>
                    <div class="skip-btn d-none">
                        <a href="#">Fwd <i class="fas fa-arrow-right"></i></a>
                    </div>
                </div>
            </form>
        </fieldset>

        <!-- Ower-Info Form FieldSet-2 -->
        <fieldset id="fieldset_owner_credit_verification" class="fldset-steps">

            <form class="stepsform">
                <input type="hidden" name="tab_name" value="owner_info">
                <input type="hidden" name="step_name" value="owner_credit_verification">

                <h3 class="text-center">Owner Credit Verification</h3>
                <h5 class="text-center font-regular mb-4">Don’t worry, this check won’t hurt your credit.</h5>
                <div id="borrowerAttribute" class="attributeClass">
                    <div class="form-group">
                        <div class="borrower-attribute-name">
                            <label for="">Owner Date of Birth</label>
                            <span class="borrower-attribute-tooltip"></span>
                        </div>
                        <div class="type-container">
                            <div class="form-row pb-4">
                                <div class="col">
                                    <div class="ls-select">
                                        <svg class="svg-inline--fa fa-angle-down" data-fa-pseudo-element=":before" aria-hidden="true" data-fa-processed="" data-prefix="far" data-icon="angle-down" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
                                            <path fill="currentColor" d="M151.5 347.8L3.5 201c-4.7-4.7-4.7-12.3 0-17l19.8-19.8c4.7-4.7 12.3-4.7 17 0L160 282.7l119.7-118.5c4.7-4.7 12.3-4.7 17 0l19.8 19.8c4.7 4.7 4.7 12.3 0 17l-148 146.8c-4.7 4.7-12.3 4.7-17 0z">
                                            </path>
                                        </svg>
                                        @php
                                        $months = array( 'January','February','March','April','May','June','July
                                        ','August','September',
                                        'October',
                                        'November',
                                        'December',
                                        );
                                        @endphp
                                        <select id="month" name="date_of_birth_month" class="form-control no-select">
                                            <option value="" selected="selected" >Month</option>
                                            @php
                                            $i = 1;
                                            @endphp
                                            @foreach( $months as $month)
                                            @php
                                            if($i <10) $i='0' .$i @endphp <option value="{{$i}}" @if(isset($owner_info['owner_credit_verification']['date_of_birth_month']) && $owner_info['owner_credit_verification']['date_of_birth_month']==$i) selected @endif>{{$month}}</option>
                                                @php
                                                $i++;
                                                @endphp
                                                @endforeach
                                        </select>
                                        <div class="form-control-feedback text-danger" id="error_date_of_birth_month" style="display: none">Field is required</div>

                                    </div>
                                </div>
                                <div class="col">
                                    <input type="number" min="1" max="31" id="day" name="date_of_birth_day" placeholder="01" data-cy="" class="form-control" value="{{@$owner_info['owner_credit_verification']['date_of_birth_day']}}">
                                    <div class="form-control-feedback text-danger" id="error_date_of_birth_day" style="display: none">Field is required</div>

                                </div>
                                <div class="col">
                                    <input type="number" id="year" name="date_of_birth_year" data-cy="" placeholder="2021" class="form-control" value="{{@$owner_info['owner_credit_verification']['date_of_birth_year']}}">
                                    <div class="form-control-feedback text-danger" id="error_date_of_birth_year" style="display: none">Field is required</div>

                                </div>

                            </div>



                        </div>
                    </div>
                </div>
                <div id="borrowerAttribute" class="attributeClass">
                    <div class="form-group">
                        <div class="borrower-attribute-name">
                            <label for="bacontact-ssn">Social Security Number</label>
                            <span class="borrower-attribute-tooltip">
                                <a>
                                    <div class="tooltip-wrapper">
                                        <span class="tooltip-trigger"></span>
                                        <svg class="svg-inline--fa fa-question-circle fa-w-16" aria-hidden="true" data-fa-processed="" data-prefix="far" data-icon="question-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                            <path fill="currentColor" d="M256 8C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm0 448c-110.532 0-200-89.431-200-200 0-110.495 89.472-200 200-200 110.491 0 200 89.471 200 200 0 110.53-89.431 200-200 200zm107.244-255.2c0 67.052-72.421 68.084-72.421 92.863V300c0 6.627-5.373 12-12 12h-45.647c-6.627 0-12-5.373-12-12v-8.659c0-35.745 27.1-50.034 47.579-61.516 17.561-9.845 28.324-16.541 28.324-29.579 0-17.246-21.999-28.693-39.784-28.693-23.189 0-33.894 10.977-48.942 29.969-4.057 5.12-11.46 6.071-16.666 2.124l-27.824-21.098c-5.107-3.872-6.251-11.066-2.644-16.363C184.846 131.491 214.94 112 261.794 112c49.071 0 101.45 38.304 101.45 88.8zM298 368c0 23.159-18.841 42-42 42s-42-18.841-42-42 18.841-42 42-42 42 18.841 42 42z">
                                            </path>
                                        </svg>
                                    </div>
                                </a>
                            </span>
                        </div>
                        <div class="type-container">
                            <div id="borrowerAttributeSsn">
                                <input value="{{@$owner_info['owner_credit_verification']['social_security_number'] }}" name="social_security_number" type="password" masked="true" maxlength="9" minlength="9" data-cy="contact.ssn" class="form-control" tabindex="3">
                                <div class="invalid-feedback d-flex text-danger" id="error_social_security_number" style="display: none">
                                    Please enter a 9 digit social security number
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <p class="text-center pb-4">By clicking the button below you have read and agree
                    to the <a href="#" target="_blank">Credit
                        Gathering Authorization</a> as the Principle or Business Owner that has personal liability
                    and/or authorization for the business.</p>
                <div class="footer">
                    <div class="footer-form">
                        <button type="sumbit" name="next" class="btn btn-primary text-center action-button mt-3" value="Next">
                            <span>I Accept</span>
                        </button>
                    </div>
                    <div class="skip-btn d-none">
                        <a href="#">Fwd <i class="fas fa-arrow-right"></i></a>
                    </div>
                </div>
            </form>
        </fieldset>

        <!-- Ower-Info Form FieldSet-3 -->
        <fieldset id="fieldset_owner_credit_not_verified" class="fldset-steps">
            <form class="stepsform">
                <input type="hidden" name="tab_name" value="owner_info">
                <input type="hidden" name="step_name" value="owner_credit_not_verified">

                <div id="creditCheck">
                    <h3 class="text-center">Owner Credit Verification</h3>
                    <h5 class="text-center font-regular pb-2">We weren’t able to verify your credit.
                        Please continue. Your Loan specialist will help to verify it once your app is complete.</h5>
                    <div id="experian">
                        <div class="error experian-progress text-center">
                            <img src="./images/logo.png">
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <div class="footer-form">
                        <button type="submit" name="next" class="btn btn-primary text-center action-button" value="Next">
                            <span>Next</span>
                        </button>
                    </div>
                    <div class="skip-btn d-none">
                        <a href="#">Fwd <i class="fas fa-arrow-right"></i></a>
                    </div>
                </div>
            </form>
        </fieldset>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $(document).ready(function() {
        $(".stepsform").submit(function(event) {
            event.preventDefault(); //prevent default action 
            var post_url = $(this).attr("action"); //get form action url
            var request_method = $(this).attr("method"); //get form GET/POST method
            var form_data = new FormData(this); //Creates new FormData object
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: "{{ url('/basicinfo') }}",
                method: 'POST',
                data: form_data,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 800000,
                success: function(result) {
                    $('.text-danger').hide();
                    if (result.errors == true) {
                        $("#error_" + result.preStep).show();
                        $.each(result.errorsMsg, function(index, item) {
                            $.each(item, function(key, value) {
                                $("#" + index).removeClass('is-invalid');
                                $("#error_" + index).show();
                                $("#error_" + index).text(value);
                                $("#" + index).addClass('is-invalid');
                            });
                        });

                    } else {
                        $('.fldset-steps').hide();
                        $('#fieldset_' + result.nextStep).show();
                    }

                    if (typeof result.nextTabURL != 'undefined' && result.nextTabURL !=
                        '') {
                        window.location.replace(result.nextTabURL);
                    }
                    //fieldset_financial_amount
                }
            });

        });

    });
</script>
@endpush
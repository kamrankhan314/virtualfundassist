@extends('layouts.userdashboard')
@push('styles')
@endpush
@section('content')
<div class="portalContent">
    <div class="infoNav row">
        <div class="titleNav text-left col col-12 d-flex">
            <div class="titleDetail d-flex align-items-start flex-column justify-content-between">
                <div class="titlePhotoContainer mb-auto">
                    <div class="backgroundShadow">
                        <div class="pageTitle">
                            <h1>Credit Cards</h1>
                        </div>
                    </div>
                    <div class="optionalPhoto d-block d-lg-none text-right"></div>
                </div>
                <!-- Page detail start -->
                <div class="pageDetail">
                    <p class="pageDetail">Compare and apply for a credit card to build your business
                        credit. Business credit is one of the main ways lenders determine your loan
                        eligibility. Having one or more business lines of credit can increase your
                        chances
                        of getting multiple offers. Here are your recommended credit card options.
                    </p>
                </div>
            </div>
        </div>
        <div class="managerCta text-right d-none d-sm-block col col-lg-4"></div>
    </div>
    <div></div>
    <!-- Credit Cards start here -->
    <div class="row mb-3 mt-5">
        <div class="mb-3">
            <section class="row mx-0">
                <section class="flat-card col-12 mx-0 px-0 bg-light">
                    <div class="cards-container">
                        <div class="spinner mx-auto mb-3" style="display: none;">
                        </div>
                        <span>
                            <div>
                                <div class="bg-white credit-card-14 credit-card">
                                    <!-- Best Overall -->
                                    <div class="credit-card-info col-12 mb-5 portal-shadow border-0">
                                        <div class="row">
                                            <div class="col-12 p-0">
                                                <div class="vfa-choice">
                                                    <div class="row">
                                                        <span class="vfa-choice-bg"></span>
                                                        <div class="vfa-choice-badge col-10 col-md-5 pt-1">
                                                            <img src="{{ asset('public/img/star.svg')}}" alt="">
                                                            <h3 class="font-weight-light">vfa's <span class="highlight">choice</span></h3>
                                                        </div>
                                                        <div class="vfa-choice-tag col-12 col-md-7 pt-2 pl-4 pr-0 pt-lg-1 pl-lg-1">
                                                            <h3 class="mt-1 mb-2 mb-md-0">Best Overall
                                                            </h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-12 col-md-4 p-3 card-logo text-center">
                                                    <img src="{{ asset('public/img/star.svg')}}" alt="The Blue Business® Plus Credit Card from American Express" class="mx-auto mb-3"><br>
                                                    <h5 class="mb-3 d-block d-md-none">American Express
                                                        The Blue Business® Plus Credit Card from
                                                        American Express</h5>
                                                    <button type="button" class="btn btn-primary mb-3">Apply Now</button>
                                                    <span class="disclosure">On American Express' secure
                                                        website. Terms apply.</span>
                                                </div>
                                                <div class="col-12 col-md-8 text-left p-3">
                                                    <h3 class="mb-3 d-none d-md-block">The Blue
                                                        Business® Plus Credit Card from American Express
                                                    </h3>
                                                    <div class="card-data row">
                                                        <div class="col-12 col-md-4">
                                                            <div class="row">
                                                                <h6 class="col-6 col-md-12">Rewards
                                                                    Bonus
                                                                </h6>
                                                                <h6 class="data-value col-6 col-md-12">
                                                                    N/A</h6>
                                                            </div>
                                                        </div>
                                                        <div class="col-12 col-md-4">
                                                            <div class="row">
                                                                <h6 class="col-6 col-md-12">Variable APR
                                                                </h6>
                                                                <h6 class="data-value col-6 col-md-12">
                                                                    13.24% - 19.24% </h6>
                                                            </div>
                                                        </div>
                                                        <div class="col-12 col-md-4">
                                                            <div class="row">
                                                                <h6 class="col-6 col-md-12">Annual fee
                                                                </h6>
                                                                <h6 class="data-value col-6 col-md-12">
                                                                    No annual fee</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 px-0 pt-3">
                                                        <a href="#">Rates &amp; Terms</a>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 col-12">
                                                    <span id="dots"></span>
                                                    <div id="collapse_feature1" class="collapse_feature">
                                                        <ul class="expandY-max p-4 border-top">
                                                            <li class="text-left ml-5 mb-3">Earn 2X
                                                                Membership Rewards® points on everyday
                                                                business purchases such as office
                                                                supplies or client dinners. 2X applies
                                                                to the first $50,000 in purchases per
                                                                year, 1 point per dollar thereafter.
                                                            </li>
                                                            <li class="text-left ml-5 mb-3"> Enjoy the
                                                                flexibility to put more purchases on the
                                                                Card and earn rewards when you buy above
                                                                your credit limit*. </li>
                                                            <li class="text-left ml-5 mb-3"> You’ve got
                                                                the power to use your Card beyond its
                                                                credit limit* with Expanded Buying
                                                                Power. </li>
                                                            <li class="text-left ml-5 mb-3"> More buying
                                                                power for your business means more
                                                                opportunities to earn points. That’s
                                                                everyday business with the Blue Business
                                                                Plus Card. </li>
                                                            <li class="text-left ml-5 mb-3"> *The amount
                                                                you can spend above your credit limit is
                                                                not unlimited. It adjusts with your use
                                                                of the Card, your payment history,
                                                                credit record, financial resources known
                                                                to us, and other factors. </li>
                                                            <li class="text-left ml-5 mb-3"> 0.0% intro
                                                                APR on purchases for 12 months from the
                                                                date of account opening, then a variable
                                                                rate, 13.24% - 19.24%, based on your
                                                                creditworthiness and other factors at
                                                                account opening. </li>
                                                            <li class="text-left ml-5 mb-3"> No Annual
                                                                Fee
                                                            </li>
                                                            <li class="text-left ml-5"> Terms Apply</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="px-0">
                                            <div class="col-12 border-top p-4 text-center">
                                                <a>
                                                    <span id="expand_detail_list1" class="expand_detail_list">
                                                        <span class="mr-2 btn-text"> Expand
                                                            Details</span>
                                                        <span class="arrow down"></span>
                                                    </span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <!-- Best Intro Rates -->
                                <div class="bg-white credit-card-14 credit-card">
                                    <div class="credit-card-info col-12 mb-5 portal-shadow border-0">
                                        <div class="row">
                                            <div class="col-12 p-0">
                                                <div class="vfa-choice">
                                                    <div class="row">
                                                        <span class="vfa-choice-bg"></span>
                                                        <div class="vfa-choice-badge col-10 col-md-5 pt-1">
                                                            <img src="{{ asset('public/img/star.svg')}}" alt="">
                                                            <h3 class="font-weight-light">vfa's <span class="highlight">choice</span></h3>
                                                        </div>
                                                        <div class="vfa-choice-tag col-12 col-md-7 pt-2 pl-4 pr-0 pt-lg-1 pl-lg-1">
                                                            <h3 class="mt-1 mb-2 mb-md-0">Best Intro
                                                                Rates
                                                            </h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-12 col-md-4 p-3 card-logo text-center">
                                                    <img src="{{ asset('public/img/star.svg')}}" alt="American Express Blue Business Cash" class="mx-auto mb-3"><br>
                                                    <h5 class="mb-3 d-block d-md-none">American Express
                                                        American Express Blue Business Cash™ Card</h5>
                                                    <button type="button" class="btn btn-primary mb-3">Apply Now</button>
                                                    <span class="disclosure">On American Express' secure
                                                        website. Terms apply.</span>
                                                </div>
                                                <div class="col-12 col-md-8 text-left p-3">
                                                    <h3 class="mb-3">American Express Blue Business
                                                        Cash™ Card</h3>
                                                    <div class="card-data row">
                                                        <div class="col-12 col-md-4">
                                                            <div class="row">
                                                                <h6 class="col-6 col-md-12">Rewards
                                                                    Bonus
                                                                </h6>
                                                                <h6 class="data-value col-6 col-md-12">
                                                                    N/A</h6>
                                                            </div>
                                                        </div>
                                                        <div class="col-12 col-md-4">
                                                            <div class="row">
                                                                <h6 class="col-6 col-md-12">Variable APR
                                                                </h6>
                                                                <h6 class="data-value col-6 col-md-12">
                                                                    13.24% - 19.24% </h6>
                                                            </div>
                                                        </div>
                                                        <div class="col-12 col-md-4">
                                                            <div class="row">
                                                                <h6 class="col-6 col-md-12">Annual fee
                                                                </h6>
                                                                <h6 class="data-value col-6 col-md-12">
                                                                    No annual fee</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 px-0 pt-3">
                                                        <a href="">Rates &amp; Terms</a>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <span id="dots_detail"></span>
                                                    <div id="collapse_feature2" class="collapse_feature">
                                                        <ul class="expandY-max p-4 border-top">
                                                            <li class="text-left ml-5 mb-3">Earn 2% cash
                                                                back on all eligible purchases on up to
                                                                $50,000 per calendar year, then 1%. Cash
                                                                back earned is automatically credited to
                                                                your statement. </li>
                                                            <li class="text-left ml-5 mb-3"> From
                                                                workflow to inventory to floor plans,
                                                                your business is constantly changing.
                                                                That’s why you’ve got the power to spend
                                                                beyond your credit limit with Expanded
                                                                Buying Power. </li>
                                                            <li class="text-left ml-5 mb-3"> Just
                                                                remember, the amount you can spend above
                                                                your credit limit is not unlimited. It
                                                                adjusts with your use of the Card, your
                                                                payment history, credit record,
                                                                financial resources known to us and
                                                                other factors. </li>
                                                            <li class="text-left ml-5 mb-3"> 0.0% intro
                                                                APR on purchases for 12 months from the
                                                                date of account opening, then a variable
                                                                rate, 13.24% - 19.24%, based on your
                                                                creditworthiness and other factors as
                                                                determined at the time of account
                                                                opening.
                                                            </li>
                                                            <li class="text-left ml-5 mb-3"> No Annual
                                                                Fee
                                                            </li>
                                                            <li class="text-left ml-5"> Terms Apply</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="px-0">
                                            <div class="col-12 border-top p-4 text-center">
                                                <a>
                                                    <span id="expand_detail_list2" class="expand_detail_list">
                                                        <span class="mr-2 btn-text"> Expand
                                                            Details</span>
                                                        <span class="arrow down"></span>
                                                    </span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="bg-white credit-card-14 credit-card">
                                    <div class="credit-card-info col-12 mb-5 portal-shadow border-0">
                                        <div class="container">
                                            <div class="row pt-5">
                                                <div class="col-12 col-md-4 p-3 card-logo text-center">
                                                    <img src="{{ asset('public/img/star.svg')}}" alt=" American Express® Business Gold Card" class="mx-auto mb-3"><br>
                                                    <h5 class="mb-3 d-block d-md-none">American Express
                                                        American Express® Business Gold Card</h5>
                                                    <button type="button" class="btn btn-primary mb-3">Apply Now</button>
                                                    <span class="disclosure">On American Express' secure
                                                        website. Terms apply.</span>
                                                </div>
                                                <div class="col-12 col-md-8 text-left p-3">
                                                    <h3 class="mb-3 d-none d-md-block">American Express®
                                                        Business Gold Card</h3>
                                                    <div class="card-data row">
                                                        <div class="col-12 col-md-4">
                                                            <div class="row">
                                                                <h6 class="col-6 col-md-12">Bonus Points
                                                                </h6>
                                                                <h6 class="data-value col-6 col-md-12">
                                                                    70,000 </h6>
                                                            </div>
                                                        </div>
                                                        <div class="col-12 col-md-4">
                                                            <div class="row">
                                                                <h6 class="col-6 col-md-12">Variable APR
                                                                </h6>
                                                                <h6 class="data-value col-6 col-md-12">
                                                                    14.24% - 22.24%</h6>
                                                            </div>
                                                        </div>
                                                        <div class="col-12 col-md-4">
                                                            <div class="row">
                                                                <h6 class="col-6 col-md-12">Annual fee
                                                                </h6>
                                                                <h6 class="data-value col-6 col-md-12">
                                                                    $295</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 px-0 pt-3">
                                                        <a href="#">Rates &amp; Terms</a>
                                                    </div>
                                                </div>
                                                <diiv class="col-12">
                                                    <span id="dots_detail_item"></span>
                                                    <div id="collapse_feature3" class="collapse_feature">
                                                        <ul class="expandY-max p-4 border-top">
                                                            <li class="text-left ml-5 mb-3">Limited Time
                                                                Offer: Earn 70,000 Membership Rewards®
                                                                points after you spend $10,000 on
                                                                eligible purchases with the Business
                                                                Gold Card within the first 3 months of
                                                                Card Membership. Terms Apply.
                                                            </li>
                                                            <li class="text-left ml-5 mb-3"> Earn up to
                                                                $300 in statement credits on eligible
                                                                U.S. advertising and U.S. shipping
                                                                purchases. Terms and cap apply. </li>
                                                            <li class="text-left ml-5 mb-3"> MORE
                                                                REWARDS: Get 4X Membership Rewards®
                                                                points on the 2 select categories where
                                                                your business spent the most each month.
                                                                1X is earned for other purchases. **
                                                            </li>
                                                            <li class="text-left ml-5 mb-3"> **4X points
                                                                apply to the first $150,000 in combined
                                                                purchases from these 2 categories each
                                                                calendar year.* </li>
                                                            <li class="text-left ml-5 mb-3"> AIRLINE
                                                                BONUS: Get 25% points back after you use
                                                                points for all or part of an eligible
                                                                flight booked with Amex Travel, up to
                                                                250,000 points back per calendar year.*
                                                            </li>
                                                            <li class="text-left ml-5 mb-3"> PAY OVER
                                                                TIME OPTION: A flexible payment option
                                                                with interest to help manage cash flow
                                                                on purchases of $100 or more.* </li>
                                                            <li class="text-left ml-5"> *Terms Apply
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </diiv>
                                            </div>
                                        </div>
                                        <div class="px-0">
                                            <div class="col-12 border-top p-4 text-center">
                                                <a>
                                                    <span id="expand_detail_list3" class="expand_detail_list">
                                                        <span class="mr-2 btn-text"> Expand
                                                            Details</span>
                                                        <span class="arrow down"></span>
                                                    </span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </span>
                    </div>
                </section>
            </section>
        </div>
    </div>
    <div class="disclaimer">
        <div class="disclaimerThanks"></div>
    </div>
    <div class="next-steps row justify-content-center mt-5">
        <div class="col-md-5 col-12 justify-content-center justify-content-md-start">
            <h5>Have a question?</h5>
            <div class="portal-card">
                <section>
                    <div class="mr-md-2 funding-specialist-box d-flex">
                        <div class="fm-info">
                            <div class="font-medium">Funding Manager:</div>
                            <div class="font-medium">Ghumman Dilshad</div>
                            <a href="#" class="d-block">(469) 225-0569</a>
                            <a href="#">covid@virtualfundassist.com</a>
                        </div>
                        <div class="profile-img rounded-circle embed-responsive embed-responsive-1by1 float-md-right">
                            <div class="fallback-avatar embed-responsive-item d-flex justify-content-center align-items-center
                                                h3 text-uppercase text-light font-weight-bold">GD
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <div class="col-md-6 col-12">
            <h5>While You Wait</h5>
            <div class="portal-card wyw-links">
                <div class="d-flex align-items-center wyw-link-block">
                    <div class="mr-3 icon d-flex icon-list">
                        <img src="{{ asset('public/img/icon-list.png')}}" alt="">
                    </div>
                    <div>
                        <a href="#">Explore Vfa business resources</a>
                    </div>
                </div>
                <div class="d-flex align-items-center wyw-link-block">
                    <div class="mr-3 icon d-flex icon-file">
                        <img src="{{ asset('public/img/icon-file.png')}}" alt="">
                    </div>
                    <div>
                        <a href="#">Learn more ways to use a line of credit during the pandemic</a>
                    </div>
                </div>
                <div class="d-flex align-items-center wyw-link-block">
                    <div class="mr-3 icon d-flex icon-question">
                        <img src="{{ asset('public/img/icon-question.png')}}" alt="">
                    </div>
                    <div>
                        <a href="#">Still have questions? FAQs</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom-cta"></div>
</div>
@endsection

@push('scripts') 
        <script>
            // JQuery Start here

        $(document).ready(function () {
        
            $(".expand_detail_list").on("click", function () {
                $(this).parent().parent().parent().parent().find(".collapse_feature").slideToggle(500);
                $(this).find(".arrow").toggleClass("up");

                if ( $(this).find(".arrow").hasClass("up") ) {
                    $(this).find(".btn-text").text("Collapse Details");
                } else {
                    $(this).find(".btn-text").text("Expand Details");
                }
            });
        });

        </script>
@endpush
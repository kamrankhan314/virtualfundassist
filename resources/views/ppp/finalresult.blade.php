@extends('layouts.pppapp')
 @push('styles')
 @endpush
 @section('content')
 <!-- ==== (SUBMIT) TAB (CONTENT-4) START FROM HERE === -->
<!-- ==== Tab-4-Content (Documents) START FROM HERE === -->
<div class="tab-pane fade  show active" id="document" role="tabpanel" aria-labelledby="document-tab">
    <h2 class="page-header mb-0 mt-3 text-center">Paycheck Protection Program Application</h2>
    <div class="form-group" id="msform">

   <fieldset class="fldset-steps" id="fieldset_e-sign" >
            <form class="stepsform">
                <input type="hidden" name="tab_name" value="pppdocuments">
                <input type="hidden" name="step_name" value="e-sign">

                <div class="ppp-documents mb-4">
                    <!-- Sign and Consent -->
                    <h3 class="text-center">E-Sign</h3>                  
                        <!-- Acknowledge -->
                        <div class="col-12 mb-3">
                            <span style="font-size: 14px;">*You acknowledge that the electronic signatures, initials, or
                                symbols appearing on this application are the same as handwritten signatures for the
                                purposes of validity, enforceability and admissibility.</span>
                        </div>
                    </div>
                    <!-- Certify-That -->
                    <div class="row file-support">
                        <div class="col-12 mb-3">
                            <div class="custom-control custom-checkbox">
                                <input @if(!empty($applicationAuthorization['agree_to_terms']) && $applicationAuthorization['agree_to_terms']=='Yes' ) checked @endif type="checkbox" class="custom-control-input" id="certify" name="agree_to_terms" value="Yes">
                                <label class="custom-control-label" for="certify">I certify that...</label>
                            </div>

                             <div class="invalid-feedback required-field d-flex" id="error_agree_to_terms">
                                    
                                </div>
                        </div>
                        <div class="col-12 mb-3">
                            <ul class="small ml-3">
                                <li>
                                    I have read the statements included in this form, including the Statements Required by
                                    Law and Executive Orders, and I understand them.
                                </li>
                                <li>
                                    The Applicant is eligible to receive a loan under the rules in effect at the time this
                                    application is submitted that have been issued by the Small Business Administration
                                    (SBA) and the Department of the Treasury (Treasury) implementing Second Draw Paycheck
                                    Protection Program Loans under Division A, Title I of the Coronavirus Aid, Relief, and
                                    Economic Security Act (CARES Act) and the Economic Aid to Hard-Hit Small Businesses,
                                    Nonprofits, and Venues
                                    Act (the Paycheck Protection Program Rules).
                                </li>
                                <li>The Applicant, together with its affiliates (if applicable), (1) is an independent
                                    contractor, self-employed individual, or sole proprietor with noemployees; (2) employs
                                    no more than 300 employees; or
                                    (3) if NAICS 72, employs no more than 300 employees per physical location; (4) if anews
                                    organization that is majority owned or controlled by a NAICS code 511110 or 5151
                                    business or a nonprofit public
                                    broadcasting entity with a trade or business under NAICS code 511110 or 5151, employs no
                                    more than 300 employees per location.
                                </li>
                                <li>
                                    I will comply, whenever applicable, with the civil rights and other limitations in this
                                    form.
                                </li>
                                <li>
                                    All loan proceeds will be used only for business-related purposes as specified in the
                                    loan application and consistent with the Paycheck Protection Program Rules including the
                                    prohibition on using loan proceeds for lobbying activities and expenditures.
                                    If Applicant is a news organization that became eligible for a loan under Section 317 of
                                    the Economic Aid to Hard-Hit Small Businesses, Nonprofits, and Venues Act, proceeds of
                                    the loan will be used
                                    to support expenses at the component of the business concern that produces or
                                    distributes locally focused or emergency information.
                                </li>
                                <li>
                                    I understand that SBA encourages the purchase, to the extent feasible, of American-made
                                    equipment and products.
                                </li>
                                <li>
                                    The Applicant is not engaged in any activity that is illegal under federal, state or
                                    local law.
                                </li>
                                <li>
                                    For Applicants who are individuals: I authorize the SBA to request criminal record
                                    information about me from criminal justice agencies for the purpose of determining my
                                    eligibility for programs authorized by the Small Business Act, as amended.
                                </li>
                                <li>
                                    I have read and received the
                                    <a href="/agreements/ppp-disclosures">Paycheck Protection Program disclosures</a>.
                                </li>
                                <li>By submitting, you agree to
                                    <a href="/agreements/e-signature">VFA Electronic Record and Signature Agreement</a>.
                                </li>
                            </ul>
                        </div>
                        <!-- I acknowledge and accept the terms of this loan application. -->
                        <div class="col-12">
                            <div class="custom-control custom-checkbox">
                                <input @if(!empty($applicationAuthorization['accept_terms']) && $applicationAuthorization['accept_terms']=='Yes' ) checked @endif type="checkbox" class="custom-control-input" id="acknowledge" name="accept_terms" value="Yes">
                                <label class="custom-control-label" for="acknowledge">I acknowledge and accept the terms of
                                    this loan application.</label>
                                <div class="invalid-feedback required-field d-flex" id="error_accept_terms">
                                    
                                </div>
                                
                            </div>
                        </div>
                    </div>

                </div>
                <!-- SUBMIT__BUTTON--NEXT -->
                <div class="footer">
                    <div class="footer-form">
                        <button type="button" name="previous" data-value="demographic_information" class="previousBtn action-button btn-primary" value="Previous" style="float: left;" />
                        <span>Previous</span>
                        </button>
                        <button type="submit" name="next" class="btn action-button btn-primary" value="Next" style="float: right;" />
                        <span>Next</span>
                        </button>
                    </div>
                    <div class="skip-btn d-none">
                        <a href="#">Fwd <i class="fas fa-arrow-right"></i></a>
                    </div>
                </div>

            </form>
        </fieldset>
   </div>
</div>

 @endsection
 @push('scripts')<!-- Script -->
<script>
    $(document).ready(function() { 

        $(".stepsform").submit(function(event) {
            event.preventDefault(); //prevent default action 
            var post_url = $(this).attr("action"); //get form action url
            var request_method = $(this).attr("method"); //get form GET/POST method
            var form_data = new FormData(this); //Creates new FormData object
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
 
            $.ajax({
                url: "{{route('ppp.document.store')}}",
                method: 'POST',
                data: form_data,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 800000,
                success: function(result) {
                    if (result.errors == true) {
                        $("#error_" + result.preStep).show();
                        $.each(result.errorsMsg, function(index, item) {
                            $.each(item, function(key, value) {
                                $("#" + index).removeClass('is-invalid');
                                $("#error_" + index).show();
                                $("#error_" + index).text(value);
                                $("#" + index).addClass('is-invalid');
                            });
                        });
                        return false;
                    } else {
                        $('.fldset-steps').hide();
                        $('#fieldset_' + result.nextStep).show();
                    }
                    if (typeof result.nextTabURL != 'undefined' && result.nextTabURL != '') {
                        window.location.replace(result.nextTabURL);
                    }
                }
            });

        });



        $(".previousBtn").click(function(event) {
            event.preventDefault();
            var fieldName = $(this).data('value');
            $.ajax({
                url: "{{ route('ppp.return.steps') }}",
                method: 'POST',
                data: 'step_name=' + fieldName,
                dataType: 'json',
                success: function(result) {
                    $('.fldset-steps').hide();
                    if (result.nextStep != "") {
                        $('#fieldset_' + result.nextStep).hide();
                    }
                    if (result.preStep != "") {
                        $('#fieldset_' + result.preStep).show();
                    }
                }
            });

        });
    });
    </script>
 @endpush
@extends('layouts.pppapp')
@push('styles')
<style>
</style>
<!-- CSS -->
<link rel="stylesheet" type="text/css" href="{{asset('public/dist/dropzone.css')}}">
@endpush
@section('content')

@php
$imageArray = array('png','jpe','jpeg','jpg','gif','bmp');
@endphp

<!-- ==== Tab-4-Content (Documents) START FROM HERE === -->
<div class="tab-pane fade  show active" id="document" role="tabpanel" aria-labelledby="document-tab">
    <h2 class="page-header mb-0 mt-3 text-center">Paycheck Protection Program Application</h2>
    <div class="form-group" id="msform">
        <!-- (1st FIELDSET) Documents -->
        <fieldset class="fldset-steps" id="fieldset_document_intro">
            <form class="stepsform">
                <input type="hidden" name="tab_name" value="pppdocuments">
                <input type="hidden" name="step_name" value="document_intro">

                <div class="ppp-documents">
                    <h3 class="text-center mb-0">Documents</h3>
                    <p class="text-center">To submit your application to lenders you are required to provide the following
                        documents:</p>
                    <div class="form-group row mb-4">
                        <!-- Owner-Identification -->
                        <div class="col-12 mb-1">
                            <div>
                                <p class="file-text">
                                    <svg class="svg-inline--fa fa-id-card fa-w-16 file-icon mr-2" data-icon="id-card" viewBox="0 0 512 512">
                                        <path fill="#ABB7C7" d="M404 256H300c-6.627 0-12-5.373-12-12v-24c0-6.627 5.373-12 12-12h104c6.627 0 12 5.373 12 12v24c0 6.627-5.373 12-12 12zm12 68v-24c0-6.627-5.373-12-12-12H300c-6.627 0-12 5.373-12 12v24c0 6.627 5.373 12 12 12h104c6.627 0 12-5.373 12-12zm96-212v288c0 26.51-21.49 48-48 48H48c-26.51 0-48-21.49-48-48V112c0-26.51 21.49-48 48-48h416c26.51 0 48 21.49 48 48zm-48 282V144H48v250a6 6 0 0 0 6 6h404a6 6 0 0 0 6-6zm-288-98c30.928 0 56-25.072 56-56s-25.072-56-56-56-56 25.072-56 56 25.072 56 56 56zm62.896 10.869l-13.464-4.039C211.814 313.568 194.649 320 176 320s-35.814-6.432-49.433-17.17l-13.464 4.039A24 24 0 0 0 96 329.857V372c0 6.627 5.373 12 12 12h136c6.627 0 12-5.373 12-12v-42.143a24 24 0 0 0-17.104-22.988z">
                                        </path>
                                    </svg>Identification for all owners
                                </p>
                            </div>
                        </div>
                        <!-- Payroll-Reports -->
                        <div class="col-12 mb-1">
                            <div>
                                <p class="file-text">
                                    <svg class="svg-inline--fa fa-university fa-w-16 file-icon mr-2" viewBox="0 0 512 512">
                                        <path fill="currentColor" d="M456 440h-8v-56c0-13.255-10.745-24-24-24h-16V208h-48v152h-48V208h-48v152h-48V208h-48v152h-48V208H72v152H56c-13.255 0-24 10.745-24 24v56h-8c-13.255 0-24 10.745-24 24v16a8 8 0 0 0 8 8h464a8 8 0 0 0 8-8v-16c0-13.255-10.745-24-24-24zm-56 0H80v-32h320v32zm72.267-322.942L255.179 26.463a48.004 48.004 0 0 0-30.358 0L7.733 117.058A11.999 11.999 0 0 0 0 128.274V156c0 6.627 5.373 12 12 12h20v12c0 6.627 5.373 12 12 12h392c6.627 0 12-5.373 12-12v-12h20c6.627 0 12-5.373 12-12v-27.726c0-4.982-3.077-9.445-7.733-11.216zM48 144l192-72 192 72H48z">
                                        </path>
                                    </svg>Payroll reports
                                </p>
                            </div>
                        </div>
                        <!-- Business-Tax-Documents -->
                        <div class="col-12 mb-1">
                            <div>
                                <p class="file-text">
                                    <svg class="svg-inline--fa fa-university fa-w-16 file-icon mr-2" data-icon="file-alt" viewBox="0 0 384 512">
                                        <path fill="currentColor" d="M288 248v28c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-28c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12zm-12 72H108c-6.6 0-12 5.4-12 12v28c0 6.6 5.4 12 12 12h168c6.6 0 12-5.4 12-12v-28c0-6.6-5.4-12-12-12zm108-188.1V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V48C0 21.5 21.5 0 48 0h204.1C264.8 0 277 5.1 286 14.1L369.9 98c9 8.9 14.1 21.2 14.1 33.9zm-128-80V128h76.1L256 51.9zM336 464V176H232c-13.3 0-24-10.7-24-24V48H48v416h288z">
                                        </path>
                                    </svg>Business tax documents
                                </p>
                            </div>
                            <span class="italic">The more complete your documentation, the faster your application will be
                                processed.</span>
                        </div>
                    </div>
                </div>

                <!-- SUBMIT__BUTTON--NEXT -->
                <div class="footer">
                    <div class="footer-form">
                        <button type="button" name="previous" data-value="business_state" class="previousBtn action-button btn-primary" value="Previous" style="float: left;" />
                        <span>Previous</span>
                        </button>
                        <button type="submit" name="next" class="btn action-button btn-primary" value="Next" style="float: right;" />
                        <span>Next</span>
                        </button>
                    </div>
                    <div class="skip-btn d-none">
                        <a href="#">Fwd <i class="fas fa-arrow-right"></i></a>
                    </div>
                </div>
            </form>
        </fieldset>

        <!-- (2nd FIELDSET) Documents -->
        <fieldset class="fldset-steps" id="fieldset_mandatory_documents">
            <form class="stepsform" enctype="multipart/form-data">
                <input type="hidden" name="tab_name" value="pppdocuments">
                <input type="hidden" name="step_name" value="mandatory_documents">

                <div class="ppp-documents">
                    <h3 class="text-center mb-0">Upload Identification</h3>
                    <!-- Id-Verification -->
                    <div class="row">
                        <div class="col-12">
                            <label class="mb-0"><b>ID verification:</b></label>
                            <ul class="ml-0">
                                <li>Driver's License <strong>- Front</strong></li>
                                <li>Driver's License <strong>- Back</strong></li>
                                <li>Or other government issued identification</li>
                            </ul>
                        </div>
                    </div>

                    <div class="file-support row">
                        <!-- Supported-Type-Files -->
                        <div class="col-12 mb-2">
                            <svg class="svg-inline--fa fa-id-card fa-w-16 file-icon mr-2" data-icon="file-image" viewBox="0 0 384 512">
                                <path fill="#ABB7C7" d="M369.9 97.9L286 14C277 5 264.8-.1 252.1-.1H48C21.5 0 0 21.5 0 48v416c0 26.5 21.5 48 48 48h288c26.5 0 48-21.5 48-48V131.9c0-12.7-5.1-25-14.1-34zM332.1 128H256V51.9l76.1 76.1zM48 464V48h160v104c0 13.3 10.7 24 24 24h104v288H48zm32-48h224V288l-23.5-23.5c-4.7-4.7-12.3-4.7-17 0L176 352l-39.5-39.5c-4.7-4.7-12.3-4.7-17 0L80 352v64zm48-240c-26.5 0-48 21.5-48 48s21.5 48 48 48 48-21.5 48-48-21.5-48-48-48z">
                                </path>
                            </svg>
                            <span>Supported file types
                                <span class="tooltip_hover">
                                    <a>
                                        <svg class="svg-inline--fa fa-question-circle" viewBox="0 0 512 512">
                                            <path fill="currentColor" d="M256 8C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm0 448c-110.532 0-200-89.431-200-200 0-110.495 89.472-200 200-200 110.491 0 200 89.471 200 200 0 110.53-89.431 200-200 200zm107.244-255.2c0 67.052-72.421 68.084-72.421 92.863V300c0 6.627-5.373 12-12 12h-45.647c-6.627 0-12-5.373-12-12v-8.659c0-35.745 27.1-50.034 47.579-61.516 17.561-9.845 28.324-16.541 28.324-29.579 0-17.246-21.999-28.693-39.784-28.693-23.189 0-33.894 10.977-48.942 29.969-4.057 5.12-11.46 6.071-16.666 2.124l-27.824-21.098c-5.107-3.872-6.251-11.066-2.644-16.363C184.846 131.491 214.94 112 261.794 112c49.071 0 101.45 38.304 101.45 88.8zM298 368c0 23.159-18.841 42-42 42s-42-18.841-42-42 18.841-42 42-42 42 18.841 42 42z">
                                            </path>
                                        </svg>
                                    </a>
                                    <div id="tooltip" class="h-tooltip vue-tooltip tooltip-inner" x-placement="right">
                                        <div class="tooltip-content">Supported File Types: .bmp, .gif, .jpeg, .jpg, .pdf,
                                            .png, .svg, .tif, .tiff<div class="tooltip-arrow"></div>
                                        </div>
                                    </div>
                                </span>
                            </span>
                        </div>
                        <!-- Drivers License Front -->
                        <div class="col-12 mb-4">
                            <!-- ======= (DROP-ZOE-1) Drivers License Front ========= -->
                            <div class="upload-documets d-flex align-items-center mb-3">
                                <label class="control-label"><strong>ID (Front)</strong></label>
                                <div class="parent-parent-dropzon ml-auto">
                                    <!--- ===== Drop Zone Hidden | Reset Field ===== --->
                                    <div class="preview-zone hidden" id="preview-zone">
                                        <div class="box box-solid">
                                            <div class="box-body">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="dropzone-wrapper d-flex justify-content-center align-items-center" id="dropzone-wrapper">
                                        <div class="dropzone-desc">
                                            <i class="glyphicon glyphicon-download-alt"></i>
                                            <p class="mb-2"><a>Browse files</a> or drag and drop</p>
                                        </div>
                                        <input type="file" name="id_front" class="dropzone" accept="image/x-png,image/gif,image/jpeg,.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf">
                                    </div>
                                </div>
                            </div>
                            <!-- ======= (DROP-ZONE-2) Drivers License Back ========= -->
                            <div class="upload-documets d-flex align-items-center mb-4">
                                <label class="control-label"><strong>ID (Back)</strong></label>
                                <div class="parent-parent-dropzon ml-auto">
                                    <div class="preview-zone hidden" id="preview-zone">
                                        <div class="box box-solid">
                                            <div class="box-body">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="dropzone-wrapper d-flex justify-content-center align-items-center" id="dropzone-wrapper1">
                                        <div class="dropzone-desc">
                                            <i class="glyphicon glyphicon-download-alt"></i>
                                            <p class="mb-2"><a>Browse files</a> or drag and drop</p>
                                        </div>
                                        <input type="file" name="id_back" class="dropzone" accept="image/x-png,image/gif,image/jpeg,.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf">
                                    </div>
                                </div>
                            </div>

                            <!-- ======= (DROP-ZOE-2) voided_check ========= -->
                            <div class="voided_check mb-2">
                                <span> <strong>Voided Check (Optional):</strong></span>
                                <p>Use a check from the business bank account. Account number must match bank statements.
                                </p>
                            </div>
                            <div class="upload-documets d-flex align-items-center mb-3">
                                <label class="control-label"><strong>Voided Check<br> (Optional)</strong></label>
                                <div class="parent-parent-dropzon ml-auto">
                                    <div class="preview-zone hidden" id="preview-zone">
                                        <div class="box box-solid">
                                            <div class="box-body">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="dropzone-wrapper d-flex justify-content-center align-items-center" id="dropzone-wrapper1">
                                        <div class="dropzone-desc">
                                            <i class="glyphicon glyphicon-download-alt"></i>
                                            <p class="mb-2"><a>Browse files</a> or drag and drop</p>
                                        </div>
                                        <input type="file" name="voided_check" class="dropzone" accept="image/x-png,image/gif,image/jpeg,.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf">
                                    </div>
                                </div>
                            </div>
                            <!-- Upload 1st- File-Change -->
                            <p class="file-text upload-text">Uploads:</p>
                            @if(!empty($docsInDb->id_front))
                            <div class="upload-title  d-flex mb-3" id="id_front">
                                <strong class="file-label">Upload - 1</strong>
                                <a href="" class="filename">{{$docsInDb->id_front}}</a>
                                <a href="#" class="change-link remove-document" data-id="{{$docsInDb->id}}" data-field="id_front">Remove</a>
                            </div>
                            @endif
                            @if(!empty($docsInDb->id_back))
                            <div class="upload-title  d-flex mb-3" id="id_back">
                                <strong class="file-label">Upload - 2</strong>
                                <a href="" class="filename">{{$docsInDb->id_back}}</a>
                                <a href="#" class="change-link remove-document" data-id="{{$docsInDb->id}}" data-field="id_back">Remove</a>

                            </div>
                            @endif
                            @if(!empty($docsInDb->voided_check))
                            <div class="upload-title  d-flex mb-3" id="voided_check">
                                <strong class="file-label">Upload - 3</strong>
                                <a href="" class="filename">{{$docsInDb->voided_check}}</a>
                                <a href="#" class="change-link remove-document" data-id="{{$docsInDb->id}}" data-field="voided_check">Remove</a>

                            </div>
                            @endif
                            <!-- Upload 2nd-File-Change -->

                        </div>
                    </div>

                </div>

                <!-- SUBMIT__BUTTON--NEXT -->
                <div class="footer">
                    <div class="footer-form">
                        <button type="button" name="previous" data-value="document_intro" class="previousBtn action-button btn-primary" value="Previous" style="float: left;" />
                        <span>Previous</span>
                        </button>
                        <button type="submit" name="next" class="btn action-button btn-primary" value="Next" style="float: right;" />
                        <span>Next</span>
                        </button>
                    </div>
                    <div class="skip-btn d-none">
                        <a href="#">Fwd <i class="fas fa-arrow-right"></i></a>
                    </div>
                </div>

            </form>
        </fieldset>

        <!-- (3rd FIELDSET) Documents -->
        <fieldset class="fldset-steps" id="fieldset_business_type_details">
            <form class="stepsform">
                <input type="hidden" name="tab_name" value="pppdocuments">
                <input type="hidden" name="step_name" value="business_type_details">

                <div class="ppp-documents mb-5">
                    <h3 class="text-center mb-0">Business Type Details</h3>
                    <div class="row mb-3">
                        <div class="col-12 mb-2">
                            Are you Single Member LLC or Multi Member LLC?
                        </div>
                        <!-- Single-Member -->
                        <div class="form-group d-flex flex-column">
                            <div class="custom-control custom-radio custom-control-inline pb-2 d-flex justify-content-start" style="width: 100%;">
                                <label class="form-row check_question d-flex">
                                    <input @if(!empty($basicInfo) && $basicInfo->llc_member_type == 'Single') checked @endif class="d-block mr-2" type="radio" value="Single" name="llc_member_type">
                                    <span class="checkmark"></span>
                                    Single Member

                                </label>
                            </div>
                            <!-- Multi-Member -->
                            <div class="custom-control custom-radio custom-control-inline d-flex justify-content-start" style="width: 100%;">
                                <label class="form-row check_question d-flex">
                                    <input @if(!empty($basicInfo) && $basicInfo->llc_member_type == 'Multi') checked @endif class="d-block mr-2" type="radio" value="Multi" name="llc_member_type">
                                    <span class="checkmark"></span>
                                    Multiple Members
                                    <span class="tooltip_hover">
                                        <a>
                                            <svg class="svg-inline--fa fa-question-circle" viewBox="0 0 512 512">
                                                <path fill="currentColor" d="M256 8C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm0 448c-110.532 0-200-89.431-200-200 0-110.495 89.472-200 200-200 110.491 0 200 89.471 200 200 0 110.53-89.431 200-200 200zm107.244-255.2c0 67.052-72.421 68.084-72.421 92.863V300c0 6.627-5.373 12-12 12h-45.647c-6.627 0-12-5.373-12-12v-8.659c0-35.745 27.1-50.034 47.579-61.516 17.561-9.845 28.324-16.541 28.324-29.579 0-17.246-21.999-28.693-39.784-28.693-23.189 0-33.894 10.977-48.942 29.969-4.057 5.12-11.46 6.071-16.666 2.124l-27.824-21.098c-5.107-3.872-6.251-11.066-2.644-16.363C184.846 131.491 214.94 112 261.794 112c49.071 0 101.45 38.304 101.45 88.8zM298 368c0 23.159-18.841 42-42 42s-42-18.841-42-42 18.841-42 42-42 42 18.841 42 42z">
                                                </path>
                                            </svg>
                                        </a>
                                        <div id="tooltip" class="h-tooltip vue-tooltip tooltip-inner" x-placement="right">
                                            <div class="tooltip-content">A multi member LLC is defined as two or more people own
                                                the LLC.
                                                <div class="tooltip-arrow"></div>
                                            </div>
                                        </div>
                                    </span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <!-- W-2--Employees -->
                    <div class="row">
                        <div class="long-bool-text mb-2 col-md-8">
                            Do you have W-2 employees?
                        </div>
                        <div class="col long-bool-radio d-flex">
                            <div class="custom-control custom-radio custom-control-inline pb-2" style="width: 100%;">
                                <label class="form-row check_question">
                                    <input @if(!empty($basicInfo) && $basicInfo->has_w_two_employees == 'Yes') checked @endif class="d-block mr-2" type="radio" value="Yes" name="has_w_two_employees">
                                    <span class="checkmark"></span>
                                    Yes
                                </label>
                            </div>

                            <div class="custom-control custom-radio custom-control-inline pb-2" style="width: 100%;">
                                <label class="form-row check_question">
                                    <input @if(!empty($basicInfo) && $basicInfo->has_w_two_employees == 'No') checked @endif class="d-block mr-2" type="radio" value="No" name="has_w_two_employees">
                                    <span class="checkmark"></span>
                                    No
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- SUBMIT__BUTTON--NEXT -->
                <div class="footer">
                    <div class="footer-form">
                        <button type="button" name="previous" data-value="mandatory_documents" class="previousBtn action-button btn-primary" value="Previous" style="float: left;" />
                        <span>Previous</span>
                        </button>
                        <button type="submit" name="next" class="btn action-button btn-primary" value="Next" style="float: right;" />
                        <span>Next</span>
                        </button>
                    </div>
                    <div class="skip-btn d-none">
                        <a href="#">Fwd <i class="fas fa-arrow-right"></i></a>
                    </div>
                </div>
            </form>
        </fieldset>
        <!-- If user have (Single Member) and select W-2 emplyees (Yes) then this field will be open accordingly   -->
        <!-- (4th FIELDSET) Documents -->
        <fieldset class="fldset-steps" id="fieldset_payroll_documents"   >
            <form class="stepsform" enctype="multipart/form-data">
                <input type="hidden" name="tab_name" value="pppdocuments">
                <input type="hidden" name="step_name" value="payroll_documents">
                <input type="hidden" name="type" value="Uploaded Your 2019 Payroll">
                <div class="ppp-documents mt-1">
                    <h3 class="text-center mb-3">Upload Your 2019 Payroll</h3>
                    <!-- Average-Monthly-Payroll -->
                    <div class="row">
                        <div class="col-md-8 long-bool-text mb-2" style="font-size: 13px;">
                            You entered <b>${{@$basicInfo->average_monthly_payroll}}</b> for your average monthly payroll. What year did you use to calculate
                            your payroll?
                        </div>

                        <div class="col long-bool-radio d-flex">
                            <div class="custom-control custom-radio custom-control-inline pb-2" style="width: 100%;">
                                <label class="form-row check_question">
                                <input @if(!empty($basicInfo) && !empty($basicInfo->average_payroll_year) && $basicInfo->average_payroll_year == '2019') checked @endif type="radio" id="ba967-2019" name="average_payroll_year" class="d-block mr-2" value="2019">
                                    <span class="checkmark"></span>
                                    Yes
                                </label>
                            </div>

                            <div class="custom-control custom-radio custom-control-inline pb-2" style="width: 100%;">
                                <label class="form-row check_question">
                                <input @if(!empty($basicInfo) && !empty($basicInfo->average_payroll_year) &&  $basicInfo->average_payroll_year == '2020') checked @endif type="radio" id="ba967-2020" name="average_payroll_year" class="d-block mr-2" value="2020">
                                    <span class="checkmark"></span>
                                    No
                                </label>
                            </div>
                        </div>
                    </div>
                    <!-- Choose one of the following and upload your 2019 document(s): -->
                    <div class="row">
                        <div class="col-12">
                            <p class="mt-4 subheader">Choose one of the following and upload your <b class="underline">2019</b> document(s):</p>
                            <p class="italic">Preferred documents marked with blue asterisk</p>
                        </div>


                        <!-- 2019 IRS Form 941  -->
                        <div class="custom-control custom-radio custom-control-inline pb-2 d-flex justify-content-start" style="width: 100%;">
                                <label class="form-row check_question d-flex">
                                    <input @if(!empty($extraDocument['Uploaded Your 2019 Payroll']['document_type']) && $extraDocument['Uploaded Your 2019 Payroll']['document_type']=='2019 IRS Form 941 all 4 quarters' ) checked @endif class="d-block mr-2 doc_selection" type="radio" value="2019 IRS Form 941 all 4 quarters" name="document_type">
                                    <span class="checkmark"></span>
                                    2019 IRS Form 941
                                    <span class="italic">- Provide all 4 quarters</span>
                                <svg class="svg-inline--fa fa-asterisk fa-w-17 asterisk-icon" data-icon="asterisk" role="img" viewBox="0 0 512 512">
                                    <path fill="currentColor" d="M479.31 357.216L303.999 256l175.31-101.215c5.74-3.314 7.706-10.653 4.392-16.392l-12-20.785c-3.314-5.74-10.653-7.706-16.392-4.392L280 214.431V12c0-6.627-5.373-12-12-12h-24c-6.627 0-12 5.373-12 12v202.431L56.69 113.215c-5.74-3.314-13.079-1.347-16.392 4.392l-12 20.785c-3.314 5.74-1.347 13.079 4.392 16.392L208 256 32.69 357.216c-5.74 3.314-7.706 10.653-4.392 16.392l12 20.784c3.314 5.739 10.653 7.706 16.392 4.392L232 297.569V500c0 6.627 5.373 12 12 12h24c6.627 0 12-5.373 12-12V297.569l175.31 101.215c5.74 3.314 13.078 1.347 16.392-4.392l12-20.784c3.314-5.739 1.347-13.079-4.392-16.392z">
                                    </path>
                                </svg>
                                </label>
                            </div> 
                        <!-- 2019 IRS Form 944 -->
                        <div class="custom-control custom-radio custom-control-inline pb-2 d-flex justify-content-start" style="width: 100%;">
                                <label class="form-row check_question d-flex">
                                    <input @if(!empty($extraDocument['Uploaded Your 2019 Payroll']['document_type']) && $extraDocument['Uploaded Your 2019 Payroll']['document_type']=='2019 IRS Form 944' ) checked @endif class="d-block mr-2 doc_selection" type="radio" value="2019 IRS Form 944" name="document_type">
                                    <span class="checkmark"></span>
                                    2019 IRS Form 944
                                    <span class="italic">- Provide all 4 quarters</span>
                                    <svg class="svg-inline--fa fa-asterisk fa-w-17 asterisk-icon" data-icon="asterisk" role="img" viewBox="0 0 512 512">
                                    <path fill="currentColor" d="M479.31 357.216L303.999 256l175.31-101.215c5.74-3.314 7.706-10.653 4.392-16.392l-12-20.785c-3.314-5.74-10.653-7.706-16.392-4.392L280 214.431V12c0-6.627-5.373-12-12-12h-24c-6.627 0-12 5.373-12 12v202.431L56.69 113.215c-5.74-3.314-13.079-1.347-16.392 4.392l-12 20.785c-3.314 5.74-1.347 13.079 4.392 16.392L208 256 32.69 357.216c-5.74 3.314-7.706 10.653-4.392 16.392l12 20.784c3.314 5.739 10.653 7.706 16.392 4.392L232 297.569V500c0 6.627 5.373 12 12 12h24c6.627 0 12-5.373 12-12V297.569l175.31 101.215c5.74 3.314 13.078 1.347 16.392-4.392l12-20.784c3.314-5.739 1.347-13.079-4.392-16.392z">
                                    </path>
                                </svg>
                                </label>
                            </div>

                       <div class="custom-control custom-radio custom-control-inline pb-2 d-flex justify-content-start" style="width: 100%;">
                                <label class="form-row check_question d-flex">
                                    <input @if(!empty($extraDocument['Uploaded Your 2019 Payroll']['document_type']) && $extraDocument['Uploaded Your 2019 Payroll']['document_type']=='2019 Form W-2 for all employees and Form W-3' ) checked @endif type="radio" id="ppp-941-cc" name="document_type" class="d-block mr-2 doc_selection" value="2019 Form W-2 for all employees and Form W-3">
                                    <span class="checkmark"></span>
                                   2019 Form W-2 for all employees and Form W-3 
                                </label>
                            </div> 
                        <!-- 2019 3rd-Party -->
                        <div class="custom-control custom-radio custom-control-inline pb-2 d-flex justify-content-start" style="width: 100%;">
                                <label class="form-row check_question d-flex">
                                    <input @if(!empty($extraDocument['Uploaded Your 2019 Payroll']['document_type']) && $extraDocument['Uploaded Your 2019 Payroll']['document_type']=='2019 3rd-Party Payroll Processor Report' ) checked @endif type="radio" id="ppp-941-ce" name="document_type" class="d-block mr-2 doc_selection" value="2019 3rd-Party Payroll Processor Report"><span class="checkmark"></span>
                                  2019 3rd-Party Payroll Processor Report 
                                </label>
                            </div> 


                              <!-- 2019 3rd-Party -->
                        <div class="custom-control custom-radio custom-control-inline pb-2 d-flex justify-content-start" style="width: 100%;">
                                <label class="form-row check_question d-flex">
                                    <input @if(!empty($extraDocument['Uploaded Your 2019 Payroll']['document_type']) && $extraDocument['Uploaded Your 2019 Payroll']['document_type']=='2019 February Bank Statement' ) checked @endif type="radio" id="ppp-941-ce" name="document_type" class="d-block mr-2 doc_selection" value="Febuary 2019 Bank Statement"><span class="checkmark"></span>
                                  2019 February Bank Statement 
                                </label>
                            </div> 

                    </div>
                    <!-- *Draft or fillable PDFs are not acceptable for 2019 documents===Drag-Drop -->
                    <div class="file-support row">
                        <!-- Supported-File-Types -->
                        <div class="col-12 mb-2">
                            <svg class="svg-inline--fa fa-id-card fa-w-16 file-icon mr-2" data-icon="file-image" viewBox="0 0 384 512">
                                <path fill="#ABB7C7" d="M369.9 97.9L286 14C277 5 264.8-.1 252.1-.1H48C21.5 0 0 21.5 0 48v416c0 26.5 21.5 48 48 48h288c26.5 0 48-21.5 48-48V131.9c0-12.7-5.1-25-14.1-34zM332.1 128H256V51.9l76.1 76.1zM48 464V48h160v104c0 13.3 10.7 24 24 24h104v288H48zm32-48h224V288l-23.5-23.5c-4.7-4.7-12.3-4.7-17 0L176 352l-39.5-39.5c-4.7-4.7-12.3-4.7-17 0L80 352v64zm48-240c-26.5 0-48 21.5-48 48s21.5 48 48 48 48-21.5 48-48-21.5-48-48-48z">
                                </path>
                            </svg>
                            <span>Supported file types
                                <span class="tooltip_hover">
                                    <a>
                                        <svg class="svg-inline--fa fa-question-circle" viewBox="0 0 512 512">
                                            <path fill="currentColor" d="M256 8C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm0 448c-110.532 0-200-89.431-200-200 0-110.495 89.472-200 200-200 110.491 0 200 89.471 200 200 0 110.53-89.431 200-200 200zm107.244-255.2c0 67.052-72.421 68.084-72.421 92.863V300c0 6.627-5.373 12-12 12h-45.647c-6.627 0-12-5.373-12-12v-8.659c0-35.745 27.1-50.034 47.579-61.516 17.561-9.845 28.324-16.541 28.324-29.579 0-17.246-21.999-28.693-39.784-28.693-23.189 0-33.894 10.977-48.942 29.969-4.057 5.12-11.46 6.071-16.666 2.124l-27.824-21.098c-5.107-3.872-6.251-11.066-2.644-16.363C184.846 131.491 214.94 112 261.794 112c49.071 0 101.45 38.304 101.45 88.8zM298 368c0 23.159-18.841 42-42 42s-42-18.841-42-42 18.841-42 42-42 42 18.841 42 42z">
                                            </path>
                                        </svg>
                                    </a>
                                    <div id="tooltip" class="h-tooltip vue-tooltip tooltip-inner" x-placement="right">
                                        <div class="tooltip-content">Supported File Types: .bmp, .csv, .doc, .docx, .gif,
                                            .htm, .html, .jpeg, .jpg, .odc, .odp, .odt, .ots, .p7s, .pdf, .png, .ppt, .pptx,
                                            .svg, .tif, .tiff, .tsv, .txt, .vcf, .vcs, .xls, .xlsm, .xlsx, .xps<div class="tooltip-arrow"></div>
                                        </div>
                                    </div>
                                </span>
                            </span>
                        </div>
                        <!-- 2019 IRS Form 941 (drag & drop) -->
                        <div class="col-12 mb-4">
                            <p class="italic" style="font-size:14px">*Draft or fillable PDFs are not acceptable for 2019
                                documents</p>
                            <!-- ======= (DROP-ZOE-1) Drivers License Front ========= -->
                            <div class="upload-documets d-flex align-items-center mb-3">
                                <label class="irs-quarter-label"><strong id="doc_selection">2019 IRS Form 941 (Provide all 4 quarters)</strong></label>
                                <div class="parent-parent-dropzon ml-auto">
                                    <!--- ===== Drop Zone Hidden | Reset Field ===== --->
                                    <div class="preview-zone hidden" id="preview-zone">
                                        <div class="box box-solid">
                                            <div class="box-body">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-center align-items-center" id="dropzone-wrapper1">
                                        <div class="dropzone-desc">
                                            <i class="glyphicon glyphicon-download-alt"></i>
                                            <p class="mb-2"><a>Browse files</a> or drag and drop</p>
                                        </div>
                                        <input type="file" name="document_name" class="dropzone" accept="image/x-png,image/gif,image/jpeg,.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf">
                                    </div>
                                </div>
                            </div>
                            <!-- Upload 2019 PPP IRS 941- File-Change -->
                            <p class="file-text upload-text">Uploads:</p>
                            @if(!empty($extraDocument['Uploaded Your 2019 Payroll']))

                            <input type="hidden" name="id" value="{{$extraDocument['Uploaded Your 2019 Payroll']['id']}}">
                            <div class="upload-title  d-flex mb-3">
                                <strong class="file-label">Upload - 1</strong>
                                <a href="" class="filename">{{$extraDocument['Uploaded Your 2019 Payroll']['document_name']}}</a>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                <!-- SUBMIT__BUTTON--NEXT -->
                <div class="footer">
                    <div class="footer-form">
                        <button type="button" name="previous" data-value="document_intro" class="previousBtn action-button btn-primary" value="Previous" style="float: left;" />
                        <span>Previous</span>
                        </button>
                        <button type="submit" name="next" class="btn action-button btn-primary" value="Next" style="float: right;" />
                        <span>Next</span>
                        </button>
                    </div>
                    <div class="skip-btn d-none">
                        <a href="#">Fwd <i class="fas fa-arrow-right"></i></a>
                    </div>
                </div>

            </form>
        </fieldset>

        <!-- If user have (Single Member) and select W-2 emplyees (No) then this field will be open accordingly   -->
        <!-- (5th FIELDSET) Documents -->
        <fieldset class="fldset-steps" id="fieldset_LLC_taxes">
            <form class="stepsform" enctype="multipart/form-data">
                <input type="hidden" name="tab_name" value="pppdocuments">
                <input type="hidden" name="step_name" value="LLC_taxes">
                <input type="hidden" name="type" value="Uploaded 2019 Form 1120">
                <div class="ppp-documents mt-1 mb-3">
                    <h3 class="text-center mb-3">Upload 2019 Form 1120</h3>
                    <!-- Forms used to file LLC taxes -->
                    <div class="file-support row">
                        <div class="col-12 mb-4">
                            <span>Forms used to file LLC taxes
                                <span class="tooltip_hover">
                                    <a>
                                        <svg class="svg-inline--fa fa-question-circle" viewBox="0 0 512 512">
                                            <path fill="currentColor" d="M256 8C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm0 448c-110.532 0-200-89.431-200-200 0-110.495 89.472-200 200-200 110.491 0 200 89.471 200 200 0 110.53-89.431 200-200 200zm107.244-255.2c0 67.052-72.421 68.084-72.421 92.863V300c0 6.627-5.373 12-12 12h-45.647c-6.627 0-12-5.373-12-12v-8.659c0-35.745 27.1-50.034 47.579-61.516 17.561-9.845 28.324-16.541 28.324-29.579 0-17.246-21.999-28.693-39.784-28.693-23.189 0-33.894 10.977-48.942 29.969-4.057 5.12-11.46 6.071-16.666 2.124l-27.824-21.098c-5.107-3.872-6.251-11.066-2.644-16.363C184.846 131.491 214.94 112 261.794 112c49.071 0 101.45 38.304 101.45 88.8zM298 368c0 23.159-18.841 42-42 42s-42-18.841-42-42 18.841-42 42-42 42 18.841 42 42z">
                                            </path>
                                        </svg>
                                    </a>
                                    <div id="tooltip" class="h-tooltip support_file_tooltip vue-tooltip tooltip-inner" x-placement="right">
                                        <div class="tooltip-content">How did you file your taxes?
                                            <div class="tooltip-arrow h_file_tooltip"></div>
                                        </div>
                                    </div>
                                </span>
                            </span>
                            <!-- Select LLC taxes file -->
                            <select class="form-select form-control" aria-label="Default select example" name="document_type">
                                <option selected="">Select an option</option>
                                <option @if(!empty($extraDocument['Uploaded 2019 Form 1120']['document_type']) && $extraDocument['Uploaded 2019 Form 1120']['document_type']=='From 1120' ) selected @endif value="From 1120">From 1120</option>
                                <option @if(!empty($extraDocument['Uploaded 2019 Form 1120']['document_type']) && $extraDocument['Uploaded 2019 Form 1120']['document_type']=='From 1120-S' ) selected @endif value="From 1120-S">From 1120-S</option>
                                <option @if(!empty($extraDocument['Uploaded 2019 Form 1120']['document_type']) && $extraDocument['Uploaded 2019 Form 1120']['document_type']=='From 1040 and Schedule C' ) selected @endif value="From 1040 and Schedule C">From 1040 and Schedule C</option>
                            </select>
                        </div>
                        <!-- Supported-File-Types -->
                        <div class="col-12 mb-2">
                            <svg class="svg-inline--fa fa-id-card fa-w-16 file-icon mr-2" data-icon="file-image" viewBox="0 0 384 512">
                                <path fill="#ABB7C7" d="M369.9 97.9L286 14C277 5 264.8-.1 252.1-.1H48C21.5 0 0 21.5 0 48v416c0 26.5 21.5 48 48 48h288c26.5 0 48-21.5 48-48V131.9c0-12.7-5.1-25-14.1-34zM332.1 128H256V51.9l76.1 76.1zM48 464V48h160v104c0 13.3 10.7 24 24 24h104v288H48zm32-48h224V288l-23.5-23.5c-4.7-4.7-12.3-4.7-17 0L176 352l-39.5-39.5c-4.7-4.7-12.3-4.7-17 0L80 352v64zm48-240c-26.5 0-48 21.5-48 48s21.5 48 48 48 48-21.5 48-48-21.5-48-48-48z">
                                </path>
                            </svg>
                            <span>Supported file types
                                <span class="tooltip_hover">
                                    <a>
                                        <svg class="svg-inline--fa fa-question-circle" viewBox="0 0 512 512">
                                            <path fill="currentColor" d="M256 8C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm0 448c-110.532 0-200-89.431-200-200 0-110.495 89.472-200 200-200 110.491 0 200 89.471 200 200 0 110.53-89.431 200-200 200zm107.244-255.2c0 67.052-72.421 68.084-72.421 92.863V300c0 6.627-5.373 12-12 12h-45.647c-6.627 0-12-5.373-12-12v-8.659c0-35.745 27.1-50.034 47.579-61.516 17.561-9.845 28.324-16.541 28.324-29.579 0-17.246-21.999-28.693-39.784-28.693-23.189 0-33.894 10.977-48.942 29.969-4.057 5.12-11.46 6.071-16.666 2.124l-27.824-21.098c-5.107-3.872-6.251-11.066-2.644-16.363C184.846 131.491 214.94 112 261.794 112c49.071 0 101.45 38.304 101.45 88.8zM298 368c0 23.159-18.841 42-42 42s-42-18.841-42-42 18.841-42 42-42 42 18.841 42 42z">
                                            </path>
                                        </svg>
                                    </a>
                                    <div id="tooltip" class="h-tooltip vue-tooltip tooltip-inner" x-placement="right">
                                        <div class="tooltip-content">Supported File Types: .bmp, .csv, .doc, .docx, .gif,
                                            .htm, .html, .jpeg, .jpg, .odc, .odp, .odt, .ots, .p7s, .pdf, .png, .ppt, .pptx,
                                            .svg, .tif, .tiff, .tsv, .txt, .vcf, .vcs, .xls, .xlsm, .xlsx, .xps<div class="tooltip-arrow"></div>
                                        </div>
                                    </div>
                                </span>
                            </span>
                        </div>
                        <!-- *Draft or fillable PDFs are not acceptable for 2019 documents====From 1120 -->
                        <div class="col-12 mb-4">
                            <p class="italic" style="font-size:14px">*Draft or fillable PDFs are not acceptable for 2019
                                documents</p>

                            <!-- ======= (DROP-ZOE-1) Drivers License Front ========= -->
                            <div class="upload-documets d-flex align-items-center mb-3">
                                <label><strong class="form_select">Document</strong></label>
                                <div class="parent-parent-dropzon ml-auto">
                                    <!--- ===== Drop Zone Hidden | Reset Field ===== --->
                                    <div class="preview-zone hidden" id="preview-zone">
                                        <div class="box box-solid">
                                            <div class="box-body">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-center align-items-center" id="dropzone-wrapper1">
                                        <div class="dropzone-desc">
                                            <i class="glyphicon glyphicon-download-alt"></i>
                                            <p class="mb-2"><a>Browse files</a> or drag and drop</p>
                                        </div>
                                        <input type="file" name="document_name" class="dropzone" accept="image/x-png,image/gif,image/jpeg,.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf">
                                    </div>
                                </div>
                            </div>

                            <!-- Upload1 PPP From 1120- File-Change -->
                            <p class="file-text upload-text">Uploads:</p>
                            @if(!empty($extraDocument['Uploaded 2019 Form 1120']))

                            <input type="hidden" name="id" value="{{$extraDocument['Uploaded 2019 Form 1120']['id']}}">
                            <div class="upload-title  d-flex mb-3">
                                <strong class="file-label">Upload - 1</strong>
                                <a href="" class="filename">{{$extraDocument['Uploaded 2019 Form 1120']['document_name']}}</a>
                            </div>
                            @endif
                        </div>

                    </div>
                </div>

                <!-- SUBMIT__BUTTON--NEXT -->
                <div class="footer">
                    <div class="footer-form">
                        <button type="button" name="previous" data-value="payroll_documents" class="previousBtn action-button btn-primary" value="Previous" style="float: left;" />
                        <span>Previous</span>
                        </button>
                        <button type="submit" name="next" class="btn action-button btn-primary" value="Next" style="float: right;" />
                        <span>Next</span>
                        </button>
                    </div>
                    <div class="skip-btn d-none">
                        <a href="#">Fwd <i class="fas fa-arrow-right"></i></a>
                    </div>
                </div>

            </form>

        </fieldset>

        <!-- (6th FIELDSET) Documents -->
        <fieldset class="fldset-steps" id="fieldset_additional_documents" >
            <form class="stepsform" enctype="multipart/form-data">
                <input type="hidden" name="tab_name" value="pppdocuments">
                <input type="hidden" name="step_name" value="additional_documents">
                <input type="hidden" name="type" value="Additional Documents">
                <div class="ppp-documents mt-1">
                    <h3 class="text-center mb-3">Documents</h3>
                    <div class="file-support row">
                        <!-- Choose one of the following and upload the 2020 document(s): -->
                        <div class="col-12">
                            <p class="mt-4 subheader">Choose one of the following and upload your <b class="underline">2020</b> document(s):</p>
                        </div>
                        <!-- Provide Q1 2020 -->
                        
                        <div class="custom-control custom-radio custom-control-inline pb-2 d-flex justify-content-start" style="width: 100%;">
                                <label class="form-row check_question d-flex">
                                    <input @if(!empty($extraDocument['Additional Documents']['document_type']) && $extraDocument['Additional Documents']['document_type']=='2019 IRS Form 941 - Provide Q1 2020' ) checked @endif class="d-block mr-2 doc-selection" type="radio" name="document_type"   value="2019 IRS Form 941 - Provide Q1 2020">
                                    <span class="checkmark"></span>
                                   - Provide Q1 2020
                                </label>
                            </div>

                        <!-- 3rd-Party Payroll Processor Report - Must include report from Feb 2020 -->
                

                            <div class="custom-control custom-radio custom-control-inline pb-2 d-flex justify-content-start" style="width: 100%;">
                                <label class="form-row check_question d-flex">
                                    <input @if(!empty($extraDocument['Additional Documents']['document_type']) && $extraDocument['Additional Documents']['document_type']=='February 2020 Bank Statement' ) checked @endif type="radio"  name="document_type"  value="February 2020 Bank Statement" class="d-block mr-2 doc-selection">
                                    <span class="checkmark"></span>
                                  -February 2020 Bank Statement
                                </label>
                            </div>

                           <div class="custom-control custom-radio custom-control-inline pb-2 d-flex justify-content-start" style="width: 100%;">
                                <label class="form-row check_question d-flex">
                                    <input @if(!empty($extraDocument['Additional Documents']['document_type']) && $extraDocument['Additional Documents']['document_type']=='Most Recent Bank Statement' ) checked @endif type="radio"  name="document_type"  value="Most Recent Bank Statement" class="d-block mr-2 doc-selection">
                                    <span class="checkmark"></span>
                                  - Most Recent Bank Statement
                                </label>
                            </div>

                        <!-- Supported-File-Type -->
                        <div class="col-12 mb-4 mt-4">
                            <svg class="svg-inline--fa fa-id-card fa-w-16 file-icon mr-2" data-icon="file-image" viewBox="0 0 384 512">
                                <path fill="#ABB7C7" d="M369.9 97.9L286 14C277 5 264.8-.1 252.1-.1H48C21.5 0 0 21.5 0 48v416c0 26.5 21.5 48 48 48h288c26.5 0 48-21.5 48-48V131.9c0-12.7-5.1-25-14.1-34zM332.1 128H256V51.9l76.1 76.1zM48 464V48h160v104c0 13.3 10.7 24 24 24h104v288H48zm32-48h224V288l-23.5-23.5c-4.7-4.7-12.3-4.7-17 0L176 352l-39.5-39.5c-4.7-4.7-12.3-4.7-17 0L80 352v64zm48-240c-26.5 0-48 21.5-48 48s21.5 48 48 48 48-21.5 48-48-21.5-48-48-48z">
                                </path>
                            </svg>
                            <span>Supported file types
                                <span class="tooltip_hover">
                                    <a>
                                        <svg class="svg-inline--fa fa-question-circle" viewBox="0 0 512 512">
                                            <path fill="currentColor" d="M256 8C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm0 448c-110.532 0-200-89.431-200-200 0-110.495 89.472-200 200-200 110.491 0 200 89.471 200 200 0 110.53-89.431 200-200 200zm107.244-255.2c0 67.052-72.421 68.084-72.421 92.863V300c0 6.627-5.373 12-12 12h-45.647c-6.627 0-12-5.373-12-12v-8.659c0-35.745 27.1-50.034 47.579-61.516 17.561-9.845 28.324-16.541 28.324-29.579 0-17.246-21.999-28.693-39.784-28.693-23.189 0-33.894 10.977-48.942 29.969-4.057 5.12-11.46 6.071-16.666 2.124l-27.824-21.098c-5.107-3.872-6.251-11.066-2.644-16.363C184.846 131.491 214.94 112 261.794 112c49.071 0 101.45 38.304 101.45 88.8zM298 368c0 23.159-18.841 42-42 42s-42-18.841-42-42 18.841-42 42-42 42 18.841 42 42z">
                                            </path>
                                        </svg>
                                    </a>
                                    <div id="tooltip" class="h-tooltip vue-tooltip tooltip-inner" x-placement="right">
                                        <div class="tooltip-content">Supported File Types: .bmp, .csv, .doc, .docx, .gif,
                                            .htm, .html, .jpeg, .jpg, .odc, .odp, .odt, .ots, .p7s, .pdf, .png, .ppt, .pptx,
                                            .svg, .tif, .tiff, .tsv, .txt, .vcf, .vcs, .xls, .xlsm, .xlsx, .xps<div class="tooltip-arrow"></div>
                                        </div>
                                    </div>
                                </span>
                            </span>
                        </div>
                    </div>

                    <!-- *Draft or fillable PDFs are not acceptable for 2019 documents====From 1120 -->
                    <div class="col-12 mb-4">
                        <p class="italic" style="font-size:14px">*Draft or fillable PDFs are not acceptable for 2019
                            documents</p>

                        <!-- ======= (DROP-ZOE-1) Drivers License Front ========= --> 

                    <div class="upload-documets d-flex align-items-center mb-3">
                                <label class="control-label"><strong id="doc-selection">Select File</strong></label>
                                <div class="parent-parent-dropzon ml-auto">
                                    <div class="preview-zone hidden" id="preview-zone">
                                        <div class="box box-solid">
                                            <div class="box-body">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="dropzone-wrapper d-flex justify-content-center align-items-center" id="dropzone-wrapper1">
                                        <div class="dropzone-desc">
                                            <i class="glyphicon glyphicon-download-alt"></i>
                                            <p class="mb-2"><a>Browse files</a> or drag and drop</p>
                                        </div>
                                        <input type="file" name="document_name" class="dropzone" accept="image/x-png,image/gif,image/jpeg,.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf">
                                    </div>
                                </div>
                            </div>

                    <div class="file-support row">
                        <!-- Uploads -->
                        <div class="col-12 mb-4 ">
                            <p class="file-text upload-text">Uploads:</p>
                            @if(!empty($extraDocument['Additional Documents']))
                            <input type="hidden" name="id" value="{{$extraDocument['Additional Documents']['id']}}">
                            <div class="upload-title  d-flex mb-3">
                                <strong class="file-label">Upload - 1</strong>
                                <a href="" class="filename">{{$extraDocument['Additional Documents']['document_name']}}</a>
                            </div>
                            @endif
                        </div>
                        <!--  -->

                    </div>
                </div>
                <!-- SUBMIT__BUTTON--NEXT -->
                <div class="footer">
                    <div class="footer-form">
                        <button type="button" name="previous" data-value="LLC_taxes" class="previousBtn action-button btn-primary" value="Previous" style="float: left;" />
                        <span>Previous</span>
                        </button>
                        <button type="submit" name="next" class="btn action-button btn-primary" value="Next" style="float: right;" />
                        <span>Next</span>
                        </button>
                    </div>
                    <div class="skip-btn d-none">
                        <a href="#">Fwd <i class="fas fa-arrow-right"></i></a>
                    </div>
                </div>

            </form>
        </fieldset>
        <!-- (7th FIELDSET) Documents -->
        <fieldset class="fldset-steps" id="fieldset_demographic_information"  >
            <form class="stepsform" enctype="mulitpart/form-data">
                <input type="hidden" name="tab_name" value="pppdocuments">
                <input type="hidden" name="step_name" value="demographic_information">

                <div class="ppp-documents mb-4">
                    <!-- Demographic Information (Optional) -->
                    <h2 class="text-center">Demographic Information (Optional)</h2>
                    <div class="file-support row">
                        <!-- Disclosure / Veteran/gender/race/ethnicity -->
                        <div class="col-12">
                            <p>Disclosure is voluntary and will have no bearing on the loan application decision.</p>
                            <p>Veteran/gender/race/ethnicity data is collected for program reporting purposes only.</p>
                        </div>

                        <div class="multi-field-wrapper">
                            <div class="multi-fields">


                                @php
                                $principal = 1;
                                @endphp

                                @if(!empty($demographicInformation))
                                <div class="multi-field row">
                                    <!-- Principal-1 -->
                                    <div class="col-12">
                                        <h4 class="mb-0 pb-0">Principal {{ @$principal}}</h4>
                                    </div>
                                    <!-- First-Name -->
                                    <div class="col-6 mb-2">
                                        <label class="mb-0">First Name
                                            <span class="tooltip_hover">
                                                <a>
                                                    <svg class="svg-inline--fa fa-question-circle" viewBox="0 0 512 512">
                                                        <path fill="currentColor" d="M256 8C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm0 448c-110.532 0-200-89.431-200-200 0-110.495 89.472-200 200-200 110.491 0 200 89.471 200 200 0 110.53-89.431 200-200 200zm107.244-255.2c0 67.052-72.421 68.084-72.421 92.863V300c0 6.627-5.373 12-12 12h-45.647c-6.627 0-12-5.373-12-12v-8.659c0-35.745 27.1-50.034 47.579-61.516 17.561-9.845 28.324-16.541 28.324-29.579 0-17.246-21.999-28.693-39.784-28.693-23.189 0-33.894 10.977-48.942 29.969-4.057 5.12-11.46 6.071-16.666 2.124l-27.824-21.098c-5.107-3.872-6.251-11.066-2.644-16.363C184.846 131.491 214.94 112 261.794 112c49.071 0 101.45 38.304 101.45 88.8zM298 368c0 23.159-18.841 42-42 42s-42-18.841-42-42 18.841-42 42-42 42 18.841 42 42z">
                                                        </path>
                                                    </svg>
                                                </a>
                                                <div id="tooltip" class="h-tooltip vue-tooltip tooltip-inner" x-placement="right">
                                                    <div class="tooltip-content">The term “Principal” means:
                                                        1. For a self-employed individual, independent contractor, or a sole proprietor,
                                                        the self-employed individual, independent contractor, or sole proprietor.
                                                        2. For a partnership, all general partners and all limited partners owning 20%
                                                        or more of the equity of the Borrower, or any partner that is involved in the
                                                        management of the Borrower’s business.
                                                        3. For a corporation, all owners of 20% or more of the Borrower, and each
                                                        officer and director.
                                                        4. For a limited liability company, all members owning 20% or more of the
                                                        Borrower, and each officer and director.
                                                        5. Any individual hired by the Borrower to manage the day-to-day operations of
                                                        the Borrower (“key employee”).
                                                        6. Any trustor (if the Borrower is owned by a trust).
                                                        7. For a nonprofit organization, the officers and directors of the Borrower.<div class="tooltip-arrow"></div>
                                                    </div>
                                                </div>
                                            </span>
                                        </label>
                                        <input id="firstName" name="first_name[]" value="" placeholder="First and Middle" class="form-control">
                                    </div>
                                    <!-- Last-Name -->
                                    <div class="col-6 mb-2">
                                        <label class="mb-0">Last Name
                                        </label>
                                        <input id="lastName" name="last_name[]" value="" placeholder="Last" class="form-control">
                                    </div>
                                    <!-- Position -->
                                    <div class="col-6 mb-2">
                                        <label class="mb-0">Position
                                            <span class="tooltip_hover">
                                                <a>
                                                    <svg class="svg-inline--fa fa-question-circle" viewBox="0 0 512 512">
                                                        <path fill="currentColor" d="M256 8C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm0 448c-110.532 0-200-89.431-200-200 0-110.495 89.472-200 200-200 110.491 0 200 89.471 200 200 0 110.53-89.431 200-200 200zm107.244-255.2c0 67.052-72.421 68.084-72.421 92.863V300c0 6.627-5.373 12-12 12h-45.647c-6.627 0-12-5.373-12-12v-8.659c0-35.745 27.1-50.034 47.579-61.516 17.561-9.845 28.324-16.541 28.324-29.579 0-17.246-21.999-28.693-39.784-28.693-23.189 0-33.894 10.977-48.942 29.969-4.057 5.12-11.46 6.071-16.666 2.124l-27.824-21.098c-5.107-3.872-6.251-11.066-2.644-16.363C184.846 131.491 214.94 112 261.794 112c49.071 0 101.45 38.304 101.45 88.8zM298 368c0 23.159-18.841 42-42 42s-42-18.841-42-42 18.841-42 42-42 42 18.841 42 42z">
                                                        </path>
                                                    </svg>
                                                </a>
                                                <div id="tooltip" class="h-tooltip vue-tooltip tooltip-inner" x-placement="right">
                                                    <div class="tooltip-content"> Identify the Principal’s position: self-imployed
                                                        individual, indepedent contractor, sole proprietor, general partner, general
                                                        partner, owner, officer, director, member, key employee
                                                        <div class="tooltip-arrow"></div>
                                                    </div>
                                                </div>
                                            </span>
                                        </label>
                                        <select name="position[]" class="form-control">
                                            <option value="" disabled="disabled">Select
                                            </option>
                                            <option value="Self-Employed Individual">Self-Employed Individual</option>
                                            <option value="Independent Contractor">Independent Contractor</option>
                                            <option value="Sole Proprietor">Sole Proprietor</option>
                                            <option value="General Partner">General Partner</option>
                                            <option value="Owner">Owner</option>
                                            <option value="Officer">Officer</option>
                                            <option value="Director">Director</option>
                                            <option value="Member">Member</option>
                                            <option value="Key Employee">Key Employee</option>
                                        </select>
                                    </div>
                                    <!-- Vetern-Status -->
                                    <div class="col-6 mb-2">
                                        <label class="mb-0">Veteran Status
                                        </label>
                                        <select name="veteran_status[]" class="form-control">
                                            <option value="null" disabled="disabled">Select</option>
                                            <option value="Non-Veteran">Non-Veteran</option>
                                            <option value="Veteran">Veteran</option>
                                            <option value="Disabled Veteran">Disabled Veteran</option>
                                            <option value="Spouse of Veteran">Spouse of Veteran</option>
                                            <option value="Not Disclosed">Not Disclosed</option>
                                        </select>
                                    </div>
                                    <!-- Gender -->

                                    <div class="col-6 mb-3">
                                        <label class="mb-0">Gender</label>
                                        <select class="form-control" name="gender[]">
                                            <option value="" disabled="disabled">Select</option>
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                            <option value="Not Disclosed">Not Disclosed</option>
                                        </select>
                                    </div>
                                    <!-- Race -->
                                    <div class="col-6 mb-3">
                                        <label class="mb-0">Race</label>
                                        <select class="form-control" name="race[]">
                                            <option value="" disabled="disabled">Select</option>
                                            <option value="American Indian or Alaska Native">American Indian or Alaska Native</option>
                                            <option value="Asian">Asian</option>
                                            <option value="Black or African American">Black or African American</option>
                                            <option value="Native Hawaiian or Pacific Islander">Native Hawaiian or Pacific Islander</option>
                                            <option value="White">White</option>
                                            <option value="Not Disclosed">Not Disclosed</option>
                                        </select>
                                    </div>
                                    <!-- Ethnicity -->
                                    <div class="col-6 mb-3">
                                        <label class="mb-0">Ethnicity</label>
                                        <select class="form-control" name="ethnicity[]">
                                            <option value="" disabled="disabled">Select</option>
                                            <option value="Hispanic or Latino">Hispanic or Latino</option>
                                            <option value="Not Hispanic or Latino">Not Hispanic or Latino</option>
                                            <option value="Not Disclosed">Not Disclosed</option>
                                        </select>
                                    </div>
                                    <div class="col-12 mb-3" style="border-bottom: 1px solid #E8E7E7;"></div>
                                </div>{{-- multi-fields --}}
                                @else
                                @foreach($demographicInformation as $demographicInfo)

                                <div class="multi-field">
                                    <!-- Principal-1 -->
                                    <div class="col-12">
                                        <h4 class="mb-0 pb-0">Principal {{$principal}}</h4>
                                    </div>
                                    <!-- First-Name -->
                                    <div class="col-6 mb-2">
                                        <label class="mb-0">First Name
                                            <span class="tooltip_hover">
                                                <a>
                                                    <svg class="svg-inline--fa fa-question-circle" viewBox="0 0 512 512">
                                                        <path fill="currentColor" d="M256 8C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm0 448c-110.532 0-200-89.431-200-200 0-110.495 89.472-200 200-200 110.491 0 200 89.471 200 200 0 110.53-89.431 200-200 200zm107.244-255.2c0 67.052-72.421 68.084-72.421 92.863V300c0 6.627-5.373 12-12 12h-45.647c-6.627 0-12-5.373-12-12v-8.659c0-35.745 27.1-50.034 47.579-61.516 17.561-9.845 28.324-16.541 28.324-29.579 0-17.246-21.999-28.693-39.784-28.693-23.189 0-33.894 10.977-48.942 29.969-4.057 5.12-11.46 6.071-16.666 2.124l-27.824-21.098c-5.107-3.872-6.251-11.066-2.644-16.363C184.846 131.491 214.94 112 261.794 112c49.071 0 101.45 38.304 101.45 88.8zM298 368c0 23.159-18.841 42-42 42s-42-18.841-42-42 18.841-42 42-42 42 18.841 42 42z">
                                                        </path>
                                                    </svg>
                                                </a>
                                                <div id="tooltip" class="h-tooltip vue-tooltip tooltip-inner" x-placement="right">
                                                    <div class="tooltip-content">The term “Principal” means:
                                                        1. For a self-employed individual, independent contractor, or a sole proprietor,
                                                        the self-employed individual, independent contractor, or sole proprietor.
                                                        2. For a partnership, all general partners and all limited partners owning 20%
                                                        or more of the equity of the Borrower, or any partner that is involved in the
                                                        management of the Borrower’s business.
                                                        3. For a corporation, all owners of 20% or more of the Borrower, and each
                                                        officer and director.
                                                        4. For a limited liability company, all members owning 20% or more of the
                                                        Borrower, and each officer and director.
                                                        5. Any individual hired by the Borrower to manage the day-to-day operations of
                                                        the Borrower (“key employee”).
                                                        6. Any trustor (if the Borrower is owned by a trust).
                                                        7. For a nonprofit organization, the officers and directors of the Borrower.<div class="tooltip-arrow"></div>
                                                    </div>
                                                </div>
                                            </span>
                                        </label>
                                        <input id="firstName" name="first_name[]" value="{{@$demographicInfo->first_name}}" placeholder="First and Middle" class="form-control">
                                    </div>
                                    <!-- Last-Name -->
                                    <div class="col-6 mb-2">
                                        <label class="mb-0">Last Name
                                        </label>
                                        <input id="lastName" name="last_name[]" value="{{@$demographicInfo->last_name}}" placeholder="Last" class="form-control">
                                    </div>
                                    <!-- Position -->
                                    <div class="col-6 mb-2">
                                        <label class="mb-0">Position
                                            <span class="tooltip_hover">
                                                <a>
                                                    <svg class="svg-inline--fa fa-question-circle" viewBox="0 0 512 512">
                                                        <path fill="currentColor" d="M256 8C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm0 448c-110.532 0-200-89.431-200-200 0-110.495 89.472-200 200-200 110.491 0 200 89.471 200 200 0 110.53-89.431 200-200 200zm107.244-255.2c0 67.052-72.421 68.084-72.421 92.863V300c0 6.627-5.373 12-12 12h-45.647c-6.627 0-12-5.373-12-12v-8.659c0-35.745 27.1-50.034 47.579-61.516 17.561-9.845 28.324-16.541 28.324-29.579 0-17.246-21.999-28.693-39.784-28.693-23.189 0-33.894 10.977-48.942 29.969-4.057 5.12-11.46 6.071-16.666 2.124l-27.824-21.098c-5.107-3.872-6.251-11.066-2.644-16.363C184.846 131.491 214.94 112 261.794 112c49.071 0 101.45 38.304 101.45 88.8zM298 368c0 23.159-18.841 42-42 42s-42-18.841-42-42 18.841-42 42-42 42 18.841 42 42z">
                                                        </path>
                                                    </svg>
                                                </a>
                                                <div id="tooltip" class="h-tooltip vue-tooltip tooltip-inner" x-placement="right">
                                                    <div class="tooltip-content"> Identify the Principal’s position: self-imployed
                                                        individual, indepedent contractor, sole proprietor, general partner, general
                                                        partner, owner, officer, director, member, key employee
                                                        <div class="tooltip-arrow"></div>
                                                    </div>
                                                </div>
                                            </span>
                                        </label>
                                        <select name="position[]" class="form-control">
                                            <option data-v-387e9f75="" value="" disabled="disabled">Select</option>
                                            <option @if(!empty($demographicInfo->position) && $demographicInfo->position == 'Self-Employed Individual') selected="selected" @endif value="Self-Employed Individual">Self-Employed Individual</option>
                                            <option @if(!empty($demographicInfo->position) && $demographicInfo->position == 'Independent Contractor') selected="selected" @endif value="Independent Contractor">Independent Contractor</option>
                                            <option @if(!empty($demographicInfo->position) && $demographicInfo->position == 'Sole Proprietor') selected="selected" @endif value="Sole Proprietor">Sole Proprietor</option>
                                            <option @if(!empty($demographicInfo->position) && $demographicInfo->position == 'General Partner') selected="selected" @endif value="General Partner">General Partner</option>
                                            <option @if(!empty($demographicInfo->position) && $demographicInfo->position == 'Owner') selected="selected" @endif value="Owner">Owner</option>
                                            <option @if(!empty($demographicInfo->position) && $demographicInfo->position == 'Officer') selected="selected" @endif value="Officer">Officer</option>
                                            <option @if(!empty($demographicInfo->position) && $demographicInfo->position == 'Director') selected="selected" @endif value="Director">Director</option>
                                            <option @if(!empty($demographicInfo->position) && $demographicInfo->position == 'Member') selected="selected" @endif value="Member">Member</option>
                                            <option @if(!empty($demographicInfo->position) && $demographicInfo->position == 'Key Employee') selected="selected" @endif value="Key Employee">Key Employee</option>
                                        </select>
                                    </div>
                                    <!-- Vetern-Status -->
                                    <div class="col-6 mb-2">
                                        <label class="mb-0">Veteran Status
                                        </label>
                                        <select name="veteran_status[]" class="form-control">
                                            <option data-v-387e9f75="" value="null" disabled="disabled">Select
                                            </option>
                                            <option @if(!empty($demographicInfo->veteran_status) && $demographicInfo->veteran_status == 'Non-Veteran') selected="selected" @endif value="Non-Veteran">Non-Veteran</option>
                                            <option @if(!empty($demographicInfo->veteran_status) && $demographicInfo->veteran_status == 'Veteran') selected="selected" @endif value="Veteran">Veteran</option>
                                            <option @if(!empty($demographicInfo->veteran_status) && $demographicInfo->veteran_status == 'Disabled Veteran') selected="selected" @endif value="Disabled Veteran">Disabled Veteran</option>
                                            <option @if(!empty($demographicInfo->veteran_status) && $demographicInfo->veteran_status == 'Spouse of Veteran') selected="selected" @endif value="Spouse of Veteran">Spouse of Veteran</option>
                                            <option @if(!empty($demographicInfo->veteran_status) && $demographicInfo->veteran_status == 'Not Disclosed') selected="selected" @endif value="Not Disclosed">Not Disclosed</option>
                                        </select>
                                    </div>
                                    <!-- Gender -->

                                    <div class="col-6 mb-3">
                                        <label class="mb-0">Gender</label>
                                        <select class="form-control" name="gender[]">
                                            <option value="" disabled="disabled">Select</option>
                                            <option value="Male" @if(!empty($demographicInfo->gender) && $demographicInfo->gender == 'Male') selected @endif >Male</option>
                                            <option value="Female" @if(!empty($demographicInfo->gender) && $demographicInfo->gender == 'Female') selected @endif >Female</option>
                                            <option value="Not Disclosed" @if(!empty($demographicInfo->gender) && $demographicInfo->gender == 'Not Disclosed') selected @endif >Not Disclosed</option>

                                        </select>
                                    </div>
                                    <!-- Race -->
                                    <div class="col-6 mb-3">
                                        <label class="mb-0">Race</label>
                                        <select class="form-control" name="race[]">
                                            <option value="" disabled="disabled">Select</option>
                                            <option @if(!empty($demographicInfo->race) && $demographicInfo->race == 'American Indian or Alaska Native') selected="selected" @endif value="American Indian or Alaska Native">American Indian or Alaska Native</option>
                                            <option @if(!empty($demographicInfo->race) && $demographicInfo->race == 'Asian') selected="selected" @endif value="Asian">Asian</option>
                                            <option @if(!empty($demographicInfo->race) && $demographicInfo->race == 'Black or African American') selected="selected" @endif value="Black or African American">Black or African American</option>
                                            <option @if(!empty($demographicInfo->race) && $demographicInfo->race == 'Native Hawaiian or Pacific Islander') selected="selected" @endif value="Native Hawaiian or Pacific Islander">Native Hawaiian or Pacific Islander</option>
                                            <option @if(!empty($demographicInfo->race) && $demographicInfo->race == 'White') selected="selected" @endif value="White">White</option>
                                            <option @if(!empty($demographicInfo->race) && $demographicInfo->race == 'Not Disclosed') selected="selected" @endif value="Not Disclosed">Not Disclosed</option>
                                        </select>
                                    </div>
                                    <!-- Ethnicity -->
                                    <div class="col-6 mb-3">
                                        <label class="mb-0">Ethnicity</label>
                                        <select class="form-control" name="ethnicity[]">
                                            <option value="" disabled="disabled">Select</option>
                                            <option @if(!empty($demographicInfo->ethnicity) && $demographicInfo->ethnicity == 'Hispanic or Latino') selected="selected" @endif value="Hispanic or Latino">Hispanic or Latino</option>
                                            <option @if(!empty($demographicInfo->ethnicity) && $demographicInfo->ethnicity == 'Not Hispanic or Latino') selected="selected" @endif value="Not Hispanic or Latino">Not Hispanic or Latino</option>
                                            <option @if(!empty($demographicInfo->ethnicity) && $demographicInfo->ethnicity == 'Not Disclosed') selected="selected" @endif value="Not Disclosed">Not Disclosed</option>
                                        </select>
                                    </div>
                                    <div class="col-12 mb-3" style="border-bottom: 1px solid #E8E7E7;"></div>
                                </div>{{-- multi-fields --}}
                                @php
                                $principal ++;
                                @endphp
                                @endforeach
                                @endif


                            </div>
                        </div> {{-- Principal Ends here --}}

                        <!-- Add another principal -->
                        <div class="col-12 mb-2">
                            <a class="add_another_principal">Add another principal</a>
                        </div>
                        <!-- Skip -->
                        <div class="col-12 text-center">
                            <a href="#">Skip</a>
                        </div>
                    </div>
                </div>

                <!-- SUBMIT__BUTTON--NEXT -->
                <div class="footer">
                    <div class="footer-form">
                        <button type="button" name="previous" data-value="additional_documents" class="previousBtn action-button btn-primary" value="Previous" style="float: left;" />
                        <span>Previous</span>
                        </button>
                        <button type="submit" name="next" class="btn action-button btn-primary" value="Next" style="float: right;" />
                        <span>Next</span>
                        </button>
                    </div>
                    <div class="skip-btn d-none">
                        <a href="#">Fwd <i class="fas fa-arrow-right"></i></a>
                    </div>
                </div>

            </form>
        </fieldset>
        @if(!empty($applicationAuthorization['certifications']))
        @php
        $certifications = json_decode($applicationAuthorization['certifications']);
        @endphp
        @endif
        <!-- (8th FIELDSET) Documents -->
        <fieldset class="fldset-steps" id="fieldset_sign_and_consent" >
            <form class="stepsform">
                <input type="hidden" name="tab_name" value="pppdocuments">
                <input type="hidden" name="step_name" value="sign_and_consent">

                <div class="ppp-documents mb-4">
                    <!-- Sign and Consent -->
                    <h3 class="text-center">Sign and Consent</h3>
                    <div class="file-support row mb-3">
                        <!-- Certifications -->
                        <div class="col-12">
                            <h3 class="mb-0">Certifications</h3>
                            <span>By Checking the Boxes, Comspanleting the Form, and Signing Below, You Make the Following
                                Representations, Authorizations, and Certifications</span>
                        </div>
                    </div>
                    <!-- 1st-Selected-row -->
                    <div class="row mb-3">
                        <div class="col-11">
                            The Applicant was in operation on February 15, 2020, has not permanently closed, and was either
                            an eligible self-employed individual, independent contractor, or sole proprietorship with no
                            employees, or had employees for whom it paid salaries and payroll
                            taxes or paid independent contractors, as reported on Form(s) 1099-MISC.
                        </div>
                        <div class="custom-control custom-checkbox col-1 pl-0 pr-0">
                            <input @if(!empty($certifications->question1) && $certifications->question1 == 'Yes') checked @endif type="checkbox" id="att1" name="certifications[question1]" value="Yes" class="custom-control-input">
                            <label for="att1" class="custom-control-label custom-certificate-label">Yes</label>
                        </div>
                    </div>
                    <!-- 2nd-Selected-row -->
                    <div class="row mb-3">
                        <div class="col-11">
                            Current economic uncertainty makes this loan request necessary to support the ongoing operations
                            of the Applicant.
                        </div>
                        <div class="custom-control custom-checkbox col-1 pl-0 pr-0">
                            <input @if(!empty($certifications->question2) && $certifications->question2 == 'Yes') checked @endif type="checkbox" id="att2" name="certifications[question2]" value="Yes" class="custom-control-input">
                            <label for="att2" class="custom-control-label custom-certificate-label">Yes</label>
                        </div>
                    </div>
                    <!-- 3rd-Selected-row -->
                    <div class="row mb-3">
                        <div class="col-11">
                            The Applicant has realized a reduction in gross receipts in excess of 25% relative to the
                            relevant comparison time period. For loans greater than $150,000, Applicant has provided
                            documentation to the lender substantiating the decline in gross receipts.
                            For loans of $150,000 or less, Applicant will provide documentation substantiating the decline
                            in gross receipts upon or before seeking loan forgiveness for the Second Draw Paycheck
                            Protection Program Loan
                            or upon SBA request.
                        </div>
                        <div class="custom-control custom-checkbox col-1 pl-0 pr-0">
                            <input @if(!empty($certifications->question3) && $certifications->question3 == 'Yes') checked @endif type="checkbox" id="att3" name="certifications[question3]" value="Yes" class="custom-control-input">
                            <label for="att3" class="custom-control-label custom-certificate-label">Yes</label>
                        </div>
                    </div>
                    <!-- 4th-Selected-row -->
                    <div class="row mb-3">
                        <div class="col-11">
                            The Applicant received a First Draw Paycheck Protection Program Loan and, before the Second Draw
                            Paycheck Protection Program Loan is disbursed, will have used the full loan amount (including
                            any increase) of the First Draw Paycheck Protection Program
                            Loan only for eligible expenses.
                        </div>
                        <div class="custom-control custom-checkbox col-1 pl-0 pr-0">
                            <input @if(!empty($certifications->question4) && $certifications->question4 == 'Yes') checked @endif type="checkbox" id="att4" name="certifications[question4]" value="Yes" class="custom-control-input">
                            <label for="att4" class="custom-control-label custom-certificate-label">Yes</label>
                        </div>
                    </div>
                    <!-- 5th-Selected-row -->
                    <div class="row mb-3">
                        <div class="col-11">
                            The funds will be used to retain workers and maintain payroll; or make payments for mortgage
                            interest, rent, utilities, covered operations expenditures, covered property damage costs,
                            covered supplier costs, and covered worker protection expenditures
                            as specified under the Paycheck Protection Program Rules; I understand that if the funds are
                            knowingly used for unauthorized purposes, the federal government may hold me legally liable,
                            such as for charges
                            of fraud.
                        </div>
                        <div class="custom-control custom-checkbox col-1 pl-0 pr-0">
                            <input @if(!empty($certifications->question5) && $certifications->question5 == 'Yes') checked @endif type="checkbox" id="att5" name="certifications[question5]" value="Yes" class="custom-control-input">
                            <label for="att5" class="custom-control-label custom-certificate-label">Yes</label>
                        </div>
                    </div>
                    <!-- 6th-Selected-row -->
                    <div class="row mb-3">
                        <div class="col-11">
                            I understand that loan forgiveness will be provided for the sum of documented payroll costs,
                            covered mortgage interest payments, covered rent payments, covered utilities, covered operations
                            expenditures, covered property damage costs, covered supplier
                            costs, and covered worker protection expenditures, and not more than 40% of the forgiven amount
                            may be for non-payroll costs. If required, the Applicant will provide to the Lender and/or SBA
                            documentation
                            verifying the number of full-time equivalent employees on the Applicant’s payroll as well as the
                            dollar amounts of eligible expenses for the covered period following this loan.
                        </div>
                        <div class="custom-control custom-checkbox col-1 pl-0 pr-0">
                            <input @if(!empty($certifications->question6) && $certifications->question6 == 'Yes') checked @endif type="checkbox" id="att6" name="certifications[question6]" value="Yes" class="custom-control-input">
                            <label for="att6" class="custom-control-label custom-certificate-label">Yes</label>
                        </div>
                    </div>
                    <!-- 7th-Selected-row -->
                    <div class="row mb-3">
                        <div class="col-11">
                            The Applicant has not and will not receive another Second Draw Paycheck Protection Program Loan.
                        </div>
                        <div class="custom-control custom-checkbox col-1 pl-0 pr-0">
                            <input @if(!empty($certifications->question7) && $certifications->question7 == 'Yes') checked @endif type="checkbox" id="att7" name="certifications[question7]" value="Yes" class="custom-control-input">
                            <label for="att7" class="custom-control-label custom-certificate-label">Yes</label>
                        </div>
                    </div>
                    <!-- 8th-Selected-row -->
                    <div class="row mb-3">
                        <div class="col-11">
                            The Applicant has not and will not receive a Shuttered Venue Operator grant from SBA.
                        </div>
                        <div class="custom-control custom-checkbox col-1 pl-0 pr-0">
                            <input @if(!empty($certifications->question8) && $certifications->question8 == 'Yes') checked @endif type="checkbox" id="att8" name="certifications[question8]" value="Yes" class="custom-control-input">
                            <label for="att8" class="custom-control-label custom-certificate-label">Yes</label>
                        </div>
                    </div>
                    <!-- 9th-Selected-row -->
                    <div class="row mb-3">
                        <div class="col-11">
                            The President, the Vice President, the head of an Executive department, or a Member of Congress,
                            or the spouse of such person as determined under applicable common law, does not directly or
                            indirectly hold a controlling interest in the Applicant, with
                            such terms having the meanings provided in Section 322 of the Economic Aid to Hard-Hit Small
                            Businesses, Nonprofits, and Venues Act.
                        </div>
                        <div class="custom-control custom-checkbox col-1 pl-0 pr-0">
                            <input @if(!empty($certifications->question9) && $certifications->question9 == 'Yes') checked @endif type="checkbox" id="att9" name="certifications[question9]" value="Yes" class="custom-control-input">
                            <label for="att9" class="custom-control-label custom-certificate-label">Yes</label>
                        </div>
                    </div>
                    <!-- 10th-Selected-row -->
                    <div class="row mb-3">
                        <div class="col-11">
                            The Applicant is not an issuer, the securities of which are listed on an exchange registered as
                            a national securities exchange under section 6 of the Securities Exchange Act of 1934 (15 U.S.C.
                            78f).
                        </div>
                        <div class="custom-control custom-checkbox col-1 pl-0 pr-0">
                            <input @if(!empty($certifications->question10) && $certifications->question10 == 'Yes') checked @endif type="checkbox" id="att10" name="certifications[question10]" value="Yes" class="custom-control-input">
                            <label for="att10" class="custom-control-label custom-certificate-label">Yes</label>
                        </div>
                    </div>
                    <!-- 11th-Selected-row -->
                    <div class="row mb-3">
                        <div class="col-11">
                            The Applicant is not a business concern or entity (a) for which an entity created in or
                            organized under the laws of the People’s Republic of China or the Special Administrative Region
                            of Hong Kong, or that has significant operations in the People’s Republic
                            of China or the Special Administrative Region of Hong Kong, owns or holds, directly or
                            indirectly, not less than 20 percent of the economic interest of the business concern or entity,
                            including as equity
                            shares or a capital or profit interest in a limited liability company or partnership; or (b)
                            that retains, as a member of the board of directors of the business concern, a person who is a
                            resident of the
                            People’s Republic of China.
                        </div>
                        <div class="custom-control custom-checkbox col-1 pl-0 pr-0">
                            <input @if(!empty($certifications->question11) && $certifications->question11 == 'Yes') checked @endif type="checkbox" id="att11" name="certifications[question11]" value="Yes" class="custom-control-input">
                            <label for="att11" class="custom-control-label custom-certificate-label">Yes</label>
                        </div>
                    </div>
                    <!-- 12th-Selected-row -->
                    <div class="row mb-3">
                        <div class="col-11">
                            The Applicant is not required to submit a registration statement under section 2 of the Foreign
                            Agents Registration Act of 1938 (22 U.S.C. 612).
                        </div>
                        <div class="custom-control custom-checkbox col-1 pl-0 pr-0">
                            <input @if(!empty($certifications->question12) && $certifications->question12 == 'Yes') checked @endif type="checkbox" id="att12" name="certifications[question12]" value="Yes" class="custom-control-input">
                            <label for="att12" class="custom-control-label custom-certificate-label">Yes</label>
                        </div>
                    </div>
                    <!-- 13th-Selected-row -->
                    <div class="row mb-3">
                        <div class="col-11">
                            The Applicant is not a business concern or entity primarily engaged in political or lobbying
                            activities, including any entity that is organized for research or for engaging in advocacy in
                            areas such as public policy or political strategy or otherwise
                            describes itself as a think tank in any public documents.
                        </div>
                        <div class="custom-control custom-checkbox col-1 pl-0 pr-0">
                            <input @if(!empty($certifications->question13) && $certifications->question13 == 'Yes') checked @endif type="checkbox" id="att13" name="certifications[question13]" value="Yes" class="custom-control-input">
                            <label for="att13" class="custom-control-label custom-certificate-label">Yes</label>
                        </div>
                    </div>
                    <!-- 14th-Selected-row -->
                    <div class="row mb-3">
                        <div class="col-11">
                            I further certify that the information provided in this application and the information provided
                            in all supporting documents and forms is true and accurate in all material respects. I
                            understand that knowingly making a false statement to obtain a guaranteed
                            loan from SBA is punishable under the law, including under 18 U.S.C. 1001 and 3571 by
                            imprisonment of not more than five years and/or a fine of up to $250,000; under 15 U.S.C. 645 by
                            imprisonment of not
                            more than two years and/or a fine of not more than $5,000; and, if submitted to a federally
                            insured institution, under 18 U.S.C. 1014 by imprisonment of not more than thirty years and/or a
                            fine of not more
                            than $1,000,000.
                        </div>
                        <div class="custom-control custom-checkbox col-1 pl-0 pr-0">
                            <input @if(!empty($certifications->question14) && $certifications->question14 == 'Yes') checked @endif type="checkbox" id="att14" name="certifications[question14]" value="Yes" class="custom-control-input">
                            <label for="att14" class="custom-control-label custom-certificate-label">Yes</label>
                        </div>
                    </div>
                    <!-- 15th-Selected-row -->
                    <div class="row mb-3">
                        <div class="col-11">
                            I acknowledge that the Lender will confirm the eligible loan amount using required documents
                            submitted. I understand, acknowledge, and agree that the Lender can share any tax information
                            that I have provided with SBA’s authorized representatives, including
                            authorized representatives of the SBA Office of Inspector General, for the purpose of compliance
                            with SBA Loan Program Requirements and all SBA reviews.
                        </div>
                        <div class="custom-control custom-checkbox col-1 pl-0 pr-0">
                            <input @if(!empty($certifications->question15s) && $certifications->question15 == 'Yes') checked @endif type="checkbox" id="att15" name="certifications[question15]" value="Yes" class="custom-control-input">
                            <label for="att15" class="custom-control-label custom-certificate-label">Yes</label>
                        </div>
                    </div>
                    <!-- Applicant Authorization -->
                    <div class="row">
                        <div class="col-12">
                            <h4>Applicant Authorization - Charisma Iwu</h4>
                        </div>
                        <!-- First and Middle Names* -->
                        <div class="col-6 mb-2">
                            <label class="mb-0">First and Middle Names*</label>
                            <input id="first_name" value="{{@$applicationAuthorization['first_name']}}" name="first_name" placeholder="First and Middle Names*" class="form-control">
                            <span style="font-size: 12px;">Please provide full legal name</span>
                             <div class="invalid-feedback required-field d-flex" id="error_first_name">
                                    
                                </div>
                        </div>
                        <!-- Last Name* -->
                        <div class="col-6 mb-2">
                            <label class="mb-0">Last Name*</label>
                            <input id="lastName" value="{{@$applicationAuthorization['last_name']}}" name="last_name" placeholder="Last Name*" class="form-control">
                        </div>
                        <!-- Birth Date -->
                        <div class="col-12 mb-0 pb-0">
                            <label class="mb-0">Birth Date</label>
                        </div>

                        @php
                        if(!empty($applicationAuthorization['date_of_birth'])){
                            $date_of_birth = explode("-", $applicationAuthorization['date_of_birth']);
                        }
                        $months = array( 'January','February','March','April','May','June','July ','August','September',
                        'October',
                        'November',
                        'December',
                        );
                        @endphp

                        <!-- Month -->
                        <div class="col-4 mb-2">
                            <select name="birth_month" class="form-control">
                                <option value="" disabled="disabled">Month</option>

                                @php
                                $i = 1;
                                @endphp
                                @foreach($months as $month)
                                @php
                                if($i <10) $i='0' .$i @endphp <option value="{{$i}}" @if(!empty($date_of_birth[1]) && $date_of_birth[1]==$i) selected @endif>{{$month}}</option>
                                    @php
                                    $i++;
                                    @endphp
                                    @endforeach
                            </select>
                             <div class="invalid-feedback required-field d-flex" id="error_birth_month"> </div>
                        </div>
                        <!-- Day -->
                        <div class="col-4 mb-2">
                            <input value="{{@$date_of_birth[2]}}" type="text" min="1" max="31" name="birth_day" placeholder="Day" class="form-control">
                       <div class="invalid-feedback required-field d-flex" id="error_birth_day"> </div>
                        </div>
                        <!-- Year -->
                        <div class="col-4 mb-2">
                            <input value="{{@$date_of_birth[0]}}" type="text" name="birth_year" placeholder="Year" class="form-control">
                        <div class="invalid-feedback required-field d-flex" id="error_birth_year"> </div>
                        </div>
                        <!-- Applicant Gender* -->
                        <div class="col-12 mb-3">
                            <label class="mb-0">Applicant Gender*</label>
                            <div class="custom-radio custom-control-inline mb-1 ml-3">
                                <input @if(!empty($applicationAuthorization['gender']) && $applicationAuthorization['gender']=='Male' ) checked @endif type="radio" id="ppp-941-cl" name="gender" class="custom-control-input" value="Male">
                                <label for="ppp-941-cl" class="custom-control-label ml-2">Male</label>
                            </div>
                            <div class="custom-radio custom-control-inline mb-1 ml-3">
                                <input @if(!empty($applicationAuthorization['gender']) && $applicationAuthorization['gender']=='Female' ) checked @endif type="radio" id="ppp-941-co" value="Female" name="gender" class="custom-control-input" value="quarter">
                                <label for="ppp-941-co" class="custom-control-label ml-2">Female</label>
                            </div>
                            <div class="custom-radio custom-control-inline mb-1 ml-3">
                                <input @if(!empty($applicationAuthorization['gender']) && $applicationAuthorization['gender']=='Not Disclosed' ) checked @endif type="radio" id="ppp-941-cq" value="Not Disclosed" name="gender" class="custom-control-input" value="quarter">
                                <label for="ppp-941-cq" class="custom-control-label ml-2">Not Disclosed </label>
                            </div>

                             <div class="invalid-feedback required-field d-flex" id="error_gender"> </div>
                        </div>
                        <!-- *Gender data  -->
                        <div class="col-12 pl-0">
                            <span style="font-size: 12px;">*Gender data is collected for SBA program reporting purposes only
                                and may be shared with the SBA or the lender.</span>
                        </div>
                        <!-- Applicant SSN* -->
                        <div class="col-12 mb-2">
                            <label class="mb-0">Applicant SSN*
                                <span class="tooltip_hover">
                                    <a>
                                        <svg class="svg-inline--fa fa-question-circle" viewBox="0 0 512 512">
                                            <path fill="currentColor" d="M256 8C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm0 448c-110.532 0-200-89.431-200-200 0-110.495 89.472-200 200-200 110.491 0 200 89.471 200 200 0 110.53-89.431 200-200 200zm107.244-255.2c0 67.052-72.421 68.084-72.421 92.863V300c0 6.627-5.373 12-12 12h-45.647c-6.627 0-12-5.373-12-12v-8.659c0-35.745 27.1-50.034 47.579-61.516 17.561-9.845 28.324-16.541 28.324-29.579 0-17.246-21.999-28.693-39.784-28.693-23.189 0-33.894 10.977-48.942 29.969-4.057 5.12-11.46 6.071-16.666 2.124l-27.824-21.098c-5.107-3.872-6.251-11.066-2.644-16.363C184.846 131.491 214.94 112 261.794 112c49.071 0 101.45 38.304 101.45 88.8zM298 368c0 23.159-18.841 42-42 42s-42-18.841-42-42 18.841-42 42-42 42 18.841 42 42z">
                                            </path>
                                        </svg>
                                    </a>
                                    <div id="tooltip" class="h-tooltip vue-tooltip tooltip-inner" x-placement="right">
                                        <div class="tooltip-content">Your Social Security Number and personal information
                                            are required by the government as part of the application process.
                                            <div class="tooltip-arrow"></div>
                                        </div>
                                    </div>
                                </span>
                            </label>
                            <input value="{{@$applicationAuthorization['applicant_ssn']}}" type="password" id="ssn" name="applicant_ssn" value="" placeholder="Social Security Number" class="form-control">
                       
                             <div class="invalid-feedback required-field d-flex" id="error_applicant_ssn"> </div>
                        </div>
                        <!-- Applicant Home Address* -->
                        <div class="col-12 mb-2">
                            <label class="mb-0">Applicant Home Address*</label>
                            <div class="form-group">
                                <input value="{{@$applicationAuthorization['full_address']}}" name="full_address" type="text" placeholder="Enter your address here" class="form-control pac-target-input" autocomplete="off">
                             <div class="invalid-feedback required-field d-flex" id="error_full_address"> </div>
                            </div>
                        </div>
                        <!-- Dasal -->
                        <div class="col-12 mb-2">
                            <div class="form-group">
                                <input value="{{@$applicationAuthorization['city']}}" id="baundefined" name="city" type="text" placeholder="City" class="form-control">
                           <div class="invalid-feedback required-field d-flex" id="error_city"> </div>
                            </div>
                        </div>
                        <!-- Select-State -->
                        <div class="col-6 mb-2">
                              @php
            $state_list = array('AL'=>"Alabama",
            'AK'=>"Alaska",
            'AZ'=>"Arizona",
            'AR'=>"Arkansas",
            'CA'=>"California",
            'CO'=>"Colorado",
            'CT'=>"Connecticut",
            'DE'=>"Delaware",
            'DC'=>"District Of Columbia",
            'FL'=>"Florida",
            'GA'=>"Georgia",
            'HI'=>"Hawaii",
            'ID'=>"Idaho",
            'IL'=>"Illinois",
            'IN'=>"Indiana",
            'IA'=>"Iowa",
            'KS'=>"Kansas",
            'KY'=>"Kentucky",
            'LA'=>"Louisiana",
            'ME'=>"Maine",
            'MD'=>"Maryland",
            'MA'=>"Massachusetts",
            'MI'=>"Michigan",
            'MN'=>"Minnesota",
            'MS'=>"Mississippi",
            'MO'=>"Missouri",
            'MT'=>"Montana",
            'NE'=>"Nebraska",
            'NV'=>"Nevada",
            'NH'=>"New Hampshire",
            'NJ'=>"New Jersey",
            'NM'=>"New Mexico",
            'NY'=>"New York",
            'NC'=>"North Carolina",
            'ND'=>"North Dakota",
            'OH'=>"Ohio",
            'OK'=>"Oklahoma",
            'OR'=>"Oregon",
            'PA'=>"Pennsylvania",
            'RI'=>"Rhode Island",
            'SC'=>"South Carolina",
            'SD'=>"South Dakota",
            'TN'=>"Tennessee",
            'TX'=>"Texas",
            'UT'=>"Utah",
            'VT'=>"Vermont",
            'VA'=>"Virginia",
            'WA'=>"Washington",
            'WV'=>"West Virginia",
            'WI'=>"Wisconsin",
            'WY'=>"Wyoming");
            @endphp
            <select name="state" data-cy="" placeholder="State" class="form-control">
               <option value="">Select a state</option>
               @foreach($state_list as $keys=> $state_names)
               <option value="{{$keys}}" @if(!empty($applicationAuthorization['state']) && $applicationAuthorization['state']==$keys) selected @endif>{{$state_names}}</option>
               @endforeach
            </select>
                        </div>
                        <!-- ZIP Code -->
                        <div class="col-6 mb-2">
                            <input value="{{@$applicationAuthorization['zipcode']}}" name="zipcode" value="" masked="true" type="text" placeholder="ZIP Code" class="form-control form-zip required">
                        <div class="invalid-feedback required-field d-flex" id="error_zipcode"> </div>
                        </div>
                        <!-- Applicant Title* -->
                        <div class="col-12 mb-2">
                            <label>Applicant Title*</label>
                            <input value="{{@$applicationAuthorization['applicant_title']}}" placeholder="Title" type="text" id="title" name="applicant_title" value="one" class="form-control">
                         <div class="invalid-feedback required-field d-flex" id="error_applicant_title"> </div>
                        </div>
                        <!-- Acknowledge -->
                      
                    </div>
                    <!-- Certify-That -->
                  

                </div>
                <!-- SUBMIT__BUTTON--NEXT -->
                <div class="footer">
                    <div class="footer-form">
                        <button type="button" name="previous" data-value="demographic_information" class="previousBtn action-button btn-primary" value="Previous" style="float: left;" />
                        <span>Previous</span>
                        </button>
                        <button type="submit" name="next" class="btn action-button btn-primary" value="Next" style="float: right;" />
                        <span>Next</span>
                        </button>
                    </div>
                    <div class="skip-btn d-none">
                        <a href="#">Fwd <i class="fas fa-arrow-right"></i></a>
                    </div>
                </div>

            </form>
        </fieldset>

    </div>
</div>

@endsection
@push('scripts')
<!-- Script -->
<script>
    $(document).ready(function() {

        $(".doc_selection").click(function(){ 
            $("#doc_selection").text($(this).val());
        }); 
         $(".doc-selection").click(function(){ 
            $("#doc-selection").text($(this).val());
        }); 
        $('.form-select').on('change', function (e) {
            console.log(valueSelected);
            var optionSelected = $("option:selected", this);
            var valueSelected = this.value; 
            $(".form_select").text(valueSelected);
        });
        

        $(".add_another_principal").click(function(e) {
            e.preventDefault();
            $('.multi-field:first-child').clone(true).appendTo('.multi-fields').find('input').val('').focus();
        });

        $(".stepsform").submit(function(event) {
            event.preventDefault(); //prevent default action 
            var post_url = $(this).attr("action"); //get form action url
            var request_method = $(this).attr("method"); //get form GET/POST method
            var form_data = new FormData(this); //Creates new FormData object
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $("#date_greater").hide();
            $.ajax({
                url: "{{route('ppp.document.store')}}",
                method: 'POST',
                data: form_data,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 800000,
                success: function(result) {
                    if (result.errors == true) {
                        $("#error_" + result.preStep).show();
                        $.each(result.errorsMsg, function(index, item) {
                            $.each(item, function(key, value) {
                                $("#" + index).removeClass('is-invalid');
                                $("#error_" + index).show();
                                $("#error_" + index).text(value);
                                $("#" + index).addClass('is-invalid');
                            });
                        });
                        return false;
                    } else {
                        $('.fldset-steps').hide();
                        $('#fieldset_' + result.nextStep).show();
                    }
                    if (typeof result.nextTabURL != 'undefined' && result.nextTabURL != '') {
                        window.location.replace(result.nextTabURL);
                    }
                }
            });

        });



        $(".previousBtn").click(function(event) {
            event.preventDefault();
            var fieldName = $(this).data('value');
            $.ajax({
                url: "{{ route('ppp.return.steps') }}",
                method: 'POST',
                data: 'step_name=' + fieldName,
                dataType: 'json',
                success: function(result) {
                    $('.fldset-steps').hide();
                    if (result.nextStep != "") {
                        $('#fieldset_' + result.nextStep).hide();
                    }
                    if (result.preStep != "") {
                        $('#fieldset_' + result.preStep).show();
                    }
                }
            });

        });
    });

    var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");

    function readFile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                var htmlPreview =
                    '<i class="fa fa-times remove-preview box-tools"></i>' +
                    '<img width="100" src="' + e.target.result + '" />' +
                    '<p>' + input.files[0].name + '</p>';

                var wrapperZone = $(input).parent();
                var previewZone = $(input).parent().parent().find('.preview-zone');
                var boxZone = $(input).parent().parent().find('.preview-zone').find('.box').find('.box-body');

                wrapperZone.removeClass('dragover');
                previewZone.removeClass('hidden');
                boxZone.empty();
                boxZone.append(htmlPreview);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    function reset(e) {
        e.val(null);
        //e.wrap('<form>').closest('form').get(0).reset();
        //e.unwrap();
    }

    $(".dropzone").change(function() {
        readFile(this);
    });

    $('#dropzone-wrapper').on('dragover', function(e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).addClass('dragover');
    });

    $('#dropzone-wrapper').on('dragleave', function(e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).removeClass('dragover');
    });

    $(document).on('click', '.remove-preview', function() {
        var boxZone = $(this).parents('.box-body');
        //var previewZone = $(this).parents('.preview-zone');
        var dropzone = $(this).parents('.upload-documets').find('.dropzone');
        boxZone.empty();
        //previewZone.addClass('hidden');
        reset(dropzone);
    });
    $(document).on('click', '.remove-document', function(e) {
        e.preventDefault();
        var id = $(this).data('id');
        var field = $(this).data('field');
        $.ajax({
            url: "{{url('/ppp/filedelete/') }}/" + id + '/' + field,
            method: 'GET',
            dataType: 'json',
            success: function(result) {
                if (result.msg == 'success') {
                    $('#' + field).removeClass('d-flex');
                    $('#' + field).addClass('d-none');
                }

            }
        });
    });
</script>
@endpush
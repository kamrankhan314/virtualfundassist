@extends('layouts.pppapp')
@push('styles')
@endpush
@section('content')
@php
if(!empty($stepsArray['businessinfo'])){
$business_info = $stepsArray['businessinfo'];
}
@endphp
<!-- ==== Tab-2-Content (Business) START FROM HERE === -->
<div class="tab-pane fade  show active" id="business" role="tabpanel" aria-labelledby="business-tab">
    <h2 class="page-header mb-0 mt-3 text-center">Paycheck Protection Program Application</h2>
    <div class="form-group" id="msform">
        <!-- (1st FIELDSET)  Business Information -->
        <fieldset class="fldset-steps" id="fieldset_basic_info">
            <form class="stepsform">
                <input type="hidden" name="tab_name" value="business_info">
                <input type="hidden" name="step_name" value="basic_info">
                <div class="ppp-business-information">
                    <h3 class="text-center">Business information</h3>
                    <!-- BUSINESS NAME-FIELD -->
                    <div class="form-group row business-name">
                        <div class="col col-12">
                            <label for="businessName" class="control-label">Business Name</label>
                            <input value="{{@Auth::user()->business_name}}" id="business_name" name="business_name" type="text" placeholder="Business Name" class="form-control">
                            <div class="form-control-feedback text-danger required-field text-left" id="error_business_name">
                                Please enter your business name
                            </div>
                        </div>
                    </div>
                    <!-- BUSINESS ADDRESS-FIELD -->
                    <div class="form-group row business-address">
                        <div class="col col-12">
                            <label for="businessName" class="control-label">Business Address</label>
                            <input value="{{@$business_info['basic_info']['business_address'] }}" name="business_address" data-cy="borrowerStreet" type="text" placeholder="Enter your address here" class="form-control pac-target-input" autocomplete="off">
                            <div class="form-control-feedback text-danger text-left" id="error_business_address" style="display: none">Field is required</div>

                        </div>
                    </div>
                    <!-- CITY-FIELD -->
                    <div class="form-group row business-city">
                        <div class="col col-12">
                            <label for="businessName" class="control-label">City</label>
                            <div class="form-group">
                                <input value="{{@$business_info['basic_info']['city'] }}" name="city" data-cy="borrowerCity" type="text" placeholder="City" class="form-control">
                                <div class="form-control-feedback text-left text-danger required-field" id="error_city">
                                Please enter a city</div>
                            </div>
                        </div>
                    </div>
                    <!-- CITY-SELECT OPTION & ZIP-CODE FIELD-->
                    <div class="form-group row business-city-selection">
                        <div class="col-12 col-md-6">
                            <div class="ls-select mb-0">
                                <svg class="svg-inline--fa fa-angle-down fa-w-10" data-fa-pseudo-element=":before" data-icon="angle-down" viewBox="0 0 320 512">
                                    <path fill="currentColor" d="M151.5 347.8L3.5 201c-4.7-4.7-4.7-12.3 0-17l19.8-19.8c4.7-4.7 12.3-4.7 17 0L160 282.7l119.7-118.5c4.7-4.7 12.3-4.7 17 0l19.8 19.8c4.7 4.7 4.7 12.3 0 17l-148 146.8c-4.7 4.7-12.3 4.7-17 0z">
                                    </path>
                                </svg>@php
                                $state_list = array('AL'=>"Alabama",
                                'AK'=>"Alaska",
                                'AZ'=>"Arizona",
                                'AR'=>"Arkansas",
                                'CA'=>"California",
                                'CO'=>"Colorado",
                                'CT'=>"Connecticut",
                                'DE'=>"Delaware",
                                'DC'=>"District Of Columbia",
                                'FL'=>"Florida",
                                'GA'=>"Georgia",
                                'HI'=>"Hawaii",
                                'ID'=>"Idaho",
                                'IL'=>"Illinois",
                                'IN'=>"Indiana",
                                'IA'=>"Iowa",
                                'KS'=>"Kansas",
                                'KY'=>"Kentucky",
                                'LA'=>"Louisiana",
                                'ME'=>"Maine",
                                'MD'=>"Maryland",
                                'MA'=>"Massachusetts",
                                'MI'=>"Michigan",
                                'MN'=>"Minnesota",
                                'MS'=>"Mississippi",
                                'MO'=>"Missouri",
                                'MT'=>"Montana",
                                'NE'=>"Nebraska",
                                'NV'=>"Nevada",
                                'NH'=>"New Hampshire",
                                'NJ'=>"New Jersey",
                                'NM'=>"New Mexico",
                                'NY'=>"New York",
                                'NC'=>"North Carolina",
                                'ND'=>"North Dakota",
                                'OH'=>"Ohio",
                                'OK'=>"Oklahoma",
                                'OR'=>"Oregon",
                                'PA'=>"Pennsylvania",
                                'RI'=>"Rhode Island",
                                'SC'=>"South Carolina",
                                'SD'=>"South Dakota",
                                'TN'=>"Tennessee",
                                'TX'=>"Texas",
                                'UT'=>"Utah",
                                'VT'=>"Vermont",
                                'VA'=>"Virginia",
                                'WA'=>"Washington",
                                'WV'=>"West Virginia",
                                'WI'=>"Wisconsin",
                                'WY'=>"Wyoming");
                                @endphp
                                <select id="" name="state_id" data-cy="borrowerStateId" class="form-control">
                                    <option disabled="disabled" selected="selected" value="">
                                        Select a state</option>
                                    @foreach($state_list as $keys=> $state_names)
                                    <option value="{{$keys}}" @if(!empty($business_info['basic_info']['state_id']) && $business_info['basic_info']['state_id']==$keys) selected @endif>
                                        {{$state_names}}
                                    </option>
                                    @endforeach
                                </select>
                                <div class="form-control-feedback text-danger required-field text-left" id="error_state_id">
                                    Field is required</div>

                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <input value="{{@$business_info['basic_info']['zip_code'] }}" name="zip_code" data-cy="borrowerZipId" masked="true" type="text" placeholder="ZIP Code" class="form-control form-zip required">

                            <div class="form-control-feedback text-danger required-field text-left" id="error_zip_code">
                                Field is required</div>

                        </div>
                    </div>
                    <!-- DBA NAME-FIELD -->
                    <div class="form-group row business-dba">
                        <div class="col col-12">
                            <div class="ppp-business borrower-attribute-name">
                                <label for="ba236" class="d-flex">DBA Name
                                    <span>
                                        <a>
                                            <svg class="svg-inline--fa fa-question-circle fa-w-16" viewBox="0 0 512 512">
                                                <path fill="currentColor" d="M256 8C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm0 448c-110.532 0-200-89.431-200-200 0-110.495 89.472-200 200-200 110.491 0 200 89.471 200 200 0 110.53-89.431 200-200 200zm107.244-255.2c0 67.052-72.421 68.084-72.421 92.863V300c0 6.627-5.373 12-12 12h-45.647c-6.627 0-12-5.373-12-12v-8.659c0-35.745 27.1-50.034 47.579-61.516 17.561-9.845 28.324-16.541 28.324-29.579 0-17.246-21.999-28.693-39.784-28.693-23.189 0-33.894 10.977-48.942 29.969-4.057 5.12-11.46 6.071-16.666 2.124l-27.824-21.098c-5.107-3.872-6.251-11.066-2.644-16.363C184.846 131.491 214.94 112 261.794 112c49.071 0 101.45 38.304 101.45 88.8zM298 368c0 23.159-18.841 42-42 42s-42-18.841-42-42 18.841-42 42-42 42 18.841 42 42z">
                                                </path>
                                            </svg>
                                        </a>
                                    </span>
                                </label>
                            </div>
                            <div class="input-group">
                                <input value="{{@$business_info['basic_info']['dba_name'] }}" name="dba_name" type="text" class="form-control">
                            </div>
                            <div class="form-control-feedback text-danger required-field text-left" id="error_dba_name">
                                    Field is required</div>
                        </div>
                    </div>

                    <!-- SELECT MONTH/DAY/YEAR FIELD -->
                    <div class="ppp-business">
                        <label class="d-flex">Business Start Date
                            <span>
                                <a>
                                    <svg class="svg-inline--fa fa-question-circle fa-w-16" viewBox="0 0 512 512">
                                        <path fill="currentColor" d="M256 8C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm0 448c-110.532 0-200-89.431-200-200 0-110.495 89.472-200 200-200 110.491 0 200 89.471 200 200 0 110.53-89.431 200-200 200zm107.244-255.2c0 67.052-72.421 68.084-72.421 92.863V300c0 6.627-5.373 12-12 12h-45.647c-6.627 0-12-5.373-12-12v-8.659c0-35.745 27.1-50.034 47.579-61.516 17.561-9.845 28.324-16.541 28.324-29.579 0-17.246-21.999-28.693-39.784-28.693-23.189 0-33.894 10.977-48.942 29.969-4.057 5.12-11.46 6.071-16.666 2.124l-27.824-21.098c-5.107-3.872-6.251-11.066-2.644-16.363C184.846 131.491 214.94 112 261.794 112c49.071 0 101.45 38.304 101.45 88.8zM298 368c0 23.159-18.841 42-42 42s-42-18.841-42-42 18.841-42 42-42 42 18.841 42 42z">
                                        </path>
                                    </svg>
                                </a>
                            </span>
                        </label>
                        <div class="form-group row business-date-selection">
                            <div class="col-md-4 col-12">
                                <div class="ls-select">
                                    <svg class="svg-inline--fa fa-angle-down fa-w-10" data-fa-pseudo-element=":before" data-icon="angle-down" viewBox="0 0 320 512">
                                        <path fill="currentColor" d="M151.5 347.8L3.5 201c-4.7-4.7-4.7-12.3 0-17l19.8-19.8c4.7-4.7 12.3-4.7 17 0L160 282.7l119.7-118.5c4.7-4.7 12.3-4.7 17 0l19.8 19.8c4.7 4.7 4.7 12.3 0 17l-148 146.8c-4.7 4.7-12.3 4.7-17 0z">
                                        </path>
                                    </svg>
                                    @php
                                    $months = array( 'January','February','March','April','May','June','July
                                    ','August','September',
                                    'October',
                                    'November',
                                    'December',
                                    );
                                    @endphp
                                    <select id="month" name="business_start_date_month" data-cy="" class="form-control no-select">
                                        <option value="" selected="selected" disabled="disabled">Month</option>
                                        @php
                                        $i = 1;
                                        @endphp
                                        @foreach( $months as $month)
                                        @php
                                        if($i <10) $i='0' .$i @endphp <option value="{{$i}}" @if(isset($business_info['basic_info']['business_start_date_month']) && $business_info['basic_info']['business_start_date_month']==$i) selected @endif>{{$month}}</option>
                                            @php
                                            $i++;
                                            @endphp
                                            @endforeach
                                    </select>
                                    <div class="form-control-feedback text-danger required-field text-left" id="error_business_start_date_month">
                                        Field is required</div>
                                </div>

                            </div>
                            <div data-v-6cddc630="" class="col-md-4 col-12">
                                <input type="number" min="1" max="31" id="day" name="business_start_date_day" placeholder="01" class="form-control" value="{{@$business_info['basic_info']['business_start_date_day']}}">
                                <div class="form-control-feedback text-danger required-field text-left" id="error_business_start_date_day">
                                    Field is required</div>
                            </div>
                            <div data-v-6cddc630="" class="col-md-4 col-12">
                                <input type="number" id="year" name="business_start_date_year" data-cy="" placeholder="2021" class="form-control" value="{{@$business_info['basic_info']['business_start_date_year']}}">

                                <div class="form-control-feedback text-danger required-field text-left" id="error_business_start_date_year">
                                    Field is required</div>
                            </div>
                        </div>
                    </div>
                    <!-- BUSINESS TYPE-FIELD -->
                    <div class="form-group row business-type">
                        <div class="col col-md-12">
                            <label for="businessName" class="control-label">Business Type</label>
                            <select id="business_type" name="business_type" class="form-select form-control" aria-label="Default select example" aria-placeholder="Enter your address here">
                                <option selected>Select Business Type</option>
                                <option @if(!empty($business_info['basic_info']['business_type']) && $business_info['basic_info']['business_type']=='Sole proprietor' ) selected @endif value="Sole proprietor">
                                    Sole proprietor
                                </option>
                                <option @if(!empty($business_info['basic_info']['business_type']) && $business_info['basic_info']['business_type']=='Partnership' ) selected @endif value="Partnership">
                                    Partnership
                                </option>
                                <option @if(!empty($business_info['basic_info']['business_type']) && $business_info['basic_info']['business_type']=='C-Corp' ) selected @endif value="C-Corp">
                                    C-Corp
                                </option>
                                <option @if(!empty($business_info['basic_info']['business_type']) && $business_info['basic_info']['business_type']=='S-Corp' ) selected @endif value="S-Corp">
                                    S-Corp
                                </option>
                                <option @if(!empty($business_info['basic_info']['business_type']) && $business_info['basic_info']['business_type']=='LLC' ) selected @endif value="LLC">
                                    LLC
                                </option>
                                <option @if(!empty($business_info['basic_info']['business_type']) && $business_info['basic_info']['business_type']=='Independent Contractor' ) selected @endif value="Independent Contractor">
                                    Independent Contractor
                                </option>
                                <option @if(!empty($business_info['basic_info']['business_type']) && $business_info['basic_info']['business_type']=='Self-Employed Individual' ) selected @endif value="Self-Employed Individual">
                                    Self-Employed Individual
                                </option>
                                <option @if(!empty($business_info['basic_info']['business_type']) && $business_info['basic_info']['business_type']=='501(c)(3) Non Profit' ) selected @endif value="501(c)(3) Non Profit">
                                    501(c)(3) Non Profit
                                </option>
                                <option @if(!empty($business_info['basic_info']['business_type']) && $business_info['basic_info']['business_type']=='501(c)(6) Non Profit Membership' ) selected @endif value="501(c)(6) Non Profit Membership">
                                    501(c)(6) Non Profit Membership
                                </option>
                                <option @if(!empty($business_info['basic_info']['business_type']) && $business_info['basic_info']['business_type']=='501(c)(19) veterans organization' ) selected @endif value="501(c)(19) veterans organization">
                                    501(c)(19) veterans organization
                                </option>
                                <option @if(!empty($business_info['basic_info']['business_type']) && $business_info['basic_info']['business_type']=='Tribal business (sec. 31(b)(2)(C) of Small Business Act)' ) selected @endif value="Tribal business (sec. 31(b)(2)(C) of Small Business Act)">
                                    Tribal business (sec. 31(b)(2)(C) of Small Business Act)
                                </option>
                                <option value="Housing Cooperative" @if(!empty($business_info['basic_info']['business_type']) && $business_info['basic_info']['business_type']=='Housing Cooperative' ) selected @endif>
                                    Housing Cooperative
                                </option>
                                <option value="Joint Venture" @if(!empty($business_info['basic_info']['business_type']) && $business_info['basic_info']['business_type']=='Joint Venture' ) selected @endif>
                                    Joint Venture
                                </option>
                                <option value=" Professional Association" @if(!empty($business_info['basic_info']['business_type']) && $business_info['basic_info']['business_type']=='Professional Association' ) selected @endif>
                                    Professional Association
                                </option>
                                <option value="Trust" @if(!empty($business_info['basic_info']['business_type']) && $business_info['basic_info']['business_type']=='Trust' ) selected @endif>
                                    Trust
                                </option>
                                <option value="Cooperative" @if(!empty($business_info['basic_info']['business_type']) && $business_info['basic_info']['business_type']=='Cooperative' ) selected @endif>
                                    Cooperative
                                </option>
                                <option value="Tenant in Common" @if(!empty($business_info['basic_info']['business_type']) && $business_info['basic_info']['business_type']=='Tenant in Common' ) selected @endif>
                                    Tenant in Common
                                </option>
                                <option value="Employee Stock Ownership Plan (ESOP)" @if(!empty($business_info['basic_info']['business_type']) && $business_info['basic_info']['business_type']=='Employee Stock Ownership Plan (ESOP)' ) selected @endif>>
                                    Employee Stock Ownership Plan (ESOP)
                                </option>
                                <option value="Non-Profit Organization" @if(!empty($business_info['basic_info']['business_type']) && $business_info['basic_info']['business_type']=='Non-Profit Organization' ) selected @endif>
                                    Non-Profit Organization
                                </option>
                                <option value="Non-Profit Child Care Center" @if(!empty($business_info['basic_info']['business_type']) && $business_info['basic_info']['business_type']=='Non-Profit Child Care Center' ) selected @endif>
                                    Non-Profit Child Care Center
                                </option>
                                <option value="Roll-over as Business Startups (ROBS)" @if(!empty($business_info['basic_info']['business_type']) && $business_info['basic_info']['business_type']=='Roll-over as Business Startups (ROBS)' ) selected @endif>
                                    Roll-over as Business Startups (ROBS)
                                </option>
                            </select>
                            <div class="form-control-feedback text-danger required-field text-left" id="error_business_type">
                                Please select a business type</div>
                        </div>
                    </div>
                    <!-- BUSINESS__TIN(EIN,SIN)--FIELD -->
                    <div class="business-tin" style="display:none">
                        <label for="businesstin" class="control-label">Business TIN (EIN, SSN)</label>
                        <div class="form-group row">
                            <div class="col col-md-4">
                                <select name="business_tin" id="business_tin" class="form-select form-control" aria-label="Default select example" aria-placeholder="Select">
                                    <option value="" class="text-muted">Select</option>
                                    <option @if(isset($business_info['basic_info']['business_tin']) && $business_info['basic_info']['business_tin']=="EIN" ) selected @endif value="EIN">
                                        EIN
                                    </option>
                                    <option @if(isset($business_info['basic_info']['business_tin']) && $business_info['basic_info']['business_tin']=="SSN" ) selected @endif value="SSN">
                                        SSN
                                    </option>
                                </select>
                            </div>
                            <div class="col col-md-8 pl-0">
                                <div class="input-group">
                                    <input id="tax_id" name="tax_id" type="text" class="form-control" value="{{@$business_info['basic_info']['tax_id']}}">
                                </div>
                                <div class="form-control-feedback text-danger required-field text-left" id="error_tax_id">
                                    Please enter a 9 digit Employer ID number
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- FEDRAL/STATE TAX-ID--FIELD -->
                    <div class="form-group row business-tax" style="display: none">
                        <div class="col col-md-12">
                            <div class="form-group">
                                <div class="ppp-first-loan borrower-attribute-name">
                                    <label for="ba236" class="d-flex">Federal/State Tax ID
                                        <span>
                                            <a>
                                                <svg class="svg-inline--fa fa-question-circle fa-w-16" viewBox="0 0 512 512">
                                                    <path fill="currentColor" d="M256 8C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm0 448c-110.532 0-200-89.431-200-200 0-110.495 89.472-200 200-200 110.491 0 200 89.471 200 200 0 110.53-89.431 200-200 200zm107.244-255.2c0 67.052-72.421 68.084-72.421 92.863V300c0 6.627-5.373 12-12 12h-45.647c-6.627 0-12-5.373-12-12v-8.659c0-35.745 27.1-50.034 47.579-61.516 17.561-9.845 28.324-16.541 28.324-29.579 0-17.246-21.999-28.693-39.784-28.693-23.189 0-33.894 10.977-48.942 29.969-4.057 5.12-11.46 6.071-16.666 2.124l-27.824-21.098c-5.107-3.872-6.251-11.066-2.644-16.363C184.846 131.491 214.94 112 261.794 112c49.071 0 101.45 38.304 101.45 88.8zM298 368c0 23.159-18.841 42-42 42s-42-18.841-42-42 18.841-42 42-42 42 18.841 42 42z">
                                                    </path>
                                                </svg>
                                            </a>
                                        </span>
                                    </label>
                                </div>
                                <div class="type-container">
                                    <input id="tax_id" name="tax_id" value="{{@$business_info['basic_info']['tax_id']}}" type="text" placeholder="" class="form-control">
                                </div>
                            </div>
                            <div class="form-control-feedback text-danger required-field text-left" id="error_tax_id">
                                Federal/State Tax ID must be 9 digits in length
                            </div>
                        </div>
                    </div>

                    <!-- INDUSTRY--FIELD -->
                    <div class="form-group row business-industry">
                        <div class="col col-md-12">
                            <div class="form-group">
                                <label for="ba236" class="d-flex">Industry
                                    <span>
                                        <a>
                                            <svg class="svg-inline--fa fa-question-circle fa-w-16" viewBox="0 0 512 512">
                                                <path fill="currentColor" d="M256 8C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm0 448c-110.532 0-200-89.431-200-200 0-110.495 89.472-200 200-200 110.491 0 200 89.471 200 200 0 110.53-89.431 200-200 200zm107.244-255.2c0 67.052-72.421 68.084-72.421 92.863V300c0 6.627-5.373 12-12 12h-45.647c-6.627 0-12-5.373-12-12v-8.659c0-35.745 27.1-50.034 47.579-61.516 17.561-9.845 28.324-16.541 28.324-29.579 0-17.246-21.999-28.693-39.784-28.693-23.189 0-33.894 10.977-48.942 29.969-4.057 5.12-11.46 6.071-16.666 2.124l-27.824-21.098c-5.107-3.872-6.251-11.066-2.644-16.363C184.846 131.491 214.94 112 261.794 112c49.071 0 101.45 38.304 101.45 88.8zM298 368c0 23.159-18.841 42-42 42s-42-18.841-42-42 18.841-42 42-42 42 18.841 42 42z">
                                                </path>
                                            </svg>
                                        </a>
                                    </span>
                                </label>
                                <div class="type-container">
                                    <input value="{{@$business_info['basic_info']['business_industry']}}" id="business_industry" name="business_industry" type="text" placeholder="Start typing to select industry" class="form-control">
                                </div>
                            </div>
                            <div class="form-control-feedback text-danger required-field text-left" id="error_business_industry">
                                This field requires you to select an industry from the drop-down
                            </div>
                        </div>
                    </div>
                    <!-- NUMBER OF EMPLOYEE--FIELD -->
                    <div class="form-group row business-number">
                        <div class="col col-md-12">
                            <label for="businessName" class="control-label">Number of Employees (Including
                                Owners)</label>
                            <div class="input-group">
                                <input value="{{@$business_info['basic_info']['total_employees'] }}" name="total_employees" type="number" data-cy="number_of_employees" class="form-control">
                            </div>
                            <div class="form-control-feedback text-danger required-field text-left" id="error_total_employees">
                                Please enter a number between 1 and 500
                            </div>

                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-9 col-12 text-left">
                            <div class="average_payroll">Has the Applicant received an SBA Economic Injury Disaster Loan
                                between January 31, 2020 and April 3, 2020?</div>
                        </div>
                        <div class="col col-md-3 col-12 text-left p-0">
                            <div class="custom-control custom-radio custom-control-inline p-0">
                                <label class="form-row">Yes
                                    <input @if(!empty($business_info['basic_info']['sbayear']) && $business_info['basic_info']['sbayear']=='Yes' ) checked @endif class="d-block ml-2" type="radio" name="sbayear" value="Yes">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline p-1">
                                <label class="form-row">No
                                    <input class="d-block ml-2" type="radio" @if(!empty($business_info['basic_info']['sbayear']) && $business_info['basic_info']['sbayear']=='No' ) checked @endif name="sbayear" value="No">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row business-number sbadetails" style="display: none">
                        <div class="col col-md-12">
                            <label for="businessName" class="control-label">Please provide the amount and term
                                details</label>
                            <div class="input-group">
                                <textarea class="form-control" name="sbadetails">{{@$business_info['basic_info']['sbadetails']}}</textarea>
                            </div>
                            <div class="form-control-feedback text-danger required-field text-left" id="error_sbayear">
                                Please enter value
                            </div>

                        </div>
                    </div>

                    <!-- MONTHLY__PAYROLL--FIELD -->
                    <div class="business-payroll">
                        <div class="form-group row">
                            <div class="col col-md-12">
                                <div class="form-group mb-0">
                                    <div class="ppp-business-payroll borrower-attribute-name">
                                        <label for="ba236" class="d-flex mb-0">Average Monthly Payroll
                                            <span>
                                                <a>
                                                    <svg class="svg-inline--fa fa-question-circle fa-w-16" viewBox="0 0 512 512">
                                                        <path fill="currentColor" d="M256 8C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm0 448c-110.532 0-200-89.431-200-200 0-110.495 89.472-200 200-200 110.491 0 200 89.471 200 200 0 110.53-89.431 200-200 200zm107.244-255.2c0 67.052-72.421 68.084-72.421 92.863V300c0 6.627-5.373 12-12 12h-45.647c-6.627 0-12-5.373-12-12v-8.659c0-35.745 27.1-50.034 47.579-61.516 17.561-9.845 28.324-16.541 28.324-29.579 0-17.246-21.999-28.693-39.784-28.693-23.189 0-33.894 10.977-48.942 29.969-4.057 5.12-11.46 6.071-16.666 2.124l-27.824-21.098c-5.107-3.872-6.251-11.066-2.644-16.363C184.846 131.491 214.94 112 261.794 112c49.071 0 101.45 38.304 101.45 88.8zM298 368c0 23.159-18.841 42-42 42s-42-18.841-42-42 18.841-42 42-42 42 18.841 42 42z">
                                                        </path>
                                                    </svg>
                                                </a>
                                            </span>
                                        </label>
                                    </div>
                                </div>
                                <div class="ppp-loan-amount-calc mb-2 text-left average_payroll"><a data-v-25c214cc="" href="https://www.virtualfundassist.com/business-calculators/sba-loan-calculator/" target="_blank">Payroll Calculator</a>
                                    <div class="average_payroll">This affects your max loan amount and can be based on
                                        2019
                                        or 2020. Associated year tax documents will be required to prove payroll.
                                    </div>
                                    <div class="input-group flex-nowrap">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">$</span>
                                        </div>
                                        <input name="average_monthly_payroll" id="average_monthly_payroll" type="text" class="form-control comma_separate" value="{{@$business_info['basic_info']['average_monthly_payroll']}}">
                                    </div>
                                    <div class="form-control-feedback text-danger required-field text-left" id="error_average_monthly_payroll">
                                        Please enter a valid dollar amount
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- MONTHLY__PAYROLL RADIO-BTN SELECTOR -->
                        <div class="form-group row">
                            <div class="col-md-9 col-12 text-left">
                                <div class="average_payroll">You entered $<b id="average_payroll">{{@$business_info['basic_info']['average_monthly_payroll']}}</b>
                                    for your average
                                    monthly payroll.
                                    What year did you use to calculate your payroll?</div>
                            </div>
                            <div class="col col-md-3 col-12 text-left p-0">
                                <div class="custom-control custom-radio custom-control-inline p-0">
                                    <label class="form-row">2019
                                        <input class="d-block ml-2" type="radio" value="2019" name="average_payroll_year" @if(!empty($business_info['basic_info']['average_payroll_year']) && $business_info['basic_info']['average_payroll_year']=='2019' ) checked="checked" @endif>
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline p-1">
                                    <label class="form-row">2020
                                        <input class="d-block ml-2" type="radio" value="2020" name="average_payroll_year" @if(!empty($business_info['basic_info']['average_payroll_year']) && $business_info['basic_info']['average_payroll_year']=='2020' ) checked="checked" @endif>
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- LOAN__AMOUNT--FIELD -->
                    <div class="form-group row business-amount">
                        <div class="col col-12">
                            <div class="ppp-business borrower-attribute-name">
                                <label class="d-flex mb-0">Loan Amount
                                    <span>
                                        <a>
                                            <svg class="svg-inline--fa fa-question-circle fa-w-16" viewBox="0 0 512 512">
                                                <path fill="currentColor" d="M256 8C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm0 448c-110.532 0-200-89.431-200-200 0-110.495 89.472-200 200-200 110.491 0 200 89.471 200 200 0 110.53-89.431 200-200 200zm107.244-255.2c0 67.052-72.421 68.084-72.421 92.863V300c0 6.627-5.373 12-12 12h-45.647c-6.627 0-12-5.373-12-12v-8.659c0-35.745 27.1-50.034 47.579-61.516 17.561-9.845 28.324-16.541 28.324-29.579 0-17.246-21.999-28.693-39.784-28.693-23.189 0-33.894 10.977-48.942 29.969-4.057 5.12-11.46 6.071-16.666 2.124l-27.824-21.098c-5.107-3.872-6.251-11.066-2.644-16.363C184.846 131.491 214.94 112 261.794 112c49.071 0 101.45 38.304 101.45 88.8zM298 368c0 23.159-18.841 42-42 42s-42-18.841-42-42 18.841-42 42-42 42 18.841 42 42z">
                                                </path>
                                            </svg>
                                        </a>
                                    </span>
                                </label>
                            </div>
                            <div class="average_payroll text-left">
                                Maximum loan amount you could qualify for: $ <span class="loan_amount">0,0</span> 
                            <input name="loan_amount" id="loan_amount" type="hidden" value="">
                            </div>
                        </div>
                        <div class="col col-12">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">$</span>
                                </div>
                                <input name="financial_amount" id="financial_amount" type="text" class="form-control comma_separate" value="{{@$business_info['basic_info']['financial_amount']}}">
                            </div>
                            <div class="form-control-feedback text-danger required-field text-left" id="error_financial_amount">
                                Please enter a valid dollar amount
                            </div>
                        </div>
                    </div>
                    <!-- LOAN__PURPOSE--FIELD -->
                    <div class="business-loan-purpose">
                        <label class="control-label">Loan Purpose</label>
                        <div class="form-group row">
                            <div class="col col-md-12">
                                <div>
                                    <ul class="list-group list-group-flush ml-3">
                                        <li class="list-group-item p-0">
                                            <!-- Default checked -->
                                            <div class="custom-control custom-checkbox">
                                                <input value="Payroll Costs" @if(!empty($business_info['basic_info']['loan_purpose']) && in_array('Payroll Costs',$business_info['basic_info']['loan_purpose']) ) checked @endif type="checkbox" class="custom-control-input" id="payrollCost" name="loan_purpose[]">
                                                <label class="custom-control-label" for="payrollCost">Payroll
                                                    Costs</label>
                                            </div>
                                        </li>
                                        <li class="list-group-item p-0">
                                            <!-- Default checked -->
                                            <div class="custom-control custom-checkbox">
                                                <input value="Rent/Mortgage Interest" @if(!empty($business_info['basic_info']['loan_purpose']) && in_array('Rent/Mortgage Interest',$business_info['basic_info']['loan_purpose']) ) checked @endif type="checkbox" class="custom-control-input" id="rentMortage" name="loan_purpose[]">
                                                <label class="custom-control-label" for="rentMortage">Rent/Mortgage
                                                    Interest</label>
                                            </div>
                                        </li>
                                        <li class="list-group-item p-0">
                                            <!-- Default checked -->
                                            <div class="custom-control custom-checkbox">
                                                <input value="Utilities" @if(!empty($business_info['basic_info']['loan_purpose']) && in_array('Utilities',$business_info['basic_info']['loan_purpose']) ) checked @endif type="checkbox" class="custom-control-input" id="utilities" name="loan_purpose[]">
                                                <label class="custom-control-label" for="utilities">Utilities</label>
                                            </div>
                                        </li>
                                        <li class="list-group-item p-0">
                                            <!-- Default checked -->
                                            <div class="custom-control custom-checkbox">
                                                <input value="Covered Operations Expenditures" @if(!empty($business_info['basic_info']['loan_purpose']) && in_array('Covered Operations Expenditures',$business_info['basic_info']['loan_purpose']) ) checked @endif type="checkbox" class="custom-control-input" id="coveredOperation" name="loan_purpose[]">
                                                <label class="custom-control-label" for="coveredOperation">Covered
                                                    Operations Expenditures</label>
                                            </div>
                                        </li>
                                        <li class="list-group-item p-0">
                                            <!-- Default checked -->
                                            <div class="custom-control custom-checkbox">
                                                <input value="Covered Property Damage" @if(!empty($business_info['basic_info']['loan_purpose']) && in_array('Covered Property Damage',$business_info['basic_info']['loan_purpose']) ) checked @endif type="checkbox" class="custom-control-input" id="coveredProperty" name="loan_purpose[]">
                                                <label class="custom-control-label" for="coveredProperty">Covered
                                                    Property Damage</label>
                                            </div>
                                        </li>
                                        <li class="list-group-item p-0">
                                            <!-- Default checked -->
                                            <div class="custom-control custom-checkbox">
                                                <input value="Covered Supplier Costs" @if(!empty($business_info['basic_info']['loan_purpose']) && in_array('Covered Supplier Costs',$business_info['basic_info']['loan_purpose']) ) checked @endif type="checkbox" class="custom-control-input" id="coveredSupplier" name="loan_purpose[]">
                                                <label class="custom-control-label" for="coveredSupplier">Covered
                                                    Supplier Costs</label>
                                            </div>
                                        </li>
                                        <li class="list-group-item p-0">
                                            <!-- Default checked -->
                                            <div class="custom-control custom-checkbox">
                                                <input value="Covered Worker Protection Expenditures" @if(!empty($business_info['basic_info']['loan_purpose']) && in_array('Covered Worker Protection Expenditures',$business_info['basic_info']['loan_purpose']) ) checked @endif type="checkbox" class="custom-control-input" id="coveredWorker" name="loan_purpose[]">
                                                <label class="custom-control-label" for="coveredWorker">Covered Worker
                                                    Protection Expenditures</label>
                                            </div>
                                        </li>
                                        <li class="list-group-item p-0">
                                            <!-- Default checked -->
                                            <div class="custom-control custom-checkbox">
                                                <input value="Other" @if(!empty($business_info['basic_info']['loan_purpose']) && in_array('Other',$business_info['basic_info']['loan_purpose']) ) checked @endif type="checkbox" class="custom-control-input" id="other" name="loan_purpose[]">
                                                <label class="custom-control-label" for="other">Other</label>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="form-control-feedback text-danger required-field text-left" id="error_basic_info"> Please Enter all required fields</div>
                                <div data-cy="payroll-feedback" class="invalid-feedback d-flex payroll-feedback">
                                    Deselecting 'Payroll' may disqualify yourself from loan funds and loan forgiveness.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- SUBMIT__BUTTON--NEXT -->
                <div class="footer">
                    <div class="footer-form">
                        <button type="button" name="previous" data-value="business_state" class="previousBtn action-button btn-primary" value="Previous" style="float: left;" />
                        <span>Previous</span>
                        </button>
                        <button type="submit" name="next" class="btn action-button btn-primary" value="Next" style="float: right;" />
                        <span>Next</span>
                        </button>
                    </div>
                    <div class="skip-btn d-none">
                        <a href="#">Fwd <i class="fas fa-arrow-right"></i></a>
                    </div>
                </div>
            </form>
        </fieldset>

        <!-- (2nd FIELDSET)  Revenue Reduction -->
        <fieldset class="fldset-steps" id="fieldset_revenue_reduction">
            <form class="stepsform">
                <input type="hidden" name="tab_name" value="basic_info">
                <input type="hidden" name="step_name" value="revenue_reduction">
                <div class="ppp-revenue-reduction">
                    <h3 class="text-center">Revenue Reduction</h3>
                    <div>
                        <p style="font-size: 17px;">The SBA requires at least a 25% revenue reduction for Second Draw
                            eligibility.</p>
                        <p style="font-size: 16px;">Enter your Gross Receipts and Expenses for 2019 and 2020 below.</p>
                    </div>
                    <!-- 2019 GROSS__RECEIPT--FIELD 1ST-SECTION -->
                    <div class="form-group row business-gross-receipt">
                        <div class="ppp-business pb-2 pl-3 col col-12">
                            <!-- Label Gross--Receipt -->
                            <label for="ba236" class="mb-0">2019 <b>gross receipts</b> for each quarter
                                <span>
                                    <a>
                                        <svg class="svg-inline--fa fa-question-circle fa-w-16" viewBox="0 0 512 512">
                                            <path fill="currentColor" d="M256 8C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm0 448c-110.532 0-200-89.431-200-200 0-110.495 89.472-200 200-200 110.491 0 200 89.471 200 200 0 110.53-89.431 200-200 200zm107.244-255.2c0 67.052-72.421 68.084-72.421 92.863V300c0 6.627-5.373 12-12 12h-45.647c-6.627 0-12-5.373-12-12v-8.659c0-35.745 27.1-50.034 47.579-61.516 17.561-9.845 28.324-16.541 28.324-29.579 0-17.246-21.999-28.693-39.784-28.693-23.189 0-33.894 10.977-48.942 29.969-4.057 5.12-11.46 6.071-16.666 2.124l-27.824-21.098c-5.107-3.872-6.251-11.066-2.644-16.363C184.846 131.491 214.94 112 261.794 112c49.071 0 101.45 38.304 101.45 88.8zM298 368c0 23.159-18.841 42-42 42s-42-18.841-42-42 18.841-42 42-42 42 18.841 42 42z">
                                            </path>
                                        </svg>
                                        <div id="tooltip" class="h-tooltip vue-tooltip tooltip-inner" x-placement="right">
                                            <div class="tooltip-arrow"></div>
                                            <div class="tooltip-content">Generally, gross receipts are considered "total
                                                income" (or in the case of a sole proprietorship,
                                                independent contractor, or self-employed individual "gross income") plus
                                                "cost of goods sold," and excludes net capital
                                                gains or losses as these terms are defined and reported on IRS tax
                                                return
                                                forms.
                                            </div>
                                        </div>
                                    </a>
                                </span>
                            </label>
                            <span class="accordance">Enter '0' for quarters if your business did not have gross
                                receipts</span>

                        </div>
                    </div>
                    @php
                    $years = array('2019','2020');
                    @endphp
                    @foreach($years as $year)
                    <!-- {{$year}} GROSS__RECEIPT--FIELD 1ST-SECTION -->
                    <div class="form-group row business-gross-receipt">
                        <div class="ppp-business pb-2 pl-3 col col-12">
                            <!-- Label Gross--Receipt -->
                            <label for="ba236" class="mb-0">{{$year}}<b>gross receipts</b> for each quarter</label>
                        </div>
                        <!-- Q1---Field -->
                        <div class="col col-md-6 mb-3">
                            <div class="input-group">
                                <span class="pt-1 pr-2">Q1</span>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">$</span>
                                </div>
                                <input type="text" class="form-control comma_separate" name="g_first_quarter[{{$year}}]">
                            </div>
                        </div>
                        <!-- Q2---Field -->
                        <div class="col col-md-6 mb-3">
                            <div class="input-group">
                                <span class="pt-1 pr-2">Q2</span>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">$</span>
                                </div>
                                <input type="text" class="form-control comma_separate" name="g_second_quarter[{{$year}}]">
                            </div>

                        </div>
                        <!-- Q3---Field -->
                        <div class="col col-md-6 mb-3">
                            <div class="input-group">
                                <span class="pt-1 pr-2">Q3</span>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">$</span>
                                </div>
                                <input type="text" class="form-control comma_separate" name="g_third_quarter[{{$year}}]">
                            </div>

                        </div>
                        <!-- Q4---Field -->
                        <div class="col col-md-6 mb-3">
                            <div class="input-group">
                                <span class="pt-1 pr-2">Q4</span>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">$</span>
                                </div>
                                <input type="text" class="form-control comma_separate" name="g_fourth_quarter[{{$year}}]">
                            </div>

                        </div>
                    </div>
                    @endforeach

                    <!-- Calculate Revenue -->
                    <div class="mb-3">
                        <p class="mb-0">Calculated reduction in revenue:</p>
                        <span class="accordance">This percentage is calculated based on your provided gross
                            receipts</span>
                    </div>
                    @foreach($years as $year)
                    <!-- {{$year}} EXPENSES__QUARTER--FIELD 1ST-SECTION -->
                    <div class="form-group row business-gross-receipt">
                        <div class="ppp-business pb-2 pl-3 col col-12">
                            <!-- Label Gross--Receipt -->
                            <label for="ba236" class="mb-0">{{$year}} <b>expenses </b> for each quarter</label>
                            <span class="accordance">Enter '0' for quarters if your business did not have
                                expenses</span>
                        </div>
                        <!-- Q1---Field -->
                        <div class="col col-md-6 mb-3">
                            <div class="input-group">
                                <span class="pt-1 pr-2">Q1</span>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">$</span>
                                </div>
                                <input type="text" class="form-control comma_separate" name="e_first_quarter[{{$year}}]">
                            </div>

                        </div>
                        <!-- Q2---Field -->
                        <div class="col col-md-6 mb-3">
                            <div class="input-group">
                                <span class="pt-1 pr-2">Q2</span>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">$</span>
                                </div>
                                <input type="text" class="form-control comma_separate" name="e_second_quarter[{{$year}}]">
                            </div>

                        </div>
                        <!-- Q3---Field -->
                        <div class="col col-md-6 mb-3">
                            <div class="input-group">
                                <span class="pt-1 pr-2">Q3</span>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">$</span>
                                </div>
                                <input type="text" class="form-control comma_separate" name="e_third_quarter[{{$year}}]">
                            </div>

                        </div>
                        <!-- Q4---Field -->
                        <div class="col col-md-6 mb-3">
                            <div class="input-group">
                                <span class="pt-1 pr-2">Q4</span>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">$</span>
                                </div>
                                <input type="text" class="form-control comma_separate" name="e_fourth_quarter[{{$year}}]">
                            </div>

                        </div>
                    </div>
                    <!-- 2020 EXPENSES__QUARTER--FIELD 1ST-SECTION -->
                    @endforeach
                    <!-- CHECK__RADIO--BUTTON -->
                    <div class="form-group row">
                        <div class="col col-md-12">
                            <div>
                                <ul class="list-group list-group-flush ml-3">
                                    <li class="list-group-item p-0">
                                        <!-- Default checked -->
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="accordance" name="agree_to_terms">
                                            <label class="custom-control-label accordance" for="accordance">
                                                In accordance with <a href="#">VFA E-Signature agreement,</a> by
                                                checking
                                                this box I verify that the information I provided is accurate and could
                                                be
                                                supported by valid documentation that would satisfy an audit by the SBA.
                                            </label>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- SUBMIT__BUTTON--NEXT -->
                <div class="footer">
                    <div class="footer-form">
                        <button type="button" name="previous" data-value="basic_info" class="previousBtn action-button btn-primary" value="Previous" style="float: left;" />
                        <span>Previous</span>
                        </button>
                        <button type="submit" name="next" class="btn action-button btn-primary" value="Next" style="float: right;" />
                        <span>Next</span>
                        </button>
                    </div>
                    <div class="skip-btn d-none">
                        <a href="#">Fwd <i class="fas fa-arrow-right"></i></a>
                    </div>
                </div>

            </form>
        </fieldset>

        <!-- (3rd FIELDSET)  Business Questions -->
        <fieldset id="fieldset_business_questions" class="fldset-steps">
            <form class="stepsform">

                <input type="hidden" name="tab_name" value="basic_info">
                <input type="hidden" name="step_name" value="business_questions">
                <div class="ppp-business-questions">
                    <div class="business_questions">
                        <h3 class="text-center">Business Questions</h3>
                        <p class="check_question">If questions (1), (2), (4), or (5) below are answered "Yes" or
                            question
                            (6) below is answered "No", the loan will not be approved.</p>
                        <p class="check_question">By Checking the Boxes You Make the Following Representations,
                            Authorizations, and Certifications:</p>
                    </div>
                    <!-- ORDERD__LIST--QUESTIONARE===SECTION -->
                    <ol>
                        <div class="form-group">
                            <!--  Question-list-1 -->
                            <div class="questioner_list row">
                                <div class="col col-md-9 pr-0 mb-4">
                                    <li class="question_list">Is the Applicant or any owner of the Applicant presently
                                        suspended, debarred, proposed for debarment, declared ineligible, voluntarily
                                        excluded from participation in this transaction by any Federal
                                        department or agency, or presently involved in any bankruptcy?
                                    </li>
                                </div>
                                <div class="col col-md-3 col-12 text-left p-0">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <label class="form-row check_question">Yes
                                            <input class="d-block ml-2" type="radio" @if(!empty($businessQuestions['first_question']) && $businessQuestions['first_question']=='Yes' ) checked="checked" @endif value='Yes' name="first_question">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <label class="form-row check_question">No
                                            <input class="d-block ml-1" type="radio" value='No' @if(!empty($businessQuestions['first_question']) && $businessQuestions['first_question']=='No' ) checked="checked" @endif name="first_question">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <!--  Question-list-2 -->
                            <div class="questioner_list row">
                                <div class="col col-md-9 pr-0 mb-4">
                                    <li class="question_list">Has the Applicant, any owner of the Applicant, or any
                                        business
                                        owned or controlled by any of them, ever obtained a direct or guaranteed loan
                                        from
                                        SBA or any other Federal agency that is currently
                                        delinquent or has defaulted in the last 7 years and caused a loss to the
                                        government?
                                    </li>
                                </div>
                                <div class="col col-md-3 col-12 text-left p-0">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <label class="form-row check_question">Yes
                                            <input class="d-block ml-2" type="radio" value='Yes' @if(!empty($businessQuestions['second_question']) && $businessQuestions['second_question']=='Yes' ) checked="checked" @endif name="second_question">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <label class="form-row check_question">No
                                            <input class="d-block ml-1" type="radio" value='No' @if(!empty($businessQuestions['second_question']) && $businessQuestions['second_question']=='No' ) checked="checked" @endif name="second_question">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <!--  Question-list-3 -->
                            <div class="questioner_list row">
                                <div class="col col-md-9 pr-0 mb-4">
                                    <li class="question_list">Is the Applicant or any owner of the Applicant an owner of
                                        any
                                        other business, or have common management with, any other business?
                                    </li>
                                </div>
                                <div class="col col-md-3 col-12 text-left p-0">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <label class="form-row check_question">Yes
                                            <input class="d-block ml-2" type="radio" @if(!empty($businessQuestions['third_question']) && $businessQuestions['third_question']=='Yes' ) checked="checked" @endif value='Yes' name="third_question">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <label class="form-row check_question">No
                                            <input class="d-block ml-1" type="radio" value='No' @if(!empty($businessQuestions['third_question']) && $businessQuestions['third_question']=='No' ) checked="checked" @endif name="third_question">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <!--  Question-list-4 -->
                            <div class="questioner_list row">
                                <div class="col col-md-9 pr-0 mb-4">
                                    <li class="question_list">Is the Applicant (if an individual) or any individual
                                        owning
                                        20% or more of the equity of the Applicant presently incarcerated or, for any
                                        felony, presently subject to an indictment, criminal information,
                                        arraignment, or other means by which formal criminal charges are brought in any
                                        jurisdiction?
                                    </li>
                                </div>
                                <div class="col col-md-3 col-12 text-left p-0">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <label class="form-row check_question">Yes
                                            <input class="d-block ml-2" type="radio" value='Yes' @if(!empty($businessQuestions['fourth_question']) && $businessQuestions['fourth_question']=='Yes' ) checked="checked" @endif name="fourth_question">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <label class="form-row check_question">No
                                            <input class="d-block ml-1" type="radio" value='Yes' @if(!empty($businessQuestions['fourth_question']) && $businessQuestions['fourth_question']=='No' ) checked="checked" @endif name="fourth_question">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <!--  Question-list-5 -->
                            <div class="questioner_list row">
                                <div class="col col-md-9 pr-0 mb-4">
                                    <li class="question_list">Within the last 5 years, for any felony involving fraud,
                                        bribery, embezzlement, or a false statement in a loan application or an
                                        application
                                        for federal financial assistance, or within the last year,
                                        for any other felony, has the Applicant (if an individual) or any owner of the
                                        Applicant 1) been convicted; 2) pleaded guilty; 3) pleaded nolo contendere; or
                                        4)
                                        been placed on any form of parole
                                        or probation (including probation before judgment)?
                                    </li>
                                </div>
                                <div class="col col-md-3 col-12 text-left p-0">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <label class="form-row check_question">Yes
                                            <input class="d-block ml-2" type="radio" @if(!empty($businessQuestions['fifth_question']) && $businessQuestions['fifth_question']=='Yes' ) checked="checked" @endif value='Yes' name="fifth_question">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <label class="form-row check_question">No
                                            <input class="d-block ml-1" type="radio" value='No' @if(!empty($businessQuestions['fifth_question']) && $businessQuestions['fifth_question']=='No' ) checked="checked" @endif name="fifth_question">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <!--  Question-list-6 -->
                            <div class="questioner_list row">
                                <div class="col col-md-9 pr-0">
                                    <li class="question_list">Is the United States the principal place of residence for
                                        all
                                        employees of the Applicant included in the Applicant’s payroll calculation
                                        above?
                                    </li>
                                </div>
                                <div class="col col-md-3 col-12 text-left p-0">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <label class="form-row check_question">Yes
                                            <input class="d-block ml-2" type="radio" @if(!empty($businessQuestions['sixth_question']) && $businessQuestions['sixth_question']=='Yes' ) checked="checked" @endif value='Yes' name="sixth_question">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <label class="form-row check_question">No
                                            <input class="d-block ml-1" type="radio" value='No' @if(!empty($businessQuestions['sixth_question']) && $businessQuestions['sixth_question']=='No' ) checked="checked" @endif name="sixth_question">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <!--  Question-list-7 -->
                            <div class="questioner_list row">
                                <div class="col col-md-9 pr-0 pt-3">
                                    <li class="question_list">Is the Applicant a franchise that is listed in the SBA’s
                                        Franchise Directory?
                                    </li>
                                </div>
                                <div class="col col-md-3 col-12 text-left p-0">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <label class="form-row check_question">Yes
                                            <input class="d-block ml-2" type="radio" @if(!empty($businessQuestions['seven_question']) && $businessQuestions['seven_question']=='Yes' ) checked="checked" @endif value='Yes' name="seven_question">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <label class="form-row check_question">No
                                            <input class="d-block ml-1" type="radio" value='No' @if(!empty($businessQuestions['seven_question']) && $businessQuestions['seven_question']=='No' ) checked="checked" @endif name="seven_question">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <!--  Question-list-8 -->
                            <div class="questioner_list row">
                                <div class="col col-md-9 pr-0 mb-4 pt-3">
                                    <li class="question_list">Is the Applicant a franchise?
                                    </li>
                                </div>
                                <div class="col col-md-3 col-12 text-left p-0">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <label class="form-row check_question">Yes
                                            <input class="d-block ml-2" type="radio" @if(!empty($businessQuestions['eight_question']) && $businessQuestions['eight_question']=='Yes' ) checked="checked" @endif value='Yes' name="eight_question">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <label class="form-row check_question">No
                                            <input class="d-block ml-1" type="radio" value='No' @if(!empty($businessQuestions['eight_question']) && $businessQuestions['eight_question']=='No' ) checked="checked" @endif name="eight_question">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ol>
                </div>
                <!-- SUBMIT__BUTTON--NEXT -->
                <div class="footer">
                    <div class="footer-form">
                        <button type="button" name="previous" data-value="basic_info" class="previousBtn action-button btn-primary" value="Previous" style="float: left;" />
                        <span>Previous</span>
                        </button>
                        <button type="submit" name="next" class="btn action-button btn-primary" value="Next" style="float: right;" />
                        <span>Next</span>
                        </button>
                    </div>
                    <div class="skip-btn d-none">
                        <a href="#">Fwd <i class="fas fa-arrow-right"></i></a>
                    </div>
                </div>
            </form>
        </fieldset>
    </div>{{-- msform --}}
</div>
@endsection

@push('scripts')
<script>
    $(document).ready(function() {
        if($("#average_monthly_payroll").val() !=''){

            var average_monthly_payroll = $("#average_monthly_payroll").val(); 
            var number = removeCommas(average_monthly_payroll);
            $(".loan_amount").text(CommaFormatted(parseInt(number)*2.5));
            $("#loan_amount").val(parseInt(number)*2.5);
        }
         $("#average_monthly_payroll").keyup(function() {
             var average_payroll = $(this).val();
            $("#average_payroll").text(average_payroll);
            a = removeCommas(average_payroll); 
            $(".loan_amount").text(CommaFormatted(parseInt(a)*2.5));
            $("#loan_amount").val(parseInt(a)*2.5);
            });

        $(".stepsform").submit(function(event) {
            event.preventDefault(); //prevent default action 
            var post_url                = $(this).attr("action"); //get form action url
            var request_method          = $(this).attr("method"); //get form GET/POST method
            var form_data               = new FormData(this); //Creates new FormData object
            var average_monthly_payroll = $("#average_monthly_payroll").val();
            var financial_amount        = $("#financial_amount").val();
            var financial_amount        = parseInt(removeCommas(financial_amount));
            var loan_amount             = parseInt(removeCommas(average_monthly_payroll))*2.5;  
            if(financial_amount > loan_amount){
                $("#error_financial_amount").show();
                $("#financial_amount").addClass('is-invalid'); 
                $("#error_financial_amount").text("Financial amount sould not greater than "+loan_amount);
                 $( "#financial_amount" ).focus();
                return false;
            }

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $("#date_greater").hide();
            $.ajax({
                url: "{{route('ppp.basicinfo.store')}}",
                method: 'POST',
                data: form_data,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 800000,
                success: function(result) {
                    if (result.errors == true) {
                        $("#error_" + result.preStep).show();
                        $.each(result.errorsMsg, function(index, item) {
                            $.each(item, function(key, value) {
                                $("#" + index).removeClass('is-invalid');
                                $("#error_" + index).show();
                                $("#error_" + index).text(value);
                                $("#" + index).addClass('is-invalid');
                            });
                        });
                        return false;
                    } else {
                        $('.fldset-steps').hide();
                        $('#fieldset_' + result.nextStep).show();
                    }
                    if (typeof result.nextTabURL != 'undefined' && result.nextTabURL !=
                        '') {
                        window.location.replace(result.nextTabURL);
                    }
                }
            });

        });

        $(".previousBtn").click(function(event) {
            event.preventDefault();
            var fieldName = $(this).data('value');
            $.ajax({
                url: "{{ route('ppp.return.steps') }}",
                method: 'POST',
                data: 'step_name=' + fieldName,
                dataType: 'json',
                success: function(result) {
                    $('.fldset-steps').hide();
                    if (result.nextStep != "") {
                        $('#fieldset_' + result.nextStep).hide();
                    }
                    if (result.preStep != "") {
                        $('#fieldset_' + result.preStep).show();
                    }
                }
            });

        });

        $("#business_type").change(function() {
            var business_type = $(this).val();
            console.log(business_type);
            switch (business_type) {
                case 'partnership':
                    $('.business-tax').show();
                    $('.business-tin').hide();
                    break;
                case 'llc':
                    $('.business-tax').hide();
                    $('.business-tin').show();
                    break;
                case 'selfEmployed':
                    $('.business-tax').hide();
                    $('.business-tin').show();
                    break;
                case 'vetOrg':
                    $('.business-tax').show();
                    $('.business-tin').hide();
                    break;

                default:
                    $('.business-tax').show();
                    $('.business-tin').hide();
                    break;
            }

        });

        $("input[name$='sbayear']").click(function() {
            var has_ppload = $(this).val();
            if (has_ppload == 'Yes') {
                $(".sbadetails").show();
            } else {
                $(".sbadetails").hide();
            }
        });


       


        function CommaFormatted(amount) {
            var delimiter = ","; // replace comma if desired 
            var d = amount;
            var i = parseInt(amount);
            if (isNaN(i)) {
                return '';
            }
            var minus = '';
            if (i < 0) {
                minus = '-';
            }
            i = Math.abs(i);
            var n = new String(i);
            var a = [];
            while (n.length > 3) {
                var nn = n.substr(n.length - 3);
                a.unshift(nn);
                n = n.substr(0, n.length - 3);
            }
            if (n.length > 0) {
                a.unshift(n);
            }
            n = a.join(delimiter);
            if (d.length < 1) {
                amount = n;
            } else {
                amount = n
            }
            amount = minus + amount;
            return amount;
        }
        $('.comma_separate').on('keyup', function() {
            var number = removeCommas($(this).val());
            var formattedNumber = CommaFormatted(number);
            $(this).val(formattedNumber);
        });

        function removeCommas(str) {
            while (str.search(",") >= 0) {
                str = (str + "").replace(',', '');
            }
            return str;
        }

    });
</script>
@endpush
@extends('layouts.pppapp')
@push('styles')
@endpush
@section('content')
@php 
@endphp
<!-- ==== Tab-3-Content (Owner-Information) START FROM HERE === -->
   <div class="tab-pane fade  show active" id="owner-info" role="tabpanel" aria-labelledby="owner-info-tab">
       <h2 class="page-header mb-0 mt-3 text-center">Paycheck Protection Program Application</h2>
       <div class="form-group" id="msform">
           <!-- (1st FIELDSET) Owner Information -->
        @if(!Auth::check())
        <fieldset id="fieldset_loan_profile" class="fldset-steps">
            <div class="form-control-feedback text-danger" id="error_loan_profile" style="display: none">
                Please Fill the required fields.
            </div>
            <form  id="stepsform" method="POST" action="{{route('create.ppp.profile')}}">
                <input type="hidden" name="tab_name" value="owner_info">
                <input type="hidden" name="step_name" value="loan_profile">
                <div class="header-info text-center">
                    <h3>Create your loan profile</h3>
                    <span class="hr-divider"><span>or</span></span>
                    <h5>Already have an account?<a class="pl-2" href="https://virtualfundassist.com/lendio/tabs/login" alt="signin">Sign in</a></h5>
                </div>
                <!-- FIRST & LAST NANME FIELD -->
                <div class="form-group row name">
                    <div class="col col-md-6 col-12">
                        <label for="firstName" class="control-label">Name</label>
                        <input value="{{@$owner_info['loan_profile']['first_name'] }}" id="first_name" data-cy="firstName" name="first_name" type="text" placeholder="First" class="form-control">
                        <div class="form-control-feedback text-danger" id="error_first_name" style="display: none">Field is required</div>
                    </div>
                    <div class="col col-md-6 col-12"><label>&nbsp;</label>
                        <input value="{{@$owner_info['loan_profile']['last_name'] }}" id="lastName" data-cy="lastName" name="last_name" type="text" placeholder="Last" class="form-control">
                    </div>
                </div>

                <!-- BUSINESS NANME FIELD -->
                <div class="form-group row business-name">
                    <div class="col col-12">
                        <label for="businessName" class="control-label">Business Name</label>
                        <input value="{{@$owner_info['loan_profile']['business_name'] }}" id="business_name" name="business_name" type="text" placeholder="Business Name" class="form-control">
                        <div class="form-control-feedback text-danger" id="error_business_name" style="display: none">Field is required</div>
                    </div>
                </div>

                <!-- MOBILE PHONE FIELD -->
                <div class="form-group row phone">
                    <div class="col col-12">
                        <label for="phone" class="control-label">Mobile Phone</label>
                        <input value="{{@$owner_info['loan_profile']['mobile_number'] }}" id="mobile_number" name="mobile_number" type="tel" placeholder="(___)___-____" class="form-control" maxlength="12">
                        <div class="form-control-feedback text-danger" id="error_mobile_number" style="display: none">Field is required</div>
                    </div>
                </div>

                <!-- EMAIL FIELD -->
                <div class="form-group row email">
                    <div class="col col-12">
                        <label for="email" class="control-label">Email address</label>
                        <div style="width: 100%;">
                            <input value="{{@$owner_info['loan_profile']['email'] }}" id="email" name="email" type="email" placeholder="Email" class="form-control">
                            <div class="form-control-feedback text-danger" id="error_email" style="display: none">Field is required</div>
                        </div>
                    </div>
                </div>

                <!-- Pass/ConfirmPass FIELD -->
                <div class="row">
                    <div class="col col-12">
                        <div class="form-group w-100">
                            <label for="password">Password</label>
                            <input type="password" id="password" name="password" class="form-control">
                        </div>
                        <div class="form-group w-100">
                            <label for="password_confirmation">Confirm Password</label>
                            <input type="password" id="password_confirmation" name="password_confirmation" class="form-control">
                            <div class="form-control-feedback text-danger" id="error_password" style="display: none">Field is required</div>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <div class="footer-form">
                        <button type="submit" name="next" class="btn btn-primary text-center action-button" value="Next">
                            <span>continue</span>
                        </button>
                    </div>
                    <div class="skip-btn d-none">
                        <a href="#">Fwd <i class="fas fa-arrow-right"></i></a>
                    </div>
                </div>
            </form>
        </fieldset>
        @endif

           <fieldset class="fldset-steps"  id="fieldset_ownership_changed">
            <form class="stepsform">
                <input type="hidden" name="tab_name" value="owner_info">
                <input type="hidden" name="step_name" value="ownership_changed">
               <div class="ppp-owner-information">
                   <h3 class="text-center">Owner Information</h3>
                   <div class="form-group">
                       <!--  Ownership-list -->
                       <div class="questioner_list row">
                           <div class="col col-md-9 pr-0 mb-4">
                               <span class="question_list">Has ownership of the business changed since First Draw of
                                   PPP? (Beneficial Ownership only)
                                   <span class="tooltip_hover">
                                       <a>
                                           <svg class="svg-inline--fa fa-question-circle fa-w-16" viewBox="0 0 512 512">
                                               <path fill="currentColor"
                                                   d="M256 8C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm0 448c-110.532 0-200-89.431-200-200 0-110.495 89.472-200 200-200 110.491 0 200 89.471 200 200 0 110.53-89.431 200-200 200zm107.244-255.2c0 67.052-72.421 68.084-72.421 92.863V300c0 6.627-5.373 12-12 12h-45.647c-6.627 0-12-5.373-12-12v-8.659c0-35.745 27.1-50.034 47.579-61.516 17.561-9.845 28.324-16.541 28.324-29.579 0-17.246-21.999-28.693-39.784-28.693-23.189 0-33.894 10.977-48.942 29.969-4.057 5.12-11.46 6.071-16.666 2.124l-27.824-21.098c-5.107-3.872-6.251-11.066-2.644-16.363C184.846 131.491 214.94 112 261.794 112c49.071 0 101.45 38.304 101.45 88.8zM298 368c0 23.159-18.841 42-42 42s-42-18.841-42-42 18.841-42 42-42 42 18.841 42 42z">
                                               </path>
                                           </svg>
                                       </a>
                                       <div id="tooltip" class="h-tooltip vue-tooltip tooltip-inner"
                                           x-placement="right">
                                           <div class="tooltip-content">A Beneficial Owner (“Owner”) is each individual,
                                               who, directly or indirectly, through any contract, arrangement,
                                               understanding, relationship or otherwise, owns 20 percent or more of the
                                               equity interests of the Business for which you are applying. If a trust
                                               owns directly or indirectly, through any contract, arrangement,
                                               understanding, relationship or otherwise, 20 percent or more of the
                                               equity interests of a legal entity customer, the owner is the trustee.
                                               <div class="tooltip-arrow"></div>
                                           </div>
                                       </div>
                                   </span>
                               </span>
                           </div>
                           <div class="col col-md-3 col-12 text-left p-0">
                               <div class="custom-control custom-radio custom-control-inline">
                                   <label class="form-row check_question">Yes
                                       <input @if(!empty($ownerinfo['ownership_changed']['ownership_changed']) && $ownerinfo['ownership_changed']['ownership_changed'] == 'Yes') checked @endif class="d-block ml-2" type="radio" value="Yes" checked="checked" name="ownership_changed">
                                       <span class="checkmark"></span>
                                   </label>
                               </div>
                               <div class="custom-control custom-radio custom-control-inline">
                                   <label class="form-row check_question">No
                                       <input @if(!empty($ownerinfo['ownership_changed']['ownership_changed']) && $ownerinfo['ownership_changed']['ownership_changed'] == 'No') checked @endif class="d-block ml-1" type="radio" value="No" name="ownership_changed">
                                       <span class="checkmark"></span>
                                   </label>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
               <!-- SUBMIT__BUTTON--NEXT -->
                 <!-- SUBMIT__BUTTON--NEXT -->
                <div class="footer">
               <div class="footer-form"> 
                  <button type="submit" name="next" class="btn action-button btn-primary" value="Next" style="float: right;" />
                  <span>Next</span>
                  </button>
               </div>
               <div class="skip-btn d-none">
                  <a href="#">Fwd <i class="fas fa-arrow-right"></i></a>
               </div>
            </div>

            </form>
           </fieldset>

           <!-- (2nd FIELDSET) Owner Information -->
           <fieldset  class="fldset-steps"  id="fieldset_business_percentage">
            <form class="stepsform">
                <input type="hidden" name="tab_name" value="owner_info">
                <input type="hidden" name="step_name" value="business_percentage">

               <div class="ppp-owner-information">
                   <h3 class="text-center">Owner Information</h3>
                   <div class="form-group mb-4">
                       <!--  Ownership-list -->
                       <div class="questioner_list row">
                           <div class="col col-md-9 pr-0 mb-4">
                               <span class="question_list">Do you or any individual own 20% or more of the business?
                               </span>
                           </div>
                           <div class="col col-md-3 col-12 text-left p-0">
                               <div class="custom-control custom-radio custom-control-inline">
                                   <label class="form-row check_question">Yes
                                       <input @if(!empty($ownerinfo['business_percentage']['ownership_has_twentypercent_share']) && $ownerinfo['business_percentage']['ownership_has_twentypercent_share'] == 'Yes') checked @endif class="d-block ml-2" type="radio" value="Yes" checked="checked" name="ownership_has_twentypercent_share">
                                       <span class="checkmark"></span>
                                   </label>
                               </div>
                               <div class="custom-control custom-radio custom-control-inline">
                                   <label class="form-row check_question">No
                                       <input @if(!empty($ownerinfo['business_percentage']['ownership_has_twentypercent_share']) && $ownerinfo['business_percentage']['ownership_has_twentypercent_share'] == 'No') checked @endif class="d-block ml-1" type="radio" value="No" name="ownership_has_twentypercent_share">
                                       <span class="checkmark"></span>
                                   </label>
                               </div>
                           </div>
                       </div>
                       <!--  Ownership-Percentage -->
                       <div class="questioner_list row">
                           <div class="col col-md-12 mb-2">
                               <span class="question_list">What is your ownership percentage (direct or indirect)?
                                   <span class="tooltip_hover">
                                       <a>
                                           <svg class="svg-inline--fa fa-question-circle fa-w-16" viewBox="0 0 512 512">
                                               <path fill="currentColor"
                                                   d="M256 8C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm0 448c-110.532 0-200-89.431-200-200 0-110.495 89.472-200 200-200 110.491 0 200 89.471 200 200 0 110.53-89.431 200-200 200zm107.244-255.2c0 67.052-72.421 68.084-72.421 92.863V300c0 6.627-5.373 12-12 12h-45.647c-6.627 0-12-5.373-12-12v-8.659c0-35.745 27.1-50.034 47.579-61.516 17.561-9.845 28.324-16.541 28.324-29.579 0-17.246-21.999-28.693-39.784-28.693-23.189 0-33.894 10.977-48.942 29.969-4.057 5.12-11.46 6.071-16.666 2.124l-27.824-21.098c-5.107-3.872-6.251-11.066-2.644-16.363C184.846 131.491 214.94 112 261.794 112c49.071 0 101.45 38.304 101.45 88.8zM298 368c0 23.159-18.841 42-42 42s-42-18.841-42-42 18.841-42 42-42 42 18.841 42 42z">
                                               </path>
                                           </svg>
                                       </a>
                                       <div id="tooltip" class="h-tooltip vue-tooltip tooltip-inner"
                                           x-placement="right">
                                           <div class="tooltip-content">Indirect Ownership is defined as having equity
                                               interest of a business through some other business entity. For example,
                                               if you own 100% of Company A, which owns 25% of Company B, then you are
                                               an indirect owner of Company B.
                                               <div class="tooltip-arrow"></div>
                                           </div>
                                       </div>
                                   </span>
                               </span>
                               <!-- Ownership-Percentage Input---Field -->
                               <div class="input-group">
                                   <input value="{{@$ownerinfo['business_percentage']['business_percentage']}}" name="business_percentage" type="number" class="form-control" >
                                   <div class="input-group-prepend">
                                       <span class="input-group-text" style="border-radius: 0em;">%</span>
                                   </div>
                               </div>
                           </div>
                       </div>
                       <span class="question_list">Any individual with at least 20% share in the business must be listed
                           on the application. Add these owners using the link below.
                       </span>
                   </div>
               </div>
                 <!-- SUBMIT__BUTTON--NEXT -->
                <div class="footer">
               <div class="footer-form">
                  <button type="button" name="previous" data-value="ownership_changed" class="previousBtn action-button btn-primary" value="Previous" style="float: left;" />
                  <span>Previous</span>
                  </button>
                  <button type="submit" name="next" class="btn action-button btn-primary" value="Next" style="float: right;" />
                  <span>Next</span>
                  </button>
               </div>
               <div class="skip-btn d-none">
                  <a href="#">Fwd <i class="fas fa-arrow-right"></i></a>
               </div>
            </div>
            </form>
           </fieldset>

       </div>
   </div>
@endsection

@push('scripts')
<script>
    $(document).ready(function () { 
            $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
         });

        $("#stepsform").submit(function(event) {
         event.preventDefault(); //prevent default action 
         var post_url       = $(this).attr("action"); //get form action url
         var request_method = $(this).attr("method"); //get form GET/POST method
         var form_data      = new FormData(this); //Creates new FormData object 
         $.ajax({
            url: post_url,
            method: request_method,
            data: form_data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 800000,
            success: function(result) {
                if (result.errors == true) {
                        //$("#error_"+result.preStep).show();
                        $.each(result.errorsMsg, function(index, item) {
                            $.each(item, function(key, value) {
                                $("#" + index).removeClass('is-invalid');
                                $("#error_" + index).show();
                                $("#error_" + index).text(value);
                                $("#" + index).addClass('is-invalid');
                            });
                        });
                        return false;
                    } else {
                        $('.fldset-steps').hide();
                        $('#fieldset_' + result.nextStep).show();
                    }
               if (typeof result.nextTabURL != 'undefined' && result.nextTabURL != '') {
                  window.location.replace(result.nextTabURL);
               } 
            }
         });

      });

      $(".stepsform").submit(function(event) {
         event.preventDefault(); //prevent default action 
         var post_url       = $(this).attr("action"); //get form action url
         var request_method = $(this).attr("method"); //get form GET/POST method
         var form_data      = new FormData(this); //Creates new FormData object
         $.ajaxSetup({
            headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
         });

         $("#date_greater").hide();
         $.ajax({
            url: "{{route('ppp.basicinfo.store')}}",
            method: 'POST',
            data: form_data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 800000,
            success: function(result) {
                if (result.errors == true) {
                        $("#error_" + result.preStep).show();
                        $.each(result.errorsMsg, function(index, item) {
                            $.each(item, function(key, value) {
                                $("#" + index).removeClass('is-invalid');
                                $("#error_" + index).show();
                                $("#error_" + index).text(value);
                                $("#" + index).addClass('is-invalid');
                            });
                        });
                        return false;
                    } else {
                        $('.fldset-steps').hide();
                        $('#fieldset_' + result.nextStep).show();
                    }
               if (typeof result.nextTabURL != 'undefined' && result.nextTabURL != '') {
                  window.location.replace(result.nextTabURL);
               } 
            }
         });

      });

        $(".previousBtn").click(function(event) {
         event.preventDefault();
         var fieldName = $(this).data('value');
         $.ajax({
            url: "{{ route('ppp.return.steps') }}",
            method: 'POST',
            data: 'step_name=' + fieldName,
            dataType: 'json',
            success: function(result) { 
               $('.fldset-steps').hide();
               if (result.nextStep != "") {
                  $('#fieldset_' + result.nextStep).hide();
               }
               if (result.preStep != "") {
                  $('#fieldset_' + result.preStep).show();
               }
            }
         });

      });



    });
</script>
@endpush
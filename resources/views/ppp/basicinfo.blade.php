@extends('layouts.pppapp')
@push('styles')
@endpush
@section('content')
@php
@endphp
<div class="tab-pane fade show active" id="getting-started" role="tabpanel" aria-labelledby="getting-started-tab">
    <h2 class="page-header mb-0 mt-3 text-center">Paycheck Protection Program Application</h2>
    <form class="form-group" id="msform">
        <input type="hidden" name="step_name" value="ppp_loan">
        <input type="hidden" name="tab_name" value="getting_started">
        <!-- (1st FILEDSET) -->
        <fieldset class="fldset-steps" id="fieldset_ppp_loan">

            <div class="ppp-loan-started mb-4">

                <h3 class="text-center">Let's get you started on the right path</h3>
                <div class="row d-flex align-items-center mb-4">
                    <div class="col col-md-9 col-12 text-left p-0 pl-3">
                        Are you applying for your second PPP Loan?
                    </div>
                    <div class="col col-md-3 col-12 text-left p-0">
                        <div class="custom-control custom-radio custom-control-inline">
                            <label class="form-row">Yes
                                <input class="d-block ml-2" type="radio" value="Yes" name="has_ppload">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <label class="form-row">No
                                <input class="d-block ml-1" type="radio" checked value="No" name="has_ppload">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    </div>
                </div>
                <!-- Yes/NO Option Operation -->
                <div class="optional_form" style="display:none">{{-- Optional for starts --}}
                    <div class="form-group">
                        <div class="ppp-first-loan borrower-attribute-name">
                            <label for="ba236" class="d-flex">First PPP SBA Loan #
                                <span>
                                    <a>
                                        <svg class="svg-inline--fa fa-question-circle fa-w-16" viewBox="0 0 512 512">
                                            <path fill="currentColor"
                                                d="M256 8C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm0 448c-110.532 0-200-89.431-200-200 0-110.495 89.472-200 200-200 110.491 0 200 89.471 200 200 0 110.53-89.431 200-200 200zm107.244-255.2c0 67.052-72.421 68.084-72.421 92.863V300c0 6.627-5.373 12-12 12h-45.647c-6.627 0-12-5.373-12-12v-8.659c0-35.745 27.1-50.034 47.579-61.516 17.561-9.845 28.324-16.541 28.324-29.579 0-17.246-21.999-28.693-39.784-28.693-23.189 0-33.894 10.977-48.942 29.969-4.057 5.12-11.46 6.071-16.666 2.124l-27.824-21.098c-5.107-3.872-6.251-11.066-2.644-16.363C184.846 131.491 214.94 112 261.794 112c49.071 0 101.45 38.304 101.45 88.8zM298 368c0 23.159-18.841 42-42 42s-42-18.841-42-42 18.841-42 42-42 42 18.841 42 42z">
                                            </path>
                                        </svg>
                                    </a>
                                </span>
                            </label>
                        </div>
                        <div class="type-container">
                            <input value="" id="sba_loan_number" name="sba_loan_number" type="number"
                                placeholder="01-23456789" class="form-control" onKeyPress="if(this.value.length==10) return false;">
                        </div>
                        <div class="form-control-feedback text-danger required-field text-left"
                            id="error_sba_loan_number">
                            SBA loan numbers should not be less than 10 digits in length</div>
                    </div>

                    <div class="ppp-extra-loan">
                        <div class="ppp-first-loan borrower-attribute-name">
                            <label for="ba236" class="d-flex">Funded Loan Amount
                                <span>
                                    <a>
                                        <svg class="svg-inline--fa fa-question-circle fa-w-16" viewBox="0 0 512 512">
                                            <path fill="currentColor"
                                                d="M256 8C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm0 448c-110.532 0-200-89.431-200-200 0-110.495 89.472-200 200-200 110.491 0 200 89.471 200 200 0 110.53-89.431 200-200 200zm107.244-255.2c0 67.052-72.421 68.084-72.421 92.863V300c0 6.627-5.373 12-12 12h-45.647c-6.627 0-12-5.373-12-12v-8.659c0-35.745 27.1-50.034 47.579-61.516 17.561-9.845 28.324-16.541 28.324-29.579 0-17.246-21.999-28.693-39.784-28.693-23.189 0-33.894 10.977-48.942 29.969-4.057 5.12-11.46 6.071-16.666 2.124l-27.824-21.098c-5.107-3.872-6.251-11.066-2.644-16.363C184.846 131.491 214.94 112 261.794 112c49.071 0 101.45 38.304 101.45 88.8zM298 368c0 23.159-18.841 42-42 42s-42-18.841-42-42 18.841-42 42-42 42 18.841 42 42z">
                                            </path>
                                        </svg>
                                    </a>
                                </span>
                            </label>
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">$</span>
                            </div>
                            <input id="financial_amount" name="financial_amount" type="text" class="form-control">
                        </div>
                        <div class="form-control-feedback text-danger required-field text-left"
                            id="error_financial_amount"></div>

                    </div>
                </div>{{-- Optional Form ends --}}

            </div>

            <div class="footer">
                <div class="footer-form">
                    <button type="submit" class="btn btn-primary text-center action-button" value="Next">
                        <span>Next</span>
                    </button>
                </div>
             {{--    <div class="footer-form" id="no_loan">
                    <button onClick="location.href='{{route('ppp.businessinfo')}}'" type="button"
                        class="btn btn-primary text-center" value="Next">
                        <span>Next</span>
                    </button>
                </div> --}}
            </div>


        </fieldset>

    </form>
</div>
@endsection

@push('scripts')
<script>
    $(document).ready(function () {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("input[name$='has_ppload']").click(function () {
            var has_ppload = $(this).val();
            if (has_ppload == 'Yes') {
                //$("#no_loan").hide();
                $(".optional_form").show();
            } else {
                //$("#no_loan").show();
                $(".optional_form").hide();
            }
        });
        //$(document).on('click',".action-button",function(event){
        $("#msform").submit(function (event) {
            var form_data = new FormData(this); //Creates new FormData object 
            event.preventDefault();
            var error = false;
            var has_ppload = $('input[name=has_ppload]:checked', '#msform').val();
            if(has_ppload ==  'Yes'){ 
            var sba_loan_number = $("#sba_loan_number").val();
            if (sba_loan_number.length < 10) {
                $("#error_sba_loan_number").show();
                $("#sba_loan_number").addClass("is-invalid");
                error = true;
            } else {
                $("#error_sba_loan_number").hide();
                $("#sba_loan_number").removeClass("is-invalid");
            }
            var financial_amount = $("#financial_amount").val();

            if (sba_loan_number == '') {
                $("#error_financial_amount").show();
                $("#financial_amount").addClass("is-invalid");
                error = true;
            } else {
                $("#error_financial_amount").hide();
                $("#financial_amount").removeClass("is-invalid");
            }
            }// END IF

            if (error == false) {
                $.ajax({
                    url: "{{ route('ppp.validate') }}",
                    method: 'POST',
                    data: form_data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    timeout: 800000,
                    success: function (result) {
                        if (result.errors == true) {
                            $("#error_financial_amount").show();
                            $("#error_financial_amount").text(result.errorsMsg);
                        } else {
                            if (typeof result.nextTabURL != 'undefined' && result
                                .nextTabURL != '') {
                                window.location.replace(result.nextTabURL);
                            }
                        }

                    }

                });
            }

        });



        function CommaFormatted(amount) {
            var delimiter = ","; // replace comma if desired 
            var d = amount;
            var i = parseInt(amount);
            if (isNaN(i)) {
                return '';
            }
            var minus = '';
            if (i < 0) {
                minus = '-';
            }
            i = Math.abs(i);
            var n = new String(i);
            var a = [];
            while (n.length > 3) {
                var nn = n.substr(n.length - 3);
                a.unshift(nn);
                n = n.substr(0, n.length - 3);
            }
            if (n.length > 0) {
                a.unshift(n);
            }
            n = a.join(delimiter);
            if (d.length < 1) {
                amount = n;
            } else {
                amount = n
            }
            amount = minus + amount;
            return amount;
        }
        $('#financial_amount').on('keyup', function () {
            var number = removeCommas($(this).val());
            var formattedNumber = CommaFormatted(number);
            $("#financial_amount").val(formattedNumber);
        });


        function removeCommas(str) {
            while (str.search(",") >= 0) {
                str = (str + "").replace(',', '');
            }
            return str;
        }

    }); // DOCUMENT READY ENDS HERE.
</script>
@endpush
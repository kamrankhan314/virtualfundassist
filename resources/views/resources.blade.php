@extends('layouts.userdashboard')
@push('styles')
@endpush
@section('content')
<div class="portalContent">
    <div class="infoNav row">
        <div class="titleNav text-left col col-lg-8 d-flex">
            <div class="titleDetail d-flex align-items-start flex-column justify-content-between">
                <div class="titlePhotoContainer mb-auto">
                    <div class="backgroundShadow">
                        <div class="pageTitle">
                            <h1>Your Resources</h1>
                        </div>
                    </div>
                    <p style="color:#8c9db3">Interested in other ways to build your business? Explore our alternative financing options and business solutions.</p>
                    <div class="optionalPhoto d-block d-lg-none text-right"></div>
                </div>
            </div>
        </div>
    </div>
    <div></div>
    <div class="row mb-3 mt-5">
        <div class="container expand-max-service-offers">
            <section class="row mx-0">
                <section class="flat-card col-12 mx-0 px-0 mb-4 portal-shadow border-0">
                    <div class="section-head">
                        <span>Other Financing Services</span>
                    </div>
                    <div class="message">
                        <div class="opt-in-box-container d-flex flex-wrap align-items-center py-4">
                            <div class="text-center mb-2 p-5 pt-lg-4 col-12 col-md-4">
                                <div class="opt-in-logo mx-auto">
                                    <img src="img/sunrise-logo.png">
                                </div>
                            </div>
                            <div class="col-12 col-md-8 right-side pt-1 pb-2 px-4 border-left border-md-0">
                                <div class="d-flex flex-wrap">
                                    <div class="col-12 col-lg-8">
                                        <h4>Get ready for your loan offers. Have a Sunrise bookkeeper create your P&amp;L</h4>
                                        <ul class="mb-3">
                                            <li>Your bookkeeper will ensure all of your reports are accurate</li>
                                            <li>Match and categorize all of your transactions</li>
                                            <li>Be available to answer your questions each month</li>
                                        </ul>
                                    </div>
                                    <div class="col-12 col-lg-4">
                                        <div class="option-banner">
                                            <div class="text-center h1 mb-0">50% off</div>
                                            <div class="text-center text-muted mb-2">your first month*</div>
                                        </div>
                                        <div class="d-flex justify-content-center text-center text-md-left">
                                            <button class="d-flex justify-content-center btn btn-primary text-wrap mb-3">
                                                <span>Get a bookkeeper</span>
                                            </button>
                                        </div>
                                        <div class="transcluded-option-description mb-3 gray-700">*Prices vary based on the number of transactions per month.</div>
                                    </div>
                                </div>
                                <div class="alert-wrapper my-3 closed" style="display: none;">
                                    <div class="alert alert-undefined">
                                        <div class="alert-content-wrapper w-100 d-flex">
                                            <span class="alert-icon icon-24-undefined" style="vertical-align: middle;">
                                                <i></i>
                                            </span>
                                            <span class="alert-title"></span>
                                            <span class="alert-content w-100">
                                                <div></div>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </section>
        </div>
        <div class="container expand-max-service-offers">
            <section class="row mx-0">
                <section class="flat-card col-12 mx-0 px-0 mb-4 portal-shadow border-0">
                    <div class="section-head">
                        <span>Partner Services</span>
                    </div>
                    <div class="message">
                        <div class="opt-in-box-container d-flex flex-wrap align-items-center py-4">
                            <div class="text-center mb-2 p-5 pt-lg-4 col-12 col-md-4">
                                <div class="opt-in-logo mx-auto">
                                    <img src="{{ asset('public/img/opt-in-coverwallet-logo.png')}}" alt="">
                                </div>
                            </div>
                            <div class="col-12 col-md-8 right-side pt-1 pb-2 px-4 border-left border-md-0">
                                <div class="d-flex flex-wrap">
                                    <div class="inline">
                                        <h4>Business Insurance Customized for Small Businesses</h4>
                                        <p>$99 Annual Fee Waived + Free Quotes</p>
                                        <ul class="mb-3">
                                            <li>Free one-on-one risk expert consultations</li>
                                            <li>Convenient online assessment and no-cost digital quotes</li>
                                            <li>All-in-one platform to deal with your business insurance needs</li>
                                            <li>Intuitive, hassle-free and paperless insurance experience</li>
                                        </ul>
                                    </div>
                                    <div class="inline">
                                        <div class="d-flex justify-content-center text-center text-md-left">
                                            <button class="d-flex justify-content-center btn btn-primary text-wrap mb-3">
                                                <span>Get Free Access</span>
                                            </button>
                                            <div class="alert-wrapper my-3 closed" style="display: none;">
                                                <div class="alert alert-undefined">
                                                    <div class="alert-content-wrapper w-100 d-flex">
                                                        <span class="alert-icon icon-24-undefined" style="vertical-align: middle;">
                                                            <i></i>
                                                        </span>
                                                        <span class="alert-title"></span>
                                                        <span class="alert-content w-100">
                                                            <div></div>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="transcluded-option-description mb-3 gray-700">Visit
                                            <a href="#">vfa.com</a> to learn more or <a href="tel:111111111">call us at (646) 111-111111</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="opt-in-box-container d-flex flex-wrap align-items-center py-4">
                            <div class="text-center mb-2 p-5 pt-lg-4 col-12 col-md-4">
                                <div class="opt-in-logo mx-auto">
                                    <img src="{{ asset('public/img/lexingtonlawlogo.png')}}" alt="">
                                </div>
                            </div>
                            <div class="col-12 col-md-8 right-side pt-1 pb-2 px-4 border-left border-md-0">
                                <div class="d-flex flex-wrap">
                                    <div class="inline">
                                        <h4>Get better business loan options by working to improve your credit</h4>
                                        <div class="transcluded-option-description mb-3 gray-700">
                                            <p>Better credit could help you qualify for a lower interest rate on your loan. Leverage Lexington Law Firm, trusted leaders in credit repair, to work to remove items hurting your credit report.</p>
                                            <p>In 2016, Lexington Law clients saw more than 9 million negative items from their past clients’ credit reports.
                                                <a href="#">How we count removals</a>.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="inline">
                                        <div class="mb-3">
                                            <form class="d-flex">
                                                <div class="custom-control custom-checkbox mb-0">
                                                    <input type="checkbox" id="Lexington Law Credit Repair2" class="custom-control-input">
                                                    <label for="Lexington Law Credit Repair2" class="custom-control-label">Yes, I would like to receive a FREE personalized credit repair analysis from a consultant at Lexington Law.*</label>
                                                </div>
                                            </form>
                                            <div class="alert-wrapper my-3 closed" style="display: none;">
                                                <div class="alert alert-undefined">
                                                    <div class="alert-content-wrapper w-100 d-flex">
                                                        <span class="alert-icon icon-24-undefined" style="vertical-align: middle;">
                                                            <i></i></span>
                                                        <span class="alert-title"></span>
                                                        <span class="alert-content w-100">
                                                            <div></div>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="transcluded-option-description mb-3 gray-700">*By checking the box above, I agree by electronic signature to: (1) be contacted by Lexington Law Firm about credit repair or credit repair marketing by a live agent, artificial or prerecorded voice, and SMS text at my residential or cellular number (303-184-9407), dialed manually or by autodialer, and by email (consent to be contacted is not a condition to purchase services); and (2) the
                                            <a href="#">Privacy Policy</a> and
                                            <a href="#">Terms of Use</a> (including this
                                            <a href="#">arbitration provision</a>).
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </section>
        </div>
    </div>



    <div class="disclaimer">
        <div class="disclaimerThanks">
            <p>Thank you for applying for a Paycheck Protection loan!</p>
        </div>
        <p class="ppp-alert-content">Congratulations! We received your application and are moving it into processing. In the meantime, keep a close eye on emails from Lendio. You can always log in to our site for instant status updates. We strongly encourage you to review our <a target="_blank" href="#">PPP Loan Relief</a> page offers answers to FAQs and Next Steps.
        </p>
    </div>
    <div class="next-steps row justify-content-center mt-5">
        <div class="col-md-5 col-12 justify-content-center justify-content-md-start">
            <h5>Have a question?</h5>
            <div class="portal-card">
                <section>
                    <div class="mr-md-2 funding-specialist-box d-flex">
                        <div class="fm-info">
                            <div class="font-medium">Funding Manager:</div>
                            <div class="font-medium">Asher Hamblin</div>
                            <a href="#" class="d-block">(385) 381-6716</a>
                            <a href="#">asher.hamblin@ls.lendio.com</a>
                        </div>
                        <div class="profile-img rounded-circle embed-responsive embed-responsive-1by1 float-md-right">
                            <div class="fallback-avatar embed-responsive-item d-flex justify-content-center align-items-center
                                                h3 text-uppercase text-light font-weight-bold">AH
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <div class="col-md-5 col-12">
            <h5>While You Wait</h5>
            <div class="portal-card wyw-links">
                <div class="d-flex align-items-center wyw-link-block">
                    <div class="mr-3 icon d-flex icon-list">
                        <img src="{{ asset('public/img/icon-list.png')}}" alt="">
                    </div>
                    <div>
                        <a href="#">Fill out our marketplace application to see if you qualify for other business financing</a>
                    </div>
                </div>
                <div class="d-flex align-items-center wyw-link-block">
                    <div class="mr-3 icon d-flex icon-file icon-list">
                        <img src="{{ asset('public/img/icon-file.png')}}" alt="">
                    </div>
                    <div>
                        <a href="#">Learn more about PPP loan forgiveness</a>
                    </div>
                </div>
                <div class="d-flex align-items-center wyw-link-block">
                    <div class="mr-3 icon d-flex icon-question icon-list">
                        <img src="{{ asset('public/img/icon-question.png')}}" alt="">
                    </div>
                    <div>
                        <a href="#">Still have questions? FAQs</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
@endpush
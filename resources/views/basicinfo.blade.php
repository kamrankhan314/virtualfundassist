@extends('layouts.master')
@push('styles')
@endpush
@section('content')
@php
$stepsArray = session('stepsArray');
//print_r($stepsArray);
$stepsArrayMaintainer = session('stepsArrayMaintainer');
if(!empty($stepsArray['basicinfo'])){
$basic_info = $stepsArray['basicinfo'];
}
@endphp
<!-- ==== (BASIC INFO) TAB (CONTENT-1) START FROM HERE === -->
<div class="tab-pane fade show active" id="nav-basicinfo" role="tabpanel" aria-labelledby="nav-basicinfo-tab">
   <h2 class="page-header mb-0 mt-3 text-center">Standard Business Financing Application</h2>
   <div id="msform">
      <!-- (1st FILEDSET) ** How much do you need? ** === -->
      <fieldset class="fldset-steps" id="fieldset_financial_amount">
         <form class="stepsform">
            <label for="#">How much do you need?</label>
            <div class="input-group">
               <div class="input-group-prepend">
                  <span class="input-group-text">$</span>
               </div>
               <input value="{{@$basic_info['financial_amount']['financial_amount']}}" id="financial_amount" name="financial_amount" data-cy="financial_amount" type="text" separator="," placeholder="" class="form-control">
               <input type="hidden" name="step_name" value="financial_amount">
               <input type="hidden" name="tab_name" value="basic_info">
            </div>
            <div class="form-control-feedback text-danger" id="error_financial_amount" style="display: none">Field is required</div>
            <div class="footer">
               <div class="footer-form">
                  <button type="submit" class="btn btn-primary text-center action-button" value="Next">
                     <span>Next</span>
                  </button>
               </div>
               <div class="skip-btn d-none">
                  <a href="#">Fwd <i class="fas fa-arrow-right"></i></a>
               </div>
            </div>
         </form>
      </fieldset>

      <!-- (2nd FILEDSET) ** How's your credit score? ** === -->
      <fieldset class="fldset-steps" id="fieldset_credit_card_score">
         <form class="stepsform">
            <label>How's your credit score?</label>
            <div id="#" class="d-flex flex-wrap justify-content-center">
               <label class="selection-pill radiobox_option   @if(!empty($basic_info['credit_card_score']['credit_card_score'])  && $basic_info['credit_card_score']['credit_card_score'] =='499 or below') pill-active @endif">499 or below
                  <input type="radio" @if(!empty($basic_info['credit_card_score']['credit_card_score']) && $basic_info['credit_card_score']['credit_card_score']=='499 or below' ) checked="checked" @endif name="credit_card_score" value="499 or below">
                  <span class="checkmark"></span>
               </label>
               <label class="selection-pill radiobox_option @if(!empty($basic_info['credit_card_score']['credit_card_score'])  && $basic_info['credit_card_score']['credit_card_score'] =='500 - 599') pill-active @endif">500 - 599
                  <input @if(!empty($basic_info['credit_card_score']['credit_card_score']) && $basic_info['credit_card_score']['credit_card_score']=='500 - 599' ) checked="checked" @endif class="radio_submit" type="radio" name="credit_card_score" value="500 - 599">
                  <span class="checkmark"></span>
               </label>

               <label class="selection-pill radiobox_option @if(!empty($basic_info['credit_card_score']['credit_card_score'])  && $basic_info['credit_card_score']['credit_card_score'] =='600 - 649') pill-active @endif">600 - 649
                  <input @if(!empty($basic_info['credit_card_score']['credit_card_score']) && $basic_info['credit_card_score']['credit_card_score']=='600 - 649' ) checked="checked" @endif class="radio_submit" type="radio" name="credit_card_score" value="600 - 649">
                  <span class="checkmark"></span>
               </label>
               <label class="selection-pill radiobox_option @if(!empty($basic_info['credit_card_score']['credit_card_score'])  && $basic_info['credit_card_score']['credit_card_score'] =='650 - 679') pill-active @endif">650 - 679
                  <input @if(!empty($basic_info['credit_card_score']['credit_card_score']) && $basic_info['credit_card_score']['credit_card_score']=='650 - 679' ) checked="checked" @endif class="radio_submit" type="radio" name="credit_card_score" value="650 - 679">
                  <span class="checkmark"></span>
               </label> <label class="selection-pill radiobox_option @if(!empty($basic_info['credit_card_score']['credit_card_score'])  && $basic_info['credit_card_score']['credit_card_score'] =='680 - 719') pill-active @endif">680 - 719
                  <input @if(!empty($basic_info['credit_card_score']['credit_card_score']) && $basic_info['credit_card_score']['credit_card_score']=='680 - 719' ) checked="checked" @endif class="radio_submit" type="radio" name="credit_card_score" value="680 - 719">
                  <span class="checkmark"></span>
               </label> <label class="selection-pill radiobox_option @if(!empty($basic_info['credit_card_score']['credit_card_score'])  && $basic_info['credit_card_score']['credit_card_score'] =='720 or above') pill-active @endif">720 or above
                  <input @if(!empty($basic_info['credit_card_score']['credit_card_score']) && $basic_info['credit_card_score']['credit_card_score']=='720 or above' ) checked="checked" @endif class="radio_submit" type="radio" name="credit_card_score" value="720 or above">
                  <span class="checkmark"></span>

                  <input type="hidden" name="tab_name" value="basic_info">
                  <input type="hidden" name="step_name" value="credit_card_score">
            </div>
            <div class="form-control-feedback text-danger text-center" id="error_credit_card_score" style="display: none">Please select one option</div>
            <div class="footer">
               <div class="footer-form">
                  <button type="button" name="previous" data-value="financial_amount" class="previousBtn action-button btn-primary" value="Previous" style="float: left;" />
                  <span>Previous</span>
                  </button>
                  <button type="submit" name="next" class="btn action-button btn-primary" value="Next" style="float: right;" />
                  <span>Next</span>
                  </button>
               </div>
               <div class="skip-btn d-none">
                  <a href="#">Fwd <i class="fas fa-arrow-right"></i></a>
               </div>
            </div>
         </form>
      </fieldset>

      <!-- (3rd FILEDSET) ** When did you start your business? ** === -->
      <fieldset id="fieldset_business_start_date" class="fldset-steps">
         <form class="stepsform">
            <h4 class="text-center">When did you start your business?</h4>
            <label>Business Start Date</label>
            <div class="form-row">
               <div class="col">
                  <div class="ls-select">
                     <svg class="svg-inline--fa fa-angle-down" data-fa-pseudo-element=":before" aria-hidden="true" data-fa-processed="" data-prefix="far" data-icon="angle-down" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
                        <path fill="currentColor" d="M151.5 347.8L3.5 201c-4.7-4.7-4.7-12.3 0-17l19.8-19.8c4.7-4.7 12.3-4.7 17 0L160 282.7l119.7-118.5c4.7-4.7 12.3-4.7 17 0l19.8 19.8c4.7 4.7 4.7 12.3 0 17l-148 146.8c-4.7 4.7-12.3 4.7-17 0z">
                        </path>
                     </svg>
                     @php
                     $months = array( 'January','February','March','April','May','June','July ','August','September',
                     'October',
                     'November',
                     'December',
                     );
                     @endphp
                     <select id="" id="month" name="business_start_date_month" data-cy="" class="form-control no-select">
                        <option value="" selected="selected" disabled="disabled">Month</option>
                        @php
                        $i = 1;
                        @endphp
                        @foreach( $months as $month)
                        @php
                        if($i <10) $i='0' .$i @endphp <option value="{{$i}}" @if(isset($basic_info['business_start_date']['business_start_date_month']) && $basic_info['business_start_date']['business_start_date_month']==$i) selected @endif>{{$month}}</option>
                           @php
                           $i++;
                           @endphp
                           @endforeach
                     </select>
                  </div>
               </div>
               <div class="col">
                  <input type="number" min="1" max="31" id="day" name="business_start_date_day" placeholder="01" data-cy="" class="form-control" value="{{@$basic_info['business_start_date']['business_start_date_day']}}">
               </div>
               <div class="col">
                  <input type="number" id="year" name="business_start_date_year" data-cy="" placeholder="2021" class="form-control" value="{{@$basic_info['business_start_date']['business_start_date_year']}}">
               </div>

               <input type="hidden" name="tab_name" value="basic_info">
               <input type="hidden" name="step_name" value="business_start_date">
            </div>
            <div class="form-control-feedback text-danger" id="error_business_start_date" style="display: none">Field is required</div>
            <div class="form-control-feedback text-danger" id="date_greater" style="display: none">Please select a previous date.</div>
            <div class="footer">
               <div class="footer-form">
                  <button type="button" name="previous" data-value="credit_card_score" class="previousBtn action-button btn-primary" value="Previous" style="float: left;" />
                  <span>Previous</span>
                  </button>
                  <button type="submit" name="next" class="btn action-button btn-primary" value="Next" style="float: right;" />
                  <span>Next</span>
                  </button>
               </div>
               <div class="skip-btn d-none">
                  <a href="#">Fwd <i class="fas fa-arrow-right"></i></a>
               </div>
            </div>
         </form>
      </fieldset>

      <!-- (4th FILEDSET) ** What is your average monthly revenue over the last 3 months? ** === -->
      <fieldset id="fieldset_average_month_revenue" class="fldset-steps">
         <form class="stepsform">
            <h4 class="text-center">What is your average monthly revenue over the last 3 months?</h4>
            <div id="#" class="d-flex flex-wrap justify-content-center">
               <label class="selection-pill radiobox_option1  @if(!empty($basic_info['average_month_revenue']['average_month_revenue'])  && $basic_info['average_month_revenue']['average_month_revenue'] =='$0') pill-active @endif">$0
                  <input type="radio" type="radio" @if(!empty($basic_info['average_month_revenue']['average_month_revenue']) && $basic_info['average_month_revenue']['average_month_revenue']=='$0' ) checked="checked" @endif name="average_month_revenue" value="$0">
                  <span class="checkmark"></span>
               </label>
               <label class="selection-pill radiobox_option1  @if(!empty($basic_info['average_month_revenue']['average_month_revenue'])  && $basic_info['average_month_revenue']['average_month_revenue'] =='$1 - $4k') pill-active @endif">$200k+
                  <input type="radio" type="radio" @if(!empty($basic_info['average_month_revenue']['average_month_revenue']) && $basic_info['average_month_revenue']['average_month_revenue']=='$1 - $4k' ) checked="checked" @endif name="average_month_revenue" value="$1 - $4k">
                  <span class="checkmark"></span>
               </label>

               <label class="selection-pill  radiobox_option1 @if(!empty($basic_info['average_month_revenue']['average_month_revenue'])  && $basic_info['average_month_revenue']['average_month_revenue'] =='$5k - $7k') pill-active @endif">$5k - $7k
                  <input type="radio" type="radio" @if(!empty($basic_info['average_month_revenue']['average_month_revenue']) && $basic_info['average_month_revenue']['average_month_revenue']=='$5k - $7k' ) checked="checked" @endif name="average_month_revenue" value="$5k - $7k">
                  <span class="checkmark"></span>
               </label>
               <label class="selection-pill radiobox_option1 @if(!empty($basic_info['average_month_revenue']['average_month_revenue'])  && $basic_info['average_month_revenue']['average_month_revenue'] =='$8k - $14k') pill-active @endif">$8k - $14k
                  <input type="radio" type="radio" @if(!empty($basic_info['average_month_revenue']['average_month_revenue']) && $basic_info['average_month_revenue']['average_month_revenue']=='$8k - $14k' ) checked="checked" @endif name="average_month_revenue" value="$8k - $14k">
                  <span class="checkmark"></span>
               </label>
               <label class="selection-pill radiobox_option1 @if(!empty($basic_info['average_month_revenue']['average_month_revenue'])  && $basic_info['average_month_revenue']['average_month_revenue'] =='$15k - $19k') pill-active @endif">$15k - $19k
                  <input type="radio" type="radio" @if(!empty($basic_info['average_month_revenue']['average_month_revenue']) && $basic_info['average_month_revenue']['average_month_revenue']=='$15k - $19k' ) checked="checked" @endif name="average_month_revenue" value="$15k - $19k">
                  <span class="checkmark"></span>
               </label>
               <label class="selection-pill radiobox_option1 @if(!empty($basic_info['average_month_revenue']['average_month_revenue'])  && $basic_info['average_month_revenue']['average_month_revenue'] =='$20k - $49k') pill-active @endif">$20k - $49k
                  <input type="radio" type="radio" @if(!empty($basic_info['average_month_revenue']['average_month_revenue']) && $basic_info['average_month_revenue']['average_month_revenue']=='$20k - $49k' ) checked="checked" @endif name="average_month_revenue" value="$20k - $49k">
                  <span class="checkmark"></span>
               </label>
               <label class="selection-pill radiobox_option1 @if(!empty($basic_info['average_month_revenue']['average_month_revenue'])  && $basic_info['average_month_revenue']['average_month_revenue'] =='$50k - $79k') pill-active @endif">$50k - $79k
                  <input type="radio" type="radio" @if(!empty($basic_info['average_month_revenue']['average_month_revenue']) && $basic_info['average_month_revenue']['average_month_revenue']=='$50k - $79k' ) checked="checked" @endif name="average_month_revenue" value="$50k - $79k">
                  <span class="checkmark"></span>
               </label>
               <label class="selection-pill radiobox_option1 @if(!empty($basic_info['average_month_revenue']['average_month_revenue'])  && $basic_info['average_month_revenue']['average_month_revenue'] =='$80k - $199k') pill-active @endif">$80k - $199k
                  <input type="radio" type="radio" @if(!empty($basic_info['average_month_revenue']['average_month_revenue']) && $basic_info['average_month_revenue']['average_month_revenue']=='$80k - $199k' ) checked="checked" @endif name="average_month_revenue" value="$80k - $199k">
                  <span class="checkmark"></span>
               </label>
               <label class="selection-pill radiobox_option1 @if(!empty($basic_info['average_month_revenue']['average_month_revenue'])  && $basic_info['average_month_revenue']['average_month_revenue'] =='$200k+') pill-active @endif">$200k+
                  <input type="radio" type="radio" @if(!empty($basic_info['average_month_revenue']['average_month_revenue']) && $basic_info['average_month_revenue']['average_month_revenue']=='$200k+' ) checked="checked" @endif name="average_month_revenue" value="$200k+">
                  <span class="checkmark"></span>
               </label>
               <input type="hidden" name="tab_name" value="basic_info">
               <input type="hidden" name="step_name" value="average_month_revenue">
            </div>
            <div class="form-control-feedback text-danger text-center" id="error_average_month_revenue" style="display: none">Please select on option</div>
            <div class="footer">
               <div class="footer-form">
                  <button type="button" name="previous" data-value="business_start_date" class="previousBtn  action-button btn-primary" value="Previous" style="float: left;" />
                  <span>Previous</span>
                  </button>
                  <button type="submit" name="next" class="btn action-button btn-primary" value="Next" style="float: right;" />
                  <span>Next</span>
                  </button>
               </div>
               <div class="skip-btn d-none">
                  <a href="#">Fwd <i class="fas fa-arrow-right"></i></a>
               </div>
            </div>
         </form>
      </fieldset>

      <!-- (5th FILEDSET) ** >What percent of the business do you own? ** === -->
      <fieldset id="fieldset_business_percentage" class="fldset-steps">
         <form class="stepsform">
            <input type="hidden" name="tab_name" value="basic_info">
            <input type="hidden" name="step_name" value="business_percentage">
            <h4 class="text-center">What percent of the business do you own?</h4>
            <label for="#">What is your percentage of ownership (direct or indirect)?</label>
            <div id="" displayaltoptions="true">
               <div class="input-group">
                  <input value="{{@$basic_info['business_percentage']['business_percentage']}}" name="business_percentage" type="text" data-cy="" class="form-control">
                  <div class="input-group-append">
                     <span class="input-group-text">%</span>
                  </div>
               </div>
               <div class="form-control-feedback text-danger" id="error_business_percentage" style="display: none">Field is required</div>
            </div>
            <div class="footer">
               <div class="footer-form">
                  <button type="button" name="previous" data-value="average_month_revenue" class="previousBtn action-button btn-primary" value="Previous" style="float: left;" />
                  <span>Previous</span>
                  </button>
                  <button type="submit" name="next" class="btn action-button btn-primary" value="Next" style="float: right;" />
                  <span>Next</span>
                  </button>
               </div>
               <div class="skip-btn d-none">
                  <a href="#">Fwd <i class="fas fa-arrow-right"></i></a>
               </div>
            </div>
         </form>
      </fieldset>

      <!-- (6th FILEDSET) ** Let's learn a little about your business. Do you...? ** === -->
      <fieldset id="fieldset_business_description" class="fldset-steps">
         <form class="stepsform">
            <input type="hidden" name="tab_name" value="basic_info">
            <input type="hidden" name="step_name" value="business_description">
            <h4 class="text-center">Let's learn a little about your business. Do you...?</h4>
            <label>Select all that apply</label>
            <div class="d-flex flex-wrap justify-content-center">
               <label for="" tabindex="1" class="selection-pill m-1 m-sm-2 checkbox_option @if(!empty($basic_info['business_description']['business_description']) && in_array('Accept credit cards',$basic_info['business_description']['business_description']) ) pill-active @endif ">
                  <div class="box text-center checkbox_option_sub">
                     <input @if(!empty($basic_info['business_description']['business_description']) && in_array('Accept credit cards',$basic_info['business_description']['business_description']) ) checked @endif type="checkbox" value="Accept credit cards" name="business_description[]" class="selection-pill__check">
                     <div class="option-box__label">Accept credit cards</div>
                  </div>
               </label>
               <label for="" tabindex="2" class="selection-pill m-1 m-sm-2 checkbox_option @if(!empty($basic_info['business_description']['business_description']) && in_array('Have a prior bankruptcy',$basic_info['business_description']['business_description']) ) pill-active @endif ">
                  <div class="box text-center checkbox_option_sub">
                     <input @if(!empty($basic_info['business_description']['business_description']) && in_array('Have a prior bankruptcy',$basic_info['business_description']['business_description']) ) checked @endif type="checkbox" value="Have a prior bankruptcy" name="business_description[]" class="selection-pill__check">
                     <div class="option-box__label">Have a prior bankruptcy</div>
                  </div>
               </label>
               <label for="" tabindex="3" class="selection-pill m-1 m-sm-2 checkbox_option @if(!empty($basic_info['business_description']['business_description']) && in_array('Operate as a franchise',$basic_info['business_description']['business_description']) ) pill-active @endif ">
                  <div class="box text-center checkbox_option_sub">
                     <input @if(!empty($basic_info['business_description']['business_description']) && in_array('Operate as a franchise',$basic_info['business_description']['business_description']) ) checked @endif type="checkbox" value="Operate as a franchise" name="business_description[]" class="selection-pill__check">
                     <div class="option-box__label">Operate as a franchise
                     </div>
                  </div>
               </label>
               <label for="" tabindex="4" class="selection-pill m-1 m-sm-2 checkbox_option @if(!empty($basic_info['business_description']['business_description']) && in_array('Invoice customers',$basic_info['business_description']['business_description']) ) pill-active @endif ">
                  <div class="box text-center checkbox_option_sub">
                     <input @if(!empty($basic_info['business_description']['business_description']) && in_array('Invoice customers',$basic_info['business_description']['business_description']) ) checked @endif type="checkbox" value="Invoice customers" name="business_description[]" class="selection-pill__check">
                     <div class="option-box__label">Invoice customers</div>
                  </div>
               </label>
               <label for="" tabindex="5" class="selection-pill m-1 m-sm-2 checkbox_option @if(!empty($basic_info['business_description']['business_description']) && in_array('Operate as a non-profit',$basic_info['business_description']['business_description']) ) pill-active @endif ">
                  <div class="box text-center checkbox_option_sub">
                     <input @if(!empty($basic_info['business_description']['business_description']) && in_array('Operate as a non-profit',$basic_info['business_description']['business_description']) ) checked @endif type="checkbox" value="Operate as a non-profit" name="business_description[]" class="selection-pill__check">
                     <div class="option-box__label">Operate as a non-profit</div>
                  </div>
               </label>
               <label for="" tabindex="5" class="selection-pill m-1 m-sm-2 checkbox_option @if(!empty($basic_info['business_description']['business_description']) && in_array('Do business seasonally',$basic_info['business_description']['business_description']) ) pill-active @endif ">
                  <div class="box text-center checkbox_option_sub">
                     <input @if(!empty($basic_info['business_description']['business_description']) && in_array('Do business seasonally',$basic_info['business_description']['business_description']) ) checked @endif type="checkbox" value="Do business seasonally" name="business_description[]" class="selection-pill__check">
                     <div class="option-box__label">Do business seasonally</div>
                  </div>
               </label>
               <label for="" tabindex="5" class="selection-pill m-1 m-sm-2 checkbox_option @if(!empty($basic_info['business_description']['business_description']) && in_array('Use a tool for bookkeeping',$basic_info['business_description']['business_description']) ) pill-active @endif ">
                  <div class="box text-center checkbox_option_sub">
                     <input @if(!empty($basic_info['business_description']['business_description']) && in_array('Use a tool for bookkeeping',$basic_info['business_description']['business_description']) ) checked @endif type="checkbox" value="Use a tool for bookkeeping" name="business_description[]" class="selection-pill__check">
                     <div class="option-box__label">Use a tool for bookkeeping</div>
                  </div>
               </label>
            </div>
            <div class="form-control-feedback text-danger text-center" id="error_annul_personal_income" style="display: none">Please select on option</div>
            <div class="footer">
               <div class="footer-form">
                  <button type="button" name="previous" data-value="business_percentage" class="previousBtn action-button btn-primary" value="Previous" style="float: left;" />
                  <span>Previous</span>
                  </button>
                  <button type="submit" name="next" class="btn action-button btn-primary" value="Next" style="float: right;" />
                  <span>Next</span>
                  </button>
               </div>
               <div class="skip-btn d-none">
                  <a href="#">Fwd <i class="fas fa-arrow-right"></i></a>
               </div>
            </div>
         </form>
      </fieldset>


      <!-- (7th FILEDSET) ** What purpose would your loan serve? ** === -->
      <fieldset id="fieldset_loan_purpose" class="fldset-steps">
         <form class="stepsform">
            <input type="hidden" name="tab_name" value="basic_info">
            <input type="hidden" name="step_name" value="loan_purpose">
            <h4 class="text-center">What purpose would your loan serve?</h4>
            <label>Select all that apply</label>
            <div class="d-flex flex-wrap justify-content-center">
               <label for="" tabindex="1" class="selection-pill m-1 m-sm-2 checkbox_option @if(!empty($basic_info['loan_purpose']['loan_purpose']) && in_array('Expansion',$basic_info['loan_purpose']['loan_purpose']) ) pill-active @endif ">
                  <div class="box text-center checkbox_option_sub">
                     <input type="checkbox" @if(!empty($basic_info['loan_purpose']['loan_purpose']) && in_array('Expansion',$basic_info['loan_purpose']['loan_purpose']) ) checked @endif value="Expansion" name="loan_purpose[]" class="selection-pill__check">
                     <div class="option-box__label">Expansion</div>
                  </div>
               </label>
               <label for="" tabindex="2" class="selection-pill m-1 m-sm-2 checkbox_option @if(!empty($basic_info['loan_purpose']['loan_purpose']) && in_array('Working Capital',$basic_info['loan_purpose']['loan_purpose']) ) pill-active @endif">
                  <div class="box text-center checkbox_option_sub">
                     <input @if(!empty($basic_info['loan_purpose']['loan_purpose']) && in_array('Working Capital',$basic_info['loan_purpose']['loan_purpose']) ) checked @endif type="checkbox" value="Working Capital" name="loan_purpose[]" class="selection-pill__check">
                     <div class="option-box__label">Working Capital</div>
                  </div>
               </label>
               <label for="" tabindex="3" class="selection-pill m-1 m-sm-2 checkbox_option @if(!empty($basic_info['loan_purpose']['loan_purpose']) && in_array('Payroll',$basic_info['loan_purpose']['loan_purpose']) ) pill-active @endif">
                  <div class="box text-center checkbox_option_sub">
                     <input @if(!empty($basic_info['loan_purpose']['loan_purpose']) && in_array('Payroll',$basic_info['loan_purpose']['loan_purpose']) ) checked @endif type="checkbox" value="Payroll" name="loan_purpose[]" class="selection-pill__check">
                     <div class="option-box__label">Payroll</div>
                  </div>
               </label>
               <label for="" tabindex="4" class="selection-pill m-1 m-sm-2 checkbox_option @if(!empty($basic_info['loan_purpose']['loan_purpose']) && in_array('Purchase a Business',$basic_info['loan_purpose']['loan_purpose']) ) pill-active @endif">
                  <div class="box text-center checkbox_option_sub">
                     <input @if(!empty($basic_info['loan_purpose']['loan_purpose']) && in_array('Purchase a Business',$basic_info['loan_purpose']['loan_purpose']) ) checked @endif type="checkbox" value="Purchase a Business" name="loan_purpose[]" class="selection-pill__check">
                     <div class="option-box__label">Purchase a Business</div>
                  </div>
               </label>
               <label for="" tabindex="5" class="selection-pill m-1 m-sm-2 checkbox_option @if(!empty($basic_info['loan_purpose']['loan_purpose']) && in_array('Purchase a Franchise',$basic_info['loan_purpose']['loan_purpose']) ) pill-active @endif">
                  <div class="box text-center checkbox_option_sub">
                     <input @if(!empty($basic_info['loan_purpose']['loan_purpose']) && in_array('Purchase a Franchise',$basic_info['loan_purpose']['loan_purpose']) ) checked @endif type="checkbox" value="Purchase a Franchise" name="loan_purpose[]" class="selection-pill__check">
                     <div class="option-box__label">Purchase a Franchise</div>
                  </div>
               </label>
               <label for="" tabindex="6" class="selection-pill m-1 m-sm-2 checkbox_option @if(!empty($basic_info['loan_purpose']['loan_purpose']) && in_array('Equipment',$basic_info['loan_purpose']['loan_purpose']) ) pill-active @endif">
                  <div class="box text-center checkbox_option_sub">
                     <input @if(!empty($basic_info['loan_purpose']['loan_purpose']) && in_array('Equipment',$basic_info['loan_purpose']['loan_purpose']) ) checked @endif type="checkbox" value="Equipment" name="loan_purpose[]" class="selection-pill__check">
                     <div class="option-box__label">Equipment</div>
                  </div>
               </label>
               <label for="" tabindex="7" class="selection-pill m-1 m-sm-2 checkbox_option @if(!empty($basic_info['loan_purpose']['loan_purpose']) && in_array('Real Estate',$basic_info['loan_purpose']['loan_purpose']) ) pill-active @endif">
                  <div class="box text-center checkbox_option_sub">
                     <input @if(!empty($basic_info['loan_purpose']['loan_purpose']) && in_array('Real Estate',$basic_info['loan_purpose']['loan_purpose']) ) checked @endif type="checkbox" value="Real Estate" name="loan_purpose[]" class="selection-pill__check">
                     <div class="option-box__label">Real Estate</div>
                  </div>
               </label>
               <label for="" tabindex="8" class="selection-pill m-1 m-sm-2 checkbox_option @if(!empty($basic_info['loan_purpose']['loan_purpose']) && in_array('Buy Out a Partner',$basic_info['loan_purpose']['loan_purpose']) ) pill-active @endif">
                  <div class="box text-center checkbox_option_sub">
                     <input @if(!empty($basic_info['loan_purpose']['loan_purpose']) && in_array('Buy Out a Partner',$basic_info['loan_purpose']['loan_purpose']) ) checked @endif type="checkbox" value="Buy Out a Partner" name="loan_purpose[]" class="selection-pill__check">
                     <div class="option-box__label">Buy Out a Partner</div>
                  </div>
               </label>
               <label for="" tabindex="9" class="selection-pill m-1 m-sm-2  checkbox_option @if(!empty($basic_info['loan_purpose']['loan_purpose']) && in_array('Start a Business',$basic_info['loan_purpose']['loan_purpose']) ) pill-active @endif">
                  <div class="box text-center checkbox_option_sub">
                     <input @if(!empty($basic_info['loan_purpose']['loan_purpose']) && in_array('Start a Business',$basic_info['loan_purpose']['loan_purpose']) ) checked @endif type="checkbox" value="Start a Business" name="loan_purpose[]" class="selection-pill__check">
                     <div class="option-box__label">Start a Business</div>
                  </div>
               </label>
               <label for="" tabindex="10" class="selection-pill m-1 m-sm-2 checkbox_option @if(!empty($basic_info['loan_purpose']['loan_purpose']) && in_array('Finance Accounts Receivable',$basic_info['loan_purpose']['loan_purpose']) ) pill-active @endif">
                  <div class="box text-center checkbox_option_sub">
                     <input @if(!empty($basic_info['loan_purpose']['loan_purpose']) && in_array('Finance Accounts Receivable',$basic_info['loan_purpose']['loan_purpose']) ) checked @endif type="checkbox" value="Finance Accounts Receivable" name="loan_purpose[]" class="selection-pill__check">
                     <div class="option-box__label">Finance Accounts Receivable</div>
                  </div>
               </label>
               <label for="0" tabindex="11" class="selection-pill m-1 m-sm-2 checkbox_option @if(!empty($basic_info['loan_purpose']['loan_purpose']) && in_array('Other',$basic_info['loan_purpose']['loan_purpose']) ) pill-active @endif">
                  <div class="box text-center checkbox_option_sub">
                     <input @if(!empty($basic_info['loan_purpose']['loan_purpose']) && in_array('Other',$basic_info['loan_purpose']['loan_purpose']) ) checked @endif type="checkbox" value="Other" name="loan_purpose[]" class="selection-pill__check">
                     <div class="option-box__label">Other</div>
                  </div>
               </label>
            </div>
            <div class="form-control-feedback text-danger text-center" id="error_loan_purpose" style="display: none">Please select on option</div>
            <div class="footer">
               <div class="footer-form">
                  <button type="button" name="previous" data-value="business_description" class="previousBtn action-button btn-primary" value="Previous" style="float: left;" />
                  <span>Previous</span>
                  </button>
                  <button type="submit" name="next" class="btn action-button btn-primary" value="Next" style="float: right;" />
                  <span>Next</span>
                  </button>
               </div>
               <div class="skip-btn d-none">
                  <a href="#">Fwd <i class="fas fa-arrow-right"></i></a>
               </div>
            </div>
         </form>
      </fieldset>

      <!-- (8th FILEDSET) ** What's your annual personal income? ** === -->
      <fieldset id="fieldset_annul_personal_income" class="fldset-steps">
         <form class="stepsform">
            <input type="hidden" name="tab_name" value="basic_info">
            <input type="hidden" name="step_name" value="annul_personal_income">
            <h4 class="text-center">What's your annual personal income?</h4>
            <div class="biq-type-container">
               <div id="" class="d-flex flex-wrap justify-content-center">
                  <label class="selection-pill radiobox_option2 @if(!empty($basic_info['annul_personal_income']['annul_personal_income'])  && $basic_info['annul_personal_income']['annul_personal_income'] =='Below $30K') pill-active @endif">Below $30K
                     <input type="radio" @if(!empty($basic_info['annul_personal_income']['annul_personal_income']) && $basic_info['annul_personal_income']['annul_personal_income']=='Below $30K' ) checked="checked" @endif name="annul_personal_income" value="Below $30K">
                     <span class="checkmark"></span>
                  </label>
                  <label class="selection-pill radiobox_option2 @if(!empty($basic_info['annul_personal_income']['annul_personal_income'])  && $basic_info['annul_personal_income']['annul_personal_income'] =='$30K - $50K') pill-active @endif">$30K - $50K
                     <input type="radio" @if(!empty($basic_info['annul_personal_income']['annul_personal_income']) && $basic_info['annul_personal_income']['annul_personal_income']=='$30K - $50K' ) checked="checked" @endif name="annul_personal_income" value="$30K - $50K">
                     <span class="checkmark"></span>
                  </label>
                  <label class="selection-pill radiobox_option2 @if(!empty($basic_info['annul_personal_income']['annul_personal_income'])  && $basic_info['annul_personal_income']['annul_personal_income'] =='$50K - $75K') pill-active @endif">$50K - $75K
                     <input type="radio" @if(!empty($basic_info['annul_personal_income']['annul_personal_income']) && $basic_info['annul_personal_income']['annul_personal_income']=='$50K - $75K' ) checked="checked" @endif name="annul_personal_income" value="$50K - $75K">
                     <span class="checkmark"></span>
                  </label>
                  <label class="selection-pill radiobox_option2 @if(!empty($basic_info['annul_personal_income']['annul_personal_income'])  && $basic_info['annul_personal_income']['annul_personal_income'] =='Above $150K') pill-active @endif">Above $150K
                     <input type="radio" @if(!empty($basic_info['annul_personal_income']['annul_personal_income']) && $basic_info['annul_personal_income']['annul_personal_income']=='Above $150K' ) checked="checked" @endif name="annul_personal_income" value="Above $150K">
                     <span class="checkmark"></span>
                  </label>
               </div>
               <div class="form-control-feedback text-danger text-center" id="error_annul_personal_income" style="display: none">Please select on option</div>
            </div>
            <div class="footer">
               <div class="footer-form">
                  <button type="button" name="previous" data-value="loan_purpose" class="previousBtn action-button btn-primary" value="Previous" style="float: left;" />
                  <span>Previous</span>
                  </button>
                  <button type="submit" name="next" class="btn action-button btn-primary" value="Next" style="float: right;" />
                  <span>Next</span>
                  </button>
               </div>
               <div class="skip-btn d-none">
                  <a href="#">Fwd <i class="fas fa-arrow-right"></i></a>
               </div>
            </div>
         </form>
      </fieldset>

      <!-- (9th FILEDSET) ** What are your annual profits? **  NEW  ===  -->
      <fieldset class="fldset-steps" id="fieldset_annual_profit">
         <form class="stepsform">
            <label>What are your annual profits?</label>
            <div id="#" class="d-flex flex-wrap justify-content-center">

               <label class="selection-pill radiobox_option4  @if(!empty($basic_info['annual_profit']['annual_profit'])  && $basic_info['annual_profit']['annual_profit'] =='below $24k') pill-active @endif">below $24k
                  <input type="radio" @if(!empty($basic_info['annual_profit']['annual_profit']) && $basic_info['annual_profit']['annual_profit']=='below $24k' ) checked="checked" @endif name="annual_profit" value="below $24k">
                  <span class="checkmark"></span>
               </label>

               <label class="selection-pill radiobox_option4 @if(!empty($basic_info['annual_profit']['annual_profit'])  && $basic_info['annual_profit']['annual_profit'] =='$25k+') pill-active @endif">$25k+
                  <input @if(!empty($basic_info['annual_profit']['annual_profit']) && $basic_info['annual_profit']['annual_profit']=='$25k+' ) checked="checked" @endif class="radio_submit" type="radio" name="annual_profit" value="$25k+">
                  <span class="checkmark"></span>
               </label>

               <label class="selection-pill radiobox_option4 @if(!empty($basic_info['annual_profit']['annual_profit'])  && $basic_info['annual_profit']['annual_profit'] =='$48k+') pill-active @endif">$48k+
                  <input @if(!empty($basic_info['annual_profit']['annual_profit']) && $basic_info['annual_profit']['annual_profit']=='$48k+' ) checked="checked" @endif class="radio_submit" type="radio" name="annual_profit" value="$48k+">
                  <span class="checkmark"></span>
               </label>

               <label class="selection-pill radiobox_option4 @if(!empty($basic_info['annual_profit']['annual_profit'])  && $basic_info['annual_profit']['annual_profit'] =='#72k+') pill-active @endif">#72k+
                  <input @if(!empty($basic_info['annual_profit']['annual_profit']) && $basic_info['annual_profit']['annual_profit']=='#72k+' ) checked="checked" @endif class="radio_submit" type="radio" name="annual_profit" value="#72k+">
                  <span class="checkmark"></span>
               </label>

               <label class="selection-pill radiobox_option4 @if(!empty($basic_info['annual_profit']['annual_profit'])  && $basic_info['annual_profit']['annual_profit'] =='$96k+') pill-active @endif">$96k+
                  <input @if(!empty($basic_info['annual_profit']['annual_profit']) && $basic_info['annual_profit']['annual_profit']=='$96k+' ) checked="checked" @endif class="radio_submit" type="radio" name="annual_profit" value="$96k+">
                  <span class="checkmark"></span>
               </label>

               <label class="selection-pill radiobox_option4 @if(!empty($basic_info['annual_profit']['annual_profit'])  && $basic_info['annual_profit']['annual_profit'] =='$120k+') pill-active @endif">$120k+
                  <input @if(!empty($basic_info['annual_profit']['annual_profit']) && $basic_info['annual_profit']['annual_profit']=='$120k+' ) checked="checked" @endif class="radio_submit" type="radio" name="annual_profit" value="$120k+">
                  <span class="checkmark"></span>
               </label>

               <label class="selection-pill radiobox_option4 @if(!empty($basic_info['annual_profit']['annual_profit'])  && $basic_info['annual_profit']['annual_profit'] =='$180k+') pill-active @endif">$180k+
                  <input @if(!empty($basic_info['annual_profit']['annual_profit']) && $basic_info['annual_profit']['annual_profit']=='$180k+' ) checked="checked" @endif class="radio_submit" type="radio" name="annual_profit" value="$180k+">
                  <span class="checkmark"></span>
               </label>

               <label class="selection-pill radiobox_option4 @if(!empty($basic_info['annual_profit']['annual_profit'])  && $basic_info['annual_profit']['annual_profit'] =='$240k+') pill-active @endif">$240k+
                  <input @if(!empty($basic_info['annual_profit']['annual_profit']) && $basic_info['annual_profit']['annual_profit']=='$240k+' ) checked="checked" @endif class="radio_submit" type="radio" name="annual_profit" value="$240k+">
                  <span class="checkmark"></span>
               </label>

               <input type="hidden" name="tab_name" value="basic_info">
               <input type="hidden" name="step_name" value="annual_profit">

            </div>
            <div class="form-control-feedback text-danger text-center" id="error_annual_profit" style="display: none">Please select one option</div>
            <div class="footer">
               <div class="footer-form">
                  <button type="button" name="previous" data-value="annul_personal_income" class="previousBtn action-button btn-primary" value="Previous" style="float: left;" />
                  <span>Previous</span>
                  </button>
                  <button type="submit" name="next" class="btn action-button btn-primary" value="Next" style="float: right;" />
                  <span>Next</span>
                  </button>
               </div>
               <div class="skip-btn d-none">
                  <a href="#">Fwd <i class="fas fa-arrow-right"></i></a>
               </div>
            </div>
         </form>
      </fieldset>

      <!-- (10th FILEDSET) ** What type of entity is your business? ** NEW   ===  -->
      <fieldset class="fldset-steps" id="fieldset_business_entity">
         <form class="stepsform">

               <input type="hidden" name="tab_name" value="basic_info">
               <input type="hidden" name="step_name" value="business_entity">
            <label>What type of entity is your business?</label>
            <div id="#" class="d-flex flex-wrap justify-content-center">

               <label class="selection-pill radiobox_option5 @if(!empty($basic_info['business_entity']['business_entity'])  && $basic_info['business_entity']['business_entity'] =='LLC') pill-active @endif">LLC
                  <input type="radio" @if(!empty($basic_info['business_entity']['business_entity']) && $basic_info['business_entity']['business_entity']=='LLC' ) checked="checked" @endif name="business_entity" value="LLC">
                  <span class="checkmark"></span>
               </label>

               <label class="selection-pill radiobox_option5 @if(!empty($basic_info['business_entity']['business_entity'])  && $basic_info['business_entity']['business_entity'] =='Corporation') pill-active @endif">Corporation
                  <input @if(!empty($basic_info['business_entity']['business_entity']) && $basic_info['business_entity']['business_entity']=='Corporation' ) checked="checked" @endif class="radio_submit" type="radio" name="business_entity" value="Corporation">
                  <span class="checkmark"></span>
               </label>

               <label class="selection-pill radiobox_option5 @if(!empty($basic_info['business_entity']['business_entity'])  && $basic_info['business_entity']['business_entity'] =='Sole Proprietor') pill-active @endif">Sole Proprietor
                  <input @if(!empty($basic_info['business_entity']['business_entity']) && $basic_info['business_entity']['business_entity']=='Sole Proprietor' ) checked="checked" @endif class="radio_submit" type="radio" name="business_entity" value="Sole Proprietor">
                  <span class="checkmark"></span>
               </label>

               <label class="selection-pill radiobox_option5 @if(!empty($basic_info['business_entity']['business_entity'])  && $basic_info['business_entity']['business_entity'] =='Legal Partnership') pill-active @endif">Legal Partnership
                  <input @if(!empty($basic_info['business_entity']['business_entity']) && $basic_info['business_entity']['business_entity']=='Legal Partnership' ) checked="checked" @endif class="radio_submit" type="radio" name="business_entity" value="Legal Partnership">
                  <span class="checkmark"></span>
               </label>
            </div>
            <div class="form-control-feedback text-danger text-center" id="error_business_entity" style="display: none">Please select one option</div>
            <div class="footer">
               <div class="footer-form">
                  <button type="button" name="previous" data-value="annual_profit" class="previousBtn action-button btn-primary" value="Previous" style="float: left;" />
                  <span>Previous</span>
                  </button>
                  <button type="submit" name="next" class="btn action-button btn-primary" value="Next" style="float: right;" />
                  <span>Next</span>
                  </button>
               </div>
               <div class="skip-btn d-none">
                  <a href="#">Fwd <i class="fas fa-arrow-right"></i></a>
               </div>
            </div>
         </form>
      </fieldset>

 <!-- (11th FILEDSET) ** Which state is your business in? ** === -->

      <fieldset id="fieldset_business_state" class="fldset-steps">
         <form class="stepsform">
            <input type="hidden" name="tab_name" value="basic_info">
            <input type="hidden" name="step_name" value="business_state">
            <h4 class="text-center">Which state is your business in?</h4>
            <label for="#">Business State</label>
            @php
            $state_list = array('AL'=>"Alabama",
            'AK'=>"Alaska",
            'AZ'=>"Arizona",
            'AR'=>"Arkansas",
            'CA'=>"California",
            'CO'=>"Colorado",
            'CT'=>"Connecticut",
            'DE'=>"Delaware",
            'DC'=>"District Of Columbia",
            'FL'=>"Florida",
            'GA'=>"Georgia",
            'HI'=>"Hawaii",
            'ID'=>"Idaho",
            'IL'=>"Illinois",
            'IN'=>"Indiana",
            'IA'=>"Iowa",
            'KS'=>"Kansas",
            'KY'=>"Kentucky",
            'LA'=>"Louisiana",
            'ME'=>"Maine",
            'MD'=>"Maryland",
            'MA'=>"Massachusetts",
            'MI'=>"Michigan",
            'MN'=>"Minnesota",
            'MS'=>"Mississippi",
            'MO'=>"Missouri",
            'MT'=>"Montana",
            'NE'=>"Nebraska",
            'NV'=>"Nevada",
            'NH'=>"New Hampshire",
            'NJ'=>"New Jersey",
            'NM'=>"New Mexico",
            'NY'=>"New York",
            'NC'=>"North Carolina",
            'ND'=>"North Dakota",
            'OH'=>"Ohio",
            'OK'=>"Oklahoma",
            'OR'=>"Oregon",
            'PA'=>"Pennsylvania",
            'RI'=>"Rhode Island",
            'SC'=>"South Carolina",
            'SD'=>"South Dakota",
            'TN'=>"Tennessee",
            'TX'=>"Texas",
            'UT'=>"Utah",
            'VT'=>"Vermont",
            'VA'=>"Virginia",
            'WA'=>"Washington",
            'WV'=>"West Virginia",
            'WI'=>"Wisconsin",
            'WY'=>"Wyoming");
            @endphp
            <select name="business_state" id="" data-cy="" placeholder="State" class="form-control">
               <option value="">Select a state</option>
               @foreach($state_list as $keys=> $state_names)
               <option value="{{$keys}}" @if(!empty($basic_info['business_state']['business_state']) && $basic_info['business_state']['business_state']==$keys) selected @endif>{{$state_names}}</option>
               @endforeach
            </select>
            <div class="form-control-feedback text-danger" id="error_business_state" style="display: none">Please select on option</div>
            <div class="footer">
               <div class="footer-form">
                  <button type="button" name="previous" data-value="business_entity" class="previousBtn action-button btn-primary" value="Previous" style="float: left;" />
                  <span>Previous</span>
                  </button>
                  <button type="submit" name="next" class="btn action-button btn-primary" value="Next" style="float: right;" />
                  <span>Next</span>
                  </button>
               </div>
               <div class="skip-btn d-none">
                  <a href="#">Fwd <i class="fas fa-arrow-right"></i></a>
               </div>
            </div>
         </form>

      </fieldset>

      <!-- (12th FILEDSET) ** Which best describes your business? ** === -->
      <fieldset id="fieldset_business_information" class="fldset-steps">
         <form class="stepsform">
            <input type="hidden" name="tab_name" value="basic_info">
            <input type="hidden" name="step_name" value="business_information">
            <h4 class="text-center">Which best describes your business?</h4>
            <label for="#">Which best describes your business?</label>
            <select name="business_information" id="business_information" data-cy="" class="form-control">
               <option value="">Select an option</option>
               <option @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='agricultureForestry' ) selected @endif value="agricultureForestry">Agriculture, Forestry, Fishing and Hunting</option>
               <option @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='artsEntertainment' ) selected @endif value="artsEntertainment">Arts, Entertainment, and Recreation</option>
               <option @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='adultEntertainment' ) selected @endif value="adultEntertainment">Adult Entertainment</option>
               <option value="gambling" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='gambling' ) selected @endif>Gambling</option>
               <option value="automotive" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='automotive' ) selected @endif>Automobile Dealers &amp; Parts</option>
               <option value="construction" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='construction' ) selected @endif>Construction</option>
               <option value="education" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='education' ) selected @endif>Education</option>
               <option value="finance" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='finance' ) selected @endif>Finance and Insurance</option>
               <option value="healthcare" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='healthcare' ) selected @endif>Healthcare</option>
               <option value="socialAssistance" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='socialAssistance' ) selected @endif>Social Assistance</option>
               <option value="informationMedia" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='informationMedia' ) selected @endif>IT, Media, or Publishing</option>
               <option value="legalServices" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='legalServices' ) selected @endif>Legal Services</option>
               <option value="mining" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='mining' ) selected @endif>Mining (except Oil and Gas)</option>
               <option value="oilGas" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='oilGas' ) selected @endif>Oil and Gas Extraction</option>
               <option value="manufacturing" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='manufacturing' ) selected @endif>Manufacturing</option>
               <option value="governmentPublic" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='governmentPublic' ) selected @endif>Political, Governmental, or Public Organizations</option>
               <option value="realEstate" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='realEstate' ) selected @endif>Real Estate</option>
               <option value="religiousOrganizations" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='religiousOrganizations' ) selected @endif>Religious Organizations</option>
               <option value="restaurants" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='restaurants' ) selected @endif>Restaurants and Food Services</option>
               <option value="retail" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='retail' ) selected @endif>Retail Stores</option>
               <option value="firearms" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='firearms' ) selected @endif>Firearm Sales</option>
               <option value="gasStations" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='gasStations' ) selected @endif>Gas Stations</option>
               <option value="transportation" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='transportation' ) selected @endif>Transportation and Warehousing</option>
               <option value="freightTrucking" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='freightTrucking' ) selected @endif>Freight Trucking</option>
               <option value="travelAgencies" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='travelAgencies' ) selected @endif>Travel Agencies</option>
               <option value="utilities" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='utilities' ) selected @endif>Utilities</option>
               <option value="wholesale" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='wholesale' ) selected @endif>Wholesale Trade</option>
               <option value="other" @if(!empty($basic_info['business_information']['business_information']) && $basic_info['business_information']['business_information']=='other' ) selected @endif>All Other</option>
            </select>
            <div class="form-control-feedback text-danger" id="error_business_information" style="display: none">Please select on option</div>
            <div class="footer">
               <div class="footer-form">
                  <button type="button" name="previous" data-value="business_state" class="previousBtn action-button btn-primary" value="Previous" style="float: left;" />
                  <span>Previous</span>
                  </button>
                  <button type="submit" name="next" class="btn action-button btn-primary" value="Next" style="float: right;" />
                  <span>Next</span>
                  </button>
               </div>
               <div class="skip-btn d-none">
                  <a href="#">Fwd <i class="fas fa-arrow-right"></i></a>
               </div>
            </div>
         </form>
      </fieldset>

      <!-- (13th FILEDSET) ** Which is most important to you? ** === -->
      <fieldset class="fldset-steps" id="fieldset_whatismore_important_toyou">
         <form class="stepsform">
            <label>Which is most important to you?</label>
            <div id="#" class="d-flex flex-wrap justify-content-center">
               <label class="selection-pill radiobox_option3 @if(!empty($basic_info['whatismore_important_toyou']['whatismore_important_toyou'])  && $basic_info['whatismore_important_toyou']['whatismore_important_toyou'] =='Amount of Funds') pill-active @endif">Amount of Funds
                  <input type="radio" @if(!empty($basic_info['whatismore_important_toyou']['whatismore_important_toyou']) && $basic_info['whatismore_important_toyou']['whatismore_important_toyou']=='Amount of Funds' ) checked="checked" @endif name="whatismore_important_toyou" value="Amount of Funds">
                  <span class="checkmark"></span>
               </label>
               <label class="selection-pill radiobox_option3 @if(!empty($basic_info['whatismore_important_toyou']['whatismore_important_toyou'])  && $basic_info['whatismore_important_toyou']['whatismore_important_toyou'] =='Speed of Funds') pill-active @endif">Speed of Funds
                  <input type="radio" @if(!empty($basic_info['whatismore_important_toyou']['whatismore_important_toyou']) && $basic_info['whatismore_important_toyou']['whatismore_important_toyou']=='Speed of Funds' ) checked="checked" @endif name="whatismore_important_toyou" value="Speed of Funds">
                  <span class="checkmark"></span>
               </label>
               <label class="selection-pill radiobox_option3 @if(!empty($basic_info['whatismore_important_toyou']['whatismore_important_toyou'])  && $basic_info['whatismore_important_toyou']['whatismore_important_toyou'] =='Cost of Funds') pill-active @endif">Cost of Funds
                  <input type="radio" @if(!empty($basic_info['whatismore_important_toyou']['whatismore_important_toyou']) && $basic_info['whatismore_important_toyou']['whatismore_important_toyou']=='Cost of Funds' ) checked="checked" @endif name="whatismore_important_toyou" value="Cost of Funds">
                  <span class="checkmark"></span>
               </label>
               <input type="hidden" name="tab_name" value="basic_info">
               <input type="hidden" name="step_name" value="whatismore_important_toyou">
            </div>
            <div class="form-control-feedback text-danger text-center" id="error_whatismore_important_toyou" style="display: none">Please select one option</div>
            <div class="footer">
               <div class="footer-form">
                  <button type="button" name="previous" data-value="business_information" class="previousBtn action-button btn-primary" value="Previous" style="float: left;" />
                  <span>Previous</span>
                  </button>
                  <button type="submit" name="next" class="btn action-button btn-primary" value="Next" style="float: right;" />
                  <span>Next</span>
                  </button>
               </div>
               <div class="skip-btn d-none">
                  <a href="#">Fwd <i class="fas fa-arrow-right"></i></a>
               </div>
            </div>
         </form>
      </fieldset>

   </div>
</div>
@endsection

@push('scripts')
<script>
   $(document).ready(function() {
      $(".radiobox_option").click(function(e) {
         $(".radiobox_option").removeClass('pill-active');
         e.preventDefault();
         var chk = $(this).find("input[type='radio']");
         chk.prop("checked", true);
         $(this).addClass('pill-active');
      });

      $(".radiobox_option1").click(function(e) {
         $(".radiobox_option1").removeClass('pill-active');
         e.preventDefault();
         var chk = $(this).find("input[type='radio']");
         chk.prop("checked", true);
         $(this).addClass('pill-active');
      });

      $(".radiobox_option2").click(function(e) {
         $(".radiobox_option2").removeClass('pill-active');
         e.preventDefault();
         var chk = $(this).find("input[type='radio']");
         chk.prop("checked", true);
         $(this).addClass('pill-active');
      });
      $(".radiobox_option3").click(function(e) {
         $(".radiobox_option3").removeClass('pill-active');
         e.preventDefault();
         var chk = $(this).find("input[type='radio']");
         chk.prop("checked", true);
         $(this).addClass('pill-active');
      });

       $(".radiobox_option4").click(function(e) {
         $(".radiobox_option4").removeClass('pill-active');
         e.preventDefault();
         var chk = $(this).find("input[type='radio']");
         chk.prop("checked", true);
         $(this).addClass('pill-active');
      });

        $(".radiobox_option5").click(function(e) {
         $(".radiobox_option5").removeClass('pill-active');
         e.preventDefault();
         var chk = $(this).find("input[type='radio']");
         chk.prop("checked", true);
         $(this).addClass('pill-active');
      });

      $.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
      });
      $(".previousBtn").click(function(event) {
         event.preventDefault();
         var fieldName = $(this).data('value');
         $.ajax({
            url: "{{ url('/return-steps') }}",
            method: 'POST',
            data: 'step_name=' + fieldName,
            dataType: 'json',
            success: function(result) {
               console.log(result);
               $('.fldset-steps').hide();
               if (result.nextStep != "") {
                  $('#fieldset_' + result.nextStep).hide();
               }
               if (result.preStep != "") {
                  $('#fieldset_' + result.preStep).show();
               }
            }
         });

      });

      $(".stepsform").submit(function(event) {
         event.preventDefault(); //prevent default action 
         var post_url = $(this).attr("action"); //get form action url
         var request_method = $(this).attr("method"); //get form GET/POST method
         var form_data = new FormData(this); //Creates new FormData object
         $.ajaxSetup({
            headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
         });

         $("#date_greater").hide();
         $.ajax({
            url: "{{ url('/basicinfo') }}",
            method: 'POST',
            data: form_data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 800000,
            success: function(result) {
               if (result.errors == true) {
                  if (result.errorsMsg == 'date_greater') {
                     $("#date_greater").show();
                  } else {
                     $("#error_" + result.preStep).show();
                     $("#" + result.preStep).addClass('is-invalid');
                  }
               } else {
                  $('.fldset-steps').hide();
                  $('#fieldset_' + result.nextStep).show();
                  $('#fieldset_' + result.nextStep).find(".previousBtn").attr('data-value', result.preStep);
               }

               if (typeof result.nextTabURL != 'undefined' && result.nextTabURL != '') {
                  window.location.replace(result.nextTabURL);
               }


               //fieldset_financial_amount
            }
         });

      });

      ////////////////////////////////////////
      var today = new Date();
      var dd = today.getDate();
      var mm = today.getMonth() + 1; //January is 0!
      var yyyy = today.getFullYear();
      if (dd < 10) {
         dd = '0' + dd
      }
      if (mm < 10) {
         mm = '0' + mm
      }
      today = yyyy + '-' + mm + '-' + dd;
      // document.getElementById("business_start_date").setAttribute("max", today);

      function CommaFormatted(amount) {
         var delimiter = ","; // replace comma if desired 
         var d = amount;
         var i = parseInt(amount);
         if (isNaN(i)) {
            return '';
         }
         var minus = '';
         if (i < 0) {
            minus = '-';
         }
         i = Math.abs(i);
         var n = new String(i);
         var a = [];
         while (n.length > 3) {
            var nn = n.substr(n.length - 3);
            a.unshift(nn);
            n = n.substr(0, n.length - 3);
         }
         if (n.length > 0) {
            a.unshift(n);
         }
         n = a.join(delimiter);
         if (d.length < 1) {
            amount = n;
         } else {
            amount = n
         }
         amount = minus + amount;
         return amount;
      }
      $('#financial_amount').on('keyup', function() {
         var number = removeCommas($(this).val());
         var formattedNumber = CommaFormatted(number);
         $("#financial_amount").val(formattedNumber);
      });


      function removeCommas(str) {
         while (str.search(",") >= 0) {
            str = (str + "").replace(',', '');
         }
         return str;
      }



   });
</script>
@endpush
@extends('layouts.master')
@push('styles')

<!-- CSS -->
<link rel="stylesheet" type="text/css" href="{{asset('public/dist/dropzone.css')}}">
@endpush
@section('content')
<!-- ==== (DOCUMENTS) TAB (CONTENT-4) START FROM HERE === -->
<div class="tab-pane fade active show" id="nav-documents" role="tabpanel" aria-labelledby="nav-documents-tab">
  <h2 class="page-header mb-0 mt-3 text-center">Standard Business Financing Application</h2>
  <div class="documents-info" id="msform">
    @csrf
    <!-- (DOCUMENTS) FIELD-SET-1 START FROM HERE -->{{--
      <fieldset>
        <section class="bank-statements institution-search-page">
          <h3 class="pb-3 text-center">6 Months of Bank Statements</h3>
          <h3 class="pb-3 text-center">Select Your Bank</h3>
          <h5 class="option text-center">Option 1</h5>
          <p class="text-center help-text">Connect to your online bank account for a<b>fast</b> and
            <b>safe</b> way to send your revenue history.</p>
          <div class="text-center pt-3">
            <button id="finicityConnect" class="btn btn-action btn-primary">Connect</button>
          </div>
          <hr>
          <h5 class="option text-center mt-4">Option 2</h5>
          <p class="text-center" style="width: 60%; margin: 0px auto 25px;">Upload PDF versions of your bank
            statements from each of your business accounts.</p>
        </section>
        <button type="button" name="next" class="btn btn-primary text-center next action-button" value="Next">
          <span>Upload Manually</span>
        </button>

      </fieldset> --}}

    <!-- (DOCUMENTS) FIELD-SET-2 START FROM HERE -->
    <fieldset>
      <section class="row">
        <div class="col-md-12">
          <div class="question-text col-12">
            <h3>Upload statements</h3>
          </div>
          <h6 style="text-align: left;"><strong>Requirements:</strong></h6>
          <ul style="text-align: left; padding: 0px;">
            <li>Use official bank statement (No CSV or spreadsheet).</li>
            <li>Use .PDF file format.</li>
            <li>Include every page in the statement (even blank pages).</li>
          </ul>
        </div>
        <div class="col-md-12">

          <!-- UPLOAD FILE FOR MONTH OF NOVEMBER -->
          <div class="upload-box-container">
            <div class="upload-box">
              <div class="upload-content wo-help-text wo-file-text">
                <div class="upload-title">
                  <span data-cy="fileUploadLabel" class="upload-label">
                    <strong>Drivers License Front</strong>
                  </span>
                  <p class="upload-text">
                    <span class="upload-browse">
                      <form action="{{route('upload.file')}}" class='dropzone' id='dropzone1'>

                      </form>
                    </span>
                  </p>
                </div>
              </div>
            </div>
            <div class="errored"></div>
          </div>

          <!-- UPLOAD FILE FOR MONTH OF NOVEMBER -->
          <div class="upload-box-container">
            <div class="upload-box">
              <div class="upload-content wo-help-text wo-file-text">
                <div class="upload-title">
                  <span data-cy="fileUploadLabel" class="upload-label">
                    <strong>Drivers License Back</strong>
                  </span>
                  <p class="upload-text">
                    <span class="upload-browse">
                      <form action="{{route('upload.file')}}" class='dropzone' id='dropzone2'>
                      </form>
                    </span>
                  </p>
                </div>
              </div>
            </div>
            <div class="errored"></div>
          </div>

          <!-- UPLOAD FILE FOR MONTH OF NOVEMBER -->
          <div class="upload-box-container">
            <div class="upload-box">
              <div class="upload-content wo-help-text wo-file-text">
                <div class="upload-title">
                  <span data-cy="fileUploadLabel" class="upload-label">
                    <strong>Voided Check</strong>
                  </span>
                  <p class="upload-text">
                    <span class="upload-browse">
                      <form action="{{route('upload.file')}}" class='dropzone' id='dropzone3'>
                      </form>
                    </span>
                  </p>
                </div>
              </div>
            </div>
            <div class="errored"></div>
          </div>

          <!-- UPLOAD FILE FOR MONTH OF December -->
          <div class="upload-box-container">
            <div class="upload-box">
              <div class="upload-content wo-help-text wo-file-text">
                <div class="upload-title">
                  <span data-cy="fileUploadLabel" class="upload-label">
                    <strong>Schedule C</strong>
                  </span>
                  <p class="upload-text">
                    <span class="upload-browse">
                      <form action="{{route('upload.file')}}" class='dropzone' id='dropzone4'>
                      </form>
                    </span>
                  </p>
                </div>
              </div>
            </div>
            <div class="errored"></div>
          </div>

          <!-- UPLOAD FILE FOR MONTH OF January -->
          <div class="upload-box-container">
            <div class="upload-box">
              <div class="upload-content wo-help-text wo-file-text">
                <div class="upload-title">
                  <span data-cy="fileUploadLabel" class="upload-label">
                    <strong>Bank Statement Feb. 2020</strong>
                  </span>
                  <p class="upload-text">
                    <span class="upload-browse">
                      <form action="{{route('upload.file')}}" class='dropzone' id='dropzone5'>
                      </form>
                    </span>
                  </p>
                </div>
              </div>
            </div>
            <div class="errored"></div>
          </div>

          <!-- UPLOAD FILE FOR MONTH OF Febrauary -->
          <div class="upload-box-container">
            <div class="upload-box">
              <div class="upload-content wo-help-text wo-file-text">
                <div class="upload-title">
                  <span data-cy="fileUploadLabel" class="upload-label">
                    <strong>Business Formation docs.</strong>
                  </span>
                  <p class="upload-text">
                    <span class="upload-browse">
                      <form action="{{route('upload.file')}}" class='dropzone' id='dropzone6'></form>
                    </span>
                  </p>
                </div>
              </div>
            </div>
            <div class="errored"></div>
          </div>

        </div>
      </section>
      <div class="footer-form">
      <button onclick="window.location.href='{{route('finalform')}}'" type="button" name="next" class="btn btn-primary text-center action-button" value="Next">
          <span>Upload</span>
        </button>
      </div>
    </fieldset>

  </div>
</div>

@endsection
@push('scripts')
<!-- JS -->
<script src="{{asset('public/dist/min/dropzone.min.js')}}" type="text/javascript"></script>

<!-- Script -->
<script>
  var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");

  Dropzone.autoDiscover = false;
  var myDropzone = new Dropzone("#dropzone1", {
    maxFilesize: 3, // 3 mb
    acceptedFiles: ".jpeg,.jpg,.png,.pdf",
    maxFiles: 1,
  });
  myDropzone.on("sending", function(file, xhr, formData) {
    formData.append("_token", CSRF_TOKEN);
  });

  var myDropzone = new Dropzone("#dropzone2", {
    maxFilesize: 3, // 3 mb
    acceptedFiles: ".jpeg,.jpg,.png,.pdf",
    maxFiles: 1,
  });
  myDropzone.on("sending", function(file, xhr, formData) {
    formData.append("_token", CSRF_TOKEN);
  });

  var myDropzone = new Dropzone("#dropzone3", {
    maxFilesize: 3, // 3 mb
    acceptedFiles: ".jpeg,.jpg,.png,.pdf",
    maxFiles: 1,
  });
  myDropzone.on("sending", function(file, xhr, formData) {
    formData.append("_token", CSRF_TOKEN);
  });

  var myDropzone = new Dropzone("#dropzone4", {
    maxFilesize: 3, // 3 mb
    acceptedFiles: ".jpeg,.jpg,.png,.pdf",
    maxFiles: 1,
  });
  myDropzone.on("sending", function(file, xhr, formData) {
    formData.append("_token", CSRF_TOKEN);
  });

  var myDropzone = new Dropzone("#dropzone5", {
    maxFilesize: 3, // 3 mb
    acceptedFiles: ".jpeg,.jpg,.png,.pdf",
    maxFiles: 1,
  });
  myDropzone.on("sending", function(file, xhr, formData) {
    formData.append("_token", CSRF_TOKEN);
  });

  var myDropzone = new Dropzone("#dropzone6", {
    maxFilesize: 3, // 3 mb
    acceptedFiles: ".jpeg,.jpg,.png,.pdf",
    maxFiles: 1,
  });
  myDropzone.on("sending", function(file, xhr, formData) {
    formData.append("_token", CSRF_TOKEN);
  });
</script>

@endpush
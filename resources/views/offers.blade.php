@extends('layouts.userdashboard')
@push('styles')
@endpush
@section('content')
<div class="portalContent">
    <div class="infoNav row">
        <div class="titleNav text-left col col-lg-8 d-flex">
            <div class="titleDetail d-flex align-items-start flex-column justify-content-between">
                <div class="titlePhotoContainer mb-auto">
                    <div class="backgroundShadow">
                        <div class="pageTitle">
                            <h2>Paycheck Protection Program Offers</h2>
                        </div>
                    </div>
                    <div class="optionalPhoto d-block d-lg-none text-right"></div>
                </div>
                <div class="pageDetail">
                    <div>
                        <h4 class="large-text">YOUR APPLICATIONS</h4>
                        <ul class="nav">
                            <li class="nav-container">
                                <a class="active clickable-nav selected-nav">Second Draw</a>
                            </li>
                             <li class="nav-container">
                             <a class="active clickable-nav selected-nav">Loan Number: {{@$basicInfo->sba_loan_number}}</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="managerCta text-right d-none d-sm-block col col-lg-4">
            <span>
                <img src="{{ asset('public/img/offers-ppp-pathway.png')}}" alt="" class="offer-image">
            </span>
        </div>
    </div>
    <div></div>
    <div class="row stepsContent mb-3 mt-5">
        <div class="deals-box-container container mb-3 expand-max" currentpppdraw="Second Draw">
            <section class="flat-card mx-auto portal-shadow border-0">
                <section class="deals-box d-flex flex-column">
                    <div class="section-head p-4 d-flex justify-content-between">
                        <div class="section-head-title">LOAN OFFERS</div>
                        <div class="d-flex justify-content-between button-options">
                            <div class="d-flex">
                                <div class="pointer box-top-button d-flex align-items-center">
                                    <img src="{{asset('public/img/add_material.png')}}" alt="" class="pr-2 add-icon">
                                <span class="loan-box-button"><a href="{{route('ppp.documents')}}">Add Documents</a></span>
                                </div>
                                <div class="pointer box-top-button d-flex align-items-center">
                                    <img src="{{ asset('public/img/edit_material.png')}}" alt="" class="pr-2 add-icon">
                                    <span class="loan-box-button"><a href="{{route('edit.application')}}">Edit Application</a></span>
                                </div>
                            </div>
                            <div class="pointer d-flex align-items-center">
                                <img src="{{ asset('public/img/save_alt_material.png')}}" alt="" class="pr-2 add-icon">
                                <span class="loan-box-button">Download Application</span>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex flex-row justify-content-center">
                        <div deals="" dealstate="Processing" class="info-box">
                            <div class="d-flex align-items-center flex-column pb-4">
                                <div class="w-75 d-flex flex-column align-items-center text-justify">
                                    <div class="w-70 gray-text text-center pt-4">
                                        <h2 class="gray-text">
                                            <svg class="svg-inline--fa fa-circle fa-w-16" data-icon="circle" viewBox="0 0 512 512">
                                                <path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm0 448c-110.5 0-200-89.5-200-200S145.5 56 256 56s200 89.5 200 200-89.5 200-200 200z">
                                                </path>
                                            </svg>
                                        </h2>
                                    </div>
                                    <div class="text-center">
                                        <h4>Success! We are reviewing your application.</h4>
                                    </div>
                                    <div class="gray-text text-center">
                                        <span>Our team is working to find you the best options for your business. You will be notified when an offer is received.</span>
                                    </div>
                                </div>
                                <div class="w-75 d-flex flex-row justify-content-around pt-3">
                                    <div class="d-flex flex-column align-items-center"></div>
                                    <div class="d-flex flex-column align-items-center">
                                        <div class="gray-text"></div>
                                    </div>
                                </div>
                                <div class="w-75 d-flex flex-row justify-content-around pt-3"></div>
                            </div>
                            <div>
                                <div class="status-tracker">
                                    <div class="circle dark-blue">
                                        <span class="status-label">Application Received</span>
                                        <span class="date">2/18/21</span>
                                    </div>
                                    <span class="bar full-gradient"></span>
                                    <div class="circle  light-blue">
                                        <span class="status-label">Application Under Review</span>
                                        <span class="date">In Progress</span>
                                    </div>
                                    <span class="bar-gray"></span>
                                    <div class="circle-gray">
                                        <span class="status-label">Sent To Lender</span>
                                        <span class="date"></span>
                                    </div>
                                    <span class="bar-gray"></span>
                                    <div class="circle-gray">
                                        <span class="status-label">SBA Decision</span>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div id="VoidedCheck">
                                    <div class="voided-check-container w-75 align-items-center">
                                        <div>
                                            <h4>Please Upload a Voided Check</h4>
                                            <p class="close-text"> Lenders require a voided check in order to send you your PPP funds.
                                            </p>
                                            <p class="bigger-margin mb-5">
                                                <strong>Use a check from the payroll bank account for the business.</strong>
                                            </p>
                                        </div>
                                        <div>
                                            <div>
                                                <div class="upload-box-container mb-4">
                                                    <div class="upload-box">
                                                        <input type="file" accept=".bmp, .csv, .doc, .docx, .gif, .htm, .html, .jpeg, .jpg, .odc, .odp, .odt, .ots, .p7s, .pdf, .png, .ppt, .pptx, .svg, .tif, .tiff, .tsv, .txt, .vcf, .vcs, .xls, .xlsm, .xlsx, .xps" class="input-file">
                                                        <div class="upload-content">
                                                            <div class="upload-title">
                                                                <span class="upload-label">
                                                                    <strong>Voided Check</strong>
                                                                </span>
                                                                <p class="upload-text">
                                                                    <span class="upload-browse"><a href="javascript:void(0);">Browse files</a>
                                                                        <span class="d-none d-xl-inline ml-1">or drag and drop</span>
                                                                    </span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="errored"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </section>
        </div>
    </div>
    <div class="disclaimer">
        <div class="disclaimerThanks">
            <p>Thank you for applying for a Paycheck Protection loan!</p>
        </div>
        <p class="ppp-alert-content">Congratulations! We received your application and are moving it into processing. In the meantime, keep a close eye on emails from VFA. You can always log in to our site for instant status updates. We strongly encourage you to review our <a target="_blank" href="#">PPP Loan Relief</a> page offers answers to FAQs and Next Steps.
        </p>
    </div>
    <div class="next-steps row justify-content-center mt-5">
        <div class="col-md-5 col-12 justify-content-center justify-content-md-start">
            <h5>Have a question?</h5>
            <div class="portal-card">
                <section>
                    <div class="mr-md-2 funding-specialist-box d-flex">
                        <div class="fm-info">
                            <div class="font-medium">Funding Manager:</div>
                            <div class="font-medium">Ghumman Dilshad</div>
                            <a href="#" class="d-block">(469) 225-0569</a>
                            <a href="#">covid@virtualfundassist.com</a>
                        </div>
                        <div class="profile-img rounded-circle embed-responsive embed-responsive-1by1 float-md-right">
                            <div class="fallback-avatar embed-responsive-item d-flex justify-content-center align-items-center
                                                h3 text-uppercase text-light font-weight-bold">GD
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <div class="col-md-5 col-12">
            <h5>While You Wait</h5>
            <div class="portal-card wyw-links">
                <div class="d-flex align-items-center wyw-link-block">
                    <div class="mr-3 icon d-flex icon-list">
                        <img src="{{ asset('public/img/icon-list.png')}}" alt="">
                    </div>
                    <div>
                        <a href="#">Fill out our marketplace application to see if you qualify for other business financing</a>
                    </div>
                </div>
                <div class="d-flex align-items-center wyw-link-block">
                    <div class="mr-3 icon d-flex icon-file icon-list">
                        <img src="{{ asset('public/img/icon-file.png')}}" alt="">
                    </div>
                    <div>
                        <a href="#">Learn more about PPP loan forgiveness</a>
                    </div>
                </div>
                <div class="d-flex align-items-center wyw-link-block">
                    <div class="mr-3 icon d-flex icon-question icon-list">
                        <img src="{{ asset('public/img/icon-question.png')}}" alt="">
                    </div>
                    <div>
                        <a href="#">Still have questions? FAQs</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom-cta mb-5">
        <div class="gray-box">
            <div class="d-flex sunrise-header mb-4">
                <img src="{{ asset('public/img/sunrise-and-lendio-logo.svg')}}" alt="">
                <a href="#" class="btn btn-primary d-none d-md-block mt-0">Learn More</a>
            </div>
            <div class="d-flex">
                <div>
                    <p class="pt-2">Sunrise by VFA is a free bookkeeping software designed for your small business needs. Manage and track your PPP loan expenses, access loan forgiveness resources, and chat with a loan forgiveness advisor once your account is set up.</p>
                    <div class="d-md-flex">
                        <ul class="mb-0 mr-md-3">
                            <li>Leverage PPP Loan Forgiveness guides and checklists </li>
                            <li>Match and categorize PPP loan expenses</li>
                            <li>Create professional-looking invoices online or via our mobile app</li>
                        </ul>
                        <ul>
                            <li>Get free financial reports like cash flow and profit and loss statements</li>
                            <li>Plus, upgrade and get access to a dedicated professional bookkeeper</li>
                        </ul>
                    </div>
                    <div class="text-center d-md-none">
                        <a href="#" class="btn btn-primary">Learn More</a>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <p class="ppp-withdraw-link text-center mt-5">Withdraw My Application</p>
        </div>
    </div>
</div>
@endsection

@push('scripts')
@endpush
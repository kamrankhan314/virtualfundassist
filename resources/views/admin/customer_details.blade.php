@extends('layouts.main')
@push('styles')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
@endpush
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Customers</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Customers Details</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-12">

            <!-- /.card -->

            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Customers Details Listing features</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr><th>Options</th>
                                 <th>Financial Amount</th>
                                <th>Credit Card Score</th>
                                <th>Business Start Date</th>
                                <th>Average Month Revenue</th>
                                <th>Business Information</th>
                                <th>Business Percentage</th>
                                <th>Business Entity</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!$basicInfo->isEmpty())
                            @foreach($basicInfo as $info)
                            <tr> <td><a href="{{url('/admin/basicinfo')}}/{{$info->id}}" title="Show Full Details">View</a></td>
                            <td><a href="{{url('/admin/basicinfo')}}/{{$info->id}}" title="Show Full Details">{{$info->financial_amount}}</a></td>
                                <td>{{$info->credit_card_score}}</td>
                                <td>{{$info->business_start_date}}</td>
                                <td>{{$info->average_month_revenue}}</td>
                                <td>{{$info->business_information}}</td>
                                <td>{{$info->business_percentage}}</td>
                                <td>{{$info->business_entity}}</td>
                                
                            </tr>
                            @endforeach
                            @endif

                        </tbody>
                        <tfoot>
                            <tr><th>Options</th>
                                <th>Financial Amount</th>
                                <th>Credit Card Score</th>
                                <th>Business Start Date</th>
                                <th>Average Month Revenue</th>
                                <th>Business Information</th>
                                <th>Business Percentage</th>
                                <th>Business Entity</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->



    <div class="row">
        <div class="col-12">

            <!-- /.card -->

            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Customers Bank Details</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example2" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Bank Name</th>
                                <th>Bank Routring Number</th>
                                <th>Bank Account Number</th>
                                <th>Economic Injury Disaster loan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!$bankInfo->isEmpty())
                            @foreach($bankInfo as $info)
                            <tr>
                                <td>{{$info->bank_name}}</td>
                                <td>{{$info->bank_routring_number}}</td>
                                <td>{{$info->bank_account_number}}</td>
                                <td>{{$info->economicinnjury_disasterloan}}</td>
                            </tr>
                            @endforeach
                            @endif

                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Bank Name</th>
                                <th>Bank Routring Number</th>
                                <th>Bank Account Number</th>
                                <th>Economic Injury Disaster loan</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

@php 
$imageArray = array('png','jpe','jpeg','jpg','gif','bmp'); 

@endphp

    <div class="row">
        <div class="col-12">

            <!-- /.card -->

            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Customers Documents</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example3" class="table table-bordered table-striped">
                        <thead>
                            <tr> <th>Sr.No</th> 
                                <th>Title</th> 
                                <th>Documents</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($documents) 
                            <tr>
                                <th>1</th>
                                <th>Licence Front</th>
                                <td>
                                    @if(isset($documents->drivers_license_front)) 
                                          @php 
                                          $realpath       = $documents->drivers_license_front; 
                                          $realpathArray  = explode(".",$realpath);
                                          $fileExpenstion = end($realpathArray);
                                          @endphp
                                          @if(in_array(strtolower($fileExpenstion),$imageArray))
                                          <a href="{{asset('storage/app/'.$documents->drivers_license_front)}}" target="_blank"><img width="100" src="{{asset('storage/app/'.$documents->drivers_license_front)}}" /></a>
                                          @else
                                          <a href="{{asset('storage/app/'.$documents->drivers_license_front)}}" target="_blank">{{$documents->drivers_license_front}}</a>
                                          @endif
                                        @endif 
                                </td></tr> 

                                <tr>  
                                    <th>2</th>
                                    <th>Licence Back</th> 
                                <td>
                                     @if(!empty($documents->drivers_license_back)) 
                                          @php 
                                          $realpath       = $documents->drivers_license_back; 
                                          $realpathArray  = explode(".",$realpath);
                                          $fileExpenstion = end($realpathArray);
                                          @endphp
                                          @if(in_array(strtolower($fileExpenstion),$imageArray))
                                          <a href="{{asset('storage/app/'.$documents->drivers_license_back)}}" target="_blank"><img width="100" src="{{asset('storage/app/'.$documents->drivers_license_back)}}" /></a>
                                          @else
                                          <a href="{{asset('storage/app/'.$documents->drivers_license_back)}}" target="_blank">{{$documents->drivers_license_back}}</a>
                                          @endif
                                        @endif
 
                               
                                </td></tr> 
 
                                 <tr> <th>3</th> <th>Voided Check</th>
                                <td> 

                                    @if(!empty($documents->voided_check)) 
                                          @php 
                                          $realpath       = $documents->voided_check; 
                                          $realpathArray  = explode(".",$realpath);
                                          $fileExpenstion = end($realpathArray);
                                          @endphp
                                          @if(in_array(strtolower($fileExpenstion),$imageArray))
                                          <a href="{{asset('storage/app/'.$documents->voided_check)}}" target="_blank"><img width="100" src="{{asset('storage/app/'.$documents->voided_check)}}" /></a>
                                          @else
                                          <a href="{{asset('storage/app/'.$documents->voided_check)}}" target="_blank">{{$documents->voided_check}}</a>
                                          @endif
                                        @endif
                               
                                </td></tr> 

                                 <tr>  <th>4</th><th>Schedule C</th>
                                <td> 

                                   @if(!empty($documents->schedule_c)) 
                                          @php 
                                          $realpath       = $documents->schedule_c; 
                                          $realpathArray  = explode(".",$realpath);
                                          $fileExpenstion = end($realpathArray);
                                          @endphp
                                          @if(in_array(strtolower($fileExpenstion),$imageArray))
                                          <a href="{{asset('storage/app/'.$documents->schedule_c)}}" target="_blank"><img width="100" src="{{asset('storage/app/'.$documents->schedule_c)}}" /></a>
                                          @else
                                          <a href="{{asset('storage/app/'.$documents->schedule_c)}}" target="_blank">{{$documents->schedule_c}}</a>
                                          @endif
                                        @endif
                               
                                </td></tr> 

                                 <tr>  <th>5</th><th>Bank Statement</th>
                                <td> 
                                   @if(!empty($documents->bank_statement)) 
                                          @php 
                                          $realpath       = $documents->bank_statement; 
                                          $realpathArray  = explode(".",$realpath);
                                          $fileExpenstion = end($realpathArray);
                                          @endphp
                                          @if(in_array(strtolower($fileExpenstion),$imageArray))
                                          <a href="{{asset('storage/app/'.$documents->bank_statement)}}" target="_blank"><img width="100" src="{{asset('storage/app/'.$documents->bank_statement)}}" /></a>
                                          @else
                                          <a href="{{asset('storage/app/'.$documents->bank_statement)}}" target="_blank">{{$documents->bank_statement}}</a>
                                          @endif
                                        @endif
                                </td></tr> 

                                 <tr>  <th>6</th>
                                     <th>Business Formation Doc</th>
                                <td> 
                               @if(!empty($documents->business_formation_doc)) 
                                          @php 
                                          $realpath       = $documents->business_formation_doc; 
                                          $realpathArray  = explode(".",$realpath);
                                          $fileExpenstion = end($realpathArray);
                                          @endphp
                                          @if(in_array(strtolower($fileExpenstion),$imageArray))
                                          <a href="{{asset('storage/app/'.$documents->business_formation_doc)}}" target="_blank"><img width="100" src="{{asset('storage/app/'.$documents->business_formation_doc)}}" /></a>
                                          @else
                                          <a href="{{asset('storage/app/'.$documents->business_formation_doc)}}" target="_blank">{{$documents->business_formation_doc}}</a>
                                          @endif
                                        @endif
                                </td>
                            </tr> 
                            @endif

                        </tbody>
                        <tfoot>
                            <tr>  <th>Sr.No</th>
                                <th>Title</th>
                            
                                
                                <th>Documents</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

</section>
<!-- /.content -->


@endsection


@push('scripts')
<!-- DataTables -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
    $(document).ready(function () {
        $('#example1').DataTable({
            "scrollX": true,
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
        });

         $('#example2').DataTable();$('#example3').DataTable();
    });
</script>
@endpush
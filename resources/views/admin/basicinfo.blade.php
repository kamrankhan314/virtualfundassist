@extends('layouts.main')
@push('styles') 
@endpush
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Customers Basic Information</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Customers Information</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-12">

            <!-- /.card -->

            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Customers Details Information</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body p-0"> 
                         <div class="table-responsive mailbox-messages">
                <table class="table table-hover table-striped">
                  <tbody>
                  <tr> 
                      <td class="mailbox-name"><a href="#">Financial Amount</a></td>
                    <td class="mailbox-subject"><b>{{$info->financial_amount}}
                    </td> 
                  </tr>
                   <tr><td class="mailbox-name"><a href="#">Credit Card Score</a></td>
                    <td class="mailbox-subject"><b>{{$info->credit_card_score}}</td></tr>
                        <tr><td class="mailbox-name"><a href="#">Business Start Date</a></td>
                    <td class="mailbox-subject"><b>{{$info->business_start_date}}</td></tr>
                        <tr><td class="mailbox-name"><a href="#">Average Month Revenue</a></td>
                    <td class="mailbox-subject"><b>{{$info->average_month_revenue}}</td></tr>
                        <tr><td class="mailbox-name"><a href="#">Business Information</a></td>
                    <td class="mailbox-subject"><b>{{$info->business_information}}</td></tr>
                        <tr><td class="mailbox-name"><a href="#">Business Percentage</a></td>
                    <td class="mailbox-subject"><b>{{$info->business_percentage}}</td></tr>
                        <tr><td class="mailbox-name"><a href="#">Business Entity</a></td>
                    <td class="mailbox-subject"><b>{{$info->business_entity}}</td></tr>


                    <tr><td class="mailbox-name"><a href="#">Business Description</a></td>
                    <td class="mailbox-subject"><b>{{$info->business_description}}</td></tr>

                        <tr><td class="mailbox-name"><a href="#">Business State</a></td>
                    <td class="mailbox-subject"><b>{{$info->business_state}}</td></tr>
                    <tr><td class="mailbox-name"><a href="#">What is more important to you</a></td>
                    <td class="mailbox-subject"><b>{{$info->whatismore_important_toyou}}</td></tr>
                    <tr><td class="mailbox-name"><a href="#">Business Name</a></td>
                    <td class="mailbox-subject"><b>{{$info->business_name}}</td></tr>
                         <tr><td class="mailbox-name"><a href="#">Business Address</a></td>
                    <td class="mailbox-subject"><b>{{$info->business_address}}</td></tr>


                                     <tr><td class="mailbox-name"><a href="#">City</a></td>
                    <td class="mailbox-subject"><b>{{$info->city}}</td></tr>

                                          <tr><td class="mailbox-name"><a href="#">State </a></td>
                    <td class="mailbox-subject"><b>{{$info->state_id}}</td></tr>

                                                        <tr><td class="mailbox-name"><a href="#">Zip Code</a></td>
                    <td class="mailbox-subject"><b>{{$info->zip_code}}</td></tr>
                                        <tr><td class="mailbox-name"><a href="#">Mobile Number</a></td>
                    <td class="mailbox-subject"><b>{{$info->mobile_number}}</td></tr>

                                             <tr><td class="mailbox-name"><a href="#">Tax ID</a></td>
                    <td class="mailbox-subject"><b>{{$info->tax_id}}</td></tr>

                         <tr><td class="mailbox-name"><a href="#">Business Ownership Type</a></td>
                    <td class="mailbox-subject"><b>{{$info->business_ownership_type}}</td></tr>

                        <tr><td class="mailbox-name"><a href="#">Annual Personal Income</a></td>
                    <td class="mailbox-subject"><b>{{$info->annual_personal_income}}</td></tr>

                             <tr><td class="mailbox-name"><a href="#">Existing Business Debt</a></td>
                    <td class="mailbox-subject"><b>{{$info->existing_business_debt}}</td></tr>

                             <tr><td class="mailbox-name"><a href="#">Do You Invoice Customers</a></td>
                    <td class="mailbox-subject"><b>{{$info->is_invoice_customers}}</td></tr>


                             <tr><td class="mailbox-name"><a href="#">Is Seasonal Business</a></td>
                    <td class="mailbox-subject"><b>{{$info->is_seasonal}}</td></tr>

                         <tr><td class="mailbox-name"><a href="#">Do You accept credit cards</a></td>
                    <td class="mailbox-subject"><b>{{$info->is_accept_credit_cards}}</td></tr>


                         <tr><td class="mailbox-name"><a href="#">Do You use accounting software</a></td>
                    <td class="mailbox-subject"><b>{{$info->is_use_accounting_software}}</td></tr>


                         <tr><td class="mailbox-name"><a href="#">is your business non profit</a></td>
                    <td class="mailbox-subject"><b>{{$info->is_business_non_profit}}</td></tr>


                         <tr><td class="mailbox-name"><a href="#">is you business franchise</a></td>
                    <td class="mailbox-subject"><b>{{$info->is_business_franchise}}</td></tr>


                         <tr><td class="mailbox-name"><a href="#">Status</a></td>
                    <td class="mailbox-subject"><b>{{$info->status}}</td></tr>
                  </tbody>
                </table>
                <!-- /.table -->
              </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row --> 

</section>
<!-- /.content -->


@endsection 

@push('scripts') 
<script>
    $(document).ready(function () {
  
    });
</script>
@endpush
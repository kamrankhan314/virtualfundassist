@extends('layouts.master')
@push('styles')
<style>

.logPathway-widget {
    height: 50vh;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    padding: 20px;
    background-color: aliceblue;
    border-radius: 20px;
}
</style>
@endpush
@section('content')
<section class="logPathway">
    <div class="cotainer">
        <div class="row">
            <div class="col col-md-6 col-12">
                <div class="logPathway-widget">
                    <h4>Paycheck Protection Program Application</h4>
                    <a href="{{route('ppp.basicinfo')}}" role="button" class="btn btn-primary mb-2">Apply PPP</a>
                </div>
            </div>
            
            <div class="col col-md-6 col-12">
                <div class="logPathway-widget">
                    <h4>Standard Business Financing Application</h4>
                    <a href="{{route('basicinfo')}}" role="button" class="btn btn-primary mb-2">Apply SBF</a>
                </div>
            </div> 
        </div>
    </div>
</section>

@endsection

@push('scripts')
@endpush